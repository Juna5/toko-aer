<?php

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::middleware(['auth', 'impersonate'])->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/absent-form', 'HomeController@showAbsent')->name('home.show_absent');
    Route::get('/absent-form-checkout', 'HomeController@showAbsentCheckout')->name('home.show_absent_checkout');
    Route::post('/absent-store', 'HomeController@storeAbsent')->name('home.store_absent');

    Route::get('impersonate', 'ImpersonateController@index')->name('impersonate.index');
    Route::post('impersonate/datatable', 'ImpersonateController@datatable')->name('impersonate.datatable');
    Route::get('/users/{id}/impersonate', 'ImpersonateController@impersonate')->name('impersonate.impersonate');
    Route::get('/users/stop', 'ImpersonateController@stopImpersonating')->name('impersonate.stop');

    Route::resource('profile', 'ProfileController')->only(['index', 'store']);

    Route::namespace('Admin')->prefix('admin')->name('admin.')->group(function () {
        Route::resource('users', 'UserController');
        Route::resource('roles', 'RoleController');
        Route::resource('permissions', 'PermissionController');
    });
});

Route::resource('logo', 'LogoController');
