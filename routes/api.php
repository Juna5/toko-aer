<?php

Route::namespace('Api\v1')->group(function () {
    Route::post('auth/login', 'Auth\LoginController@login');
    Route::post('auth/logout', 'Auth\LogoutController');
    Route::post('auth/login-using-email', 'Auth\LoginController@loginWithEmail');
    Route::post('auth/register', 'Auth\RegisterController@register');

    # Protected Route
    Route::middleware('auth:api')->group(function () {
        Route::post('absent/check-in', 'AbsentController@checkin');
        Route::post('absent/check-out', 'AbsentController@checkout');
        Route::get('absent', 'AbsentController@index');
        Route::apiResource('product', 'ProductController');
        Route::get('show-product', 'ProductController@show');
        Route::apiResource('customer', 'CustomerController');
        Route::apiResource('tax', 'TaxController');
        Route::apiResource('supplier', 'SupplierController');

        Route::apiResource('sales-order', 'SalesOrderController');
        Route::delete('sales-order-delete/{id}', 'SalesOrderController@destroy');

        Route::post('sales-order-cancel/{id}', 'SalesOrderController@cancel');
        Route::post('sales-order-cancel-approval/{id}', 'SalesOrderController@cancelApproved');


        Route::get('cancel-approved-list', 'SalesOrderController@cancelApprovedListPerDepot');
        Route::get('transaction-amt-summary', 'SalesOrderController@transactionAmtSummaryPerDepot');
        Route::get('transaction-summary', 'SalesOrderController@transactionSummaryPerDepot');
        Route::get('leader-depot', 'SalesOrderController@getLeaderDepot');
        Route::get('transaction-list', 'SalesOrderController@transactionList');
        Route::post('assign-voucher', 'SalesOrderController@assignVoucher');
        Route::get('transaction-per-depot', 'SalesOrderController@transactionListPerDepot');
        Route::get('auth/user', function () {
            return response()->json(request()->user());
        });

        Route::apiResource('profile', 'ProfileController');

    });

    Route::post('image/destroy', 'ImageController@destroy')->name('image.destroy');
});
