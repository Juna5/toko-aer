(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./Modules/HR/Resources/assets/js/setting_shift/Create.vue":
/*!*****************************************************************!*\
  !*** ./Modules/HR/Resources/assets/js/setting_shift/Create.vue ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_281f828a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=281f828a& */ "./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=template&id=281f828a&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_281f828a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_281f828a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/HR/Resources/assets/js/setting_shift/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=script&lang=js&":
/*!******************************************************************************************!*\
  !*** ./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=template&id=281f828a&":
/*!************************************************************************************************!*\
  !*** ./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=template&id=281f828a& ***!
  \************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_281f828a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=281f828a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=template&id=281f828a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_281f828a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_281f828a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue":
/*!**************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_2c6603db___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=2c6603db& */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=template&id=2c6603db&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_2c6603db___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_2c6603db___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=template&id=2c6603db&":
/*!*********************************************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=template&id=2c6603db& ***!
  \*********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_2c6603db___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=2c6603db& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=template&id=2c6603db&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_2c6603db___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_2c6603db___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue":
/*!************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_56537fa9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=56537fa9& */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=template&id=56537fa9&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_56537fa9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_56537fa9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=template&id=56537fa9&":
/*!*******************************************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=template&id=56537fa9& ***!
  \*******************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_56537fa9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=56537fa9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=template&id=56537fa9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_56537fa9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_56537fa9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue":
/*!****************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Supplier_vue_vue_type_template_id_05f8feeb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Supplier.vue?vue&type=template&id=05f8feeb& */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=template&id=05f8feeb&");
/* harmony import */ var _Supplier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Supplier.vue?vue&type=script&lang=js& */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Supplier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Supplier_vue_vue_type_template_id_05f8feeb___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Supplier_vue_vue_type_template_id_05f8feeb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Supplier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Supplier.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Supplier_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=template&id=05f8feeb&":
/*!***********************************************************************************************************************!*\
  !*** ./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=template&id=05f8feeb& ***!
  \***********************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Supplier_vue_vue_type_template_id_05f8feeb___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Supplier.vue?vue&type=template&id=05f8feeb& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=template&id=05f8feeb&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Supplier_vue_vue_type_template_id_05f8feeb___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Supplier_vue_vue_type_template_id_05f8feeb___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue":
/*!********************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_1e9b3efc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=1e9b3efc& */ "./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=template&id=1e9b3efc&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_1e9b3efc___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_1e9b3efc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=template&id=1e9b3efc&":
/*!***************************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=template&id=1e9b3efc& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_1e9b3efc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=1e9b3efc& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=template&id=1e9b3efc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_1e9b3efc___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_1e9b3efc___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue":
/*!******************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_f8e64be0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=f8e64be0& */ "./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=template&id=f8e64be0&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_f8e64be0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_f8e64be0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=template&id=f8e64be0&":
/*!*************************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=template&id=f8e64be0& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_f8e64be0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=f8e64be0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=template&id=f8e64be0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_f8e64be0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_f8e64be0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue":
/*!**************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_7d0829d2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=7d0829d2& */ "./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=template&id=7d0829d2&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_7d0829d2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_7d0829d2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Stock/Resources/assets/js/components/stock_in/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=template&id=7d0829d2&":
/*!*********************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=template&id=7d0829d2& ***!
  \*********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_7d0829d2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=7d0829d2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=template&id=7d0829d2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_7d0829d2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_7d0829d2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue":
/*!************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_18f205e5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=18f205e5& */ "./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=template&id=18f205e5&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_18f205e5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_18f205e5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=template&id=18f205e5&":
/*!*******************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=template&id=18f205e5& ***!
  \*******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_18f205e5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=18f205e5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=template&id=18f205e5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_18f205e5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_18f205e5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue":
/*!********************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_5ec8cc5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=5ec8cc5e& */ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=template&id=5ec8cc5e&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_5ec8cc5e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_5ec8cc5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=template&id=5ec8cc5e&":
/*!***************************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=template&id=5ec8cc5e& ***!
  \***************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_5ec8cc5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=5ec8cc5e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=template&id=5ec8cc5e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_5ec8cc5e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_5ec8cc5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue":
/*!******************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_c87bc9c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=c87bc9c2& */ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=template&id=c87bc9c2&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_c87bc9c2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_c87bc9c2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=template&id=c87bc9c2&":
/*!*************************************************************************************************************!*\
  !*** ./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=template&id=c87bc9c2& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_c87bc9c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=c87bc9c2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=template&id=c87bc9c2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_c87bc9c2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_c87bc9c2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue":
/*!********************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Create_vue_vue_type_template_id_359fa112___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Create.vue?vue&type=template&id=359fa112& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=template&id=359fa112&");
/* harmony import */ var _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Create.vue?vue&type=script&lang=js& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Create_vue_vue_type_template_id_359fa112___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Create_vue_vue_type_template_id_359fa112___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=template&id=359fa112&":
/*!***************************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=template&id=359fa112& ***!
  \***************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_359fa112___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Create.vue?vue&type=template&id=359fa112& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=template&id=359fa112&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_359fa112___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Create_vue_vue_type_template_id_359fa112___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue":
/*!**********************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue ***!
  \**********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Customer_vue_vue_type_template_id_65505774___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Customer.vue?vue&type=template&id=65505774& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=template&id=65505774&");
/* harmony import */ var _Customer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Customer.vue?vue&type=script&lang=js& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Customer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Customer_vue_vue_type_template_id_65505774___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Customer_vue_vue_type_template_id_65505774___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Customer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=template&id=65505774&":
/*!*****************************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=template&id=65505774& ***!
  \*****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_template_id_65505774___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Customer.vue?vue&type=template&id=65505774& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=template&id=65505774&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_template_id_65505774___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Customer_vue_vue_type_template_id_65505774___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue":
/*!******************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Edit_vue_vue_type_template_id_de1e02c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Edit.vue?vue&type=template&id=de1e02c0& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=template&id=de1e02c0&");
/* harmony import */ var _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Edit.vue?vue&type=script&lang=js& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Edit_vue_vue_type_template_id_de1e02c0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Edit_vue_vue_type_template_id_de1e02c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=template&id=de1e02c0&":
/*!*************************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=template&id=de1e02c0& ***!
  \*************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_de1e02c0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Edit.vue?vue&type=template&id=de1e02c0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=template&id=de1e02c0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_de1e02c0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Edit_vue_vue_type_template_id_de1e02c0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue":
/*!**************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PriceHistory_vue_vue_type_template_id_4fecc121___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PriceHistory.vue?vue&type=template&id=4fecc121& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=template&id=4fecc121&");
/* harmony import */ var _PriceHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PriceHistory.vue?vue&type=script&lang=js& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PriceHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PriceHistory_vue_vue_type_template_id_4fecc121___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PriceHistory_vue_vue_type_template_id_4fecc121___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PriceHistory.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceHistory_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=template&id=4fecc121&":
/*!*********************************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=template&id=4fecc121& ***!
  \*********************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceHistory_vue_vue_type_template_id_4fecc121___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./PriceHistory.vue?vue&type=template&id=4fecc121& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=template&id=4fecc121&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceHistory_vue_vue_type_template_id_4fecc121___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_PriceHistory_vue_vue_type_template_id_4fecc121___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue":
/*!*********************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Product_vue_vue_type_template_id_4577b449___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Product.vue?vue&type=template&id=4577b449& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=template&id=4577b449&");
/* harmony import */ var _Product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Product.vue?vue&type=script&lang=js& */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Product_vue_vue_type_template_id_4577b449___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Product_vue_vue_type_template_id_4577b449___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Product.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Product_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=template&id=4577b449&":
/*!****************************************************************************************************************************!*\
  !*** ./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=template&id=4577b449& ***!
  \****************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Product_vue_vue_type_template_id_4577b449___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../../node_modules/vue-loader/lib??vue-loader-options!./Product.vue?vue&type=template&id=4577b449& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=template&id=4577b449&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Product_vue_vue_type_template_id_4577b449___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Product_vue_vue_type_template_id_4577b449___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      errors: "",
      loading: false,
      show_start: false,
      save_loading: false,
      selectedMonth: null,
      validationErrors: "",
      selectedYear: null,
      month: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
      years: [],
      dataCalendar: [],
      employees: [],
      selectedEmployeeShift1: [],
      selectedEmployeeShift2: []
    };
  },
  mounted: function mounted() {
    this.fetchYear();
    this.fetchEmployee();
  },
  watch: {
    selectedEmployeeShift1: {
      handler: function handler() {
        var _this = this;

        var selectedEmployeeShifts1 = this.selectedEmployeeShift1;
        selectedEmployeeShifts1.map(function (item, i) {
          // remove duplicate before entry
          _this.dataCalendar[i].shift1 = [];
          item.map(function (empl, j) {
            _this.dataCalendar[i].shift1.push({
              id: empl.id
            });
          });
        });
      }
    },
    selectedEmployeeShift2: {
      handler: function handler() {
        var _this2 = this;

        var selectedEmployeeShifts2 = this.selectedEmployeeShift2;
        selectedEmployeeShifts2.map(function (item, i) {
          // remove duplicate before entry
          _this2.dataCalendar[i].shift2 = [];
          item.map(function (empl, j) {
            _this2.dataCalendar[i].shift2.push({
              id: empl.id
            });
          });
        });
      }
    }
  },
  methods: {
    fetchYear: function fetchYear() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/setting/year");

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                result.map(function (res) {
                  _this3.years.push({
                    name: res
                  });
                });
                _context.next = 13;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 10]]);
      }))();
    },
    fetchEmployee: function fetchEmployee() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/hr/setting/shift_setting/get-status-employee");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this4.employees.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchCalendar: function fetchCalendar() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this5.dataCalendar = [];
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /hr/setting/shift_setting/get-calendar/".concat(_this5.selectedYear.name, "/").concat(_this5.selectedMonth));

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (item) {
                  _this5.dataCalendar.push({
                    id: item.id,
                    date: item.date,
                    day: item.days_of_week,
                    shift1: [],
                    shift2: []
                  });
                });

              case 8:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    handleSubmit: function handleSubmit() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var data, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this6.loading = true;
                _context4.prev = 1;
                data = {};
                data["setting_shift"] = _this6.dataCalendar;

                if (!_this6.dataCalendar) {
                  _context4.next = 11;
                  break;
                }

                _context4.next = 7;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/hr/setting/shift_setting", data);

              case 7:
                response = _context4.sent;

                if (response.data.success == true) {
                  window.location.href = response.data.redirect;
                } else {
                  window.location.href = response.data.redirect;
                } // window.location.href = "/hr/setting/shift_setting";


                _context4.next = 12;
                break;

              case 11:
                alert("Please check the input");

              case 12:
                _context4.next = 18;
                break;

              case 14:
                _context4.prev = 14;
                _context4.t0 = _context4["catch"](1);
                _this6.loading = false;

                if (_context4.t0.response.status === 422) {
                  console.log(_context4.t0.response.status);
                  _this6.validationErrors = _context4.t0.response.data.errors;
                } else {
                  alert("Please check the input");
                }

              case 18:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[1, 14]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _Supplier__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./Supplier */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["prId"],
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__["default"],
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default.a,
    Supplier: _Supplier__WEBPACK_IMPORTED_MODULE_7__["default"]
  },
  data: function data() {
    return {
      products: [],
      form: {
        discountAllItem: 0,
        getTotal: 0,
        subTotal: 0,
        tax: "",
        payment_method_id: 3,
        payment_term_id: 3,
        // date: new Date().toISOString().substr(0, 10),
        currency_id: 1,
        supplier_id: null
      },
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedItem: [],
      selectedUom: [],
      validationErrors: "",
      selectedSupplier: null,
      selectedCurrency: {
        id: 1,
        name: "Rupiah"
      },
      selectedPaymentMethod: {
        id: 3,
        name: "Cash"
      },
      selectedPaymentTerm: {
        id: 3,
        name: "IMMEDIATE"
      },
      supplier: [],
      currency: [],
      paymentMethod: [],
      paymentTerm: [],
      show_start: false,
      product_id: null,
      billing: [],
      pr_history: []
    };
  },
  mounted: function mounted() {
    this.fetchTaxs();
    this.fetchItems();
    this.fetchUoms();
    this.fetchSupplier();
    this.fetchCurrency();
    this.fetchPaymentMethod();
    this.fetchPaymentTerm();
    this.callFromChild();
    var date = new Date();
    this.form.date = moment(date).format('Y-MM-D');
  },
  computed: {
    getTotal: function getTotal() {
      var total = this.products.reduce(function (a, b) {
        if (b.check) {
          return a + b.price * b.qty - (b.discountAmount ? b.discountAmount : 0) - (b.discountRate ? b.discountRate / 100 * b.price : 0) * b.qty;
        } else {
          return a + 0;
        }
      }, 0);
      this.form.getTotal = total ? total : 0;
      return total;
    },
    subTotal: function subTotal() {
      var total = this.form.getTotal - this.form.discountAllItem;
      this.form.subTotal = total ? total : 0;
      return total;
    },
    getPpn: function getPpn() {
      var total = this.form.tax / 100 * this.form.subTotal;
      this.form.ppnTotal = total ? total : 0;
      return total;
    },
    total: function total() {
      var total = this.form.subTotal + this.form.ppnTotal;
      this.form.total = total ? total : 0;
      return total;
    }
  },
  watch: {
    // item for product
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          if (item) {
            _this.products[i].item = item.id;
          } else {
            _this.products[i].item = null;
          }
        });
      }
    },
    selectedUom: {
      handler: function handler() {
        var _this2 = this;

        var selectedUoms = this.selectedUom;
        selectedUoms.map(function (uoms, i) {
          if (uoms) {
            _this2.products[i].uom = uoms.id;
          } else {
            _this2.products[i].uom = null;
          }
        });
      }
    },
    selectedSupplier: {
      handler: function handler() {
        if (this.selectedSupplier) {
          this.form.supplier_id = this.selectedSupplier.id;
        } else {
          this.form.supplier_id = null;
        }
      }
    },
    selectedCurrency: {
      handler: function handler() {
        if (this.selectedCurrency) {
          this.form.currency_id = this.selectedCurrency.id;
        } else {
          this.form.currency_id = null;
        }
      }
    },
    selectedPaymentMethod: {
      handler: function handler() {
        if (this.selectedPaymentMethod) {
          this.form.payment_method_id = this.selectedPaymentMethod.id;
        } else {
          this.form.payment_method_id = null;
        }
      }
    },
    selectedPaymentTerm: {
      handler: function handler() {
        if (this.selectedPaymentTerm) {
          this.form.payment_term_id = this.selectedPaymentTerm.id;
        } else {
          this.form.payment_term_id = null;
        }
      }
    }
  },
  methods: {
    callFromChild: function callFromChild() {
      var _this3 = this;

      this.$root.$on("refetch_the_supplier_from_po", function () {
        _this3.supplier = [];

        _this3.fetchSupplier();
      });
    },
    showSupplier: function showSupplier(e) {
      this.$modal.show('example-modal');
    },
    childMessageReceived: function childMessageReceived(value) {
      this.supplier = [];
      this.items = [];
    },
    showPriceHistory: function showPriceHistory(e, data, index) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result, procurementHistory, ph;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this4.product_id = data.item;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/billing-history/".concat(data.item));

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                _this4.billing = result;
                _context.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/procurement-history/".concat(data.item));

              case 10:
                procurementHistory = _context.sent;
                _context.next = 13;
                return procurementHistory.data;

              case 13:
                ph = _context.sent;
                _this4.pr_history = ph;

                _this4.$modal.show('price-history-modal');

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
      } else {
        return true;
      }
    },
    showDetail: function showDetail(event) {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var id, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = event.id;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/purchase-order/supplier-detail/".concat(id));

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;

                if (!result.currency) {
                  _this5.selectedCurrency = {
                    id: "",
                    name: ""
                  };
                } else {
                  _this5.selectedCurrency = {
                    id: result.currency_id,
                    name: result.currency.name
                  };
                }

                if (!result.payment_term) {
                  _this5.selectedPaymentTerm = {
                    id: "",
                    name: ""
                  };
                } else {
                  _this5.selectedPaymentTerm = {
                    id: result.payment_term_id,
                    name: result.payment_term.name
                  };
                }

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    fetchSupplier: function fetchSupplier() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/supplier");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this6.supplier.push({
                    id: res.id,
                    name: res.company_name
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    fetchCurrency: function fetchCurrency() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/currency");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this7.currency.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchPaymentMethod: function fetchPaymentMethod() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/payment-method");

              case 3:
                response = _context5.sent;
                _context5.next = 6;
                return response.data;

              case 6:
                result = _context5.sent;
                result.map(function (res) {
                  _this8.paymentMethod.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context5.next = 13;
                break;

              case 10:
                _context5.prev = 10;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 13:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 10]]);
      }))();
    },
    fetchPaymentTerm: function fetchPaymentTerm() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/payment-term");

              case 3:
                response = _context6.sent;
                _context6.next = 6;
                return response.data;

              case 6:
                result = _context6.sent;
                result.map(function (res) {
                  _this9.paymentTerm.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context6.next = 13;
                break;

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 10]]);
      }))();
    },
    fetchTaxs: function fetchTaxs() {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/tax");

              case 3:
                response = _context7.sent;
                _context7.next = 6;
                return response.data;

              case 6:
                result = _context7.sent;
                _this10.form.tax_id = result.id;
                _this10.form.tax = parseInt(result.rate);
                _context7.next = 14;
                break;

              case 11:
                _context7.prev = 11;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 14:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 11]]);
      }))();
    },
    fetchItems: function fetchItems() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _context8.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/product");

              case 3:
                response = _context8.sent;
                _context8.next = 6;
                return response.data;

              case 6:
                result = _context8.sent;
                result.map(function (res) {
                  _this11.items.push({
                    id: res.id,
                    name: res.code + " - " + res.name
                  });
                });
                _context8.next = 13;
                break;

              case 10:
                _context8.prev = 10;
                _context8.t0 = _context8["catch"](0);
                console.log(_context8.t0);

              case 13:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 10]]);
      }))();
    },
    fetchItemDetails: function fetchItemDetails(e, i) {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                if (e) {
                  product_id = e.id;
                  _this12.products[i].price = null;
                  _this12.products[i].discountRate = null;
                  _this12.products[i].discountAmount = null;
                  _this12.products[i].uom = null; // this.selectedUom[i] =  null

                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-detail/".concat(product_id)).then(function (response) {
                    var uom = response.data.uom_id;
                    _this12.selectedUom[i] = {
                      id: response.data.uom_id,
                      name: response.data.uom.name
                    };
                    _this12.products[i].uom = uom;
                    console.log(_this12.selectedUom);

                    if (uom) {
                      axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(uom)).then(function (response) {
                        if (response.data) {
                          var price = response.data.price;
                          var discountRate = response.data.discount;
                          var discountAmount = response.data.discount_amount;
                          _this12.products[i].price = parseInt(price, 10);
                          _this12.products[i].discountRate = discountRate;
                          _this12.products[i].discountAmount = parseInt(discountAmount, 10);
                        }
                      })["catch"](function (e) {
                        return console.log(e);
                      });
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 1:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }))();
    },
    fetchUomDetails: function fetchUomDetails(e, i) {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                product_id = _this13.products[i].item;
                _this13.products[i].uom = null;
                _this13.products[i].price = null;
                _this13.products[i].discountRate = null;
                _this13.products[i].discountAmount = null;

                if (e) {
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(e.id)).then(function (response) {
                    if (response.data) {
                      var price = response.data.price;
                      var discountRate = response.data.discount;
                      var discountAmount = response.data.discount_amount;
                      _this13.products[i].price = parseInt(price, 10);
                      _this13.products[i].discountRate = discountRate;
                      _this13.products[i].discountAmount = parseInt(discountAmount, 10);
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 6:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context11.sent;
                _context11.next = 5;
                return response.data;

              case 5:
                result = _context11.sent;
                _this14.uoms = result;

              case 7:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }))();
    },
    addItem: function addItem() {
      this.products.push({
        item: "",
        uom: "",
        qty: null,
        remark: "",
        description: "",
        price: null,
        discountRate: null,
        discountAmount: null,
        check: true
      });
    },
    deleteItem: function deleteItem(i) {
      this.products.splice(i, 1);
    },
    handleSubmit: function handleSubmit() {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _this15.loading = true;
                _context12.prev = 1;
                data = {};
                data["date"] = _this15.form.date;
                data["time"] = _this15.form.time;
                data["supplier_id"] = _this15.form.supplier_id;
                data["currency_id"] = _this15.form.currency_id;
                data["payment_term_id"] = _this15.form.payment_term_id;
                data["payment_method_id"] = _this15.form.payment_method_id;
                data["products"] = _this15.products;
                data["get_total"] = _this15.form.getTotal;
                data["discount_all_item"] = _this15.form.discountAllItem;
                data["sub_total"] = _this15.form.subTotal;
                data["tax_id"] = _this15.form.tax_id;
                data["total"] = _this15.form.total;
                data["description"] = _this15.form.description;
                _context12.next = 18;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/procurement/purchase-order", data);

              case 18:
                response = _context12.sent;
                _context12.next = 21;
                return response;

              case 21:
                result = _context12.sent;

                if (result.data.success == true) {
                  window.location.href = "/procurement/purchase-order";
                } else {
                  alert(result.data.message);
                }

                _context12.next = 29;
                break;

              case 25:
                _context12.prev = 25;
                _context12.t0 = _context12["catch"](1);
                _this15.loading = false;

                if (_context12.t0.response.status === 422) {
                  console.log(_context12.t0.response.status);
                  _this15.validationErrors = _context12.t0.response.data.errors;
                } else {
                  alert("Please check the input");
                }

              case 29:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12, null, [[1, 25]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _Supplier__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./Supplier */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["id", "resubmit"],
  components: {
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default.a,
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Supplier: _Supplier__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    return {
      products: [],
      form: {
        title: "",
        supplier_id: null,
        currency_id: null,
        discountAllItem: null,
        getTotal: null,
        tax: "",
        time: null,
        gstTotal: null,
        subTotal: null,
        resubmit: null,
        project_id: null
      },
      show_start: false,
      show_end: false,
      cost_centers: [],
      suppliers: [],
      currency: [],
      paymentMethod: [],
      paymentTerm: [],
      departments: [],
      projects: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedSupplier: null,
      selectedCurrency: null,
      selectedPaymentMethod: null,
      selectedPaymentTerm: null,
      selectedProject: null,
      selectedItem: [],
      selectedUom: [],
      validationErrors: "",
      product_id: null,
      billing: [],
      pr_history: []
    };
  },
  mounted: function mounted() {
    this.fetchPo();
    this.fetchSupplier();
    this.fetchTaxs();
    this.fetchCurrency();
    this.fetchItems();
    this.fetchPaymentMethod();
    this.fetchPaymentTerm();
    this.fetchUoms();
    this.fetchProjectCode();
    this.callFromChild();
  },
  computed: {
    getTotal: function getTotal() {
      var total = this.products.reduce(function (a, b) {
        return a + b.price * b.qty - (b.discountAmount ? b.discountAmount : 0) - (b.discountRate ? b.discountRate / 100 * b.price : 0) * b.qty;
      }, 0);
      this.form.getTotal = total ? total : 0;
      return total;
    },
    subTotal: function subTotal() {
      var total = this.form.getTotal - this.form.discountAllItem;
      this.form.subTotal = total ? total : 0;
      return total;
    },
    getGst: function getGst() {
      var total = this.form.tax / 100 * this.form.subTotal;
      this.form.gstTotal = total ? total : 0;
      return total;
    },
    total: function total() {
      var total = this.form.subTotal + this.form.gstTotal;
      this.form.total = total ? total : 0;
      return total;
    }
  },
  watch: {
    changeDate: function changeDate() {
      var submitionDate = this.form.submition_date;
      this.form.required_date = moment(submitionDate).add(21, "days").format("Y-MM-D");
    },
    selectedDepartment: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        this.form.department_id = this.selectedDepartment.id;
      }
    },
    selectedSupplier: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedSupplier) {
          this.form.supplier_id = this.selectedSupplier.id;
        } else {
          this.form.supplier_id = null;
        }
      }
    },
    selectedCurrency: {
      handler: function handler() {
        if (this.selectedCurrency) {
          this.form.currency_id = this.selectedCurrency.id;
        } else {
          this.form.currency_id = null;
        }
      }
    },
    selectedPaymentMethod: {
      handler: function handler() {
        if (this.selectedPaymentMethod) {
          this.form.payment_method_id = this.selectedPaymentMethod.id;
        } else {
          this.form.payment_method_id = null;
        }
      }
    },
    selectedPaymentTerm: {
      handler: function handler() {
        if (this.selectedPaymentTerm) {
          this.form.payment_term_id = this.selectedPaymentTerm.id;
        } else {
          this.form.payment_term_id = null;
        }
      }
    },
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          selectedItems.map(function (item, i) {
            if (item) {
              _this.products[i].item = item.id;
            } else {
              _this.products[i].item = null;
            }
          });
        });
      }
    },
    selectedUom: {
      handler: function handler() {
        var _this2 = this;

        var selectedUoms = this.selectedUom;
        selectedUoms.map(function (uom, i) {
          if (uom) {
            _this2.products[i].uom = uom.id;
          } else {
            _this2.products[i].uom = null;
          }
        });
      }
    },
    selectedProject: {
      handler: function handler() {
        if (this.selectedProject) {
          this.form.project_id = this.selectedProject.id;
        } else {
          this.form.project_id = null;
        }
      }
    }
  },
  methods: {
    callFromChild: function callFromChild() {
      var _this3 = this;

      this.$root.$on("refetch_the_supplier_from_po", function () {
        _this3.fetchSupplier();
      });
    },
    showSupplier: function showSupplier(e) {
      this.$modal.show('example-modal');
    },
    childMessageReceived: function childMessageReceived(value) {
      this.items = [];
      this.fetchSupplier();
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
      } else {
        return true;
      }
    },
    showPriceHistory: function showPriceHistory(e, data, index) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result, procurementHistory, ph;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this4.product_id = data.item;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/billing-history/".concat(data.item));

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                _this4.billing = result;
                _context.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/procurement-history/".concat(data.item));

              case 10:
                procurementHistory = _context.sent;
                _context.next = 13;
                return procurementHistory.data;

              case 13:
                ph = _context.sent;
                _this4.pr_history = ph;

                _this4.$modal.show('price-history-modal');

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    fetchPo: function fetchPo() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/procurement/purchase_order/".concat(_this5.id));

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                _this5.form.title = result.title;
                _this5.form.purchase_requisition_id = result.purchase_requisition_id ? result.purchase_requisition.code_number : 0; // this.form.department = result.department_id;

                _this5.form.date = result.date;
                _this5.form.time = result.time;
                _this5.form.discountAllItem = parseInt(result.discount_all_item, 10); // push to selected cost center

                _this5.selectedSupplier = result.supplier;
                _this5.selectedCurrency = result.currency;
                _this5.selectedPaymentTerm = result.payment_term;
                _this5.selectedPaymentMethod = result.payment_method;
                _this5.selectedDepartment = result.department;
                _this5.selectedProject = result.project;
                result.items.map(function (item, i) {
                  // push to selected item
                  _this5.selectedItem.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this5.selectedUom.push({
                    id: item.uom_id,
                    name: item.uom.name
                  });

                  _this5.products.push({
                    id: item.id,
                    item: item.product.name,
                    qty: parseInt(item.qty, 10),
                    uom: item.uom_id,
                    description: item.description,
                    price: parseInt(item.price, 10),
                    discountRate: item.discount_rate,
                    discountAmount: parseInt(item.discount_amount, 10),
                    subTotal: parseInt(item.sub_total, 10)
                  });
                });
                _context2.next = 24;
                break;

              case 21:
                _context2.prev = 21;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 24:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 21]]);
      }))();
    },
    fetchDepartments: function fetchDepartments() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/department");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this6.departments.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    fetchSupplier: function fetchSupplier() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/supplier");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this7.suppliers.push({
                    id: res.id,
                    name: res.company_name
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchTaxs: function fetchTaxs() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/tax");

              case 3:
                response = _context5.sent;
                _context5.next = 6;
                return response.data;

              case 6:
                result = _context5.sent;
                _this8.form.tax_id = result.id;
                _this8.form.tax = parseInt(result.rate);
                _context5.next = 14;
                break;

              case 11:
                _context5.prev = 11;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 14:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 11]]);
      }))();
    },
    fetchCurrency: function fetchCurrency() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/currency");

              case 3:
                response = _context6.sent;
                _context6.next = 6;
                return response.data;

              case 6:
                result = _context6.sent;
                result.map(function (res) {
                  _this9.currency.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context6.next = 13;
                break;

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 10]]);
      }))();
    },
    fetchPaymentMethod: function fetchPaymentMethod() {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/payment-method");

              case 3:
                response = _context7.sent;
                _context7.next = 6;
                return response.data;

              case 6:
                result = _context7.sent;
                result.map(function (res) {
                  _this10.paymentMethod.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context7.next = 13;
                break;

              case 10:
                _context7.prev = 10;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 13:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 10]]);
      }))();
    },
    fetchPaymentTerm: function fetchPaymentTerm() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _context8.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/payment-term");

              case 3:
                response = _context8.sent;
                _context8.next = 6;
                return response.data;

              case 6:
                result = _context8.sent;
                result.map(function (res) {
                  _this11.paymentTerm.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context8.next = 13;
                break;

              case 10:
                _context8.prev = 10;
                _context8.t0 = _context8["catch"](0);
                console.log(_context8.t0);

              case 13:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 10]]);
      }))();
    },
    fetchItems: function fetchItems() {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.prev = 0;
                _context9.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/product");

              case 3:
                response = _context9.sent;
                _context9.next = 6;
                return response.data;

              case 6:
                result = _context9.sent;
                result.map(function (res) {
                  _this12.items.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context9.next = 13;
                break;

              case 10:
                _context9.prev = 10;
                _context9.t0 = _context9["catch"](0);
                console.log(_context9.t0);

              case 13:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, null, [[0, 10]]);
      }))();
    },
    fetchItemDetails: function fetchItemDetails(e, i) {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                if (e) {
                  product_id = e.id; // this.products[i].uom = null

                  _this13.products[i].price = null;
                  _this13.products[i].discountRate = null;
                  _this13.products[i].discountAmount = null;
                  _this13.selectedUom[i] = null;
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-detail/".concat(product_id)).then(function (response) {
                    var uom = response.data.uom_id;
                    console.log(uom);
                    _this13.products[i].uom = uom;
                    _this13.selectedUom[i] = {
                      id: uom,
                      name: response.data.uom.name
                    };
                    console.log(_this13.selectedUom[i]);

                    if (uom) {
                      axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(uom)).then(function (response) {
                        if (response.data) {
                          var price = response.data.price;
                          var discountRate = response.data.discount;
                          var discountAmount = response.data.discount_amount;
                          _this13.products[i].price = parseInt(price, 10);
                          _this13.products[i].discountRate = discountRate;
                          _this13.products[i].discountAmount = parseInt(discountAmount, 10);
                        }
                      })["catch"](function (e) {
                        return console.log(e);
                      });
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 1:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    },
    fetchUomDetails: function fetchUomDetails(e, i) {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                product_id = _this14.products[i].item; // this.products[i].uom = null

                _this14.products[i].price = null;
                _this14.products[i].discountRate = null;
                _this14.products[i].discountAmount = null;

                if (e) {
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(e.id)).then(function (response) {
                    if (response.data) {
                      var price = response.data.price;
                      var discountRate = response.data.discount;
                      var discountAmount = response.data.discount_amount;
                      _this14.products[i].price = parseInt(price, 10);
                      _this14.products[i].discountRate = discountRate;
                      _this14.products[i].discountAmount = parseInt(discountAmount, 10);
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 5:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context12.sent;
                _context12.next = 5;
                return response.data;

              case 5:
                result = _context12.sent;
                _this15.uoms = result;

              case 7:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    fetchProjectCode: function fetchProjectCode() {
      var _this16 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _context13.prev = 0;
                _context13.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/purchase-order/project");

              case 3:
                response = _context13.sent;
                _context13.next = 6;
                return response.data;

              case 6:
                result = _context13.sent;
                result.map(function (res) {
                  _this16.projects.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context13.next = 12;
                break;

              case 10:
                _context13.prev = 10;
                _context13.t0 = _context13["catch"](0);

              case 12:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, null, [[0, 10]]);
      }))();
    },
    addItem: function addItem() {
      this.products.push({
        item: "",
        uom: "",
        qty: null,
        remark: "",
        description: '',
        price: null,
        discountRate: null,
        discountAmount: null
      });
    },
    removeItem: function removeItem(itemId) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _context14.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/procurement/purchase_order/removeItem/".concat(itemId));

              case 2:
                response = _context14.sent;
                _context14.next = 5;
                return response.data;

              case 5:
                result = _context14.sent;

              case 6:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14);
      }))();
    },
    deleteItem: function deleteItem(i, itemId) {
      var _this17 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You can't revert your action",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes Delete it!",
        cancelButtonText: "No, Keep it!",
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          if (typeof itemId !== "undefined") {
            _this17.removeItem(itemId);
          }

          _this17.products.splice(i, 1);

          _this17.$swal("Deleted", "You successfully deleted this data", "success");
        } else {
          _this17.$swal("Cancelled", "Your file is still intact", "info");
        }
      });
    },
    handleSubmit: function handleSubmit() {
      var _this18 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee15() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                _this18.loading = true;
                _context15.prev = 1;
                data = {};
                data["_method"] = "PUT";
                data["title"] = _this18.form.title;
                data['project_id'] = _this18.form.project_id;
                data["date"] = _this18.form.date;
                data["supplier_id"] = _this18.form.supplier_id;
                data["currency_id"] = _this18.form.currency_id;
                data["time"] = _this18.form.time;
                data["purchase_requisition_id"] = _this18.form.purchase_requisition_id;
                data["payment_term_id"] = _this18.form.payment_term_id;
                data["payment_method_id"] = _this18.form.payment_method_id;
                data["products"] = _this18.products;
                data["get_total"] = _this18.form.getTotal;
                data["discount_all_item"] = _this18.form.discountAllItem;
                data["sub_total"] = _this18.form.subTotal;
                data["tax_id"] = _this18.form.tax_id;
                data["total"] = _this18.form.total;
                data["resubmit"] = _this18.resubmit;
                _context15.next = 22;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/procurement/purchase-order/".concat(_this18.id), data);

              case 22:
                response = _context15.sent;
                _context15.next = 25;
                return response;

              case 25:
                result = _context15.sent;

                if (result.data.success == true) {
                  window.location.href = "/procurement/purchase-order";
                } else {
                  alert(result.data.message);
                }

                _context15.next = 33;
                break;

              case 29:
                _context15.prev = 29;
                _context15.t0 = _context15["catch"](1);
                _this18.loading = false;

                if (_context15.t0.response.status === 422) {
                  _this18.errors = _context15.t0.response.data.errors;
                }

              case 33:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15, null, [[1, 29]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-js-modal */ "./node_modules/vue-js-modal/dist/index.js");
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_js_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


Vue.use(vue_js_modal__WEBPACK_IMPORTED_MODULE_2___default.a);

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  data: function data() {
    return {
      form: {
        company_name: "",
        name: "",
        roc: "",
        phone: "",
        fax: "",
        email: "",
        currency_id: "",
        price_category_id: "",
        payment_term_id: "",
        address: "",
        description: ""
      },
      resizable: false,
      adaptive: true,
      selectedPriceCategory: null,
      selectedCurrency: null,
      selectedPaymentTerm: null,
      validationErrors: "",
      price_category: [],
      paymentTerm: [],
      currency: []
    };
  },
  mounted: function mounted() {
    this.fetchCurrency();
    this.fetchPriceCategory();
    this.fetchPaymentTerm();
  },
  watch: {
    selectedPriceCategory: {
      handler: function handler() {
        if (this.selectedPriceCategory) {
          this.form.price_category_id = this.selectedPriceCategory.id;
        } else {
          this.form.price_category_id = null;
        }
      }
    },
    selectedCurrency: {
      handler: function handler() {
        if (this.selectedCurrency) {
          this.form.currency_id = this.selectedCurrency.id;
        } else {
          this.form.currency_id = null;
        }
      }
    },
    selectedPaymentTerm: {
      handler: function handler() {
        if (this.selectedPaymentTerm) {
          this.form.payment_term_id = this.selectedPaymentTerm.id;
        } else {
          this.form.payment_term_id = null;
        }
      }
    }
  },
  methods: {
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    fetchPriceCategory: function fetchPriceCategory() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/procurement/product/price-category");

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                result.map(function (res) {
                  _this.price_category.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context.next = 13;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 10]]);
      }))();
    },
    fetchCurrency: function fetchCurrency() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/currency");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this2.currency.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchPaymentTerm: function fetchPaymentTerm() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/purchase-order/payment-term");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this3.paymentTerm.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    onClickButton: function onClickButton() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this4.loading = true;
                _context4.prev = 1;
                data = {};
                data["company_name"] = _this4.form.company_name;
                data["name"] = _this4.form.name;
                data["phone"] = _this4.form.phone;
                data["fax"] = _this4.form.fax;
                data["email"] = _this4.form.email;
                data["currency_id"] = _this4.form.currency_id;
                data["price_category_id"] = _this4.form.price_category_id;
                data["payment_term_id"] = _this4.form.payment_term_id;
                data["address"] = _this4.form.address;
                data["description"] = _this4.form.description;
                _context4.next = 15;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/procurement/supplier", data);

              case 15:
                response = _context4.sent;
                _this4.form = {
                  company_name: "",
                  name: "",
                  roc: "",
                  phone: "",
                  fax: "",
                  email: "",
                  currency_id: "",
                  price_category_id: "",
                  payment_term_id: "",
                  address: "",
                  description: ""
                };
                _this4.selectedPriceCategory = null;
                _this4.selectedCurrency = null;
                _this4.selectedPaymentTerm = null;
                _this4.validationErrors = "";
                _context4.next = 23;
                return response;

              case 23:
                result = _context4.sent;

                _this4.$emit('messageFromChild');

                _this4.$root.$emit('refetch_the_supplier_from_po');

                _this4.$modal.hide('example-modal');

                _this4.$swal("Yeay!", "Your entry has been added successfully", "success");

                _context4.next = 34;
                break;

              case 30:
                _context4.prev = 30;
                _context4.t0 = _context4["catch"](1);
                _this4.loading = false;

                if (_context4.t0.response.status === 422) {
                  _this4.validationErrors = _context4.t0.response.data.errors;
                }

              case 34:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[1, 30]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['so', 'so-id'],
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__["default"],
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default.a
  },
  data: function data() {
    return {
      products: [],
      form: {
        paymentTerm: null,
        address: null,
        description: null
      },
      salesmans: [],
      customers: [],
      warehouses: [],
      departments: [],
      salesOrderMaterials: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedSalesman: null,
      selectedCustomer: null,
      selectedDepartment: null,
      selectedSalesOrderMaterial: null,
      selectedItem: [],
      selectedWarehouses: [],
      validationErrors: "",
      show_start: false,
      fetchLoading: false,
      showForm: this.soId ? true : false,
      customerDetail: "",
      show_customer_detail: false
    };
  },
  mounted: function mounted() {
    this.fetchDepartments();
    this.fetchSalesmens();
    this.fetchCustomers();
    this.fetchSalesOrderMaterial();
    this.fetchSalesOrderMaterialDetailSO();
    this.fetchItems();
    this.fetchWarehouses();
    this.fetchUoms();
    this.date();
  },
  computed: {},
  watch: {
    selectedDepartment: {
      handler: function handler() {
        this.form.department = this.selectedDepartment.id;
      }
    },
    selectedSalesman: {
      handler: function handler() {
        this.form.salesman = this.selectedSalesman.id;
      }
    },
    selectedCustomer: {
      handler: function handler() {
        this.form.customer = this.selectedCustomer.id;
      }
    },
    selectedQuotationMaterial: {
      handler: function handler() {
        this.form.quotationMaterial = this.selectedQuotationMaterial.id;
      }
    },
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          _this.products[i].item = item.id;
        });
      }
    },
    selectedWarehouses: {
      handler: function handler() {
        var _this2 = this;

        var selectedWarehouses = this.selectedWarehouses;
        selectedWarehouses.map(function (warehouse, i) {
          _this2.products[i].warehouse = warehouse.id;
        });
      }
    }
  },
  methods: {
    date: function date() {
      var date = new Date();
      this.form.date = moment(date).format('Y-MM-D');
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    callFromChildCustomer: function callFromChildCustomer() {
      var _this3 = this;

      this.$root.$on('refetch_the_customer_from_qm', function () {
        _this3.rerenderCustomers();
      });
    },
    showCustomer: function showCustomer(e) {
      console.log(e);
      this.$modal.show('customer-modal');
    },
    fetchDepartments: function fetchDepartments() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/department");

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                result.map(function (res) {
                  _this4.departments.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context.next = 13;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 10]]);
      }))();
    },
    fetchCustomers: function fetchCustomers() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this5.customers.push({
                    id: res.id,
                    name: res.company_name + " - " + res.pic_1 + " - " + res.pic_phone_1
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchCustomersDetail: function fetchCustomersDetail(e) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var responses, results, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _this6.customerDetail = "";
                _context3.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(e.id));

              case 4:
                responses = _context3.sent;
                _context3.next = 7;
                return responses.data;

              case 7:
                results = _context3.sent;
                _context3.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(results.customer_id));

              case 10:
                response = _context3.sent;
                _context3.next = 13;
                return response.data;

              case 13:
                result = _context3.sent;
                _this6.customerDetail = result;
                _context3.next = 20;
                break;

              case 17:
                _context3.prev = 17;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 20:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 17]]);
      }))();
    },
    rerenderCustomers: function rerenderCustomers() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(_this7.selectedCustomer.priceCategory));

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this7.customers.push({
                    id: res.id,
                    name: res.company_name + " - " + res.pic_1 + " - " + res.pic_phone_1,
                    priceCategory: res.price_category_id
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    childMessageReceivedCustomer: function childMessageReceivedCustomer(value) {
      this.customers = [];
      this.fetchCustomers();
    },
    fetchSalesmens: function fetchSalesmens() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/salesman");

              case 3:
                response = _context5.sent;
                _context5.next = 6;
                return response.data;

              case 6:
                result = _context5.sent;
                result.map(function (res) {
                  _this8.salesmans.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context5.next = 13;
                break;

              case 10:
                _context5.prev = 10;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 13:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 10]]);
      }))();
    },
    fetchSalesOrderMaterial: function fetchSalesOrderMaterial() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _this9.fetchLoading = true;
                _context6.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/stock/stock-cheking/sales_order");

              case 4:
                response = _context6.sent;
                _context6.next = 7;
                return response.data;

              case 7:
                result = _context6.sent;
                console.log(result);
                result.map(function (res) {
                  _this9.salesOrderMaterials.push({
                    id: res.id,
                    name: res.code_number
                  });
                });
                _this9.fetchLoading = false;
                _context6.next = 16;
                break;

              case 13:
                _context6.prev = 13;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 16:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 13]]);
      }))();
    },
    fetchSalesOrderMaterialDetail: function fetchSalesOrderMaterialDetail(event) {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var so_id, response, result, responses, results, customer, resultCustomer;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _this10.products = [];
                _this10.selectedItem = [];
                _this10.selectedWarehouses = [];
                _this10.form.department = "";
                _this10.form.customer = "";
                _this10.customerDetail = "";
                so_id = event.id;
                _context7.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(so_id));

              case 10:
                response = _context7.sent;
                _context7.next = 13;
                return response.data;

              case 13:
                result = _context7.sent;
                result.items.map(function (item, i) {
                  console.log(item);

                  if (item.delivered_qty > 0) {
                    _this10.selectedItem.push({
                      id: item.product.id,
                      name: item.product.name
                    });

                    if (item.warehouse) {
                      _this10.selectedWarehouses.push({
                        id: item.warehouse_id,
                        name: item.warehouse.name
                      });
                    }

                    _this10.products.push({
                      sales_order_id: item.sales_order_id,
                      sales_order_item_id: item.id,
                      purchaseQty: item.qty,
                      uom: item.uom.name,
                      sendQty: null,
                      confirmQty: null,
                      description: item.description
                    });
                  }
                });
                _this10.form.title = result.title; // this.form.description = result.description;

                _this10.form.address = result.customer.address;
                _this10.customer = result.customer.company_name + ' - ' + result.customer.pic_1 + ' - ' + result.customer.pic_phone_1;
                _this10.department = result.department.name;
                _this10.salesman = result.salesman.name;
                _this10.form.salesOrderMaterial = result.id;
                _this10.selectedDepartment = {
                  id: result.department_id,
                  name: result.department.name
                };
                _this10.selectedCustomer = {
                  id: result.customer_id,
                  name: result.customer.company_name + ' - ' + result.customer.pic_1 + ' - ' + result.customer.pic_phone_1 + ' - ' + result.customer.address
                };
                _this10.selectedSalesman = {
                  id: result.salesman_id,
                  name: result.salesman.name
                };
                _this10.showForm = true;
                _context7.next = 27;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(event.id));

              case 27:
                responses = _context7.sent;
                _context7.next = 30;
                return responses.data;

              case 30:
                results = _context7.sent;
                _context7.next = 33;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(results.customer_id));

              case 33:
                customer = _context7.sent;
                _context7.next = 36;
                return customer.data;

              case 36:
                resultCustomer = _context7.sent;
                _this10.customerDetail = resultCustomer;
                _context7.next = 43;
                break;

              case 40:
                _context7.prev = 40;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 43:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 40]]);
      }))();
    },
    fetchSalesOrderMaterialDetailSO: function fetchSalesOrderMaterialDetailSO() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var so_id, response, result, responses, results, customer, resultCustomer;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _this11.products = [];
                _this11.selectedItem = [];
                _this11.selectedWarehouses = [];
                _this11.form.department = "";
                _this11.form.customer = "";
                _this11.customerDetail = "";
                so_id = _this11.soId;

                if (!so_id) {
                  _context8.next = 41;
                  break;
                }

                _context8.next = 11;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(so_id));

              case 11:
                response = _context8.sent;
                _context8.next = 14;
                return response.data;

              case 14:
                result = _context8.sent;
                result.items.map(function (item, i) {
                  if (item.delivered_qty > 0) {
                    _this11.selectedItem.push({
                      id: item.product.id,
                      name: item.product.name
                    });

                    _this11.products.push({
                      sales_order_id: item.sales_order_id,
                      sales_order_item_id: item.id,
                      purchaseQty: item.delivered_qty,
                      uom: item.uom.name,
                      sendQty: "",
                      description: item.description
                    });
                  }
                });
                _this11.form.title = result.title;
                _this11.form.description = result.description;
                _this11.form.address = result.customer.address;
                _this11.customer = result.customer.company_name + ' - ' + result.customer.pic_1 + ' - ' + result.customer.pic_phone_1;
                _this11.department = result.department.name;
                _this11.salesman = result.salesman.name;
                _this11.form.salesOrderMaterial = result.id;
                _this11.selectedDepartment = {
                  id: result.department_id,
                  name: result.department.name
                };
                _this11.selectedSalesOrderMaterial = {
                  id: result.id,
                  name: result.code_number
                };
                _this11.selectedCustomer = {
                  id: result.customer_id,
                  name: result.customer.company_name + ' - ' + result.customer.pic_1 + ' - ' + result.customer.pic_phone_1 + ' - ' + result.customer.address
                };
                _this11.selectedSalesman = {
                  id: result.salesman_id,
                  name: result.salesman.name
                };
                _this11.showForm = true;
                _context8.next = 30;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(so_id));

              case 30:
                responses = _context8.sent;
                _context8.next = 33;
                return responses.data;

              case 33:
                results = _context8.sent;
                _context8.next = 36;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(results.customer_id));

              case 36:
                customer = _context8.sent;
                _context8.next = 39;
                return customer.data;

              case 39:
                resultCustomer = _context8.sent;
                _this11.customerDetail = resultCustomer;

              case 41:
                _context8.next = 46;
                break;

              case 43:
                _context8.prev = 43;
                _context8.t0 = _context8["catch"](0);
                console.log(_context8.t0);

              case 46:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 43]]);
      }))();
    },
    fetchItems: function fetchItems() {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.prev = 0;
                _context9.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/product");

              case 3:
                response = _context9.sent;
                _context9.next = 6;
                return response.data;

              case 6:
                result = _context9.sent;
                result.map(function (res) {
                  _this12.items.push({
                    id: res.id,
                    name: res.code + " - " + res.name
                  });
                });
                _context9.next = 13;
                break;

              case 10:
                _context9.prev = 10;
                _context9.t0 = _context9["catch"](0);
                console.log(_context9.t0);

              case 13:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, null, [[0, 10]]);
      }))();
    },
    fetchWarehouses: function fetchWarehouses() {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                _context10.prev = 0;
                _context10.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/warehouse");

              case 3:
                response = _context10.sent;
                _context10.next = 6;
                return response.data;

              case 6:
                result = _context10.sent;
                result.map(function (res) {
                  _this13.warehouses.push({
                    id: res.id,
                    name: res.code + " - " + res.pic
                  });
                });
                _context10.next = 13;
                break;

              case 10:
                _context10.prev = 10;
                _context10.t0 = _context10["catch"](0);
                console.log(_context10.t0);

              case 13:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, null, [[0, 10]]);
      }))();
    },
    fetchItemDetails: function fetchItemDetails(e, i) {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                id = e.id;
                axios__WEBPACK_IMPORTED_MODULE_1___default()("/transaction/quotation_material/product/".concat(id)).then(function (response) {
                  var price = response.data.price;
                  var description = response.data.description;
                  _this14.products[i].description = description, _this14.products[i].price = price;
                })["catch"](function (e) {
                  return console.log(e);
                });

              case 2:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                _context12.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context12.sent;
                _context12.next = 5;
                return response.data;

              case 5:
                result = _context12.sent;
                _this15.uoms = result;

              case 7:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    deleteItem: function deleteItem(i) {
      this.products.splice(i, 1);
    },
    handleSubmit: function handleSubmit() {
      var _this16 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _this16.loading = true;
                console.log(_this16.products);
                _context13.prev = 2;
                data = {};
                data["date"] = _this16.form.date;
                data["department_id"] = _this16.form.department;
                data["description"] = _this16.form.description;
                data["products"] = _this16.products;

                if (!(_this16.products == 0)) {
                  _context13.next = 13;
                  break;
                }

                alert("Product cannot be empty");
                _this16.loading = false;
                _context13.next = 21;
                break;

              case 13:
                console.log(data);
                _context13.next = 16;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/stock/stock-checking", data);

              case 16:
                response = _context13.sent;
                _context13.next = 19;
                return response;

              case 19:
                result = _context13.sent;

                if (result['data']['success'] === false) {
                  alert('Confirm Qty cant be greater than Purchased Qty');
                  _this16.loading = false;
                } else {
                  window.location.href = "/stock/stock-checking";
                }

              case 21:
                _context13.next = 27;
                break;

              case 23:
                _context13.prev = 23;
                _context13.t0 = _context13["catch"](2);
                _this16.loading = false;

                if (_context13.t0.response.status === 422) {
                  console.log(_context13.t0.response.status);
                  _this16.validationErrors = _context13.t0.response.data.errors;
                } else {
                  alert("Please check the input");
                }

              case 27:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13, null, [[2, 23]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["id", "resubmit"],
  components: {
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default.a,
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      products: [],
      form: {
        title: "",
        department: "",
        customer: "",
        salesman: "",
        description: "",
        address: "",
        resubmit: ""
      },
      show_start: false,
      show_end: false,
      customers: [],
      salesmans: [],
      warehouses: [],
      departments: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedCustomer: null,
      selectedDepartment: null,
      selectedSalesman: null,
      selectedWarehouse: [],
      selectedItem: [],
      validationErrors: "",
      fetchLoading: false,
      customerDetail: "",
      show_customer_detail: false
    };
  },
  mounted: function mounted() {
    this.fetchDOM();
    this.fetchCustomers();
    this.fetchSalesmens();
    this.fetchWarehouses();
    this.fetchItems();
    this.fetchUoms();
    this.fetchCustomersDetail();
  },
  watch: {
    selectedDepartment: {
      handler: function handler() {
        this.form.department = this.selectedDepartment.id;
      }
    },
    selectedCustomer: {
      handler: function handler() {
        this.form.customer = this.selectedCustomer.id;
      }
    },
    selectedSalesman: {
      handler: function handler() {
        this.form.salesman = this.selectedSalesman.id;
      }
    },
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          _this.products[i].item = item.id;
        });
      }
    },
    selectedWarehouse: {
      handler: function handler() {
        var _this2 = this;

        var selectedWarehouse = this.selectedWarehouse;
        selectedWarehouse.map(function (warehouse, i) {
          _this2.products[i].warehouse = warehouse.id;
        });
      }
    }
  },
  methods: {
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    fetchDOM: function fetchDOM() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _this3.fetchLoading = true;
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(_this3.id));

              case 4:
                response = _context.sent;
                _context.next = 7;
                return response.data;

              case 7:
                result = _context.sent;
                _this3.form.title = result.title;
                _this3.form.description = result.description;
                _this3.form.address = result.address;
                _this3.form.date = result.date;
                _this3.form.department = result.department_id;
                _this3.form.customer = result.customer_id;
                _this3.form.salesman = result.salesman_id;
                _this3.form.sales_order = result.code_number;
                _this3.form.sales_order_id = result.sales_order_id;
                _this3.selectedCustomer = {
                  id: result.customer_id,
                  name: result.customer.company_name + " - " + result.customer.pic_1 + " - " + result.customer.pic_phone_1 + " - " + result.customer.address
                };
                _this3.selectedSalesman = result.salesman;
                _this3.selectedDepartment = result.department;
                result.items.map(function (item, i) {
                  _this3.selectedItem.push({
                    id: item.product.product_id,
                    name: item.product.name
                  });

                  _this3.selectedWarehouse.push({
                    id: item.warehouse.id,
                    name: item.warehouse.name
                  });

                  _this3.products.push({
                    id: item.id,
                    item: item.product.product_id,
                    uom: item.uom_id,
                    sendQty: item.stock_qty,
                    purchaseQty: item.qty,
                    description: item.description,
                    warehouse: item.warehouse_id,
                    sales_order_item_id: item.id,
                    confirmQty: item.stock_qty
                  });
                });
                _this3.fetchLoading = false;
                _context.next = 27;
                break;

              case 24:
                _context.prev = 24;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 27:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 24]]);
      }))();
    },
    fetchCustomers: function fetchCustomers() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this4.customers.push({
                    id: res.id,
                    name: res.company_name + " - " + res.pic_1 + " - " + res.pic_phone_1
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchSalesmens: function fetchSalesmens() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/salesman");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this5.salesmans.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    fetchWarehouses: function fetchWarehouses() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/warehouse");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this6.warehouses.push({
                    id: res.id,
                    name: res.code + " - " + res.pic
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchItems: function fetchItems() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/product");

              case 3:
                response = _context5.sent;
                _context5.next = 6;
                return response.data;

              case 6:
                result = _context5.sent;
                result.map(function (res) {
                  _this7.items.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context5.next = 13;
                break;

              case 10:
                _context5.prev = 10;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 13:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 10]]);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context6.sent;
                _context6.next = 5;
                return response.data;

              case 5:
                result = _context6.sent;
                _this8.uoms = result;

              case 7:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    fetchCustomersDetail: function fetchCustomersDetail() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var responses, results, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _this9.customerDetail = "";
                _context7.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(_this9.id));

              case 4:
                responses = _context7.sent;
                _context7.next = 7;
                return responses.data;

              case 7:
                results = _context7.sent;
                _context7.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(results.customer_id));

              case 10:
                response = _context7.sent;
                _context7.next = 13;
                return response.data;

              case 13:
                result = _context7.sent;
                _this9.customerDetail = result;
                _context7.next = 20;
                break;

              case 17:
                _context7.prev = 17;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 20:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 17]]);
      }))();
    },
    removeItem: function removeItem(itemId) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/transaction/delivery_order_material/removeItem/".concat(itemId));

              case 2:
                response = _context8.sent;
                _context8.next = 5;
                return response.data;

              case 5:
                result = _context8.sent;

              case 6:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    },
    deleteItem: function deleteItem(i, itemId) {
      var _this10 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You can't revert your action",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes Delete it!",
        cancelButtonText: "No, Keep it!",
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          console.log(itemId);

          if (typeof itemId !== "undefined") {
            _this10.removeItem(itemId);
          }

          _this10.products.splice(i, 1);

          _this10.$swal("Deleted", "You successfully deleted this data", "success");
        } else {
          _this10.$swal("Cancelled", "Your file is still intact", "info");
        }
      });
    },
    handleSubmit: function handleSubmit() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _this11.loading = true;
                _context9.prev = 1;
                data = {};
                data["title"] = _this11.form.title;
                data["date"] = _this11.form.date;
                data["description"] = _this11.form.description;
                data["products"] = _this11.products; // console.log(data)

                _context9.next = 9;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/stock/stock-checking", data);

              case 9:
                response = _context9.sent;
                _context9.next = 12;
                return response;

              case 12:
                result = _context9.sent;

                if (result['data']['success'] === false) {
                  alert('Confirm Qty cant be greater than Purchased Qty');
                  _this11.loading = false;
                } else {
                  console.log(result);
                  window.location.href = "/stock/stock-checking/index";
                }

                _context9.next = 20;
                break;

              case 16:
                _context9.prev = 16;
                _context9.t0 = _context9["catch"](1);
                _this11.loading = false;

                if (_context9.t0.response.status === 422) {
                  _this11.errors = _context9.t0.response.data.errors;
                }

              case 20:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, null, [[1, 16]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['po', 'po-id'],
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__["default"],
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default.a
  },
  data: function data() {
    return {
      products: [],
      form: {
        paymentTerm: "",
        address: "",
        description: ""
      },
      salesmans: [],
      customers: [],
      suppliers: [],
      warehouses: [],
      departments: [],
      purchaseOrders: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedSalesman: null,
      selectedSupplier: null,
      selectedDepartment: null,
      selectedPurchaseOrder: null,
      selectedItem: [],
      selectedWarehouses: [],
      validationErrors: "",
      show_start: false,
      fetchLoading: false,
      showForm: this.poId ? true : false,
      customerDetail: "",
      show_customer_detail: false,
      supplierDetail: "",
      show_supplier_detail: false
    };
  },
  mounted: function mounted() {
    this.fetchDepartments();
    this.fetchSalesmens();
    this.fetchCustomers();
    this.fetchPurchaseOrders();
    this.fetchItems();
    this.fetchWarehouses();
    this.fetchUoms();
    this.date();
  },
  computed: {},
  watch: {
    selectedDepartment: {
      handler: function handler() {
        this.form.department = this.selectedDepartment.id;
      }
    },
    selectedSalesman: {
      handler: function handler() {
        this.form.salesman = this.selectedSalesman.id;
      }
    },
    selectedCustomer: {
      handler: function handler() {
        this.form.customer = this.selectedCustomer.id;
      }
    },
    selectedPurchaseOrder: {
      handler: function handler() {
        this.form.purchaseOrder = this.selectedPurchaseOrder.id;
      }
    },
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          _this.products[i].item = item.id;
        });
      }
    },
    selectedWarehouses: {
      handler: function handler() {
        var _this2 = this;

        var selectedWarehouses = this.selectedWarehouses;
        selectedWarehouses.map(function (warehouse, i) {
          _this2.products[i].warehouse = warehouse.id;
        });
      }
    }
  },
  methods: {
    date: function date() {
      var date = new Date();
      this.form.date = moment(date).format('Y-MM-D');
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    callFromChildCustomer: function callFromChildCustomer() {
      var _this3 = this;

      this.$root.$on('refetch_the_customer_from_qm', function () {
        _this3.rerenderCustomers();
      });
    },
    showCustomer: function showCustomer(e) {
      console.log(e);
      this.$modal.show('customer-modal');
    },
    fetchSupplierDetail: function fetchSupplierDetail(e) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var responses, results, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _this4.supplierDetail = "";
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/detail-purchase-order?purchase_order_id=".concat(e.id));

              case 4:
                responses = _context.sent;
                _context.next = 7;
                return responses.data;

              case 7:
                results = _context.sent;
                _context.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase-order/supplier-detail?supplier_id=".concat(results.supplier_id));

              case 10:
                response = _context.sent;
                _context.next = 13;
                return response.data;

              case 13:
                result = _context.sent;
                _this4.supplierDetail = result;
                _context.next = 20;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 20:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 17]]);
      }))();
    },
    fetchPurchaseOrders: function fetchPurchaseOrders() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase-order/get-po");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this5.purchaseOrders.push({
                    id: res.id,
                    name: res.code_number
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchDepartments: function fetchDepartments() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/department");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this6.departments.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    fetchCustomers: function fetchCustomers() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this7.customers.push({
                    id: res.id,
                    name: res.company_name + " - " + res.pic_1 + " - " + res.pic_phone_1
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchCustomersDetail: function fetchCustomersDetail(e) {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var responses, results, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _this8.customerDetail = "";
                _context5.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(e.id));

              case 4:
                responses = _context5.sent;
                _context5.next = 7;
                return responses.data;

              case 7:
                results = _context5.sent;
                _context5.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(results.customer_id));

              case 10:
                response = _context5.sent;
                _context5.next = 13;
                return response.data;

              case 13:
                result = _context5.sent;
                _this8.customerDetail = result;
                _context5.next = 20;
                break;

              case 17:
                _context5.prev = 17;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 20:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 17]]);
      }))();
    },
    rerenderCustomers: function rerenderCustomers() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(_this9.selectedCustomer.priceCategory));

              case 3:
                response = _context6.sent;
                _context6.next = 6;
                return response.data;

              case 6:
                result = _context6.sent;
                result.map(function (res) {
                  _this9.customers.push({
                    id: res.id,
                    name: res.company_name + " - " + res.pic_1 + " - " + res.pic_phone_1,
                    priceCategory: res.price_category_id
                  });
                });
                _context6.next = 13;
                break;

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 10]]);
      }))();
    },
    childMessageReceivedCustomer: function childMessageReceivedCustomer(value) {
      this.customers = [];
      this.fetchCustomers();
    },
    fetchSalesmens: function fetchSalesmens() {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/salesman");

              case 3:
                response = _context7.sent;
                _context7.next = 6;
                return response.data;

              case 6:
                result = _context7.sent;
                result.map(function (res) {
                  _this10.salesmans.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context7.next = 13;
                break;

              case 10:
                _context7.prev = 10;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 13:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 10]]);
      }))();
    },
    fetchPurchaseOrderDetail: function fetchPurchaseOrderDetail(event) {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var po_id, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _this11.products = [];
                _this11.selectedItem = [];
                _this11.selectedWarehouses = [];
                _this11.selectedSupplier = [];
                _this11.form.department = "";
                _this11.form.customer = "";
                _this11.customerDetail = "";
                po_id = event.id;
                _context8.next = 11;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/detail-purchase-order?purchase_order_id=".concat(po_id));

              case 11:
                response = _context8.sent;
                _context8.next = 14;
                return response.data;

              case 14:
                result = _context8.sent;
                result.items.map(function (item, i) {
                  console.log(item);

                  if (item.qty > 0) {
                    _this11.selectedItem.push({
                      id: item.product.id,
                      name: item.product.name
                    });

                    if (item.warehouse) {
                      _this11.selectedWarehouses.push({
                        id: item.warehouse_id,
                        name: item.warehouse.name
                      });
                    }

                    _this11.products.push({
                      purchase_order_id: item.purchase_order_id,
                      purchase_order_item_id: item.id,
                      purchaseQty: item.qty,
                      uom: item.uom.name,
                      confirmQty: "",
                      description: item.description
                    });
                  }
                });
                _this11.showForm = true;
                _this11.form.title = result.title;

                _this11.selectedSupplier.push({
                  id: result.supplier_id,
                  name: result.supplier.company_name
                });

                _this11.form.purchaseOrder = result.id;
                _this11.showForm = true;
                _context8.next = 26;
                break;

              case 23:
                _context8.prev = 23;
                _context8.t0 = _context8["catch"](0);
                console.log(_context8.t0);

              case 26:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 23]]);
      }))();
    },
    fetchSalesOrderMaterialDetailSO: function fetchSalesOrderMaterialDetailSO() {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var so_id, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.prev = 0;
                _this12.products = [];
                _this12.selectedItem = [];
                _this12.selectedWarehouses = [];
                _this12.form.department = "";
                _this12.form.customer = "";
                _this12.customerDetail = "";
                so_id = _this12.soId;

                if (!so_id) {
                  _context9.next = 21;
                  break;
                }

                _context9.next = 11;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/sales_order_material_item?sales_order_id=".concat(so_id));

              case 11:
                response = _context9.sent;
                _context9.next = 14;
                return response.data;

              case 14:
                result = _context9.sent;
                result.items.map(function (item, i) {
                  if (item.delivered_qty > 0) {
                    _this12.selectedItem.push({
                      id: item.product.id,
                      name: item.product.name
                    });

                    _this12.products.push({
                      purchase_order_id: item.purchase_order_id,
                      purchase_order_item_id: item.id,
                      purchaseQty: item.qty,
                      uom: item.uom.name,
                      confirmQty: "",
                      description: item.description
                    });
                  }
                });
                _this12.form.title = result.title;
                _this12.form.description = result.description;
                _this12.form.purchaseOrder = result.id;
                _this12.selectedPurchaseOrder = {
                  id: result.id,
                  name: result.code_number
                };
                _this12.showForm = true;

              case 21:
                _context9.next = 26;
                break;

              case 23:
                _context9.prev = 23;
                _context9.t0 = _context9["catch"](0);
                console.log(_context9.t0);

              case 26:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, null, [[0, 23]]);
      }))();
    },
    fetchItems: function fetchItems() {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                _context10.prev = 0;
                _context10.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/product");

              case 3:
                response = _context10.sent;
                _context10.next = 6;
                return response.data;

              case 6:
                result = _context10.sent;
                result.map(function (res) {
                  _this13.items.push({
                    id: res.id,
                    name: res.code + " - " + res.name
                  });
                });
                _context10.next = 13;
                break;

              case 10:
                _context10.prev = 10;
                _context10.t0 = _context10["catch"](0);
                console.log(_context10.t0);

              case 13:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, null, [[0, 10]]);
      }))();
    },
    fetchWarehouses: function fetchWarehouses() {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.prev = 0;
                _context11.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/warehouse");

              case 3:
                response = _context11.sent;
                _context11.next = 6;
                return response.data;

              case 6:
                result = _context11.sent;
                result.map(function (res) {
                  _this14.warehouses.push({
                    id: res.id,
                    name: res.code + " - " + res.pic
                  });
                });
                _context11.next = 13;
                break;

              case 10:
                _context11.prev = 10;
                _context11.t0 = _context11["catch"](0);
                console.log(_context11.t0);

              case 13:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, null, [[0, 10]]);
      }))();
    },
    fetchItemDetails: function fetchItemDetails(e, i) {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                id = e.id;
                axios__WEBPACK_IMPORTED_MODULE_1___default()("/transaction/quotation_material/product/".concat(id)).then(function (response) {
                  var price = response.data.price;
                  var description = response.data.description;
                  _this15.products[i].description = description, _this15.products[i].price = price;
                })["catch"](function (e) {
                  return console.log(e);
                });

              case 2:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this16 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _context13.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context13.sent;
                _context13.next = 5;
                return response.data;

              case 5:
                result = _context13.sent;
                _this16.uoms = result;

              case 7:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13);
      }))();
    },
    deleteItem: function deleteItem(i) {
      this.products.splice(i, 1);
    },
    handleSubmit: function handleSubmit() {
      var _this17 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _this17.loading = true;
                console.log(_this17.products);
                _context14.prev = 2;
                data = {};
                data["date"] = _this17.form.date;
                data["description"] = _this17.form.description;
                data["products"] = _this17.products;

                if (!(_this17.products == 0)) {
                  _context14.next = 12;
                  break;
                }

                alert("Product cannot be empty");
                _this17.loading = false;
                _context14.next = 21;
                break;

              case 12:
                console.log(data);
                _context14.next = 15;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/stock/stock-in", data);

              case 15:
                response = _context14.sent;
                _context14.next = 18;
                return response;

              case 18:
                result = _context14.sent;
                console.log(result);

                if (result['data']['success'] === false) {
                  alert('Confirm Qty cant be greater than Purchased Qty');
                  _this17.loading = false;
                } else if (result['data']['success'] === true) {
                  window.location.href = result['data']['redirect'];
                } else if (result['data']['success'] = 'notselectwarehouse') {
                  alert('Please select warehouse first');
                  _this17.loading = false;
                } else {
                  alert('Oops , please try again');
                }

              case 21:
                _context14.next = 27;
                break;

              case 23:
                _context14.prev = 23;
                _context14.t0 = _context14["catch"](2);
                _this17.loading = false;

                if (_context14.t0.response.status === 422) {
                  console.log(_context14.t0.response.status);
                  _this17.validationErrors = _context14.t0.response.data.errors;
                } else {
                  console.log(_context14.t0);
                  alert("Please check the input", _context14.t0);
                }

              case 27:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14, null, [[2, 23]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["id", "resubmit"],
  components: {
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default.a,
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      products: [],
      form: {
        title: "",
        department: "",
        customer: "",
        salesman: "",
        description: "",
        address: "",
        resubmit: ""
      },
      show_start: false,
      show_end: false,
      customers: [],
      salesmans: [],
      suppliers: [],
      warehouses: [],
      departments: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedCustomer: null,
      selectedSupplier: null,
      selectedDepartment: null,
      selectedSalesman: null,
      selectedWarehouses: [],
      selectedItem: [],
      validationErrors: "",
      fetchLoading: false,
      customerDetail: "",
      show_customer_detail: false,
      supplierDetail: "",
      show_supplier_detail: false
    };
  },
  mounted: function mounted() {
    this.fetchSupplierDetail();
    this.fetchWarehouses();
    this.fetchItems();
    this.fetchUoms();
    this.fetchPurchaseOrderDetail();
  },
  watch: {
    selectedDepartment: {
      handler: function handler() {
        this.form.department = this.selectedDepartment.id;
      }
    },
    selectedSalesman: {
      handler: function handler() {
        this.form.salesman = this.selectedSalesman.id;
      }
    }
  },
  methods: {
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    fetchSupplierDetail: function fetchSupplierDetail() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var responses, results, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _this.supplierDetail = "";
                _context.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/detail-purchase-order?purchase_order_id=".concat(_this.id));

              case 4:
                responses = _context.sent;
                _context.next = 7;
                return responses.data;

              case 7:
                results = _context.sent;
                _context.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase-order/supplier-detail?supplier_id=".concat(results.supplier_id));

              case 10:
                response = _context.sent;
                _context.next = 13;
                return response.data;

              case 13:
                result = _context.sent;
                _this.supplierDetail = result;
                _context.next = 20;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 20:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 17]]);
      }))();
    },
    fetchPurchaseOrderDetail: function fetchPurchaseOrderDetail() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var po_id, response, result, responses, results, supplier, resultSupplier;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _this2.products = [];
                _this2.selectedItem = [];
                _this2.selectedWarehouses = [];
                _this2.selectedSupplier = [];
                _this2.form.department = "";
                _this2.form.customer = "";
                _this2.customerDetail = "";
                po_id = _this2.id;
                _context2.next = 11;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/detail-purchase-order?purchase_order_id=".concat(po_id));

              case 11:
                response = _context2.sent;
                _context2.next = 14;
                return response.data;

              case 14:
                result = _context2.sent;
                result.items.map(function (item, i) {
                  console.log(item);

                  if (item.qty > 0) {
                    _this2.selectedItem.push({
                      id: item.product.id,
                      name: item.product.name
                    });

                    _this2.products.push({
                      purchase_order_id: item.purchase_order_id,
                      purchase_order_item_id: item.id,
                      purchaseQty: item.qty,
                      uom: item.uom_id,
                      confirmQty: item.stock_qty,
                      description: item.description,
                      warehouse: item.warehouse_id
                    });
                  }
                });
                _this2.showForm = true;
                _this2.form.title = result.title;
                _this2.form.purchase_order = result.code_number;
                _this2.form.date = result.date;

                _this2.selectedSupplier.push({
                  id: result.supplier_id,
                  name: result.supplier.company_name
                });

                _this2.supplierDetail = "";
                _context2.next = 24;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/detail-purchase-order?purchase_order_id=".concat(po_id));

              case 24:
                responses = _context2.sent;
                _context2.next = 27;
                return responses.data;

              case 27:
                results = _context2.sent;
                _context2.next = 30;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase-order/supplier-detail?supplier_id=".concat(results.supplier_id));

              case 30:
                supplier = _context2.sent;
                _context2.next = 33;
                return supplier.data;

              case 33:
                resultSupplier = _context2.sent;
                _this2.supplierDetail = resultSupplier;
                _this2.showForm = true;
                _context2.next = 41;
                break;

              case 38:
                _context2.prev = 38;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 41:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 38]]);
      }))();
    },
    fetchWarehouses: function fetchWarehouses() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/delivery_order_material/warehouse");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this3.warehouses.push({
                    id: res.id,
                    name: res.code + " - " + res.pic
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    fetchItems: function fetchItems() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/product");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this4.items.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context5.sent;
                _context5.next = 5;
                return response.data;

              case 5:
                result = _context5.sent;
                _this5.uoms = result;

              case 7:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    removeItem: function removeItem(itemId) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/transaction/delivery_order_material/removeItem/".concat(itemId));

              case 2:
                response = _context6.sent;
                _context6.next = 5;
                return response.data;

              case 5:
                result = _context6.sent;

              case 6:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    deleteItem: function deleteItem(i, itemId) {
      var _this6 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You can't revert your action",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes Delete it!",
        cancelButtonText: "No, Keep it!",
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          console.log(itemId);

          if (typeof itemId !== "undefined") {
            _this6.removeItem(itemId);
          }

          _this6.products.splice(i, 1);

          _this6.$swal("Deleted", "You successfully deleted this data", "success");
        } else {
          _this6.$swal("Cancelled", "Your file is still intact", "info");
        }
      });
    },
    handleSubmit: function handleSubmit() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _this7.loading = true;
                _context7.prev = 1;
                data = {}; // data["title"] = this.form.title;

                data["_method"] = "PUT";
                data["date"] = _this7.form.date;
                data["description"] = _this7.form.description;
                data["products"] = _this7.products;
                _context7.next = 9;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/stock/stock-in-update/".concat(_this7.id), data);

              case 9:
                response = _context7.sent;
                _context7.next = 12;
                return response;

              case 12:
                result = _context7.sent;
                console.log(response);

                if (result['data']['success'] === false) {
                  alert('Confirm Qty cant be greater than Purchased Qty');
                  _this7.loading = false;
                } else if (result['data']['success'] === true) {
                  window.location.href = result['data']['redirect'];
                } else if (result['data']['success'] = 'notselectwarehouse') {
                  alert('Please select warehouse first');
                  _this7.loading = false;
                } else {
                  alert('Oops , please try again');
                }

                _context7.next = 21;
                break;

              case 17:
                _context7.prev = 17;
                _context7.t0 = _context7["catch"](1);
                _this7.loading = false;

                if (_context7.t0.response.status === 422) {
                  _this7.errors = _context7.t0.response.data.errors;
                }

              case 21:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[1, 17]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__["default"],
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default.a
  },
  data: function data() {
    var _ref;

    return _ref = {
      products: [],
      form: {
        code_number: ""
      },
      items: [],
      errors: [],
      loading: false,
      selectedItem: [],
      validationErrors: "",
      selectedOrigin: null,
      selectedDestination: null,
      selectedProduct: null,
      origins: [],
      destinations: [],
      uoms: []
    }, _defineProperty(_ref, "products", []), _defineProperty(_ref, "show_start", false), _ref;
  },
  mounted: function mounted() {
    this.fetchWarehouse();
    this.fetchUoms(); // this.fetchCodeNumber();

    var date = new Date();
    this.form.date = moment(date).format('Y-MM-D');
  },
  computed: {
    getTotal: function getTotal() {
      var total = this.products.reduce(function (a, b) {
        return a + b.price * b.qty - (b.discountAmount ? b.discountAmount : 0) - (b.discountRate ? b.discountRate / 100 * b.price : 0) * b.qty;
      }, 0);
      this.form.getTotal = total ? total : 0;
      return total;
    },
    subTotal: function subTotal() {
      var total = this.form.getTotal - this.form.discountAllItem;
      this.form.subTotal = total ? total : 0;
      return total;
    },
    getGst: function getGst() {
      var total = this.form.tax / 100 * this.form.subTotal;
      this.form.gstTotal = total ? total : 0;
      return total;
    },
    total: function total() {
      var total = this.form.subTotal + this.form.gstTotal;
      this.form.total = total ? total : 0;
      return total;
    },
    groupBy: function (_groupBy) {
      function groupBy() {
        return _groupBy.apply(this, arguments);
      }

      groupBy.toString = function () {
        return _groupBy.toString();
      };

      return groupBy;
    }(function () {
      return groupBy(this.products, 'product_id', 'warehouse_id', 'product.name');
    })
  },
  watch: {
    // item for product
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          _this.products[i].item = item.id;
        });
      }
    },
    selectedOrigin: {
      handler: function handler() {
        this.form.origin_id = this.selectedOrigin.id;
      }
    },
    selectedDestination: {
      handler: function handler() {
        this.form.destination_id = this.selectedDestination.id;
      }
    }
  },
  methods: {
    showProduct: function showProduct() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _this2.selectedItem = [];

                if (!(_this2.selectedOrigin == null)) {
                  _context.next = 6;
                  break;
                }

                _this2.selectedItem = [];
                _context.next = 15;
                break;

              case 6:
                _this2.products = [];
                _context.next = 9;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/stock/get-stock/".concat(_this2.selectedOrigin.id));

              case 9:
                response = _context.sent;
                _context.next = 12;
                return response.data;

              case 12:
                result = _context.sent;
                _this2.items = [];
                result.map(function (item, i) {
                  _this2.items.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this2.selectedItem.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this2.products.push({
                    item: item.product.name,
                    qty: item.qty,
                    uom: item.product.uom_id,
                    remark: '',
                    description: item.product.description,
                    price: "",
                    discountRate: "",
                    discountAmount: "",
                    subTotal: ""
                  });

                  console.log(_this2.products);
                });

              case 15:
                _context.next = 20;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 20:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 17]]);
      }))();
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
      } else {
        return true;
      }
    },
    fetchWarehouse: function fetchWarehouse() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/warehouse");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this3.origins.push({
                    id: res.id,
                    title: res.code + ' - ' + res.name
                  });

                  _this3.destinations.push({
                    id: res.id,
                    title: res.code + ' - ' + res.name
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context3.sent;
                _context3.next = 5;
                return response.data;

              case 5:
                result = _context3.sent;
                _this4.uoms = result;

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    deleteItem: function deleteItem(i) {
      this.products.splice(i, 1);
    },
    handleSubmit: function handleSubmit() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var data, response, results;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _this5.loading = true;
                console.log('121');
                _context4.prev = 2;

                if (!(_this5.form.origin_id == _this5.form.destination_id)) {
                  _context4.next = 8;
                  break;
                }

                alert('Destination must be different');
                _this5.loading = false;
                _context4.next = 22;
                break;

              case 8:
                data = {};
                data["date"] = _this5.form.date;
                data["products"] = _this5.products;
                data["description"] = _this5.form.description;
                data["origin_id"] = _this5.form.origin_id;
                data["destination_id"] = _this5.form.destination_id;
                _context4.next = 16;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/stock/stock-transfer", data);

              case 16:
                response = _context4.sent;
                _context4.next = 19;
                return response;

              case 19:
                results = _context4.sent;
                console.log(results);

                if (results['data']['success'] === false) {
                  alert('Qty cant be greater than In Hand Qty');
                  _this5.loading = false;
                } else {
                  window.location.href = "/stock/stock-transfer";
                }

              case 22:
                _context4.next = 29;
                break;

              case 24:
                _context4.prev = 24;
                _context4.t0 = _context4["catch"](2);
                _this5.loading = false;
                console.log(_context4.t0);

                if (_context4.t0.response.status === 422) {
                  console.log(_context4.t0.response.status);
                  _this5.validationErrors = _context4.t0.response.data.errors;
                  console.log(_this5.validationErrors);
                } else {
                  alert("Please check the input");
                }

              case 29:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[2, 24]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["id"],
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_5___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__["default"],
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default.a
  },
  data: function data() {
    var _ref;

    return _ref = {
      products: [],
      form: {
        code_number: "",
        date: null,
        description: null
      },
      items: [],
      errors: [],
      loading: false,
      selectedItem: [],
      validationErrors: "",
      selectedOrigin: null,
      selectedDestination: null,
      selectedProduct: null,
      origins: [],
      destinations: [],
      uoms: []
    }, _defineProperty(_ref, "products", []), _defineProperty(_ref, "show_start", false), _ref;
  },
  mounted: function mounted() {
    this.fetchWarehouse();
    this.fetchUoms();
    this.getDetailTransfer();
  },
  computed: {
    groupBy: function (_groupBy) {
      function groupBy() {
        return _groupBy.apply(this, arguments);
      }

      groupBy.toString = function () {
        return _groupBy.toString();
      };

      return groupBy;
    }(function () {
      return groupBy(this.products, 'product_id', 'warehouse_id', 'product.name');
    })
  },
  watch: {
    // item for product
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          _this.products[i].item = item.id;
        });
      }
    },
    selectedOrigin: {
      handler: function handler() {
        this.form.origin_id = this.selectedOrigin.id;
      }
    },
    selectedDestination: {
      handler: function handler() {
        this.form.destination_id = this.selectedDestination.id;
      }
    }
  },
  methods: {
    showProduct: function showProduct() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _this2.selectedItem = [];

                if (!(_this2.selectedOrigin == null)) {
                  _context.next = 6;
                  break;
                }

                _this2.selectedItem = [];
                _context.next = 15;
                break;

              case 6:
                _this2.products = [];
                _context.next = 9;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/stock/get-stock/".concat(_this2.selectedOrigin.id));

              case 9:
                response = _context.sent;
                _context.next = 12;
                return response.data;

              case 12:
                result = _context.sent;
                _this2.items = [];
                result.map(function (item, i) {
                  _this2.items.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this2.selectedItem.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this2.products.push({
                    item: item.product.name,
                    qty: item.qty,
                    uom: item.product.uom_id,
                    remark: '',
                    description: item.product.description,
                    price: "",
                    discountRate: "",
                    discountAmount: "",
                    subTotal: ""
                  });
                });

              case 15:
                _context.next = 20;
                break;

              case 17:
                _context.prev = 17;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 20:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 17]]);
      }))();
    },
    getDetailTransfer: function getDetailTransfer() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _this3.selectedItem = [];
                _this3.products = [];
                _context2.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/stock/get-stock-detail/".concat(_this3.id));

              case 5:
                response = _context2.sent;
                _context2.next = 8;
                return response.data;

              case 8:
                result = _context2.sent;
                _this3.items = [];
                _this3.form.date = result[0].date;
                _this3.form.description = result[0].description;
                _this3.selectedOrigin = result[0].origin;
                _this3.selectedDestination = result[0].destination; // console.log(result[0].items)

                result[0].items.map(function (item, i) {
                  _this3.items.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this3.selectedItem.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this3.products.push({
                    item: item.product.name,
                    id: item.id,
                    product_id: item.product_id,
                    qty: item.in_hand,
                    uom: item.product.uom_id,
                    remark: '',
                    description: item.product.description,
                    price: "",
                    quantity: item.qty,
                    discountRate: "",
                    discountAmount: "",
                    subTotal: "",
                    stock_transfer_id: item.stock_transfer_id
                  });
                });
                _context2.next = 20;
                break;

              case 17:
                _context2.prev = 17;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 20:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 17]]);
      }))();
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
      } else {
        return true;
      }
    },
    fetchWarehouse: function fetchWarehouse() {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(" /api/warehouse");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                result.map(function (res) {
                  _this4.origins.push({
                    id: res.id,
                    title: res.code + ' - ' + res.name
                  });

                  _this4.destinations.push({
                    id: res.id,
                    title: res.code + ' - ' + res.name
                  });
                });
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 13:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 10]]);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context4.sent;
                _context4.next = 5;
                return response.data;

              case 5:
                result = _context4.sent;
                _this5.uoms = result;

              case 7:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    deleteItem: function deleteItem(i) {
      this.products.splice(i, 1);
    },
    handleSubmit: function handleSubmit() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var data, response, results;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _this6.loading = true;
                _context5.prev = 1;
                data = {};
                data["_method"] = "PUT";
                data["date"] = _this6.form.date;
                data["products"] = _this6.products;
                data["description"] = _this6.form.description;
                data["origin_id"] = _this6.form.origin_id;
                data["destination_id"] = _this6.form.destination_id;
                _context5.next = 11;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/stock/stock-transfer/".concat(_this6.id), data);

              case 11:
                response = _context5.sent;
                _context5.next = 14;
                return response;

              case 14:
                results = _context5.sent;

                if (results['data']['success'] === false) {
                  alert('Qty cant be greater than In Hand Qty');
                  _this6.loading = false;
                } else {
                  window.location.href = "/stock/stock-transfer";
                }

                console.log(results);
                console.log(data);
                _context5.next = 25;
                break;

              case 20:
                _context5.prev = 20;
                _context5.t0 = _context5["catch"](1);
                _this6.loading = false;
                console.log(_context5.t0);

                if (_context5.t0.response.status === 422) {
                  console.log(_context5.t0.response.status);
                  _this6.validationErrors = _context5.t0.response.data.errors;
                  console.log(_this6.validationErrors);
                } else {
                  alert("Please check the input");
                }

              case 25:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[1, 20]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var _Product__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Product */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue");
/* harmony import */ var _Customer_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Customer.vue */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue");
/* harmony import */ var _PriceHistory_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./PriceHistory.vue */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_8__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['qm', 'qm-id', 'departmentId', 'departmentName', 'currencyId', 'currencyName'],
  components: {
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_8___default.a,
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_3__["default"],
    Product: _Product__WEBPACK_IMPORTED_MODULE_4__["default"],
    Customer: _Customer_vue__WEBPACK_IMPORTED_MODULE_5__["default"],
    PriceHistory: _PriceHistory_vue__WEBPACK_IMPORTED_MODULE_6__["default"]
  },
  data: function data() {
    var _ref;

    return _ref = {
      products: [],
      form: {
        type: "",
        discountAllItem: null,
        getTotal: null,
        subTotal: null,
        currency: "",
        paymentTerm: "",
        quotationMaterial: "",
        tax: ""
      },
      salesmans: [],
      paymentTerms: [],
      customers: [],
      departments: [],
      currencies: [],
      quotationMaterials: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      selectedSalesman: null,
      selectedPaymentTerm: null,
      selectedCurrency: null,
      selectedCustomer: null,
      selectedDepartment: null,
      selectedQuotationMaterial: null,
      selectedItem: [],
      validationErrors: "",
      customerDetail: "",
      show_customer_detail: false,
      customer_addrees: null,
      price_history: null,
      billing: [],
      procurement_history: []
    }, _defineProperty(_ref, "customer_addrees", {
      length: null
    }), _defineProperty(_ref, "product_id", null), _defineProperty(_ref, "billing", []), _defineProperty(_ref, "pr_history", []), _ref;
  },
  mounted: function mounted() {
    this.fetchDepartments();
    this.fetchSalesmens();
    this.fetchCustomers();
    this.fetchPaymentTerms();
    this.fetchSalesmenFirst();
    this.fetchTaxs();
    this.fetchCurencies();
    this.fetchUoms();
  },
  computed: {
    getTotal: function getTotal() {
      var total = this.products.reduce(function (a, b) {
        return a + b.price * b.qty - (b.discountAmount ? b.discountAmount : 0) - (b.discountRate ? b.discountRate / 100 * b.price : 0) * b.qty;
      }, 0);
      this.form.getTotal = total ? parseInt(total) : 0;
      return total;
    },
    subTotal: function subTotal() {
      var total = this.form.getTotal - this.form.discountAllItem;
      this.form.subTotal = total ? parseInt(total) : 0;
      return total;
    },
    getGst: function getGst() {
      var total = this.form.tax / 100 * this.form.subTotal;
      this.form.gstTotal = total ? parseInt(total) : 0;
      return total;
    },
    total: function total() {
      var total = this.form.subTotal + this.form.gstTotal;
      this.form.total = total ? parseInt(total) : 0;
      return total;
    }
  },
  watch: {
    selectedDepartment: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        this.form.department = this.selectedDepartment.id;
      }
    },
    selectedSalesman: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedSalesman) {
          this.form.salesman = this.selectedSalesman.id;
        } else {
          this.form.salesman = null;
        }
      }
    },
    selectedPaymentTerm: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedPaymentTerm) {
          this.form.paymentTerm = this.selectedPaymentTerm.id;
        } else {
          this.form.paymentTerm = null;
        }
      }
    },
    selectedCustomer: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedCustomer) {
          this.form.customer = this.selectedCustomer.id;
          this.form.priceCategory = this.selectedCustomer.priceCategory;
        } else {
          this.form.customer = null;
          this.form.priceCategory = null;
        }
      }
    },
    selectedCurrency: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedCurrency) {
          this.form.currency = this.selectedCurrency.id;
        } else {
          this.form.currency = null;
        }
      }
    },
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          if (item) {
            _this.products[i].item = item.id;
          } else {
            _this.products[i].item = null;
          }
        });
      }
    }
  },
  methods: {
    callFromChild: function callFromChild() {
      var _this2 = this;

      this.$root.$on('refetch_the_product_from_qm', function () {
        _this2.rerenderItems();
      });
    },
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    childMessageReceivedCustomer: function childMessageReceivedCustomer(value) {
      this.customers = [];
      this.fetchCustomers();
    },
    callFromChildCustomer: function callFromChildCustomer() {
      var _this3 = this;

      this.$root.$on('refetch_the_customer_from_qm', function () {
        _this3.rerenderCustomers();
      });
    },
    childMessageReceived: function childMessageReceived(value) {
      this.items = [];
      this.fetchItemAfterCreateProduct();
    },
    show: function show(e) {
      this.$modal.show("example-modal");
    },
    showCustomer: function showCustomer(e) {
      this.$modal.show('customer-modal');
    },
    showPriceHistory: function showPriceHistory(e, data, index) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result, procurementHistory, ph;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this4.product_id = data.item;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/billing-history/".concat(data.item));

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                _this4.billing = result;
                _context.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/procurement-history/".concat(data.item));

              case 10:
                procurementHistory = _context.sent;
                _context.next = 13;
                return procurementHistory.data;

              case 13:
                ph = _context.sent;
                _this4.pr_history = ph;

                _this4.$modal.show('price-history-modal');

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    fetchDepartments: function fetchDepartments() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/department");

              case 3:
                response = _context2.sent;
                _context2.next = 6;
                return response.data;

              case 6:
                result = _context2.sent;
                result.map(function (res) {
                  _this5.departments.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 13:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 10]]);
      }))();
    },
    fetchCustomers: function fetchCustomers() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var response, result, id_customers, price_category, responses, results;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer");

              case 3:
                response = _context3.sent;
                _context3.next = 6;
                return response.data;

              case 6:
                result = _context3.sent;
                id_customers = null;
                price_category = null;
                result.map(function (res) {
                  if (res.company_name == 'On The Spot') {
                    _this6.selectedCustomer = {
                      id: res.id,
                      name: res.company_name,
                      priceCategory: res.price_category_id
                    };
                    id_customers = res.id;
                    price_category = res.price_category_id;
                  }

                  _this6.customers.push({
                    id: res.id,
                    name: res.company_name,
                    priceCategory: res.price_category_id
                  });
                });
                _this6.customerDetail = "";
                _this6.show_customer_detail = true;
                _context3.next = 14;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(id_customers));

              case 14:
                responses = _context3.sent;
                _context3.next = 17;
                return responses.data;

              case 17:
                results = _context3.sent;
                _this6.customerDetail = results;
                _this6.customer_addrees = results.address;

                _this6.fetchItems('', price_category);

                _context3.next = 26;
                break;

              case 23:
                _context3.prev = 23;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 26:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 23]]);
      }))();
    },
    fetchSalesmens: function fetchSalesmens() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/salesman");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this7.salesmans.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchSalesmenFirst: function fetchSalesmenFirst() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/salesmenId");

              case 3:
                response = _context5.sent;
                _context5.next = 6;
                return response.data;

              case 6:
                result = _context5.sent;
                _this8.selectedSalesman = {
                  id: result.id,
                  name: result.name
                };
                _context5.next = 13;
                break;

              case 10:
                _context5.prev = 10;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 13:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 10]]);
      }))();
    },
    fetchPaymentTerms: function fetchPaymentTerms() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/paymentTerm");

              case 3:
                response = _context6.sent;
                _context6.next = 6;
                return response.data;

              case 6:
                result = _context6.sent;
                result.map(function (res) {
                  if (res.name == 'Cash') {
                    _this9.selectedPaymentTerm = {
                      id: res.id,
                      name: res.code + " - " + res.name
                    };
                  }

                  _this9.paymentTerms.push({
                    id: res.id,
                    name: res.code + " - " + res.name
                  });
                });
                _context6.next = 13;
                break;

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 10]]);
      }))();
    },
    fetchCurencies: function fetchCurencies() {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/sales_order/currency");

              case 3:
                response = _context7.sent;
                _context7.next = 6;
                return response.data;

              case 6:
                result = _context7.sent;
                result.map(function (res) {
                  _this10.currencies.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context7.next = 13;
                break;

              case 10:
                _context7.prev = 10;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 13:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 10]]);
      }))();
    },
    fetchTaxs: function fetchTaxs() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _context8.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/tax");

              case 3:
                response = _context8.sent;
                _context8.next = 6;
                return response.data;

              case 6:
                result = _context8.sent;
                _this11.form.tax_id = result.id;
                _this11.form.tax = result.rate;
                _context8.next = 14;
                break;

              case 11:
                _context8.prev = 11;
                _context8.t0 = _context8["catch"](0);
                console.log(_context8.t0);

              case 14:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 11]]);
      }))();
    },
    fetchCustomersDetail: function fetchCustomersDetail(e) {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.prev = 0;
                _this12.customerDetail = "";
                _this12.show_customer_detail = true;
                _context9.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(e.id));

              case 5:
                response = _context9.sent;
                _context9.next = 8;
                return response.data;

              case 8:
                result = _context9.sent;
                _this12.customerDetail = result;
                _this12.customer_addrees = result.address;
                _context9.next = 16;
                break;

              case 13:
                _context9.prev = 13;
                _context9.t0 = _context9["catch"](0);
                console.log(_context9.t0);

              case 16:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, null, [[0, 13]]);
      }))();
    },
    fetchItems: function fetchItems(e, id) {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var response, result, _response, _result;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                _context10.prev = 0;
                _this13.items = [];
                _this13.selectedItem = [];
                _this13.products = [];

                _this13.addItem();

                if (!e.priceCategory) {
                  _context10.next = 13;
                  break;
                }

                _context10.next = 8;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(e.priceCategory));

              case 8:
                response = _context10.sent;
                _context10.next = 11;
                return response.data;

              case 11:
                result = _context10.sent;
                result.map(function (res) {
                  _this13.items.push({
                    id: res.product_id,
                    name: res.product.code + " - " + res.product.name,
                    productPrice: res.id
                  });
                });

              case 13:
                if (!id) {
                  _context10.next = 21;
                  break;
                }

                _context10.next = 16;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(id));

              case 16:
                _response = _context10.sent;
                _context10.next = 19;
                return _response.data;

              case 19:
                _result = _context10.sent;

                _result.map(function (res) {
                  _this13.items.push({
                    id: res.product_id,
                    name: res.product.code + " - " + res.product.name,
                    productPrice: res.id
                  });
                });

              case 21:
                _context10.next = 26;
                break;

              case 23:
                _context10.prev = 23;
                _context10.t0 = _context10["catch"](0);
                console.log(_context10.t0);

              case 26:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, null, [[0, 23]]);
      }))();
    },
    fetchItemAfterCreateProduct: function fetchItemAfterCreateProduct() {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.prev = 0;
                _context11.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(_this14.selectedCustomer.priceCategory));

              case 3:
                response = _context11.sent;
                _context11.next = 6;
                return response.data;

              case 6:
                result = _context11.sent;
                result.map(function (res) {
                  _this14.items.push({
                    id: res.product_id,
                    name: res.product.code + " - " + res.product.name,
                    productPrice: res.id
                  });
                });
                _context11.next = 13;
                break;

              case 10:
                _context11.prev = 10;
                _context11.t0 = _context11["catch"](0);
                console.log(_context11.t0);

              case 13:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11, null, [[0, 10]]);
      }))();
    },
    fetchItemDetails: function fetchItemDetails(e, i) {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                if (e) {
                  product_id = e.id;
                  _this15.product_id = e.id;
                  _this15.products[i].price = null;
                  _this15.products[i].discountRate = null;
                  _this15.products[i].discountAmount = null;
                  _this15.products[i].uom = null;
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/transaction/product/product-detail/".concat(product_id)).then(function (response) {
                    var uom = response.data.uom_id;
                    _this15.products[i].uom = uom;

                    if (uom) {
                      axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(uom)).then(function (response) {
                        if (response.data) {
                          var price = response.data.price;
                          var discountRate = response.data.discount;
                          var discountAmount = response.data.discount_amount;
                          _this15.products[i].price = parseInt(price, 10);
                          _this15.products[i].discountRate = discountRate;
                          _this15.products[i].discountAmount = parseInt(discountAmount, 10);
                        }
                      })["catch"](function (e) {
                        return console.log(e);
                      });
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 1:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    fetchUomDetails: function fetchUomDetails(e, i) {
      var _this16 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                product_id = _this16.products[i].item;
                _this16.products[i].uom = null;
                _this16.products[i].price = null;
                _this16.products[i].discountRate = null;
                _this16.products[i].discountAmount = null;

                if (e) {
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(e)).then(function (response) {
                    if (response.data) {
                      var price = response.data.price;
                      var discountRate = response.data.discount;
                      var discountAmount = response.data.discount_amount;
                      _this16.products[i].price = parseInt(price, 10);
                      _this16.products[i].discountRate = discountRate;
                      _this16.products[i].discountAmount = parseInt(discountAmount, 10);
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 6:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this17 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _context14.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context14.sent;
                _context14.next = 5;
                return response.data;

              case 5:
                result = _context14.sent;
                _this17.uoms = result;

              case 7:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14);
      }))();
    },
    addItem: function addItem() {
      this.products.push({
        item: null,
        qty: null,
        uom: null,
        remark: null,
        description: null,
        discountRate: null,
        discountAmount: null,
        price: null
      });
    },
    deleteItem: function deleteItem(i) {
      this.products.splice(i, 1);
    },
    handleSubmit: function handleSubmit() {
      var _this18 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee15() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                _this18.loading = true;
                _context15.prev = 1;
                data = {};
                data["_method"] = 'post';
                data["title"] = _this18.form.title;
                data["customer_id"] = _this18.form.customer;
                data["salesman_id"] = _this18.form.salesman;
                data["products"] = _this18.products;
                data["get_total"] = _this18.form.getTotal;
                data["discount_all_item"] = _this18.form.discountAllItem;
                data["sub_total"] = _this18.form.subTotal;
                data["tax_id"] = _this18.form.tax_id;
                data["total"] = _this18.form.total;
                data["description"] = _this18.form.description;
                data["payment_term_id"] = _this18.form.paymentTerm;
                _context15.next = 17;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/transaction/sales_order", data);

              case 17:
                response = _context15.sent;
                _context15.next = 20;
                return response;

              case 20:
                result = _context15.sent;

                if (result.data.success == true) {
                  window.location.href = result.data.redirect;
                }

                _context15.next = 28;
                break;

              case 24:
                _context15.prev = 24;
                _context15.t0 = _context15["catch"](1);
                _this18.loading = false;

                if (_context15.t0.response.status === 422) {
                  console.log(_context15.t0.response.status);
                  _this18.validationErrors = _context15.t0.response.data.errors;
                } else {
                  alert("Please check the input");
                }

              case 28:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15, null, [[1, 24]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-js-modal */ "./node_modules/vue-js-modal/dist/index.js");
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_js_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_7__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


Vue.use(vue_js_modal__WEBPACK_IMPORTED_MODULE_2___default.a);





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_3__["default"],
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_4___default.a,
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_7___default.a,
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_5__["default"]
  },
  data: function data() {
    return {
      form: {
        company_name: "",
        roc_no: "",
        address: "",
        email: "",
        phone: "",
        fax: "",
        pic_1: "",
        pic_phone_1: "",
        credit_limit: ""
      },
      resizable: false,
      adaptive: true,
      validationErrors: "",
      price_category: [],
      selectedPriceCategory: null
    };
  },
  mounted: function mounted() {
    this.fetchPriceCategory();
  },
  watch: {
    selectedPriceCategory: {
      handler: function handler() {
        if (this.selectedPriceCategory) {
          this.form.price_category = this.selectedPriceCategory.id;
        } else {
          this.form.price_category = null;
        }
      }
    }
  },
  methods: {
    fetchPriceCategory: function fetchPriceCategory() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/price_category");

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                result.map(function (res) {
                  _this.price_category.push({
                    id: res.id,
                    name: res.name
                  });
                });
                console.log(_this.price_category);
                _context.next = 13;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](0);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 11]]);
      }))();
    },
    onClickButton: function onClickButton() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var data, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.loading = true;
                _context2.prev = 1;
                data = {};
                data["company_name"] = _this2.form.company_name;
                data["roc_no"] = _this2.form.roc_no;
                data["address"] = _this2.form.address;
                data["email"] = _this2.form.email;
                data["phone"] = _this2.form.phone;
                data["fax"] = _this2.form.fax;
                data["pic_1"] = _this2.form.pic_1;
                data["pic_phone_1"] = _this2.form.pic_phone_1;
                data["credit_limit"] = _this2.form.credit_limit;
                data["price_category_id"] = _this2.form.price_category;
                _context2.next = 15;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/transaction/customer/store-customer", data);

              case 15:
                response = _context2.sent;
                _this2.form = {
                  company_name: "",
                  roc_no: "",
                  address: "",
                  email: "",
                  phone: "",
                  fax: "",
                  pic_1: "",
                  pic_phone_1: "",
                  credit_limit: "",
                  price_category: ""
                };
                _this2.selectedPriceCategory = null;
                _this2.validationErrors = "";

                _this2.$emit('messageFromChild');

                _this2.$root.$emit('refetch_the_customer_from_qm');

                _this2.$modal.hide('customer-modal');

                _this2.$swal("Yeay!", "Your entry has been added successfully", "success");

                _context2.next = 29;
                break;

              case 25:
                _context2.prev = 25;
                _context2.t0 = _context2["catch"](1);
                _this2.loading = false;

                if (_context2.t0.response.status === 422) {
                  _this2.validationErrors = _context2.t0.response.data.errors;
                }

              case 29:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[1, 25]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _Product__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Product */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_6__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["id", "resubmit"],
  components: {
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_3___default.a,
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_6___default.a,
    Product: _Product__WEBPACK_IMPORTED_MODULE_4__["default"],
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      products: [],
      form: {
        title: "",
        department: "",
        customer: "",
        salesman: "",
        paymentTerm: "",
        discountAllItem: "",
        currency: "",
        description: "",
        getTotal: "",
        resubmit: "",
        project_id: null
      },
      show_start: false,
      show_end: false,
      projects: [],
      customers: [],
      salesmans: [],
      currencies: [],
      paymentTerms: [],
      departments: [],
      items: [],
      uoms: [],
      errors: [],
      loading: false,
      // now: moment().format("YYYY-MM-DD"),
      selectedCurrency: null,
      selectedProject: null,
      selectedCustomer: null,
      selectedDepartment: null,
      selectedSalesman: null,
      selectedPaymentTerm: null,
      selectedItem: [],
      validationErrors: "",
      fetchLoading: false,
      customerDetail: "",
      product_id: null,
      billing: [],
      pr_history: []
    };
  },
  mounted: function mounted() {
    this.fetchSO();
    this.fetchDepartments();
    this.fetchCustomers();
    this.fetchSalesmens();
    this.fetchCurencies();
    this.fetchPaymentTerms();
    this.fetchUoms();
    this.fetchProjectCode();
  },
  computed: {
    getTotal: function getTotal() {
      var total = this.products.reduce(function (a, b) {
        return a + b.price * b.qty - (b.discountAmount ? b.discountAmount : 0) - (b.discountRate ? b.discountRate / 100 * b.price : 0) * b.qty;
      }, 0);
      this.form.getTotal = total ? parseInt(total) : 0;
      return total;
    },
    subTotal: function subTotal() {
      var total = this.form.getTotal - this.form.discountAllItem;
      this.form.subTotal = total ? parseInt(total) : 0;
      return total;
    },
    getGst: function getGst() {
      var subTotal = this.form.getTotal - this.form.discountAllItem;
      var total = this.form.taxRate / 100 * subTotal;
      this.form.gstTotal = total ? parseInt(total) : 0;
      return total;
    },
    total: function total() {
      var subTotal = this.form.getTotal - this.form.discountAllItem;
      var total = subTotal + this.form.gstTotal;
      this.form.total = total ? parseInt(total) : 0;
      return total;
    }
  },
  watch: {
    selectedDepartment: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        this.form.department = this.selectedDepartment.id;
      }
    },
    selectedCustomer: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedCustomer) {
          this.form.customer = this.selectedCustomer.id;
          this.form.priceCategory = this.selectedCustomer.priceCategory;
        } else {
          this.form.customer = null;
          this.form.priceCategory = null;
        }
      }
    },
    selectedSalesman: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedSalesman) {
          this.form.salesman = this.selectedSalesman.id;
        } else {
          this.form.salesman = null;
        }
      }
    },
    selectedCurrency: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedCurrency) {
          this.form.currency = this.selectedCurrency.id;
        } else {
          this.form.currency = null;
        }
      }
    },
    selectedPaymentTerm: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedPaymentTerm) {
          this.form.paymentTerm = this.selectedPaymentTerm.id;
        } else {
          this.form.paymentTerm = null;
        }
      }
    },
    selectedProject: {
      // append value to spesific you wanted too
      // immediate: true, // will fire when mounted
      // deep: true, // using when nested data
      handler: function handler() {
        if (this.selectedProject) {
          this.form.project_id = this.selectedProject.id;
        } else {
          this.form.project_id = null;
        }
      }
    },
    selectedItem: {
      handler: function handler() {
        var _this = this;

        var selectedItems = this.selectedItem;
        selectedItems.map(function (item, i) {
          if (item) {
            _this.products[i].item = item.id;
          } else {
            _this.products[i].item = null;
          }
        });
      }
    }
  },
  methods: {
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    },
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    childMessageReceived: function childMessageReceived(value) {
      this.items = [];
      this.fetchItemAfterCreateProduct();
    },
    show: function show(e) {
      this.$modal.show("example-modal");
    },
    showPriceHistory: function showPriceHistory(e, data, index) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result, procurementHistory, ph;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this2.product_id = data.item;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/billing-history/".concat(data.item));

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                _this2.billing = result;
                _context.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/procurement-history/".concat(data.item));

              case 10:
                procurementHistory = _context.sent;
                _context.next = 13;
                return procurementHistory.data;

              case 13:
                ph = _context.sent;
                _this2.pr_history = ph;

                _this2.$modal.show('price-history-modal');

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    fetchSO: function fetchSO() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response, result, responseProduct, resultProduct;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _this3.fetchLoading = true;
                _context2.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/sales_order/".concat(_this3.id));

              case 4:
                response = _context2.sent;
                _context2.next = 7;
                return response.data;

              case 7:
                result = _context2.sent;
                _context2.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(result.customer.price_category_id));

              case 10:
                responseProduct = _context2.sent;
                _context2.next = 13;
                return responseProduct.data;

              case 13:
                resultProduct = _context2.sent;
                resultProduct.map(function (res) {
                  _this3.items.push({
                    id: res.product_id,
                    name: res.product.code + " - " + res.product.name,
                    productPrice: res.id
                  });
                });

                _this3.fetchCustomersDetail('nana');

                _this3.form.title = result.title;
                _this3.form.description = result.description;
                _this3.form.department = result.department_id;
                _this3.form.customer = result.customer_id;
                _this3.form.salesman = result.salesman_id;
                _this3.form.paymentTerm = result.payment_term_id;
                _this3.form.currency = result.currency_id;
                _this3.form.quotationMaterial = result.quotation_material_id ? result.quotation_material.code_number : null;
                _this3.form.discountAllItem = result.discount_all_item;
                _this3.form.taxRate = result.tax.rate;
                _this3.form.tax_id = result.tax_id; // push to selected cost center

                _this3.selectedCustomer = {
                  id: result.customer_id,
                  name: result.customer.company_name,
                  priceCategory: result.customer.price_category_id
                };
                _this3.selectedSalesman = result.salesman;
                _this3.selectedProject = result.project;
                _this3.selectedPaymentTerm = {
                  id: result.payment_term.id,
                  name: result.payment_term.code + " - " + result.payment_term.name
                };
                _this3.selectedCurrency = {
                  id: result.currency_id,
                  name: result.currency.name
                };
                _this3.selectedDepartment = result.department;
                result.items.map(function (item, i) {
                  // push to selected item
                  _this3.selectedItem.push({
                    id: item.product.id,
                    name: item.product.name
                  });

                  _this3.products.push({
                    id: item.id,
                    item: item.product_id,
                    qty: item.qty,
                    uom: item.uom_id,
                    price: item.price,
                    discountRate: item.discount_rate,
                    discountAmount: item.discount_amount,
                    description: item.description
                  });
                });
                _this3.fetchLoading = false;
                _context2.next = 40;
                break;

              case 37:
                _context2.prev = 37;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 40:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 37]]);
      }))();
    },
    fetchCustomersDetail: function fetchCustomersDetail(event) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var so, resultSo, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _this4.customerDetail = "";
                _context3.next = 4;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/sales_order/".concat(_this4.id));

              case 4:
                so = _context3.sent;
                _context3.next = 7;
                return so.data;

              case 7:
                resultSo = _context3.sent;

                if (!event.id) {
                  _context3.next = 14;
                  break;
                }

                _context3.next = 11;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(event.id));

              case 11:
                response = _context3.sent;
                _context3.next = 17;
                break;

              case 14:
                _context3.next = 16;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer-detail/".concat(resultSo.customer_id));

              case 16:
                response = _context3.sent;

              case 17:
                _context3.next = 19;
                return response.data;

              case 19:
                result = _context3.sent;
                _this4.customerDetail = result;
                _context3.next = 26;
                break;

              case 23:
                _context3.prev = 23;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 26:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 23]]);
      }))();
    },
    fetchDepartments: function fetchDepartments() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/department");

              case 3:
                response = _context4.sent;
                _context4.next = 6;
                return response.data;

              case 6:
                result = _context4.sent;
                result.map(function (res) {
                  _this5.departments.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context4.next = 13;
                break;

              case 10:
                _context4.prev = 10;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 13:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 10]]);
      }))();
    },
    fetchCurencies: function fetchCurencies() {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/sales_order/currency");

              case 3:
                response = _context5.sent;
                _context5.next = 6;
                return response.data;

              case 6:
                result = _context5.sent;
                result.map(function (res) {
                  _this6.currencies.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context5.next = 13;
                break;

              case 10:
                _context5.prev = 10;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 13:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 10]]);
      }))();
    },
    fetchCustomers: function fetchCustomers() {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/customer");

              case 3:
                response = _context6.sent;
                _context6.next = 6;
                return response.data;

              case 6:
                result = _context6.sent;
                result.map(function (res) {
                  _this7.customers.push({
                    id: res.id,
                    name: res.company_name,
                    priceCategory: res.price_category_id
                  });
                });
                _context6.next = 13;
                break;

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 10]]);
      }))();
    },
    fetchSalesmens: function fetchSalesmens() {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/salesman");

              case 3:
                response = _context7.sent;
                _context7.next = 6;
                return response.data;

              case 6:
                result = _context7.sent;
                result.map(function (res) {
                  _this8.salesmans.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context7.next = 13;
                break;

              case 10:
                _context7.prev = 10;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0);

              case 13:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 10]]);
      }))();
    },
    fetchPaymentTerms: function fetchPaymentTerms() {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _context8.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/paymentTerm");

              case 3:
                response = _context8.sent;
                _context8.next = 6;
                return response.data;

              case 6:
                result = _context8.sent;
                result.map(function (res) {
                  _this9.paymentTerms.push({
                    id: res.id,
                    name: res.code + " - " + res.name
                  });
                });
                _context8.next = 13;
                break;

              case 10:
                _context8.prev = 10;
                _context8.t0 = _context8["catch"](0);
                console.log(_context8.t0);

              case 13:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 10]]);
      }))();
    },
    fetchItems: function fetchItems(e) {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                _context9.prev = 0;
                _this10.items = [];
                _this10.selectedItem = [];
                _context9.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(e.priceCategory));

              case 5:
                response = _context9.sent;
                _context9.next = 8;
                return response.data;

              case 8:
                result = _context9.sent;
                result.map(function (res) {
                  _this10.items.push({
                    id: res.product_id,
                    name: res.product.code + " - " + res.product.name,
                    productPrice: res.id
                  });
                });
                _context9.next = 15;
                break;

              case 12:
                _context9.prev = 12;
                _context9.t0 = _context9["catch"](0);
                console.log(_context9.t0);

              case 15:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9, null, [[0, 12]]);
      }))();
    },
    fetchItemAfterCreateProduct: function fetchItemAfterCreateProduct() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                _context10.prev = 0;
                _context10.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/transaction/quotation_material/product_price/".concat(_this11.selectedCustomer.priceCategory));

              case 3:
                response = _context10.sent;
                _context10.next = 6;
                return response.data;

              case 6:
                result = _context10.sent;
                result.map(function (res) {
                  _this11.items.push({
                    id: res.product_id,
                    name: res.product.code + " - " + res.product.name,
                    productPrice: res.id
                  });
                });
                _context10.next = 13;
                break;

              case 10:
                _context10.prev = 10;
                _context10.t0 = _context10["catch"](0);
                console.log(_context10.t0);

              case 13:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10, null, [[0, 10]]);
      }))();
    },
    fetchUoms: function fetchUoms() {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _context11.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/procurement/purchase_requisition/uom");

              case 2:
                response = _context11.sent;
                _context11.next = 5;
                return response.data;

              case 5:
                result = _context11.sent;
                _this12.uoms = result;

              case 7:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }))();
    },
    fetchItemDetails: function fetchItemDetails(e, i) {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                if (e) {
                  product_id = e.id;
                  _this13.products[i].price = null;
                  _this13.products[i].discountRate = null;
                  _this13.products[i].discountAmount = null;
                  _this13.products[i].uom = null;
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/transaction/product/product-detail/".concat(product_id)).then(function (response) {
                    var uom = response.data.uom_id;
                    _this13.products[i].uom = uom;

                    if (uom) {
                      axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(uom)).then(function (response) {
                        if (response.data) {
                          var price = response.data.price;
                          var discountRate = response.data.discount;
                          var discountAmount = response.data.discount_amount;
                          _this13.products[i].price = parseInt(price, 10);
                          _this13.products[i].discountRate = discountRate;
                          _this13.products[i].discountAmount = parseInt(discountAmount, 10);
                        }
                      })["catch"](function (e) {
                        return console.log(e);
                      });
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 1:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    fetchUomDetails: function fetchUomDetails(e, i) {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        var product_id;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                product_id = _this14.products[i].item;
                _this14.products[i].uom = null;
                _this14.products[i].price = null;
                _this14.products[i].discountRate = null;
                _this14.products[i].discountAmount = null;

                if (e) {
                  axios__WEBPACK_IMPORTED_MODULE_1___default()("/api/procurement/product/product-price/".concat(product_id, "/").concat(e)).then(function (response) {
                    if (response.data) {
                      var price = response.data.price;
                      var discountRate = response.data.discount;
                      var discountAmount = response.data.discount_amount;
                      _this14.products[i].price = parseInt(price, 10);
                      _this14.products[i].discountRate = discountRate;
                      _this14.products[i].discountAmount = parseInt(discountAmount, 10);
                    }
                  })["catch"](function (e) {
                    return console.log(e);
                  });
                }

              case 6:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13);
      }))();
    },
    fetchProjectCode: function fetchProjectCode() {
      var _this15 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                _context14.prev = 0;
                _context14.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/purchase-order/project");

              case 3:
                response = _context14.sent;
                _context14.next = 6;
                return response.data;

              case 6:
                result = _context14.sent;
                result.map(function (res) {
                  _this15.projects.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context14.next = 12;
                break;

              case 10:
                _context14.prev = 10;
                _context14.t0 = _context14["catch"](0);

              case 12:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14, null, [[0, 10]]);
      }))();
    },
    addItem: function addItem() {
      this.products.push({
        item: null,
        qty: null,
        uom: null,
        description: null,
        discountRate: null,
        discountAmount: null,
        price: null
      });
    },
    removeItem: function removeItem(itemId) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee15() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                _context15.next = 2;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/transaction/quotation_material/removeItem/".concat(itemId));

              case 2:
                response = _context15.sent;
                _context15.next = 5;
                return response.data;

              case 5:
                result = _context15.sent;

              case 6:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15);
      }))();
    },
    deleteItem: function deleteItem(i, itemId) {
      var _this16 = this;

      this.$swal({
        title: "Are you sure?",
        text: "You can't revert your action",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes Delete it!",
        cancelButtonText: "No, Keep it!",
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          if (typeof itemId !== "undefined") {
            _this16.removeItem(itemId);
          }

          _this16.products.splice(i, 1);

          _this16.$swal("Deleted", "You successfully deleted this data", "success");
        } else {
          _this16.$swal("Cancelled", "Your file is still intact", "info");
        }
      });
    },
    handleSubmit: function handleSubmit() {
      var _this17 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee16() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee16$(_context16) {
          while (1) {
            switch (_context16.prev = _context16.next) {
              case 0:
                _this17.loading = true;
                _context16.prev = 1;
                data = {};
                data["_method"] = "PUT";
                data["customer_id"] = _this17.form.customer;
                data["salesman_id"] = _this17.form.salesman;
                data["project_id"] = _this17.form.project_id;
                data["department_id"] = _this17.form.department;
                data["currency_id"] = _this17.form.currency;
                data["products"] = _this17.products;
                data["get_total"] = _this17.form.getTotal;
                data["discount_all_item"] = _this17.form.discountAllItem;
                data["sub_total"] = _this17.form.subTotal;
                data["tax_id"] = _this17.form.tax_id;
                data["total"] = _this17.form.total;
                data["description"] = _this17.form.description;
                data["payment_term_id"] = _this17.form.paymentTerm;
                data["resubmit"] = _this17.resubmit;
                _context16.next = 20;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/transaction/sales_order/".concat(_this17.id), data);

              case 20:
                response = _context16.sent;
                _context16.next = 23;
                return response;

              case 23:
                result = _context16.sent;

                if (result.data.success == true) {
                  window.location.href = result.data.redirect;
                }

                _context16.next = 31;
                break;

              case 27:
                _context16.prev = 27;
                _context16.t0 = _context16["catch"](1);
                _this17.loading = false;

                if (_context16.t0.response.status === 422) {
                  _this17.errors = _context16.t0.response.data.errors;
                }

              case 31:
              case "end":
                return _context16.stop();
            }
          }
        }, _callee16, null, [[1, 27]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-js-modal */ "./node_modules/vue-js-modal/dist/index.js");
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_js_modal__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/input */ "./resources/js/components/commons/input.vue");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! vue-select/dist/vue-select.css */ "./node_modules/vue-select/dist/vue-select.css");
/* harmony import */ var vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_select_dist_vue_select_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vue-md-date-picker */ "./node_modules/vue-md-date-picker/dist/vue-date-picker.js");
/* harmony import */ var vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


Vue.use(vue_js_modal__WEBPACK_IMPORTED_MODULE_1___default.a);





/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['id', 'billing', 'pr_history'],
  components: {
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_2__["default"],
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_3___default.a,
    DatePicker: vue_md_date_picker__WEBPACK_IMPORTED_MODULE_6___default.a,
    Input: _resources_js_components_commons_input__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  data: function data() {
    return {
      resizable: false,
      adaptive: true,
      validationErrors: ""
    };
  },
  methods: {
    formatPrice: function formatPrice(value) {
      var val = (value / 1).toFixed(0).replace(",", ".");
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-js-modal */ "./node_modules/vue-js-modal/dist/index.js");
/* harmony import */ var vue_js_modal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_js_modal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../../resources/js/components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vue-select */ "./node_modules/vue-select/dist/vue-select.js");
/* harmony import */ var vue_select__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_select__WEBPACK_IMPORTED_MODULE_4__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


Vue.use(vue_js_modal__WEBPACK_IMPORTED_MODULE_2___default.a);


Vue.component('v-select', {
  "extends": vue_select__WEBPACK_IMPORTED_MODULE_4___default.a,
  methods: {
    maybeAdjustScroll: function maybeAdjustScroll() {
      return false;
    }
  }
});
/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    ValidationErrors: _resources_js_components_commons_validation_errors__WEBPACK_IMPORTED_MODULE_3__["default"],
    vSelect: vue_select__WEBPACK_IMPORTED_MODULE_4___default.a
  },
  data: function data() {
    return {
      form: {
        code: "",
        name: "",
        brand: "",
        location: "",
        product_category: "",
        product_sub_category: ""
      },
      resizable: false,
      adaptive: true,
      validationErrors: "",
      product_category: [],
      product_sub_category: [],
      selectedProductCategory: null,
      selectedProductSubCategory: null
    };
  },
  mounted: function mounted() {
    this.fetchProductCategory();
  },
  watch: {
    selectedProductCategory: {
      handler: function handler() {
        if (this.selectedProductCategory) {
          this.form.product_category = this.selectedProductCategory.id;
        } else {
          this.form.product_category = null;
        }
      }
    },
    selectedProductSubCategory: {
      handler: function handler() {
        if (this.selectedProductSubCategory) {
          this.form.product_sub_category = this.selectedProductSubCategory.id;
        } else {
          this.form.product_sub_category = null;
        }
      }
    }
  },
  methods: {
    isNumber: function isNumber(evt) {
      evt = evt ? evt : window.event;
      var charCode = evt.which ? evt.which : evt.keyCode;

      if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode !== 46) {
        evt.preventDefault();
        ;
      } else {
        return true;
      }
    },
    fetchProductCategory: function fetchProductCategory() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/product_category");

              case 3:
                response = _context.sent;
                _context.next = 6;
                return response.data;

              case 6:
                result = _context.sent;
                result.map(function (res) {
                  _this.product_category.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context.next = 12;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 10]]);
      }))();
    },
    fetchProductSubCategory: function fetchProductSubCategory(event) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var id, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _this2.product_sub_category = [];
                id = event.id;
                _context2.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/transaction/product/product_sub_category/".concat(id));

              case 5:
                response = _context2.sent;
                _context2.next = 8;
                return response.data;

              case 8:
                result = _context2.sent;
                result.map(function (res) {
                  _this2.product_sub_category.push({
                    id: res.id,
                    name: res.name
                  });
                });
                _context2.next = 14;
                break;

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](0);

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 12]]);
      }))();
    },
    onClickButton: function onClickButton() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var data, response, result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this3.loading = true;
                _context3.prev = 1;
                data = {};
                data["code"] = _this3.form.code;
                data["name"] = _this3.form.name;
                data["price"] = _this3.form.price;
                data["product_category_id"] = _this3.form.product_category;
                data["product_sub_category_id"] = _this3.form.product_sub_category;
                _context3.next = 10;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post("/api/transaction/product/store", data);

              case 10:
                response = _context3.sent;
                _this3.form = {
                  code: "",
                  name: "",
                  brand: "",
                  product_category: "",
                  product_sub_category: "",
                  location: ""
                };
                _this3.validationErrors = "";
                _this3.selectedProductCategory = null;
                _this3.selectedProductSubCategory = null;
                _context3.next = 17;
                return response;

              case 17:
                result = _context3.sent;

                _this3.$emit('messageFromChild');

                _this3.$root.$emit('refetch_the_product_from_qm');

                _this3.$modal.hide('example-modal');

                _this3.$swal("Yeay!", "Your entry has been added successfully", "success");

                _context3.next = 28;
                break;

              case 24:
                _context3.prev = 24;
                _context3.t0 = _context3["catch"](1);
                _this3.loading = false;

                if (_context3.t0.response.status === 422) {
                  _this3.validationErrors = _context3.t0.response.data.errors;
                }

              case 28:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[1, 24]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ExampleComponent.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ExampleComponent.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  mounted: function mounted() {
    console.log('Component mounted.');
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/input.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/commons/input.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: ['label', 'type', 'error', 'errorMessage']
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/validation-errors.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/commons/validation-errors.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  props: ['errors'],
  computed: {
    validationErrors: function validationErrors() {
      var errors = Object.values(this.errors);
      errors = errors.flat();
      return errors;
    }
  }
});

/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=template&id=281f828a&":
/*!******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/HR/Resources/assets/js/setting_shift/Create.vue?vue&type=template&id=281f828a& ***!
  \******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.validationErrors
        ? _c("ValidationErrors", { attrs: { errors: _vm.validationErrors } })
        : _vm._e(),
      _vm._v(" "),
      _c("div", { staticClass: "form-row" }, [
        _c(
          "div",
          { staticClass: "form-group col-md-4" },
          [
            _c("label", {}, [_vm._v("Month")]),
            _vm._v(" "),
            _c("v-select", {
              attrs: { options: _vm.month, label: "name", required: "" },
              model: {
                value: _vm.selectedMonth,
                callback: function($$v) {
                  _vm.selectedMonth = $$v
                },
                expression: "selectedMonth"
              }
            }),
            _c("br")
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "form-group col-md-4" },
          [
            _c("label", [_vm._v("Year")]),
            _vm._v(" "),
            _c("v-select", {
              attrs: { options: _vm.years, label: "name", required: "" },
              model: {
                value: _vm.selectedYear,
                callback: function($$v) {
                  _vm.selectedYear = $$v
                },
                expression: "selectedYear"
              }
            }),
            _c("br")
          ],
          1
        ),
        _vm._v(" "),
        _c("div", { staticClass: "form-group col-md-4" })
      ]),
      _vm._v(" "),
      _c(
        "button",
        {
          staticClass: "btn btn-primary",
          on: {
            click: function($event) {
              $event.preventDefault()
              return _vm.fetchCalendar()
            }
          }
        },
        [_vm._v("Search")]
      ),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.handleSubmit($event)
            }
          }
        },
        [
          _c("div", { staticClass: "table-responsive" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-success fa-pull-right",
                attrs: { type: "submit" }
              },
              [_vm._v("Save")]
            ),
            _vm._v(" "),
            _c("br"),
            _c("br"),
            _c("br"),
            _vm._v(" "),
            _c(
              "table",
              {
                staticClass: "table table-bordered table-hover table-striped",
                attrs: { cellspacing: "0", id: "datatable" }
              },
              [
                _vm._m(0),
                _vm._v(" "),
                _vm._l(this.dataCalendar, function(c, index) {
                  return _c("tr", { key: index }, [
                    _c("td", [_vm._v(_vm._s(index + 1))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(c.date))]),
                    _vm._v(" "),
                    _c("td", [_vm._v(_vm._s(c.day))]),
                    _vm._v(" "),
                    _c(
                      "td",
                      [
                        _c("v-select", {
                          attrs: {
                            options: _vm.employees,
                            label: "name",
                            required: "",
                            multiple: ""
                          },
                          model: {
                            value: _vm.selectedEmployeeShift1[index],
                            callback: function($$v) {
                              _vm.$set(_vm.selectedEmployeeShift1, index, $$v)
                            },
                            expression: "selectedEmployeeShift1[index]"
                          }
                        }),
                        _c("br")
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "td",
                      [
                        _c("v-select", {
                          attrs: {
                            options: _vm.employees,
                            label: "name",
                            required: "",
                            multiple: ""
                          },
                          model: {
                            value: _vm.selectedEmployeeShift2[index],
                            callback: function($$v) {
                              _vm.$set(_vm.selectedEmployeeShift2, index, $$v)
                            },
                            expression: "selectedEmployeeShift2[index]"
                          }
                        }),
                        _c("br")
                      ],
                      1
                    )
                  ])
                })
              ],
              2
            )
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "tr",
      {
        staticStyle: { "background-color": "white", color: "grey" },
        attrs: { align: "center" }
      },
      [
        _c("th", [_vm._v("#")]),
        _vm._v(" "),
        _c("th", [_vm._v("Date")]),
        _vm._v(" "),
        _c("th", [_vm._v("Day")]),
        _vm._v(" "),
        _c("th", [_vm._v("Shift 1")]),
        _vm._v(" "),
        _c("th", [_vm._v("Shift 2")])
      ]
    )
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=template&id=2c6603db&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue?vue&type=template&id=2c6603db& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.handleSubmit($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Date")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.date,
                      expression: "form.date"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.date },
                  attrs: { type: "text", placeholder: "yyyy-mm-dd" },
                  domProps: { value: _vm.form.date },
                  on: {
                    click: function($event) {
                      _vm.show_start = true
                    },
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "date", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.date
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.date[0]))
                    ])
                  : _vm._e()
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Time")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.time,
                      expression: "form.time"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.time },
                  attrs: { type: "time", placeholder: "yyyy-mm-dd" },
                  domProps: { value: _vm.form.time },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "time", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.time
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.time[0]))
                    ])
                  : _vm._e()
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.supplier_id },
                    attrs: { options: _vm.supplier, label: "name" },
                    on: {
                      input: function($event) {
                        return _vm.showDetail($event)
                      }
                    },
                    model: {
                      value: _vm.selectedSupplier,
                      callback: function($$v) {
                        _vm.selectedSupplier = $$v
                      },
                      expression: "selectedSupplier"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.supplier_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.supplier_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("br"),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-info",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.showSupplier($event)
                      }
                    }
                  },
                  [_vm._v("+ Add New Supplier")]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-4" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(1),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.currency_id },
                    attrs: { options: _vm.currency, label: "name" },
                    model: {
                      value: _vm.selectedCurrency,
                      callback: function($$v) {
                        _vm.selectedCurrency = $$v
                      },
                      expression: "selectedCurrency"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.currency_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.currency_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.payment_method_id
                    },
                    attrs: { options: _vm.paymentMethod, label: "name" },
                    model: {
                      value: _vm.selectedPaymentMethod,
                      callback: function($$v) {
                        _vm.selectedPaymentMethod = $$v
                      },
                      expression: "selectedPaymentMethod"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.payment_method_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(
                          _vm._s(_vm.validationErrors.payment_method_id[0])
                        )
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(3),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.payment_term_id
                    },
                    attrs: { options: _vm.paymentTerm, label: "name" },
                    model: {
                      value: _vm.selectedPaymentTerm,
                      callback: function($$v) {
                        _vm.selectedPaymentTerm = $$v
                      },
                      expression: "selectedPaymentTerm"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.payment_term_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.payment_term_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass: "table table-bordered table-hovered",
                    staticStyle: { "font-size": "9px" }
                  },
                  [
                    _vm._m(4),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      [
                        _vm._l(this.products, function(product, index) {
                          return _c("tr", { key: index }, [
                            _c("td", { staticStyle: { "font-size": "16px" } }, [
                              _vm.selectedItem[index]
                                ? _c("i", {
                                    staticClass: "fa fa-info-circle",
                                    attrs: {
                                      "product-id": product.item,
                                      id: _vm.selectedItem[index]
                                    },
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.showPriceHistory(
                                          $event,
                                          product,
                                          index
                                        )
                                      }
                                    }
                                  })
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              { staticStyle: { "font-size": "16px" } },
                              [
                                _c("v-select", {
                                  attrs: {
                                    options: _vm.items,
                                    label: "name",
                                    required: ""
                                  },
                                  on: {
                                    input: function($event) {
                                      return _vm.fetchItemDetails($event, index)
                                    }
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "search",
                                        fn: function(ref) {
                                          var attributes = ref.attributes
                                          var events = ref.events
                                          return [
                                            _c(
                                              "input",
                                              _vm._g(
                                                _vm._b(
                                                  {
                                                    staticClass: "vs__search",
                                                    attrs: {
                                                      required: !_vm
                                                        .selectedItem[index]
                                                    }
                                                  },
                                                  "input",
                                                  attributes,
                                                  false
                                                ),
                                                events
                                              )
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  ),
                                  model: {
                                    value: _vm.selectedItem[index],
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedItem, index, $$v)
                                    },
                                    expression: "selectedItem[index]"
                                  }
                                }),
                                _c("br"),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.description,
                                      expression: "product.description"
                                    }
                                  ],
                                  staticClass: "form-control m-input",
                                  staticStyle: { "font-size": "16px" },
                                  attrs: {
                                    type: "text",
                                    placeholder: "Item Description",
                                    required: ""
                                  },
                                  domProps: { value: product.description },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        product,
                                        "description",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: {
                                  width: "15%",
                                  "font-size": "16px"
                                },
                                attrs: { width: "10%" }
                              },
                              [
                                _c("v-select", {
                                  attrs: {
                                    options: _vm.uoms,
                                    label: "name",
                                    required: ""
                                  },
                                  on: {
                                    input: function($event) {
                                      return _vm.fetchUomDetails($event, index)
                                    }
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "search",
                                        fn: function(ref) {
                                          var attributes = ref.attributes
                                          var events = ref.events
                                          return [
                                            _c(
                                              "input",
                                              _vm._g(
                                                _vm._b(
                                                  {
                                                    staticClass: "vs__search",
                                                    attrs: {
                                                      required: !_vm
                                                        .selectedUom[index]
                                                    }
                                                  },
                                                  "input",
                                                  attributes,
                                                  false
                                                ),
                                                events
                                              )
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  ),
                                  model: {
                                    value: _vm.selectedUom[index],
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedUom, index, $$v)
                                    },
                                    expression: "selectedUom[index]"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: { width: "15%" },
                                attrs: { width: "8%" }
                              },
                              [
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { required: "" },
                                  model: {
                                    value: product.qty,
                                    callback: function($$v) {
                                      _vm.$set(product, "qty", $$v)
                                    },
                                    expression: "product.qty"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { required: "" },
                                  model: {
                                    value: product.price,
                                    callback: function($$v) {
                                      _vm.$set(product, "price", $$v)
                                    },
                                    expression: "product.price"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "14%" } },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.discountRate,
                                      expression: "product.discountRate"
                                    }
                                  ],
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: {
                                    type: "number",
                                    value: "0",
                                    max: "100",
                                    min: "0",
                                    placeholder: "%"
                                  },
                                  domProps: { value: product.discountRate },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        product,
                                        "discountRate",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _c("br"),
                                _vm._v(" "),
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { placeholder: "IDR" },
                                  model: {
                                    value: product.discountAmount,
                                    callback: function($$v) {
                                      _vm.$set(product, "discountAmount", $$v)
                                    },
                                    expression: "product.discountAmount"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: { "font-size": "16px" },
                                attrs: { width: "13%", align: "right" }
                              },
                              [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(
                                      product.qty && product.price
                                        ? _vm.formatPrice(
                                            product.price * product.qty -
                                              (product.discountAmount
                                                ? product.discountAmount
                                                : 0) -
                                              (product.discountRate
                                                ? (product.discountRate / 100) *
                                                  product.price
                                                : 0) *
                                                product.qty
                                          )
                                        : 0
                                    ) +
                                    "\r\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td", [
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-xs btn-danger",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.deleteItem(index)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-trash" })]
                              )
                            ])
                          ])
                        }),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", { attrs: { colspan: "8" } }, [
                            _c(
                              "button",
                              {
                                staticClass:
                                  "btn btn-success pull-right btn-block",
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.addItem($event)
                                  }
                                }
                              },
                              [_vm._v("+ Add New Row")]
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("tr", { staticStyle: { "font-size": "16px" } }, [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "13px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "6", align: "right" }
                            },
                            [_vm._v("Subtotal Exclude Discount All Items")]
                          ),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _vm._v(_vm._s(_vm.formatPrice(_vm.getTotal)))
                          ])
                        ]),
                        _vm._v(" "),
                        _c("tr", { staticStyle: { "font-size": "16px" } }, [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "13px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "6", align: "right" }
                            },
                            [_vm._v("Discount All Items")]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            { attrs: { align: "right" } },
                            [
                              _c("money", {
                                staticClass: "form-control m-input",
                                staticStyle: {
                                  "font-size": "16px",
                                  "text-align": "right"
                                },
                                model: {
                                  value: _vm.form.discountAllItem,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "discountAllItem", $$v)
                                  },
                                  expression: "form.discountAllItem"
                                }
                              })
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("tr", { staticStyle: { "font-size": "16px" } }, [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "13px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "6", align: "right" }
                            },
                            [_vm._v("Sub Total")]
                          ),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _vm._v(
                              "\r\n                                    " +
                                _vm._s(_vm.formatPrice(_vm.subTotal)) +
                                "\r\n                                "
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("tr", { staticStyle: { "font-size": "16px" } }, [
                          _c("td", {
                            staticStyle: {
                              "font-size": "13px",
                              "font-weight": "bold"
                            },
                            attrs: { colspan: "5", align: "right" }
                          }),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _c(
                              "div",
                              {
                                staticClass: "form-group",
                                staticStyle: { width: "100px" }
                              },
                              [
                                _c(
                                  "label",
                                  {
                                    staticClass: "input-group-append",
                                    staticStyle: {
                                      "font-size": "13px",
                                      "font-weight": "bold",
                                      "margin-right": "5px"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\r\n                                            PPN\r\n                                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "input-group colorpickerinput"
                                  },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.tax,
                                          expression: "form.tax"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text", disabled: "" },
                                      domProps: { value: _vm.form.tax },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.form,
                                            "tax",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(5)
                                  ]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { "font-size": "16px" },
                              attrs: { align: "right" }
                            },
                            [
                              _vm._v(
                                "\r\n                                    " +
                                  _vm._s(_vm.formatPrice(_vm.getPpn)) +
                                  "\r\n                                "
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "tr",
                          {
                            staticStyle: {
                              "font-size": "16px",
                              "font-weight": "bold"
                            }
                          },
                          [
                            _c(
                              "td",
                              { attrs: { colspan: "6", align: "right" } },
                              [_vm._v("Total")]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: { color: "#5b35c4" },
                                attrs: { align: "right" }
                              },
                              [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(_vm.formatPrice(_vm.total)) +
                                    "\r\n                                "
                                )
                              ]
                            )
                          ]
                        )
                      ],
                      2
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "m-portlet__foot m-portlet__foot--fit" }, [
            _c(
              "div",
              { staticClass: "m-form__actions m-form__actions--right" },
              [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-lg",
                    attrs: { type: "submit", disabled: _vm.loading }
                  },
                  [_vm._v(_vm._s(this.loading ? "Please Wait..." : "Save"))]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _vm.show_start
            ? _c("date-picker", {
                on: {
                  close: function($event) {
                    _vm.show_start = false
                  }
                },
                model: {
                  value: _vm.form.date,
                  callback: function($$v) {
                    _vm.$set(_vm.form, "date", $$v)
                  },
                  expression: "form.date"
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _c("price-history", {
            attrs: {
              id: this.product_id,
              billing: this.billing,
              pr_history: this.pr_history
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("Supplier", { on: { messageFromChild: _vm.childMessageReceived } })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Supplier "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Currency "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Payment Method "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Payment Term "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "20%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    Item\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "10%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    UoM\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Qty\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "20%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    Price\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v("\r\n                                    Dsc (%) "),
            _c("br"),
            _vm._v(
              "\r\n                                    Dsc Amount\r\n                                "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Sub Total\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th")
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [_c("i", [_vm._v("%")])])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=template&id=56537fa9&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue?vue&type=template&id=56537fa9& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.handleSubmit($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Date")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.date,
                      expression: "form.date"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.errors.date },
                  attrs: { type: "text", placeholder: "yyyy-mm-dd" },
                  domProps: { value: _vm.form.date },
                  on: {
                    click: function($event) {
                      _vm.show_start = true
                    },
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "date", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.errors.date
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.errors.date[0]))
                    ])
                  : _vm._e()
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Time")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.time,
                      expression: "form.time"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.time },
                  attrs: { type: "time", placeholder: "yyyy-mm-dd" },
                  domProps: { value: _vm.form.time },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "time", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.time
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.time[0]))
                    ])
                  : _vm._e()
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.errors.supplier_id },
                    attrs: { options: _vm.suppliers, label: "name" },
                    model: {
                      value: _vm.selectedSupplier,
                      callback: function($$v) {
                        _vm.selectedSupplier = $$v
                      },
                      expression: "selectedSupplier"
                    }
                  }),
                  _vm._v(" "),
                  _vm.errors.supplier_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.errors.supplier_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("br"),
              _vm._v(" "),
              _c("div", { staticClass: "form-group" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-info",
                    on: {
                      click: function($event) {
                        $event.preventDefault()
                        return _vm.showSupplier($event)
                      }
                    }
                  },
                  [_vm._v("+ Add New Supplier")]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-4" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(1),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.currency_id },
                    attrs: { options: _vm.currency, label: "name" },
                    model: {
                      value: _vm.selectedCurrency,
                      callback: function($$v) {
                        _vm.selectedCurrency = $$v
                      },
                      expression: "selectedCurrency"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.currency_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.currency_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.payment_method_id
                    },
                    attrs: { options: _vm.paymentMethod, label: "name" },
                    model: {
                      value: _vm.selectedPaymentMethod,
                      callback: function($$v) {
                        _vm.selectedPaymentMethod = $$v
                      },
                      expression: "selectedPaymentMethod"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.payment_method_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(
                          _vm._s(_vm.validationErrors.payment_method_id[0])
                        )
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-4" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(3),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.payment_term_id
                    },
                    attrs: { options: _vm.paymentTerm, label: "name" },
                    model: {
                      value: _vm.selectedPaymentTerm,
                      callback: function($$v) {
                        _vm.selectedPaymentTerm = $$v
                      },
                      expression: "selectedPaymentTerm"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.payment_term_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.payment_term_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  { staticClass: "table table-bordered table-hovered" },
                  [
                    _vm._m(4),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      [
                        _vm._l(this.products, function(product, index) {
                          return _c("tr", { key: index }, [
                            _c("td", [_vm._v(_vm._s(index + 1))]),
                            _vm._v(" "),
                            _c("td", { staticStyle: { "font-size": "16px" } }, [
                              _vm.selectedItem[index]
                                ? _c("i", {
                                    staticClass: "fa fa-info-circle",
                                    attrs: {
                                      "product-id": product.item,
                                      id: _vm.selectedItem[index]
                                    },
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.showPriceHistory(
                                          $event,
                                          product,
                                          index
                                        )
                                      }
                                    }
                                  })
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c("v-select", {
                                  attrs: {
                                    options: _vm.items,
                                    label: "name",
                                    required: ""
                                  },
                                  on: {
                                    input: function($event) {
                                      return _vm.fetchItemDetails($event, index)
                                    }
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "search",
                                        fn: function(ref) {
                                          var attributes = ref.attributes
                                          var events = ref.events
                                          return [
                                            _c(
                                              "input",
                                              _vm._g(
                                                _vm._b(
                                                  {
                                                    staticClass: "vs__search",
                                                    attrs: {
                                                      required: !_vm
                                                        .selectedItem[index]
                                                    }
                                                  },
                                                  "input",
                                                  attributes,
                                                  false
                                                ),
                                                events
                                              )
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  ),
                                  model: {
                                    value: _vm.selectedItem[index],
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedItem, index, $$v)
                                    },
                                    expression: "selectedItem[index]"
                                  }
                                }),
                                _c("br"),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.description,
                                      expression: "product.description"
                                    }
                                  ],
                                  staticClass: "form-control m-input",
                                  staticStyle: { "font-size": "16px" },
                                  attrs: {
                                    type: "text",
                                    placeholder: "Item Description",
                                    required: ""
                                  },
                                  domProps: { value: product.description },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        product,
                                        "description",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "10%" } },
                              [
                                _c("v-select", {
                                  attrs: {
                                    options: _vm.uoms,
                                    label: "name",
                                    required: ""
                                  },
                                  on: {
                                    input: function($event) {
                                      return _vm.fetchUomDetails($event, index)
                                    }
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "search",
                                        fn: function(ref) {
                                          var attributes = ref.attributes
                                          var events = ref.events
                                          return [
                                            _c(
                                              "input",
                                              _vm._g(
                                                _vm._b(
                                                  {
                                                    staticClass: "vs__search",
                                                    attrs: {
                                                      required: !_vm
                                                        .selectedUom[index]
                                                    }
                                                  },
                                                  "input",
                                                  attributes,
                                                  false
                                                ),
                                                events
                                              )
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  ),
                                  model: {
                                    value: _vm.selectedUom[index],
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedUom, index, $$v)
                                    },
                                    expression: "selectedUom[index]"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "13%" } },
                              [
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { required: "" },
                                  model: {
                                    value: product.qty,
                                    callback: function($$v) {
                                      _vm.$set(product, "qty", $$v)
                                    },
                                    expression: "product.qty"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { required: "" },
                                  model: {
                                    value: product.price,
                                    callback: function($$v) {
                                      _vm.$set(product, "price", $$v)
                                    },
                                    expression: "product.price"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "14%" } },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.discountRate,
                                      expression: "product.discountRate"
                                    }
                                  ],
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: {
                                    type: "number",
                                    value: "0",
                                    max: "100",
                                    min: "0"
                                  },
                                  domProps: { value: product.discountRate },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        product,
                                        "discountRate",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _c("br"),
                                _vm._v(" "),
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  model: {
                                    value: product.discountAmount,
                                    callback: function($$v) {
                                      _vm.$set(product, "discountAmount", $$v)
                                    },
                                    expression: "product.discountAmount"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "13%", align: "right" } },
                              [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(
                                      product.qty && product.price
                                        ? _vm.formatPrice(
                                            product.price * product.qty -
                                              (product.discountAmount
                                                ? product.discountAmount
                                                : 0) -
                                              (product.discountRate
                                                ? (product.discountRate / 100) *
                                                  product.price
                                                : 0) *
                                                product.qty
                                          )
                                        : 0
                                    ) +
                                    "\r\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td", [
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-xs btn-danger",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.deleteItem(index, product.id)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-trash" })]
                              )
                            ])
                          ])
                        }),
                        _vm._v(" "),
                        !_vm.form.purchase_requisition_id
                          ? _c("tr", [
                              _c("td", { attrs: { colspan: "8" } }, [
                                _c(
                                  "button",
                                  {
                                    staticClass:
                                      "btn btn-success pull-right btn-block",
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.addItem($event)
                                      }
                                    }
                                  },
                                  [_vm._v("+ Add New Row")]
                                )
                              ])
                            ])
                          : _vm._e(),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "16px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "6", align: "right" }
                            },
                            [_vm._v("Subtotal Exclude Discount All Items")]
                          ),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _vm._v(_vm._s(_vm.formatPrice(_vm.getTotal)))
                          ]),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "16px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "6", align: "right" }
                            },
                            [_vm._v("Discount All Items")]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            { attrs: { align: "right" } },
                            [
                              _c("money", {
                                staticClass: "form-control m-input",
                                staticStyle: {
                                  "font-size": "16px",
                                  "text-align": "right"
                                },
                                attrs: { required: "" },
                                model: {
                                  value: _vm.form.discountAllItem,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "discountAllItem", $$v)
                                  },
                                  expression: "form.discountAllItem"
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "16px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "6", align: "right" }
                            },
                            [_vm._v("Sub Total")]
                          ),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _vm._v(
                              "\r\n                                    " +
                                _vm._s(_vm.formatPrice(_vm.subTotal)) +
                                "\r\n                                "
                            )
                          ]),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", {
                            staticStyle: {
                              "font-size": "13px",
                              "font-weight": "bold"
                            },
                            attrs: { colspan: "5", align: "right" }
                          }),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _c(
                              "div",
                              {
                                staticClass: "form-group",
                                staticStyle: { width: "100px" }
                              },
                              [
                                _c(
                                  "label",
                                  {
                                    staticClass: "input-group-append",
                                    staticStyle: {
                                      "font-size": "13px",
                                      "font-weight": "bold",
                                      "margin-right": "5px"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\r\n                                            GST\r\n                                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "input-group colorpickerinput"
                                  },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.tax,
                                          expression: "form.tax"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text", disabled: "" },
                                      domProps: { value: _vm.form.tax },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.form,
                                            "tax",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(5)
                                  ]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _vm._v(
                              "\r\n                                    " +
                                _vm._s(_vm.formatPrice(_vm.getGst)) +
                                "\r\n                                "
                            )
                          ]),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c(
                          "tr",
                          {
                            staticStyle: {
                              "font-size": "16px",
                              "font-weight": "bold"
                            }
                          },
                          [
                            _c(
                              "td",
                              { attrs: { colspan: "6", align: "right" } },
                              [_vm._v("Total")]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: { color: "#5b35c4" },
                                attrs: { align: "right" }
                              },
                              [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(_vm.formatPrice(_vm.total)) +
                                    "\r\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td")
                          ]
                        )
                      ],
                      2
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "m-portlet__foot m-portlet__foot--fit" }, [
            _c(
              "div",
              { staticClass: "m-form__actions m-form__actions--right" },
              [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-lg",
                    attrs: { type: "submit", disabled: _vm.loading }
                  },
                  [_vm._v(_vm._s(this.loading ? "Please Wait..." : "Update"))]
                )
              ]
            )
          ])
        ]
      ),
      _vm._v(" "),
      _c("Supplier", { on: { messageFromChild: _vm.childMessageReceived } }),
      _vm._v(" "),
      _vm.show_start
        ? _c("date-picker", {
            on: {
              close: function($event) {
                _vm.show_start = false
              }
            },
            model: {
              value: _vm.form.submition_date,
              callback: function($$v) {
                _vm.$set(_vm.form, "submition_date", $$v)
              },
              expression: "form.submition_date"
            }
          })
        : _vm._e(),
      _vm._v(" "),
      _vm.show_end
        ? _c("date-picker", {
            on: {
              close: function($event) {
                _vm.show_end = false
              }
            },
            model: {
              value: _vm.form.required_date,
              callback: function($$v) {
                _vm.$set(_vm.form, "required_date", $$v)
              },
              expression: "form.required_date"
            }
          })
        : _vm._e(),
      _vm._v(" "),
      _c("price-history", {
        attrs: {
          id: this.product_id,
          billing: this.billing,
          pr_history: this.pr_history
        }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Supplier "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Currency "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Payment Method "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Payment Term "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", { staticStyle: { "font-size": "16px" } }, [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c("th", {
          staticStyle: { "font-size": "16px" },
          attrs: { align: "center" }
        }),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { width: "20%" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Item\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { width: "12%" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    UoM\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } }, [
          _vm._v(
            "\r\n                                    Qty\r\n                                    "
          ),
          _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
        ]),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { width: "12%" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Price\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } }, [
          _vm._v("\r\n                                    Dsc (%) "),
          _c("br"),
          _vm._v(
            "\r\n                                    Dsc Amount\r\n                                "
          )
        ]),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } }, [
          _vm._v(
            "\r\n                                    Sub Total\r\n                                    "
          ),
          _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
        ]),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [_c("i", [_vm._v("%")])])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=template&id=05f8feeb&":
/*!*****************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Procurement/Resources/assets/js/components/purchase_order/Supplier.vue?vue&type=template&id=05f8feeb& ***!
  \*****************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "modal",
    {
      attrs: {
        name: "example-modal",
        resizable: _vm.resizable,
        adaptive: _vm.adaptive,
        height: "auto",
        scrollable: true
      }
    },
    [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Company Name")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.company_name,
                    expression: "form.company_name"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.company_name },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.company_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "company_name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.company_name
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.company_name[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Supplier Name")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.name,
                    expression: "form.name"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.name },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.name
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.name[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Phone")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.phone,
                    expression: "form.phone"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.phone },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.phone },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "phone", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.phone
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.phone[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Fax")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.fax,
                    expression: "form.fax"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.fax },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.fax },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "fax", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.fax
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.fax[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Email")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.email,
                    expression: "form.email"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.email },
                attrs: { type: "email", required: "" },
                domProps: { value: _vm.form.email },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "email", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.email
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.email[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", [_vm._v("Price Category")]),
                _vm._v(" "),
                _c("v-select", {
                  class: {
                    "is-invalid": _vm.validationErrors.price_category_id
                  },
                  attrs: {
                    autocomplete: "",
                    options: _vm.price_category,
                    label: "name"
                  },
                  model: {
                    value: _vm.selectedPriceCategory,
                    callback: function($$v) {
                      _vm.selectedPriceCategory = $$v
                    },
                    expression: "selectedPriceCategory"
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.price_category_id
                  ? _c("div", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.price_category_id[0]))
                    ])
                  : _vm._e()
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", [_vm._v("Currency")]),
                _vm._v(" "),
                _c("v-select", {
                  class: { "is-invalid": _vm.validationErrors.currency_id },
                  attrs: { options: _vm.currency, label: "name" },
                  model: {
                    value: _vm.selectedCurrency,
                    callback: function($$v) {
                      _vm.selectedCurrency = $$v
                    },
                    expression: "selectedCurrency"
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.currency_id
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.currency_id[0]))
                    ])
                  : _vm._e()
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-4" }, [
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", [_vm._v("Payment Term")]),
                _vm._v(" "),
                _c("v-select", {
                  class: { "is-invalid": _vm.validationErrors.payment_term_id },
                  attrs: { options: _vm.paymentTerm, label: "name" },
                  model: {
                    value: _vm.selectedPaymentTerm,
                    callback: function($$v) {
                      _vm.selectedPaymentTerm = $$v
                    },
                    expression: "selectedPaymentTerm"
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.payment_term_id
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.payment_term_id[0]))
                    ])
                  : _vm._e()
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("label", [_vm._v("Address")]),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.address,
                  expression: "form.address"
                }
              ],
              staticClass: "form-control r-0 light s-12",
              class: { "is-invalid": _vm.validationErrors.address },
              attrs: { cols: "50", rows: "40" },
              domProps: { value: _vm.form.address },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "address", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _vm.validationErrors.address
              ? _c("label", { staticClass: "text-danger" }, [
                  _vm._v(_vm._s(_vm.validationErrors.address[0]))
                ])
              : _vm._e()
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c("label", [_vm._v("Description")]),
            _vm._v(" "),
            _c("textarea", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.description,
                  expression: "form.description"
                }
              ],
              staticClass: "form-control r-0 light s-12",
              class: { "is-invalid": _vm.validationErrors.description },
              attrs: { cols: "50", rows: "40" },
              domProps: { value: _vm.form.description },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "description", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _vm.validationErrors.description
              ? _c("label", { staticClass: "text-danger" }, [
                  _vm._v(_vm._s(_vm.validationErrors.description[0]))
                ])
              : _vm._e()
          ])
        ]),
        _vm._v(" "),
        _c("br"),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-info pull-right",
                  staticStyle: { "margin-bottom": "20px" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.onClickButton($event)
                    }
                  }
                },
                [_vm._v("Save")]
              )
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=template&id=1e9b3efc&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue?vue&type=template&id=1e9b3efc& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.fetchLoading
      ? _c("div", { staticClass: "text-center" }, [_vm._m(0)])
      : _c(
          "div",
          [
            _vm.validationErrors
              ? _c("ValidationErrors", {
                  attrs: { errors: _vm.validationErrors }
                })
              : _vm._e(),
            _vm._v(" "),
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.handleSubmit($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", [_vm._v("Sales Order ")]),
                        _vm._v(" "),
                        _c("v-select", {
                          class: {
                            "is-invalid": _vm.validationErrors.sales_order_id
                          },
                          attrs: {
                            options: _vm.salesOrderMaterials,
                            label: "name"
                          },
                          on: {
                            input: function($event) {
                              _vm.fetchSalesOrderMaterialDetail($event)
                              _vm.fetchCustomersDetail($event)
                            }
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedSalesOrderMaterial
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedSalesOrderMaterial,
                            callback: function($$v) {
                              _vm.selectedSalesOrderMaterial = $$v
                            },
                            expression: "selectedSalesOrderMaterial"
                          }
                        }),
                        _vm._v(" "),
                        _vm.validationErrors.sales_order_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(
                                _vm._s(_vm.validationErrors.sales_order_id[0])
                              )
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.showForm,
                        expression: "showForm"
                      }
                    ]
                  },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-4" }, [
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c("label", [_vm._v("Customer")]),
                            _vm._v(" "),
                            _c("v-select", {
                              class: {
                                "is-invalid": _vm.validationErrors.customer_id
                              },
                              attrs: {
                                options: _vm.customers,
                                label: "name",
                                disabled: ""
                              },
                              model: {
                                value: _vm.selectedCustomer,
                                callback: function($$v) {
                                  _vm.selectedCustomer = $$v
                                },
                                expression: "selectedCustomer"
                              }
                            }),
                            _vm._v(" "),
                            _vm.validationErrors.customer_id
                              ? _c("div", { staticClass: "text-danger" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm.validationErrors.customer_id[0]
                                    ) + "\n                            "
                                  )
                                ])
                              : _vm._e()
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c("label", [_vm._v("Salesman")]),
                            _vm._v(" "),
                            _c("v-select", {
                              class: {
                                "is-invalid": _vm.validationErrors.salesman_id
                              },
                              attrs: { options: _vm.salesmans, label: "name" },
                              model: {
                                value: _vm.selectedSalesman,
                                callback: function($$v) {
                                  _vm.selectedSalesman = $$v
                                },
                                expression: "selectedSalesman"
                              }
                            }),
                            _vm._v(" "),
                            _vm.validationErrors.salesman_id
                              ? _c("div", { staticClass: "text-danger" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm.validationErrors.salesman_id[0]
                                    ) + "\n                            "
                                  )
                                ])
                              : _vm._e()
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c("label", [_vm._v("Department")]),
                            _vm._v(" "),
                            _c("v-select", {
                              class: {
                                "is-invalid": _vm.validationErrors.department_id
                              },
                              attrs: {
                                options: _vm.departments,
                                label: "name",
                                disabled: ""
                              },
                              model: {
                                value: _vm.selectedDepartment,
                                callback: function($$v) {
                                  _vm.selectedDepartment = $$v
                                },
                                expression: "selectedDepartment"
                              }
                            }),
                            _vm._v(" "),
                            _vm.validationErrors.department_id
                              ? _c("div", { staticClass: "text-danger" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm.validationErrors.department_id[0]
                                    ) + "\n                            "
                                  )
                                ])
                              : _vm._e()
                          ],
                          1
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("h4", [_vm._v("Customer Detail")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("label", [_vm._v("Company Name")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.customerDetail.company_name,
                              expression: "this.customerDetail.company_name"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            name: "company_name",
                            disabled: ""
                          },
                          domProps: { value: this.customerDetail.company_name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.customerDetail,
                                "company_name",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", [_vm._v("Email")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.customerDetail.email,
                              expression: "this.customerDetail.email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "email", disabled: "" },
                          domProps: { value: this.customerDetail.email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.customerDetail,
                                "email",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", [_vm._v("Contact Person ")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.customerDetail.pic_1,
                              expression: "this.customerDetail.pic_1"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "pic_1", disabled: "" },
                          domProps: { value: this.customerDetail.pic_1 },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.customerDetail,
                                "pic_1",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("label", [_vm._v("Phone")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.customerDetail.phone,
                              expression: "this.customerDetail.phone"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "phone", disabled: "" },
                          domProps: { value: this.customerDetail.phone },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.customerDetail,
                                "phone",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", [_vm._v("Fax Number")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.customerDetail.fax,
                              expression: "this.customerDetail.fax"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "fax", disabled: "" },
                          domProps: { value: this.customerDetail.fax },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.customerDetail,
                                "fax",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", [_vm._v("Address")]),
                        _vm._v(" "),
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.customerDetail.address,
                              expression: "this.customerDetail.address"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { disabled: "" },
                          domProps: { value: this.customerDetail.address },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.customerDetail,
                                "address",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br")
                      ]),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "col-md-4" },
                        [
                          _c("label", [_vm._v("Credit Limit")]),
                          _vm._v(" "),
                          _c("money", {
                            staticClass: "form-control",
                            staticStyle: { "text-align": "right" },
                            attrs: {
                              type: "text",
                              name: "credit_limit",
                              disabled: ""
                            },
                            model: {
                              value: this.customerDetail.credit_limit,
                              callback: function($$v) {
                                _vm.$set(
                                  this.customerDetail,
                                  "credit_limit",
                                  $$v
                                )
                              },
                              expression: "this.customerDetail.credit_limit"
                            }
                          }),
                          _vm._v(" "),
                          _c("br")
                        ],
                        1
                      )
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Date")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.date,
                                expression: "form.date"
                              }
                            ],
                            staticClass: "form-control",
                            class: { "is-invalid": _vm.validationErrors.date },
                            attrs: { type: "text", placeholder: "yyyy-mm-dd" },
                            domProps: { value: _vm.form.date },
                            on: {
                              click: function($event) {
                                _vm.show_start = true
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(_vm.form, "date", $event.target.value)
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm.validationErrors.date
                            ? _c("label", { staticClass: "text-danger" }, [
                                _vm._v(_vm._s(_vm.validationErrors.date[0]))
                              ])
                            : _vm._e()
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Remarks")]),
                          _vm._v(" "),
                          _c("textarea", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.description,
                                expression: "form.description"
                              }
                            ],
                            staticClass: "form-control",
                            class: {
                              "is-invalid": _vm.validationErrors.description
                            },
                            staticStyle: { height: "100px" },
                            attrs: { cols: "40" },
                            domProps: { value: _vm.form.description },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "description",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _vm.validationErrors.description
                            ? _c("label", { staticClass: "text-danger" }, [
                                _vm._v(
                                  _vm._s(_vm.validationErrors.description[0])
                                )
                              ])
                            : _vm._e()
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "table-responsive" }, [
                          _c(
                            "table",
                            {
                              staticClass: "table table-bordered table-hovered",
                              staticStyle: { "font-size": "16px" }
                            },
                            [
                              _vm._m(1),
                              _vm._v(" "),
                              _c(
                                "tbody",
                                _vm._l(this.products, function(product, index) {
                                  return _c("tr", { key: index }, [
                                    _c("td", { attrs: { width: "3%" } }, [
                                      _vm._v(_vm._s(index + 1))
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      [
                                        _c("v-select", {
                                          staticStyle: { "font-size": "16px" },
                                          attrs: {
                                            options: _vm.items,
                                            label: "name",
                                            disabled: "",
                                            required: ""
                                          },
                                          on: {
                                            input: function($event) {
                                              return _vm.fetchItemDetails(
                                                $event,
                                                index
                                              )
                                            }
                                          },
                                          scopedSlots: _vm._u(
                                            [
                                              {
                                                key: "search",
                                                fn: function(ref) {
                                                  var attributes =
                                                    ref.attributes
                                                  var events = ref.events
                                                  return [
                                                    _c(
                                                      "input",
                                                      _vm._g(
                                                        _vm._b(
                                                          {
                                                            staticClass:
                                                              "vs__search",
                                                            attrs: {
                                                              required: !_vm
                                                                .selectedItem[
                                                                index
                                                              ]
                                                            }
                                                          },
                                                          "input",
                                                          attributes,
                                                          false
                                                        ),
                                                        events
                                                      )
                                                    )
                                                  ]
                                                }
                                              }
                                            ],
                                            null,
                                            true
                                          ),
                                          model: {
                                            value: _vm.selectedItem[index],
                                            callback: function($$v) {
                                              _vm.$set(
                                                _vm.selectedItem,
                                                index,
                                                $$v
                                              )
                                            },
                                            expression: "selectedItem[index]"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("br"),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: product.description,
                                              expression: "product.description"
                                            }
                                          ],
                                          staticClass: "form-control m-input",
                                          staticStyle: { "font-size": "16px" },
                                          attrs: { type: "text", required: "" },
                                          domProps: {
                                            value: product.description
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                product,
                                                "description",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("td", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.uom,
                                            expression: "product.uom"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: { "font-size": "16px" },
                                        attrs: { min: "0", disabled: "" },
                                        domProps: { value: product.uom },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "uom",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _c("br")
                                    ]),
                                    _vm._v(" "),
                                    _c("td", { attrs: { width: "10%" } }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.purchaseQty,
                                            expression: "product.purchaseQty"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: {
                                          value: "0",
                                          disabled: "",
                                          required: ""
                                        },
                                        domProps: {
                                          value: product.purchaseQty
                                        },
                                        on: {
                                          keypress: function($event) {
                                            return _vm.isNumber($event)
                                          },
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "purchaseQty",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { attrs: { width: "10%" } },
                                      [
                                        _c("money", {
                                          staticClass: "form-control",
                                          staticStyle: {
                                            "font-size": "16px",
                                            "text-align": "right"
                                          },
                                          attrs: { type: "text" },
                                          model: {
                                            value: product.confirmQty,
                                            callback: function($$v) {
                                              _vm.$set(
                                                product,
                                                "confirmQty",
                                                $$v
                                              )
                                            },
                                            expression: "product.confirmQty"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("br")
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { attrs: { width: "10%" } },
                                      [
                                        _c("v-select", {
                                          staticStyle: { "font-size": "16px" },
                                          attrs: {
                                            options: _vm.warehouses,
                                            label: "name",
                                            required: ""
                                          },
                                          scopedSlots: _vm._u(
                                            [
                                              {
                                                key: "search",
                                                fn: function(ref) {
                                                  var attributes =
                                                    ref.attributes
                                                  var events = ref.events
                                                  return [
                                                    _c(
                                                      "input",
                                                      _vm._g(
                                                        _vm._b(
                                                          {
                                                            staticClass:
                                                              "vs__search",
                                                            attrs: {
                                                              required: !_vm
                                                                .selectedWarehouses[
                                                                index
                                                              ]
                                                            }
                                                          },
                                                          "input",
                                                          attributes,
                                                          false
                                                        ),
                                                        events
                                                      )
                                                    )
                                                  ]
                                                }
                                              }
                                            ],
                                            null,
                                            true
                                          ),
                                          model: {
                                            value:
                                              _vm.selectedWarehouses[index],
                                            callback: function($$v) {
                                              _vm.$set(
                                                _vm.selectedWarehouses,
                                                index,
                                                $$v
                                              )
                                            },
                                            expression:
                                              "selectedWarehouses[index]"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("br")
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("td", { attrs: { width: "10%" } }, [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn btn-xs btn-danger",
                                          on: {
                                            click: function($event) {
                                              $event.preventDefault()
                                              return _vm.deleteItem(index)
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-trash"
                                          })
                                        ]
                                      )
                                    ])
                                  ])
                                }),
                                0
                              )
                            ]
                          )
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "m-portlet__foot m-portlet__foot--fit" },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "m-form__actions m-form__actions--right"
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-primary btn-lg",
                                attrs: { type: "submit", disabled: _vm.loading }
                              },
                              [
                                _vm._v(
                                  _vm._s(
                                    this.loading ? "Please Wait..." : "Save"
                                  )
                                )
                              ]
                            )
                          ]
                        )
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _vm.show_start
                  ? _c("date-picker", {
                      on: {
                        close: function($event) {
                          _vm.show_start = false
                        }
                      },
                      model: {
                        value: _vm.form.date,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "date", $$v)
                        },
                        expression: "form.date"
                      }
                    })
                  : _vm._e()
              ],
              1
            )
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [_vm._v("#")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "30%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\n                                        Item\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                        UoM\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                        Purchased Qty\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                        Confirm Qty\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                        Warehouse\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=template&id=f8e64be0&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue?vue&type=template&id=f8e64be0& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.fetchLoading
      ? _c("div", { staticClass: "text-center" }, [_vm._m(0)])
      : _c(
          "div",
          [
            _vm.validationErrors
              ? _c("ValidationErrors", {
                  attrs: { errors: _vm.validationErrors }
                })
              : _vm._e(),
            _vm._v(" "),
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.handleSubmit($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Sales Order ")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.sales_order,
                            expression: "form.sales_order"
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.validationErrors.title },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.form.sales_order },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.form,
                              "sales_order",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.validationErrors.sales_order_id
                        ? _c("label", { staticClass: "text-danger" }, [
                            _vm._v(
                              _vm._s(_vm.validationErrors.sales_order_id[0])
                            )
                          ])
                        : _vm._e()
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", [_vm._v("Customer")]),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.customer_id },
                          attrs: {
                            options: _vm.customers,
                            label: "name",
                            required: "",
                            disabled: ""
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedCustomer
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedCustomer,
                            callback: function($$v) {
                              _vm.selectedCustomer = $$v
                            },
                            expression: "selectedCustomer"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.customer_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.customer_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", [_vm._v("Salesman")]),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.salesman_id },
                          attrs: {
                            options: _vm.salesmans,
                            label: "name",
                            required: ""
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedSalesman
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedSalesman,
                            callback: function($$v) {
                              _vm.selectedSalesman = $$v
                            },
                            expression: "selectedSalesman"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.salesman_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.salesman_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", [_vm._v("Department")]),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.department_id },
                          attrs: {
                            options: _vm.departments,
                            label: "name",
                            disabled: "",
                            required: ""
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedDepartment
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedDepartment,
                            callback: function($$v) {
                              _vm.selectedDepartment = $$v
                            },
                            expression: "selectedDepartment"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.department_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.department_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("h4", [_vm._v("Customer Detail")]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Company Name")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.company_name,
                          expression: "this.customerDetail.company_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        name: "company_name",
                        disabled: ""
                      },
                      domProps: { value: this.customerDetail.company_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "company_name",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Email")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.email,
                          expression: "this.customerDetail.email"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "email", disabled: "" },
                      domProps: { value: this.customerDetail.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "email",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Contact Person ")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.pic_1,
                          expression: "this.customerDetail.pic_1"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "pic_1", disabled: "" },
                      domProps: { value: this.customerDetail.pic_1 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "pic_1",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Phone")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.phone,
                          expression: "this.customerDetail.phone"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "phone", disabled: "" },
                      domProps: { value: this.customerDetail.phone },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "phone",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Fax Number")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.fax,
                          expression: "this.customerDetail.fax"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "fax", disabled: "" },
                      domProps: { value: this.customerDetail.fax },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "fax",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Address")]),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.address,
                          expression: "this.customerDetail.address"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { disabled: "" },
                      domProps: { value: this.customerDetail.address },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "address",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-md-4" },
                    [
                      _c("label", [_vm._v("Credit Limit")]),
                      _vm._v(" "),
                      _c("money", {
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: {
                          type: "text",
                          name: "credit_limit",
                          disabled: ""
                        },
                        model: {
                          value: this.customerDetail.credit_limit,
                          callback: function($$v) {
                            _vm.$set(this.customerDetail, "credit_limit", $$v)
                          },
                          expression: "this.customerDetail.credit_limit"
                        }
                      }),
                      _vm._v(" "),
                      _c("br")
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Date")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.date,
                            expression: "form.date"
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.errors.date },
                        attrs: { type: "text", placeholder: "yyyy-mm-dd" },
                        domProps: { value: _vm.form.date },
                        on: {
                          click: function($event) {
                            _vm.show_start = true
                          },
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(_vm.form, "date", $event.target.value)
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.date
                        ? _c("label", { staticClass: "text-danger" }, [
                            _vm._v(_vm._s(_vm.errors.date[0]))
                          ])
                        : _vm._e()
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Remarks")]),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.description,
                            expression: "form.description"
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.validationErrors.description
                        },
                        staticStyle: { height: "100px" },
                        attrs: { cols: "40" },
                        domProps: { value: _vm.form.description },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.form,
                              "description",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.validationErrors.description
                        ? _c("label", { staticClass: "text-danger" }, [
                            _vm._v(_vm._s(_vm.validationErrors.description[0]))
                          ])
                        : _vm._e()
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "table-responsive" }, [
                      _c(
                        "table",
                        {
                          staticClass: "table table-bordered table-hovered",
                          staticStyle: { "font-size": "9px" }
                        },
                        [
                          _vm._m(1),
                          _vm._v(" "),
                          _c(
                            "tbody",
                            _vm._l(this.products, function(product, index) {
                              return _c("tr", { key: index }, [
                                _c("td", { attrs: { width: "3%" } }, [
                                  _vm._v(_vm._s(index + 1))
                                ]),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  [
                                    _c("v-select", {
                                      staticStyle: { "font-size": "16px" },
                                      attrs: {
                                        options: _vm.items,
                                        label: "name",
                                        required: "",
                                        disabled: ""
                                      },
                                      on: {
                                        input: function($event) {
                                          return _vm.fetchItemDetails(
                                            $event,
                                            index
                                          )
                                        }
                                      },
                                      model: {
                                        value: _vm.selectedItem[index],
                                        callback: function($$v) {
                                          _vm.$set(_vm.selectedItem, index, $$v)
                                        },
                                        expression: "selectedItem[index]"
                                      }
                                    }),
                                    _c("br"),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: product.description,
                                          expression: "product.description"
                                        }
                                      ],
                                      staticClass: "form-control m-input",
                                      staticStyle: { "font-size": "16px" },
                                      attrs: { type: "text", required: "" },
                                      domProps: { value: product.description },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            product,
                                            "description",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: product.uom,
                                          expression: "product.uom"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      staticStyle: { "font-size": "16px" },
                                      attrs: { required: "", disabled: "" },
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            product,
                                            "uom",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    _vm._l(_vm.uoms, function(item, i) {
                                      return _c(
                                        "option",
                                        {
                                          key: i,
                                          domProps: { value: item.id }
                                        },
                                        [_vm._v(_vm._s(item.name))]
                                      )
                                    }),
                                    0
                                  )
                                ]),
                                _vm._v(" "),
                                _c("td", { attrs: { width: "10%" } }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: product.purchaseQty,
                                        expression: "product.purchaseQty"
                                      }
                                    ],
                                    staticClass: "form-control m-input",
                                    staticStyle: {
                                      "font-size": "16px",
                                      "text-align": "right"
                                    },
                                    attrs: {
                                      value: "0",
                                      required: "",
                                      disabled: ""
                                    },
                                    domProps: { value: product.purchaseQty },
                                    on: {
                                      keypress: function($event) {
                                        return _vm.isNumber($event)
                                      },
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          product,
                                          "purchaseQty",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  { attrs: { width: "20%" } },
                                  [
                                    _c("money", {
                                      staticClass: "form-control",
                                      staticStyle: {
                                        "font-size": "16px",
                                        "text-align": "right"
                                      },
                                      attrs: { type: "text" },
                                      model: {
                                        value: product.confirmQty,
                                        callback: function($$v) {
                                          _vm.$set(product, "confirmQty", $$v)
                                        },
                                        expression: "product.confirmQty"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c("br")
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  [
                                    _c("v-select", {
                                      staticStyle: { "font-size": "16px" },
                                      attrs: {
                                        options: _vm.warehouses,
                                        label: "name",
                                        required: ""
                                      },
                                      scopedSlots: _vm._u(
                                        [
                                          {
                                            key: "search",
                                            fn: function(ref) {
                                              var attributes = ref.attributes
                                              var events = ref.events
                                              return [
                                                _c(
                                                  "input",
                                                  _vm._g(
                                                    _vm._b(
                                                      {
                                                        staticClass:
                                                          "vs__search",
                                                        attrs: {
                                                          required: !_vm
                                                            .selectedWarehouse[
                                                            index
                                                          ]
                                                        }
                                                      },
                                                      "input",
                                                      attributes,
                                                      false
                                                    ),
                                                    events
                                                  )
                                                )
                                              ]
                                            }
                                          }
                                        ],
                                        null,
                                        true
                                      ),
                                      model: {
                                        value: _vm.selectedWarehouse[index],
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.selectedWarehouse,
                                            index,
                                            $$v
                                          )
                                        },
                                        expression: "selectedWarehouse[index]"
                                      }
                                    }),
                                    _c("br")
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-xs btn-danger",
                                      on: {
                                        click: function($event) {
                                          $event.preventDefault()
                                          return _vm.deleteItem(
                                            index,
                                            product.id
                                          )
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fa fa-trash" })]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        ]
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "m-portlet__foot m-portlet__foot--fit" },
                  [
                    _c(
                      "div",
                      { staticClass: "m-form__actions m-form__actions--right" },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary btn-lg",
                            attrs: { type: "submit", disabled: _vm.loading }
                          },
                          [
                            _vm._v(
                              _vm._s(this.loading ? "Please Wait..." : "Update")
                            )
                          ]
                        )
                      ]
                    )
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _vm.show_start
              ? _c("date-picker", {
                  on: {
                    close: function($event) {
                      _vm.show_start = false
                    }
                  },
                  model: {
                    value: _vm.form.date,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "date", $$v)
                    },
                    expression: "form.date"
                  }
                })
              : _vm._e()
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "30%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\n                                        Item\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                        UoM\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                        Purchased Qty\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                        Confirm Qty\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                        Warehouse\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center", width: "5%" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=template&id=7d0829d2&":
/*!***************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue?vue&type=template&id=7d0829d2& ***!
  \***************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.fetchLoading
      ? _c("div", { staticClass: "text-center" }, [_vm._m(0)])
      : _c(
          "div",
          [
            _vm.validationErrors
              ? _c("ValidationErrors", {
                  attrs: { errors: _vm.validationErrors }
                })
              : _vm._e(),
            _vm._v(" "),
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.handleSubmit($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", [_vm._v("Purchase Order ")]),
                        _vm._v(" "),
                        _c("v-select", {
                          class: {
                            "is-invalid": _vm.validationErrors.purchase_order_id
                          },
                          attrs: { options: _vm.purchaseOrders, label: "name" },
                          on: {
                            input: function($event) {
                              _vm.fetchPurchaseOrderDetail($event)
                              _vm.fetchSupplierDetail($event)
                            }
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedPurchaseOrder
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedPurchaseOrder,
                            callback: function($$v) {
                              _vm.selectedPurchaseOrder = $$v
                            },
                            expression: "selectedPurchaseOrder"
                          }
                        }),
                        _vm._v(" "),
                        _vm.validationErrors.purchase_order_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(
                                _vm._s(
                                  _vm.validationErrors.purchase_order_id[0]
                                )
                              )
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  {
                    directives: [
                      {
                        name: "show",
                        rawName: "v-show",
                        value: _vm.showForm,
                        expression: "showForm"
                      }
                    ]
                  },
                  [
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-4" }, [
                        _c(
                          "div",
                          { staticClass: "form-group" },
                          [
                            _c("label", [_vm._v("Supplier")]),
                            _vm._v(" "),
                            _c("v-select", {
                              class: { "is-invalid": _vm.errors.supplier_id },
                              attrs: {
                                options: _vm.suppliers,
                                label: "name",
                                required: "",
                                disabled: ""
                              },
                              scopedSlots: _vm._u([
                                {
                                  key: "search",
                                  fn: function(ref) {
                                    var attributes = ref.attributes
                                    var events = ref.events
                                    return [
                                      _c(
                                        "input",
                                        _vm._g(
                                          _vm._b(
                                            {
                                              staticClass: "vs__search",
                                              attrs: {
                                                required: !_vm.selectedSupplier
                                              }
                                            },
                                            "input",
                                            attributes,
                                            false
                                          ),
                                          events
                                        )
                                      )
                                    ]
                                  }
                                }
                              ]),
                              model: {
                                value: _vm.selectedSupplier,
                                callback: function($$v) {
                                  _vm.selectedSupplier = $$v
                                },
                                expression: "selectedSupplier"
                              }
                            }),
                            _vm._v(" "),
                            _vm.errors.supplier_id
                              ? _c("label", { staticClass: "text-danger" }, [
                                  _vm._v(_vm._s(_vm.errors.supplier_id[0]))
                                ])
                              : _vm._e()
                          ],
                          1
                        )
                      ])
                    ]),
                    _vm._v(" "),
                    _c("h4", [_vm._v("Supplier Detail")]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("label", [_vm._v("Company Name")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.supplierDetail.company_name,
                              expression: "this.supplierDetail.company_name"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: {
                            type: "text",
                            name: "company_name",
                            disabled: ""
                          },
                          domProps: { value: this.supplierDetail.company_name },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.supplierDetail,
                                "company_name",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", [_vm._v("Email")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.supplierDetail.email,
                              expression: "this.supplierDetail.email"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "email", disabled: "" },
                          domProps: { value: this.supplierDetail.email },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.supplierDetail,
                                "email",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("label", [_vm._v("Phone")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.supplierDetail.phone,
                              expression: "this.supplierDetail.phone"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "phone", disabled: "" },
                          domProps: { value: this.supplierDetail.phone },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.supplierDetail,
                                "phone",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br"),
                        _vm._v(" "),
                        _c("label", [_vm._v("Fax Number")]),
                        _vm._v(" "),
                        _c("input", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.supplierDetail.fax,
                              expression: "this.supplierDetail.fax"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { type: "text", name: "fax", disabled: "" },
                          domProps: { value: this.supplierDetail.fax },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.supplierDetail,
                                "fax",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "col-md-4" }, [
                        _c("label", [_vm._v("Address")]),
                        _vm._v(" "),
                        _c("textarea", {
                          directives: [
                            {
                              name: "model",
                              rawName: "v-model",
                              value: this.supplierDetail.address,
                              expression: "this.supplierDetail.address"
                            }
                          ],
                          staticClass: "form-control",
                          attrs: { disabled: "" },
                          domProps: { value: this.supplierDetail.address },
                          on: {
                            input: function($event) {
                              if ($event.target.composing) {
                                return
                              }
                              _vm.$set(
                                this.supplierDetail,
                                "address",
                                $event.target.value
                              )
                            }
                          }
                        }),
                        _vm._v(" "),
                        _c("br")
                      ])
                    ]),
                    _vm._v(" "),
                    _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "table-responsive" }, [
                          _c(
                            "table",
                            {
                              staticClass: "table table-bordered table-hovered",
                              staticStyle: { "font-size": "16px" }
                            },
                            [
                              _vm._m(1),
                              _vm._v(" "),
                              _c(
                                "tbody",
                                _vm._l(this.products, function(product, index) {
                                  return _c("tr", { key: index }, [
                                    _c("td", { attrs: { width: "3%" } }, [
                                      _vm._v(_vm._s(index + 1))
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      [
                                        _c("v-select", {
                                          staticStyle: { "font-size": "16px" },
                                          attrs: {
                                            options: _vm.items,
                                            label: "name",
                                            disabled: "",
                                            required: ""
                                          },
                                          on: {
                                            input: function($event) {
                                              return _vm.fetchItemDetails(
                                                $event,
                                                index
                                              )
                                            }
                                          },
                                          model: {
                                            value: _vm.selectedItem[index],
                                            callback: function($$v) {
                                              _vm.$set(
                                                _vm.selectedItem,
                                                index,
                                                $$v
                                              )
                                            },
                                            expression: "selectedItem[index]"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("br"),
                                        _vm._v(" "),
                                        _c("input", {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: product.description,
                                              expression: "product.description"
                                            }
                                          ],
                                          staticClass: "form-control m-input",
                                          staticStyle: { "font-size": "16px" },
                                          attrs: { type: "text", required: "" },
                                          domProps: {
                                            value: product.description
                                          },
                                          on: {
                                            input: function($event) {
                                              if ($event.target.composing) {
                                                return
                                              }
                                              _vm.$set(
                                                product,
                                                "description",
                                                $event.target.value
                                              )
                                            }
                                          }
                                        })
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("td", [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.uom,
                                            expression: "product.uom"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: { "font-size": "16px" },
                                        attrs: { min: "0", disabled: "" },
                                        domProps: { value: product.uom },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "uom",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _c("br")
                                    ]),
                                    _vm._v(" "),
                                    _c("td", { attrs: { width: "10%" } }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.purchaseQty,
                                            expression: "product.purchaseQty"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: {
                                          value: "0",
                                          disabled: "",
                                          required: ""
                                        },
                                        domProps: {
                                          value: product.purchaseQty
                                        },
                                        on: {
                                          keypress: function($event) {
                                            return _vm.isNumber($event)
                                          },
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "purchaseQty",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ]),
                                    _vm._v(" "),
                                    _c("td", { attrs: { width: "13%" } }, [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.confirmQty,
                                            expression: "product.confirmQty"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: {
                                          value: "0",
                                          type: "text",
                                          min: "0",
                                          required: ""
                                        },
                                        domProps: { value: product.confirmQty },
                                        on: {
                                          keypress: function($event) {
                                            return _vm.isNumber($event)
                                          },
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "confirmQty",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _c("br")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "td",
                                      { attrs: { width: "15%" } },
                                      [
                                        _c("v-select", {
                                          staticStyle: { "font-size": "16px" },
                                          attrs: {
                                            options: _vm.warehouses,
                                            label: "name",
                                            required: !_vm.selectedWarehouses[
                                              index
                                            ]
                                          },
                                          scopedSlots: _vm._u(
                                            [
                                              {
                                                key: "search",
                                                fn: function(ref) {
                                                  var attributes =
                                                    ref.attributes
                                                  var events = ref.events
                                                  return [
                                                    _c(
                                                      "input",
                                                      _vm._g(
                                                        _vm._b(
                                                          {
                                                            staticClass:
                                                              "vs__search",
                                                            attrs: {
                                                              required: !_vm
                                                                .selectedWarehouses[
                                                                index
                                                              ]
                                                            }
                                                          },
                                                          "input",
                                                          attributes,
                                                          false
                                                        ),
                                                        events
                                                      )
                                                    )
                                                  ]
                                                }
                                              }
                                            ],
                                            null,
                                            true
                                          ),
                                          model: {
                                            value:
                                              _vm.selectedWarehouses[index],
                                            callback: function($$v) {
                                              _vm.$set(
                                                _vm.selectedWarehouses,
                                                index,
                                                $$v
                                              )
                                            },
                                            expression:
                                              "selectedWarehouses[index]"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("br")
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c("td", { attrs: { width: "2%" } }, [
                                      _c(
                                        "button",
                                        {
                                          staticClass: "btn btn-xs btn-danger",
                                          on: {
                                            click: function($event) {
                                              $event.preventDefault()
                                              return _vm.deleteItem(index)
                                            }
                                          }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fa fa-trash"
                                          })
                                        ]
                                      )
                                    ])
                                  ])
                                }),
                                0
                              )
                            ]
                          )
                        ])
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "m-portlet__foot m-portlet__foot--fit" },
                      [
                        _c(
                          "div",
                          {
                            staticClass:
                              "m-form__actions m-form__actions--right"
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-primary btn-lg",
                                attrs: { type: "submit", disabled: _vm.loading }
                              },
                              [
                                _vm._v(
                                  _vm._s(
                                    this.loading ? "Please Wait..." : "Save"
                                  )
                                )
                              ]
                            )
                          ]
                        )
                      ]
                    )
                  ]
                ),
                _vm._v(" "),
                _vm.show_start
                  ? _c("date-picker", {
                      on: {
                        close: function($event) {
                          _vm.show_start = false
                        }
                      },
                      model: {
                        value: _vm.form.date,
                        callback: function($$v) {
                          _vm.$set(_vm.form, "date", $$v)
                        },
                        expression: "form.date"
                      }
                    })
                  : _vm._e()
              ],
              1
            )
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [_vm._v("#")]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "30%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\n                                                Item\n                                                "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                                UoM\n                                                "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                                Purchased Qty\n                                                "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "13%" }
          },
          [
            _vm._v(
              "\n                                                Confirm Qty\n                                                "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "15%" }
          },
          [
            _vm._v(
              "\n                                                Warehouse\n                                                "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center", width: "1%" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=template&id=18f205e5&":
/*!*************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue?vue&type=template&id=18f205e5& ***!
  \*************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.fetchLoading
      ? _c("div", { staticClass: "text-center" }, [_vm._m(0)])
      : _c(
          "div",
          [
            _vm.validationErrors
              ? _c("ValidationErrors", {
                  attrs: { errors: _vm.validationErrors }
                })
              : _vm._e(),
            _vm._v(" "),
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.handleSubmit($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _c("label", [_vm._v("Purchase Order ")]),
                      _vm._v(" "),
                      _c("input", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.purchase_order,
                            expression: "form.purchase_order"
                          }
                        ],
                        staticClass: "form-control",
                        class: { "is-invalid": _vm.validationErrors.title },
                        attrs: { type: "text", disabled: "" },
                        domProps: { value: _vm.form.purchase_order },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.form,
                              "purchase_order",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.validationErrors.purchase_order_id
                        ? _c("label", { staticClass: "text-danger" }, [
                            _vm._v(
                              _vm._s(_vm.validationErrors.purchase_order_id[0])
                            )
                          ])
                        : _vm._e()
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _c("label", [_vm._v("Supplier")]),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.supplier_id },
                          attrs: {
                            options: _vm.suppliers,
                            label: "name",
                            required: "",
                            disabled: ""
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedSupplier
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedSupplier,
                            callback: function($$v) {
                              _vm.selectedSupplier = $$v
                            },
                            expression: "selectedSupplier"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.supplier_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.supplier_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("h4", [_vm._v("Supplier Detail")]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Company Name")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.supplierDetail.company_name,
                          expression: "this.supplierDetail.company_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        name: "company_name",
                        disabled: ""
                      },
                      domProps: { value: this.supplierDetail.company_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.supplierDetail,
                            "company_name",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Email")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.supplierDetail.email,
                          expression: "this.supplierDetail.email"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "email", disabled: "" },
                      domProps: { value: this.supplierDetail.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.supplierDetail,
                            "email",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Phone")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.supplierDetail.phone,
                          expression: "this.supplierDetail.phone"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "phone", disabled: "" },
                      domProps: { value: this.supplierDetail.phone },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.supplierDetail,
                            "phone",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Fax Number")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.supplierDetail.fax,
                          expression: "this.supplierDetail.fax"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "fax", disabled: "" },
                      domProps: { value: this.supplierDetail.fax },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.supplierDetail,
                            "fax",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Address")]),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.supplierDetail.address,
                          expression: "this.supplierDetail.address"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { disabled: "" },
                      domProps: { value: this.supplierDetail.address },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.supplierDetail,
                            "address",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "table-responsive" }, [
                      _c(
                        "table",
                        {
                          staticClass: "table table-bordered table-hovered",
                          staticStyle: { "font-size": "9px" }
                        },
                        [
                          _vm._m(1),
                          _vm._v(" "),
                          _c(
                            "tbody",
                            _vm._l(this.products, function(product, index) {
                              return _c("tr", { key: index }, [
                                _c("td", { attrs: { width: "3%" } }, [
                                  _vm._v(_vm._s(index + 1))
                                ]),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  [
                                    _c("v-select", {
                                      staticStyle: { "font-size": "16px" },
                                      attrs: {
                                        options: _vm.items,
                                        label: "name",
                                        required: "",
                                        disabled: ""
                                      },
                                      on: {
                                        input: function($event) {
                                          return _vm.fetchItemDetails(
                                            $event,
                                            index
                                          )
                                        }
                                      },
                                      model: {
                                        value: _vm.selectedItem[index],
                                        callback: function($$v) {
                                          _vm.$set(_vm.selectedItem, index, $$v)
                                        },
                                        expression: "selectedItem[index]"
                                      }
                                    }),
                                    _c("br"),
                                    _vm._v(" "),
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: product.description,
                                          expression: "product.description"
                                        }
                                      ],
                                      staticClass: "form-control m-input",
                                      staticStyle: { "font-size": "16px" },
                                      attrs: { type: "text", required: "" },
                                      domProps: { value: product.description },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            product,
                                            "description",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("td", [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: product.uom,
                                          expression: "product.uom"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      staticStyle: { "font-size": "16px" },
                                      attrs: { required: "", disabled: "" },
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            product,
                                            "uom",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    _vm._l(_vm.uoms, function(item, i) {
                                      return _c(
                                        "option",
                                        {
                                          key: i,
                                          domProps: { value: item.id }
                                        },
                                        [_vm._v(_vm._s(item.name))]
                                      )
                                    }),
                                    0
                                  )
                                ]),
                                _vm._v(" "),
                                _c("td", { attrs: { width: "10%" } }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: product.purchaseQty,
                                        expression: "product.purchaseQty"
                                      }
                                    ],
                                    staticClass: "form-control m-input",
                                    staticStyle: {
                                      "font-size": "16px",
                                      "text-align": "right"
                                    },
                                    attrs: {
                                      value: "0",
                                      required: "",
                                      disabled: ""
                                    },
                                    domProps: { value: product.purchaseQty },
                                    on: {
                                      keypress: function($event) {
                                        return _vm.isNumber($event)
                                      },
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          product,
                                          "purchaseQty",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("td", { attrs: { width: "15%" } }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: product.confirmQty,
                                        expression: "product.confirmQty"
                                      }
                                    ],
                                    staticClass: "form-control m-input",
                                    staticStyle: {
                                      "font-size": "16px",
                                      "text-align": "right"
                                    },
                                    attrs: { value: "0", max: "100" },
                                    domProps: { value: product.confirmQty },
                                    on: {
                                      keypress: function($event) {
                                        return _vm.isNumber($event)
                                      },
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          product,
                                          "confirmQty",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _c("br")
                                ]),
                                _vm._v(" "),
                                _c("td", { attrs: { width: "15%" } }, [
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: product.warehouse,
                                          expression: "product.warehouse"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      staticStyle: { "font-size": "16px" },
                                      attrs: { required: "" },
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.$set(
                                            product,
                                            "warehouse",
                                            $event.target.multiple
                                              ? $$selectedVal
                                              : $$selectedVal[0]
                                          )
                                        }
                                      }
                                    },
                                    _vm._l(_vm.warehouses, function(item, i) {
                                      return _c(
                                        "option",
                                        {
                                          key: i,
                                          domProps: { value: item.id }
                                        },
                                        [_vm._v(_vm._s(item.name))]
                                      )
                                    }),
                                    0
                                  )
                                ]),
                                _vm._v(" "),
                                _c("td", { attrs: { width: "2%" } }, [
                                  _c(
                                    "button",
                                    {
                                      staticClass: "btn btn-xs btn-danger",
                                      on: {
                                        click: function($event) {
                                          $event.preventDefault()
                                          return _vm.deleteItem(
                                            index,
                                            product.id
                                          )
                                        }
                                      }
                                    },
                                    [_c("i", { staticClass: "fa fa-trash" })]
                                  )
                                ])
                              ])
                            }),
                            0
                          )
                        ]
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "m-portlet__foot m-portlet__foot--fit" },
                  [
                    _c(
                      "div",
                      { staticClass: "m-form__actions m-form__actions--right" },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary btn-lg",
                            attrs: { type: "submit", disabled: _vm.loading }
                          },
                          [
                            _vm._v(
                              _vm._s(this.loading ? "Please Wait..." : "Update")
                            )
                          ]
                        )
                      ]
                    )
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _vm.show_start
              ? _c("date-picker", {
                  on: {
                    close: function($event) {
                      _vm.show_start = false
                    }
                  },
                  model: {
                    value: _vm.form.date,
                    callback: function($$v) {
                      _vm.$set(_vm.form, "date", $$v)
                    },
                    expression: "form.date"
                  }
                })
              : _vm._e()
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "30%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\n                                        Item\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                        UoM\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                        Purchased Qty\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\n                                        Confirm Qty\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { "font-size": "16px" },
            attrs: { align: "center", width: "10%" }
          },
          [
            _vm._v(
              "\n                                        Warehouse\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center", width: "5%" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=template&id=5ec8cc5e&":
/*!*********************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue?vue&type=template&id=5ec8cc5e& ***!
  \*********************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.validationErrors
        ? _c("ValidationErrors", { attrs: { errors: _vm.validationErrors } })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.handleSubmit($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", [_vm._v("Origin Warehouse")]),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.origin_id },
                    attrs: { options: _vm.origins, label: "title" },
                    on: {
                      input: function($event) {
                        return _vm.showProduct($event)
                      }
                    },
                    model: {
                      value: _vm.selectedOrigin,
                      callback: function($$v) {
                        _vm.selectedOrigin = $$v
                      },
                      expression: "selectedOrigin"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.origin_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.origin_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", [_vm._v("Destination Warehouse")]),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.destination_id
                    },
                    attrs: { options: _vm.destinations, label: "title" },
                    model: {
                      value: _vm.selectedDestination,
                      callback: function($$v) {
                        _vm.selectedDestination = $$v
                      },
                      expression: "selectedDestination"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.destination_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.destination_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Date")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.date,
                      expression: "form.date"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.date },
                  attrs: { type: "text", placeholder: "yyyy-mm-dd" },
                  domProps: { value: _vm.form.date },
                  on: {
                    click: function($event) {
                      _vm.show_start = true
                    },
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "date", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.date
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.date[0]))
                    ])
                  : _vm._e()
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Remarks")]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.description,
                      expression: "form.description"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.description },
                  staticStyle: { height: "100px" },
                  attrs: { cols: "40" },
                  domProps: { value: _vm.form.description },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "description", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.description
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.currency_id[0]))
                    ])
                  : _vm._e()
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass: "table table-bordered table-hovered",
                    staticStyle: { "font-size": "9px" }
                  },
                  [
                    _vm._m(0),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(this.products, function(product, index) {
                        return _c("tr", { key: index }, [
                          _c("td", { staticStyle: { "font-size": "16px" } }, [
                            _vm._v(_vm._s(index + 1))
                          ]),
                          _vm._v(" "),
                          _c(
                            "td",
                            { staticStyle: { "font-size": "16px" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  options: _vm.items,
                                  label: "name",
                                  required: ""
                                },
                                on: {
                                  input: function($event) {
                                    return _vm.fetchItemDetails($event, index)
                                  }
                                },
                                model: {
                                  value: _vm.selectedItem[index],
                                  callback: function($$v) {
                                    _vm.$set(_vm.selectedItem, index, $$v)
                                  },
                                  expression: "selectedItem[index]"
                                }
                              }),
                              _c("br"),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: product.description,
                                    expression: "product.description"
                                  }
                                ],
                                staticClass: "form-control m-input",
                                staticStyle: { "font-size": "16px" },
                                attrs: { type: "text", required: "" },
                                domProps: { value: product.description },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      product,
                                      "description",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { width: "15%" },
                              attrs: { width: "10%" }
                            },
                            [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.uom,
                                      expression: "product.uom"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { "font-size": "16px" },
                                  attrs: { required: "" },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        product,
                                        "uom",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                _vm._l(_vm.uoms, function(item, i) {
                                  return _c(
                                    "option",
                                    { key: i, domProps: { value: item.id } },
                                    [_vm._v(_vm._s(item.name))]
                                  )
                                }),
                                0
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { width: "15%" },
                              attrs: { width: "8%" }
                            },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: product.qty,
                                    expression: "product.qty"
                                  }
                                ],
                                staticClass: "form-control m-input",
                                staticStyle: {
                                  "font-size": "16px",
                                  "text-align": "right"
                                },
                                attrs: {
                                  value: "0",
                                  required: "",
                                  disabled: ""
                                },
                                domProps: { value: product.qty },
                                on: {
                                  keypress: function($event) {
                                    return _vm.isNumber($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      product,
                                      "qty",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { width: "15%" },
                              attrs: { width: "8%" }
                            },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: product.quantity,
                                    expression: "product.quantity"
                                  }
                                ],
                                staticClass: "form-control m-input",
                                staticStyle: {
                                  "font-size": "16px",
                                  "text-align": "right"
                                },
                                attrs: { value: "0", required: "" },
                                domProps: { value: product.quantity },
                                on: {
                                  keypress: function($event) {
                                    return _vm.isNumber($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      product,
                                      "quantity",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("td", [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-xs btn-danger",
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.deleteItem(index)
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fa fa-trash" })]
                            )
                          ])
                        ])
                      }),
                      0
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "m-portlet__foot m-portlet__foot--fit" }, [
            _c(
              "div",
              { staticClass: "m-form__actions m-form__actions--right" },
              [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-lg",
                    attrs: { type: "submit", disabled: _vm.loading }
                  },
                  [_vm._v(_vm._s(this.loading ? "Please Wait..." : "Save"))]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _vm.show_start
            ? _c("date-picker", {
                on: {
                  close: function($event) {
                    _vm.show_start = false
                  }
                },
                model: {
                  value: _vm.form.date,
                  callback: function($$v) {
                    _vm.$set(_vm.form, "date", $$v)
                  },
                  expression: "form.date"
                }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "20%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    Item\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "10%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    UoM\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "10%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    In Hand\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Qty\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=template&id=c87bc9c2&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue?vue&type=template&id=c87bc9c2& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.validationErrors
        ? _c("ValidationErrors", { attrs: { errors: _vm.validationErrors } })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.handleSubmit($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", [_vm._v("Origin Warehouse")]),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.origin_id },
                    attrs: {
                      options: _vm.origins,
                      label: "name",
                      disabled: ""
                    },
                    on: {
                      input: function($event) {
                        return _vm.showProduct($event)
                      }
                    },
                    model: {
                      value: _vm.selectedOrigin,
                      callback: function($$v) {
                        _vm.selectedOrigin = $$v
                      },
                      expression: "selectedOrigin"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.origin_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.origin_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _c("label", [_vm._v("Destination Warehouse")]),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.destination_id
                    },
                    attrs: {
                      options: _vm.destinations,
                      label: "name",
                      disabled: ""
                    },
                    model: {
                      value: _vm.selectedDestination,
                      callback: function($$v) {
                        _vm.selectedDestination = $$v
                      },
                      expression: "selectedDestination"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.destination_id
                    ? _c("label", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.destination_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Date")]),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.date,
                      expression: "form.date"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.date },
                  attrs: { type: "text", placeholder: "yyyy-mm-dd" },
                  domProps: { value: _vm.form.date },
                  on: {
                    click: function($event) {
                      _vm.show_start = true
                    },
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "date", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.date
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.date[0]))
                    ])
                  : _vm._e()
              ])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" })
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _c("label", [_vm._v("Remarks")]),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.description,
                      expression: "form.description"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.description },
                  staticStyle: { height: "100px" },
                  attrs: { cols: "40" },
                  domProps: { value: _vm.form.description },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "description", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.description
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.currency_id[0]))
                    ])
                  : _vm._e()
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass: "table table-bordered table-hovered",
                    staticStyle: { "font-size": "9px" }
                  },
                  [
                    _vm._m(0),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(this.products, function(product, index) {
                        return _c("tr", { key: index }, [
                          _c("td", { staticStyle: { "font-size": "16px" } }, [
                            _vm._v(_vm._s(index + 1))
                          ]),
                          _vm._v(" "),
                          _c(
                            "td",
                            { staticStyle: { "font-size": "16px" } },
                            [
                              _c("v-select", {
                                attrs: {
                                  options: _vm.items,
                                  label: "name",
                                  required: "",
                                  disabled: ""
                                },
                                on: {
                                  input: function($event) {
                                    return _vm.fetchItemDetails($event, index)
                                  }
                                },
                                model: {
                                  value: _vm.selectedItem[index],
                                  callback: function($$v) {
                                    _vm.$set(_vm.selectedItem, index, $$v)
                                  },
                                  expression: "selectedItem[index]"
                                }
                              }),
                              _c("br"),
                              _vm._v(" "),
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: product.description,
                                    expression: "product.description"
                                  }
                                ],
                                staticClass: "form-control m-input",
                                staticStyle: { "font-size": "16px" },
                                attrs: { type: "text", required: "" },
                                domProps: { value: product.description },
                                on: {
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      product,
                                      "description",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { width: "15%" },
                              attrs: { width: "10%" }
                            },
                            [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.uom,
                                      expression: "product.uom"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { "font-size": "16px" },
                                  attrs: { required: "" },
                                  on: {
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        product,
                                        "uom",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                _vm._l(_vm.uoms, function(item, i) {
                                  return _c(
                                    "option",
                                    { key: i, domProps: { value: item.id } },
                                    [_vm._v(_vm._s(item.name))]
                                  )
                                }),
                                0
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { width: "15%" },
                              attrs: { width: "8%" }
                            },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: product.qty,
                                    expression: "product.qty"
                                  }
                                ],
                                staticClass: "form-control m-input",
                                staticStyle: {
                                  "font-size": "16px",
                                  "text-align": "right"
                                },
                                attrs: {
                                  value: "0",
                                  required: "",
                                  disabled: ""
                                },
                                domProps: { value: product.qty },
                                on: {
                                  keypress: function($event) {
                                    return _vm.isNumber($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      product,
                                      "qty",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { width: "15%" },
                              attrs: { width: "8%" }
                            },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: product.quantity,
                                    expression: "product.quantity"
                                  }
                                ],
                                staticClass: "form-control m-input",
                                staticStyle: {
                                  "font-size": "16px",
                                  "text-align": "right"
                                },
                                attrs: { value: "0", required: "" },
                                domProps: { value: product.quantity },
                                on: {
                                  keypress: function($event) {
                                    return _vm.isNumber($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      product,
                                      "quantity",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("td", [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-xs btn-danger",
                                on: {
                                  click: function($event) {
                                    $event.preventDefault()
                                    return _vm.deleteItem(index)
                                  }
                                }
                              },
                              [_c("i", { staticClass: "fa fa-trash" })]
                            )
                          ])
                        ])
                      }),
                      0
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "m-portlet__foot m-portlet__foot--fit" }, [
            _c(
              "div",
              { staticClass: "m-form__actions m-form__actions--right" },
              [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary btn-lg",
                    attrs: { type: "submit", disabled: _vm.loading }
                  },
                  [_vm._v(_vm._s(this.loading ? "Please Wait..." : "Update"))]
                )
              ]
            )
          ]),
          _vm._v(" "),
          _vm.show_start
            ? _c("date-picker", {
                on: {
                  close: function($event) {
                    _vm.show_start = false
                  }
                },
                model: {
                  value: _vm.form.date,
                  callback: function($$v) {
                    _vm.$set(_vm.form, "date", $$v)
                  },
                  expression: "form.date"
                }
              })
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "20%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    Item\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "10%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    UoM\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "10%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    In Hand\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Qty\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=template&id=359fa112&":
/*!*********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue?vue&type=template&id=359fa112& ***!
  \*********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _vm.validationErrors
        ? _c("ValidationErrors", { attrs: { errors: _vm.validationErrors } })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "form",
        {
          on: {
            submit: function($event) {
              $event.preventDefault()
              return _vm.handleSubmit($event)
            }
          }
        },
        [
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(0),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.customer_id },
                    attrs: { options: _vm.customers, label: "name" },
                    on: {
                      input: function($event) {
                        _vm.fetchItems($event)
                        _vm.fetchCustomersDetail($event)
                      }
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "search",
                        fn: function(ref) {
                          var attributes = ref.attributes
                          var events = ref.events
                          return [
                            _c(
                              "input",
                              _vm._g(
                                _vm._b(
                                  {
                                    staticClass: "vs__search",
                                    attrs: { required: !_vm.selectedCustomer }
                                  },
                                  "input",
                                  attributes,
                                  false
                                ),
                                events
                              )
                            )
                          ]
                        }
                      }
                    ]),
                    model: {
                      value: _vm.selectedCustomer,
                      callback: function($$v) {
                        _vm.selectedCustomer = $$v
                      },
                      expression: "selectedCustomer"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.customer_id
                    ? _c("div", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.customer_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "button",
                {
                  staticClass: "btn btn-info pull-right",
                  staticStyle: { "margin-bottom": "20px" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.showCustomer($event)
                    }
                  }
                },
                [_vm._v("New Customer")]
              ),
              _vm._v(" "),
              _c("br")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(1),
                  _vm._v(" "),
                  _c("v-select", {
                    class: { "is-invalid": _vm.validationErrors.salesman_id },
                    attrs: { options: _vm.salesmans, label: "name" },
                    scopedSlots: _vm._u([
                      {
                        key: "search",
                        fn: function(ref) {
                          var attributes = ref.attributes
                          var events = ref.events
                          return [
                            _c(
                              "input",
                              _vm._b(
                                {
                                  staticClass: "vs__search",
                                  attrs: { required: !_vm.selectedSalesman }
                                },
                                "input",
                                attributes,
                                false
                              )
                            )
                          ]
                        }
                      }
                    ]),
                    model: {
                      value: _vm.selectedSalesman,
                      callback: function($$v) {
                        _vm.selectedSalesman = $$v
                      },
                      expression: "selectedSalesman"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.salesman_id
                    ? _c("div", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.salesman_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ])
          ]),
          _vm._v(" "),
          _vm.show_customer_detail
            ? _c("div", [
                _c("h4", [_vm._v("Customer Detail")]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _vm.show_customer_detail
                      ? _c("div", [
                          _c("label", [_vm._v("Company Name")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: this.customerDetail.company_name,
                                expression: "this.customerDetail.company_name"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              name: "company_name",
                              disabled: ""
                            },
                            domProps: {
                              value: this.customerDetail.company_name
                            },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  this.customerDetail,
                                  "company_name",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c("label", [_vm._v("Contact Person ")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: this.customerDetail.pic_1,
                                expression: "this.customerDetail.pic_1"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              name: "pic_1",
                              disabled: ""
                            },
                            domProps: { value: this.customerDetail.pic_1 },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  this.customerDetail,
                                  "pic_1",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("br")
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _vm.show_customer_detail
                      ? _c("div", [
                          _c("label", [_vm._v("Phone")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: this.customerDetail.phone,
                                expression: "this.customerDetail.phone"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              name: "phone",
                              disabled: ""
                            },
                            domProps: { value: this.customerDetail.phone },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  this.customerDetail,
                                  "phone",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("br"),
                          _vm._v(" "),
                          _c("label", [_vm._v("Email")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: this.customerDetail.email,
                                expression: "this.customerDetail.email"
                              }
                            ],
                            staticClass: "form-control",
                            attrs: {
                              type: "text",
                              name: "email",
                              disabled: ""
                            },
                            domProps: { value: this.customerDetail.email },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  this.customerDetail,
                                  "email",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c("br")
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _vm.show_customer_detail
                      ? _c(
                          "div",
                          [
                            _c("label", [_vm._v("Fax Number")]),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: this.customerDetail.fax,
                                  expression: "this.customerDetail.fax"
                                }
                              ],
                              staticClass: "form-control",
                              attrs: {
                                type: "text",
                                name: "fax",
                                disabled: ""
                              },
                              domProps: { value: this.customerDetail.fax },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.$set(
                                    this.customerDetail,
                                    "fax",
                                    $event.target.value
                                  )
                                }
                              }
                            }),
                            _vm._v(" "),
                            _c("br"),
                            _vm._v(" "),
                            _c("label", [_vm._v("Credit Limit")]),
                            _vm._v(" "),
                            _c("money", {
                              staticClass: "form-control",
                              staticStyle: { "text-align": "right" },
                              attrs: {
                                type: "text",
                                name: "credit_limit",
                                disabled: ""
                              },
                              model: {
                                value: this.customerDetail.credit_limit,
                                callback: function($$v) {
                                  _vm.$set(
                                    this.customerDetail,
                                    "credit_limit",
                                    $$v
                                  )
                                },
                                expression: "this.customerDetail.credit_limit"
                              }
                            }),
                            _vm._v(" "),
                            _c("br")
                          ],
                          1
                        )
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  this.customer_addrees.length <= 100
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _vm.show_customer_detail
                          ? _c("div", [
                              _c("label", [_vm._v("Address")]),
                              _vm._v(" "),
                              _c("div", { staticStyle: { height: "auto" } }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: this.customer_addrees,
                                      expression: "this.customer_addrees"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { height: "80px" },
                                  attrs: { disabled: "" },
                                  domProps: { value: this.customer_addrees },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        this,
                                        "customer_addrees",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("br")
                            ])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  this.customer_addrees.length <= 200 &&
                  this.customer_addrees.length >= 100
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _vm.show_customer_detail
                          ? _c("div", [
                              _c("label", [_vm._v("Address")]),
                              _vm._v(" "),
                              _c("div", { staticStyle: { height: "auto" } }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: this.customer_addrees,
                                      expression: "this.customer_addrees"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { height: "100px" },
                                  attrs: { disabled: "" },
                                  domProps: { value: this.customer_addrees },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        this,
                                        "customer_addrees",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("br")
                            ])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  this.customer_addrees.length <= 350 &&
                  this.customer_addrees.length >= 200
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _vm.show_customer_detail
                          ? _c("div", [
                              _c("label", [_vm._v("Address")]),
                              _vm._v(" "),
                              _c("div", { staticStyle: { height: "auto" } }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: this.customer_addrees,
                                      expression: "this.customer_addrees"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { height: "150px" },
                                  attrs: { disabled: "" },
                                  domProps: { value: this.customer_addrees },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        this,
                                        "customer_addrees",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("br")
                            ])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  this.customer_addrees.length <= 650 &&
                  this.customer_addrees.length >= 350
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _vm.show_customer_detail
                          ? _c("div", [
                              _c("label", [_vm._v("Address")]),
                              _vm._v(" "),
                              _c("div", { staticStyle: { height: "auto" } }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: this.customer_addrees,
                                      expression: "this.customer_addrees"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { height: "200px" },
                                  attrs: { disabled: "" },
                                  domProps: { value: this.customer_addrees },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        this,
                                        "customer_addrees",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("br")
                            ])
                          : _vm._e()
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  this.customer_addrees.length >= 650
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _vm.show_customer_detail
                          ? _c("div", [
                              _c("label", [_vm._v("Address")]),
                              _vm._v(" "),
                              _c("div", { staticStyle: { height: "auto" } }, [
                                _c("textarea", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: this.customer_addrees,
                                      expression: "this.customer_addrees"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { height: "280px" },
                                  attrs: { disabled: "" },
                                  domProps: { value: this.customer_addrees },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        this,
                                        "customer_addrees",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ]),
                              _vm._v(" "),
                              _c("br")
                            ])
                          : _vm._e()
                      ])
                    : _vm._e()
                ])
              ])
            : _vm._e(),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-6" }, [
              _c(
                "div",
                { staticClass: "form-group" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _c("v-select", {
                    class: {
                      "is-invalid": _vm.validationErrors.payment_term_id
                    },
                    attrs: { options: _vm.paymentTerms, label: "name" },
                    model: {
                      value: _vm.selectedPaymentTerm,
                      callback: function($$v) {
                        _vm.selectedPaymentTerm = $$v
                      },
                      expression: "selectedPaymentTerm"
                    }
                  }),
                  _vm._v(" "),
                  _vm.validationErrors.payment_term_id
                    ? _c("div", { staticClass: "text-danger" }, [
                        _vm._v(_vm._s(_vm.validationErrors.payment_term_id[0]))
                      ])
                    : _vm._e()
                ],
                1
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "col-md-6" }, [
              _c("div", { staticClass: "form-group" }, [
                _vm._m(3),
                _vm._v(" "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.currencyName,
                      expression: "currencyName"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text", readonly: "" },
                  domProps: { value: _vm.currencyName },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.currencyName = $event.target.value
                    }
                  }
                })
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "form-group" }, [
                _vm._m(4),
                _vm._v(" "),
                _c("textarea", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.form.description,
                      expression: "form.description"
                    }
                  ],
                  staticClass: "form-control",
                  class: { "is-invalid": _vm.validationErrors.description },
                  staticStyle: { height: "100px" },
                  attrs: { cols: "40" },
                  domProps: { value: _vm.form.description },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.$set(_vm.form, "description", $event.target.value)
                    }
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.description
                  ? _c("label", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.description[0]))
                    ])
                  : _vm._e()
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "row" }, [
            _c("div", { staticClass: "col-md-12" }, [
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass: "table table-bordered table-hovered",
                    staticStyle: { "font-size": "9px" }
                  },
                  [
                    _vm._m(5),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      [
                        _vm._l(this.products, function(product, index) {
                          return _c("tr", { key: index }, [
                            _c("td", { staticStyle: { "font-size": "16px" } }, [
                              _vm._v(_vm._s(index + 1))
                            ]),
                            _vm._v(" "),
                            _c("td", { staticStyle: { "font-size": "16px" } }, [
                              _vm.selectedItem[index]
                                ? _c("i", {
                                    staticClass: "fa fa-info-circle",
                                    attrs: {
                                      "product-id": product.item,
                                      id: _vm.selectedItem[index]
                                    },
                                    on: {
                                      click: function($event) {
                                        $event.preventDefault()
                                        return _vm.showPriceHistory(
                                          $event,
                                          product,
                                          index
                                        )
                                      }
                                    }
                                  })
                                : _vm._e()
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c("v-select", {
                                  staticStyle: { "font-size": "16px" },
                                  attrs: {
                                    options: _vm.items,
                                    label: "name",
                                    required: ""
                                  },
                                  on: {
                                    input: function($event) {
                                      return _vm.fetchItemDetails($event, index)
                                    }
                                  },
                                  scopedSlots: _vm._u(
                                    [
                                      {
                                        key: "search",
                                        fn: function(ref) {
                                          var attributes = ref.attributes
                                          var events = ref.events
                                          return [
                                            _c(
                                              "input",
                                              _vm._g(
                                                _vm._b(
                                                  {
                                                    staticClass: "vs__search",
                                                    attrs: {
                                                      required: !_vm
                                                        .selectedItem[index]
                                                    }
                                                  },
                                                  "input",
                                                  attributes,
                                                  false
                                                ),
                                                events
                                              )
                                            )
                                          ]
                                        }
                                      }
                                    ],
                                    null,
                                    true
                                  ),
                                  model: {
                                    value: _vm.selectedItem[index],
                                    callback: function($$v) {
                                      _vm.$set(_vm.selectedItem, index, $$v)
                                    },
                                    expression: "selectedItem[index]"
                                  }
                                }),
                                _vm._v(" "),
                                _c("br"),
                                _vm._v(" "),
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.description,
                                      expression: "product.description"
                                    }
                                  ],
                                  staticClass: "form-control m-input",
                                  staticStyle: { "font-size": "16px" },
                                  attrs: {
                                    type: "text",
                                    placeholder: "Product description"
                                  },
                                  domProps: { value: product.description },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        product,
                                        "description",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c("td", { attrs: { width: "10%" } }, [
                              _c(
                                "select",
                                {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.uom,
                                      expression: "product.uom"
                                    }
                                  ],
                                  staticClass: "form-control",
                                  staticStyle: { "font-size": "16px" },
                                  attrs: { required: "" },
                                  on: {
                                    input: function($event) {
                                      return _vm.fetchUomDetails(
                                        product.uom,
                                        index
                                      )
                                    },
                                    change: function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.$set(
                                        product,
                                        "uom",
                                        $event.target.multiple
                                          ? $$selectedVal
                                          : $$selectedVal[0]
                                      )
                                    }
                                  }
                                },
                                _vm._l(_vm.uoms, function(item, i) {
                                  return _c(
                                    "option",
                                    { key: i, domProps: { value: item.id } },
                                    [_vm._v(_vm._s(item.name))]
                                  )
                                }),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "15%" } },
                              [
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { value: "0", required: "" },
                                  model: {
                                    value: product.qty,
                                    callback: function($$v) {
                                      _vm.$set(product, "qty", $$v)
                                    },
                                    expression: "product.qty"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              [
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { required: "" },
                                  model: {
                                    value: product.price,
                                    callback: function($$v) {
                                      _vm.$set(product, "price", $$v)
                                    },
                                    expression: "product.price"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              { attrs: { width: "14%" } },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: product.discountRate,
                                      expression: "product.discountRate"
                                    }
                                  ],
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: {
                                    type: "number",
                                    max: "100",
                                    min: "0",
                                    value: "0",
                                    placeholder: "%"
                                  },
                                  domProps: { value: product.discountRate },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        product,
                                        "discountRate",
                                        $event.target.value
                                      )
                                    }
                                  }
                                }),
                                _c("br"),
                                _vm._v(" "),
                                _c("money", {
                                  staticClass: "form-control m-input",
                                  staticStyle: {
                                    "font-size": "16px",
                                    "text-align": "right"
                                  },
                                  attrs: { placeholder: "IDR" },
                                  model: {
                                    value: product.discountAmount,
                                    callback: function($$v) {
                                      _vm.$set(product, "discountAmount", $$v)
                                    },
                                    expression: "product.discountAmount"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: { "font-size": "16px" },
                                attrs: { width: "13%", align: "right" }
                              },
                              [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(
                                      product.qty && product.price
                                        ? _vm.formatPrice(
                                            product.price * product.qty -
                                              (product.discountAmount
                                                ? product.discountAmount
                                                : 0) -
                                              (product.discountRate
                                                ? (product.discountRate / 100) *
                                                  product.price
                                                : 0) *
                                                product.qty
                                          )
                                        : 0
                                    ) +
                                    "\r\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td", [
                              _c(
                                "button",
                                {
                                  staticClass: "btn btn-xs btn-danger",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.deleteItem(index)
                                    }
                                  }
                                },
                                [_c("i", { staticClass: "fa fa-trash" })]
                              )
                            ])
                          ])
                        }),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            { attrs: { colspan: "3", align: "center" } },
                            [
                              _c(
                                "button",
                                {
                                  staticClass:
                                    "btn btn-info pull-right btn-block",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.show($event)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\r\n                                        + Add New Product\r\n                                    "
                                  )
                                ]
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            { attrs: { colspan: "6", align: "center" } },
                            [
                              _c(
                                "button",
                                {
                                  staticClass:
                                    "btn btn-success pull-right btn-block",
                                  on: {
                                    click: function($event) {
                                      $event.preventDefault()
                                      return _vm.addItem($event)
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\r\n                                        + Add New Row\r\n                                    "
                                  )
                                ]
                              )
                            ]
                          )
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "16px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "7", align: "right" }
                            },
                            [_vm._v("Subtotal Exclude Discount All Items")]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { "font-size": "16px" },
                              attrs: { align: "right" }
                            },
                            [_vm._v(_vm._s(_vm.formatPrice(_vm.getTotal)))]
                          ),
                          _vm._v(" "),
                          _c("td", { staticStyle: { "font-size": "16px" } })
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "16px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "7", align: "right" }
                            },
                            [_vm._v("Discount All Items")]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { "font-size": "16px" },
                              attrs: { align: "right" }
                            },
                            [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.form.discountAllItem,
                                    expression: "form.discountAllItem"
                                  }
                                ],
                                staticClass: "form-control",
                                class: {
                                  "is-invalid":
                                    _vm.validationErrors.discountAllItem
                                },
                                staticStyle: { "text-align": "right" },
                                attrs: { type: "text" },
                                domProps: { value: _vm.form.discountAllItem },
                                on: {
                                  keypress: function($event) {
                                    return _vm.isNumber($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.$set(
                                      _vm.form,
                                      "discountAllItem",
                                      $event.target.value
                                    )
                                  }
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c(
                            "td",
                            {
                              staticStyle: {
                                "font-size": "16px",
                                "font-weight": "bold"
                              },
                              attrs: { colspan: "7", align: "right" }
                            },
                            [_vm._v("Sub Total")]
                          ),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { "font-size": "16px" },
                              attrs: { align: "right" }
                            },
                            [
                              _vm._v(
                                "\r\n                                    " +
                                  _vm._s(_vm.formatPrice(_vm.subTotal)) +
                                  "\r\n                                "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c("tr", [
                          _c("td", {
                            staticStyle: {
                              "font-size": "13px",
                              "font-weight": "bold"
                            },
                            attrs: { colspan: "6", align: "right" }
                          }),
                          _vm._v(" "),
                          _c("td", { attrs: { align: "right" } }, [
                            _c(
                              "div",
                              {
                                staticClass: "form-group",
                                staticStyle: { width: "100px" }
                              },
                              [
                                _c(
                                  "label",
                                  {
                                    staticClass: "input-group-append",
                                    staticStyle: {
                                      "font-size": "13px",
                                      "font-weight": "bold",
                                      "margin-right": "5px"
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\r\n                                            GST\r\n                                        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass: "input-group colorpickerinput"
                                  },
                                  [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.tax,
                                          expression: "form.tax"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      attrs: { type: "text", disabled: "" },
                                      domProps: { value: _vm.form.tax },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.form,
                                            "tax",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _vm._m(6)
                                  ]
                                )
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "td",
                            {
                              staticStyle: { "font-size": "16px" },
                              attrs: { align: "right" }
                            },
                            [
                              _vm._v(
                                "\r\n                                    " +
                                  _vm._s(_vm.formatPrice(_vm.getGst)) +
                                  "\r\n                                "
                              )
                            ]
                          ),
                          _vm._v(" "),
                          _c("td")
                        ]),
                        _vm._v(" "),
                        _c(
                          "tr",
                          {
                            staticStyle: {
                              "font-size": "16px",
                              "font-weight": "bold"
                            }
                          },
                          [
                            _c(
                              "td",
                              { attrs: { colspan: "7", align: "right" } },
                              [_vm._v("Total")]
                            ),
                            _vm._v(" "),
                            _c(
                              "td",
                              {
                                staticStyle: { color: "#5b35c4" },
                                attrs: { align: "right" }
                              },
                              [
                                _vm._v(
                                  "\r\n                                    " +
                                    _vm._s(_vm.formatPrice(_vm.total)) +
                                    "\r\n                                "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c("td")
                          ]
                        )
                      ],
                      2
                    )
                  ]
                )
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "m-portlet_foot m-portlet_foot--fit" }, [
            _c("div", { staticClass: "m-form_actions m-form_actions--right" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-primary btn-lg",
                  attrs: { type: "submit", disabled: _vm.loading }
                },
                [_vm._v(_vm._s(this.loading ? "Please Wait..." : "Save"))]
              )
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _c("Product", { on: { messageFromChild: _vm.childMessageReceived } }),
      _vm._v(" "),
      _c("Customer", {
        on: { messageFromChild: _vm.childMessageReceivedCustomer }
      }),
      _vm._v(" "),
      _c("price-history", {
        attrs: {
          id: this.product_id,
          billing: this.billing,
          pr_history: this.pr_history
        }
      })
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Customer "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Salesman "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Payment Term "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Currency "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Remark "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [_vm._v("#")]
        ),
        _vm._v(" "),
        _c("th", {
          staticStyle: { "font-size": "16px" },
          attrs: { align: "center" }
        }),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "20%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    Item\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "12%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    UoM\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          {
            staticStyle: { width: "12%", "font-size": "16px" },
            attrs: { align: "center" }
          },
          [
            _vm._v(
              "\r\n                                    Qty\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Price\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v("\r\n                                    Dsc (%) "),
            _c("br"),
            _vm._v(
              "\r\n                                    Dsc Amount\r\n                                "
            )
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { "font-size": "16px" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                    Sub Total\r\n                                    "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [_c("i", [_vm._v("%")])])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=template&id=65505774&":
/*!***********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Customer.vue?vue&type=template&id=65505774& ***!
  \***********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "modal",
    {
      attrs: {
        name: "customer-modal",
        resizable: _vm.resizable,
        adaptive: _vm.adaptive,
        height: "auto",
        scrollable: true
      }
    },
    [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "form-group" }, [
          _c("h4", { staticStyle: { "text-align": "center" } }, [
            _vm._v("Add New Customer")
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [
                _vm._v("Company Name "),
                _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.company_name,
                    expression: "form.company_name"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.company_name },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.company_name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "company_name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.company_name
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.company_name[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", [
                  _vm._v("Price Category "),
                  _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
                ]),
                _vm._v(" "),
                _c("v-select", {
                  class: {
                    "is-invalid": _vm.validationErrors.price_category_id
                  },
                  attrs: { options: _vm.price_category, label: "name" },
                  model: {
                    value: _vm.selectedPriceCategory,
                    callback: function($$v) {
                      _vm.selectedPriceCategory = $$v
                    },
                    expression: "selectedPriceCategory"
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.price_category_id
                  ? _c("div", { staticClass: "text-danger" }, [
                      _vm._v(_vm._s(_vm.validationErrors.price_category_id[0]))
                    ])
                  : _vm._e()
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Address")]),
              _vm._v(" "),
              _c("textarea", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.address,
                    expression: "form.address"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.address },
                attrs: { required: "" },
                domProps: { value: _vm.form.address },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "address", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.address
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.address[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Phone")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.phone,
                    expression: "form.phone"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.phone },
                attrs: { type: "number", required: "" },
                domProps: { value: _vm.form.phone },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "phone", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.phone
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.phone[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Fax")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.fax,
                    expression: "form.fax"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.fax },
                attrs: { type: "number", required: "" },
                domProps: { value: _vm.form.fax },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "fax", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.fax
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.fax[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Contact Person")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.pic_1,
                    expression: "form.pic_1"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.pic_1 },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.pic_1 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "pic_1", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.pic_1
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.pic_1[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Contact Person Phone")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.pic_phone_1,
                    expression: "form.pic_phone_1"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.pic_phone_1 },
                attrs: { type: "number", required: "" },
                domProps: { value: _vm.form.pic_phone_1 },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "pic_phone_1", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.pic_phone_1
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.pic_phone_1[0]))
                  ])
                : _vm._e()
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-6" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Credit Limit")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.credit_limit,
                    expression: "form.credit_limit"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.credit_limit },
                attrs: { type: "number", required: "" },
                domProps: { value: _vm.form.credit_limit },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "credit_limit", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.credit_limit
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.credit_limit[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-info pull-right",
                  staticStyle: { "margin-bottom": "20px" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.onClickButton($event)
                    }
                  }
                },
                [_vm._v("Save")]
              )
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=template&id=de1e02c0&":
/*!*******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue?vue&type=template&id=de1e02c0& ***!
  \*******************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.fetchLoading
      ? _c("div", { staticClass: "text-center" }, [_vm._m(0)])
      : _c(
          "div",
          [
            _vm.validationErrors
              ? _c("ValidationErrors", {
                  attrs: { errors: _vm.validationErrors }
                })
              : _vm._e(),
            _vm._v(" "),
            _c(
              "form",
              {
                on: {
                  submit: function($event) {
                    $event.preventDefault()
                    return _vm.handleSubmit($event)
                  }
                }
              },
              [
                _c("div", { staticClass: "row" }, [
                  _vm.form.quotationMaterial
                    ? _c("div", { staticClass: "col-md-12" }, [
                        _c("div", { staticClass: "form-group" }, [
                          _c("label", [_vm._v("Quotation")]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.form.quotationMaterial,
                                expression: "form.quotationMaterial"
                              }
                            ],
                            staticClass: "form-control m-input",
                            attrs: { disabled: "" },
                            domProps: { value: _vm.form.quotationMaterial },
                            on: {
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.$set(
                                  _vm.form,
                                  "quotationMaterial",
                                  $event.target.value
                                )
                              }
                            }
                          }),
                          _c("br"),
                          _vm._v(" "),
                          _vm.validationErrors.description
                            ? _c("label", { staticClass: "text-danger" }, [
                                _vm._v(
                                  _vm._s(_vm.validationErrors.description[0])
                                )
                              ])
                            : _vm._e()
                        ])
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _vm._m(1),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.customer_id },
                          attrs: {
                            options: _vm.customers,
                            label: "name",
                            required: "",
                            disabled: ""
                          },
                          on: {
                            input: function($event) {
                              _vm.fetchItems($event)
                              _vm.fetchCustomersDetail($event)
                            }
                          },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedCustomer
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedCustomer,
                            callback: function($$v) {
                              _vm.selectedCustomer = $$v
                            },
                            expression: "selectedCustomer"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.customer_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.customer_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _vm._m(2),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.salesman_id },
                          attrs: { options: _vm.salesmans, label: "name" },
                          scopedSlots: _vm._u([
                            {
                              key: "search",
                              fn: function(ref) {
                                var attributes = ref.attributes
                                var events = ref.events
                                return [
                                  _c(
                                    "input",
                                    _vm._g(
                                      _vm._b(
                                        {
                                          staticClass: "vs__search",
                                          attrs: {
                                            required: !_vm.selectedSalesman
                                          }
                                        },
                                        "input",
                                        attributes,
                                        false
                                      ),
                                      events
                                    )
                                  )
                                ]
                              }
                            }
                          ]),
                          model: {
                            value: _vm.selectedSalesman,
                            callback: function($$v) {
                              _vm.selectedSalesman = $$v
                            },
                            expression: "selectedSalesman"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.salesman_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.salesman_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("h4", [_vm._v("Customer Detail")]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Company Name")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.company_name,
                          expression: "this.customerDetail.company_name"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: {
                        type: "text",
                        name: "company_name",
                        disabled: ""
                      },
                      domProps: { value: this.customerDetail.company_name },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "company_name",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Email")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.email,
                          expression: "this.customerDetail.email"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "email", disabled: "" },
                      domProps: { value: this.customerDetail.email },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "email",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Contact Person ")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.pic_1,
                          expression: "this.customerDetail.pic_1"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "pic_1", disabled: "" },
                      domProps: { value: this.customerDetail.pic_1 },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "pic_1",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-4" }, [
                    _c("label", [_vm._v("Phone")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.phone,
                          expression: "this.customerDetail.phone"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "phone", disabled: "" },
                      domProps: { value: this.customerDetail.phone },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "phone",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Fax Number")]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.fax,
                          expression: "this.customerDetail.fax"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", name: "fax", disabled: "" },
                      domProps: { value: this.customerDetail.fax },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "fax",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br"),
                    _vm._v(" "),
                    _c("label", [_vm._v("Address")]),
                    _vm._v(" "),
                    _c("textarea", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: this.customerDetail.address,
                          expression: "this.customerDetail.address"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { disabled: "" },
                      domProps: { value: this.customerDetail.address },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            this.customerDetail,
                            "address",
                            $event.target.value
                          )
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c("br")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "col-md-4" },
                    [
                      _c("label", [_vm._v("Credit Limit")]),
                      _vm._v(" "),
                      _c("money", {
                        staticClass: "form-control",
                        staticStyle: { "text-align": "right" },
                        attrs: {
                          type: "text",
                          name: "credit_limit",
                          disabled: ""
                        },
                        model: {
                          value: this.customerDetail.credit_limit,
                          callback: function($$v) {
                            _vm.$set(this.customerDetail, "credit_limit", $$v)
                          },
                          expression: "this.customerDetail.credit_limit"
                        }
                      }),
                      _vm._v(" "),
                      _c("br")
                    ],
                    1
                  )
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-6" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _vm._m(3),
                        _vm._v(" "),
                        _c("v-select", {
                          class: { "is-invalid": _vm.errors.payment_term_id },
                          attrs: {
                            options: _vm.paymentTerms,
                            label: "name",
                            required: ""
                          },
                          model: {
                            value: _vm.selectedPaymentTerm,
                            callback: function($$v) {
                              _vm.selectedPaymentTerm = $$v
                            },
                            expression: "selectedPaymentTerm"
                          }
                        }),
                        _vm._v(" "),
                        _vm.errors.payment_term_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(_vm._s(_vm.errors.payment_term_id[0]))
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "col-md-6" }, [
                    _c(
                      "div",
                      { staticClass: "form-group" },
                      [
                        _vm._m(4),
                        _vm._v(" "),
                        _c("v-select", {
                          class: {
                            "is-invalid": _vm.validationErrors.currency_id
                          },
                          attrs: {
                            options: _vm.currencies,
                            label: "name",
                            disabled: ""
                          },
                          model: {
                            value: _vm.selectedCurrency,
                            callback: function($$v) {
                              _vm.selectedCurrency = $$v
                            },
                            expression: "selectedCurrency"
                          }
                        }),
                        _vm._v(" "),
                        _vm.validationErrors.currency_id
                          ? _c("label", { staticClass: "text-danger" }, [
                              _vm._v(
                                _vm._s(_vm.validationErrors.currency_id[0])
                              )
                            ])
                          : _vm._e()
                      ],
                      1
                    )
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "form-group" }, [
                      _vm._m(5),
                      _vm._v(" "),
                      _c("textarea", {
                        directives: [
                          {
                            name: "model",
                            rawName: "v-model",
                            value: _vm.form.description,
                            expression: "form.description"
                          }
                        ],
                        staticClass: "form-control",
                        class: {
                          "is-invalid": _vm.validationErrors.description
                        },
                        staticStyle: { height: "100px" },
                        attrs: { cols: "40" },
                        domProps: { value: _vm.form.description },
                        on: {
                          input: function($event) {
                            if ($event.target.composing) {
                              return
                            }
                            _vm.$set(
                              _vm.form,
                              "description",
                              $event.target.value
                            )
                          }
                        }
                      }),
                      _vm._v(" "),
                      _vm.validationErrors.description
                        ? _c("label", { staticClass: "text-danger" }, [
                            _vm._v(_vm._s(_vm.validationErrors.description[0]))
                          ])
                        : _vm._e()
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "row" }, [
                  _c("div", { staticClass: "col-md-12" }, [
                    _c("div", { staticClass: "table-responsive" }, [
                      _c(
                        "table",
                        {
                          staticClass: "table table-bordered table-hovered",
                          staticStyle: { "font-size": "16px" }
                        },
                        [
                          _vm._m(6),
                          _vm._v(" "),
                          _c(
                            "tbody",
                            [
                              _vm._l(this.products, function(product, index) {
                                return _c("tr", { key: index }, [
                                  _c("td", [_vm._v(_vm._s(index + 1))]),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    { staticStyle: { "font-size": "16px" } },
                                    [
                                      _vm.selectedItem[index]
                                        ? _c("i", {
                                            staticClass: "fa fa-info-circle",
                                            attrs: {
                                              "product-id": product.item,
                                              id: _vm.selectedItem[index]
                                            },
                                            on: {
                                              click: function($event) {
                                                $event.preventDefault()
                                                return _vm.showPriceHistory(
                                                  $event,
                                                  product,
                                                  index
                                                )
                                              }
                                            }
                                          })
                                        : _vm._e()
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    [
                                      _c("v-select", {
                                        attrs: {
                                          options: _vm.items,
                                          label: "name",
                                          required: ""
                                        },
                                        on: {
                                          input: function($event) {
                                            return _vm.fetchItemDetails(
                                              $event,
                                              index
                                            )
                                          }
                                        },
                                        scopedSlots: _vm._u(
                                          [
                                            {
                                              key: "search",
                                              fn: function(ref) {
                                                var attributes = ref.attributes
                                                var events = ref.events
                                                return [
                                                  _c(
                                                    "input",
                                                    _vm._g(
                                                      _vm._b(
                                                        {
                                                          staticClass:
                                                            "vs__search",
                                                          attrs: {
                                                            required: !_vm
                                                              .selectedItem[
                                                              index
                                                            ]
                                                          }
                                                        },
                                                        "input",
                                                        attributes,
                                                        false
                                                      ),
                                                      events
                                                    )
                                                  )
                                                ]
                                              }
                                            }
                                          ],
                                          null,
                                          true
                                        ),
                                        model: {
                                          value: _vm.selectedItem[index],
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.selectedItem,
                                              index,
                                              $$v
                                            )
                                          },
                                          expression: "selectedItem[index]"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.description,
                                            expression: "product.description"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: { "font-size": "16px" },
                                        attrs: {
                                          type: "text",
                                          placeholder: "Product description"
                                        },
                                        domProps: {
                                          value: product.description
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "description",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("td", { attrs: { width: "10%" } }, [
                                    _c(
                                      "select",
                                      {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.uom,
                                            expression: "product.uom"
                                          }
                                        ],
                                        staticClass: "form-control",
                                        staticStyle: { "font-size": "16px" },
                                        attrs: { required: "" },
                                        on: {
                                          input: function($event) {
                                            return _vm.fetchUomDetails(
                                              product.uom,
                                              index
                                            )
                                          },
                                          change: function($event) {
                                            var $$selectedVal = Array.prototype.filter
                                              .call(
                                                $event.target.options,
                                                function(o) {
                                                  return o.selected
                                                }
                                              )
                                              .map(function(o) {
                                                var val =
                                                  "_value" in o
                                                    ? o._value
                                                    : o.value
                                                return val
                                              })
                                            _vm.$set(
                                              product,
                                              "uom",
                                              $event.target.multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            )
                                          }
                                        }
                                      },
                                      _vm._l(_vm.uoms, function(item, i) {
                                        return _c(
                                          "option",
                                          {
                                            key: i,
                                            domProps: { value: item.id }
                                          },
                                          [_vm._v(_vm._s(item.name))]
                                        )
                                      }),
                                      0
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    { attrs: { width: "15%" } },
                                    [
                                      _c("money", {
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: { value: "0", required: "" },
                                        model: {
                                          value: product.qty,
                                          callback: function($$v) {
                                            _vm.$set(product, "qty", $$v)
                                          },
                                          expression: "product.qty"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    [
                                      _c("money", {
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: { required: "" },
                                        model: {
                                          value: product.price,
                                          callback: function($$v) {
                                            _vm.$set(product, "price", $$v)
                                          },
                                          expression: "product.price"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    { attrs: { width: "14%" } },
                                    [
                                      _c("input", {
                                        directives: [
                                          {
                                            name: "model",
                                            rawName: "v-model",
                                            value: product.discountRate,
                                            expression: "product.discountRate"
                                          }
                                        ],
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: {
                                          type: "number",
                                          value: "0",
                                          max: "100",
                                          min: "0",
                                          placeholder: "%"
                                        },
                                        domProps: {
                                          value: product.discountRate
                                        },
                                        on: {
                                          input: function($event) {
                                            if ($event.target.composing) {
                                              return
                                            }
                                            _vm.$set(
                                              product,
                                              "discountRate",
                                              $event.target.value
                                            )
                                          }
                                        }
                                      }),
                                      _c("br"),
                                      _vm._v(" "),
                                      _c("money", {
                                        staticClass: "form-control m-input",
                                        staticStyle: {
                                          "font-size": "16px",
                                          "text-align": "right"
                                        },
                                        attrs: { placeholder: "RM" },
                                        model: {
                                          value: product.discountAmount,
                                          callback: function($$v) {
                                            _vm.$set(
                                              product,
                                              "discountAmount",
                                              $$v
                                            )
                                          },
                                          expression: "product.discountAmount"
                                        }
                                      })
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    { attrs: { width: "13%", align: "right" } },
                                    [
                                      _vm._v(
                                        "\r\n                                        " +
                                          _vm._s(
                                            product.qty && product.price
                                              ? _vm.formatPrice(
                                                  product.price * product.qty -
                                                    (product.discountAmount
                                                      ? product.discountAmount
                                                      : 0) -
                                                    (product.discountRate
                                                      ? (product.discountRate /
                                                          100) *
                                                        product.price
                                                      : 0) *
                                                      product.qty
                                                )
                                              : 0
                                          ) +
                                          "\r\n                                    "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("td", [
                                    _c(
                                      "button",
                                      {
                                        staticClass: "btn btn-xs btn-danger",
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.deleteItem(
                                              index,
                                              product.id
                                            )
                                          }
                                        }
                                      },
                                      [_c("i", { staticClass: "fa fa-trash" })]
                                    )
                                  ])
                                ])
                              }),
                              _vm._v(" "),
                              _c("tr", [
                                _c(
                                  "td",
                                  { attrs: { colspan: "3", align: "center" } },
                                  [
                                    _c(
                                      "button",
                                      {
                                        staticClass:
                                          "btn btn-info pull-right btn-block",
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.show($event)
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\r\n                                            + Add New Product\r\n                                        "
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "td",
                                  { attrs: { colspan: "6", align: "center" } },
                                  [
                                    _c(
                                      "button",
                                      {
                                        staticClass:
                                          "btn btn-success pull-right btn-block",
                                        on: {
                                          click: function($event) {
                                            $event.preventDefault()
                                            return _vm.addItem($event)
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\r\n                                            + Add New Row\r\n                                        "
                                        )
                                      ]
                                    )
                                  ]
                                )
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticStyle: {
                                      "font-size": "16px",
                                      "font-weight": "bold"
                                    },
                                    attrs: { colspan: "7", align: "right" }
                                  },
                                  [
                                    _vm._v(
                                      "Subtotal Exclude Discount All Items"
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("td", { attrs: { align: "right" } }, [
                                  _vm._v(_vm._s(_vm.formatPrice(_vm.getTotal)))
                                ]),
                                _vm._v(" "),
                                _c("td")
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticStyle: {
                                      "font-size": "16px",
                                      "font-weight": "bold"
                                    },
                                    attrs: { colspan: "7", align: "right" }
                                  },
                                  [_vm._v("Discount All Items")]
                                ),
                                _vm._v(" "),
                                _c("td", { attrs: { align: "right" } }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.discountAllItem,
                                        expression: "form.discountAllItem"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    class: {
                                      "is-invalid":
                                        _vm.validationErrors.discountAllItem
                                    },
                                    staticStyle: { "text-align": "right" },
                                    attrs: { type: "text" },
                                    domProps: {
                                      value: _vm.form.discountAllItem
                                    },
                                    on: {
                                      keypress: function($event) {
                                        return _vm.isNumber($event)
                                      },
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "discountAllItem",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _c("td")
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticStyle: {
                                      "font-size": "16px",
                                      "font-weight": "bold"
                                    },
                                    attrs: { colspan: "7", align: "right" }
                                  },
                                  [_vm._v("Sub Total")]
                                ),
                                _vm._v(" "),
                                _c("td", { attrs: { align: "right" } }, [
                                  _vm._v(
                                    "\r\n                                        " +
                                      _vm._s(_vm.formatPrice(_vm.subTotal)) +
                                      "\r\n                                    "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("td")
                              ]),
                              _vm._v(" "),
                              _c("tr", [
                                _c(
                                  "td",
                                  {
                                    staticStyle: {
                                      "font-size": "13px",
                                      "font-weight": "bold"
                                    },
                                    attrs: { colspan: "6", align: "right" }
                                  },
                                  [_vm._v("GST")]
                                ),
                                _vm._v(" "),
                                _c("td", { attrs: { align: "right" } }, [
                                  _c(
                                    "div",
                                    {
                                      staticClass: "form-group",
                                      staticStyle: { width: "100px" }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          staticClass:
                                            "input-group colorpickerinput"
                                        },
                                        [
                                          _c("input", {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value: _vm.form.taxRate,
                                                expression: "form.taxRate"
                                              }
                                            ],
                                            staticClass: "form-control",
                                            attrs: {
                                              type: "text",
                                              disabled: ""
                                            },
                                            domProps: {
                                              value: _vm.form.taxRate
                                            },
                                            on: {
                                              input: function($event) {
                                                if ($event.target.composing) {
                                                  return
                                                }
                                                _vm.$set(
                                                  _vm.form,
                                                  "taxRate",
                                                  $event.target.value
                                                )
                                              }
                                            }
                                          }),
                                          _vm._v(" "),
                                          _vm._m(7)
                                        ]
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("td", { attrs: { align: "right" } }, [
                                  _vm._v(
                                    "\r\n                                        " +
                                      _vm._s(_vm.formatPrice(_vm.getGst)) +
                                      "\r\n                                    "
                                  )
                                ]),
                                _vm._v(" "),
                                _c("td")
                              ]),
                              _vm._v(" "),
                              _c(
                                "tr",
                                {
                                  staticStyle: {
                                    "font-size": "16px",
                                    "font-weight": "bold"
                                  }
                                },
                                [
                                  _c(
                                    "td",
                                    { attrs: { colspan: "7", align: "right" } },
                                    [_vm._v("Total")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "td",
                                    {
                                      staticStyle: { color: "#5b35c4" },
                                      attrs: { align: "right" }
                                    },
                                    [
                                      _vm._v(
                                        "\r\n                                        " +
                                          _vm._s(_vm.formatPrice(_vm.total)) +
                                          "\r\n                                    "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("td")
                                ]
                              )
                            ],
                            2
                          )
                        ]
                      )
                    ])
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "div",
                  { staticClass: "m-portlet__foot m-portlet__foot--fit" },
                  [
                    _c(
                      "div",
                      { staticClass: "m-form__actions m-form__actions--right" },
                      [
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-primary btn-lg",
                            attrs: { type: "submit", disabled: _vm.loading }
                          },
                          [
                            _vm._v(
                              _vm._s(this.loading ? "Please Wait..." : "Update")
                            )
                          ]
                        )
                      ]
                    )
                  ]
                )
              ]
            ),
            _vm._v(" "),
            _c("Product", {
              on: { messageFromChild: _vm.childMessageReceived }
            }),
            _vm._v(" "),
            _c("price-history", {
              attrs: {
                id: this.product_id,
                billing: this.billing,
                pr_history: this.pr_history
              }
            })
          ],
          1
        )
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "div",
      { staticClass: "spinner-border", attrs: { role: "status" } },
      [_c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])]
    )
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Customer "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Salesman "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Payment Term "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Currency "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("label", [
      _vm._v("Remark "),
      _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("thead", [
      _c("tr", [
        _c("th", { attrs: { align: "center" } }, [_vm._v("#")]),
        _vm._v(" "),
        _c("th", {
          staticStyle: { "font-size": "16px" },
          attrs: { align: "center" }
        }),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { width: "20%" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                        Item\r\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { width: "12%" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                        UoM\r\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c(
          "th",
          { staticStyle: { width: "12%" }, attrs: { align: "center" } },
          [
            _vm._v(
              "\r\n                                        Qty\r\n                                        "
            ),
            _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
          ]
        ),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } }, [
          _vm._v(
            "\r\n                                        Price\r\n                                        "
          ),
          _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
        ]),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } }, [
          _vm._v("\r\n                                        Dsc (%) "),
          _c("br"),
          _vm._v(
            "\r\n                                        Dsc Amount\r\n                                    "
          )
        ]),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } }, [
          _vm._v(
            "\r\n                                        Sub Total\r\n                                        "
          ),
          _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
        ]),
        _vm._v(" "),
        _c("th", { attrs: { align: "center" } })
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "input-group-append" }, [
      _c("div", { staticClass: "input-group-text" }, [_c("i", [_vm._v("%")])])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=template&id=4fecc121&":
/*!***************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue?vue&type=template&id=4fecc121& ***!
  \***************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "modal",
    {
      staticStyle: {
        "padding-top": "40px",
        "padding-bottom": "40px",
        "overflow-x": "scroll"
      },
      attrs: {
        name: "price-history-modal",
        resizable: _vm.resizable,
        adaptive: _vm.adaptive,
        height: "auto",
        scrollable: true,
        width: "55%"
      }
    },
    [
      _c("div", { staticClass: "card-body" }, [
        _c(
          "div",
          {
            staticClass: "tab-pane",
            attrs: {
              id: "price_history",
              role: "tabpanel",
              "aria-labelledby": "home-tab"
            }
          },
          [
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "h4",
                  {
                    staticClass: "card-title",
                    staticStyle: { "text-align": "center" }
                  },
                  [_vm._v("Riwayat Penjualan")]
                ),
                _vm._v(" "),
                _c("hr")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass:
                      "table table-bordered table-hover table-striped",
                    attrs: { cellspacing: "0", id: "datatable" }
                  },
                  [
                    _c("tr", [
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Customer Name")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Date")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Price")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Amount")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Sales Order Number")
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(this.billing, function(product, index) {
                        return _c("tr", { key: index }, [
                          _c("td", [
                            _vm._v(
                              _vm._s(product.sales_order.customer.company_name)
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [_vm._v(_vm._s(product.sales_order.date))]),
                          _vm._v(" "),
                          _c("td", { staticStyle: { "text-align": "right" } }, [
                            _vm._v(_vm._s(_vm.formatPrice(product.price)))
                          ]),
                          _vm._v(" "),
                          _c("td", { staticStyle: { "text-align": "right" } }, [
                            _vm._v(_vm._s(_vm.formatPrice(product.sub_total)))
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(_vm._s(product.sales_order.code_number))
                          ])
                        ])
                      }),
                      0
                    )
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c("hr"),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _c("div", { staticClass: "card-body" }, [
                _c(
                  "h4",
                  {
                    staticClass: "card-title",
                    staticStyle: { "text-align": "center" }
                  },
                  [_vm._v("Riwayat Pembelian")]
                ),
                _vm._v(" "),
                _c("hr")
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "table-responsive" }, [
                _c(
                  "table",
                  {
                    staticClass:
                      "table table-bordered table-hover table-striped",
                    attrs: { cellspacing: "0", id: "datatable" }
                  },
                  [
                    _c("tr", [
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Supplier Name")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Date")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Price")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("Amount")
                      ]),
                      _vm._v(" "),
                      _c("th", { staticStyle: { "text-align": "center" } }, [
                        _vm._v("PO Number")
                      ])
                    ]),
                    _vm._v(" "),
                    _c(
                      "tbody",
                      _vm._l(this.pr_history, function(history, index) {
                        return _c("tr", { key: index }, [
                          _c("td", [
                            _vm._v(
                              _vm._s(
                                history.purchase_order.supplier.company_name
                              )
                            )
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(_vm._s(history.purchase_order.date))
                          ]),
                          _vm._v(" "),
                          _c("td", { staticStyle: { "text-align": "right" } }, [
                            _vm._v(_vm._s(_vm.formatPrice(history.price)))
                          ]),
                          _vm._v(" "),
                          _c("td", { staticStyle: { "text-align": "right" } }, [
                            _vm._v(_vm._s(_vm.formatPrice(history.sub_total)))
                          ]),
                          _vm._v(" "),
                          _c("td", [
                            _vm._v(_vm._s(history.purchase_order.code_number))
                          ])
                        ])
                      }),
                      0
                    )
                  ]
                )
              ])
            ])
          ]
        )
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=template&id=4577b449&":
/*!**********************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./Modules/Transaction/Resources/assets/js/components/sales_order_material/Product.vue?vue&type=template&id=4577b449& ***!
  \**********************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "modal",
    {
      attrs: {
        name: "example-modal",
        resizable: _vm.resizable,
        adaptive: _vm.adaptive,
        height: "600",
        scrollable: true
      }
    },
    [
      _c("div", { staticClass: "card-body" }, [
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("h4", { staticStyle: { "text-align": "center" } }, [
                _vm._v("Add New Product")
              ])
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "col-md-12" }, [
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", [_vm._v("Product Category")]),
                _vm._v(" "),
                _c("v-select", {
                  class: {
                    "is-invalid": _vm.validationErrors.product_category_id
                  },
                  attrs: {
                    autocomplete: "",
                    options: _vm.product_category,
                    label: "name"
                  },
                  on: {
                    input: function($event) {
                      return _vm.fetchProductSubCategory($event)
                    }
                  },
                  model: {
                    value: _vm.selectedProductCategory,
                    callback: function($$v) {
                      _vm.selectedProductCategory = $$v
                    },
                    expression: "selectedProductCategory"
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.product_category_id
                  ? _c("div", { staticClass: "text-danger" }, [
                      _vm._v(
                        _vm._s(_vm.validationErrors.product_category_id[0])
                      )
                    ])
                  : _vm._e()
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c(
              "div",
              { staticClass: "form-group" },
              [
                _c("label", [_vm._v("Product Sub Category")]),
                _vm._v(" "),
                _c("v-select", {
                  class: {
                    "is-invalid": _vm.validationErrors.product_sub_category_id
                  },
                  attrs: {
                    autocomplete: "",
                    options: _vm.product_sub_category,
                    label: "name"
                  },
                  model: {
                    value: _vm.selectedProductSubCategory,
                    callback: function($$v) {
                      _vm.selectedProductSubCategory = $$v
                    },
                    expression: "selectedProductSubCategory"
                  }
                }),
                _vm._v(" "),
                _vm.validationErrors.product_sub_category_id
                  ? _c("div", { staticClass: "text-danger" }, [
                      _vm._v(
                        _vm._s(
                          _vm.validationErrors.product_sub_category_id[0]
                        ) + "\r\n                    "
                      )
                    ])
                  : _vm._e()
              ],
              1
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [
                _vm._v("Code "),
                _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.code,
                    expression: "form.code"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.code },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.code },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "code", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.code
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.code[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [
                _vm._v("Name "),
                _c("sup", { staticStyle: { color: "red" } }, [_vm._v("*")])
              ]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.name,
                    expression: "form.name"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.name },
                attrs: { type: "text", required: "" },
                domProps: { value: _vm.form.name },
                on: {
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "name", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.name
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.name[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c("label", [_vm._v("Price")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.form.price,
                    expression: "form.price"
                  }
                ],
                staticClass: "form-control",
                class: { "is-invalid": _vm.validationErrors.price },
                staticStyle: { "text-align": "right" },
                attrs: { type: "number", required: "" },
                domProps: { value: _vm.form.price },
                on: {
                  keypress: function($event) {
                    return _vm.isNumber($event)
                  },
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.form, "price", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _vm.validationErrors.price
                ? _c("label", { staticClass: "text-danger" }, [
                    _vm._v(_vm._s(_vm.validationErrors.price[0]))
                  ])
                : _vm._e()
            ])
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "row" }, [
          _c("div", { staticClass: "col-md-12" }, [
            _c("div", { staticClass: "form-group" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-info pull-right",
                  staticStyle: { "margin-bottom": "20px" },
                  on: {
                    click: function($event) {
                      $event.preventDefault()
                      return _vm.onClickButton($event)
                    }
                  }
                },
                [_vm._v("Save")]
              )
            ])
          ])
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ExampleComponent.vue?vue&type=template&id=299e239e&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/ExampleComponent.vue?vue&type=template&id=299e239e& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "container" }, [
      _c("div", { staticClass: "row justify-content-center" }, [
        _c("div", { staticClass: "col-md-8" }, [
          _c("div", { staticClass: "card" }, [
            _c("div", { staticClass: "card-header" }, [
              _vm._v("Example Component")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "card-body" }, [
              _vm._v(
                "\n                    I'm an example component.\n                "
              )
            ])
          ])
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/input.vue?vue&type=template&id=4ecc5528&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/commons/input.vue?vue&type=template&id=4ecc5528& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "form-group" }, [
    _c("label", [_vm._v(_vm._s(_vm.label))]),
    _vm._v(" "),
    _c("input", {
      staticClass: "form-control",
      class: { "is-invalid": _vm.error },
      attrs: { type: "type" }
    }),
    _vm._v(" "),
    _vm.error
      ? _c("label", { staticClass: "text-danger" }, [
          _vm._v(_vm._s(_vm.errorMessage))
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/loader.vue?vue&type=template&id=4c685a25&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/commons/loader.vue?vue&type=template&id=4c685a25& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "d-flex justify-content-center" }, [
      _c("div", { staticClass: "spinner-border", attrs: { role: "status" } }, [
        _c("span", { staticClass: "sr-only" }, [_vm._v("Loading...")])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true&":
/*!****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/commons/validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true& ***!
  \****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm.validationErrors
    ? _c("div", [
        _c(
          "ul",
          { staticClass: "alert alert-danger" },
          _vm._l(_vm.validationErrors, function(value, key, index) {
            return _c("li", [_vm._v("@" + _vm._s(value))])
          }),
          0
        )
      ])
    : _vm._e()
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var v_select2_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! v-select2-component */ "./node_modules/v-select2-component/dist/Select2.esm.js");
/* harmony import */ var vue_sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-sweetalert2 */ "./node_modules/vue-sweetalert2/dist/index.js");
/* harmony import */ var sweetalert2_dist_sweetalert2_min_css__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2/dist/sweetalert2.min.css */ "./node_modules/sweetalert2/dist/sweetalert2.min.css");
/* harmony import */ var sweetalert2_dist_sweetalert2_min_css__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2_dist_sweetalert2_min_css__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue_currency_input__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue-currency-input */ "./node_modules/vue-currency-input/dist/vue-currency-input.esm.js");





__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");

window.Vue = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
Vue.use(vue_sweetalert2__WEBPACK_IMPORTED_MODULE_1__["default"]);
Vue.use(vue_currency_input__WEBPACK_IMPORTED_MODULE_3__["default"], {
  globalOptions: {
    allowNegative: false,
    currency: null
  },
  componentName: 'Money'
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('ValidationErrors', __webpack_require__(/*! ./components/commons/validation-errors */ "./resources/js/components/commons/validation-errors.vue"));
Vue.component('Input', __webpack_require__(/*! ./components/commons/input */ "./resources/js/components/commons/input.vue"));
Vue.component('example-component', __webpack_require__(/*! ./components/ExampleComponent.vue */ "./resources/js/components/ExampleComponent.vue")["default"]); // Vue.component(
//     'purchaserequisition-create',
//     require('../../Modules/Procurement/Resources/assets/js/components/purchase_requisition/Create.vue').default
// );
// Vue.component(
//     'purchaserequisition-edit',
//     require('../../Modules/Procurement/Resources/assets/js/components/purchase_requisition/Edit.vue').default
// );

Vue.component("purchase-order-create", __webpack_require__(/*! ../../Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue")["default"]);
Vue.component("purchase-order-edit", __webpack_require__(/*! ../../Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue */ "./Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue")["default"]); // Vue.component(
//     "quotation-material-create",
//     require("../../Modules/Transaction/Resources/assets/js/components/quotation_material/Create.vue")
//     .default
// );
// Vue.component(
//     "quotation-material-edit",
//     require("../../Modules/Transaction/Resources/assets/js/components/quotation_material/Edit.vue")
//     .default
// );

Vue.component("sales-order-material-create", __webpack_require__(/*! ../../Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue")["default"]);
Vue.component("sales-order-material-edit", __webpack_require__(/*! ../../Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue")["default"]); // Vue.component(
//     "delivery-order-material-create",
//     require("../../Modules/Transaction/Resources/assets/js/components/delivery_order_material/Create.vue")
//     .default
// );
// Vue.component(
//     "delivery-order-material-edit",
//     require("../../Modules/Transaction/Resources/assets/js/components/delivery_order_material/Edit.vue")
//     .default
// );

Vue.component("stock-transfer-create", __webpack_require__(/*! ../../Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue */ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue")["default"]);
Vue.component("stock-transfer-edit", __webpack_require__(/*! ../../Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue */ "./Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue")["default"]);
Vue.component("stock-checking-create", __webpack_require__(/*! ../../Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue */ "./Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue")["default"]);
Vue.component("stock-checking-edit", __webpack_require__(/*! ../../Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue */ "./Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue")["default"]); // Vue.component('receivable-create', require('../../Modules/Finance/Resources/assets/js/receivable/Create.vue').default);
// Vue.component('receivable-edit', require('../../Modules/Finance/Resources/assets/js/receivable/Edit.vue').default);
//
// Vue.component('payable-create', require('../../Modules/Finance/Resources/assets/js/payable/Create.vue').default);

Vue.component('price-history', __webpack_require__(/*! ../../Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue */ "./Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue")["default"]); // Vue.component('payable-edit', require('../../Modules/Finance/Resources/assets/js/payable/Edit.vue').default);
// Vue.component('purchase-return-create', require("../../Modules/Procurement/Resources/assets/js/components/purchase_return/Create.vue").default);
// Vue.component('purchase-return-edit', require("../../Modules/Procurement/Resources/assets/js/components/purchase_return/Edit.vue").default);
//
// Vue.component('sales-return-create', require("../../Modules/Transaction/Resources/assets/js/components/sales_return/Create").default);
// Vue.component('sales-return-edit', require("../../Modules/Transaction/Resources/assets/js/components/sales_return/Edit").default);

Vue.component('create-shift-setting', __webpack_require__(/*! ../../Modules/HR/Resources/assets/js/setting_shift/Create.vue */ "./Modules/HR/Resources/assets/js/setting_shift/Create.vue")["default"]);
Vue.component("stock-in-create", __webpack_require__(/*! ../../Modules/Stock/Resources/assets/js/components/stock_in/Create.vue */ "./Modules/Stock/Resources/assets/js/components/stock_in/Create.vue")["default"]);
Vue.component("stock-in-edit", __webpack_require__(/*! ../../Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue */ "./Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue")["default"]); // Vue.component('payable-deposit-create', require('../../Modules/Finance/Resources/assets/js/payable_deposit/payable_deposit.vue').default);
// Vue.component('payable-deposit-edit', require('../../Modules/Finance/Resources/assets/js/payable_deposit/edit_payable_deposit.vue').default);
//
// Vue.component('receivable-deposit-create', require('../../Modules/Finance/Resources/assets/js/receivable_deposit/receivable_deposit.vue').default);
// Vue.component('receivable-deposit-edit', require('../../Modules/Finance/Resources/assets/js/receivable_deposit/edit_receivable_deposit.vue').default);

Vue.component('Select2', v_select2_component__WEBPACK_IMPORTED_MODULE_0__["default"]);
Vue.component('Loader', __webpack_require__(/*! ./components/commons/loader */ "./resources/js/components/commons/loader.vue"));
var app = new Vue({
  el: '#app'
});

/***/ }),

/***/ "./resources/js/bootstrap.js":
/*!***********************************!*\
  !*** ./resources/js/bootstrap.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

window._ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
window.jQuery = window.$ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
window.moment = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");

__webpack_require__(/*! popper.js */ "./node_modules/popper.js/dist/esm/popper.js");

__webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");

__webpack_require__(/*! jquery.nicescroll */ "./node_modules/jquery.nicescroll/jquery.nicescroll.js");

__webpack_require__(/*! ./stisla/stisla */ "./resources/js/stisla/stisla.js");

__webpack_require__(/*! ./stisla/scripts */ "./resources/js/stisla/scripts.js");

__webpack_require__(/*! ./stisla/custom */ "./resources/js/stisla/custom.js");
/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */


var token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
  window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
  console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
// import Echo from 'laravel-echo'
// window.Pusher = require('pusher-js');
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

/***/ }),

/***/ "./resources/js/components/ExampleComponent.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/ExampleComponent.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ExampleComponent_vue_vue_type_template_id_299e239e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ExampleComponent.vue?vue&type=template&id=299e239e& */ "./resources/js/components/ExampleComponent.vue?vue&type=template&id=299e239e&");
/* harmony import */ var _ExampleComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ExampleComponent.vue?vue&type=script&lang=js& */ "./resources/js/components/ExampleComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ExampleComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ExampleComponent_vue_vue_type_template_id_299e239e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ExampleComponent_vue_vue_type_template_id_299e239e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/ExampleComponent.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/ExampleComponent.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/ExampleComponent.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ExampleComponent.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ExampleComponent.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleComponent_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/ExampleComponent.vue?vue&type=template&id=299e239e&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/ExampleComponent.vue?vue&type=template&id=299e239e& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleComponent_vue_vue_type_template_id_299e239e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ExampleComponent.vue?vue&type=template&id=299e239e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/ExampleComponent.vue?vue&type=template&id=299e239e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleComponent_vue_vue_type_template_id_299e239e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ExampleComponent_vue_vue_type_template_id_299e239e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/commons/input.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/commons/input.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _input_vue_vue_type_template_id_4ecc5528___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./input.vue?vue&type=template&id=4ecc5528& */ "./resources/js/components/commons/input.vue?vue&type=template&id=4ecc5528&");
/* harmony import */ var _input_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./input.vue?vue&type=script&lang=js& */ "./resources/js/components/commons/input.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _input_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _input_vue_vue_type_template_id_4ecc5528___WEBPACK_IMPORTED_MODULE_0__["render"],
  _input_vue_vue_type_template_id_4ecc5528___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/commons/input.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/commons/input.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/commons/input.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_input_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./input.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/input.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_input_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/commons/input.vue?vue&type=template&id=4ecc5528&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/commons/input.vue?vue&type=template&id=4ecc5528& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_input_vue_vue_type_template_id_4ecc5528___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./input.vue?vue&type=template&id=4ecc5528& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/input.vue?vue&type=template&id=4ecc5528&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_input_vue_vue_type_template_id_4ecc5528___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_input_vue_vue_type_template_id_4ecc5528___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/commons/loader.vue":
/*!****************************************************!*\
  !*** ./resources/js/components/commons/loader.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _loader_vue_vue_type_template_id_4c685a25___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./loader.vue?vue&type=template&id=4c685a25& */ "./resources/js/components/commons/loader.vue?vue&type=template&id=4c685a25&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _loader_vue_vue_type_template_id_4c685a25___WEBPACK_IMPORTED_MODULE_0__["render"],
  _loader_vue_vue_type_template_id_4c685a25___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/commons/loader.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/commons/loader.vue?vue&type=template&id=4c685a25&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/commons/loader.vue?vue&type=template&id=4c685a25& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loader_vue_vue_type_template_id_4c685a25___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./loader.vue?vue&type=template&id=4c685a25& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/loader.vue?vue&type=template&id=4c685a25&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loader_vue_vue_type_template_id_4c685a25___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_loader_vue_vue_type_template_id_4c685a25___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/commons/validation-errors.vue":
/*!***************************************************************!*\
  !*** ./resources/js/components/commons/validation-errors.vue ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _validation_errors_vue_vue_type_template_id_7ef6891d_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true& */ "./resources/js/components/commons/validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true&");
/* harmony import */ var _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=script&lang=js& */ "./resources/js/components/commons/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _validation_errors_vue_vue_type_template_id_7ef6891d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _validation_errors_vue_vue_type_template_id_7ef6891d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "7ef6891d",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/commons/validation-errors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/commons/validation-errors.vue?vue&type=script&lang=js&":
/*!****************************************************************************************!*\
  !*** ./resources/js/components/commons/validation-errors.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/commons/validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true&":
/*!**********************************************************************************************************!*\
  !*** ./resources/js/components/commons/validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true& ***!
  \**********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_7ef6891d_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/commons/validation-errors.vue?vue&type=template&id=7ef6891d&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_7ef6891d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_7ef6891d_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/stisla/custom.js":
/*!***************************************!*\
  !*** ./resources/js/stisla/custom.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */


/***/ }),

/***/ "./resources/js/stisla/scripts.js":
/*!****************************************!*\
  !*** ./resources/js/stisla/scripts.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
 // ChartJS

if (window.Chart) {
  Chart.defaults.global.defaultFontFamily = "'Nunito', 'Segoe UI', 'Arial'";
  Chart.defaults.global.defaultFontSize = 12;
  Chart.defaults.global.defaultFontStyle = 500;
  Chart.defaults.global.defaultFontColor = "#999";
  Chart.defaults.global.tooltips.backgroundColor = "#000";
  Chart.defaults.global.tooltips.bodyFontColor = "rgba(255,255,255,.7)";
  Chart.defaults.global.tooltips.titleMarginBottom = 10;
  Chart.defaults.global.tooltips.titleFontSize = 14;
  Chart.defaults.global.tooltips.titleFontFamily = "'Nunito', 'Segoe UI', 'Arial'";
  Chart.defaults.global.tooltips.titleFontColor = '#fff';
  Chart.defaults.global.tooltips.xPadding = 15;
  Chart.defaults.global.tooltips.yPadding = 15;
  Chart.defaults.global.tooltips.displayColors = false;
  Chart.defaults.global.tooltips.intersect = false;
  Chart.defaults.global.tooltips.mode = 'nearest';
} // DropzoneJS


if (window.Dropzone) {
  Dropzone.autoDiscover = false;
} // Basic confirm box


$('[data-confirm]').each(function () {
  var me = $(this),
      me_data = me.data('confirm');
  me_data = me_data.split("|");
  me.fireModal({
    title: me_data[0],
    body: me_data[1],
    buttons: [{
      text: me.data('confirm-text-yes') || 'Yes',
      "class": 'btn btn-danger btn-shadow',
      handler: function handler() {
        eval(me.data('confirm-yes'));
      }
    }, {
      text: me.data('confirm-text-cancel') || 'Cancel',
      "class": 'btn btn-secondary',
      handler: function handler(modal) {
        $.destroyModal(modal);
        eval(me.data('confirm-no'));
      }
    }]
  });
}); // Global

$(function () {
  var sidebar_nicescroll_opts = {
    cursoropacitymin: 0,
    cursoropacitymax: .8,
    zindex: 892
  },
      now_layout_class = null;

  var sidebar_sticky = function sidebar_sticky() {
    if ($("body").hasClass('layout-2')) {
      $("body.layout-2 #sidebar-wrapper").stick_in_parent({
        parent: $('body')
      });
      $("body.layout-2 #sidebar-wrapper").stick_in_parent({
        recalc_every: 1
      });
    }
  };

  sidebar_sticky();
  var sidebar_nicescroll;

  var update_sidebar_nicescroll = function update_sidebar_nicescroll() {
    var a = setInterval(function () {
      if (sidebar_nicescroll != null) sidebar_nicescroll.resize();
    }, 10);
    setTimeout(function () {
      clearInterval(a);
    }, 600);
  };

  var sidebar_dropdown = function sidebar_dropdown() {
    if ($(".main-sidebar").length) {
      $(".main-sidebar").niceScroll(sidebar_nicescroll_opts);
      sidebar_nicescroll = $(".main-sidebar").getNiceScroll();
      $(".main-sidebar .sidebar-menu li a.has-dropdown").off('click').on('click', function () {
        var me = $(this);
        var active = false;

        if (me.parent().hasClass("active")) {
          active = true;
        }

        $('.main-sidebar .sidebar-menu li.active > .dropdown-menu').slideUp(500, function () {
          update_sidebar_nicescroll();
          return false;
        });
        $('.main-sidebar .sidebar-menu li.active').removeClass('active');

        if (active == true) {
          me.parent().removeClass('active');
          me.parent().find('> .dropdown-menu').slideUp(500, function () {
            update_sidebar_nicescroll();
            return false;
          });
        } else {
          me.parent().addClass('active');
          me.parent().find('> .dropdown-menu').slideDown(500, function () {
            update_sidebar_nicescroll();
            return false;
          });
        }

        return false;
      });
      $('.main-sidebar .sidebar-menu li.active > .dropdown-menu').slideDown(500, function () {
        update_sidebar_nicescroll();
        return false;
      });
    }
  };

  sidebar_dropdown();

  if ($("#top-5-scroll").length) {
    $("#top-5-scroll").css({
      height: 315
    }).niceScroll();
  }

  $(".main-content").css({
    minHeight: $(window).outerHeight() - 108
  });
  $(".nav-collapse-toggle").click(function () {
    $(this).parent().find('.navbar-nav').toggleClass('show');
    return false;
  });
  $(document).on('click', function (e) {
    $(".nav-collapse .navbar-nav").removeClass('show');
  });

  var toggle_sidebar_mini = function toggle_sidebar_mini(mini) {
    var body = $('body');

    if (!mini) {
      body.removeClass('sidebar-mini');
      $(".main-sidebar").css({
        overflow: 'hidden'
      });
      setTimeout(function () {
        $(".main-sidebar").niceScroll(sidebar_nicescroll_opts);
        sidebar_nicescroll = $(".main-sidebar").getNiceScroll();
      }, 500);
      $(".main-sidebar .sidebar-menu > li > ul .dropdown-title").remove();
      $(".main-sidebar .sidebar-menu > li > a").removeAttr('data-toggle');
      $(".main-sidebar .sidebar-menu > li > a").removeAttr('data-original-title');
      $(".main-sidebar .sidebar-menu > li > a").removeAttr('title');
    } else {
      body.addClass('sidebar-mini');
      body.removeClass('sidebar-show');
      sidebar_nicescroll.remove();
      sidebar_nicescroll = null;
      $(".main-sidebar .sidebar-menu > li").each(function () {
        var me = $(this);

        if (me.find('> .dropdown-menu').length) {
          me.find('> .dropdown-menu').hide();
          me.find('> .dropdown-menu').prepend('<li class="dropdown-title pt-3">' + me.find('> a').text() + '</li>');
        } else {
          me.find('> a').attr('data-toggle', 'tooltip');
          me.find('> a').attr('data-original-title', me.find('> a').text());
          $("[data-toggle='tooltip']").tooltip({
            placement: 'right'
          });
        }
      });
    }
  };

  $("[data-toggle='sidebar']").click(function () {
    var body = $("body"),
        w = $(window);

    if (w.outerWidth() <= 1024) {
      body.removeClass('search-show search-gone');

      if (body.hasClass('sidebar-gone')) {
        body.removeClass('sidebar-gone');
        body.addClass('sidebar-show');
      } else {
        body.addClass('sidebar-gone');
        body.removeClass('sidebar-show');
      }

      update_sidebar_nicescroll();
    } else {
      body.removeClass('search-show search-gone');

      if (body.hasClass('sidebar-mini')) {
        toggle_sidebar_mini(false);
      } else {
        toggle_sidebar_mini(true);
      }
    }

    return false;
  });

  var toggleLayout = function toggleLayout() {
    var w = $(window),
        layout_class = $('body').attr('class') || '',
        layout_classes = layout_class.trim().length > 0 ? layout_class.split(' ') : '';

    if (layout_classes.length > 0) {
      layout_classes.forEach(function (item) {
        if (item.indexOf('layout-') != -1) {
          now_layout_class = item;
        }
      });
    }

    if (w.outerWidth() <= 1024) {
      if ($('body').hasClass('sidebar-mini')) {
        toggle_sidebar_mini(false);
        $('.main-sidebar').niceScroll(sidebar_nicescroll_opts);
        sidebar_nicescroll = $(".main-sidebar").getNiceScroll();
      }

      $("body").addClass("sidebar-gone");
      $("body").removeClass("layout-2 layout-3 sidebar-mini sidebar-show");
      $("body").off('click touchend').on('click touchend', function (e) {
        if ($(e.target).hasClass('sidebar-show') || $(e.target).hasClass('search-show')) {
          $("body").removeClass("sidebar-show");
          $("body").addClass("sidebar-gone");
          $("body").removeClass("search-show");
          update_sidebar_nicescroll();
        }
      });
      update_sidebar_nicescroll();

      if (now_layout_class == 'layout-3') {
        var nav_second_classes = $(".navbar-secondary").attr('class'),
            nav_second = $(".navbar-secondary");
        nav_second.attr('data-nav-classes', nav_second_classes);
        nav_second.removeAttr('class');
        nav_second.addClass('main-sidebar');
        var main_sidebar = $(".main-sidebar");
        main_sidebar.find('.container').addClass('sidebar-wrapper').removeClass('container');
        main_sidebar.find('.navbar-nav').addClass('sidebar-menu').removeClass('navbar-nav');
        main_sidebar.find('.sidebar-menu .nav-item.dropdown.show a').click();
        main_sidebar.find('.sidebar-brand').remove();
        main_sidebar.find('.sidebar-menu').before($('<div>', {
          "class": 'sidebar-brand'
        }).append($('<a>', {
          href: $('.navbar-brand').attr('href')
        }).html($('.navbar-brand').html())));
        setTimeout(function () {
          sidebar_nicescroll = main_sidebar.niceScroll(sidebar_nicescroll_opts);
          sidebar_nicescroll = main_sidebar.getNiceScroll();
        }, 700);
        sidebar_dropdown();
        $(".main-wrapper").removeClass("container");
      }
    } else {
      $("body").removeClass("sidebar-gone sidebar-show");
      if (now_layout_class) $("body").addClass(now_layout_class);

      var _nav_second_classes = $(".main-sidebar").attr('data-nav-classes'),
          _nav_second = $(".main-sidebar");

      if (now_layout_class == 'layout-3' && _nav_second.hasClass('main-sidebar')) {
        _nav_second.find(".sidebar-menu li a.has-dropdown").off('click');

        _nav_second.find('.sidebar-brand').remove();

        _nav_second.removeAttr('class');

        _nav_second.addClass(_nav_second_classes);

        var _main_sidebar = $(".navbar-secondary");

        _main_sidebar.find('.sidebar-wrapper').addClass('container').removeClass('sidebar-wrapper');

        _main_sidebar.find('.sidebar-menu').addClass('navbar-nav').removeClass('sidebar-menu');

        _main_sidebar.find('.dropdown-menu').hide();

        _main_sidebar.removeAttr('style');

        _main_sidebar.removeAttr('tabindex');

        _main_sidebar.removeAttr('data-nav-classes');

        $(".main-wrapper").addClass("container"); // if(sidebar_nicescroll != null)
        //   sidebar_nicescroll.remove();
      } else if (now_layout_class == 'layout-2') {
        $("body").addClass("layout-2");
      } else {
        update_sidebar_nicescroll();
      }
    }
  };

  toggleLayout();
  $(window).resize(toggleLayout);
  $("[data-toggle='search']").click(function () {
    var body = $("body");

    if (body.hasClass('search-gone')) {
      body.addClass('search-gone');
      body.removeClass('search-show');
    } else {
      body.removeClass('search-gone');
      body.addClass('search-show');
    }
  }); // tooltip

  $("[data-toggle='tooltip']").tooltip(); // popover

  $('[data-toggle="popover"]').popover({
    container: 'body'
  }); // Select2

  if (jQuery().select2) {
    $(".select2").select2();
  } // Selectric


  if (jQuery().selectric) {
    $(".selectric").selectric({
      disableOnMobile: false,
      nativeOnMobile: false
    });
  }

  $(".notification-toggle").dropdown();
  $(".notification-toggle").parent().on('shown.bs.dropdown', function () {
    $(".dropdown-list-icons").niceScroll({
      cursoropacitymin: .3,
      cursoropacitymax: .8,
      cursorwidth: 7
    });
  });
  $(".message-toggle").dropdown();
  $(".message-toggle").parent().on('shown.bs.dropdown', function () {
    $(".dropdown-list-message").niceScroll({
      cursoropacitymin: .3,
      cursoropacitymax: .8,
      cursorwidth: 7
    });
  });

  if ($(".chat-content").length) {
    $(".chat-content").niceScroll({
      cursoropacitymin: .3,
      cursoropacitymax: .8
    });
    $('.chat-content').getNiceScroll(0).doScrollTop($('.chat-content').height());
  }

  if (jQuery().summernote) {
    $(".summernote").summernote({
      dialogsInBody: true,
      minHeight: 250
    });
    $(".summernote-simple").summernote({
      dialogsInBody: true,
      minHeight: 150,
      toolbar: [['style', ['bold', 'italic', 'underline', 'clear']], ['font', ['strikethrough']], ['para', ['paragraph']]]
    });
  }

  if (window.CodeMirror) {
    $(".codeeditor").each(function () {
      var editor = CodeMirror.fromTextArea(this, {
        lineNumbers: true,
        theme: "duotone-dark",
        mode: 'javascript',
        height: 200
      });
      editor.setSize("100%", 200);
    });
  } // Follow function


  $('.follow-btn, .following-btn').each(function () {
    var me = $(this),
        follow_text = 'Follow',
        unfollow_text = 'Following';
    me.click(function () {
      if (me.hasClass('following-btn')) {
        me.removeClass('btn-danger');
        me.removeClass('following-btn');
        me.addClass('btn-primary');
        me.html(follow_text);
        eval(me.data('unfollow-action'));
      } else {
        me.removeClass('btn-primary');
        me.addClass('btn-danger');
        me.addClass('following-btn');
        me.html(unfollow_text);
        eval(me.data('follow-action'));
      }

      return false;
    });
  }); // Dismiss function

  $("[data-dismiss]").each(function () {
    var me = $(this),
        target = me.data('dismiss');
    me.click(function () {
      $(target).fadeOut(function () {
        $(target).remove();
      });
      return false;
    });
  }); // Collapsable

  $("[data-collapse]").each(function () {
    var me = $(this),
        target = me.data('collapse');
    me.click(function () {
      $(target).collapse('toggle');
      $(target).on('shown.bs.collapse', function (e) {
        e.stopPropagation();
        me.html('<i class="fas fa-minus"></i>');
      });
      $(target).on('hidden.bs.collapse', function (e) {
        e.stopPropagation();
        me.html('<i class="fas fa-plus"></i>');
      });
      return false;
    });
  }); // Gallery

  $(".gallery .gallery-item").each(function () {
    var me = $(this);
    me.attr('href', me.data('image'));
    me.attr('title', me.data('title'));

    if (me.parent().hasClass('gallery-fw')) {
      me.css({
        height: me.parent().data('item-height')
      });
      me.find('div').css({
        lineHeight: me.parent().data('item-height') + 'px'
      });
    }

    me.css({
      backgroundImage: 'url("' + me.data('image') + '")'
    });
  });

  if (jQuery().Chocolat) {
    $(".gallery").Chocolat({
      className: 'gallery',
      imageSelector: '.gallery-item'
    });
  } // Background


  $("[data-background]").each(function () {
    var me = $(this);
    me.css({
      backgroundImage: 'url(' + me.data('background') + ')'
    });
  }); // Custom Tab

  $("[data-tab]").each(function () {
    var me = $(this);
    me.click(function () {
      if (!me.hasClass('active')) {
        var tab_group = $('[data-tab-group="' + me.data('tab') + '"]'),
            tab_group_active = $('[data-tab-group="' + me.data('tab') + '"].active'),
            target = $(me.attr('href')),
            links = $('[data-tab="' + me.data('tab') + '"]');
        links.removeClass('active');
        me.addClass('active');
        target.addClass('active');
        tab_group_active.removeClass('active');
      }

      return false;
    });
  }); // Bootstrap 4 Validation

  $(".needs-validation").submit(function () {
    var form = $(this);

    if (form[0].checkValidity() === false) {
      event.preventDefault();
      event.stopPropagation();
    }

    form.addClass('was-validated');
  }); // alert dismissible

  $(".alert-dismissible").each(function () {
    var me = $(this);
    me.find('.close').click(function () {
      me.alert('close');
    });
  });

  if ($('.main-navbar').length) {} // Image cropper


  $('[data-crop-image]').each(function (e) {
    $(this).css({
      overflow: 'hidden',
      position: 'relative',
      height: $(this).data('crop-image')
    });
  }); // Slide Toggle

  $('[data-toggle-slide]').click(function () {
    var target = $(this).data('toggle-slide');
    $(target).slideToggle();
    return false;
  }); // Dismiss modal

  $("[data-dismiss=modal]").click(function () {
    $(this).closest('.modal').modal('hide');
    return false;
  }); // Width attribute

  $('[data-width]').each(function () {
    $(this).css({
      width: $(this).data('width')
    });
  }); // Height attribute

  $('[data-height]').each(function () {
    $(this).css({
      height: $(this).data('height')
    });
  }); // Chocolat

  if ($('.chocolat-parent').length && jQuery().Chocolat) {
    $('.chocolat-parent').Chocolat();
  } // Sortable card


  if ($('.sortable-card').length && jQuery().sortable) {
    $('.sortable-card').sortable({
      handle: '.card-header',
      opacity: .8,
      tolerance: 'pointer'
    });
  } // Daterangepicker


  if (jQuery().daterangepicker) {
    if ($(".datepicker").length) {
      $('.datepicker').daterangepicker({
        locale: {
          format: 'YYYY-MM-DD'
        },
        singleDatePicker: true
      });
    }

    if ($(".datetimepicker").length) {
      $('.datetimepicker').daterangepicker({
        locale: {
          format: 'YYYY-MM-DD hh:mm'
        },
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true
      });
    }

    if ($(".daterange").length) {
      $('.daterange').daterangepicker({
        locale: {
          format: 'YYYY-MM-DD'
        },
        drops: 'down',
        opens: 'right'
      });
    }
  } // Timepicker


  if (jQuery().timepicker && $(".timepicker").length) {
    $(".timepicker").timepicker({
      icons: {
        up: 'fas fa-chevron-up',
        down: 'fas fa-chevron-down'
      }
    });
  }
});

/***/ }),

/***/ "./resources/js/stisla/stisla.js":
/*!***************************************!*\
  !*** ./resources/js/stisla/stisla.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function ($, window, i) {
  // Bootstrap 4 Modal
  $.fn.fireModal = function (options) {
    var options = $.extend({
      size: 'modal-md',
      center: false,
      animation: true,
      title: 'Modal Title',
      closeButton: true,
      header: true,
      bodyClass: '',
      footerClass: '',
      body: '',
      buttons: [],
      autoFocus: true,
      removeOnDismiss: false,
      created: function created() {},
      appended: function appended() {},
      onFormSubmit: function onFormSubmit() {},
      modal: {}
    }, options);
    this.each(function () {
      i++;
      var id = 'fire-modal-' + i,
          trigger_class = 'trigger--' + id,
          trigger_button = $('.' + trigger_class);
      $(this).addClass(trigger_class); // Get modal body

      var body = options.body;

      if (_typeof(body) == 'object') {
        if (body.length) {
          var part = body;
          body = body.removeAttr('id').clone().removeClass('modal-part');
          part.remove();
        } else {
          body = '<div class="text-danger">Modal part element not found!</div>';
        }
      } // Modal base template


      var modal_template = '   <div class="modal' + (options.animation == true ? ' fade' : '') + '" tabindex="-1" role="dialog" id="' + id + '">  ' + '     <div class="modal-dialog ' + options.size + (options.center ? ' modal-dialog-centered' : '') + '" role="document">  ' + '       <div class="modal-content">  ' + (options.header == true ? '         <div class="modal-header">  ' + '           <h5 class="modal-title">' + options.title + '</h5>  ' + (options.closeButton == true ? '           <button type="button" class="close" data-dismiss="modal" aria-label="Close">  ' + '             <span aria-hidden="true">&times;</span>  ' + '           </button>  ' : '') + '         </div>  ' : '') + '         <div class="modal-body">  ' + '         </div>  ' + (options.buttons.length > 0 ? '         <div class="modal-footer">  ' + '         </div>  ' : '') + '       </div>  ' + '     </div>  ' + '  </div>  '; // Convert modal to object

      var modal_template = $(modal_template); // Start creating buttons from 'buttons' option

      var this_button;
      options.buttons.forEach(function (item) {
        // get option 'id'
        var id = "id" in item ? item.id : ''; // Button template

        this_button = '<button type="' + ("submit" in item && item.submit == true ? 'submit' : 'button') + '" class="' + item["class"] + '" id="' + id + '">' + item.text + '</button>'; // add click event to the button

        this_button = $(this_button).off('click').on("click", function () {
          // execute function from 'handler' option
          item.handler.call(this, modal_template);
        }); // append generated buttons to the modal footer

        $(modal_template).find('.modal-footer').append(this_button);
      }); // append a given body to the modal

      $(modal_template).find('.modal-body').append(body); // add additional body class

      if (options.bodyClass) $(modal_template).find('.modal-body').addClass(options.bodyClass); // add footer body class

      if (options.footerClass) $(modal_template).find('.modal-footer').addClass(options.footerClass); // execute 'created' callback

      options.created.call(this, modal_template, options); // modal form and submit form button

      var modal_form = $(modal_template).find('.modal-body form'),
          form_submit_btn = modal_template.find('button[type=submit]'); // append generated modal to the body

      $("body").append(modal_template); // execute 'appended' callback

      options.appended.call(this, $('#' + id), modal_form, options); // if modal contains form elements

      if (modal_form.length) {
        // if `autoFocus` option is true
        if (options.autoFocus) {
          // when modal is shown
          $(modal_template).on('shown.bs.modal', function () {
            // if type of `autoFocus` option is `boolean`
            if (typeof options.autoFocus == 'boolean') modal_form.find('input:eq(0)').focus(); // the first input element will be focused
            // if type of `autoFocus` option is `string` and `autoFocus` option is an HTML element
            else if (typeof options.autoFocus == 'string' && modal_form.find(options.autoFocus).length) modal_form.find(options.autoFocus).focus(); // find elements and focus on that
          });
        } // form object


        var form_object = {
          startProgress: function startProgress() {
            modal_template.addClass('modal-progress');
          },
          stopProgress: function stopProgress() {
            modal_template.removeClass('modal-progress');
          }
        }; // if form is not contains button element

        if (!modal_form.find('button').length) $(modal_form).append('<button class="d-none" id="' + id + '-submit"></button>'); // add click event

        form_submit_btn.click(function () {
          modal_form.submit();
        }); // add submit event

        modal_form.submit(function (e) {
          // start form progress
          form_object.startProgress(); // execute `onFormSubmit` callback

          options.onFormSubmit.call(this, modal_template, e, form_object);
        });
      }

      $(document).on("click", '.' + trigger_class, function () {
        var modal = $('#' + id).modal(options.modal);

        if (options.removeOnDismiss) {
          modal.on('hidden.bs.modal', function () {
            modal.remove();
          });
        }

        return false;
      });
    });
  }; // Bootstrap Modal Destroyer


  $.destroyModal = function (modal) {
    modal.modal('hide');
    modal.on('hidden.bs.modal', function () {});
  }; // Card Progress Controller


  $.cardProgress = function (card, options) {
    var options = $.extend({
      dismiss: false,
      dismissText: 'Cancel',
      spinner: true,
      onDismiss: function onDismiss() {}
    }, options);
    var me = $(card);
    me.addClass('card-progress');

    if (options.spinner == false) {
      me.addClass('remove-spinner');
    }

    if (options.dismiss == true) {
      var btn_dismiss = '<a class="btn btn-danger card-progress-dismiss">' + options.dismissText + '</a>';
      btn_dismiss = $(btn_dismiss).off('click').on('click', function () {
        me.removeClass('card-progress');
        me.find('.card-progress-dismiss').remove();
        options.onDismiss.call(this, me);
      });
      me.append(btn_dismiss);
    }

    return {
      dismiss: function dismiss(dismissed) {
        $.cardProgressDismiss(me, dismissed);
      }
    };
  };

  $.cardProgressDismiss = function (card, dismissed) {
    var me = $(card);
    me.removeClass('card-progress');
    me.find('.card-progress-dismiss').remove();
    if (dismissed) dismissed.call(this, me);
  };

  $.chatCtrl = function (element, chat) {
    var chat = $.extend({
      position: 'chat-right',
      text: '',
      time: moment(new Date().toISOString()).format('hh:mm'),
      picture: '',
      type: 'text',
      // or typing
      timeout: 0,
      onShow: function onShow() {}
    }, chat);
    var target = $(element),
        element = '<div class="chat-item ' + chat.position + '" style="display:none">' + '<img src="' + chat.picture + '">' + '<div class="chat-details">' + '<div class="chat-text">' + chat.text + '</div>' + '<div class="chat-time">' + chat.time + '</div>' + '</div>' + '</div>',
        typing_element = '<div class="chat-item chat-left chat-typing" style="display:none">' + '<img src="' + chat.picture + '">' + '<div class="chat-details">' + '<div class="chat-text"></div>' + '</div>' + '</div>';
    var append_element = element;

    if (chat.type == 'typing') {
      append_element = typing_element;
    }

    if (chat.timeout > 0) {
      setTimeout(function () {
        target.find('.chat-content').append($(append_element).fadeIn());
      }, chat.timeout);
    } else {
      target.find('.chat-content').append($(append_element).fadeIn());
    }

    var target_height = 0;
    target.find('.chat-content .chat-item').each(function () {
      target_height += $(this).outerHeight();
    });
    setTimeout(function () {
      target.find('.chat-content').scrollTop(target_height, -1);
    }, 100);
    chat.onShow.call(this, append_element);
  };
})(jQuery, this, 0);

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! E:\pub\toko-aer\resources\js\app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! E:\pub\toko-aer\resources\sass\app.scss */"./resources/sass/app.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);