<?php

namespace Modules\Transaction\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Yajra\DataTables\Services\DataTable;

class SalesOrderDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.sales_order.detail', $row->id) . '">' . $row->code_number . '</a>';
                return $detail;
            })
            ->editColumn('department_id', function ($row) {
                return optional($row->department)->name;
            })
            ->editColumn('customer_id', function ($row) {
                $customer = optional($row->customer)->company_name . ' - ' . optional($row->customer)->pic_1;
                return $customer;
            })
            ->editColumn('salesman_id', function ($row) {
                return optional($row->salesman)->name;
            })
            ->editColumn('payment_term_id', function ($row) {
                return optional($row->paymentTerm)->name;
            })
            ->editColumn('approver_to_go', function ($row) {
                return $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval);
            })
            ->editColumn('latest_approver', function ($row) {
                return $row->latestApprove ? optional($row->latestApprove)->name : '-';
            })
            ->editColumn('employee_id', function ($row) {
                return optional($row->employee)->name;
            })
            ->editColumn('quotation_material_id', function ($row) {
                if($row->quotation_material_id){
                    $detail = '<a target="_blank" href="' . route('transaction.quotation_material.detail', optional($row->quotationMaterial)->id) . '">' .
                    optional($row->quotationMaterial)->code_number . '</a>';
                }else{
                    $detail = '-';
                }

                return $detail;
            })
            ->addColumn('delivery_order_id', function ($row) {
                $deliveryOrder = DeliveryOrderMaterial::whereNotNull('sales_order_id')->whereNull('deleted_at')->where('sales_order_id', $row->id)->get();

                    if(!empty($deliveryOrder)){
                        foreach($deliveryOrder as $do){
                            return '<a target="_blank" href="' . route('transaction.delivery_order_material.detail', $do->id) . '">' . $do->code_number . '</a>';
                        }
                    }else{
                        return '-';
                    }
            })
            ->addColumn('stock_qty', function ($row) {
                $stockChecking = SalesOrderItem::whereNotNull('stock_qty')->whereNull('deleted_at')->where('sales_order_id', $row->id)->where('stock_qty', '!=', 0)->first();

                if($stockChecking){
                    $stock = '<i class="fas fa-check"></i>';
                }else{
                    $stock = '-';
                }
                return $stock;
            })
            ->editColumn('status', function ($row) {
                $waiting = '<span class="badge badge-success">Waiting</span>';
                $rejected = '<span class="badge badge-danger">Rejected</span>';
                $approved = '<span class="badge badge-info">Approved</span>';

                if (empty($row->status)) {
                    return '-';
                } elseif ($row->status == 'waiting') {
                    return $waiting;
                } elseif ($row->status == 'approved') {
                    return $approved;
                } elseif ($row->status == 'rejected') {
                    return $rejected;
                }
            })
            ->addColumn('action', function ($row) {
                $openDiv = '<div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';

                $edit = '<a href="' . route('transaction.sales_order.edit', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="Edit"><i class="fa fa-edit" style="color: blue" aria-hidden="true"></i> Edit</a>';
                $pdf = '<a href="' . route('transaction.sales_order.pdf', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="View PDF"><i class="fa fa-file-pdf" style="color: orange" aria-hidden="true"></i> PDF</a>';
                $do =' <a href="' . route('transaction.delivery_order_material.create', ['id' => $row->id]) . '" style="margin-left: 10px" class="dropdown-item" title="Create DO">
                                <i class="fa fa-plus" style="color: orange"></i> Create DO</a>';

                $delete = '<a data-href="' . route('transaction.sales_order.destroy', $row->id) . '"data-toggle="modal" data-target="#confirm-delete-modal" style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Delete">
                                <i class="fa fa-trash" style="color: red" aria-hidden="true"></i> Delete</a>';

                $resubmit ='<a href="' . route('transaction.sales_order.edit', [$row->id, 'resubmit' => true]) . '" style="margin-left: 10px" class="dropdown-item" title="Resubmit">
                                <i class="fa fa-redo-alt" style="color: aqua" aria-hidden="true"></i> Resubmit</a>';

                $attachment ='<a data-href="' . route('transaction.sales_order.attachment', ['id' => $row->id]) . '" data-toggle="modal" data-target="#attachment" id="modal_attachment" data-id="'.$row->id.'" style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Add or view Attachment"><i class="fa fa-paperclip" style="color: green" aria-hidden="true"></i> Attachment</a>';
                $print = '<a href="' . route('transaction.sales_order.print', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="Print"><i class="fa fa-print" style="color: black" aria-hidden="true"></i> Print</a>';
                $so = SalesOrder::with('items')->whereId($row->id)
                ->where('status', 'approved')
                ->whereHas('items', function ($q){
                    $q->where('delivered_qty', '>' ,0);
                })
                ->first();
                $do='';
                if($so){
                    $do =' <a href="' . route('transaction.delivery_order_material.create', ['id' => $row->id]) . '" style="margin-left: 10px" class="dropdown-item" title="Create DO">
                                <i class="fa fa-plus" style="color: orange"></i> Create DO</a>';
                }


                if ($row->status == 'waiting') {
                    if ($row->latestApprove == null) {
                        return $openDiv . (userCan('edit sales order') ? $edit : '') . (userCan('delete sales order') ? $delete : '') . $attachment . $do  . $pdf. $print . $closeDiv;
                    }
                } elseif ($row->status == 'approved') {
                    return $openDiv . $do . $attachment . $pdf. $print . $closeDiv;
                } elseif ($row->status == 'rejected') {
                    return $openDiv . (userCan('edit sales order') ? $resubmit : '') . (userCan('delete sales order') ? $delete : '') . $attachment . $pdf . $print . $closeDiv;
                }
            })
            ->rawColumns(['action', 'status', 'code_number', 'quotation_material_id', 'delivery_order_id', 'stock_qty']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\SalesOrderDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SalesOrder $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        return $model
            ->with(['department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm'])
            ->where('employee_id', $employee->id)
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('salesorderdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->parameters([
                'order' => [
                    3,
                    'DESC'
                ]
            ])
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->addClass('text-center'),
            // Column::make('quotation_material_id')->title('Transfered From')->addClass('text-center'),
            // Column::make('stock_qty')->addClass('text-center')->title('Stock Checking'),
            // Column::make('delivery_order_id')->title('Delivery Order')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            // Column::make('title')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('customer_id')->title('Customer')->addClass('text-center'),
            Column::make('salesman_id')->title('Salesman')->addClass('text-center'),
            // Column::make('department_id')->title('Department')->addClass('text-center'),
            Column::make('payment_term_id')->title('Payment Term')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'SalesOrder_' . date('YmdHis');
    }
}
