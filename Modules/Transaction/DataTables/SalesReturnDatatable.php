<?php

namespace Modules\Transaction\DataTables;

use Modules\HR\Entities\Employee;
use Modules\Transaction\Entities\SalesReturn;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Modules\Transaction\Entities\SalesOrder;
use Yajra\DataTables\Services\DataTable;

class SalesReturnDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('customer_id', function ($row) {
                return optional($row->customer)->company_name;
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee ? optional($row->employee)->name : '-';
            })
            ->addColumn('action', function ($row) {
                $openDiv = '<div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';

                $edit = '<a href="' . route('transaction.sales-return.edit', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="Edit"><i class="fa fa-edit" style="color: blue" aria-hidden="true"></i> Edit</a>';
                $delete = '<a data-href="' . route('transaction.sales-return.destroy', $row->id) . '"data-toggle="modal" data-target="#confirm-delete-modal"  style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Delete">
                                <i class="fa fa-trash" style="color: red" aria-hidden="true"></i> Delete</a>';

                return $openDiv . (userCan('view sales return') ? $edit : '') . (userCan('delete sales return') ? $delete : '');
            })
            ->rawColumns(['status', 'action', 'invoice_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseInvoiceDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return SalesReturn::with('customer')->get(['id','date','code_number', 'customer_id', 'description','employee_id']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('purchaseinvoicedatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('customer_id')->title('Customer')->addClass('text-center'),
            Column::make('description')->title('Remarks')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseReturn' . date('YmdHis');
    }
}
