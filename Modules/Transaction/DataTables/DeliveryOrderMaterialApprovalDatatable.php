<?php

namespace Modules\Transaction\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Modules\Transaction\Entities\DeliveryOrderMaterialApproval;
use Yajra\DataTables\Services\DataTable;

class DeliveryOrderMaterialApprovalDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.delivery_order_material.detail', $row->delivery_order_material_id) . '">' . optional($row->deliveryOrderMaterial)->code_number . '</a>';
                return $detail;
            })
            ->editColumn('employee', function ($row) {
                return optional($row->employee)->name;
            })
            ->editColumn('sales_order_id', function ($row) {
                if($row->deliveryOrderMaterial->sales_order_id){
                    $detail = '<a target="_blank" href="' . route('transaction.sales_order.detail', optional($row->deliveryOrderMaterial->salesOrder)->id) . '">' . 
                    optional($row->deliveryOrderMaterial->salesOrder)->code_number . '</a>';
                }else{
                    $detail = '-';
                }
        
                return $detail;
            })
            ->editColumn('title', function ($row) {
                return optional($row->deliveryOrderMaterial)->title;
            })
            ->editColumn('is_approved', function ($row) {
                if ($row->is_approved == 0) {
                    return "Rejected";
                }elseif ($row->is_approved == 1) {
                    return "Approved";
                }
            })
            ->rawColumns(['status', 'code_number', 'sales_order_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\DeliveryOrderMaterialApprovalDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(DeliveryOrderMaterialApproval $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        $row = $model
            ->with(['deliveryOrderMaterial', 'deliveryOrderMaterial.employee', 'deliveryOrderMaterial.salesOrder'])
            ->where('employee_id', $employee->id)
            ->whereNotNull('date')
            ->latest()
            ->get()
            ->unique('delivery_order_material_id');
        return $row;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('deliveryordermaterialapprovaldatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    // ->parameters([
                    //     'order' => [
                    //         5,
                    //         'DESC'
                    //     ]
                    // ])
                    ->buttons([
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number'),
            Column::make('sales_order_id')->addClass('text-center')->title('Transfered From'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee')->addClass('text-center')->title('Created By'),
            // Column::make('title')->addClass('text-center'),
            Column::make('is_approved')->addClass('text-center')->title('Approval Status'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DeliveryOrderMaterialApproval_' . date('YmdHis');
    }
}
