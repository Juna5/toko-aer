<?php

namespace Modules\Transaction\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\DeliveryOrderMaterial;

class DeliveryOrderMaterialOutstandingDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            // ->editColumn('sales_order_id', function ($row) {
            //     $detail = '<a href="' . route('transaction.sales_order.detail', $row->id) . '">' . optional($row->salesOrder)->code_number . '</a>';
            //     return $detail;
            // })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.sales_order.detail', $row->id) . '">' . $row->code_number . '</a>';
                return $detail;
            })
            ->editColumn('department_id', function ($row) {
                return optional($row->salesOrder)->name;
            })
            ->editColumn('customer_id', function ($row) {
                $customer = optional($row->customer)->company_name . ' - ' . optional($row->customer)->pic_1;
                return $customer;
            })
            ->editColumn('salesman_id', function ($row) {
                return optional($row->salesman)->name;
            })
            ->editColumn('approver_to_go', function ($row) {
                return $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval);
            })
            ->editColumn('latest_approver', function ($row) {
                return $row->latestApprove ? optional($row->latestApprove)->name : '-';
            })
            ->editColumn('employee_id', function ($row) {
                return optional($row->employee)->name;
            })
            // ->editColumn('sales_order_id', function ($row) {
            //     if($row->sales_order_id){
            //         $detail = '<a target="_blank" href="' . route('transaction.sales_order.detail', optional($row->salesOrder)->id) . '">' . 
            //         optional($row->salesOrder)->code_number . '</a>';
            //     }else{
            //         $detail = '-';
            //     }
        
            //     return $detail;
            // })
            ->addColumn('action', function ($row) {
                
                $openDiv = '<div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';
                $deliveryOrder = "";
                    
                        $deliveryOrder = '<a href="' . route('transaction.delivery_order_material.create', ['id' => $row->id]) . '"
                        style="margin-left: 10px" class="dropdown-item" title="Create Delivery Order">
                        <i class="fa fa-file" style="color: #046ea8"></i> Delivery Order
                        </a>';
                    
                return $openDiv . $deliveryOrder . $closeDiv;
            })
            ->rawColumns(['action', 'code_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\DeliveryOrderMaterialOutstandingDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SalesOrder $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        $stockChecking = SalesOrderItem::whereNotNull('stock_qty')->get();

        $deliveryOrder = DeliveryOrderMaterial::whereNotNull('sales_order_id')->get();


        return $model
            ->with(['department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm'])
            ->where('employee_id', $employee->id)
            ->whereNotIn('id', $deliveryOrder->pluck('sales_order_id'))
            ->whereNotIn('id', $stockChecking->pluck('stock_qty'))
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('deliveryordermaterialoutstandingdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons([
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::make('sales_order_id')->title('Sales Order Number')->addClass('text-center'),
            Column::make('code_number')->title('Sales Order Number')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('customer_id')->title('Customer')->addClass('text-center'),
            Column::make('salesman_id')->title('Salesman')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),
            Column::make('approver_to_go')->title('Next Approver')->addClass('text-center'),
            Column::make('latest_approver')->title('Final Approver')->addClass('text-center'),
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'DeliveryOrderMaterialOutstanding_' . date('YmdHis');
    }
}
