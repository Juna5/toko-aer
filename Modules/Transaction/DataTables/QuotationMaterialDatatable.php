<?php

namespace Modules\Transaction\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Modules\Transaction\Entities\QuotationMaterial;
use Yajra\DataTables\Services\DataTable;

class QuotationMaterialDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.quotation_material.detail', $row->id) . '">' . $row->code_number . '</a>';
                return $detail;
            })
            ->editColumn('department_id', function ($row) {
                return optional($row->department)->name;
            })
            ->editColumn('customer_id', function ($row) {
                $customer = optional($row->customer)->company_name . ' - ' . optional($row->customer)->pic_1;
                return $customer;
            })
            ->editColumn('salesman_id', function ($row) {
                return optional($row->salesman)->name;
            })
            ->editColumn('payment_term_id', function ($row) {
                return optional($row->paymentTerm)->name;
            })
            ->editColumn('approver_to_go', function ($row) {
                return $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval);
            })
            ->editColumn('latest_approver', function ($row) {
                return $row->latestApprove ? optional($row->latestApprove)->name : '-';
            })
            ->editColumn('valid_until', function ($row) {
                return format_d_month_y($row->valid_until);
            })
            ->editColumn('employee', function ($row) {
                return optional($row->employee)->name;
            })
            ->editColumn('status', function ($row) {
                $waiting = '<span class="badge badge-success">Waiting</span>';
                $rejected = '<span class="badge badge-danger">Rejected</span>';
                $approved = '<span class="badge badge-info">Approved</span>';

                if (empty($row->status)) {
                    return '-';
                } elseif ($row->status == 'waiting') {
                    return $waiting;
                } elseif ($row->status == 'approved') {
                    return $approved;
                } elseif ($row->status == 'rejected') {
                    return $rejected;
                }
            })
            ->addColumn('action', function ($row) {
                $openDiv = '<div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';

                $edit = '<a href="' . route('transaction.quotation_material.edit', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="Edit"><i class="fa fa-edit" style="color: blue" aria-hidden="true"></i> Edit</a>';

                $delete = '<a data-href="' . route('transaction.quotation_material.destroy', $row->id) . '"data-toggle="modal" data-target="#confirm-delete-modal"  style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Delete">
                                <i class="fa fa-trash" style="color: red" aria-hidden="true"></i> Delete</a>';

                $resubmit ='<a href="' . route('transaction.quotation_material.edit', [$row->id, 'resubmit' => true]) . '"  style="margin-left: 10px" class="dropdown-item" title="Resubmit">
                                <i class="fa fa-redo-alt" style="color: aqua" aria-hidden="true"></i> Resubmit</a>' ;

                $so =' <a href="' . route('transaction.sales_order.create', ['id' => $row->id]) . '" style="margin-left: 10px" class="dropdown-item" title="Create SO">
                                <i class="fa fa-plus" style="color: orange"></i> Create SO</a>';

                 $attachment ='<a data-href="' . route('transaction.quotation_material.attachment', ['id' => $row->id]) . '" data-toggle="modal" data-target="#attachment" id="modal_attachment" data-id="'.$row->id.'" style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Add or view Attachment"><i class="fa fa-paperclip" style="color: green" aria-hidden="true"></i> Attachment</a>';

                 $pdf  = '<a href="' . route('transaction.quotation_material.pdf', [$row->id, 'report' => false]) . '"
                                style="margin-left: 10px" class="dropdown-item" title="View PDF" target="_blank">
                                <i class="fa fa-file-pdf" style="color: orange" aria-hidden="true"></i> PDF
                        </a>';

                if ($row->status == 'waiting') {
                    if ($row->latestApprove == null) {
                        return $openDiv . (userCan('edit quotation material') ? $edit : '') . (userCan('delete quotation material') ? $delete : '') . $attachment . $closeDiv;
                    }else{
                        return $openDiv . $pdf . $closeDiv;
                    }
                } elseif ($row->status == 'approved') {
                    return $openDiv . $so . $attachment . $pdf. $closeDiv;
                } elseif ($row->status == 'rejected') {
                    return  $openDiv . (userCan('edit quotation material') ? $resubmit : '') . (userCan('delete quotation material') ? $delete : '') . $attachment . $closeDiv;
                }
            })
            ->rawColumns(['action', 'status', 'code_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\QuotationMaterialDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(QuotationMaterial $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        return $model
            ->with(['department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm'])
            ->where('employee_id', $employee->id)
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('purchaserequisitiondatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->parameters([
                'order' => [
                    3,
                    'DESC'
                ]
            ])
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            // Column::make('title')->addClass('text-center'),
            Column::make('employee')->title('Created By')->addClass('text-center'),
            Column::make('customer_id')->title('Customer')->addClass('text-center'),
            Column::make('salesman_id')->title('Salesman')->addClass('text-center'),
            // Column::make('department_id')->title('Department')->addClass('text-center'),
            Column::make('payment_term_id')->title('Payment Term')->addClass('text-center'),
            Column::make('valid_until')->addClass('text-center'),
            Column::make('approver_to_go')->title('Next Approver')->addClass('text-center'),
            Column::make('latest_approver')->title('Final Approver')->addClass('text-center'),
            Column::make('status')->title('Approval Status')->addClass('text-center'),
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'QuotationMaterial_' . date('YmdHis');
    }
}
