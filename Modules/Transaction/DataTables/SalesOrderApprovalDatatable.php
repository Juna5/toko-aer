<?php

namespace Modules\Transaction\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Yajra\DataTables\Services\DataTable;

class SalesOrderApprovalDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.sales_order.detail', $row->sales_order_id) . '">' . optional($row->salesOrder)->code_number . '</a>';
                return $detail;
            })
            ->editColumn('quotation_material_id', function ($row) {
                if($row->salesOrder->quotation_material_id){
                    $detail = '<a target="_blank" href="' . route('transaction.quotation_material.detail', optional($row->salesOrder->quotationMaterial)->id) . '">' . 
                    optional($row->salesOrder->quotationMaterial)->code_number . '</a>';
                }else{
                    $detail = '-';
                }
        
                return $detail;
            })
            ->editColumn('employee', function ($row) {
                return $row->employee->name;
            })
            ->editColumn('title', function ($row) {
                return $row->salesOrder->title;
            })
            ->addColumn('delivery_order_id', function ($row) {    
                $deliveryOrder = DeliveryOrderMaterial::whereNotNull('sales_order_id')->whereNull('deleted_at')->where('sales_order_id', $row->sales_order_id)->get();
                      
                    if(!empty($deliveryOrder)){
                        foreach($deliveryOrder as $do){
                            return '<a target="_blank" href="' . route('transaction.delivery_order_material.detail', $do->id) . '">' . $do->code_number . '</a>';   
                        }
                    }else{
                        return '-';
                    }
            })
            ->addColumn('stock_qty', function ($row) {
                $stockChecking = SalesOrderItem::whereNotNull('stock_qty')->whereNull('deleted_at')->where('sales_order_id', $row->sales_order_id)->where('stock_qty', '!=', 0)->first();
                 
                if($stockChecking){
                    $stock = '<i class="fas fa-check"></i>';
                }else{
                    $stock = '-';
                }
                return $stock;
            })
            ->editColumn('is_approved', function ($row) {
                if ($row->is_approved == 0) {
                    return "Rejected";
                }elseif ($row->is_approved == 1) {
                    return "Approved";
                }
            })
            ->rawColumns(['status', 'code_number', 'quotation_material_id', 'delivery_order_id', 'stock_qty']);
            ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\SalesOrderApprovalDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SalesOrderApproval $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        $row = $model
            ->with(['salesOrder', 'salesOrder.employee', 'salesOrder.quotationMaterial'])
            ->where('employee_id', $employee->id)
            ->whereNotNull('date')
            ->latest()
            ->get()
            ->unique('sales_order_id');
        return $row;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('salesorderdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->parameters([
                'order' => [
                    0,
                    'DESC'
                ]
            ])
            ->buttons([
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number'),
            Column::make('quotation_material_id')->addClass('text-center')->title('Transfered From'),
            Column::make('stock_qty')->addClass('text-center')->title('Stock Checking'),
            Column::make('delivery_order_id')->title('Delivery Order')->addClass('text-center'),
            Column::make('employee')->addClass('text-center')->title('Created By'),
            Column::make('date')->addClass('text-center'),
            // Column::make('title')->addClass('text-center'),
            Column::make('is_approved')->addClass('text-center')->title('Approval Status'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'SalesOrder_' . date('YmdHis');
    }
}
