<?php

namespace Modules\Transaction\Http\Controllers;

use App\Services\InvoiceStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\HR\Entities\Employee;
use Modules\Finance\Entities\Invoice;
use Modules\Finance\Entities\InvoiceDetail;
use Modules\Transaction\DataTables\SalesReturnDatatable;
use Modules\Transaction\Entities\SalesReturn;
use Modules\Transaction\Entities\SalesReturnDetail;

class SalesReturnController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SalesReturnDatatable $datatable)
    {
        $this->hasPermissionTo('view sales return');
        return $datatable->render('transaction::sales_return.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('view sales return');
        return view('transaction::sales_return.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->hasPermissionTo('add sales return');
        DB::beginTransaction();
        try{
            $request->validate([
                'customer_id' => 'required|integer',
                'invoice_id' => 'required|integer',
                'date' => 'required|date',
            ]);

            $invoice = Invoice::find($request->invoice_id);
            if ($request->grand_total > $invoice->outstanding){
                return response()->json([
                    'status' => 422,
                    'message' => 'Return Qty must be less than Purchase Qty.'
                ]);
            }

            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('sales return');
            $code_number = generate_code_sequence('J/SRT', $sequence_number, 3);

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $salesReturn = SalesReturn::create([
                'code_number' => $request->code_number ?? $code_number,
                'customer_id' => $request->customer_id,
                'invoice_id' => $request->invoice_id,
                'date' => $request->date,
                'description' => $request->remarks,
                'grand_total' => $request->grand_total,
                'subtotal_exclude_discount' => $request->subtotal_exclude_discount,
                'discount_all_item' => $request->discount_all_item,
                'subtotal_include_discount' => $request->subtotal_include_discount,
                'discount_gst' => $request->discount_gst,
                'amount_gst' => $request->amount_gst,
                'employee_id' => $employee->id,
            ]);
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }

        try{
            $details = collect(request('details'));
            foreach ($details as $detail){
                foreach ($invoice->invoiceDetails as $invoiceDetail){
                    if ($detail['qty'] > $invoiceDetail->returnable_qty){
                        return response()->json([
                            'status' => 422,
                            'message' => 'Returned Qty Cannot Bigger Than Current Qty.'
                        ]);
                    }
                }
                SalesReturnDetail::create([
                    'sales_return_id' => $salesReturn->id,
                    'invoice_detail_id' => $detail['id'],
                    'item' => $detail['item'],
                    'uom' => $detail['uom'],
                    'qty' => $detail['qty'],
                    'price' => $detail['price'],
                    'discount_amount' => $detail['discount_amount'],
                    'sub_total' => $detail['sub_total']
                ]);

                # update sales invoice returnable qty.
                $updateQty = new \App\Services\PurchaseReturn;
                $updateQty->updateSalesReturnableQty($detail['id'], $detail['qty']);
            }
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }

        try{
            # sum total for get status invoice
            $invoiceStatus = new InvoiceStatus;
            $invoiceStatus->changeStatusInvoice($salesReturn, $salesReturn->invoice_id, Invoice::class);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        DB::commit();

        return response()->json([
            'status' => 200,
            'message' => 'Data has been saved successfully'
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('transaction::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->hasPermissionTo('edit sales return');
        $return = SalesReturn::with('details', 'customer', 'invoice', 'details.invoiceDetail')
            ->whereHas('details', function ($query){
                $query->orderBy('created_at', 'ASC');
            })
            ->findOrFail($id);

        return view('transaction::sales_return.edit', compact('return'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit sales return');
        DB::beginTransaction();
        $request->validate([
            'customer_id' => 'required|integer',
            'invoice_id' => 'required|integer',
            'date' => 'required|date',
        ]);

        try{
            $purchaseInvoice = Invoice::find($request->invoice_id);
            if ($request->grand_total > $purchaseInvoice->outstanding){
                return response()->json([
                    'status' => 422,
                    'message' => 'Return Qty must be less than Purchase Qty.'
                ]);
            }

            $purchaseReturn = SalesReturn::findOrFail($id);
            $purchaseReturn->update([
                'date' => $request->date,
                'description' => $request->remarks,
                'grand_total' => $request->grand_total,
                'subtotal_exclude_discount' => $request->subtotal_exclude_discount,
                'discount_all_item' => $request->discount_all_item,
                'subtotal_include_discount' => $request->subtotal_include_discount,
                'discount_gst' => $request->discount_gst,
                'amount_gst' => $request->amount_gst
            ]);
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }

        try{
            $details = collect(request('details'));
            foreach ($details as $detail){
                foreach ($purchaseInvoice->invoiceDetails as $invoiceDetail){
                    if ($detail['qty'] > $invoiceDetail->returnable_qty){
                        return response()->json([
                            'status' => 422,
                            'message' => 'Returned Qty Cannot Bigger Than Current Qty.'
                        ]);
                    }
                }
                $purchaseInvoiceDetail = InvoiceDetail::find($detail['invoice_detail_id']);

                $returnDetail = SalesReturnDetail::find($detail['id']);
                $returnDetail->update([
                    'qty' => $detail['qty'],
                    'price' => $detail['price'],
                    'discount_amount' => $detail['discount_amount'],
                    'sub_total' => $detail['sub_total']
                ]);

                # update purchase invoice returnable qty.
                $result = SalesReturnDetail::where('invoice_detail_id', $detail['invoice_detail_id'])->get();
                $purchaseInvoiceDetail->update([
                    'returnable_qty' =>  $detail['invoice_qty'] - $result->sum('qty')
                ]);
            }
        }catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }

        try{
            # sum total for get status invoice
            $invoiceStatus = new InvoiceStatus;
            $invoiceStatus->changeStatusInvoice($purchaseReturn, $purchaseReturn->invoice_id, Invoice::class);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        DB::commit();

        return response()->json([
            'status' => 200,
            'message' => 'Data has been updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete sales return');
        DB::beginTransaction();
        try{
            $return = SalesReturn::find($id);

            # sum total for get and change status invoice and return back returnable qty on purchase invoice detail
            foreach ($return->details as $detail){
                $purchaseInvoiceDetail = InvoiceDetail::find($detail->invoice_detail_id);
                $purchaseInvoiceDetail->update([
                    'returnable_qty' => $purchaseInvoiceDetail->returnable_qty + $detail->qty
                ]);
            }
            $puchaseInvoice = Invoice::find($return->invoice_id);
            $puchaseInvoice->update([
                'outstanding' => $puchaseInvoice->outstanding + $return->grand_total
            ]);
            $puchaseInvoice->outstanding == $puchaseInvoice->total ?
                $puchaseInvoice->update(['payment_status' => 'Unpaid']) :
                $puchaseInvoice->update(['payment_status' => 'Paid Partially']);

            $return->delete();
        } catch (\Exception $e){
            DB::rollBack();
            return $e->getMessage();
        }
        DB::commit();

        noty()->danger('Cool!', 'Your data has been deleted');
        return back();
    }

    public function report()
    {
        $salesReturn = SalesReturn::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $salesReturn->whereBetween('date', [$fromDate, $untilDate])->orderBy('date', 'desc')->paginate(10);
        }

        return view('transaction::sales_return.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $row = SalesReturn::findOrFail($id);
        $salesReturnDetail = SalesReturnDetail::where('sales_return_id', $row->id)->get();

        return view('transaction::sales_return.report.detail', compact('row', 'salesReturnDetail'));
    }
}
