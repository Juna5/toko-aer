<?php

namespace Modules\Transaction\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Transaction\DataTables\CreditNoteDatatable;
use Modules\Procurement\DataTables\DebitNoteDatatable;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Finance\Entities\Cost;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\JournalTotal;
use Modules\Finance\Entities\Receivable;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Customer;
use Modules\Transaction\Entities\CreditNote;
use Modules\Procurement\Entities\DebitNote;
use Modules\Transaction\Http\Requests\CreditNoteRequest;
use Modules\Procurement\Http\Requests\DebitNoteRequest;

class CreditNoteController extends Controller
{
    public function index(CreditNoteDatatable $datatable, DebitNoteDatatable $debitNoteDatatable)
    {
        $this->hasPermissionTo('view credit note');
        // $debit = DebitNoteDatatable;
        return $datatable->render('transaction::credit_note.index');
    }

    public function create()
    {
        $this->hasPermissionTo('view credit note');

        $banks = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $charts = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $customers = Customer::all();

        return view('transaction::credit_note.create', compact('banks', 'charts','customers'));
    }

    public function store(CreditNoteRequest $request)
    {
        dd($request->all());
        $this->hasPermissionTo('add credit note');

        DB::beginTransaction();
        try {
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('credit note');
            // DD($sequence_number);
            $code_number = generate_code_sequence('J/CN', $sequence_number, 3);

            $journal = Journal::create([
                'journal_number' => $code_number,
                'journal_date' => $request->date,
                'description' => 'Credit Note' .' | '. $request->description,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            if($request->cn_dn == 1){
                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->bank,
                    'description' => $request->description,
                    'debit' => 0,
                    'credit' => $request->amount,
                ]);

                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->charge_of_account_id,
                    'description' => $request->description,
                    'debit' => $request->amount,
                    'credit' => 0,
                ]);
            }else{
                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->bank,
                    'description' => $request->description,
                    'debit' => $request->amount,
                    'credit' => 0,
                ]);

                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->charge_of_account_id,
                    'description' => $request->description,
                    'debit' => 0,
                    'credit' => $request->amount,
                ]);
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            if($request->cn_dn == 1){
               JournalTotal::create([
                'journal_id' => $journal->id,
                'total_credit' => $request->amount,
                'total_debit' => $request->amount,
                ]); 
            }else{
               JournalTotal::create([
                'journal_id' => $journal->id,
                'total_credit' => $request->amount,
                'total_debit' => $request->amount,
                ]);
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            if($request->cn_dn == 1){
                $employee = Employee::where('user_id', auth()->user()->id)->first();
                $credit_note = new CreditNote();
                $credit_note->charge_of_account_bank_id = $request->bank;
                $credit_note->charge_of_account_id = $request->charge_of_account_id;
                $credit_note->journal_id = $journal->id;
                $credit_note->customer_id = $request->customer_id;
                $credit_note->receivable_id = $request->receivable_id;
                $credit_note->date = $request->date;
                $credit_note->amount = $request->amount;
                $credit_note->description = $request->description;
                $credit_note->employee_id = $employee->id;
                $credit_note->save();
            }else{
                $employee = Employee::where('user_id', auth()->user()->id)->first();
                $debit_note = new DebitNote();
                $debit_note->charge_of_account_bank_id = $request->bank;
                $debit_note->charge_of_account_id = $request->charge_of_account_id;
                $debit_note->journal_id = $journal->id;
                $debit_note->supplier_id = $request->supplier_id;
                $debit_note->payable_id = $request->payable_id;
                $debit_note->date = $request->date;
                $debit_note->amount = $request->amount;
                $debit_note->description = $request->description;
                $debit_note->employee_id = $employee->id;
                $debit_note->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('transaction.credit_note.index');
    }

    public function show($id)
    {
        return view('transaction::show');
    }

    public function edit(CreditNote $credit_note, $id)
    {

       
        $credit_note = CreditNote::findOrFail($id);

        $this->hasPermissionTo('edit credit note');

        $banks = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $charts = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $customers = Customer::all();
        $receivables = Receivable::all();
        $receivableDetail = Receivable::with('invoice', 'journal', 'customer', 'details', 'chartOfAccount','details.invoice')->find($credit_note->receivable_id);
        // dd($receivableDetail->details);
        $grand_total = $receivableDetail->details->sum('amount');
        // dd($data);

        return view('transaction::credit_note.edit', compact('credit_note', 'banks', 'charts','customers','receivables','receivableDetail','grand_total'));
    }

    public function update(CreditNoteRequest $request, $id)
    {
       
        $this->hasPermissionTo('edit credit note');

        DB::beginTransaction();
        try {
            $credit_note = CreditNote::findOrFail($id);
            $credit_note->charge_of_account_bank_id = $request->bank;
            $credit_note->charge_of_account_id = $request->charge_of_account_id;
            $credit_note->customer_id = $request->customer_id;
            $credit_note->receivable_id = $request->receivable_id;
            $credit_note->date = $request->date;
            $credit_note->amount = $request->amount;
            $credit_note->description = $request->description;
            $credit_note->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $journal = Journal::find($credit_note->journal_id);
            $journal->journal_date = $request->date;
            $journal->description = 'Credit Note' .' | '. $request->description;
            $journal->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $journalItems = JournalItem::where('journal_id', $journal->id)->get();
            $journalItems->map(function ($item, $key) use ($request) {
                if ($item->debit == 0 && $item->credit != 0){
                    $item = JournalItem::find($item->id);
                    $item->chart_of_account_id = in_array($item->chart_of_account_id, [8,9]) ? $request->bank : $request->charge_of_account_id;
                    $item->description = $request->description;
                    $item->credit = $request->amount;
                    $item->save();
                } else if($item->debit != 0 && $item->credit == 0){
                    $item = JournalItem::find($item->id);
                    $item->chart_of_account_id = in_array($item->chart_of_account_id, [8,9]) ? $request->bank : $request->charge_of_account_id;
                    $item->description = $request->description;
                    $item->debit = $request->amount;
                    $item->save();
                }
            });
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $total = JournalTotal::where('journal_id', $journal->id)->firstOrFail();
            $total->total_credit = $request->amount;
            $total->total_debit = $request->amount;
            $total->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('transaction.credit_note.index');
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete credit note');

        DB::beginTransaction();
        try {
            $credit_note = CreditNote::findOrFail($id);
            $journal = Journal::findOrFail($credit_note->journal_id);
            $items = JournalItem::where('journal_id', $journal->id)->get();
            foreach ($items as $item) {
                $item->delete();
            }
            $journal->delete();
            JournalTotal::where('journal_id', $journal->id)->first()->delete();
            $credit_note->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

     public function getReceivable()
    {
        $set = request()->id;
        $receivable = Receivable::where('customer_id', $set)->get();

        switch (request()->type):
            case 'receivable':
                $return = '<option value="">--Please Select--</option>';
        foreach ($receivable as $temp) {
            $return .= "<option value='$temp->id'>$temp->code_number</option>";
        }
        return $return;
        break;
        endswitch;
    }

    public function report()
    {
        $creditNote = CreditNote::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $creditNote->whereBetween('date', [$fromDate, $untilDate])->get();
        }

        return view('transaction::credit_note.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $creditNote = CreditNote::with('employee')->findOrFail($id);
        $banks = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $charts = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $customers = Customer::all();
        $receivables = Receivable::all();
        $receivableDetail = Receivable::with('invoice', 'journal', 'customer', 'details', 'chartOfAccount','details.invoice')->find($creditNote->receivable_id);
        $grand_total = $receivableDetail->details->sum('amount');

        return view('transaction::credit_note.report.detail', compact('creditNote', 'banks', 'charts','customers','receivables','receivableDetail','grand_total'));
    }
}
