<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Finance\Entities\Receivable;
use Modules\Procurement\Entities\Customer;


class CreditNoteController extends Controller
{

    public function getReceivable()
    {
        $set = request()->id;
        $receivable = Receivable::with('invoice', 'journal', 'customer', 'details', 'chartOfAccount','details.invoice')->where('customer_id', $set)
        ->whereHas('details', function($detail){
            $detail->whereNull('deleted_at')->whereHas('invoice', function($invoice){
                $invoice->whereNull('deleted_at');
            });
        })
        ->get();

        switch (request()->type):
            case 'receivable':
                $return = '<option value="">--Please Select--</option>';
        foreach ($receivable as $temp) {
            $return .= "<option value='$temp->id'>$temp->code_number</option>";
        }
        return $return;
        break;
        endswitch;
    }

    public function getReceivableDetail($id)
    {
        $receivable = Receivable::with('invoice', 'journal', 'customer', 'details', 'chartOfAccount','details.invoice')->find($id);
        $sukes = 'sukses';
        return response()->json($receivable);
    }

    public function getCustomer(){
        $id = request()->id;
        $customer = Customer::findOrFail($id);
        return $customer;
    }

}
