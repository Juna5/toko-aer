<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Modules\Procurement\Entities\Project;
use App\Http\Controllers\ApiController;

class TransactionController extends ApiController
{
    public function project()
    {
        return Project::all();
    }
}
