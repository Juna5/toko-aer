<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Transaction\Entities\QuotationMaterial;
use Modules\Transaction\Entities\QuotationMaterialItem;

class QuotationMaterialController extends Controller
{
    public function edit($id)
	{
		return QuotationMaterial::with(['project','department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])->find($id);
	}

    public function removeItem($id)
	{
		QuotationMaterialItem::find($id)->forceDelete();
		return response()->json([
			'success' => true
		]);
	}
}
