<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;

class SalesOrderController extends Controller
{
    public function edit($id)
	{
		return SalesOrder::with(['project', 'department', 'currency', 'quotationMaterial','customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])->find($id);
	}

    public function removeItem($id)
	{
		SalesOrderItem::find($id)->forceDelete();
		return response()->json([
			'success' => true
		]);
	}
}
