<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\ProductPrice;
use Modules\Procurement\Entities\ProductSubCategory;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Entities\Location;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Http\Requests\ProductRequest;

class ProductController extends Controller
{
    public function store(ProductRequest $request)
	{
        $row = Product::create([
            'code' => $request->code,
            'name' => $request->name,
            'product_category_id' => $request->product_category_id,
            'product_sub_category_id' => $request->product_sub_category_id,
        ]);
        $priceCategory = PriceCategory::all();
        foreach ($priceCategory as $value) {
           $price = ProductPrice::create([
                'product_id'            => $row->id,
                'price_category_id'     => $value->id,
                'price'                 => $request->price,
            ]);
        }
		return $priceCategory;
	}

    public function productCategory()
	{
		return ProductCategory::all();
	}

    public function location()
	{
		return Location::all();
	}

    public function priceCategory()
    {
        return PriceCategory::all();
    }

    public function productSubCategory($id)
    {
        $subCategory = ProductSubCategory::where('product_category_id' ,$id)->get();
        return $subCategory;
    }

    public function productDetail($id)
    {
        return Product::with('uom')->findOrFail($id);
    }

    public function productPrice($productId, $uomId)
    {
        return ProductPrice::where('product_id', $productId)->where('uom_id', $uomId)->first();
    }

    public function priceHistory()
    {
        $priceHistory = SalesOrderItem::with('product', 'salesOrder')->whereHas('salesOrder', function ($q) use ($id) {
            $q->whereProductId($id);
        })->get();
    }


    public function billingHistory(){
        $id = request()->id;
        $priceHistory = SalesOrderItem::with('product', 'salesOrder','salesOrder.customer')->whereHas('salesOrder', function ($q) use ($id) {
            $q->whereProductId($id);
        })->orderBy('id', 'DESC')
            ->take(20)
            ->get();

        return $priceHistory;
    }

    public function procurementHistory(){
         $id = request()->id;
         $priceHistoryPo = PurchaseOrderItem::with('product', 'purchaseOrder','purchaseOrder.supplier')->whereHas('purchaseOrder', function ($q) use ($id) {
            $q->whereProductId($id);
        })->orderBy('id', 'DESC')
            ->take(20)
            ->get();

        return $priceHistoryPo;
    }


}
