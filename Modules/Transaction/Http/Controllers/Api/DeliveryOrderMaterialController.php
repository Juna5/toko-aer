<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;

use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Modules\Transaction\Entities\DeliveryOrderMaterialItem;
use Modules\Transaction\Entities\SalesOrderItem;

class DeliveryOrderMaterialController extends Controller
{
    public function edit($id)
	{
		return DeliveryOrderMaterial::with(['department','customer', 'salesman', 'paymentTerm', 'salesOrder', 'items', 'items.salesOrderMaterialItem.product', 'items.salesOrderMaterialItem.uom', 'items.warehouse'])->find($id);
	}

    public function getItem($id)
    {
        $row = DB::select("select dis.* from sales_orders so
        inner join delivery_order_materials dos on dos.sales_order_id = so.id
        inner join delivery_order_material_items dis on dis.delivery_order_material_id = dos.id
        where so.id = $id" );

        return $row;
    }

    public function removeItem($id)
	{
        $row = DeliveryOrderMaterialItem::find($id);
        $salesOrderItem = SalesOrderItem::find($row->sales_order_item_id);
        $salesOrderItem->delivered_qty = $salesOrderItem->delivered_qty + $row->qty;
        $salesOrderItem->save();
        DeliveryOrderMaterial::where('sales_order_id', $salesOrderItem->sales_order_id)->update(['status_partial' => 0]);
        $row->forceDelete();
		return response()->json([
			'success' => true
		]);
	}
}
