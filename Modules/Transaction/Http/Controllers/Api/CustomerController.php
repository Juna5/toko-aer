<?php

namespace Modules\Transaction\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Http\Requests\CustomerRequest;

class CustomerController extends Controller
{
    public function store(CustomerRequest $request)
	{
        $row = Customer::create([
        'company_name'         => $request->company_name,
        'roc_no'               => $request->roc_no,
        'address'              => $request->address,
        'email'                => $request->email,
        'phone'                => $request->phone,
        'fax'                  => $request->fax,
        'pic_1'                => $request->pic_1,
        'pic_phone_1'          => $request->pic_phone_1,
        'credit_limit'         => $request->credit_limit,
        'deposit'              => $request->deposit,
        'price_category_id'    => $request->price_category_id
        ]);

		return ;
	}


}
