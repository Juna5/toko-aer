<?php

namespace Modules\Transaction\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Finance\Entities\Invoice;
use Modules\Finance\Entities\Receivable;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('transaction::index');
    }

    public function view()
    {

        return view('transaction::view');
    }

    public function openInvoices()
    {
        $time = Carbon::now();
        $openInvoice = Invoice::where('payment_status', '!=', 'Paid')->where('invoice_due_date' ,'>=', $time->now())->whereNull('deleted_at')->orderBy('id', 'DESC')->get();

        $total = Invoice::where('payment_status', '!=', 'Paid')->where('invoice_due_date' ,'>=', $time->now())->whereNull('deleted_at')->sum('outstanding');

        return view('transaction::open_invoices_report', compact('openInvoice', 'total'));
    }

    public function overDueInvoices()
    {
        $time = Carbon::now();
        $overDueInvoice = Invoice::where('payment_status', '!=', 'Paid')->where('invoice_due_date','<=', $time->now())->whereNull('deleted_at')->orderBy('id', 'DESC')->get();

         $total = Invoice::where('payment_status', '!=', 'Paid')->where('invoice_due_date','<=', $time->now())->whereNull('deleted_at')->sum('outstanding');

        return view('transaction::overdue_invoices_report', compact('overDueInvoice', 'total'));
    }

    public function receivedPayment()
    {
        $first_day_of_the_current_month = Carbon::now()->startOfMonth();
        $last_day_of_the_current_month = Carbon::now()->endOfMonth();
        $time = Carbon::now();

        $receivedPayment = Receivable::whereBetween('date', [$first_day_of_the_current_month, $last_day_of_the_current_month])->get();

        $total = Receivable::whereBetween('date', [$first_day_of_the_current_month, $last_day_of_the_current_month])->whereNull('deleted_at')->sum('amount');

        return view('transaction::payments_received_report', compact('receivedPayment', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('transaction::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('transaction::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('transaction::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
