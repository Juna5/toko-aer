<?php

namespace Modules\Transaction\Http\Controllers\QuotationMaterial;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\Transaction\Entities\QuotationMaterial;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Transaction\Entities\QuotationMaterialApproval;
use Modules\Transaction\DataTables\QuotationMaterialApprovalDatatable;

class QuotationMaterialApprovalController extends Controller
{
    public function index(QuotationMaterialApprovalDatatable $datatable)
    {
        $this->hasPermissionTo('view quotation material approval');

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if (empty($employee)){
            flash('Ops.. please check your employee id.')->error();
            return back();
        }

        $quotationMaterials = filterModuleByStatus(QuotationMaterial::class, [
            'employee', 'department', 'salesman', 'tax', 'customer', 'paymentTerm'
        ]);

        return $datatable->render('transaction::quotation_material.approval.index', compact('quotationMaterials'));
    }


    public function show($id)
    {
        $this->hasPermissionTo('view quotation material approval');

        $row = QuotationMaterial::findOrFail($id);
        $items = QuotationMaterialItem::where('status', 1)->where('quotation_material_id', $id)->get();
        $histories = QuotationMaterialApproval::where('quotation_material_id', $id)
            ->get()
            ->sortBy('id');
        if($row){
            $media = $row->getMedia('quotation_material') ?? [];
        }else{
            $media = [];
        }

        return view('transaction::quotation_material.approval.detail', compact('row', 'items','histories','media'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit quotation material approval');

        $description = request('description');
        if (request()->has('approve')) {
            \Approval::action(QuotationMaterial::class, QuotationMaterialApproval::class, $id, 'quotation_material_id', $description);
        } else {
            \Approval::reject(QuotationMaterial::class, QuotationMaterialApproval::class, $id, 'quotation_material_id', $description);
        }

        flash('Your data has been saved successfully')->success();
        return redirect()->route('transaction.quotation_material.approval.index');
    }

    public function updateItem(Request $request, $id)
    {
        $this->hasPermissionTo('delete quotation material approval');

        $row = QuotationMaterialItem::findOrFail($id);
        $row->status = 0;
        $row->save();

        $quotation = QuotationMaterial::find($row->quotation_material_id);

        $getTotal = $quotation->get_total - $row->sub_total;
        $subTotal = $quotation->sub_total - $row->sub_total;
        $gst = ($quotation->tax->rate / 100) * $subTotal;
        $total = $subTotal + $gst;

        $quotation->get_total = $getTotal;
        $quotation->sub_total = $subTotal;
        $quotation->total = $total;
        $quotation->save();

        flash('Your data has been deleted successfuly')->error();
        return back();
    }
}
