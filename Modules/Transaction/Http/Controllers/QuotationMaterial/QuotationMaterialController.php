<?php

namespace Modules\Transaction\Http\Controllers\QuotationMaterial;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Tax;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\ProductPrice;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Transaction\Entities\QuotationMaterial;
use Modules\Transaction\Exports\QuotationMaterialExport;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Transaction\Entities\QuotationMaterialApproval;
use Modules\Transaction\DataTables\QuotationMaterialDatatable;
use Modules\Transaction\Http\Requests\QuotationMaterialRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\ApproverEmail;
use Mail;

class QuotationMaterialController extends Controller
{
    protected $menu = 'quotation material';

    public function index(QuotationMaterialDatatable $datatable)
    {
        $this->hasPermissionTo('view quotation material');

        $row = QuotationMaterial::all();

        return $datatable->render('transaction::quotation_material.entry.index', compact('row'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
     public function create()
     {
         $this->hasPermissionTo('add quotation material');

         $employee = Employee::find(employee()->id);

         return view('transaction::quotation_material.entry.create', compact('employee'));
     }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(QuotationMaterialRequest $request)
    {
        $this->hasPermissionTo('add quotation material');
        DB::beginTransaction();
        try {

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $firstApproval = getFirstApproval($this->menu, $request->department_id, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $request->department_id, $employee->division_id);
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('quotation material');
            $code_number = generate_code_sequence('QM', $sequence_number, 3);
            $qm = QuotationMaterial::create([
                'employee_id' => $employee->id,
                'code_number' => $code_number,
                'status' => 'waiting',
                'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
                'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
                'date' => now()->format('Y-m-d'),

                'title' => $code_number,
                'customer_id' => $request->customer_id,
                'project_id' => $request->project_id,
                'salesman_id' => $request->salesman_id,
                'department_id' => $request->department_id,
                'get_total' => $request->getTotal,
                'discount_all_item' => $request->discount_all_item,
                'sub_total' => $request->sub_total,
                'tax_id' => $request->tax_id,
                'total' => $request->total,
                'description' => $request->description,
                'payment_term_id' => $request->payment_term_id,
                'valid_until' => $request->valid_until,
            ]);

            foreach ($request->products as $row) {
                $subTotal = ($row['price'] * $row['qty']) - ($row['discountAmount'] ? $row['discountAmount'] : 0 ) - ($row['discountRate'] ? (($row['discountRate'] / 100) * $row['price']) : 0) * $row['qty'];
                if ($row['item'] != null) {
                    QuotationMaterialItem::create([
                    'quotation_material_id' => $qm->id,
                    'product_id' => $row['item'],
                    'qty' => $row['qty'],
                    'uom_id' => $row['uom'],
                    'price' => $row['price'],
                    'discount_rate' => $row['discountRate'],
                    'discount_amount' => $row['discountAmount'],
                    'sub_total' => $subTotal,
                    'description' => $row['description']
                    ]);
                }
            }

            foreach ($getApproval as $row) {
                QuotationMaterialApproval::create([
                'quotation_material_id' => $qm->id,
                'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                'role' => isset($row->role) ? $row->role : null,
                'is_approved' => false,
                ]);
            }

            if ($firstApproval->approver_id){

            Mail::to($firstApproval->approver->email)->send(new ApproverEmail($qm));
            }

        } catch(\Exception $e)
        {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage()
            ]);
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('transaction.quotation_material.index')
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $row = QuotationMaterial::find($id);

        $media = $row->getMedia('quotation_material');

        if ($row){
            $media = $row->getMedia('quotation_material');

                } else {
                    $media = '';
                }

        return view('transaction::quotation_material.entry.modals.attachment',compact('media'))->withMedia($media)->withRow($row);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('transaction::quotation_material.entry.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit quotation material');

        DB::beginTransaction();
        try {
            # action for resubmit
            if (request()->resubmit) {
                \Approval::resubmit(QuotationMaterial::class, QuotationMaterialApproval::class, $id, 'quotation_material_id', [
                    'title' => $request->title,
                    'customer_id' => $request->customer_id,
                    'salesman_id' => $request->salesman_id,
                    'get_total' => $request->get_total,
                    'discount_all_item' => $request->discount_all_item,
                    'sub_total' => $request->sub_total,
                    'tax_id' => $request->tax_id,
                    'total' => $request->total,
                    'description' => $request->description,
                    'payment_term_id' => $request->payment_term_id,
                    'valid_until' => $request->valid_until,
                ]);

                foreach ($request->products as $row) {
                    if ($row['item'] != null) {
                        $subTotal = ($row['price'] * $row['qty']) - ($row['discountAmount'] ? $row['discountAmount'] : 0 ) - ($row['discountRate'] ? (($row['discountRate'] / 100) * $row['price']) : 0) * $row['qty'];
                        if (array_key_exists('id', $row)) {

                            $prItem = QuotationMaterialItem::find($row['id']);
                            $prItem->update([
                            'product_id' => $row['item'],
                            'qty' => $row['qty'],
                            'uom_id' => $row['uom'],
                            'price' => $row['price'],
                            'discount_rate' => $row['discountRate'],
                            'discount_amount' => $row['discountAmount'],
                            'sub_total' => $subTotal,
                            'description' => $row['description'],
                            'status' => 1,
                            ]);
                        } else {
                            QuotationMaterialItem::create([

                                'quotation_material_id' => $id,
                                'product_id' => $row['item'],
                                'qty' => $row['qty'],
                                'uom_id' => $row['uom'],
                                'price' => $row['price'],
                                'discount_rate' => $row['discountRate'],
                                'discount_amount' => $row['discountAmount'],
                                'sub_total' => $subTotal,
                                'description' => $row['description']
                            ]);
                        }
                    }
                }
            }else{
                # updated data
                $pr = QuotationMaterial::findOrFail($id);
                $pr->title = $request->title;
                $pr->customer_id = $request->customer_id;
                $pr->salesman_id = $request->salesman_id;
                $pr->get_total = $request->get_total;
                $pr->discount_all_item = $request->discount_all_item;
                $pr->sub_total = $request->sub_total;
                $pr->tax_id = $request->tax_id;
                $pr->total = $request->total;
                $pr->description = $request->description;
                $pr->payment_term_id = $request->payment_term_id;
                $pr->valid_until = $request->valid_until;
                $pr->save();

                foreach ($request->products as $row) {
                    if ($row['item'] != null) {
                        $subTotal = ($row['price'] * $row['qty']) - ($row['discountAmount'] ? $row['discountAmount'] : 0 ) - ($row['discountRate'] ? (($row['discountRate'] / 100) * $row['price']) : 0) * $row['qty'];
                        if (array_key_exists('id', $row)) {

                            $item = QuotationMaterialItem::find($row['id']);
                            $item->update([
                                'product_id' => $row['item'],
                                'qty' => $row['qty'],
                                'uom_id' => $row['uom'],
                                'price' => $row['price'],
                                'discount_rate' => $row['discountRate'],
                                'discount_amount' => $row['discountAmount'],
                                'sub_total' => $subTotal,
                                'description' => $row['description'],
                                'status' => 1
                            ]);
                        } else {
                            QuotationMaterialItem::create([
                                'quotation_material_id' => $id,
                                'product_id' => $row['item'],
                                'qty' => $row['qty'],
                                'uom_id' => $row['uom'],
                                'price' => $row['price'],
                                'discount_rate' => $row['discountRate'],
                                'discount_amount' => $row['discountAmount'],
                                'sub_total' => $subTotal,
                                'description' => $row['description']
                            ]);
                        }
                    }
                }
            }
        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('transaction.quotation_material.index')
        ]);
    }

     public function destroy($id)
     {
         $this->hasPermissionTo('delete quotation material');

         $row = QuotationMaterial::findOrFail($id);

         QuotationMaterialApproval::where('quotation_material_id', $row->id)->delete();
         QuotationMaterialItem::where('quotation_material_id', $row->id)->delete();

         $row->delete();

         noty()->danger('Cool!', 'Your entry has been deleted');
         return back();
     }

     public function detail($id)
     {
         $row = QuotationMaterial::findOrFail($id);
         $items = QuotationMaterialItem::where('quotation_material_id', $id)->get();
         $approvals = QuotationMaterialApproval::where('quotation_material_id', $id)->orderBy('id', 'asc')->get();
         if($row){
            $media = $row->getMedia('quotation_material');
        }else{
            $media = [];
        }

         return view('transaction::quotation_material.entry.detail', compact('row', 'items', 'approvals', 'media'));
     }

    public function customer()
    {
        $customer = Customer::where('deleted_at', null)->get();
        return $customer;
    }

    public function customerDetail($id)
    {
        $customer = Customer::find($id);
        return $customer;
    }

    public function salesman()
    {
        return Employee::all();
    }

    public function productPrice($id){
        return ProductPrice::with('product')->whereHas('product', function($product) {
            $product->whereNull('deleted_at');
        })->where('price_category_id', $id)->get();
    }

    public function productDetail($id, $productId)
    {
        $product = ProductPrice::with('product')->whereId($id)->where('product_id', $productId)->first();
        return response()->json($product);
    }

    public function tax()
    {
        return Tax::where('code', 'PPN')->first();
    }

    public function paymentTerm()
    {
        return PaymentTerm::all();
    }

    public function saveAttachment()
    {
        $row = QuotationMaterial::find(request('id_quotation'));

        if (request()->hasFile('attachment')) {
            foreach (request()->attachment as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('quotation_material');
            }
        }

      return redirect()->route('transaction.quotation_material.index', compact('row'));
    }

    public function salesmenId()
	{
        $employeeId = auth()->user()->id;
        $salesmen = Employee::whereUserId($employeeId)->first();
        return $salesmen;
	}

    public function export()
    {
        return Excel::download(new QuotationMaterialExport, 'QuotationMaterial.xlsx');
    }

    public function pdf($id)
    {
        $row = QuotationMaterial::findOrFail($id);
        $items = QuotationMaterialItem::with(['product', 'uom'])->where('quotation_material_id', $id)->get();
        $histories = QuotationMaterialApproval::where('quotation_material_id', $id)->orderBy('id', 'asc')->get();

        $pdf = PDF::loadView('transaction::quotation_material.entry.pdf', compact('row', 'items', 'histories'));
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream($row->code_number . ".pdf");

    }

    public function report()
    {
        $quotation = [];
        $roleName = auth()->user()->getRoleNames();

        $employee = Employee::where('user_id', auth()->user()->id)->first();

        $fromDate  = (!empty(request('from_date'))) ? request('from_date') : '';
        $untilDate = (!empty(request('until_date'))) ? request('until_date') : '';
        $page = '10';

        if($fromDate && $untilDate && $employee){
            $quotation = QuotationMaterial::withoutGlobalScopes()->with('employee', 'department', 'salesman', 'tax', 'customer', 'paymentTerm')->whereNull('deleted_at')->whereRaw("date(quotation.date) >= '" . $fromDate . "' AND date(quotation.date) <= '" . $untilDate . "' ")->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->paginate($page);
        }

        return view('transaction::report.quotation_material', compact('quotation'));
    }

    public function detailReport($id)
    {
        $row = QuotationMaterial::findOrFail($id);
        $items = QuotationMaterialItem::where('quotation_material_id', $id)->get();
        $approvals = QuotationMaterialApproval::where('quotation_material_id', $id)->orderBy('id', 'asc')->get();
        $from_date = request('from_date');
        $until_date = request('until_date');

        if($row){
            $media = $row->getMedia('quotation_material_pdf');
        }else{
            $media = [];
        }

        return view('transaction::report.quotation_material_detail', compact('row', 'items', 'approvals','from_date','until_date', 'media'));
    }
}
