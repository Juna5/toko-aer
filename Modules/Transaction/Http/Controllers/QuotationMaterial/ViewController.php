<?php

namespace Modules\Transaction\Http\Controllers\QuotationMaterial;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ViewController extends Controller
{
    public function __invoke()
    {
        return view('transaction::quotation_material.index');
    }
}
