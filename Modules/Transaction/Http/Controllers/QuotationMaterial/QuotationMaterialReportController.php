<?php

namespace Modules\Transaction\Http\Controllers\QuotationMaterial;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\HR\Entities\Employee;
use Modules\Transaction\Entities\QuotationMaterial;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Transaction\Entities\QuotationMaterialApproval;

class QuotationMaterialReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $this->hasPermissionTo('view quotation material report');
        $quotationMaterials = [];
        $roleName           = auth()->user()->getRoleNames();

        $employee           = Employee::where('user_id', auth()->user()->id)->first();
        $fromDate           = request()->from_date;
        $untilDate          = request()->until_date;
        $status             = request()->status;
        $page               = 10;

        if($fromDate && $untilDate && $employee){
            if($status == 'all'){
                $quotationMaterials = QuotationMaterial::orderBy('date','desc')->where('date', '>=', $fromDate)->where('date', '<=', $untilDate)->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->paginate($page);
            }else{
                 $quotationMaterials = QuotationMaterial::orderBy('date','desc')->where('date', '>=', $fromDate)->where('date', '<=', $untilDate)->where('status', $status)->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->paginate($page);
            }
        }

        return view('transaction::quotation_material.report.index', compact('quotationMaterials'));
    }

    public function show($id)
    {
        $row = QuotationMaterial::findOrFail($id);
        $items = QuotationMaterialItem::where('quotation_material_id', $id)->get();
        $approvals = QuotationMaterialApproval::where('quotation_material_id', $id)->orderBy('id', 'asc')->get();

        if($row){
            $media = $row->getMedia('quotation_material');
        }else{
            $media = [];
        }

         return view('transaction::quotation_material.report.detail', compact('row', 'items', 'approvals', 'media'));
    }
}
