<?php

namespace Modules\Transaction\Http\Controllers\DeliveryOrderMaterial;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Modules\Transaction\Entities\DeliveryOrderMaterialItem;
use Modules\Transaction\Entities\DeliveryOrderMaterialApproval;
use Modules\Transaction\DataTables\DeliveryOrderMaterialApprovalDatatable;

class DeliveryOrderMaterialApprovalController extends Controller
{
    public function index(DeliveryOrderMaterialApprovalDatatable $datatable)
    {
        $this->hasPermissionTo('view delivery order material approval');

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if (empty($employee)){
            flash('Ops.. please check your employee id.')->error();
            return back();
        }

        $rows = filterModuleByStatus(DeliveryOrderMaterial::class, [
            'employee', 'department', 'paymentTerm', 'salesOrder.paymentTerm', 'customer', 'salesman', 'salesOrder'
        ]);
        
        return $datatable->render('transaction::delivery_order_material.approval.index', compact('rows'));
    }


    public function show($id)
    {
        $this->hasPermissionTo('view sales order approval');

        $row = DeliveryOrderMaterial::findOrFail($id);
        $items = DeliveryOrderMaterialItem::where('delivery_order_material_id', $id)->get();
        $histories = DeliveryOrderMaterialApproval::where('delivery_order_material_id', $id)
            ->get()
            ->sortBy('id');
        if($row){
            $media = $row->getMedia('delivery_order_material') ?? [];
        }else{
            $media = [];
        }

        return view('transaction::delivery_order_material.approval.detail', compact('row', 'items','histories','media'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit sales order approval');

        $description = request('description');
        if (request()->has('approve')) {
            \Approval::action(DeliveryOrderMaterial::class, DeliveryOrderMaterialApproval::class, $id, 'delivery_order_material_id', $description);
        } else {
            \Approval::reject(DeliveryOrderMaterial::class, DeliveryOrderMaterialApproval::class, $id, 'delivery_order_material_id', $description);
        }

        flash('Your data has been saved successfully')->success();
        return redirect()->route('transaction.delivery_order_material.approval.index');
    }
}
