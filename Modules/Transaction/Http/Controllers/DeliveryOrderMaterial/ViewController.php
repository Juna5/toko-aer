<?php

namespace Modules\Transaction\Http\Controllers\DeliveryOrderMaterial;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ViewController extends Controller
{
    public function __invoke()
    {
        return view('transaction::delivery_order_material.index');
    }
}
