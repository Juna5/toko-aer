<?php

namespace Modules\Transaction\Http\Controllers\DeliveryOrderMaterial;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Modules\Transaction\Entities\DeliveryOrderMaterialItem;
use Modules\Transaction\Exports\DeliveryOrderMaterialExport;
use Modules\Transaction\Entities\DeliveryOrderMaterialApproval;
use Modules\Transaction\DataTables\DeliveryOrderMaterialDatatable;
use Modules\Transaction\DataTables\DeliveryOrderMaterialOutstandingDatatable;
use Modules\Transaction\Http\Requests\DeliveryOrderMaterialRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\ApproverEmail;
use Mail;

class DeliveryOrderMaterialController extends Controller
{
    protected $menu = 'delivery order material';

    public function index(DeliveryOrderMaterialDatatable $datatable)
    {
        $this->hasPermissionTo('view sales order');
            // dd(DeliveryOrderMaterial::first());
        return $datatable->render('transaction::delivery_order_material.entry.index');
    }

    public function outstanding(DeliveryOrderMaterialOutstandingDatatable $datatable)
    {
        $this->hasPermissionTo('view delivery order material');
        return $datatable->render('transaction::delivery_order_material.entry.index_outstanding');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
     public function create()
     {
         $this->hasPermissionTo('add sales order');

         return view('transaction::delivery_order_material.entry.create');
     }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(DeliveryOrderMaterialRequest $request)
    {
        $this->hasPermissionTo('add delivery order material');
        DB::beginTransaction();
        try {
            foreach ($request->products as $row) {
                if ($row['sendQty'] > $row['purchaseQty']) {
                    return response()->json([
                    'success' => false,
                    'redirect' => route('transaction.delivery_order_material.create')
                    ]);
                }
            }
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $firstApproval = getFirstApproval($this->menu, $request->department_id, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $request->department_id, $employee->division_id);
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('delivery order material');
            $code_number = generate_code_sequence('DO', $sequence_number, 3);
            $do = DeliveryOrderMaterial::create([
            'employee_id' => $employee->id,
            'code_number' => $code_number,
            'status' => 'waiting',
            'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
            'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
            'date' => $request->date,

            'sales_order_id' => $request->sales_order_id,
            'title' => $request->title,
            'customer_id' => $request->customer_id,
            'salesman_id' => $request->salesman_id,
            'department_id' => $request->department_id,
            'payment_term_id' => 1,
            'description' => $request->description,
            'address' => $request->address,
            ]);

            foreach ($request->products as $row) {
                //update Sales Order Item
                $soItem = SalesOrderItem::findOrFail($row['sales_order_item_id']);
                $soItem->delivered_qty = $soItem->delivered_qty - $row['sendQty'];
                $soItem->stock_qty = $soItem->stock_qty - $row['sendQty'];
                $soItem->save();

                // return response()->json([
                // 'success' => true,
                // 'redirect' => route('transaction.delivery_order_material.create')
                // ]);

                if ($row['item'] != null) {
                    DeliveryOrderMaterialItem::create([
                    'delivery_order_material_id' => $do->id,
                    'sales_order_id' => $row['sales_order_id'],
                    'sales_order_item_id' => $row['sales_order_item_id'],
                    'qty' => $row['sendQty'],
                    'warehouse_id' => 1,
                    'description' => $row['description']
                    ]);
                    


                    if($row['warehouse_id'] != null){
                        $checkstock = Stock::where('warehouse_id', $row['warehouse_id'])->where('product_id', $row['item'])->first();
                        if($checkstock){
                            $checkstock->qty = $checkstock->qty - $row['sendQty'];
                            $checkstock->save();
                            
                            StockTransaction::create([
                                'product_id' => $soItem->product_id,
                                'warehouse_id' => $row['warehouse_id'],
                                'delivery_order_id' => $do->id,
                                'date' => $request->date,
                                'type' => 'OUT',
                                'qty'  => $row['sendQty'],
                                'status' => true,
                                'description' => $request->description,
                                'in_hand' => $checkstock->qty,
                                'stock_id' => $checkstock->id
                            ]); 
                        }
                        
                    }
                }

            }

            foreach ($getApproval as $row) {
                DeliveryOrderMaterialApproval::create([
                'delivery_order_material_id' => $do->id,
                'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                'role' => isset($row->role) ? $row->role : null,
                'is_approved' => false,
                ]);
            }

            $cekSo = SalesOrderItem::where('sales_order_id', $request->sales_order_id)->get();
            $pluckSo = $cekSo->pluck('delivered_qty')->toArray();
            if(count($pluckSo) == 0){
                DeliveryOrderMaterial::where('sales_order_id', $request->sales_order_id)->update(['status_partial' => 1]);
            }

            // if ($firstApproval->approver_id){
            //     Mail::to($firstApproval->approver->email)->send(new ApproverEmail($do));
            // }

        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('transaction.delivery_order_material.create')
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $row = DeliveryOrderMaterial::find($id);

        $media = $row->getMedia('delivery_order_material');

        if($row){
            $media = $row->getMedia('delivery_order_material');

                } else {
                    $media = '';
                }
        return view('transaction::delivery_order_material.entry.modals.attachment',compact('media'))->withMedia($media)->withRow($row);
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('transaction::delivery_order_material.entry.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit sales order');

        DB::beginTransaction();
        try {
            foreach ($request->products as $row) {
                if ($row['sendQty'] > $row['purchaseQty']) {
                    return response()->json([
                    'success' => false,
                    'redirect' => route('transaction.delivery_order_material.create')
                    ]);
                }
            }
            # action for resubmit
            if (request()->resubmit) {
                \Approval::resubmit(DeliveryOrderMaterial::class, DeliveryOrderMaterialApproval::class, $id, 'delivery_order_material_id', [
                    'title' => $request->title,
                    'customer_id' => $request->customer_id,
                    'salesman_id' => $request->salesman_id,
                    'payment_term_id' => 1,
                    'address' => $request->address,
                    'date' => $request->date,
                    'description' => $request->description,
                ]);
            }else{
                # updated data
                $pr = DeliveryOrderMaterial::findOrFail($id);
                $pr->title = $request->title;
                $pr->customer_id = $request->customer_id;
                $pr->salesman_id = $request->salesman_id;
                $pr->payment_term_id = 1;
                $pr->address = $request->address;
                $pr->date = $request->date;
                $pr->description = $request->description;
                $pr->save();
            }

            foreach ($request->products as $row) {
                if ($row['item'] != null) {
                    if (array_key_exists('id', $row)) {
                        $doItem = DeliveryOrderMaterialItem::find($row['id']);
                        $soItem = SalesOrderItem::findOrFail($row['sales_order_item_id']);

                        $sel = ($doItem->qty * -1) + $row['sendQty'];
                        if ($sel > 0 ) {
                            $soItemDel = $soItem->delivered_qty - $sel;
                            $soStockQty = $soItem->stock_qty - $sel;

                        }else{
                            $soItemDel = $soItem->delivered_qty - $sel;
                            $soStockQty = $soItem->stock_qty - $sel;
                        }
                        $soItem->delivered_qty = $soItemDel;
                        $soItem->stock_qty = $soStockQty;
                        $soItem->save();

                        $doItem->update([
                        'qty' => (int) $row['sendQty'],
                        'warehouse_id' => $soItem->warehouse_id,
                        ]);

                        if($soItem->warehouse_id){
                           
                                 $checkstock = Stock::where('warehouse_id', $soItem->warehouse_id)->where('product_id', $soItem->product_id)->first();
                                if($checkstock){
                                $checkstock->qty = $soStockQty;
                                $checkstock->save();

                                $stockTransaction = StockTransaction::where('warehouse_id', $soItem->warehouse_id)->where('product_id', $soItem->product_id)->where('stock_id', $checkstock->id)->first();
                                $stockTransaction->qty = $row['sendQty'];
                                $stockTransaction->in_hand = $checkstock->qty;
                                $stockTransaction->save();
                            }
                       
                        }
                    }
                }
            }

            $cekSo = SalesOrderItem::where('sales_order_id', $request->sales_order_id)->get();
            $pluckSo = $cekSo->pluck('delivered_qty')->toArray();
            if(count($pluckSo) == 0){
                DeliveryOrderMaterial::where('sales_order_id', $request->sales_order_id)->update(['status_partial' => 1]);
            }else{
                DeliveryOrderMaterial::where('sales_order_id', $request->sales_order_id)->update(['status_partial' => 0]);
            }
        }
        catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('transaction.delivery_order_material.index')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
     public function destroy($id)
     {
         $this->hasPermissionTo('delete sales order');

         $row = DeliveryOrderMaterial::findOrFail($id);

         $item = DeliveryOrderMaterialItem::where('delivery_order_material_id', $row->id)->get();
         foreach ($item as $key => $value) {
             $salesOrderItem = SalesOrderItem::find($value->sales_order_item_id);
             $salesOrderItem->delivered_qty = $salesOrderItem->delivered_qty + $value->qty;
             $salesOrderItem->stock_qty = $salesOrderItem->stock_qty + $value->qty;
             $salesOrderItem->save();

            $checkstock = Stock::where('warehouse_id', $salesOrderItem->warehouse_id)->where('product_id', $salesOrderItem->product_id)->first();

            if($checkstock){
                $checkstock->qty = $salesOrderItem->stock_qty + $value->qty;
                $checkstock->save();
                $stockTransaction = StockTransaction::where('warehouse_id', $row['warehouse_id'])->where('product_id', $soItem->product_id)->where('stock_id', $checkstock->id)->get();
                foreach($stockTransaction as $transaction){
                    $transaction->delete();
                }
                           
            }
         }

         $cekSo = SalesOrderItem::where('sales_order_id', $row->sales_order_id)->get();
         $pluckSo = $cekSo->pluck('delivered_qty')->toArray();
         if(count($pluckSo) == 0){
             DeliveryOrderMaterial::where('sales_order_id', $row->sales_order_id)->update(['status_partial' => 1]);
         }else{
             DeliveryOrderMaterial::where('sales_order_id', $row->sales_order_id)->update(['status_partial' => 0]);
         }

         DeliveryOrderMaterialApproval::where('delivery_order_material_id', $row->id)->delete();
         $item->each->delete();
         $row->delete();

         noty()->danger('Cool!', 'Your entry has been deleted');
         return redirect()->route('transaction.delivery_order_material.index');
     }

     public function detail($id)
     {
         $row = DeliveryOrderMaterial::findOrFail($id);
         $items = DeliveryOrderMaterialItem::with('salesOrderMaterialItem.product', 'salesOrderMaterialItem')->where('delivery_order_material_id', $id)->get();
         $approvals = DeliveryOrderMaterialApproval::where('delivery_order_material_id', $id)->orderBy('id', 'asc')->get();
         if($row){
            $media = $row->getMedia('delivery_order_material');
        }else{
            $media = [];
        }

         return view('transaction::delivery_order_material.entry.detail', compact('row', 'items', 'approvals', 'media'));
     }

    public function salesOrderMaterial()
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first()->id;

        $row = SalesOrder::with('items')->where('employee_id', $employee)
        ->where('status', 'approved')
        ->whereHas('items', function ($q){
            $q->where('delivered_qty', '>' ,0);
        })->whereHas('items', function ($q){
            $q->where('stock_qty', '>' ,0);
        })
        ->get();
        return $row;
    }

    public function warehouse()
    {
        $row = WarehouseMaster::all();
        return $row;
    }

    public function salesOrderMaterialItem()
	{
        return SalesOrder::with(['department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom','items.warehouse'])
        ->whereHas('items', function ($q) {
            $q->where('status', true);
        })
        ->find(request()->sales_order_id);
	}

    public function report(){
        $deliveryOrder  = [];
        $roleName       = auth()->user()->getRoleNames();

        $employee       = Employee::where('user_id', auth()->user()->id)->first();

        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $status         = request()->status;
        $page           = 10;

        if($fromDate && $untilDate && $employee){
            if($status == 'all'){
                $deliveryOrder = DeliveryOrderMaterial::withoutGlobalScopes()->with('employee', 'department', 'paymentTerm', 'salesOrder', 'customer', 'salesman')->whereNull('deleted_at')->whereRaw("date(delivery_order_materials.date) >= '" . $fromDate . "' AND date(delivery_order_materials.date) <= '" . $untilDate . "' ")->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->paginate($page);

            }else{
                $deliveryOrder = DeliveryOrderMaterial::withoutGlobalScopes()->with('employee', 'department', 'paymentTerm', 'salesOrder', 'customer', 'salesman')->whereNull('deleted_at')->whereRaw("date(delivery_order_materials.date) >= '" . $fromDate . "' AND date(delivery_order_materials.date) <= '" . $untilDate . "' ")->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->where('status', $status)->paginate($page);
            }
        }

        return view('transaction::report.delivery_order', compact('deliveryOrder'));
    }

    public function detailReport($id)
    {
        $row = DeliveryOrderMaterial::findOrFail($id);
        $items = DeliveryOrderMaterialItem::with('salesOrderMaterialItem.product', 'salesOrderMaterialItem')->where('delivery_order_material_id', $id)->get();
        $approvals = DeliveryOrderMaterialApproval::where('delivery_order_material_id', $id)->orderBy('id', 'asc')->get();
        $from_date = request('from_date');
        $until_date = request('until_date');

        if($row){
            $media = $row->getMedia('delivery_order_material');
        }else{
            $media = [];
        }
        return view('transaction::report.delivery_order_detail', compact('row', 'items', 'approvals','from_date','until_date', 'media'));
    }

    public function pdf($id)
    {
        $row = DeliveryOrderMaterial::findOrFail($id);
        $items = DeliveryOrderMaterialItem::with('salesOrderMaterialItem.product', 'salesOrderMaterialItem')->where('delivery_order_material_id', $id)->get();
        $histories = DeliveryOrderMaterialApproval::where('delivery_order_material_id', $id)->orderBy('id', 'asc')->get();
        if(request('report') == false)
        {
            $pdf = PDF::loadView('transaction::delivery_order_material.entry.pdf', compact('row', 'items', 'histories'));
        }else{
            $pdf = PDF::loadView('transaction::report.delivery_order_pdf', compact('row', 'items', 'histories'));
        }
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream($row->code_number . ".pdf");

    }

    public function saveAttachment()
    {
        $row = DeliveryOrderMaterial::find(request('id_delivery_order_material'));

        if (request()->hasFile('attachment')) {
            foreach (request()->attachment as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('delivery_order_material');
            }
        }

        return redirect()->route('transaction.delivery_order_material.index', compact('row'));
    }

    public function export()
    {
        return Excel::download(new DeliveryOrderMaterialExport, 'DeliveryOrderMaterial.xlsx');
    }
}
