<?php

namespace Modules\Transaction\Http\Controllers\SalesOrder;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Transaction\Entities\SalesOrder;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Maatwebsite\Excel\Facades\Excel;
use Rap2hpoutre\FastExcel\FastExcel;
use Modules\Transaction\Exports\SalesOrderReportExport;

class SalesOrderReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $this->hasPermissionTo('view sales order report');
        $salesOrders    = [];
        $roleName       = auth()->user()->getRoleNames();

        $employee       = Employee::where('user_id', auth()->user()->id)->first();
        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $depot          = request()->depot;
        $page           = 10;

        $warehouse = $employee ? WarehouseMaster::where('id', $employee->depot_id)->get() : WarehouseMaster::where('deleted_at', null)->get();
        $result = [];
        $airGalon = 0;
        $airIsiUlang = 0;
        $voucher25Terjual = 0;
        $voucher25Terpakai = 0;
        $voucher50Terjual = 0;
        $voucher50Terpakai = 0;
        $totalVoucherTerpakai = 0;
        $totalVoucherTerjual = 0;
        $getQty = 0;
        if($fromDate && $untilDate && $depot){

            $getQty = getQtyReport($depot,$fromDate,$untilDate);

            $salesOrders  =  SalesOrder::whereNull('cancel_approved_at')->when($fromDate, function($query) use($fromDate){
                $query->where('date', '>=', $fromDate);           
            })
            ->when($untilDate, function($query) use($untilDate){
                $query->where('date', '<=', $untilDate);
            })
            ->when($depot, function($query) use($depot){
                $query->where('depot_id', $depot);
            })
            ->whereNull('deleted_at')->take(1)->paginate($page);

            $result = SalesOrder::whereNull('cancel_approved_at')->when($fromDate, function($query) use($fromDate){
            $query->where('date', '>=', $fromDate);           
            })->when($untilDate, function($query) use($untilDate){
                $query->where('date', '<=', $untilDate);
            })->when($depot, function($query) use($depot){
                $query->where('depot_id', $depot);
            })->whereNull('deleted_at')->get();

            $airGalon = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->whereHas('product', function($query){
                $query->where('code','GL')->where('deleted_at', null);
            })->where('deleted_at', null)->get()->sum('qty');
            $airIsiUlang = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->whereHas('product', function($query){
                $query->where('code','GU')->where('deleted_at', null);
            })->where('deleted_at', null)->get()->sum('qty');
            $voucher25Terjual = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->whereHas('product', function($query){
                $query->where('code','GV')->where('deleted_at', null);
            })->where('deleted_at', null)->get()->sum('qty');

            $voucher25Terjual = $voucher25Terjual * 25;
            $voucher25Terpakai = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->where('description','ilike','Voucher 25')->where('deleted_at', null)->get()->sum('qty_voucher');

            $voucher50Terjual = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->whereHas('product', function($query){
                $query->where('code','GB')->where('deleted_at', null);
            })->where('deleted_at', null)->get()->sum('qty');

            $voucher50Terjual = $voucher50Terjual * 50;
            $voucher50Terpakai = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->where('description','ilike','Voucher 50')->where('deleted_at', null)->get()->sum('qty_voucher');

            $totalVoucherTerpakai = SalesOrderItem::whereIn('sales_order_id', $result->pluck('id'))->where('deleted_at', null)->get()->sum('qty_voucher');

            $totalVoucherTerjual = $voucher25Terjual + $voucher50Terjual; 
        }
        
        return view('transaction::sales_order.report.index', compact('getQty','salesOrders','warehouse',
        'result','airGalon','airIsiUlang','voucher25Terjual',
        'voucher25Terpakai','voucher50Terjual','voucher50Terpakai',
        'totalVoucherTerjual','totalVoucherTerpakai'));

        // $salesOrder = [];
        // $roleName = auth()->user()->getRoleNames();

        // $employee = Employee::where('user_id', auth()->user()->id)->first();

        // $fromDate  = (!empty(request('from_date'))) ? request('from_date') : '';
        // $untilDate = (!empty(request('until_date'))) ? request('until_date') : '';
        // $page = '10';

        // if($fromDate && $untilDate && $employee){
        //         $salesOrder = SalesOrder::withoutGlobalScopes()->with('employee', 'department', 'salesman', 'tax', 'customer', 'paymentTerm')->whereNull('deleted_at')->whereRaw("date(sales_orders.date) >= '" . $fromDate . "' AND date(sales_orders.date) <= '" . $untilDate . "' ")->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->paginate($page);
        // }

        // return view('transaction::report.sales_order', compact('salesOrder'));
    }

    public function arrayPaginator($array, $request)
    {
        $page = Input::get('page', 1);
        $perPage = 10;
        $offset = ($page * $perPage) - $perPage;

        return new \Illuminate\Pagination\LengthAwarePaginator(array_slice($array, $offset, $perPage, true), count($array), $perPage, $page,
            ['path' => $request->url(), 'query' => $request->query()]);
    }

    public function show($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
        $histories = SalesOrderApproval::where('sales_order_id', $id)
            ->get()
            ->sortBy('id');
        if ($row) {
            $media = $row->getMedia('sales_order');
        } else {
            $media = [];
        }
        return view('transaction::sales_order.report.detail', compact('row', 'items', 'approvals', 'histories', 'media'));
    }

    public function pdf($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
        $histories = SalesOrderApproval::where('sales_order_id', $id)
            ->get()
            ->sortBy('id');
        $pdf = PDF::loadView('transaction::report.sales_order_pdf', compact('row', 'items', 'approvals', 'histories'));
        $pdf->setPaper('A4', 'potrait');

        return $pdf->stream("invoice.pdf");
    }

    public function export(Request $request)
    {
        $salesOrders    = [];
        $roleName       = auth()->user()->getRoleNames();

        $employee       = Employee::where('user_id', auth()->user()->id)->first();
        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $depot          = request()->depot;
        $page           = 10;

        $warehouse = $employee ? WarehouseMaster::where('id', $employee->depot_id)->get() : WarehouseMaster::where('deleted_at', null)->get();
        $result = [];
        $airGalon = 0;
        $airIsiUlang = 0;
        $voucher25Terjual = 0;
        $voucher25Terpakai = 0;
        $voucher50Terjual = 0;
        $voucher50Terpakai = 0;
        $totalVoucherTerpakai = 0;
        $totalVoucherTerjual = 0;
        $totalAmount = 0;

        if($fromDate && $untilDate && $depot){
            $fromDateQuery = "and so.date >= '" .$fromDate."'";
            $untilDateQuery = "and so.date <= '" .$untilDate."'";
            $depotQuery = "and so.depot_id = " .$depot;

            $salesOrders = DB::select("select so.*, e.name as employee_name, w.name as depot_name, c.name as currency_name from sales_orders so
            join employees e on e.id = so.employee_id
            join warehouse_masters w on w.id = so.depot_id
            join currencies c on c.id = so.currency_id
            where so.deleted_at isnull 
            and so.cancel_approved_at isnull
            and e.deleted_at is null
            and w.deleted_at isnull
            and c.deleted_at isnull
            $fromDateQuery 
            $untilDateQuery 
            $depotQuery");

            $salesOrderId = [];
            foreach ($salesOrders as $result) {
                $totalAmount += $result->total;
                $salesOrderId[] = $result->id;
            }

            $implode = implode(',', $salesOrderId);


            $items = $salesOrders ?  DB::select("select p.name as product_name, p.code as product_code , sum(qty) from sales_order_items soi 
                join products p on p.id = soi.product_id
                join uoms u on u.id = soi.uom_id
                where soi.deleted_at is null
                and p.deleted_at is null
                and u.deleted_at isnull
                and soi.sales_order_id in ($implode)
                group by 1,2
            ") : [];

        }

        $filename = 'Transaction '.$fromDate .' - '. $untilDate.'.xlsx';
        return Excel::download(new SalesOrderReportExport($salesOrders, $totalVoucherTerjual, $totalVoucherTerpakai,$airGalon, $airIsiUlang, $totalAmount, $items), $filename);
    }
}
