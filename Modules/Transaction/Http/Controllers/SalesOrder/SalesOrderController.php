<?php

namespace Modules\Transaction\Http\Controllers\SalesOrder;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\Voucher;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\QuotationMaterial;
use Modules\Transaction\Exports\SalesOrderExport;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Transaction\DataTables\SalesOrderDatatable;
use Modules\Transaction\Http\Requests\SalesOrderRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\ApproverEmail;
use Mail;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;

class SalesOrderController extends Controller
{
    protected $menu = 'sales order material';

    public function index(SalesOrderDatatable $datatable)
    {
        $this->hasPermissionTo('view sales order');

        return $datatable->render('transaction::sales_order.entry.index');
    }

    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
        $this->hasPermissionTo('add sales order');

        $currency = Currency::where('code', 'IDR')->first();

        return view('transaction::sales_order.entry.create', compact('currency'));
    }

    /**
    * Store a newly created resource in storage.
    * @param Request $request
    * @return Response
    */
    public function store(SalesOrderRequest $request)
    {

        $this->hasPermissionTo('add sales order');
        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            if (!$employee->depot_id) {
                noty()->danger('Depot Null');
                return response()->json([
                    'success' => true,
                    'redirect' => back()
                ]);
            }
            $currency = Currency::where('code', 'IDR')->first();
            if (!$currency) {
                noty()->danger('Currency Null');
                return response()->json([
                    'success' => true,
                    'redirect' => back()
                ]);
            }
            $now = now()->format('H:i');

            if($now >= "07:00" && $now <= "14:00"){
                $shift = 1;
            }else{
                $shift = 2;
            }
            $firstApproval = getFirstApproval($this->menu, $request->department_id, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $request->department_id, $employee->division_id);
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('sales order material');
            $code_number = generate_code_sequence('SO', $sequence_number, 3);
            $qm = SalesOrder::create([
                'employee_id' => $employee->id,
                'code_number' => $code_number,
                'status' => 'waiting',
                'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
                'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
                'date' => now()->format('Y-m-d'),

                'title' => $code_number,
                'customer_id' => $request->customer_id,
                'currency_id' => $currency->id,
                'quotation_material_id' => $request->quotation_material_id,
                'salesman_id' => $request->salesman_id,
                'department_id' => $employee->department_id,
                'get_total' => $request->get_total,
                'discount_all_item' => (int) $request->discount_all_item,
                'sub_total' => $request->sub_total,
                'tax_id' => $request->tax_id,
                'total' => $request->total,
                'description' => $request->description,
                'payment_term_id' => $request->payment_term_id,
                'depot_id' => $employee->depot_id,
                'shift' => $shift,
            ]);

            foreach ($request->products as $row) {
                $subTotal = ($row['price'] * $row['qty']) - ($row['discountAmount'] ? $row['discountAmount'] : 0 ) - ($row['discountRate'] ? (($row['discountRate'] / 100) * $row['price']) : 0) * $row['qty'];
                if ($row['item'] != null) {
                    SalesOrderItem::create([
                        'sales_order_id' => $qm->id,
                        'product_id' => $row['item'],
                        'qty' => (int) $row['qty'],
                        'delivered_qty' => (int) $row['qty'],
                        'uom_id' => $row['uom'],
                        'price' => $row['price'],
                        'discount_rate' => $row['discountRate'],
                        'discount_amount' => $row['discountAmount'],
                        'sub_total' => $subTotal,
                        'description' => $row['description']
                    ]);


                    $product = Product::findOrFail($row['item']);
                    $seal = Product::whereName('Seal')->orWhere('code','SL')->first();
                    $tissu = Product::whereName('Tissue Basah')->orWhere('code','TB')->first();
                    $galon = Product::whereName('Galon Kosong')->orWhere('code','GK')->first();
                    if(!$galon){
                        $galon = Product::create(['code' => 'GK','name' => 'Galon Kosong', 'brand' => 'AerPlus','product_category_id' => 2, 'product_sub_category_id' => 3,'uom_id'=>10,'description' => 'Galon Kosong']);
                    }

                    $stockSeal = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $seal->id)->first();
                    $stockGalon  = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $galon->id)->first();
                    $stockTissu  = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $tissu->id)->first();
                    $stockProduct  = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $row['item'])->first();
                    $stockAir  = Stock::where('warehouse_id', $employee->depot_id)->where('description', 'Stock Air')->first();

                    if(!$stockSeal){
                        $stockSeal = Stock::create(['product_id' => $seal->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000, 'in' => 1000]);
                    }elseif($stockSeal->qty < (int) $row['qty']){
                        noty()->danger('Stok Seal Tidak Mencukupi');

                    }
                    if(!$stockGalon){
                        $stockGalon = Stock::create(['product_id' => $galon->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000,'in' => 1000]);
                    }elseif($stockGalon->qty < (int) $row['qty']){
                        noty()->danger('Stok Galon Tidak Mencukupi');

                    }
                    if(!$stockTissu){
                        $stockTissu = Stock::create(['product_id' => $tissu->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000,'in' => 1000]);
                    }elseif($stockTissu->qty < (int) $row['qty']){
                        noty()->danger('Stok Tissu Basah Tidak Mencukupi');
                    }
                    if(!$stockProduct){
                        $stockProduct = Stock::create(['product_id' => $product->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000,'in' => 1000]);
                    }elseif($stockProduct->qty < (int) $row['qty']){
                        noty()->danger('Stok '.$product->name.' Tidak Mencukupi');

                    }

                    $jml = (int) $row['qty'] * (int) 19.5;
                    if(!$stockAir){
                        $stockAir = Stock::create(['warehouse_id' => $employee->depot_id, 'qty' => 8000 ,'in' => 8000, 'description' => 'Stock Air']);
                    }elseif((int) $stockAir->qty < $jml){
                        noty()->danger('Stok Air Tidak Mencukupi');

                    }

                    if($product->name == 'Air Galon Isi Ulang'){
                        $req_air = $stockAir;
                        $req_air->qty = (int) $req_air->qty - $jml;
                        $req_air->status = true;
                        $req_air->out = (int) $req_air->out + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = $employee->depot_id;
                        $trans->type = 'OUT';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Sales Order Air No : '.$qm->code_number;
                        $trans->in_hand = (int) $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->sales_order_id = $qm->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty - (int) $row['qty'];
                        $req_seal->out = (int) $req_seal->out + (int) $row['qty'];
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = $employee->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'OUT';
                        $transSeal->qty = (int) $row['qty'];
                        $transSeal->status  = true;
                        $transSeal->description = 'Sales Order Seal No : '.$qm->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->sales_order_id = $qm->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty - (int) $row['qty'];
                        $req_tissu->out = (int) $req_tissu->out + (int) $row['qty'];
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = $employee->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'OUT';
                        $transTissu->qty = (int) $row['qty'];
                        $transTissu->status  = true;
                        $transTissu->description = 'Sales Order Tissu Basah No : '.$qm->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->sales_order_id = $qm->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty - (int) $row['qty'];
                        $req_produk->out = (int) $req_produk->out + (int) $row['qty'];
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = $employee->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'OUT';
                        $transProduk->qty = (int) $row['qty'];
                        $transProduk->status  = 1;
                        $transProduk->description = 'Sales Order '.$product->name.' No : '.$qm->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->sales_order_id = $qm->id;
                        $transProduk->save();

                    }
                    if($product->name == 'Air Galon'){
                        $req_air = $stockAir;
                        $req_air->qty = (int) $req_air->qty - $jml;
                        $req_air->status = true;
                        $req_air->out = (int) $req_air->out + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = $employee->depot_id;
                        $trans->type = 'OUT';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Sales Order Air No : '.$qm->code_number;
                        $trans->in_hand = (int) $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->sales_order_id = $qm->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty - (int) $row['qty'];
                        $req_seal->out = (int) $req_seal->out + (int) $row['qty'];
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = $employee->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'OUT';
                        $transSeal->qty = (int) $row['qty'];
                        $transSeal->status  = true;
                        $transSeal->description = 'Sales Order Seal No : '.$qm->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->sales_order_id = $qm->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty - (int) $row['qty'];
                        $req_tissu->out = (int) $req_tissu->out + (int) $row['qty'];
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = $employee->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'OUT';
                        $transTissu->qty = (int) $row['qty'];
                        $transTissu->status  = true;
                        $transTissu->description = 'Sales Tissu Basah No : '.$qm->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->sales_order_id = $qm->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty - (int) $row['qty'];
                        $req_produk->out = (int) $req_produk->out + (int) $row['qty'];
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = $employee->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'OUT';
                        $transProduk->qty =(int) $row['qty'];
                        $transProduk->status  = 1;
                        $transProduk->description = 'Sales '.$product->name.' No : '.$qm->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->sales_order_id = $qm->id;
                        $transProduk->save();

                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty - (int) $row['qty'];
                        $req_galon->out = (int) $req_galon->out + (int) $row['qty'];
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = $employee->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'OUT';
                        $transGalon->qty =(int) $row['qty'];
                        $transGalon->status  = 1;
                        $transGalon->description = 'Sales Galon  No : '.$qm->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->sales_order_id = $qm->id;
                        $transGalon->save();
                   }

                   if($product->name == 'Galon Kosong'){
                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty - (int) $row['qty'];
                        $req_galon->out = (int) $req_galon->out + (int) $row['qty'];
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = $employee->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'OUT';
                        $transGalon->qty =(int) $row['qty'];
                        $transGalon->status  = 1;
                        $transGalon->description = 'Sales Galon  No : '.$qm->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->sales_order_id = $qm->id;
                        $transGalon->save();
                }
            }

        }

        foreach ($getApproval as $row) {
            SalesOrderApproval::create([
                'sales_order_id' => $qm->id,
                'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                'role' => isset($row->role) ? $row->role : null,
                'is_approved' => false,
            ]);
        }

            // if ($firstApproval->approver_id){

            // Mail::to($firstApproval->approver->email)->send(new ApproverEmail($qm));
            // }

    } catch(\Exception $e)
    {
        DB::rollback();
        flash('Ops, try again: '.$e->getMessage())->error();
         noty()->danger('Opps!', 'Something Went Wrong');
         return response()->json([
        'success' => true,
        'message' => 'nope',
        'message' => $e->getMessage(),
        'line' => $e->getLine(),
        'redirect' => route('transaction.sales_order.index')
        ]);

    }
    DB::commit();

    noty()->success('Yeay!', 'Your entry has been added successfully');
    return response()->json([
        'success' => true,
        'message' => 'yey',
        'redirect' => route('transaction.sales_order.index')
    ]);
}

    /**
    * Show the specified resource.
    * @param int $id
    * @return Response
    */
    public function show($id)
    {
        $row = SalesOrder::find($id);

        $media = $row->getMedia('sales_order');

        if($row){
            $media = $row->getMedia('sales_order');

        } else {
            $media = '';
        }
        return view('transaction::sales_order.entry.modals.attachment',compact('media'))->withMedia($media)->withRow($row);
    }

    /**
    * Show the form for editing the specified resource.
    * @param int $id
    * @return Response
    */
    public function edit($id)
    {
        return view('transaction::sales_order.entry.edit');
    }

    /**
    * Update the specified resource in storage.
    * @param Request $request
    * @param int $id
    * @return Response
    */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit sales order');

        DB::beginTransaction();
        try {
            # action for resubmit

            # updated data
            $pr = SalesOrder::findOrFail($id);
            $pr->customer_id = $request->customer_id;
            $pr->salesman_id = $request->salesman_id;
            $pr->get_total = $request->get_total;
            $pr->discount_all_item = $request->discount_all_item;
            $pr->sub_total = $request->sub_total;
            $pr->total = $request->total;
            $pr->description = $request->description;
            $pr->payment_term_id = $request->payment_term_id;
            $pr->project_id = $request->project_id;
            $pr->save();

            $st = StockTransaction::where('sales_order_id', $pr->id)->where('type', 'OUT')->get();
            foreach($st as $data){
                $stock = Stock::where('product_id', $data->product_id)->where('warehouse_id',$pr->depot_id)->first();
                if($stock){
                    $stock = $stock;
                    $stock->qty = (int) $stock->qty + (int) $data->qty;
                    $stock->out = (int) $stock->out - (int) $data->qty;
                    $stock->save();
                 }else{
                    $stock = Stock::whereNull('product_id')->where('warehouse_id',$pr->depot_id)->first();
                    $stock->qty = (int) $stock->qty + (int) $data->qty;
                    $stock->out = (int) $stock->out - (int) $data->qty;
                    $stock->save();
                }
                $data->delete();
            }




             foreach ($request->products as $row) {
                if ($row['item'] != null) {

                $subTotal = ($row['price'] * $row['qty']) - ($row['discountAmount'] ? $row['discountAmount'] : 0 ) - ($row['discountRate'] ? (($row['discountRate'] / 100) * $row['price']) : 0) * $row['qty'];


                    if (array_key_exists('id', $row)) {
                        $item = SalesOrderItem::find($row['id']);

                        $item->update([
                        'product_id' => $row['item'],
                        'qty' => (int) $row['qty'],
                        'uom_id' => $row['uom'],
                        'price' => $row['price'],
                        'discount_rate' => $row['discountRate'],
                        'discount_amount' => $row['discountAmount'],
                        'sub_total' => $subTotal,
                        'description' => $row['description']

                        ]);

                    } else {
                      $item =  SalesOrderItem::create([
                        'sales_order_id' => $id,
                        'product_id' => $row['item'],
                        'qty' => (int) $row['qty'],
                        'uom_id' => $row['uom'],
                        'price' => $row['price'],
                        'discount_rate' => $row['discountRate'],
                        'discount_amount' => $row['discountAmount'],
                        'sub_total' => $subTotal,
                        'description' => $row['description']
                        ]);


                    }



                    $product = Product::findOrFail($row['item']);
                    $seal = Product::whereName('Seal')->orWhere('code','SL')->first();
                    $tissu = Product::whereName('Tissue Basah')->orWhere('code','TB')->first();
                    $galon = Product::whereName('Galon Kosong')->orWhere('code','GK')->first();
                    if(!$galon){
                        $galon = Product::create(['code' => 'GK','name' => 'Galon Kosong', 'brand' => 'AerPlus','product_category_id' => 2, 'product_sub_category_id' => 3,'uom_id'=>10,'description' => 'Galon Kosong']);
                    }

                    $stockSeal = Stock::where('warehouse_id', $pr->depot_id)->where('product_id', $seal->id)->first();
                    $stockGalon  = Stock::where('warehouse_id', $pr->depot_id)->where('product_id', $galon->id)->first();
                    $stockTissu  = Stock::where('warehouse_id', $pr->depot_id)->where('product_id', $tissu->id)->first();
                    $stockProduct  = Stock::where('warehouse_id', $pr->depot_id)->where('product_id', $row['item'])->first();
                    $stockAir  = Stock::where('warehouse_id', $pr->depot_id)->where('description', 'Stock Air')->first();

                    if(!$stockSeal){
                        $stockSeal = Stock::create(['product_id' => $seal->id, 'warehouse_id' => $pr->depot_id, 'qty' => 1000, 'in' => 1000]);
                    }elseif($stockSeal->qty < (int) $row['qty']){
                        noty()->danger('Stok Seal Tidak Mencukupi');

                    }
                    if(!$stockGalon){
                        $stockGalon = Stock::create(['product_id' => $galon->id, 'warehouse_id' => $pr->depot_id, 'qty' => 1000,'in' => 1000]);
                    }elseif($stockGalon->qty < (int) $row['qty']){
                        noty()->danger('Stok Galon Tidak Mencukupi');

                    }
                    if(!$stockTissu){
                        $stockTissu = Stock::create(['product_id' => $tissu->id, 'warehouse_id' => $pr->depot_id, 'qty' => 1000,'in' => 1000]);
                    }elseif($stockTissu->qty < (int) $row['qty']){
                        noty()->danger('Stok Tissu Basah Tidak Mencukupi');
                    }
                    if(!$stockProduct){
                        $stockProduct = Stock::create(['product_id' => $product->id, 'warehouse_id' => $pr->depot_id, 'qty' => 1000,'in' => 1000]);
                    }elseif($stockProduct->qty < (int) $row['qty']){
                        noty()->danger('Stok '.$product->name.' Tidak Mencukupi');

                    }

                    $jml = (int) $row['qty'] * (int) 19.5;
                    if(!$stockAir){
                        $stockAir = Stock::create(['warehouse_id' => $pr->depot_id, 'qty' => 8000,'in' => 8000, 'description' => 'Stock Air']);
                    }elseif((int) $stockAir->qty < $jml){
                        noty()->danger('Stok Air Tidak Mencukupi');

                    }

                    if($product->name == 'Air Galon Isi Ulang'){
                        $req_air = $stockAir;
                        $req_air->qty = (int) $req_air->qty - $jml;
                        $req_air->status = true;
                        $req_air->out = (int) $req_air->out + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = $pr->depot_id;
                        $trans->type = 'OUT';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Sales Order Air No : '.$pr->code_number;
                        $trans->in_hand = (int) $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->sales_order_id = $pr->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty - (int) $row['qty'];
                        $req_seal->out = (int) $req_seal->out + (int) $row['qty'];
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = $pr->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'OUT';
                        $transSeal->qty = (int) $row['qty'];
                        $transSeal->status  = true;
                        $transSeal->description = 'Sales Order Seal No : '.$pr->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->sales_order_id = $pr->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty - (int) $row['qty'];
                        $req_tissu->out = (int) $req_tissu->out + (int) $row['qty'];
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = $pr->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'OUT';
                        $transTissu->qty = (int) $row['qty'];
                        $transTissu->status  = true;
                        $transTissu->description = 'Sales Order Tissu Basah No : '.$pr->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->sales_order_id = $pr->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty - (int) $row['qty'];
                        $req_produk->out = (int) $req_produk->out + (int) $row['qty'];
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = $pr->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'OUT';
                        $transProduk->qty = (int) $row['qty'];
                        $transProduk->status  = 1;
                        $transProduk->description = 'Sales Order '.$product->name.' No : '.$pr->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->sales_order_id = $pr->id;
                        $transProduk->save();

                    }
                    if($product->name == 'Air Galon'){
                        $req_air = $stockAir;
                        $req_air->qty = (int) $req_air->qty - $jml;
                        $req_air->status = true;
                        $req_air->out = (int) $req_air->out + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = $pr->depot_id;
                        $trans->type = 'OUT';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Sales Order Air No : '.$pr->code_number;
                        $trans->in_hand = (int) $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->sales_order_id = $pr->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty - (int) $row['qty'];
                        $req_seal->out = (int) $req_seal->out + (int) $row['qty'];
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = $pr->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'OUT';
                        $transSeal->qty = (int) $row['qty'];
                        $transSeal->status  = true;
                        $transSeal->description = 'Sales Order Seal No : '.$pr->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->sales_order_id = $pr->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty - (int) $row['qty'];
                        $req_tissu->out = (int) $req_tissu->out + (int) $row['qty'];
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = $pr->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'OUT';
                        $transTissu->qty = (int) $row['qty'];
                        $transTissu->status  = true;
                        $transTissu->description = 'Sales Tissu Basah No : '.$pr->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->sales_order_id = $pr->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty - (int) $row['qty'];
                        $req_produk->out = (int) $req_produk->out + (int) $row['qty'];
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = $pr->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'OUT';
                        $transProduk->qty = (int) $row['qty'];
                        $transProduk->status  = 1;
                        $transProduk->description = 'Sales '.$product->name.' No : '.$pr->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->sales_order_id = $pr->id;
                        $transProduk->save();

                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty - (int) $row['qty'];
                        $req_galon->out = (int) $req_galon->out + (int) $row['qty'];
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = $pr->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'OUT';
                        $transGalon->qty =(int) $row['qty'];
                        $transGalon->status  = 1;
                        $transGalon->description = 'Sales Galon  No : '.$pr->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->sales_order_id = $pr->id;
                        $transGalon->save();
                   }

                   if($product->name == 'Galon Kosong'){
                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty - (int) $row['qty'];
                        $req_galon->out = (int) $req_galon->out + (int) $row['qty'];
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = $pr->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'OUT';
                        $transGalon->qty =(int) $row['qty'];
                        $transGalon->status  = 1;
                        $transGalon->description = 'Sales Galon  No : '.$pr->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->sales_order_id = $pr->id;
                        $transGalon->save();
                    }



                }
            }

        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            noty()->danger('Opps!', 'Something Went Wrong');
             return response()->json([
            'success' => true,
            'message' => 'nope',
            'message' => $e->getMessage(),
            'line' => $e->getLine(),
            'redirect' => route('transaction.sales_order.index')
            ]);
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('transaction.sales_order.index')
        ]);
    }

    /**
    * Remove the specified resource from storage.
    * @param int $id
    * @return Response
    */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete sales order');

        $row = SalesOrder::findOrFail($id);

        $soi = SalesOrderItem::where('sales_order_id', $row->id)->delete();

        $st = StockTransaction::where('sales_order_id', $row->id)->get();

        if($st){
            foreach($st as $data){
                $stock = Stock::findOrfail($data->stock_id);
                if($stock){
                    if($stock->description == 'Stock Air'){
                        $jml = $data->qty;
                    }else{

                        $jml = $data->qty;
                    }
                    $stock = $stock;
                    $stock->qty = $stock->qty + $jml;
                    $stock->out = $stock->out - $jml;
                    $stock->save();

                }
                $data->destroy();
            }
        }

        SalesOrderApproval::where('sales_order_id', $row->id)->delete();

        $row->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return redirect()->route('transaction.sales_order.report.index');
    }

    public function detail($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
        if($row){
            $media = $row->getMedia('sales_order');
        }else{
            $media = [];
        }

        return view('transaction::sales_order.entry.detail', compact('row', 'items', 'approvals', 'media'));
    }

     public function print($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
        if($row){
            $media = $row->getMedia('sales_order');
        }else{
            $media = [];
        }

        return view('transaction::sales_order.entry.print', compact('row', 'items', 'approvals', 'media'));
    }

    public function currency()
    {
        $currency = Currency::where('name', 'RM')->first();
        return $currency;
    }

    public function quotationMaterial()
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first()->id;
        $quotationMaterial = QuotationMaterial::where('employee_id', $employee)->where('status', 'approved')->get();
        return $quotationMaterial;
    }

    public function quotationMaterialFirst()
    {
        return QuotationMaterial::with(['department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])
        ->find(request()->quotation_material_id);
    }

    public function fetchQuotationMaterialItem()
    {
        return QuotationMaterialItem::with(['product', 'uom'])->where('status', true)
        ->where('quotation_material_id', request()->quotation_material_id)->get();
    }

    public function report(){
        $salesOrder = [];
        $roleName = auth()->user()->getRoleNames();

        $employee = Employee::where('user_id', auth()->user()->id)->first();

        $fromDate  = (!empty(request('from_date'))) ? request('from_date') : '';
        $untilDate = (!empty(request('until_date'))) ? request('until_date') : '';
        $page = '50';

        if($fromDate && $untilDate && $employee){
            $salesOrder = SalesOrder::withoutGlobalScopes()
            ->with('employee', 'department', 'salesman', 'tax', 'customer', 'paymentTerm')
            ->whereNull('deleted_at')
            ->whereNull('cancel_approved_at')
            ->whereRaw("date(sales_orders.date) >= '" . $fromDate . "' AND date(sales_orders.date) <= '" . $untilDate . "' ")->whereRaw("employee_id = '$employee->id' or approver_to_go = '$employee->id' or latest_approver = '$employee->id'")->whereNull('deleted_at')->paginate($page);
        }

        return view('transaction::report.sales_order', compact('salesOrder'));

    }

    public function detailReport($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
        $from_date = request('from_date');
        $until_date = request('until_date');
        $vouchers = Voucher::where('sales_order_id', $id)->get();
        if($row){
            $media = $row->getMedia('sales_order');
        }else{
            $media = [];
        }

        return view('transaction::report.sales_order_detail', compact('row', 'items', 'approvals','from_date','until_date', 'media', 'vouchers'));
    }

    public function saveAttachment()
    {
        $row = SalesOrder::find(request('id_sales_order'));

        if (request()->hasFile('attachment')) {
            foreach (request()->attachment as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('sales_order');
            }
        }

        return redirect()->route('transaction.sales_order.index', compact('row'));
    }

    public function export()
    {
        return Excel::download(new SalesOrderExport, 'SalesOrder.xlsx');
    }

    public function customerDetail($id)
    {
        $customer = Customer::find($id);
        return $customer;
    }
}
