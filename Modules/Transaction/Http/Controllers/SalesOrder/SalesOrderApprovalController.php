<?php

namespace Modules\Transaction\Http\Controllers\SalesOrder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Transaction\DataTables\SalesOrderApprovalDatatable;

class SalesOrderApprovalController extends Controller
{
    public function index(SalesOrderApprovalDatatable $datatable)
    {
        $this->hasPermissionTo('view sales order approval');

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if (empty($employee)){
            flash('Ops.. please check your employee id.')->error();
            return back();
        }

        $rows = filterModuleByStatus(SalesOrder::class, [
            'employee', 'department', 'salesman', 'tax', 'customer', 'paymentTerm', 'quotationMaterial'
        ]);
        
        return $datatable->render('transaction::sales_order.approval.index', compact('rows'));
    }




    public function show($id)
    {
        $this->hasPermissionTo('view sales order approval');

        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('status', 1)->where('sales_order_id', $id)->get();
        $histories = SalesOrderApproval::where('sales_order_id', $id)
            ->get()
            ->sortBy('id');
        if($row){
            $media = $row->getMedia('sales_order') ?? [];
        }else{
            $media = [];
        }

        return view('transaction::sales_order.approval.detail', compact('row', 'items','histories','media'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit sales order approval');

        $description = request('description');
        if (request()->has('approve')) {
            \Approval::action(SalesOrder::class, SalesOrderApproval::class, $id, 'sales_order_id', $description);
        } else {
            \Approval::reject(SalesOrder::class, SalesOrderApproval::class, $id, 'sales_order_id', $description);
        }

        flash('Your data has been saved successfully')->success();
        return redirect()->route('transaction.sales_order.approval.index');
    }

    public function updateItem(Request $request, $id)
    {
        $this->hasPermissionTo('delete sales order approval');

        $row = SalesOrderItem::findOrFail($id);
        $row->status = 0;
        $row->save();

        $quotation = SalesOrder::find($row->sales_order_id);

        $getTotal = $quotation->get_total - $row->sub_total;
        $subTotal = $quotation->sub_total - $row->sub_total;
        $gst = ($quotation->tax->rate / 100) * $subTotal;
        $total = $subTotal + $gst;

        $quotation->get_total = (int)$getTotal;
        $quotation->sub_total = (int)$subTotal;
        $quotation->total = (int)$total;
        $quotation->save();

        flash('Your data has been deleted successfuly')->error();
        return back();
    }
}
