<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryOrderMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_order_materials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('code_number');
            $table->date('date');
            $table->string('title');
            $table->integer('sales_order_id');
            $table->integer('customer_id');
            $table->integer('salesman_id');
            $table->integer('department_id');
            $table->integer('payment_term_id');
            $table->integer('vehicle_id')->nullable();
            $table->string('driver')->nullable();
            $table->text('description')->nullable();
            $table->text('address')->nullable();

            $table->enum('status', ['waiting', 'approved', 'rejected']);
            $table->integer('approver_to_go')->nullable();
            $table->integer('latest_approver')->nullable();
            $table->string('role_approval')->nullable();
            $table->boolean('is_print')->default(false);
            $table->text('rejected_reason')->nullable();
            $table->integer('rejected_by')->nullable();
            $table->date('rejected_at')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_materials');
    }
}
