<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationMaterialItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_material_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('quotation_material_id');
            $table->integer('product_id');
            $table->integer('uom_id');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('discount_rate')->nullable();
            $table->integer('discount_amount')->nullable();
            $table->float('sub_total');
            $table->boolean('status')->default(1);
            $table->string('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_material_items');
    }
}
