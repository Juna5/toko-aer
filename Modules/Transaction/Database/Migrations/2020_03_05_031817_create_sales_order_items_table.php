<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_items', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sales_order_id');
            $table->integer('product_id');
            $table->integer('uom_id');
            $table->integer('qty');
            $table->integer('price');
            $table->integer('discount_rate')->nullable();
            $table->integer('discount_amount')->nullable();
            $table->float('sub_total');
            $table->float('delivered_qty')->nullable();
            $table->string('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_items');
    }
}
