<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_returns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id')->nullable();
            $table->integer('invoice_id')->nullable();
            $table->date('date')->nullable();
            $table->string('code_number')->nullable();
            $table->text('description')->nullable();
            $table->float('subtotal_exclude_discount')->nullable();
            $table->float('discount_all_item')->nullable();
            $table->float('subtotal_include_discount')->nullable();
            $table->float('discount_gst')->nullable();
            $table->float('amount_gst')->nullable();
            $table->float('grand_total')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_returns');
    }
}
