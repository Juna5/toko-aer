<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_id');
            $table->string('code_number');
            $table->date('date');
            $table->string('title');
            $table->integer('quotation_material_id')->nullable();
            $table->integer('customer_id');
            $table->integer('salesman_id');
            $table->integer('department_id');
            $table->float('get_total');
            $table->integer('discount_all_item')->nullable();
            $table->float('sub_total');
            $table->integer('tax_id');
            $table->integer('currency_id');
            $table->float('total');
            $table->integer('payment_term_id');
            $table->text('description')->nullable();

            $table->enum('status', ['waiting', 'approved', 'rejected']);
            $table->integer('approver_to_go')->nullable();
            $table->integer('latest_approver')->nullable();
            $table->string('role_approval')->nullable();
            $table->boolean('is_print')->default(false);
            $table->text('rejected_reason')->nullable();
            $table->integer('rejected_by')->nullable();
            $table->date('rejected_at')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_orders');
    }
}
