<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesReturnDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_return_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sales_return_id')->nullable();
            $table->integer('invoice_detail_id')->nullable();
            $table->string('item')->nullable();
            $table->string('uom')->nullable();
            $table->integer('qty')->nullable();
            $table->float('price')->nullable();
            $table->integer('discount_amount')->nullable();
            $table->float('sub_total')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_return_details');
    }
}
