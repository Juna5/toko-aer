<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveryOrderMaterialItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_order_material_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('delivery_order_material_id');
            $table->integer('sales_order_item_id');
            $table->integer('sales_order_id');
            $table->float('qty');
            $table->integer('warehouse_id');
            $table->text('description')->nullable();
            $table->boolean('status')->default(1);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('delivery_order_material_items');
    }
}
