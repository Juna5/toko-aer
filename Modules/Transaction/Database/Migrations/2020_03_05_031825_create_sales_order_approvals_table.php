<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesOrderApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_order_approvals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('sales_order_id');
            $table->integer('employee_id')->nullable();
            $table->string('role')->nullable();
            $table->boolean('is_approved');
            $table->date('date')->nullable();
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_order_approvals');
    }
}
