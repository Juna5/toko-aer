<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class SalesOrderApprovalTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add sales order approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit sales order approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view sales order approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete sales order approval', 'created_at' => now()],
         ]);
    }
}
