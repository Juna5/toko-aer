<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class DeliveryOrderMaterialTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Permission::insert([
                        ['guard_name' => 'web', 'name' => 'add delivery order material', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'edit delivery order material', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'delete delivery order material', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'view delivery order material', 'created_at' => now()],
                ]);
        }
}
