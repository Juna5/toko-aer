<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TransactionDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call(QuotationMaterialTableSeeder::Class);
        // $this->call(QuotationMaterialApprovalTableSeeder::Class);
        // $this->call(SalesOrderTableSeeder::Class);
        // $this->call(SalesOrderApprovalTableSeeder::Class);
        // $this->call(DeliveryOrderMaterialTableSeeder::Class);
        // $this->call(DeliveryOrderMaterialApprovalTableSeeder::Class);
        // $this->call(QuotationMaterialReportTableSeeder::Class);
        $this->call(SalesOrderReportTableSeeder::Class);
//        $this->call(CreditNoteTableSeeder::Class);
        // $this->call(SalesReturnTableSeeder::Class);
    }
}
