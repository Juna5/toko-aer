<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class CreditNoteTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Permission::insert([
                        ['guard_name' => 'web', 'name' => 'add credit note', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'edit credit note', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'delete credit note', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'view credit note', 'created_at' => now()],
                ]);
        }
}
