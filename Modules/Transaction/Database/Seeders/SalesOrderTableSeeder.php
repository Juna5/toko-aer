<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class SalesOrderTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Permission::insert([
                        ['guard_name' => 'web', 'name' => 'add sales order', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'edit sales order', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'delete sales order', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'view sales order', 'created_at' => now()],
                ]);
        }
}
