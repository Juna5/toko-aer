<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class SalesReturnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add sales return', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit sales return', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete sales return', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view sales return', 'created_at' => now()],
        ]);
    }
}
