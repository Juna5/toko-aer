<?php

namespace Modules\Transaction\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class QuotationMaterialTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Permission::insert([
                        ['guard_name' => 'web', 'name' => 'add quotation material', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'edit quotation material', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'delete quotation material', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'view quotation material', 'created_at' => now()],
                ]);
        }
}
