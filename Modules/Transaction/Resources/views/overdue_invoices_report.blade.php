@extends('transaction::layouts.master')

@section('transaction::title', 'Overdue Invoices Report')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Penjualan',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Overdue Invoices Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Invoice Number</th>
                                <th>Invoice Date</th>
                                <th>Invoice Due Date</th>
                                <th>Created By</th>
                                <th>Amount</th>
                                <th>Outstanding</th>
                                <th>Payment Status</th>
                            </tr>

                            @forelse($overDueInvoice as $index => $row)
                                @php
                                    if ($row->payment_status == 'Paid') {
                                        $status = 'badge badge-success';
                                    } elseif ($row->payment_status == 'Paid Partially') {
                                        $status = 'badge badge-warning';
                                    } elseif ($row->payment_status == 'Unpaid') {
                                        $status = 'badge badge-primary';
                                    }
                                @endphp
                                <tr align="center" >
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('finance.invoice.show', $row->id) }}" target="_blank">{{ $row->invoice_number }}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->invoice_date) }}</td>
                                    <td>{{ format_d_month_y($row->invoice_due_date) }}</td>
                                    <td>{{ $row->employee_id ? optional($row->employee)->name : '-' }}</td>
                                    <td style="text-align: right;">{{ amount_international_with_comma($row->total) }}</td>
                                    <td style="text-align: right;">{{ amount_international_with_comma($row->outstanding) }}</td>
                                    <td><span class="{{$status}}">{{ $row->payment_status }}</span></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="row clearfix" style="margin-top:20px">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <table class="table table-bordered table-hover" id="tab_logic_total">
                                        <tbody id="grand_total">
                                        <tr>
                                            <th class="text-center">Grand Total</th>
                                            <td class="text-center">
                                                {{ amount_international_with_comma($total) }}
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-left" style="margin-bottom: 2%">
                            <a href="{{ route('billing.view') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
