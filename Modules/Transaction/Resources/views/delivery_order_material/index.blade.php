@extends('transaction::layouts.master')

@section('transaction::title', 'Delivery Order')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Delivery Order',
'active' => true,
'url' => route('transaction.delivery_order_material.view')
])
@endsection

@section('transaction::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-clipboard-list"></i>
                </div>
                <div class="card-body">
                    <h4>Delivery Order Entry</h4>
                    <p>Create order material.</p>
                    @can('view sales order')
                    <a href="{{ route('transaction.delivery_order_material.index') }}" class="card-cta">Go to Delivery Order Entry <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-envelope-open-text"></i>
                </div>
                <div class="card-body">
                    <h4>Delivery Order Approval</h4>
                    <p>All order material approval data.</p>
                    @can('view sales order approval')
                    <a href="{{ route('transaction.delivery_order_material.approval.index') }}" class="card-cta">Go to Delivery Order Approval <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-folder-open"></i>
                </div>
                <div class="card-body">
                    <h4>Delivery Order Report</h4>
                    <p>ViewDelivery Order Report</p>
                    @can('view sales order')
                    <a href="{{ route('transaction.delivery_order_material.delivery-order-report') }}" class="card-cta">Go to Delivery Order Report <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
