@extends('transaction::layouts.master')

@section('transaction::title', 'Delivery Order')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Delivery Order',
'active' => true,
'url' => route('transaction.delivery_order_material.index')
])
@endsection

@section('transaction::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary ">{{ format_d_month_y($row->date) }} <br>{{ optional($row->employee)->name }}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-md-right">
                                                <div class="invoice-number text-primary ">{{ $row->code_number }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-md">
                                                    <!-- <tr>
                                                        <td width="30%">Title</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->title }}</td>
                                                    </tr> -->
                                                    <tr>
                                                        <td width="30%">Sales Order</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->salesOrder->code_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Customer</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->customer)->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Salesman</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->salesman)->name }}</td>
                                                    </tr>
                                                    <!-- <tr>
                                                        <td width="30%">Department</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->department)->name }}</td>
                                                    </tr> -->
                                                    <tr>
                                                        <td width="30%">Payment Term</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->salesOrder->paymentTerm)->code . ' - ' . optional($row->salesOrder->paymentTerm)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Address</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->address }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Description</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->description }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">

                                                    <tr>
                                                        <td width="30%">Status</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ ucfirst($row->status) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Final Approver</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->latestApprove)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Next Approver</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval) }}</td>
                                                    </tr>
                                                    @if($row->rejected_by)
                                                        <tr>
                                                            <td width="30%">Rejected By</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ optional($row->rejectedBy)->name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Rejected At</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ format_d_month_y($row->rejected_at) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Rejected Reason</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ $row->rejected_reason }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <div class="section-title">Item</div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-md">
                                            <tr>
                                                <th>Item</th>
                                                <th>UoM</th>
                                                <th>Qty</th>
                                                <th>From Warehouse</th>
                                                <th>Description</th>
                                            </tr>
                                            @foreach($items as $item)
                                                <tr>
                                                    <td>{{ optional($item->salesOrderMaterialItem->product)->name }}</td>
                                                    <td>{{ optional($item->salesOrderMaterialItem->uom)->name }}</td>
                                                    <td align="right">{{ amount_international_with_comma($item->qty) }}</td>

                                                    <td>{{ optional($item->warehouse)->name }}</td>

                                                    <td>{{ $item->description }}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-lg-12">
                                            <div class="section-title">Attachment</div>
                                                <div class="form-group m-form__group row">
                                                @foreach($media as $index => $med)
                                                    <div class='form-group col-md-2'>
                                                        <div style="display:block;position:relative;overflow:hidden;width:100%;max-width:100%;height:200px;padding:5px 10px;font-size:14px;line-height:22px;color:#777;background-color:#FFF;background-image:none;text-align:center;">
                                                            <img src="{{ asset($med->getFullUrl()) }}" data-default-file="{{ asset($med->getFullUrl()) }}" style="top:50%;-webkit-transform:translate(0,-50%);transform:translate(0,-50%);position:relative;max-width:100%;max-height:100%;background-color:#FFF;-webkit-transition:border-color .15s linear;transition:border-color .15s linear;">

                                                        </div>
                                                        <span>
                                                        <br>
                                                            <a href="{{ asset($med->getFullUrl()) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                                                                                        <i class="fa fa-file text-white" ></i> View File
                                                            </a>
                                                        </span>
                                                    </div>
                                                 @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                           
                                    <div class="row mt-4">
                                        <div class="col-lg-12">
                                            <div class="section-title">History</div>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-md">
                                                    <tr>
                                                        <th>Employee</th>
                                                        <th>Status</th>
                                                        <th>Date</th>
                                                        <th>Description</th>
                                                    </tr>
                                                    @foreach($approvals as $approve)
                                                    <tr>
                                                        <td>{{ $approve->role}} {{ $approve->role &&  optional($approve->employee)->name ?'-': '' }} {{  optional($approve->employee)->name }}</td>
                                                        <td>{{ $approve->is_approved ? "Approved" : ($row->status == 'rejected' ? 'Rejected' : 'Waiting') }}</td>
                                                        <td>{{ format_d_month_y($approve->date) }}</td>
                                                        <td>{{ $approve->description }}</td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                </div>
                                <div class="text-md-right">
                                        <a href="{{ route('transaction.delivery_order_material.index') }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
