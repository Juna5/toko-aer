@extends('transaction::layouts.master')

@section('transaction::title', 'Delivery Order')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Penjualan',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Delivery Order Entry',
    'active' => true,
    'url' => route('transaction.delivery_order_material.index')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">
                <div class="card-body">
                    <div class="form-group pb-1">
                        <a href="{{ route('transaction.delivery_order_material.export') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                            <img src="{{asset('images/excel.png')}}" alt="">
                        </a>
                    </div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="document-tab"  href="{{ route('transaction.delivery_order_material.outstanding') }}" role="tab" aria-controls="outstanding" aria-selected="false">Outstanding</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        @include('transaction::delivery_order_material.entry.tab.entry')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('transaction::delivery_order_material.entry.modals.index')

    <script>
     $(document).on('click', '#modal_attachment', function(){
        var valId = $(this).data('id');
        $('#id_delivery_order_material').val(valId);
        var requestUrl = '{{ route('transaction.delivery_order_material.show', ':id') }}';
        var getData = requestUrl.replace(':id', valId);

          $('#attachment').modal('hide');

        $.ajax({
            url: getData,
            type: "GET",
            success: function(media) {

           $("#attachment").find('.modal-body').html(media)

            }
        });
    });
</script>
    </script>
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
        $('.dropify').dropify();
        $('.add-image-btn').click(function() {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='20M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
    </script>
@endpush
