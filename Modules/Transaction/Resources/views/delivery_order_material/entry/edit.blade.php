@extends('transaction::layouts.master')

@section('transaction::title', 'Edit Delivery Order')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Delivery Order',
'active' => true,
'url' => route('transaction.delivery_order_material.index')
])
@endsection
@section('transaction::breadcrumb-3')
@include('transaction::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('transaction.delivery_order_material.edit', request()->segment(3))
])
@endsection
@section('transaction::content')

<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('transaction.delivery_order_material.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        @if(request()->resubmit)
                        <delivery-order-material-edit :id="{{ request()->segment(3) }}" :resubmit="{{request()->resubmit}}"/>
                            @endif
                            <delivery-order-material-edit :id="{{ request()->segment(3) }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
