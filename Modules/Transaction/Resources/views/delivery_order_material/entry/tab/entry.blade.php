<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
	<div class="card-body">	
        <h4 class="card-title" style="text-align: center;">
        	Entry
    	</h4>
        @include('flash::message')
        @include('include.error-list')
        <div class="row">
            <div class="col-12">           		
            	<div class="table-responsive">
                    {!! $dataTable->table() !!}
                </div>		            
            </div>
        </div>
	</div>
</div>

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush