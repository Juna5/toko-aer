@extends('transaction::layouts.master')

@section('transaction::title', 'Delivery Order Outstanding')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Penjualan',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Delivery Order Outstanding',
    'active' => true,
    'url' => route('transaction.delivery_order_material.outstanding')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">
                <div class="card-body">
                    <div class="form-group pb-1">
                        <a href="{{ route('transaction.delivery_order_material.export') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                            <img src="{{asset('images/excel.png')}}" alt="">
                        </a>
                    </div>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab" href="{{ route('transaction.delivery_order_material.index') }}" role="tab" aria-controls="home" aria-selected="false">Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="document-tab" data-toggle="tab" href="#outstanding" role="tab" aria-controls="outstanding" aria-selected="true">Outstanding</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        @include('transaction::delivery_order_material.entry.tab.outstanding')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
