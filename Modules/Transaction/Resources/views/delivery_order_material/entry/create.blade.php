@extends('transaction::layouts.master')

@section('transaction::title', 'Add Delivery Order')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Delivery Order',
'active' => true,
'url' => route('transaction.delivery_order_material.index')
])
@endsection
@section('transaction::breadcrumb-3')
@include('transaction::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('transaction.delivery_order_material.create')
])
@endsection
@section('transaction::content')
<div class="row" id="pr-create">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('transaction.delivery_order_material.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        @if(request()->id)
                        <delivery-order-material-create :so-id="{{request()->id}}"/>
                            @endif
                            <delivery-order-material-create/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
