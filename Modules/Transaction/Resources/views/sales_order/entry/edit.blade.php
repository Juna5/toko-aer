@extends('transaction::layouts.master')

@section('transaction::title', 'Edit Penjualan')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Billing',
'active' => true,
'url' => route('billing.view')
])
@endsection
@section('transaction::breadcrumb-3')
@include('transaction::include.breadcrum', [
'title' => 'Penjualan',
'active' => true,
'url' => route('transaction.sales_order.index')
])
@endsection
@section('transaction::breadcrumb-4')
@include('transaction::include.breadcrum', [
'title' => request()->resubmit ? 'Resubmit':'Edit',
'active' => false,
])
@endsection
@section('transaction::content')

<div class="row" id="pr-create">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('transaction.sales_order.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        @if(request()->resubmit)
                        <sales-order-material-edit :id="{{ request()->segment(3) }}" :resubmit="{{request()->resubmit}}"/>
                            @endif
                            <sales-order-material-edit :id="{{ request()->segment(3) }}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
