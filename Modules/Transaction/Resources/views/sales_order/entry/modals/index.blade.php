<div class="modal fade" id="attachment" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
        
            </div>
            <div class="modal-body" >
                
            </div>
        </div>
    </div>
</div>

@push('javascript')
<script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
        $('.dropify').dropify();
        $('.add-image-btn').click(function() {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='20M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
    </script>
@endpush