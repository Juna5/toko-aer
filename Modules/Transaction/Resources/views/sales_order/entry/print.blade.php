<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <div class="row">
                                            <div class="col-md-6" style="font-size:50px">
                                                <div class="invoice-number text-primary">{{ format_d_month_y($row->date) }} <br>{{ optional($row->employee)->name }} </div>
                                            </div>
                                            <div class="col-md-6 text-md-right" style="font-size:50px">
                                                <div class="invoice-number text-primary">{{ $row->code_number }}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive" >
                                                <table class="table table-hover table-lg" width="100%" style="font-size:50px">
                                                    <tr>
                                                        <td width="2%">Customer</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->customer)->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">Salesman</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->salesman)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="2%">Payment Term</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->paymentTerm)->code . ' - ' . optional($row->paymentTerm)->name }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row mt-4">
                                <div class="col-md-12">
                                    
                                    <br>
                                    <div class="">
                                        <table class="table table-hover table-lg" width="100%" style="font-size:45px">
                                            <tr>
                                                <th width="30%">Item</th>
                                                <th>UoM</th>
                                                <th>Qty</th>
                                                <th></th>
                                                <th>Price</th>
                                                <th></th>
                                               
                                            </tr>
                                            @foreach($items as $item)
                                            <tr >
                                                <td>{{ optional($item->product)->name }}</td>
                                                <td  align="center">{{ optional($item->uom)->name }}</td>
                                                <td align="center">{{ amount_international_with_comma($item->qty) }} </td>
                                                <td align="center"> x </td>
                                                <td  align="left">{{ amount_international_with_comma($item->price) }}</td>
                                                <td align="left">{{ amount_international_with_comma($item->qty * $item->price) }}</td>
                                               
                                            </tr>
                                            
                                            @endforeach
                                            <tr>
                                                <td colspan="6"><hr></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" align="left" width="1%">Total</td>
                                                <td  align="left"  style="font-weight: bold;" width="2%">{{ amount_international_with_comma($row->total) }}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    <hr>
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>