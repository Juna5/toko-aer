@extends('transaction::layouts.master')

@section('transaction::title', 'Penjualan')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Penjualan',
    'active' => true,
    'url' => route('transaction.sales_order.index')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('transaction::content')
    <div class="card" id="settings-card">
        @include('flash::message')
        <div class="card-body">
            <div class="card">
                <div class="form-group pb-1">
                    <a href="{{ route('transaction.sales_order.export') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                        <img src="{{asset('images/excel.png')}}" alt="">
                    </a>
                </div>
                <div class="table-responsive">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('transaction::sales_order.entry.modals.index')

    <script>
     $(document).on('click', '#modal_attachment', function(){
        var valId = $(this).data('id');
        $('#id_sales_order').val(valId);
        var requestUrl = '{{ route('transaction.sales_order.show', ':id') }}';
        var getData = requestUrl.replace(':id', valId);

          $('#attachment').modal('hide');

        $.ajax({
            url: getData,
            type: "GET",
            success: function(media) {

           $("#attachment").find('.modal-body').html(media)

            }
        });
    });
    </script>

    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
        $('.dropify').dropify();
        $('.add-image-btn').click(function() {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='20M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
    </script>
    @include('shared.wrapperDatatable')
@endpush
