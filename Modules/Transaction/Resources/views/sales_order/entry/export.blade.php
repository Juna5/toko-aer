<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Code Number</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Quotation</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Customer</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Salesman</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Project</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Payment Term</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Currency</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Next Approver</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Final Approver</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Status</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->code_number}}</td>
    <td>{{format_d_month_y($row->date)}}</td>
    <td>{{optional($row->quotationMaterial)->code_number}}</td>
    <td>{{optional($row->customer)->company_name}} - {{optional($row->customer)->pic_1}} - {{optional($row->customer)->pic_phone_1}} - {{optional($row->customer)->address}}</td>
    <td>{{optional($row->salesman)->name}}</td>
    <td>{{optional($row->project)->name}}</td>
    <td>{{optional($row->paymentTerm)->name}}</td>
    <td>{{optional($row->currency)->name}}</td>
    <td>{{$row->description}}</td>
    <td>{{optional($row->approveToGo)->name}}</td>
    <td>{{optional($row->latestApprove)->name}}</td>
    <td>{{$row->status}}</td>
  </tr>
  @endforeach
</table>
