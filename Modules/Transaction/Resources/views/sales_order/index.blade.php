@extends('transaction::layouts.master')

@section('transaction::title', 'Sales Order')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Sales Order',
'active' => true,
'url' => route('transaction.sales_order.view')
])
@endsection

@section('transaction::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-clipboard-list"></i>
                </div>
                <div class="card-body">
                    <h4>Sales Order Entry</h4>
                    <p>Create sales order.</p>
                    @can('view sales order')
                    <a href="{{ route('transaction.sales_order.index') }}" class="card-cta">Go to Sales Order Entry <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-envelope-open-text"></i>
                </div>
                <div class="card-body">
                    <h4>Sales Order Approval</h4>
                    <p>All sales order approval data.</p>
                    @can('view sales order approval')
                    <a href="{{ route('transaction.sales_order.approval.index') }}" class="card-cta">Go to Sales Order Approval <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
        
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-folder-open"></i>
                </div>
                <div class="card-body">
                    <h4>Sales Order Report</h4>
                    <p>View Sales Order Report</p>
                    @can('view sales order report')
                    <a href="{{ route('transaction.sales_order.sales-order-report') }}" class="card-cta">Go to Sales Order Report <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>

         <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-folder-open"></i>
                </div>
                <div class="card-body">
                    <h4>Stock Checking</h4>
                    <p>View Stock Checking</p>
                    @can('view sales order')
                    <a href="{{ route('stock.stock-checking.index') }}" class="card-cta">Go to Stock Cheking <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>

         <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-folder-open"></i>
                </div>
                <div class="card-body">
                    <h4>Account Receivable</h4>
                    <p>View Account Receivable</p>
                    @can('view account receivable')
                    <a href="{{ route('finance.account_receivable.index') }}" class="card-cta">Go to Account Receivable <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
