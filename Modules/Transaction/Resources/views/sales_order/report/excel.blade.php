<style type="text/css">
    table{
        table-layout: fixed;
    }


</style>

<table>
    <tr>
        <td>#Penjualan</td>
        <td>Amount (Rp.)</td>
    </tr>
    <tr>
        <td>{{ count($rows) }}</td>
        <td>{{ amount_international_with_comma($totalAmount) }}</td> 
    </tr>
    <tr>
        <td></td>
    </tr>
    @foreach($items as $it)
    <tr>
        <td>#{{ $it->product_name }}</td>
    </tr>
    <tr>
        <td>{{ $it->sum }}</td>
    </tr>
    @endforeach
    
    <tr>
        <td></td>
    </tr>
    <tr>
        <th>Code Number</th>
        <th>Date</th>
        <th>Created By</th>
        <th>Depot</th>
        <th>Currency</th>
        <th>Description</th>
        <th>Total</th>
        <th>Item</th>
        <th>Qty</th>
        <th>Price</th>
        <th>SubTotal</th>
        <th>Created At</th>
        <th>Voucher Terpakai</th>
    </tr>
    @foreach($rows as $row)
    @php
        $details = \Illuminate\Support\Facades\DB::select("select soi.*, p.name as product_name, p.code as product_code, u.name as uom from sales_order_items soi 
            join products p on p.id = soi.product_id
            join uoms u on u.id = soi.uom_id
            where soi.deleted_at is null
            and p.deleted_at is null
            and u.deleted_at isnull
            and soi.sales_order_id = $row->id
       ");
    @endphp
        @foreach($details as $index => $item)        
        <tr>
            <td>
                @if($index == 0)
                    {{$row->code_number}}
                @endif
            </td>
            <td>{{ format_d_month_y($row->date) }}</td>
            <td>{{ $row->employee_name }}</td>
            <td>{{ $row->depot_name }}</td>
            <td>{{ $row->currency_name }}</td>
            <td>{{ $row->description }}</td>
            <td> 
                @if($index == 0)
                    {{ amount_international_with_comma($row->total ,0,",",",") }}
                @endif
            </td>
            <td>{{ $item->product_name }} {{ $item->uom }}</td>
            <td>{{ amount_international_with_comma($item->qty) }}</td>
            <td>{{ amount_international_with_comma($item->price) }}</td>
            <td>{{ amount_international_with_comma($item->sub_total) }}</td>
            <td>{{ format_d_month_y_time($row->created_at) }}</td>
            <td>
                @if($index == 0)
                    {{ amount_international_with_comma($item->qty_voucher) }}
                @endif
            </td>
        </tr>
        @endforeach
    @endforeach
</table>