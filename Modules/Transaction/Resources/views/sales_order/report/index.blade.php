@extends('transaction::layouts.master')

@section('transaction::title', 'Report Penjualan')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Report Penjualan',
'active' => true,
'url' => route('transaction.sales_order.view')
])
@endsection

@section('transaction::breadcrumb-3')
@include('transaction::include.breadcrum', [
'title' => 'Report Penjualan',
'active' => false,
'url' => '',
])
@endsection

@section('transaction::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <form action="{{ route('transaction.sales_order.report.index') }}" method="GET">
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">From Date:</label>
                                <input type="date" class="form-control m-input datepicker" id="from_date" name="from_date" value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">To Date:</label>
                                <input type="date" class="form-control m-input datepicker" id="until_date" name="until_date" value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Depot:</label>
                                <select class="form-control m-input select2" name="depot" id="depot">
                                    @foreach($warehouse as $row)
                                        @if(old('depot'))
                                            <option value="{{ $row->id }}" {{ $row->id == old('depot') ? 'selected' : '' }}>{{$row->name}}</option>
                                        @else
                                            <option value="{{ $row->id }}" {{ $row->id == request()->depot ? 'selected' : '' }}>{{$row->name}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                   
                </form>
                <div class="text-left">
                    <a href="{{ route('billing.view') }}" class="btn btn-warning">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </a>
                </div>
                <br>
                @if($result)
                <div class="text-left">
                     <a href="{{ route('transaction.sales_order.export') }}?from_date={{request()->from_date}}&until_date={{request()->until_date}}&depot={{request()->depot}}" class="btn btn-success">Download Excel</a>
                </div>
                @endif
                <br><br><br>
                <div class="card-body p-10">
                    @if($result)
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="card card-statistic-2">
                                <div class="card-icon shadow-primary bg-primary"><i class="fas fa-file"></i></div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>#Penjualan</h4>
                                    </div>
                                    <div class="card-body">
                                        {{ $result->count() }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="card card-statistic-2">
                                <div class="card-icon shadow-primary bg-success"><i class="fas fa-database"></i></div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Amount (Rp.)</h4>
                                    </div>
                                    <div class="card-body">
                                        {{ amount_international_with_comma($result->sum('total')) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        

                    </div>
                    <div class="row">
                        @foreach($getQty as $i)
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="card card-statistic-2">
                                <div class="card-icon shadow-primary bg-warning"><i class="fas fa-boxes"></i></div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>{{$i->name}}</h4>
                                    </div>
                                    <div class="card-body">
                                        {{$i->total}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="card card-statistic-2">
                                <div class="card-icon shadow-primary bg-warning"><i class="fas fa-boxes"></i></div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Voucher Terpakai</h4>
                                    </div>
                                    <div class="card-body">
                                        {{$totalVoucherTerpakai}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                    @endif
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                        <tr style="background-color:white ; color: grey" align="center">
                            <th>#</th>
                            <th>Code Number</th>
                            <th>Date</th>
                            <th>Created By</th>
                            <th>Depot</th>
<!--                             <th>Customer</th>
                            <th>Salesman</th> -->
                            <th>Total</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>

                        @forelse($salesOrders as $index => $row)
                        @php
                        if (empty($row->status)) {
                        $status = "";
                        } elseif ($row->status == 'waiting') {
                        $status = 'badge badge-success';
                        } elseif ($row->status == 'approved') {
                        $status = 'badge badge-info';
                        } elseif ($row->status == 'rejected') {
                        $status = 'badge badge-danger';
                        }
                        @endphp
                        <tr align="center">
                            <td>{{ $index +1 }}</td>
                            <td><a href="{{ route('transaction.sales_order.report-detail',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->code_number }}</a></td>
                            <td>{{ format_d_month_y($row->date) }}</td>
                            <td>{{ optional($row->employee)->name }}</td>
                            <td>{{ optional($row->depot)->name }}</td>
                            <!-- <td>{{ optional($row->customer)->pic_1 }}</td>
                            <td>{{ optional($row->salesman)->name }}</td> -->
                            <td align="right">{{ amount_international_with_comma($row->total ,0,",",",") }}</td>
                            <td>{{ format_d_month_y_time($row->created_at) }}</td>
                            <td>
                                <a data-href="{{ route('transaction.sales_order.destroy', $row->id) }}" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td colspan="10" class="text-center">Data not found</td>
                        </tr>
                        @endforelse
                    </table>
                    <div class="col-md-6 ">
                        @if(!empty($salesOrders))
                            {{ $salesOrders->appends(request()->query())->links() }}
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

    @endsection

    @push('javascript')
    @include('shared.delete-modal')

    @endpush
