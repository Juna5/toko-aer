@extends('transaction::layouts.master')

@section('transaction::title', 'Sales Order Approval')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Billing',
'active' => true,
'url' => route('billing.view')
])
@endsection

@section('transaction::breadcrumb-3')
@include('transaction::include.breadcrum', [
'title' => 'Sales Order Approval',
'active' => true,
'url' => route('transaction.sales_order.approval.index')
])
@endsection

@section('transaction::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Approval</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">History</a>
                    </li>
                </ul>
            </div>
            <div class="card-body p-0">
                @include('flash::message')
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr class="text-center">
                                        <th class="text-center">
                                            <i class="fas fa-th"></i>
                                        </th>
                                        <th>Code Number</th>
                                        <th>Transfered From</th>
                                        <th>Date</th>
                                        <th>Created By</th>
                                        <!-- <th>Title</th> -->
                                        <th>Customer</th>
                                        <th>Salesman</th>
                                        <!-- <th>Department</th> -->
                                        <th>Payment Term</th>
                                        {{-- <th>Description</th>
 --}}                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($rows as $row)
                                    <tr class="text-center">
                                        <td>
                                            <div class="sort-handler">
                                                <i class="fas fa-th"></i>
                                            </div>
                                        </td>
                                        <td>{{ $row['code_number'] }}</td>
                                        <td>
                                            @if(empty($row->quotationMaterial))
                                                {{ $row['quotation_material']['code_number'] ?? '-' }}
                                            @endif
                                        </td>
                                        <td>{{ format_d_month_y($row['date']) }}</td>
                                        <td>{{ $row['employee']['name'] }}</td>
                                        <!-- <td>{{ $row['title'] }}</td> -->
                                        <td>{{ $row['customer']['company_name'].' - '.$row['customer']['pic_1'].' - '. $row['customer']['pic_phone_1'] .' - '. $row['customer']['address'] }}</td>
                                        <td>{{ $row['salesman']['name'] }}</td>
                                        <!-- <td>{{ $row['department']['name'] }}</td> -->
                                        <td>{{ $row['payment_term']['code'] .' - ' . $row['payment_term']['name'] }}</td>
                                        {{-- <td>{{ $row['description'] }}</td> --}}
                                        <td>
                                            <a href="{{ route('transaction.sales_order.approval.show', $row['id']) }}" class="btn btn-info">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card-body p-10">
                            <div class="table-responsive">
                                {!! $dataTable->table() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('javascript')
@include('shared.wrapperDatatable')
@endpush
