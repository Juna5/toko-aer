@extends('transaction::layouts.master')

@section('transaction::title', 'Sales Return Edit')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Sales Return',
    'active' => true,
    'url' => route('transaction.sales-return.index')
    ])
@endsection

@section('transaction::breadcrumb-4')
    @include('transaction::include.breadcrum', [
    'title' => 'Sales Return Edit',
    'active' => true,
    'url' => route('transaction.sales-return.edit', $return->id)
    ])
@endsection

@push('stylesheet')
    <style>
        input[type=number] {
            text-align:right;
        }
    </style>
@endpush

@section('transaction::content')
    @include('flash::message')
    @include('include.error-list')
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <sales-return-edit :return="{{ $return }}"></sales-return-edit>
                </div>
            </div>
        </div>
    </div>
@endsection