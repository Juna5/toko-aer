@extends('transaction::layouts.master')

@section('transaction::title', 'Sales Return')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Sales Return',
    'active' => true,
    'url' => route('transaction.sales-return.index')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endpush

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover table-stripped']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    @include('shared.wrapperDatatable')
@endpush