@extends('transaction::layouts.master')

@section('transaction::title', 'Sales Return Create')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Sales Return',
    'active' => true,
    'url' => route('transaction.sales-return.index')
    ])
@endsection

@section('transaction::breadcrumb-4')
    @include('transaction::include.breadcrum', [
    'title' => 'Sales Return Create',
    'active' => true,
    'url' => route('transaction.sales-return.create')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endpush

@section('transaction::content')
    @include('flash::message')
    @include('include.error-list')
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <sales-return-create></sales-return-create>
                </div>
            </div>
        </div>
    </div>
@endsection