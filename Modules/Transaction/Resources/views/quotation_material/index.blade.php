@extends('transaction::layouts.master')

@section('transaction::title', 'Quotation')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Quotation',
'active' => true,
'url' => route('transaction.quotation_material.view')
])
@endsection

@section('transaction::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-clipboard-list"></i>
                </div>
                <div class="card-body">
                    <h4>Quotation Entry</h4>
                    <p>Create quotation for employee.</p>
                    @can('view quotation material')
                    <a href="{{ route('transaction.quotation_material.index') }}" class="card-cta">Go to Quotation Entry <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-envelope-open-text"></i>
                </div>
                <div class="card-body">
                    <h4>Quotation Approval</h4>
                    <p>All quotation approval data.</p>
                    @can('view quotation material approval')
                    <a href="{{ route('transaction.quotation_material.approval.index') }}" class="card-cta">Go to Quotation Approval <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
         <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-chart-bar"></i>
                </div>
                <div class="card-body">
                    <h4>Quotation Report</h4>
                    <p>View Quotation Report</p>
                    @can('view quotation material report')
                    <a href="{{ route('transaction.quotation_material.report.index') }}" class="card-cta">Go to Quotation Report <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
