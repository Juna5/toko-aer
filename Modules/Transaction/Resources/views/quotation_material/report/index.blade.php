@extends('transaction::layouts.master')

@section('transaction::title', 'Quotation Report')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Quotation Report',
    'active' => false,
    ])
@endsection

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <form action="{{ route('transaction.quotation_material.report.index')}}" method="GET">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>From Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="from_date"
                                           name="from_date"
                                           value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>To Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="until_date"
                                           name="until_date"
                                           value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status:</label>
                                    <select class="form-control m-input" name="status" id="status">
                                        <option value="all" {{ old('status', request('status')) == 'all' ? 'selected' : '' }}>
                                            All
                                        </option>
                                        <option value="waiting" {{ old('status', request('status')) == 'waiting' ? 'selected' : '' }}>
                                            Waiting
                                        </option>
                                        <option value="approved" {{ old('status', request('status')) == 'approved' ? 'selected' : '' }}>
                                            Approved
                                        </option>
                                        <option value="rejected" {{ old('status', request('status')) == 'rejected' ? 'selected' : '' }}>
                                            Rejected
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                        <a href="{{ route('billing.view') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br><br><br>

                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                            <tr>
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Approval Status</th>
                                <th>Total</th>
                            </tr>

                            @forelse($quotationMaterials as $index => $row)
                                @php
                                    if (empty($row->status)) {
                                    $status = "";
                                    } elseif ($row->status == 'waiting') {
                                    $status = 'badge badge-success';
                                    } elseif ($row->status == 'approved') {
                                    $status = 'badge badge-info';
                                    } elseif ($row->status == 'rejected') {
                                    $status = 'badge badge-danger';
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('transaction.quotation_material.report.show',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->code_number}}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->date) }}</td>
                                    <td>{{ $row->employee->name }}</td>
                                    <td><span class="{{$status}}">{{ ucfirst($row->status) }}</span></td>
                                    <td align="right">{{ number_format($row->total ,0,",",",") }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">Data not found</td>
                                </tr>

                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(request('from_date') && request('end_date'))
                                {{ $salesOrder->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
