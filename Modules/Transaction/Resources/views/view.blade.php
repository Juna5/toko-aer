@extends('transaction::layouts.master')

@section('transaction::title', 'Penjualan')

@section('transaction::breadcrumb-2')
@include('transaction::include.breadcrum', [
'title' => 'Penjualan',
'active' => true,
'url' => '/billing'
])
@endsection

@section('transaction::content')
<div class="section-body">
    @include('flash::message')

    <!-- <div class="row">
        <div class="col-12 col-md-4 col-lg-4">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Card Header</h4>

                </div>
                <div class="card-body" style="background-color: #fcd581;">
                    <p>Write something here</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Card Header</h4>

                </div>
                <div class="card-body">
                    <p>Write something here</p>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4 col-lg-4">
            <div class="card card-primary">
                <div class="card-header">
                    <h4>Card Header</h4>

                </div>
                <div class="card-body">
                    <p>Write something here</p>
                </div>
            </div>
        </div>
    </div> -->
    <div class="row">
        <!-- <div class="col-md-3">
            <div class="card border-radius card-icons">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/billing/sales_quotation_2.png') }}" class="img-fluid images-icon">
                            <div class=" font-weight-bold text-icons title">Quotation</div>
                            <div class="text-muted text-small text-icons">
                                @can('view quotation material')
                                <a href="{{ route('transaction.quotation_material.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view quotation material approval')
                                <a href="{{ route('transaction.quotation_material.approval.index') }}" class="card-cta">Approval</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view quotation material report')
                                    <a href="{{ route('transaction.quotation_material.report.index') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-md-3">
            <div class="card border-radius card-icons">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/billing/sales_order_2.png') }}" class="images-icon">
                            <div class=" font-weight-bold text-icons title">PENJUALAN</div>
                            <div class="text-muted text-small text-icons">
                                <!-- @can('view sales order')
                                <a href="{{ route('transaction.sales_order.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan -->
                                @can('view sales order approval')
                                <a href="{{ route('transaction.sales_order.approval.index') }}" class="card-cta">Approval</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view sales order report')
                                <a href="{{ route('transaction.sales_order.report.index') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/billing/stock_checking.png') }}" class="">
                            <div class=" font-weight-bold">Stock Checking</div>
                            <div class="text-muted text-small">
                                <a href="{{ route('stock.stock-checking.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>

                                <a href="{{ route('stock.stock-checking-report') }}" class="card-cta">Reports</a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/billing/sales_delivery.png') }}" class="">
                            <div class=" font-weight-bold">Delivery Order</div>
                            <div class="text-muted text-small">
                                @can('view delivery order material')
                                <a href="{{ route('transaction.delivery_order_material.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view delivery order material approval')
                                <a href="{{ route('transaction.delivery_order_material.approval.index') }}" class="card-cta">Approval</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view delivery order material')
                                    <a href="{{ route('transaction.delivery_order_material.delivery-order-report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/billing/sales_invoice.png') }}" class="">
                            <div class=" font-weight-bold">Invoice</div>
                            <div class="text-muted text-small">
                                @can('view invoice')
                                    <a href="{{ route('finance.invoice.index') }}" class="card-cta">Entry</a>
                                    <div class="bullet"></div>
                                @endcan
                                @can('view finance report')
                                    <a href="{{ route('finance.report.invoice.index') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/billing/return.png') }}" class="">
                            <div class=" font-weight-bold">Sales Return</div>
                            <div class="text-muted text-small">
                                @can('view sales return')
                                    <a href="{{ route('transaction.sales-return.index') }}" class="card-cta">Entry</a>
                                    <div class="bullet"></div>
                                    <a href="{{ route('transaction.sales-return-report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->

    </div>
</div>
@endsection
