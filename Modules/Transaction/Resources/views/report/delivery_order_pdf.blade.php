<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style type="text/css">
        @font-face {
            font-family: Arial, Helvetica, sans-serif;
        }

        * {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <style media="screen">
        .margin {
            /* border: 1px solid black; */
            /* padding: 4px; */
            font-size: 12px;
            /* padding-bottom: 100px; */
        }

        /* Create four equal columns that floats next to each other */
        .column {
            float: left;
            width: 30%;
            height: 11.1%;
        ;
            /* Should be removed. Only for demonstration */
        }

        .column2 {
            float: left;
            width: 70%;
            height: auto;
            /* Should be removed. Only for demonstration */
        }

        .column3 {
            float: left;
            width: 55%;
            height: auto;
        ;
            /* Should be removed. Only for demonstration */
        }

        .column4 {
            float: left;
            width: 60%;
            height: auto;
            /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }


        .page_break {
            page-break-after: always;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 15%;
            width: 100%;
            /* color: white; */
            font-size:10px
        }
    </style>
    <title></title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">

<script type="text/php">
    if ( isset($pdf) ) {
            $pdf->page_text(550, 770, "Page: {PAGE_NUM} of {PAGE_COUNT}", 'Arial', 9, array(0,0,0));
        }
    </script>

<div class="margin">
    <div class="row">
        <div class="column" style="border: 1px solid #881719">
            <img src="images/logo.png" width="80%" style="margin-top: 30px; margin-left: 20px;">
        </div>
        <div class="column2" style="background-color:#881719; border: 1px solid #881719">
            <div class="" >
                <p style="font-size:11pt;font-weight:bold; margin-left:20px">
                  <span style="color:#fff!important;">SENG LIY ENGINEERING (M) SDN BHD <label for="" style="font-size: 9px;">201301041820(1071644-P)</label> <br>
                    <label for="" style="font-size:9pt;">
                        NO. 28, JALAN TIRAM 14, TAMAN PERINDUSTRIAN TIRAM, <br>
                        JALAN SUNGAI TIRAM, 81800 ULU TIRAM, JOHOR <br>
                        Tel: 07-861 2730, Fax: 07-863 2730(BILLING) / 07-862 0611(ACCOUNT)</label><br>
                    <span style="font-size:9px;">
                        Email : sengliyengineering@gmail.com
                  </span>
                </p>
            </div>
        </div>
    </div>
    <br>
    <!-- <center><b><span style="color:Black!important; font-size:16pt;font-weight:bolder;margin-top:-19px;font-family:Arial">TAX INVOICE</span></b></center> -->
    <br>
    <table width="100%">
        <tr>
            <td width="58%" style="vertical-align:top">
                <table>
                    <tr>
                        <td>
                            <b style="font-size:15px">Bill To :</b><br>
                            {{ optional($row->customer)->company_name }} <br>
                            {{ optional($row->customer)->address}} <br>
                            {{ optional($row->customer)->phone}} <br>
                            {{ optional($row->customer)->email}}
                        </td>

                    </tr>

                </table>
            </td>
            <td style="vertical-align:top">
                <table >
                    <tr>
                        <td>
                            <b style="font-size:15px">DELIVERY ORDER :</b><br>
                            {{ $row->code_number}} <br>
                            {{ $row->paymentTerm->name}} <br>
                            {{ format_d_month_y($row->date)}} <br>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="5" style=" border-collapse: collapse;">
        <tr style=" background-color: #881719; color:#fff">
            <th>Item</th>
            <th>UoM</th>
            <th>Qty</th>
            <th>Description</th>
        </tr>
        @foreach($items as $item)
        <tr style="background-color: {{ $item->status == 0 ? '#f98686': '#fff' }}">
            <td>{{ optional($item->salesOrderMaterialItem->product)->name }}</td>
            <td>{{ optional($item->salesOrderMaterialItem->uom)->name }}</td>
            <td align="right">{{ $item->qty }}</td>

            <td>{{ $item->description }}</td>
        </tr>
        @endforeach
       
    </table>
    <br>
    <i> Description : <br>
        <b>{{ $row->description }}</b>
    </i>
    <br><br>
</div>
<div class="footer">
    <table width="100%"> 
        <tr>
            <td style="vertical-align:top">
                <p>Notes:</p>
                <ol style="margin-left:-22px">
                    <li>All cheques should be crossed and made payable to <br> SENG LIY ENGINEERING (M) SDN BHD</li>
                    <li>Goods sold are neither returnable nor refundable. Otherwise <br> a cancellation fee of 20% on purchase price will be imposed.</li>
                </ol>
                <br>
                <label style="margin-left:20px">_________________________________________ <br>
                <b style="font-size:12.5px; margin-left:20px">SENG LIY ENGINEERING (M) SDN BHD</b></label>
            </td>
            <td align="right">
                 <br><br><br><br><br><br><br><br><br><br>
                _________________________________________ <br>
                <b style="font-size:12.5px; margin-right: 25px">Receipient's Chop & Signature</b><br>
                <label style="margin-right: 61%" for="">Name:</label> <br>
                <label style="margin-right: 37%" for="">Date: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Time:</label>

            </td>
        </tr>
    </table>
</div>
</body>

</html>
