<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <style type="text/css">
        @font-face {
            font-family: Arial, Helvetica, sans-serif;
        }

        * {
            font-family: Arial, Helvetica, sans-serif;
        }
    </style>
    <style media="screen">
        .margin {
            /* border: 1px solid black; */
            /* padding: 4px; */
            font-size: 12px;
            /* padding-bottom: 100px; */
        }

        /* Create four equal columns that floats next to each other */
        .column {
            float: left;
            width: 30%;
            height: 11.1%;
        ;
            /* Should be removed. Only for demonstration */
        }

        .column2 {
            float: left;
            width: 70%;
            height: auto;
            /* Should be removed. Only for demonstration */
        }

        .column3 {
            float: left;
            width: 55%;
            height: auto;
        ;
            /* Should be removed. Only for demonstration */
        }

        .column4 {
            float: left;
            width: 60%;
            height: auto;
            /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }


        .page_break {
            page-break-after: always;
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 10%;
            width: 100%;
            /* color: white; */
            font-size:10px
        }
    </style>
    <title></title>
</head>

<body style="font-family: Arial, Helvetica, sans-serif;">

<script type="text/php">
    if ( isset($pdf) ) {
            $pdf->page_text(550, 770, "Page: {PAGE_NUM} of {PAGE_COUNT}", 'Arial', 9, array(0,0,0));
        }
    </script>

<div class="margin">
    <div class="row">
        <div class="column" style="border: 1px solid #881719">
            <img src="images/logo.png" width="80%" style="margin-top: 30px; margin-left: 20px;">
        </div>
        <div class="column2" style="background-color:#881719; border: 1px solid #881719">
            <div class="" >
                <p style="font-size:11pt;font-weight:bold; margin-left:20px">
                  <span style="color:#fff!important;">SENG LIY ENGINEERING (M) SDN BHD <label for="" style="font-size: 9px;">201301041820(1071644-P)</label> <br>
                    <label for="" style="font-size:9pt;">
                        NO. 28, JALAN TIRAM 14, TAMAN PERINDUSTRIAN TIRAM, <br>
                        JALAN SUNGAI TIRAM, 81800 ULU TIRAM, JOHOR <br>
                        Tel: 07-861 2730, Fax: 07-863 2730(BILLING) / 07-862 0611(ACCOUNT)</label><br>
                    <span style="font-size:9px;">
                        Email : sengliyengineering@gmail.com
                  </span>
                </p>
            </div>
        </div>
    </div>
    <br>
    <!-- <center><b><span style="color:Black!important; font-size:16pt;font-weight:bolder;margin-top:-19px;font-family:Arial">TAX INVOICE</span></b></center> -->
    <br>
    <table width="100%">
        <tr>
            <td width="58%" style="vertical-align:top">
                <table>
                    <tr>
                        <td>
                            <b style="font-size:15px">Bill To :</b><br>
                            {{ optional($row->customer)->company_name }} <br>
                            {{ optional($row->customer)->address}} <br>
                            {{ optional($row->customer)->phone}} <br>
                            {{ optional($row->customer)->email}}
                        </td>

                    </tr>

                </table>
            </td>
            <td style="vertical-align:top">
                <table >
                    <tr>
                        <td>
                            <b style="font-size:15px">SALES ORDER :</b><br>
                            {{ $row->code_number}} <br>
                            {{ $row->paymentTerm->name}} <br>
                            {{ format_d_month_y($row->date)}} <br>
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table width="100%" cellpadding="5" style=" border-collapse: collapse;">
        <tr style=" background-color: #881719; color:#fff">
            <th>Item</th>
            <th>UoM</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Dsc Rate (%)</th>
            <th>Dsc Amount</th>
            <th>Description</th>
            <th>Sub Total</th>
        </tr>
        @foreach($items as $item)
        <tr style="background-color: {{ $item->status == 0 ? '#f98686': '#fff' }}">
            <td>{{ optional($item->product)->name }}</td>
            <td>{{ optional($item->uom)->name }}</td>
            <td align="right">{{ $item->qty }}</td>
            <td  align="right">{{ number_format($item->price) }}</td>
            <td>{{ $item->discount_rate }}</td>
            <td  align="right">{{ number_format($item->discount_amount) }}</td>
            <td>{{ $item->description }}</td>
            <td  align="right">{{ number_format($item->sub_total) }}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="7" align="right">Subtotal Exclude Discount All Items</td>
            <td  align="right">{{ number_format($row->get_total) }}</td>
        </tr>
        <tr>
            <td colspan="7" align="right">Discount All Items </td>
            <td  align="right">{{ number_format($row->discount_all_item) }}</td>
        </tr>
        <tr>
            <td colspan="7" align="right">Sub Total</td>
            <td  align="right">{{ number_format($row->sub_total) }}</td>
        </tr>
        <tr>
            <td colspan="6" align="right">GST</td>
            <td align="right"> {{ optional($row->tax)->rate}}%</td>
            <td  align="right">{{ number_format(($row->tax->rate / 100) * $row->sub_total) }}</td>
        </tr>
        <tr>
            <td colspan="6">{{ ucwords(number_to_word($row->total)) }}</td>
            <td  align="right">Total</td>
            <td  align="right"  style="color:#510A32; font-weight: bold;">{{ number_format($row->total) }}</td>
        </tr>
    </table>
    <br>
    <i> Description : <br>
        <b>{{ $row->description }}</b>
        <br>
        **FOR PREPARE PAYMENT,TQ**
    </i>
    <br><br>
</div>
<div class="footer">
    <table width="100%"> 
        <tr>
            <td>
                <p>Notes:</p>
                <ol style="margin-left:-22px">
                    <li>All cheques should be crossed and made payable to <br> SENG LIY ENGINEERING (M) SDN BHD <br> Bank A/C No: Public Bank Bhd 3186795305</li>
                    <li>Goods sold are neither returnable nor refundable. Otherwise <br> a cancellation fee of 20% on purchase price will be imposed.</li>
                </ol>
            </td>
            <td align="right">
                 <br><br>
                <label for="">THIS IS A COMPUTER GENERATED DOCUMENT</label> <br>
                <label for="" style="margin-right: 45px">NO SIGNATURE IS REQUIRED</label><br><br>
                _________________________________________ <br>
                <b style="font-size:12.5px">SENG LIY ENGINEERING (M) SDN BHD</b>
            </td>
        </tr>
    </table>
</div>
</body>

</html>
