@extends('transaction::layouts.master')

@section('transaction::title', 'Delivery Order Report')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Delivery Order Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <form action="{{ route('transaction.delivery_order_material.delivery-order-report') }}" method="GET">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="from_date"
                                           name="from_date" value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">To Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="until_date"
                                           name="until_date" value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Status:</label>
                                    <select class="form-control m-input" name="status" id="status">
                                        <option value="all" {{ old('status', request('status')) == 'all' ? 'selected' : '' }}>
                                            All
                                        </option>
                                        <option value="waiting" {{ old('status', request('status')) == 'waiting' ? 'selected' : '' }}>
                                            Waiting
                                        </option>
                                        <option value="approved" {{ old('status', request('status')) == 'approved' ? 'selected' : '' }}>
                                            Approved
                                        </option>
                                        <option value="rejected" {{ old('status', request('status')) == 'rejected' ? 'selected' : '' }}>
                                            Rejected
                                        </option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                        <a href="{{ route('billing.view') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br><br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Transfered From</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Approval Status</th>
                                <th>Customer</th>
                                <th>Salesman</th>
                                <th></th>
                            </tr>
                            @forelse($deliveryOrder as $index => $row)
                                @php
                                    if (empty($row->status)) {
                                        $status = "";
                                    } elseif ($row->status == 'waiting') {
                                        $status = 'badge badge-success';
                                    } elseif ($row->status == 'approved') {
                                        $status = 'badge badge-info';
                                    } elseif ($row->status == 'rejected') {
                                        $status = 'badge badge-danger';
                                    }
                                @endphp
                                <tr align="center" >
                                    <td>{{ $index +1 }}</td>
                                    <td><a href="{{ route('transaction.delivery_order_material.report-detail',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->code_number }}</a></td>
                                    <td><a href="{{ route('transaction.sales_order.report-detail',[ $row->salesOrder->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->salesOrder->code_number }}</a></td>
                                    <td>{{ format_d_month_y($row->date) }}</td>
                                    <td>{{ optional($row->employee)->name }}</td>
                                    <td><span class="{{$status}}">{{ ucfirst($row->status) }}</span></td>
                                    <td>{{ optional($row->customer)->pic_1 }}</td>
                                    <td>{{ optional($row->salesman)->name }}</td>
                                    <td><a target="_blank" href="{{ route('transaction.delivery_order_material.pdf', [$row->id, 'report' => true]) }}" class="btn btn-danger"><i class="fa fa-file-pdf"></i></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="10" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(!empty($deliveryOrder))
                                {{ $deliveryOrder->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


