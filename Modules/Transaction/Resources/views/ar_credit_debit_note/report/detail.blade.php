@extends('transaction::layouts.master')

@section('transaction::title', 'Credit Note Detail')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Cash Book',
    'active' => true,
    'url' => route('cash-book.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Credit Note Report',
    'active' => false,
    'url' => route('transaction.credit_note.report', $creditNote->id),
    ])
@endsection

@section('transaction::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary ">{{ format_d_month_y($creditNote->date) }}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-md-right">
                                                <div class="invoice-number text-primary ">{{ optional($creditNote->receivable)->code_number }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Bank Account</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($creditNote->chartOfAccountBank)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Chart of Account</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($creditNote->chartOfAccount)->code }} - {{ optional($creditNote->chartOfAccount)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Customer</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($creditNote->customer)->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($creditNote->date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Amount</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ amount_international_with_comma($creditNote->amount) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Description</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $creditNote->description }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive" style="margin-top: 10%">
                                <table class="table table-bordered table-hover">
                                    <tr>
                                        <th class="text-center"> Invoice</th>
                                        <th class="text-center"> Date</th>
                                        <th class="text-center"> Total Amount</th>
                                        <th class="text-center"> Amount Due</th>
                                        <th class="text-center"> Payment</th>
                                    </tr>
                                    
                                        @foreach($receivableDetail->details as $detail)
                                        <tr>
                                             <td align="center">{{ optional($detail->invoice)->invoice_number }}</td>
                                             <td align="center">{{ format_d_month_y($detail->invoice->invoice_date) }}</td>
                                             <td align="right">{{ amount_international_with_comma($detail->invoice->total) }}</td>
                                             
                                             <td align="right">{{amount_international_with_comma($detail->invoice->outstanding)}}</td>
                                             <td align="right">{{amount_international_with_comma($detail->amount)}}</td>
                                        </tr>
                                        @endforeach
                                   
                                </table>
                            </div>
                             <div class="row clearfix" style="margin-top:20px">
                                <div class="col-md-12">
                                    <div class="float-right">
                                        <table class="table table-bordered table-hover" id="tab_logic_total">
                                            <tbody id="grand_total">
                                            <tr>
                                                <th class="text-center">Grand Total</th>
                                                <td class="text-center">
                                                          {{ amount_international_with_comma($grand_total) }}   
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="text-md-right">
                                <a href="{{ route('transaction.credit_note.report') }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
