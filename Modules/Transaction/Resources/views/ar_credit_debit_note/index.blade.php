@extends('transaction::layouts.master')

@section('transaction::title', 'A/R Credit Debit Note')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Cash Book',
    'active' => true,
    'url' => route('cash-book.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'A/R Credit Debit Note',
    'active' => true,
    'url' => route('transaction.ar_credit_debit_note.index')
    ])
@endsection

@section('transaction::content')
    @include('flash::message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
