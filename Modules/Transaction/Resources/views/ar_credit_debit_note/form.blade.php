@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group">
            <label>Bank Account <span style="color:red">*</span></label>
            <select name="bank" id="leave" class="form-control r-0 light s-12 select2" required>
                <option value="">Please Select</option>
                @foreach($banks as $bank)
                    @if( old('bank') )
                        <option value="{{ $bank->id }}" {{ old('bank') == $bank->id ? 'selected' : ''  }}>{{ $bank->name }}</option>
                    @else
                        <option value="{{ $bank->id }}" {{ $bank->id == $cost->charge_of_account_bank_id ? 'selected' : ''  }}>{{ $bank->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Cost Name <span style="color:red">*</span></label>
            <input type="text" name="cost_name" value="{{ old('cost_name') ?? $cost->cost_name }}" placeholder="cth: buy gasoline, parking expense, salary expense, etc" class="form-control r-0 light s-12" required>
        </div>
        <div class="form-group">
            <label>Vendor</label>
            <input type="text" name="vendor" value="{{ old('vendor') ?? $cost->vendor }}" placeholder="cth: Petronas, Wallmart, etc" class="form-control r-0 light s-12">
        </div>
        <div class="form-group">
            <label>Chart Of Account <span style="color:red">*</span></label>
            <select name="charge_of_account_id" id="balance" class="form-control r-0 light s-12 select2" required>
                <option value="">Please Select</option>
                @foreach($charts as $chart)
                    @if( old('charge_of_account_id') )
                        <option value="{{ $chart->id }}" {{ old('charge_of_account_id') == $chart->id ? 'selected' : ''  }}>{{ $chart->getFullName() }}</option>
                    @else
                        <option value="{{ $chart->id }}" {{ $chart->id == $cost->charge_of_account_id ? 'selected' : ''  }}>{{ $chart->getFullName() }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Date <span style="color:red">*</span></label>
            <input type="date" class="form-control datepicker" name="date" value="{{ old('date') ?? $cost->date }}" required>
        </div>
        <div class="form-group">
            <label>Category</label>
            <input type="text" name="category" value="{{ old('category') ?? $cost->category }}" placeholder="cth: Parking, gasoline, salary, etc" class="form-control r-0 light s-12">
        </div>
        <div class="form-group">
            <label class="">Amount: <span style="color:red">*</span></label>
            <input type="text" class="form-control separator currency" value="{{ old('amount') ?? $cost->amount }}" placeholder="1.000.000" required>
            <input type="hidden" name="amount" class="separator-hidden" value="{{ old('amount') ?? $cost->amount }}">
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control" placeholder="cth: Parking at West Coast Mall, gasoline 5 gal, etc">{{ old('description') ?? $cost->description }}</textarea>
        </div>
    </div>
</div>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
