@extends('transaction::layouts.master')

@section('transaction::title', 'Payments Received Last 30 Days Report')

@section('transaction::breadcrumb-2')
    @include('transaction::include.breadcrum', [
    'title' => 'Billing',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('transaction::breadcrumb-3')
    @include('transaction::include.breadcrum', [
    'title' => 'Payments Received Last 30 Days Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('transaction::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Customer</th>
                                <th>Amount</th>
                                <th></th>
                            </tr>

                            @forelse($receivedPayment as $index => $row)
                                <tr align="center" >
                                    <td>{{ $index +1 }}</td>
                                    <td><a href="{{ route('finance.payment.receivable-report-detail',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->code_number }}</a></td>
                                    <td>{{ format_d_month_y($row->date) }}</td>
                                    <td>{{ $row->employee_id ? optional($row->employee)->name : '-' }}</td>
                                    <td>{{ optional($row->customer)->company_name }}</td>
                                    <td align="right">{{ amount_international_with_comma($row->amount) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="row clearfix" style="margin-top:20px">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <table class="table table-bordered table-hover" id="tab_logic_total">
                                        <tbody id="grand_total">
                                        <tr>
                                            <th class="text-center">Grand Total</th>
                                            <td class="text-center">
                                                {{ amount_international_with_comma($total) }}   
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-left" style="margin-bottom: 2%">
                            <a href="{{ route('billing.view') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection