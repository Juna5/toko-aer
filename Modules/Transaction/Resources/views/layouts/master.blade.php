@extends('layouts.app')

@section('content')
<section class="section">
	<div class="section-header">
		<h1>@yield('transaction::title', config('transaction.name'))</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></div>
            @yield('transaction::breadcrumb-2')
			@yield('transaction::breadcrumb-3')
            @yield('transaction::breadcrumb-4')
        </div>
	</div>

	<div class="section-body">
		@yield('transaction::content')
	</div>
</section>
@endsection
