<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <h4>Jump To</h4>
        </div>
        <div class="card-body">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item"><a href="{{ route('hr.master.job-level.index') }}" class="nav-link {{ set_active('hr.master.job-level.*') }}">Job Level</a></li>
                <li class="nav-item"><a href="{{ route('hr.master.religion.index') }}" class="nav-link {{ set_active('hr.master.religion.*') }}">Religion</a></li>
                <li class="nav-item"><a href="{{ route('hr.master.job-position.index') }}" class="nav-link {{ set_active('hr.master.job-position.*') }}">Job Position</a></li>
                <li class="nav-item"><a href="{{ route('hr.master.division.index') }}" class="nav-link {{ set_active('hr.master.division.*') }}">Division</a></li>
                <li class="nav-item"><a href="{{ route('hr.master.department.index') }}" class="nav-link {{ set_active('hr.master.department.*') }}">Department</a></li>
                <li class="nav-item"><a href="{{ route('hr.master.cost-center.index') }}" class="nav-link {{ set_active('hr.master.cost-centre.*') }}">Cost Centre</a></li>
                <li class="nav-item"><a href="{{ route('hr.master.leave-type.index') }}" class="nav-link {{ set_active('hr.master.leave-type.*') }}">Leave Type</a></li>
            </ul>
        </div>
    </div>
</div>