<?php

namespace Modules\Transaction\Entities;

use Modules\Procurement\Entities\Uom;
use Illuminate\Database\Eloquent\Model;
use Modules\Procurement\Entities\Product;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationMaterialItem extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function product()
	{
		return $this->belongsTo(Product::class);
	}

    public function uom()
	{
		return $this->belongsTo(Uom::class);
	}
}
