<?php

namespace Modules\Transaction\Entities;

use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Finance\Entities\Invoice;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Customer;

class SalesReturn extends Model
{
    use SoftDeletes, CascadeSoftDeletes;

    protected $guarded = [];

    protected $cascadeDeletes = ['details'];

    protected $dates = ['deleted_at'];

    public function details()
    {
        return $this->hasMany(SalesReturnDetail::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
