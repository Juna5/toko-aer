<?php

namespace Modules\Transaction\Entities;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Department;
use Illuminate\Database\Eloquent\Model;
use Modules\Procurement\Entities\Tax;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Procurement\Entities\Project;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class QuotationMaterial extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $guarded = [];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function salesman()
    {
        return $this->belongsTo(Employee::class);
    }

    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }

    public function paymentTerm()
    {
        return $this->belongsTo(PaymentTerm::class);
    }

    public function items()
	{
		return $this->hasMany(QuotationMaterialItem::class, 'quotation_material_id');
	}

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function latestApprove()
    {
        return $this->belongsTo(Employee::class, 'latest_approver');
    }

    public function approveToGo()
    {
        return $this->belongsTo(Employee::class, 'approver_to_go');
    }

    public function rejectedBy()
    {
        return $this->belongsTo(Employee::class, 'rejected_by');
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
