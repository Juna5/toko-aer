<?php

namespace Modules\Transaction\Entities;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Department;
use Modules\Procurement\Entities\Tax;
use Illuminate\Database\Eloquent\Model;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\Project;
use Modules\Procurement\Entities\Voucher;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Procurement\Entities\WarehouseMaster;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class SalesOrder extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $guarded = [];

    public function scopeApproved($query)
    {
        return $query->where('status', 'approved');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function quotationMaterial()
    {
        return $this->belongsTo(QuotationMaterial::class);
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function salesman()
    {
        return $this->belongsTo(Employee::class);
    }

    public function tax()
    {
        return $this->belongsTo(Tax::class);
    }

    public function paymentTerm()
    {
        return $this->belongsTo(PaymentTerm::class);
    }

    public function items()
	{
		return $this->hasMany(SalesOrderItem::class, 'sales_order_id');
	}

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function latestApprove()
    {
        return $this->belongsTo(Employee::class, 'latest_approver');
    }

    public function approveToGo()
    {
        return $this->belongsTo(Employee::class, 'approver_to_go');
    }

    public function rejectedBy()
    {
        return $this->belongsTo(Employee::class, 'rejected_by');
    }
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function depot()
    {
        return $this->belongsTo(WarehouseMaster::class, 'depot_id');
    }

    public function voucher()
    {
        return $this->hasMany(Voucher::class, 'sales_order_id');
    }

}
