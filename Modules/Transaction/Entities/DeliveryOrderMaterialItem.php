<?php

namespace Modules\Transaction\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Procurement\Entities\Uom;
use Modules\Procurement\Entities\Product;

class DeliveryOrderMaterialItem extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function salesOrderMaterialItem()
	{
		return $this->belongsTo(SalesOrderItem::class, 'sales_order_item_id');
	}

    public function uom()
	{
		return $this->belongsTo(Uom::class);
	}

    public function warehouse()
	{
		return $this->belongsTo(WarehouseMaster::class);
	}

	public function deliveryOrderMaterial()
	{
		return $this->belongsTo(DeliveryOrderMaterial::class);
	}

}
