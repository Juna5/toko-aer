<?php

namespace Modules\Transaction\Entities;

use Modules\HR\Entities\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryOrderMaterialApproval extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function employee()
	{
		return $this->belongsTo(Employee::class);
	}

	public function deliveryOrderMaterial()
	{
	return $this->belongsTo(DeliveryOrderMaterial::class);
	}
}
