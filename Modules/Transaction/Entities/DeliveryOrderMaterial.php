<?php

namespace Modules\Transaction\Entities;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Department;
use Modules\Procurement\Entities\Tax;
use Illuminate\Database\Eloquent\Model;
use Modules\Procurement\Entities\Customer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\PaymentTerm;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class DeliveryOrderMaterial extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $guarded = [];

    public function scopeApproved($query)
    {
        return $query->where('status', 'approved');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function salesOrder()
    {
        return $this->belongsTo(SalesOrder::class);
    }

    public function salesman()
    {
        return $this->belongsTo(Employee::class);
    }

    public function paymentTerm()
    {
        return $this->belongsTo(PaymentTerm::class);
    }

    public function items()
	{
		return $this->hasMany(DeliveryOrderMaterialItem::class, 'delivery_order_material_id');
	}

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function latestApprove()
    {
        return $this->belongsTo(Employee::class, 'latest_approver');
    }

    public function approveToGo()
    {
        return $this->belongsTo(Employee::class, 'approver_to_go');
    }

    public function rejectedBy()
    {
        return $this->belongsTo(Employee::class, 'rejected_by');
    }
}
