<?php

namespace Modules\Transaction\Entities;

use Modules\HR\Entities\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class QuotationMaterialApproval extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function employee()
	{
		return $this->belongsTo(Employee::class);
	}

	public function quotationMaterial()
	{
		return $this->belongsTo(QuotationMaterial::class);
	}
}
