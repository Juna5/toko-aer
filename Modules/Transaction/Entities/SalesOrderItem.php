<?php

namespace Modules\Transaction\Entities;

use Modules\Procurement\Entities\Uom;
use Illuminate\Database\Eloquent\Model;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Transaction\Entities\SalesOrder;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesOrderItem extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function product()
	{
		return $this->belongsTo(Product::class, 'product_id');
	}

    public function uom()
	{
		return $this->belongsTo(Uom::class);
	}

	public function salesOrder()
	{
		return $this->belongsTo(SalesOrder::class);
	}

	public function warehouse()
	{
		return $this->belongsTo(WarehouseMaster::class, 'warehouse_id');
	}
}
