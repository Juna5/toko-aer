<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['impersonate', 'auth'])->group(function () {
   Route::get('/billing', 'TransactionController@view')->name('billing.view');
    Route::name('transaction.')->prefix('transaction')->group(function () {
        Route::get('open_invoices_report', 'TransactionController@openInvoices')->name('open_invoices_report');
        Route::get('overdue_invoices_report', 'TransactionController@overdueInvoices')->name('overdue_invoices_report');
        Route::get('payments_received_report', 'TransactionController@receivedPayment')->name('payments_received_report');

        Route::name('quotation_material.')->prefix('quotation_material')->namespace('QuotationMaterial')->group(function () {
            Route::get('view', 'ViewController')->name('view');
            Route::put('/{id}', 'QuotationMaterialController@update')->name('update');
            Route::resource('/', 'QuotationMaterialController');
            Route::get('/{id}/detail', 'QuotationMaterialController@detail')->name('detail');
            Route::get('/export', 'QuotationMaterialController@export')->name('export');
            Route::get('/{id}/edit', 'QuotationMaterialController@edit')->name('edit');
            Route::get('/{id}/show', 'QuotationMaterialController@show')->name('show');
            Route::get('/{id}/pdf', 'QuotationMaterialController@pdf')->name('pdf');
            Route::delete('/{id}', 'QuotationMaterialController@destroy')->name('destroy');
            Route::post('attachment','QuotationMaterialController@saveAttachment')->name('attachment');

            Route::get('/tax', 'QuotationMaterialController@tax')->name('tax');
            Route::get('/product_price/{id}', 'QuotationMaterialController@productPrice')->name('product_price');
            Route::get('/product_detail/{id}/{productId}', 'QuotationMaterialController@productDetail')->name('product_detail');
            Route::get('/salesman', 'QuotationMaterialController@salesman')->name('salesman');
            Route::get('/paymentTerm', 'QuotationMaterialController@paymentTerm')->name('paymentTerm');
            Route::get('/customer', 'QuotationMaterialController@customer')->name('customer');
             Route::get('/customer-detail/{id}', 'QuotationMaterialController@customerDetail')->name('customer');
            Route::get('/salesmenId', 'QuotationMaterialController@salesmenId')->name('salesmenId');

            Route::resource('approval', 'QuotationMaterialApprovalController');
            Route::put('approval/update_item/{id}', 'QuotationMaterialApprovalController@updateItem')->name('approval.update_item');

            Route::resource('report', 'QuotationMaterialReportController');
        });

        Route::name('sales_order.')->prefix('sales_order')->namespace('SalesOrder')->group(function () {
            Route::get('view', 'ViewController')->name('view');
            Route::post('/store', 'SalesOrderController@store')->name('store');
            Route::resource('/', 'SalesOrderController');
            Route::get('/export', 'SalesOrderController@export')->name('export');
            Route::get('/{id}/detail', 'SalesOrderController@detail')->name('detail');
            Route::get('/{id}/print', 'SalesOrderController@print')->name('print');
            Route::get('/{id}/edit', 'SalesOrderController@edit')->name('edit');
            Route::get('/{id}/show', 'SalesOrderController@show')->name('show');
            Route::put('/{id}', 'SalesOrderController@update')->name('update');
            Route::post('attachment','SalesOrderController@saveAttachment')->name('attachment');
             Route::get('/customer-detail-sales/{id}', 'SalesOrderController@customerDetail')->name('customer-detail-sales');

            Route::delete('/{id}', 'SalesOrderController@destroy')->name('destroy');

            Route::get('/currency', 'SalesOrderController@currency')->name('currency');
            Route::get('/quotation_material', 'SalesOrderController@quotationMaterial')->name('quotation_material');
            Route::get('/quotation_material_first', 'SalesOrderController@quotationMaterialFirst')->name('quotation_material_first');
            Route::get('/fetch_quotation_material_item', 'SalesOrderController@fetchQuotationMaterialItem')->name('fetch_quotation_material_item');

            Route::resource('approval', 'SalesOrderApprovalController');
            Route::put('approval/update_item/{id}', 'SalesOrderApprovalController@updateItem')->name('approval.update_item');

            Route::resource('report', 'SalesOrderReportController');
            Route::get('sales-order-report', 'SalesOrderController@report')->name('sales-order-report');
            Route::get('/{id}/report-detail', 'SalesOrderController@detailReport')->name('report-detail');
            Route::get('/pdf/{id}', 'SalesOrderReportController@pdf')->name('pdf');

            Route::get('/download-excel/', 'SalesOrderReportController@export')->name('export');

        });
        Route::name('delivery_order_material.')->prefix('delivery_order_material')->namespace('DeliveryOrderMaterial')->group(function () {
            Route::get('view', 'ViewController')->name('view');
            Route::resource('/', 'DeliveryOrderMaterialController');
            Route::get('/outstanding', 'DeliveryOrderMaterialController@outstanding')->name('outstanding');
            Route::get('/export', 'DeliveryOrderMaterialController@export')->name('export');
            Route::get('/{id}/detail', 'DeliveryOrderMaterialController@detail')->name('detail');
            Route::get('/{id}/pdf', 'DeliveryOrderMaterialController@pdf')->name('pdf');
            Route::get('/{id}/edit', 'DeliveryOrderMaterialController@edit')->name('edit');
            Route::get('/{id}/show', 'DeliveryOrderMaterialController@show')->name('show');
            Route::put('/{id}', 'DeliveryOrderMaterialController@update')->name('update');
            Route::post('attachment','DeliveryOrderMaterialController@saveAttachment')->name('attachment');

            Route::delete('/{id}', 'DeliveryOrderMaterialController@destroy')->name('destroy');

            Route::get('/sales_order_material', 'DeliveryOrderMaterialController@salesOrderMaterial')->name('sales_order_material');
            Route::get('/warehouse', 'DeliveryOrderMaterialController@warehouse')->name('warehouse');
            Route::get('/sales_order_material_item', 'DeliveryOrderMaterialController@salesOrderMaterialItem')->name('sales_order_material_item');

            Route::resource('approval', 'DeliveryOrderMaterialApprovalController');
            Route::put('approval/update_item/{id}', 'DeliveryOrderMaterialApprovalController@updateItem')->name('approval.update_item');
            Route::get('delivery-order-report', 'DeliveryOrderMaterialController@report')->name('delivery-order-report');
            Route::get('/{id}/report-detail', 'DeliveryOrderMaterialController@detailReport')->name('report-detail');

        });

        Route::name('credit_note.')->prefix('credit_note')->group(function() {
            Route::resource('/', 'CreditNoteController');
            Route::get('/{id}/edit', 'CreditNoteController@edit')->name('edit');
            Route::put('/{id}', 'CreditNoteController@update')->name('update');
            Route::post('transaction/credit-note/receivable', 'CreditNoteController@getReceivable')->name('getReceivable');
            Route::delete('/{id}', 'CreditNoteController@destroy')->name('destroy');
            Route::get('report', 'CreditNoteController@report')->name('report');
            Route::get('detail/{id}', 'CreditNoteController@reportDetail')->name('report-detail');
        });

        Route::name('ar_credit_debit_note.')->prefix('ar_credit_debit_note')->group(function() {
            Route::resource('/', 'ARCreditDebitNoteController');
            Route::get('/{id}/edit', 'ARCreditDebitNoteController@edit')->name('edit');
            Route::put('/{id}', 'ARCreditDebitNoteController@update')->name('update');
            Route::post('transaction/ar-credit-debit-note/receivable', 'ARCreditDebitNoteController@getReceivable')->name('getReceivable');
            Route::delete('/{id}', 'ARCreditDebitNoteController@destroy')->name('destroy');
            Route::get('report', 'ARCreditDebitNoteController@report')->name('report');
            Route::get('detail/{id}', 'ARCreditDebitNoteController@reportDetail')->name('report-detail');
        });

        Route::resource('sales-return', 'SalesReturnController');
        Route::get('sales-return-report', 'SalesReturnController@report')->name('sales-return-report');
        Route::get('sales-return-detail/{id}', 'SalesReturnController@reportDetail')->name('sales-return-report-detail');
    });
});
