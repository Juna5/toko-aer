<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/transaction', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->name('api.')->group(function () {
    Route::get('transaction/quotation_material/{id}', 'QuotationMaterialController@edit')->name('transaction.quotation_material');
    Route::post('transaction/quotation_material/removeItem/{id}', 'QuotationMaterialController@removeItem')->name('transaction.quotation_material');

    Route::get('transaction/sales_order/{id}', 'SalesOrderController@edit')->name('transaction.sales_order');
    Route::post('transaction/sales_order/removeItem/{id}', 'SalesOrderController@removeItem')->name('transaction.sales_order');

    Route::get('transaction/delivery_order_material/{id}', 'DeliveryOrderMaterialController@edit')->name('transaction.delivery_order_material');
    Route::post('transaction/delivery_order_material/removeItem/{id}', 'DeliveryOrderMaterialController@removeItem')->name('transaction.delivery_order_material');
    Route::get('transaction/delivery_order_material/getItem/{id}', 'DeliveryOrderMaterialController@getItem')->name('transaction.delivery_order_material');

    Route::get('receivable-detail/{id}', 'CreditNoteController@getReceivableDetail');

    Route::name('transaction.')->prefix('transaction')->group(function ()
    {
        Route::get('/project', 'TransactionController@project')->name('project');
    });

    Route::name('transaction.customer.')->prefix('transaction/customer')->group(function () {

        Route::post('/store-customer', 'CustomerController@store')->name('store-customer');
    });

    Route::name('transaction.product.')->prefix('transaction/product')->group(function () {
        Route::get('/product_category', 'ProductController@productCategory')->name('product_category');
        Route::get('/product_sub_category/{id}', 'ProductController@productSubCategory')->name('product_sub_category');
        Route::get('/location', 'ProductController@location')->name('location');
        Route::post('/store', 'ProductController@store')->name('store');
        Route::get('/price_category','ProductController@priceCategory')->name('price_category');
        Route::get('/product-detail/{id}', 'ProductController@productDetail')->name('product-detail');
        Route::get('/product-price','ProductController@productPrice')->name('product-price');
        Route::get('/billing-history/{id}', 'ProductController@billingHistory')->name('billing-history');
        Route::get('/procurement-history/{id}', 'ProductController@procurementHistory')->name('procurement-history');
    });

    Route::name('credit_note.')->prefix('credit_note')->group(function() {
        Route::post('/credit-note/receivable', 'CreditNoteController@getReceivable')->name('getReceivable');
        Route::post('/credit-note/customer', 'CreditNoteController@getCustomer')->name('getCustomer');
    });
});
