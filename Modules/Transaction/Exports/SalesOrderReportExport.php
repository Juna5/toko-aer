<?php

namespace Modules\Transaction\Exports;

use Modules\Transaction\Entities\SalesOrder;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnWidths;


class SalesOrderReportExport implements FromView, ShouldAutoSize
{
	protected $rows;
	protected $totalVoucherTerjual;
	protected $totalVoucherTerpakai;
	protected $airGalon;
	protected $airIsiUlang;
	protected $totalAmount;
	protected $items;

	function __construct($rows, $totalVoucherTerjual, $totalVoucherTerpakai,  $airGalon, $airIsiUlang, $totalAmount, $items) {
        $this->rows = $rows;
        $this->totalVoucherTerjual = $totalVoucherTerjual;
        $this->totalVoucherTerpakai = $totalVoucherTerpakai;
        $this->airIsiUlang = $airIsiUlang;
        $this->airGalon = $airGalon;
        $this->totalAmount = $totalAmount;
        $this->items = $items;
	}

	

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('transaction::sales_order.report.excel', [
            'rows' => $this->rows,
            'totalVoucherTerjual' => $this->totalVoucherTerjual,
            'totalVoucherTerpakai' => $this->totalVoucherTerpakai,
            'airGalon' => $this->airGalon,
            'airIsiUlang' => $this->airIsiUlang,
            'totalAmount' => $this->totalAmount,
            'items' => $this->items,
        ]);
    }
}