<?php

namespace Modules\Transaction\Exports;

use Modules\Transaction\Entities\QuotationMaterial;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class QuotationMaterialExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('transaction::quotation_material.entry.export', [
            'rows' => QuotationMaterial::all()
        ]);
    }
}