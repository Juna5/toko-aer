<?php

namespace Modules\Transaction\Exports;

use Modules\Transaction\Entities\SalesOrder;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SalesOrderExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('transaction::sales_order.entry.export', [
            'rows' => SalesOrder::all()
        ]);
    }
}