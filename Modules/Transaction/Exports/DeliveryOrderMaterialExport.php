<?php

namespace Modules\Transaction\Exports;

use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DeliveryOrderMaterialExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('transaction::delivery_order_material.entry.export', [
            'rows' => DeliveryOrderMaterial::all()
        ]);
    }
}