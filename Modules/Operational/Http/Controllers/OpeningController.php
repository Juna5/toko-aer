<?php

namespace Modules\Operational\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Operational\DataTables\OpeningDatatable;
use Modules\HR\Entities\Employee;
use Carbon\Carbon;
use Modules\CashBook\Entities\Cash;
use Modules\CashBook\Entities\CashDetail;
use Modules\Stock\Entities\Stock;
use Modules\Operational\Entities\Opening;
use Modules\Operational\Entities\Closing;
use Modules\Operational\Http\Requests\OpeningRequest;
use Illuminate\Support\Facades\DB;

class OpeningController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(OpeningDatatable $datatable)
    {
        return $datatable->render('operational::opening.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $employee = Employee::all();
        $employe = Employee::where('user_id', auth()->user()->id)->first();
        $meteranIn =  null;
        $meteranOut =  null;
        $meteranOut2 =  null;
        $buktiUang =  null;
        if ($employe) {
            $date = date('Y-m-d', strtotime('-1 days', strtotime(now())));
            $closing = Closing::whereDepotId($employe->depot_id)->whereClosingDate($date)->first();
            $stock = Stock::where('warehouse_id', $employe->depot_id)->where('description', 'Stock Air')->first();
            $cash = Cash::where('depot_id', $employe->depot_id)->first();
            if ($stock) {
                $stock = $stock;
            } else {
                $stock = new Stock();
                $stock->description = 'Stock Air';
                $stock->warehouse_id = $employe->depot_id;
                $stock->save();
            }
            if ($cash) {
                $cash = $cash;
            } else {
                $cash = new Cash();
                $cash->depot_id = $employe->depot_id;
                $cash->save();
            }
        } else {

            noty()->danger('Oops', 'Please set your depot location');
            return redirect()->route('operational.opening.index');
        }

        return view('operational::opening.create', compact('employee', 'stock', 'cash', 'closing','meteranIn','meteranOut','meteranOut2','buktiUang'));
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(OpeningRequest $request)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $date = now();
            $date->toDateTimeString();

            if ($employee) {
                $datec = date('Y-m-d', strtotime('-1 days', strtotime(now())));
                $closing = Closing::whereDepotId($employee->depot_id)->whereClosingDate($datec)->first();
                $openings = Opening::where('depot_id', $employee->depot_id)->where('opening_date', $date)->first();
                $stock = Stock::where('warehouse_id', $employee->depot_id)->where('description', 'Stock Air')->first();
                $cash = Cash::where('depot_id', $employee->depot_id)->first();

                if ($stock) {
                    $stock = $stock;
                } else {
                    $stock = new Stock();
                    $stock->description = 'Stock Air';
                    $stock->warehouse_id = $employee->depot_id;
                    $stock->save();
                }
                if ($cash) {
                    $cash = $cash;
                } else {
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
                if ($openings) {
                    noty()->danger('Oops', 'Data sudah ada , silahkan ke menu edit untuk merubah');
                    return redirect()->route('operational.opening.index');
                } else {
                    if (str_replace(',', '', $request->cash_amount) == '') {
                        $cashamount = 0;
                    } else {
                        $cashamount = str_replace(',', '', $request->cash_amount);
                    }
                    if ($stock->in == null) {
                        $stock->in = 0;
                    }
                    if ($stock->out == null) {
                        $stock->out = 0;
                    }
                    if ($cash->amount == null) {
                        $cash->amount = 0;
                    }
                    $opening = new Opening();
                    $opening->depot_id = $employee->depot_id;
                    $opening->employee_id = $employee->id;
                    $opening->opening_date = $date;
                    $opening->meteran_in_awal = str_replace(',', '', $closing ? $closing->meteran_in_akhir : 0);
                    $opening->meteran_in_status = $request->meteran_in_status ?? 0;
                    $opening->meteran_in = str_replace(',', '', $request->meteran_in);
                    $opening->meteran_out_awal = str_replace(',', '', $closing ? $closing->meteran_out_akhir : 0);
                    $opening->meteran_out_status = $request->meteran_out_status ?? 0;
                    $opening->meteran_out = str_replace(',', '', $request->meteran_out);
                    $opening->meteran_out_awal2 = str_replace(',', '', $closing ? $closing->meteran_out_akhir2 : 0);
                    $opening->meteran_out_status2 = $request->meteran_out_status2 ?? 0;
                    $opening->meteran_out2 = str_replace(',', '', $request->meteran_out2);
                    $opening->cash_awal = str_replace(',', '', $cash->amount);
                    $opening->cash_status = $request->cash_status ?? 0;
                    $opening->cash_amount = $cashamount;
                    $opening->disaksikan_oleh = $request->disaksikan_oleh;
                    $opening->menyapu_halaman_parkir = $request->menyapu_halaman_parkir ?? 0;
                    $opening->membersihkan_toilet = $request->membersihkan_toilet ?? 0;
                    $opening->menyiram_tanaman = $request->menyiram_tanaman ?? 0;
                    $opening->seragam_rapi_bersih_server = $request->seragam_rapi_bersih_server ?? 0;
                    $opening->seragam_rapi_bersih_kasir = $request->seragam_rapi_bersih_kasir ?? 0;
                    $opening->memakai_masker_face_shield_server = $request->memakai_masker_face_shield_server ?? 0;
                    $opening->memakai_masker_face_shield_kasir = $request->memakai_masker_face_shield_kasir ?? 0;
                    $opening->kebersihan_diri_server = $request->kebersihan_diri_server ?? 0;
                    $opening->kebersihan_diri_kasir = $request->kebersihan_diri_kasir ?? 0;
                    $opening->sering_cuci_tangan_server = $request->sering_cuci_tangan_server ?? 0;
                    $opening->sering_cuci_tangan_kasir = $request->sering_cuci_tangan_kasir ?? 0;
                    $opening->sopan_terhadap_customer_server = $request->sopan_terhadap_customer_server ?? 0;
                    $opening->sopan_terhadap_customer_kasir = $request->sopan_terhadap_customer_kasir ?? 0;
                    $opening->pelayanan_maksimal_server = $request->pelayanan_maksimal_server ?? 0;
                    $opening->pelayanan_maksimal_kasir = $request->pelayanan_maksimal_kasir ?? 0;
                    $opening->bicara_dan_bercanda_seperlunya_server = $request->bicara_dan_bercanda_seperlunya_server ?? 0;
                    $opening->bicara_dan_bercanda_seperlunya_kasir = $request->bicara_dan_bercanda_seperlunya_kasir ?? 0;
                    $opening->makan_minum_di_ruang_yang_tersedia_server = $request->makan_minum_di_ruang_yang_tersedia_server ?? 0;
                    $opening->makan_minum_di_ruang_yang_tersedia_kasir = $request->makan_minum_di_ruang_yang_tersedia_kasir ?? 0;
                    $opening->tidak_menggunakan_hp_saat_kerja_server = $request->tidak_menggunakan_hp_saat_kerja_server ?? 0;
                    $opening->tidak_menggunakan_hp_saat_kerja_kasir = $request->tidak_menggunakan_hp_saat_kerja_kasir ?? 0;
                    $opening->save();
                }
            } else {
                noty()->danger('Oops', 'Please set your depot location');
                return redirect()->route('operational.opening.create');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            if ($request->hasFile('meteran_in_photo')) {
                $opening->addMedia($request->meteran_in_photo)->toMediaCollection('meteran_in_photo');
            }
            if ($request->hasFile('meteran_out_photo')) {
                $opening->addMedia($request->meteran_out_photo)->toMediaCollection('meteran_out_photo');
            }
            if ($request->hasFile('meteran_out_photo2')) {
                $opening->addMedia($request->meteran_out_photo2)->toMediaCollection('meteran_out_photo2');
            }
            if ($request->hasFile('bukti_uang_photo')) {
                $opening->addMedia($request->bukti_uang_photo)->toMediaCollection('bukti_uang_photo');
            }
        } catch (\Exception $e) {
            DB::rollback();
            flash('Ops, try again' . $e->getMessage())->error();
            return back();
        }

        try {
            $stocks = Stock::where('warehouse_id', $employee->depot_id)->where('description', 'Stock Air')->first();
            $cashes = Cash::where('depot_id', $employee->depot_id)->first();
            // if ($stocks) {
            //     $stock = $stocks;
            //     if ($request->meteran_in) {
            //         $stock->in = str_replace(',', '', $request->meteran_in);
            //         $stock->save();
            //     }

            //     if ($request->meteran_out) {
            //         $stock->out = str_replace(',', '', $request->meteran_out);
            //         $stock->save();
            //     }

            //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
            //     $stock->save();
            // } else {
            //     $stock = new Stock();
            //     $stock->warehouse_id = $employee->depot_id;
            //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
            //     $stock->in = str_replace(',', '', $request->meteran_in);
            //     $stock->out = str_replace(',', '', $request->meteran_out);
            //     $stock->description = 'Stock Air';
            //     $stock->status  = true;
            //     $stock->save();
            // }

            if ($cashes) {
                $cash = $cashes;
                if ($request->cash_amount) {
                    $cash->amount = str_replace(',', '', $request->cash_amount);
                    $cash->save();
                }
            } else {
                $cash = new Cash();
                $cash->depot_id = str_replace(',', '', $employee->depot_id);
                $cash->amount = $request->cash_amount;
                $cash->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('operational.opening.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('operational::opening.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $employee = Employee::all();
        $employe = Employee::where('user_id', auth()->user()->id)->first();
        $opening = Opening::findOrFail($id);
        $meteranIn = $opening ? $opening->getFirstMedia('meteran_in_photo') : null;
        $meteranOut = $opening ? $opening->getFirstMedia('meteran_out_photo') : null;
        $meteranOut2 = $opening ? $opening->getFirstMedia('meteran_out_photo2') : null;
        $buktiUang = $opening ? $opening->getFirstMedia('bukti_uang_photo') : null;

        if ($employe) {
            $date = date('Y-m-d', strtotime('-1 days', strtotime($opening->opening_date)));
            $closing = Closing::whereDepotId($employe->depot_id)->whereClosingDate($date)->first();
            $stock = Stock::where('warehouse_id', $opening->depot_id)->where('description', 'Stock Air')->first();
            $cash = Cash::where('depot_id', $opening->depot_id)->first();
        } else {
            noty()->danger('Oops', 'Please set your depot location');
            return redirect()->route('operational.opening.index');
        }


        return view('operational::opening.edit', compact('employee', 'stock','cash','opening', 'closing', 'meteranIn', 'meteranOut', 'meteranOut2', 'buktiUang'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(OpeningRequest $request, $id)
    {
        DB::beginTransaction();

        try {

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $date = Carbon::now();
            $date->toDateTimeString();
            $opening = Opening::findOrFail($id);
            $dateC = date('Y-m-d', strtotime('-1 days', strtotime($opening->opening_date)));
            $closing = Closing::whereDepotId($employee->depot_id)->whereClosingDate($dateC)->first();
            $stock = Stock::where('warehouse_id', $employee->depot_id)->where('description', 'Stock Air')->first();
            $cash = Cash::where('depot_id', $employee->depot_id)->first();
            if ($stock) {
                $stock = $stock;
            } else {
                $stock = new Stock();
                $stock->description = 'Stock Air';
                $stock->warehouse_id = $employee->depot_id;
                $stock->save();
            }
            if ($cash) {
                $cash = $cash;
            } else {
                $cash = new Cash();
                $cash->depot_id = $employee->depot_id;
                $cash->save();
            }
            if ($employee) {
                if (str_replace(',', '', $request->cash_amount) == '') {
                    $cashamount = 0;
                } else {
                    $cashamount = str_replace(',', '', $request->cash_amount);
                }
                if ($stock->in == null) {
                    $stock->in = 0;
                }
                if ($stock->out == null) {
                    $stock->out = 0;
                }
                if ($cash->amount == null) {
                    $cash->amount = 0;
                }
                $opening = $opening;
                $opening->depot_id = $employee->depot_id;
                $opening->employee_id = $employee->id;
                $opening->meteran_in_awal = str_replace(',', '', $closing ? $closing->meteran_in_akhir : 0);
                $opening->meteran_in_status = $request->meteran_in_status ?? 0;
                $opening->meteran_in = str_replace(',', '', $request->meteran_in);
                $opening->meteran_out_awal = str_replace(',', '', $closing ? $closing->meteran_out_akhir : 0);
                $opening->meteran_out_status = $request->meteran_out_status ?? 0;
                $opening->meteran_out = str_replace(',', '', $request->meteran_out);
                $opening->meteran_out_awal2 = str_replace(',', '', $closing ? $closing->meteran_out_akhir2 : 0);
                $opening->meteran_out_status2 = $request->meteran_out_status2 ?? 0;
                $opening->meteran_out2 = str_replace(',', '', $request->meteran_out2);
                $opening->cash_awal = str_replace(',', '', $cash->amount);
                $opening->cash_status = $request->cash_status ?? 0;
                $opening->cash_amount = $cashamount;
                $opening->disaksikan_oleh = $request->disaksikan_oleh;
                $opening->menyapu_halaman_parkir = $request->menyapu_halaman_parkir ?? 0;
                $opening->membersihkan_toilet = $request->membersihkan_toilet ?? 0;
                $opening->menyiram_tanaman = $request->menyiram_tanaman ?? 0;
                $opening->seragam_rapi_bersih_server = $request->seragam_rapi_bersih_server ?? 0;
                $opening->seragam_rapi_bersih_kasir = $request->seragam_rapi_bersih_kasir ?? 0;
                $opening->memakai_masker_face_shield_server = $request->memakai_masker_face_shield_server ?? 0;
                $opening->memakai_masker_face_shield_kasir = $request->memakai_masker_face_shield_kasir ?? 0;
                $opening->kebersihan_diri_server = $request->kebersihan_diri_server ?? 0;
                $opening->kebersihan_diri_kasir = $request->kebersihan_diri_kasir ?? 0;
                $opening->sering_cuci_tangan_server = $request->sering_cuci_tangan_server ?? 0;
                $opening->sering_cuci_tangan_kasir = $request->sering_cuci_tangan_kasir ?? 0;
                $opening->sopan_terhadap_customer_server = $request->sopan_terhadap_customer_server ?? 0;
                $opening->sopan_terhadap_customer_kasir = $request->sopan_terhadap_customer_kasir ?? 0;
                $opening->pelayanan_maksimal_server = $request->pelayanan_maksimal_server ?? 0;
                $opening->pelayanan_maksimal_kasir = $request->pelayanan_maksimal_kasir ?? 0;
                $opening->bicara_dan_bercanda_seperlunya_server = $request->bicara_dan_bercanda_seperlunya_server ?? 0;
                $opening->bicara_dan_bercanda_seperlunya_kasir = $request->bicara_dan_bercanda_seperlunya_kasir ?? 0;
                $opening->makan_minum_di_ruang_yang_tersedia_server = $request->makan_minum_di_ruang_yang_tersedia_server ?? 0;
                $opening->makan_minum_di_ruang_yang_tersedia_kasir = $request->makan_minum_di_ruang_yang_tersedia_kasir ?? 0;
                $opening->tidak_menggunakan_hp_saat_kerja_server = $request->tidak_menggunakan_hp_saat_kerja_server ?? 0;
                $opening->tidak_menggunakan_hp_saat_kerja_kasir = $request->tidak_menggunakan_hp_saat_kerja_kasir ?? 0;
                $opening->save();

                if ($request->hasFile('meteran_in_photo')) {
                    if ($opening->getMedia('meteran_in_photo')->first()) {
                        $opening->getMedia('meteran_in_photo')->first()->delete();
                    }
                    $opening->addMedia($request->meteran_in_photo)->toMediaCollection('meteran_in_photo');
                }

                if ($request->hasFile('meteran_out_photo')) {
                    if ($opening->getMedia('meteran_out_photo')->first()) {
                        $opening->getMedia('meteran_out_photo')->first()->delete();
                    }
                    $opening->addMedia($request->meteran_out_photo)->toMediaCollection('meteran_out_photo');
                }
                if ($request->hasFile('meteran_out_photo2')) {
                    if ($opening->getMedia('meteran_out_photo2')->first()) {
                        $opening->getMedia('meteran_out_photo2')->first()->delete();
                    }
                    $opening->addMedia($request->meteran_out_photo2)->toMediaCollection('meteran_out_photo2');
                }
                if ($request->hasFile('bukti_uang_photo')) {
                    if ($opening->getMedia('bukti_uang_photo')->first()) {
                        $opening->getMedia('bukti_uang_photo')->first()->delete();
                    }
                    $opening->addMedia($request->bukti_uang_photo)->toMediaCollection('bukti_uang_photo');
                }
            } else {
                noty()->danger('Oops', 'Please set your depot location');
                return redirect()->route('operational.opening.index');
            }
        } catch (\Exception $e) {
            DB::rollback();
            flash('Ops, try again' . $e->getMessage())->error();
            return back();
        }

        try {
            $stocks = Stock::where('warehouse_id', $employee->depot_id)->where('description', 'Stock Air')->first();
            $cashes = Cash::where('depot_id', $employee->depot_id)->first();
            // if ($stocks) {
            //     $stock = $stocks;
            //     $stock->in = str_replace(',', '', $stock->in) - str_replace(',', '', $opening->meteran_in);
            //     $stock->out = str_replace(',', '', $stock->out) + str_replace(',', '', $opening->meteran_out);
            //     $stock->qty = str_replace(',', '', $stock->in) - str_replace(',', '', $stock->out);
            //     $stock->save();

            //     if ($request->meteran_in) {
            //         $stock->in = str_replace(',', '', $request->meteran_in);
            //         $stock->save();
            //     }

            //     if ($request->meteran_out) {
            //         $stock->out = str_replace(',', '', $request->meteran_out);
            //         $stock->save();
            //     }

            //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
            //     $stock->save();
            // } else {
            //     $stock = new Stock();
            //     $stock->warehouse_id = $employee->depot_id;
            //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
            //     $stock->in = str_replace(',', '', $request->meteran_in);
            //     $stock->out = str_replace(',', '', $request->meteran_out);
            //     $stock->description = 'Stock Air';
            //     $stock->status  = true;
            //     $stock->save();
            // }

            if ($cashes) {
                $cash = $cashes;
                if ($request->cash_amount) {
                    $cash->amount = str_replace(',', '', $request->cash_amount);
                    $cash->save();
                }
            } else {
                $cash = new Cash();
                $cash->depot_id = $employee->depot_id;
                $cash->amount = str_replace(',', '', $request->cash_amount);
                $cash->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('operational.opening.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $opening = Opening::findOrFail($id);

            // $employee = Employee::where('user_id', auth()->user()->id)->first();
            // if($employee){
            $stocks = Stock::where('warehouse_id', $opening->depot_id)->where('description', 'Stock Air')->first();
            $cashes = Cash::where('depot_id', $opening->depot_id)->first();
            // if ($stocks) {
            //     $stock = $stocks;
            //     $stock->in = $stock->in - $opening->meteran_in;
            //     $stock->out = $stock->out + $opening->meteram_out;
            //     $stock->qty = $stock->in - $stock->out;
            //     $stock->save();
            // }

            if ($cashes) {
                $cash = $cashes;
                $cash->amount = $cash->amount - $opening->cash_amount;
                $cash->save();
            }
            // }else{
            //     noty()->danger('Oops','Please set your depot location');
            //     return redirect()->route('operational.opening.index');
            // }

            $opening->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function view()
    {
        return view('operational::opening.view');
    }

    public function report()
    {
        $opening = [];
        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $page           = 10;

        if ($fromDate && $untilDate) {
            $opening = Opening::withoutGlobalScopes()
                ->with('employee', 'depot')
                ->whereNull('deleted_at')
                ->whereBetween('opening_date', [$fromDate, $untilDate])
                ->paginate($page);
        }
        return view('operational::opening.report', compact('opening'));
    }

    public function reportDetail($id)
    {
        $row = Opening::findOrFail($id);
        $from_date = request('from_date');
        $until_date = request('until_date');

        return view('operational::opening.report-detail', compact('row', 'from_date', 'until_date'));
    }
}
