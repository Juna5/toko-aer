<?php

namespace Modules\Operational\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Operational\DataTables\SerahTerimaDatatable;
use Modules\HR\Entities\Employee;
use Carbon\Carbon;
use Modules\CashBook\Entities\Cash;
use Modules\CashBook\Entities\CashDetail;
use Modules\Stock\Entities\Stock;
use Modules\Operational\Entities\SerahTerimaShift;
use Modules\Operational\Entities\Opening;
use Modules\Operational\Http\Requests\SerahTerimaRequest;
use Illuminate\Support\Facades\DB;

class SerahTerimaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SerahTerimaDatatable $datatable)
    {
        return $datatable->render('operational::serah-terima.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $employee = Employee::all();
        $employe = Employee::where('user_id', auth()->user()->id)->first();
        $yesterday = Carbon::yesterday()->format('yy-m-d');

        if($employe){
            $opening = Opening::whereDepotId($employe->depot_id)->whereOpeningDate(now())->first();
            if ($opening == null) {
                noty()->danger('Oops','Please set your Opening');
                return redirect()->route('operational.serah-terima.index');
            }
            $serahTerimaYes = SerahTerimaShift::where('depot_id', $employe->depot_id)->where('serah_terima_date', $yesterday)->first();

            $stock = Stock::where('warehouse_id', $employe->depot_id)->where('description','Stock Air')->first();
            $cash = Cash::where('depot_id', $employe->depot_id)->first();
            if($stock){
                $stock = $stock;
            }else{
                $stock = new Stock();
                $stock->description = 'Stock Air';
                $stock->warehouse_id = $employe->depot_id;
                $stock->save();
            }
            if($cash){
                $cash = $cash;
            }else{
                $cash = new Cash();
                $cash->depot_id = $employe->depot_id;
                $cash->save();
            }


        }else{

            noty()->danger('Oops','Please set your depot location');
            return redirect()->route('operational.serah-terima.index');
        }

        return view('operational::serah-terima.create', compact('employee', 'stock','cash','serahTerimaYes', 'opening'));
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(SerahTerimaRequest $request)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $date = Carbon::now();
            $date->toDateTimeString();

            if($employee){
                $serahTerima = SerahTerimaShift::where('depot_id', $employee->depot_id)->where('serah_terima_date', $date->format('yy-m-d'))->first();
                $stock = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cash = Cash::where('depot_id', $employee->depot_id)->first();
                if($stock){
                    $stock = $stock;
                }else{
                    $stock = new Stock();
                    $stock->description = 'Stock Air';
                    $stock->warehouse_id = $employee->depot_id;
                    $stock->save();
                }
                if($cash){
                    $cash = $cash;
                }else{
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
                if($serahTerima){
                    noty()->danger('Oops','Data sudah ada , silahkan ke menu edit untuk merubah');
                    return redirect()->route('operational.serah-terima.index');
                }else{
                    $serahTerima = new SerahTerimaShift();
                    $serahTerima->depot_id = $employee->depot_id;
                    $serahTerima->employee_id = $employee->id;
                    $serahTerima->serah_terima_date = $date->format('yy-m-d');
                    $serahTerima->meteran_in_awal = str_replace(',', '', $request->meteran_in_awal) ?? 0;
                    $serahTerima->meteran_in_akhir = str_replace(',', '', $request->meteran_in_akhir) ?? 0;
                    $serahTerima->meteran_in = str_replace(',', '', $request->meteran_in) ?? 0;
                    $serahTerima->meteran_out_awal = str_replace(',', '', $request->meteran_out_awal) ?? 0;
                    $serahTerima->meteran_out_akhir = str_replace(',', '', $request->meteran_out_akhir) ?? 0;
                    $serahTerima->meteran_out = str_replace(',', '', $request->meteran_out) ?? 0;
                    $serahTerima->meteran_out_awal2 = str_replace(',', '', $request->meteran_out_awal2) ?? 0;
                    $serahTerima->meteran_out_akhir2 = str_replace(',', '', $request->meteran_out_akhir2) ?? 0;
                    $serahTerima->meteran_out2 = str_replace(',', '', $request->meteran_out2) ?? 0;
                    $serahTerima->cash_amount = str_replace(',', '', $request->cash_amount) ?? 0;
                    $serahTerima->persediaan_akhir = str_replace(',', '', $request->persediaan_akhir);
                    $serahTerima->drop_amplop_setoran_bank = $request->drop_amplop_setoran_bank ?? 0;
                    $serahTerima->disiapkan_oleh = $request->disiapkan_oleh ;
                    $serahTerima->disetujui_oleh = $request->disetujui_oleh;
                    $serahTerima->save();
                }
            }else{
                noty()->danger('Oops','Please set your depot location');
                return redirect()->route('operational.serah-terima.create');
            }

        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        try {
                $stocks = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cashes = Cash::where('depot_id', $employee->depot_id)->first();
                // if($stocks){
                //     $stock = $stocks;
                //     if($request->meteran_in){
                //         $stock->in = str_replace(',', '', $request->meteran_in);
                //         $stock->save();
                //     }

                //     if($request->meteran_out){
                //         $stock->out = str_replace(',', '', $request->meteran_out);
                //         $stock->save();
                //     }

                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->save();


                // }else{
                //     $stock = new Stock();
                //     $stock->warehouse_id = $employee->depot_id;
                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->in = str_replace(',', '', $request->meteran_in);
                //     $stock->out = str_replace(',', '', $request->meteran_out);
                //     $stock->description = 'Stock Air';
                //     $stock->status  = true;
                //     $stock->save();
                // }

                if($cashes){
                    $cash = $cashes;
                    if($request->cash_amount){
                        $cash->amount = str_replace(',', '', $request->cash_amount);
                        $cash->save();
                    }

                }else{
                    $cash = new Cash();
                    $cash->depot_id = str_replace(',', '', $employee->depot_id);
                    $cash->amount = $request->cash_amount;
                    $cash->save();
                }

        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('operational.serah-terima.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('operational::opening.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $employee = Employee::all();
        $employe = Employee::where('user_id', auth()->user()->id)->first();
        $yesterday = Carbon::yesterday()->format('yy-m-d');
        $serahTerima = SerahTerimaShift::findOrFail($id);
        if($employe){
            $opening = Opening::whereDepotId($employe->depot_id)->whereOpeningDate($serahTerima->serah_terima_date)->first();
            $serahTerimaYes = SerahTerimaShift::where('depot_id', $serahTerima->depot_id)->where('serah_terima_date', $yesterday)->first();
            $stock = Stock::where('warehouse_id', $serahTerima->depot_id)->where('description','Stock Air')->first();
            $cash = Cash::where('depot_id', $serahTerima->depot_id)->first();
        }else{
            noty()->danger('Oops','Please set your depot location');
            return redirect()->route('operational.opening.index');
        }


        return view('operational::serah-terima.edit', compact('employee', 'stock','cash','serahTerima','serahTerimaYes', 'opening'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(SerahTerimaRequest $request, $id)
    {
        DB::beginTransaction();

        try {

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $date = Carbon::now();
            $date->toDateTimeString();
            $serahTerima = SerahTerimaShift::where('depot_id', $employee->depot_id)->where('serah_terima_date', $date->format('yy-m-d'))->first();
             $stock = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cash = Cash::where('depot_id', $employee->depot_id)->first();
                if($stock){
                    $stock = $stock;
                }else{
                    $stock = new Stock();
                    $stock->description = 'Stock Air';
                    $stock->warehouse_id = $employee->depot_id;
                    $stock->save();
                }
                if($cash){
                    $cash = $cash;
                }else{
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
            if($employee){
                $serahTerima = $serahTerima;
                    $serahTerima->depot_id = $employee->depot_id;
                    $serahTerima->employee_id = $employee->id;
                    $serahTerima->meteran_in_awal = str_replace(',', '', $request->meteran_in_awal) ?? 0;
                    $serahTerima->meteran_in_akhir = str_replace(',', '', $request->meteran_in_akhir) ?? 0;
                    $serahTerima->meteran_in = str_replace(',', '', $request->meteran_in) ?? 0;
                    $serahTerima->meteran_out_awal = str_replace(',', '', $request->meteran_out_awal) ?? 0;
                    $serahTerima->meteran_out_akhir = str_replace(',', '', $request->meteran_out_akhir) ?? 0;
                    $serahTerima->meteran_out = str_replace(',', '', $request->meteran_out) ?? 0;
                    $serahTerima->meteran_out_awal2 = str_replace(',', '', $request->meteran_out_awal2) ?? 0;
                    $serahTerima->meteran_out_akhir2 = str_replace(',', '', $request->meteran_out_akhir2) ?? 0;
                    $serahTerima->meteran_out2 = str_replace(',', '', $request->meteran_out2) ?? 0;
                    $serahTerima->cash_amount = str_replace(',', '', $request->cash_amount) ?? 0;
                    $serahTerima->persediaan_akhir = str_replace(',', '', $request->persediaan_akhir);
                    $serahTerima->drop_amplop_setoran_bank = $request->drop_amplop_setoran_bank ?? 0;
                    $serahTerima->disiapkan_oleh = $request->disiapkan_oleh ;
                    $serahTerima->disetujui_oleh = $request->disetujui_oleh;
                    $serahTerima->save();

            }else{
                noty()->danger('Oops','Please set your depot location');
                return redirect()->route('operational.serah-terima.index');
            }
        } catch(\Exception $e) {
            DB::rollback();
            flash('Ops, try again'. $e->getMessage())->error();
            return back();
        }

        try {
                $stocks = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cashes = Cash::where('depot_id', $employee->depot_id)->first();
                // if($stocks){
                //     $stock = $stocks;
                //     $stock->in = str_replace(',', '', $stock->in) - str_replace(',', '', $serahTerima->meteran_in);
                //     $stock->out = str_replace(',', '', $stock->out) + str_replace(',', '', $serahTerima->meteran_out);
                //     $stock->qty = str_replace(',', '', $stock->in) - str_replace(',', '', $stock->out);
                //     $stock->save();

                //     if($request->meteran_in){
                //         $stock->in = str_replace(',', '', $request->meteran_in);
                //         $stock->save();
                //     }

                //     if($request->meteran_out){
                //         $stock->out = str_replace(',', '', $request->meteran_out);
                //         $stock->save();
                //     }

                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->save();


                // }else{
                //     $stock = new Stock();
                //     $stock->warehouse_id = $employee->depot_id;
                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->in = str_replace(',', '', $request->meteran_in);
                //     $stock->out = str_replace(',', '', $request->meteran_out);
                //     $stock->description = 'Stock Air';
                //     $stock->status  = true;
                //     $stock->save();
                // }

                if($cashes){
                    $cash = $cashes;
                    if($request->cash_amount){
                        $cash->amount = str_replace(',', '', $request->cash_amount);
                        $cash->save();
                    }

                }else{
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->amount = str_replace(',', '', $request->cash_amount);
                    $cash->save();
                }

        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('operational.serah-terima.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $serahTerima = SerahTerimaShift::findOrFail($id);

            // $employee = Employee::where('user_id', auth()->user()->id)->first();
            // if($employee){
                $stocks = Stock::where('warehouse_id', $serahTerima->depot_id)->where('description','Stock Air')->first();
                $cashes = Cash::where('depot_id', $serahTerima->depot_id)->first();
                // if($stocks){
                //     $stock = $stocks;
                //     $stock->in = $stock->in - $serahTerima->meteran_in;
                //     $stock->out = $stock->out + $serahTerima->meteram_out;
                //     $stock->qty = $stock->in - $stock->out;
                //     $stock->save();
                // }

                if($cashes){
                    $cash = $cashes;
                    $cash->amount = $cash->amount - $serahTerima->cash_amount;
                    $cash->save();
                }
            // }else{
            //     noty()->danger('Oops','Please set your depot location');
            //     return redirect()->route('operational.serah-terima.index');
            // }

            $serahTerima->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();

    }

    public function view(){
        return view('operational::serah-terima.view');
    }

    public function report(){
        $opening = [];
        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $page           = 10;

        if($fromDate && $untilDate){
                $opening = SerahTerimaShift::withoutGlobalScopes()
                ->with('employee', 'depot')
                ->whereNull('deleted_at')
                ->whereBetween('serah_terima_date', [$fromDate, $untilDate])
                ->paginate($page);

        }
        return view('operational::serah-terima.report', compact('opening'));

    }

    public function reportDetail($id)
    {
        $row = SerahTerimaShift::findOrFail($id);
        $from_date = request('from_date');
        $until_date = request('until_date');

        return view('operational::serah-terima.report-detail', compact('row','from_date','until_date'));
    }

}
