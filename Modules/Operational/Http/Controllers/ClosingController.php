<?php

namespace Modules\Operational\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Operational\DataTables\ClosingDatatable;
use Modules\HR\Entities\Employee;
use Carbon\Carbon;
use Modules\CashBook\Entities\Cash;
use Modules\CashBook\Entities\CashDetail;
use Modules\Stock\Entities\Stock;
use Modules\Operational\Entities\Closing;
use Modules\Operational\Entities\SerahTerimaShift;
use Modules\Operational\Http\Requests\ClosingRequest;
use Illuminate\Support\Facades\DB;

class ClosingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ClosingDatatable $datatable)
    {
        return $datatable->render('operational::closing.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $employee = Employee::all();
        $employe = Employee::where('user_id', auth()->user()->id)->first();
        $today = Carbon::parse()->format('yy-m-d');
        if($employe){
            $serahTerima = SerahTerimaShift::whereDepotId($employe->depot_id)->whereSerahTerimaDate(now())->first();
            $serahTerimaYes = SerahTerimaShift::where('serah_terima_date', $today)->first();
            $stock = Stock::where('warehouse_id', $employe->depot_id)->where('description','Stock Air')->first();
            $cash = Cash::where('depot_id', $employe->depot_id)->first();
            if($stock){
                $stock = $stock;
            }else{
                $stock = new Stock();
                $stock->description = 'Stock Air';
                $stock->warehouse_id = $employe->depot_id;
                $stock->save();
            }
            if($cash){
                $cash = $cash;
            }else{
                $cash = new Cash();
                $cash->depot_id = $employe->depot_id;
                $cash->save();
            }


        }else{

            noty()->danger('Oops','Please set your depot location');
            return redirect()->route('operational.closing.index');
        }

        return view('operational::closing.create', compact('employee', 'stock','cash','serahTerimaYes', 'serahTerima'));
    }



    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ClosingRequest $request)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $date = Carbon::now();
            $date->toDateTimeString();

            if($employee){
                $closing = Closing::where('depot_id', $employee->depot_id)->where('closing_date', $date->format('yy-m-d'))->first();
                $stock = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cash = Cash::where('depot_id', $employee->depot_id)->first();
                if($stock){
                    $stock = $stock;
                }else{
                    $stock = new Stock();
                    $stock->description = 'Stock Air';
                    $stock->warehouse_id = $employee->depot_id;
                    $stock->save();
                }
                if($cash){
                    $cash = $cash;
                }else{
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
                if($closing){
                    noty()->danger('Oops','Data sudah ada , silahkan ke menu edit untuk merubah');
                    return redirect()->route('operational..index');
                }else{
                    $closing = new Closing();
                    $closing->depot_id = $employee->depot_id;
                    $closing->employee_id = $employee->id;
                    $closing->closing_date = $date->format('yy-m-d');
                    $closing->meteran_in_awal = str_replace(',', '', $request->meteran_in_awal) ?? 0;
                    $closing->meteran_in_akhir = str_replace(',', '', $request->meteran_in_akhir) ?? 0;
                    $closing->meteran_in = str_replace(',', '', $request->meteran_in) ?? 0;
                    $closing->meteran_out_awal = str_replace(',', '', $request->meteran_out_awal) ?? 0;
                    $closing->meteran_out_akhir = str_replace(',', '', $request->meteran_out_akhir) ?? 0;
                    $closing->meteran_out = str_replace(',', '', $request->meteran_out) ?? 0;
                    $closing->meteran_out_awal2 = str_replace(',', '', $request->meteran_out_awal2) ?? 0;
                    $closing->meteran_out_akhir2 = str_replace(',', '', $request->meteran_out_akhir2) ?? 0;
                    $closing->meteran_out2 = str_replace(',', '', $request->meteran_out2) ?? 0;
                    $closing->cash_amount = str_replace(',', '', $request->cash_amount) ?? 0;
                    $closing->persediaan_akhir = str_replace(',', '', $request->persediaan_akhir) ?? 0;
                    $closing->drop_amplop_setoran_bank = $request->drop_amplop_setoran_bank ?? 0;
                    $closing->disiapkan_oleh = $request->disiapkan_oleh;
                    $closing->disetujui_oleh = $request->disetujui_oleh ;
                    $closing->menyapu_mengepel_lantai = $request->menyapu_mengepel_lantai ?? 0;
                    $closing->lap_etalase_kaca_meja_kasir = $request->lap_etalase_kaca_meja_kasir ?? 0;
                    $closing->membersihkan_filter_air = $request->membersihkan_filter_air ?? 0;
                    $closing->menyemprot_disinfektan_diarea_serving = $request->menyemprot_disinfektan_diarea_serving ?? 0;
                    $closing->save();
                }
            }else{
                noty()->danger('Oops','Please set your depot location');
                return redirect()->route('operational.serah-terima.create');
            }

        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }


        try {
                $stocks = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cashes = Cash::where('depot_id', $employee->depot_id)->first();
                // if($stocks){
                //     $stock = $stocks;
                //     if($request->meteran_in){
                //         $stock->in = str_replace(',', '', $request->meteran_in);
                //         $stock->save();
                //     }

                //     if($request->meteran_out){
                //         $stock->out = str_replace(',', '', $request->meteran_out);
                //         $stock->save();
                //     }

                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->save();


                // }else{
                //     $stock = new Stock();
                //     $stock->warehouse_id = $employee->depot_id;
                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->in = str_replace(',', '', $request->meteran_in);
                //     $stock->out = str_replace(',', '', $request->meteran_out);
                //     $stock->description = 'Stock Air';
                //     $stock->status  = true;
                //     $stock->save();
                // }

                if($cashes){
                    $cash = $cashes;
                    if($request->cash_amount){
                        $cash->amount = str_replace(',', '', $request->cash_amount);
                        $cash->save();
                    }

                }else{
                    $cash = new Cash();
                    $cash->depot_id = str_replace(',', '', $employee->depot_id);
                    $cash->amount = $request->cash_amount;
                    $cash->save();
                }

        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('operational.closing.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('operational::opening.show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $employee = Employee::all();
        $employe = Employee::where('user_id', auth()->user()->id)->first();
        $today = Carbon::parse()->format('yy-m-d');
        $closing = Closing::findOrFail($id);
        if($employe){
            $serahTerima = SerahTerimaShift::whereDepotId($employe->depot_id)->whereSerahTerimaDate($closing->closing_date)->first();
            $serahTerimaYes = SerahTerimaShift::where('depot_id', $closing->depot_id)->where('serah_terima_date', $today)->first();
            $stock = Stock::where('warehouse_id', $closing->depot_id)->where('description','Stock Air')->first();
            $cash = Cash::where('depot_id', $closing->depot_id)->first();
        }else{
            noty()->danger('Oops','Please set your depot location');
            return redirect()->route('operational.closing.index');
        }


        return view('operational::closing.edit', compact('employee', 'stock','cash','closing','serahTerimaYes', 'serahTerima'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ClosingRequest $request, $id)
    {
        DB::beginTransaction();

        try {

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $date = Carbon::now();
            $date->toDateTimeString();
            $serahTerima = SerahTerimaShift::where('depot_id', $employee->depot_id)->where('serah_terima_date', $date->format('yy-m-d'))->first();
             $closing = Closing::where('depot_id', $employee->depot_id)->where('closing_date', $date->format('yy-m-d'))->first();
             $stock = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cash = Cash::where('depot_id', $employee->depot_id)->first();
                if($stock){
                    $stock = $stock;
                }else{
                    $stock = new Stock();
                    $stock->description = 'Stock Air';
                    $stock->warehouse_id = $employee->depot_id;
                    $stock->save();
                }
                if($cash){
                    $cash = $cash;
                }else{
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
            if($employee){
                $closing = $closing;
                    $closing->depot_id = $employee->depot_id;
                    $closing->employee_id = $employee->id;
                    $closing->meteran_in_awal = str_replace(',', '', $request->meteran_in_awal) ?? 0;
                    $closing->meteran_in_akhir = str_replace(',', '', $request->meteran_in_akhir) ?? 0;
                    $closing->meteran_in = str_replace(',', '', $request->meteran_in) ?? 0;
                    $closing->meteran_out_awal = str_replace(',', '', $request->meteran_out_awal) ?? 0;
                    $closing->meteran_out_akhir = str_replace(',', '', $request->meteran_out_akhir) ?? 0;
                    $closing->meteran_out = str_replace(',', '', $request->meteran_out) ?? 0;
                    $closing->meteran_out_awal2 = str_replace(',', '', $request->meteran_out_awal2) ?? 0;
                    $closing->meteran_out_akhir2 = str_replace(',', '', $request->meteran_out_akhir2) ?? 0;
                    $closing->meteran_out2 = str_replace(',', '', $request->meteran_out2) ?? 0;
                    $closing->cash_amount = str_replace(',', '', $request->cash_amount) ?? 0;
                    $closing->persediaan_akhir = str_replace(',', '', $request->persediaan_akhir) ?? 0;
                    $closing->drop_amplop_setoran_bank = $request->drop_amplop_setoran_bank ?? 0;
                    $closing->disiapkan_oleh = $request->disiapkan_oleh;
                    $closing->disetujui_oleh = $request->disetujui_oleh ;
                    $closing->menyapu_mengepel_lantai = $request->menyapu_mengepel_lantai ?? 0;
                    $closing->lap_etalase_kaca_meja_kasir = $request->lap_etalase_kaca_meja_kasir ?? 0;
                    $closing->membersihkan_filter_air = $request->membersihkan_filter_air ?? 0;
                    $closing->menyemprot_disinfektan_diarea_serving = $request->menyemprot_disinfektan_diarea_serving ?? 0;
                    $closing->save();

            }else{
                noty()->danger('Oops','Please set your depot location');
                return redirect()->route('operational.serah-terima.index');
            }
        } catch(\Exception $e) {
            DB::rollback();
            flash('Ops, try again'. $e->getMessage())->error();
            return back();
        }

        try {
                $stocks = Stock::where('warehouse_id', $employee->depot_id)->where('description','Stock Air')->first();
                $cashes = Cash::where('depot_id', $employee->depot_id)->first();
                // if($stocks){
                //     $stock = $stocks;
                //     $stock->in = str_replace(',', '', $stock->in) - str_replace(',', '', $closing->meteran_in);
                //     $stock->out = str_replace(',', '', $stock->out) + str_replace(',', '', $closing->meteran_out);
                //     $stock->qty = str_replace(',', '', $stock->in) - str_replace(',', '', $stock->out);
                //     $stock->save();

                //     if($request->meteran_in){
                //         $stock->in = str_replace(',', '', $request->meteran_in);
                //         $stock->save();
                //     }

                //     if($request->meteran_out){
                //         $stock->out = str_replace(',', '', $request->meteran_out);
                //         $stock->save();
                //     }

                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->save();


                // }else{
                //     $stock = new Stock();
                //     $stock->warehouse_id = $employee->depot_id;
                //     $stock->qty = str_replace(',', '', $request->meteran_in) - str_replace(',', '', $request->meteran_out);
                //     $stock->in = str_replace(',', '', $request->meteran_in);
                //     $stock->out = str_replace(',', '', $request->meteran_out);
                //     $stock->description = 'Stock Air';
                //     $stock->status  = true;
                //     $stock->save();
                // }

                if($cashes){
                    $cash = $cashes;
                    if($request->cash_amount){
                        $cash->amount = str_replace(',', '', $request->cash_amount);
                        $cash->save();
                    }

                }else{
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->amount = str_replace(',', '', $request->cash_amount);
                    $cash->save();
                }

        } catch(\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('operational.closing.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $closing = Closing::findOrFail($id);

            // $employee = Employee::where('user_id', auth()->user()->id)->first();
            // if($employee){
                $stocks = Stock::where('warehouse_id', $closing->depot_id)->where('description','Stock Air')->first();
                $cashes = Cash::where('depot_id', $closing->depot_id)->first();
                // if($stocks){
                //     $stock = $stocks;
                //     $stock->in = $stock->in - $closing->meteran_in;
                //     $stock->out = $stock->out + $closing->meteram_out;
                //     $stock->qty = $stock->in - $stock->out;
                //     $stock->save();
                // }

                if($cashes){
                    $cash = $cashes;
                    $cash->amount = $cash->amount - $closing->cash_amount;
                    $cash->save();
                }
            // }else{
            //     noty()->danger('Oops','Please set your depot location');
            //     return redirect()->route('operational.closing.index');
            // }

            $closing->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();

    }

    public function view(){
        return view('operational::serah-terima.view');
    }

    public function report(){
        $opening = [];
        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $page           = 10;

        if($fromDate && $untilDate){
                $opening = Closing::withoutGlobalScopes()
                ->with('employee', 'depot')
                ->whereNull('deleted_at')
                ->whereBetween('closing_date', [$fromDate, $untilDate])
                ->paginate($page);

        }
        return view('operational::closing.report', compact('opening'));

    }

    public function reportDetail($id)
    {
        $row = Closing::findOrFail($id);
        $from_date = request('from_date');
        $until_date = request('until_date');

        return view('operational::closing.report-detail', compact('row','from_date','until_date'));
    }

}
