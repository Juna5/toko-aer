@csrf
<div class="card">
    <div class="card-body col-md-12">
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran In</label>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Awal (m3)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_in_awals" style="text-align: right;" value="{{ old('meteran_in_awals') ?? $opening ? $opening->meteran_in/1000 : '-'  }}" id="m31" oninput="weightConverter(this.value, 'liter1')" onchange="weightConverter(this.value, 'liter1')" readonly step="any">
            </div>
             <div class="form-group col-md-6">
                <label for="photo_profile" >Awal (Liter)</label>
                <input type="text" class="form-control r-0 light s-12" data-type="currency" name="meteran_in_awal_1" style="text-align: right;" value="{{ old('meteran_in_awal') ??  $opening ? $opening->meteran_in : '-' }}" id="liter1" readonly step="any">
                <input type="hidden" class="form-control r-0 light s-12" data-type="currency" name="meteran_in_awal" style="text-align: right;" value="{{ old('meteran_in_awal') ??  $opening ? $opening->meteran_in : '0' }}"  step="any">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Akhir (m3)</label>
                <input type="number" class="form-control r-0 light s-12 " name="meteran_in_akhirs"  style="text-align: right;" value="{{ old('meteran_in_akhirs') ?? $serahTerima ? $serahTerima->meteran_in_akhir/1000 : ''  }}" oninput="weightConverter(this.value, 'liter2','liter1','liter2','meteran_in')" onchange="weightConverter(this.value, 'liter2','liter1','liter2','meteran_in')" step="any">
            </div>
             <div class="form-group col-md-6">
                <label for="photo_profile">Akhir (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " readonly data-type="currency" name="meteran_in_akhir" style="text-align: right;" value="{{ old('meteran_in_akhir') ?? $serahTerima->meteran_in_akhir  }}" id="liter2" step="any">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran In (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " readonly data-type="currency" name="meteran_in" style="text-align: right;" value="{{ old('meteran_in') ?? $serahTerima->meteran_in  }}" id="meteran_in" step="any">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran Out 1</label>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Awal (m3)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awals" style="text-align: right;" value="{{ old('meteran_out_awals') ?? $opening ? $opening->meteran_out/1000 : '-'  }}"   readonly step="any">
            </div>
             <div class="form-group col-md-6">
                <label for="photo_profile" >Awal (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awal_1" style="text-align: right;" value="{{ old('meteran_out_awal') ?? $opening ? $opening->meteran_out : '-' }}" id="liter3" readonly step="any">
                <input type="hidden" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awal" style="text-align: right;" value="{{ old('meteran_out_awal') ?? $opening ? $opening->meteran_out : '0' }}"  step="any">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Akhir (m3)</label>
                <input type="number" class="form-control r-0 light s-12"  name="meteran_out_akhirs" style="text-align: right;" value="{{ old('meteran_out_akhirs') ?? $serahTerima->meteran_out_akhir/1000  }}" oninput="weightConverter(this.value, 'liter4','liter3','liter4','meteran_out')" onchange="weightConverter(this.value, 'liter4','liter3','liter4','meteran_out')" id="m32" step="any">
            </div>
             <div class="form-group col-md-6">
                <label for="photo_profile">Akhir (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " readonly data-type="currency" name="meteran_out_akhir" style="text-align: right;" value="{{ old('meteran_out_akhir') ?? $serahTerima->meteran_out_akhir  }}" id="liter4" step="any">
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran Out (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " readonly data-type="currency" name="meteran_out" style="text-align: right;" value="{{ old('meteran_out') ?? $serahTerima->meteran_out  }}" id="meteran_out" step="any">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran Out 2</label>
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Awal (m3)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awals2" style="text-align: right;" value="{{ old('meteran_out_awals2') ?? $opening ? $opening->meteran_out2/1000 : '-'  }}"   readonly step="any">
            </div>
             <div class="form-group col-md-6">
                <label for="photo_profile" >Awal (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awal_c2" style="text-align: right;" value="{{ old('meteran_out_awal2') ?? $opening ? $opening->meteran_out2 : '-' }}" id="liter5" readonly step="any">
                <input type="hidden" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awal2" style="text-align: right;" value="{{ old('meteran_out_awal2') ?? $opening ? $opening->meteran_out2 : '0' }}"  step="any">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Akhir (m3)</label>
                <input type="number" class="form-control r-0 light s-12"  name="meteran_out_akhirs2" style="text-align: right;" value="{{ old('meteran_out_akhirs2') ?? $serahTerima->meteran_out_akhir2/1000  }}" oninput="weightConverter(this.value, 'liter6','liter5','liter6','meteran_out2')" onchange="weightConverter(this.value, 'liter6','liter5','liter6','meteran_out2')" step="any">
            </div>
             <div class="form-group col-md-6">
                <label for="photo_profile">Akhir (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " readonly data-type="currency" name="meteran_out_akhir2" style="text-align: right;" value="{{ old('meteran_out_akhir2') ?? $serahTerima->meteran_out_akhir2  }}" id="liter6" step="any">
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran Out (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " readonly data-type="currency" name="meteran_out2" style="text-align: right;" value="{{ old('meteran_out2') ?? $serahTerima->meteran_out2  }}" id="meteran_out2" step="any">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Persediaan Akhir (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="persediaan_akhir" id="persediaan" style="text-align: right;" value="{{ old('persediaan_akhir') ?? $serahTerima->persediaan_akhir  }}" >
            </div>
        </div>
        <hr>
        <div class="row">
           <div class="form-group col-md-6">
            <label for="photo_profile" >Drop Amplop Setoran Bank</label><br>
                <label class="colorinput">
                    <input name="drop_amplop_setoran_bank" type="checkbox" value="1" class="colorinput-input" {{ old('drop_amplop_setoran_bank') ?? $serahTerima->drop_amplop_setoran_bank == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Saldo Kas</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="cash_amount" style="text-align: right;" value="{{ old('cash_amount') ?? number_format($serahTerima->cash_amount)  }}" >
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Disiapkan Oleh</label>
                 <select name="disiapkan_oleh" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($employee as $row)
                            @if( old('disiapkan_oleh') )
                                <option value="{{ $row->id }}" {{ old('disiapkan_oleh') == $row->id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @else
                                <option value="{{ $row->id }}" {{ $row->id == $serahTerima->disiapkan_oleh ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @endif
                        @endforeach
                    </select>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Disetujui Oleh</label>
                 <select name="disetujui_oleh" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($employee as $rows)
                            @if( old('disetujui_oleh') )
                                <option value="{{ $rows->id }}" {{ old('disetujui_oleh') == $rows->id ? 'selected' : ''  }}>{{ $rows->name  }}</option>
                            @else
                                <option value="{{ $rows->id }}" {{ $rows->id == $serahTerima->disetujui_oleh ? 'selected' : ''  }}>{{ $rows->name  }}</option>
                            @endif
                        @endforeach
                    </select>
            </div>
        </div>

    </div>
</div>

<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
