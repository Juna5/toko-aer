@extends('operational::layouts.app')

@section('operational::title', 'Serah Terima Report')

@section('operational::breadcrumb-2')
    @include('operational::include.breadcrum', [
    'title' => 'operational',
    'active' => true,
    'url' => route('operational.view')
    ])
@endsection

@section('operational::breadcrumb-3')
    @include('operational::include.breadcrum', [
    'title' => 'Serah Terima',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('operational::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <form action="{{ route('operational.serah-terima.report') }}" method="GET">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="from_date"
                                           name="from_date"
                                           value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">To Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="until_date"
                                           name="until_date"
                                           value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                        <a href="{{ route('operational.view') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br><br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Serah Terima Date</th>
                                <th>Created By</th>
                                <th>Depot</th>
                            </tr>

                            @forelse($opening as $index => $row)
                                <tr align="center">
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('operational.serah-terima.report-detail',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}">{{ format_d_month_y($row->serah_terima_date) }}</a>
                                    </td>
                                    <td>{{ optional($row->employee)->name }}</td>
                                     <td>{{ optional($row->depot)->name }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(!empty($opening))
                                {{ $opening->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
