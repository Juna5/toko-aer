@extends('operational::layouts.app')

@section('operational::title', 'Serah Terima')

@section('operational::breadcrumb-2')
    @include('operational::include.breadcrum', [
    'title' => 'Operational',
    'active' => true,
    'url' => route('operational.view')
    ])
@endsection

@section('operational::breadcrumb-3')
    @include('operational::include.breadcrum', [
    'title' => 'Serah Terima',
    'active' => true,
    'url' => route('operational.serah-terima.index')
    ])
@endsection

@section('operational::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        @include('flash::message')

        <div class="card-body">
            <div class="form-group pb-1">
                <a href="{{ route('operational.serah-terima.index') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                    <img src="{{asset('images/excel.png')}}" alt="">
                </a>
            </div>
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
