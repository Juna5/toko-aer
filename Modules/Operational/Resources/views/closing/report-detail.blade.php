@extends('operational::layouts.app')

@section('operational::title', 'Closing')

@section('operational::breadcrumb-2')
@include('operational::include.breadcrum', [
'title' => 'Closing',
'active' => true,
'url' => route('operational.opening.index')
])
@endsection

@section('operational::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary">{{ $row->code_number }}
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Employee</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->employee)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Opening Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($row->serah_terima_date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Depot</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->depot_id ? optional($row->depot)->name : '-' }}</td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="col-md-6">
                                         <div class="section-title">Stock</div>
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Meteran In Awal</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_in_awal ?  number_format($row->meteran_in_awal) : '-' }} Liter </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran In Akhir</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_in_akhir ?  number_format($row->meteran_in_akhir) : '-' }} Liter </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran In</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_in ?  number_format($row->meteran_in) :  '-'  }} Liter </td>
                                                    </tr>
                                                  
                                                    <tr>
                                                        <td width="30%">Meteran Out Awal</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_out_awal ?  number_format($row->meteran_out_awal) : '-' }} Liter </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran Out Akhir</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_out_akhir ?  number_format($row->meteran_out_akhir) : '-' }} Liter </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran In</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ number_format($row->meteran_out) }} Liter </td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                        <div class="row">
                                           <div class="col-md-6">
                                             <div class="section-title">Kas</div>
                                                <div class="table-responsive">
                                                    <table class="table  table-hover table-md">
                                                        <tr>
                                                            <td width="30%">Saldo Cas</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ $row->cash_amount ?  number_format($row->cash_amount) : '-' }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Disiapkan Oleh</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ optional($row->disiapkan)->name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Disetujui Oleh</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ optional($row->disetujui)->name }}</td>
                                                        </tr>
                                                        
                                                        
                                                        
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                           <div class="col-md-6">
                                             <div class="section-title">Kebersihan Toko</div>
                                                <div class="table-responsive">
                                                    <table class="table  table-hover table-md">
                                                        @php
                                                        $check = 'fa-times';
                                                        $check2 = 'fa-times';
                                                        $check3 = 'fa-times';
                                                        $check4 = 'fa-times';
                                                        if($row->menyapu_mengepel_lantai == true){
                                                            $check = 'fa-check';
                                                        }
                                                        
                                                        if($row->lap_etalase_kaca_meja_kasir == true){
                                                            $check2 = 'fa-check';
                                                        }

                                                       
                                                        if($row->membersihkan_filter_air == true){
                                                            $check3 = 'fa-check';
                                                        }
     
                                                        if($row->meyemprotkan_desinfektan_diarea_serving == true){
                                                            $check4 = 'fa-check';
                                                        }


                                                        @endphp
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $check }}"></i></td>
                                                            
                                                            <td width="80%">Menyapu & Mengepel Lantai</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $check2 }}"></i></td>
                                                          
                                                            <td width="80%">Lap Etalase & Kaca & Meja Kasir</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $check3 }}"></i></td>
                                                            <td width="80%">Membersihkan Filter Air</td>
                                                        </tr>
                                                         <tr>
                                                            <td width="10%"><i class="fa {{ $check4 }}"></i></td>
                                                            <td width="80%">Menyemprot Desinfektan diarea serving</td>
                                                        </tr>

                                                        
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                           
                                    <hr>
                                      <div class="text-md-right">
                                        <a href="{{ route('operational.closing.report',['from_date' => $from_date,'until_date'=>$until_date]) }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
