@extends('operational::layouts.app')

@section('operational::title', 'Closing')

@section('operational::breadcrumb-2')
@include('operational::include.breadcrum', [
'title' => 'Closing',
'active' => true,
'url' => route('operational.closing.index')
])
@endsection
@section('operational::breadcrumb-3')
@include('operational::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('operational.closing.edit', $closing->id)
])
@endsection

@section('operational::content')
<div class="row">
    <div class="container">
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card no-b no-r">
                    <div class="card-body">
                        <h5 class="card-title">
                            <div class="text-right">
                                <a href="{{ route('operational.closing.index') }}" class="btn btn-warning">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </a>
                            </div>
                        </h5>
                        <hr>
                        @include('flash::message')
                        @include('include.error-list')
                        <div class="row">
                            <div class="col-12">
                                <form action="{{ route('operational.closing.update', $closing->id) }}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @include('operational::closing.form', [
                                    'submitButtonText' => 'Update'
                                    ])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script type="text/javascript">


    function weightConverter(valNum, target, awal, akhir, meteran) {
        document.getElementById(target).value = valNum*1000;
        var akhirs =  document.getElementById(akhir).value
        var awals =  document.getElementById(awal).value
        if(awals == '-'){
            awals = 0
        }else{
            awals = awals
        }

        document.getElementById(meteran).value =   parseInt(akhirs) - parseInt(awals)
        var ins =  document.getElementById('liter2').value
        var outs1 =  document.getElementById('liter4').value
        var outs2 =  document.getElementById('liter6').value
        if (outs1 == '') {
            outs1 = 0;
        }
        if (outs2 == '') {
            outs2 = 0;
        }
        var outs = parseInt(outs1) + parseInt(outs2);
        
        var persediaan;
        if(ins != '' && outs != ''){
            persediaan = parseInt(ins) - parseInt(outs);
        }else if(ins != '' && outs == ''){
            persediaan = parseInt(ins)
        }else if(ins == '' && outs !=''){
            perdiaan = parseInt(outs)
        }else {
            persediaan = 0;
        }

        document.getElementById('persediaan').value = persediaan
        console.log(persediaan)

    }



</script>
@endpush
