@extends('operational::layouts.app')

@section('operational::title', 'Operational')

@section('operational::breadcrumb-2')
@include('operational::include.breadcrum', [
'title' => 'Operational',
'active' => true,
'url' => route('operational.view')
])
@endsection

@section('operational::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/png/open.png') }}" class="img-fluid">
                            <div class=" font-weight-bold">Opening</div>
                            <div class="text-muted text-small">
                                @can('view purchase requisition')
                                @endcan
                                <a href="{{ route('operational.opening.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                            
                                @can('view purchase requisition')
                                @endcan
                                <a href="{{ route('operational.opening.report') }}" class="card-cta">Reports</a>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/png/change_shift.png') }}" class="">
                            <div class=" font-weight-bold">Serah Terima</div>
                            <div class="text-muted text-small">
                                @can('view purchase order')
                                @endcan
                                <a href="{{ route('operational.serah-terima.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                
                                @can('view purchase order')
                                
                                <a href="{{ route('operational.serah-terima.report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/png/close.png') }}" class="">
                            <div class=" font-weight-bold">Closing</div>
                            <div class="text-muted text-small">
                                @can('view purchase order')
                                @endcan

                                <a href="{{ route('operational.closing.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @can('view purchase order')
                               
                                <a href="{{ route('operational.closing.report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
@endsection