@csrf
<div class="card">
    <div class="card-body">
        <div>
            A. Operational
        </div>
        <br>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran In Awal (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_in_awal" style="text-align: right;" value="{{ old('meteran_in_awal') ?? $closing ? number_format($closing->meteran_in_akhir) : 0  }}" readonly>
            </div>

                    <div class="form-group col-md-3">
                    <label for="photo_profile">Sesuai</label><br>
                    <label class="colorinput">
                        <input name="meteran_in_status" type="radio" value="1" class="colorinput-input" {{ old('meteran_in_status') ?? $opening->meteran_in_status == "1" ? 'checked' : '' }} />
                        <span class="colorinput-color bg-info"></span>
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label for="photo_profile">Tidak Sesuai</label><br>
                    <label class="colorinput">
                        <input name="meteran_in_status" type="radio" value="0" class="colorinput-input" {{ old('meteran_in_status') ?? $opening->meteran_in_status == "0" ? 'checked' : '' }}/>
                        <span class="colorinput-color bg-info"></span>
                    </label>
                </div>


            <div class="form-group col-md-6">
                <label for="photo_profile">Angka Meteran Air (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_in" style="text-align: right;" value="{{ old('meteran_in') ?? number_format($opening->meteran_in) }}">
            </div>
            <div class="form-group col-md-6">

            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Foto Meteran</label>
                <input
                type="file"
                name="meteran_in_photo"
                id="input-file-max-fs"
                class="dropify"
                data-max-file-size="10M"
                data-id="{{ $meteranIn ? $meteranIn->id : '' }}"
                data-default-file="{{ $meteranIn ? '/storage/'.$meteranIn->id.'/'.$meteranIn->file_name : '' }}"
                />
            </div>

        </div>
       <div class="row">
           <div class="form-group col-md-12">
               <hr>
               <label>Meteran Out 1</label>
           </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran Out Awal (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awal" style="text-align: right;" value="{{ old('meteran_out_awal') ?? $closing ? number_format($closing->meteran_out_akhir) : 0  }}" readonly>
            </div>
            <div class="form-group col-md-3">
                    <label for="photo_profile">Sesuai</label><br>
                    <label class="colorinput">
                        <input name="meteran_out_status" type="radio" value="1" class="colorinput-input" {{ old('meteran_out_status') ?? $opening->meteran_out_status == "1" ? 'checked' : '' }} />
                        <span class="colorinput-color bg-info"></span>
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label for="photo_profile">Tidak Sesuai</label><br>
                    <label class="colorinput">
                        <input name="meteran_out_status" type="radio" value="0" class="colorinput-input" {{ old('meteran_out_status') ?? $opening->meteran_out_status == "0" ? 'checked' : '' }}/>
                        <span class="colorinput-color bg-info"></span>
                    </label>
                </div>


            <div class="form-group col-md-6">
                <label for="photo_profile">Angka Meteran Air (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out" style="text-align: right;" value="{{ old('meteran_out') ?? number_format($opening->meteran_out) }}">
            </div>
            <div class="form-group col-md-6">

            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Foto Meteran</label>
                <input
                type="file"
                name="meteran_out_photo"
                id="input-file-max-fs"
                class="dropify"
                data-max-file-size="10M"
                data-id="{{ $meteranOut ? $meteranOut->id : '' }}"
                data-default-file="{{ $meteranOut ? '/storage/'.$meteranOut->id.'/'.$meteranOut->file_name : '' }}"
                />
            </div>

        </div>
       <div class="row">
           <div class="form-group col-md-12">
               <hr>
               <label>Meteran Out 2</label>
           </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Meteran Out Awal (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out_awal2" style="text-align: right;" value="{{ old('meteran_out_awal2') ?? $closing ? number_format($closing->meteran_out_akhir2) : 0  }}" readonly>
            </div>
            <div class="form-group col-md-3">
                    <label for="photo_profile">Sesuai</label><br>
                    <label class="colorinput">
                        <input name="meteran_out_status2" type="radio" value="1" class="colorinput-input" {{ old('meteran_out_status2') ?? $opening->meteran_out_status2 == "1" ? 'checked' : '' }} />
                        <span class="colorinput-color bg-info"></span>
                    </label>
                </div>
                <div class="form-group col-md-3">
                    <label for="photo_profile">Tidak Sesuai</label><br>
                    <label class="colorinput">
                        <input name="meteran_out_status2" type="radio" value="0" class="colorinput-input" {{ old('meteran_out_status2') ?? $opening->meteran_out_status2 == "0" ? 'checked' : '' }}/>
                        <span class="colorinput-color bg-info"></span>
                    </label>
                </div>


            <div class="form-group col-md-6">
                <label for="photo_profile">Angka Meteran Air (Liter)</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="meteran_out2" style="text-align: right;" value="{{ old('meteran_out2') ?? number_format($opening->meteran_out2) }}">
            </div>
            <div class="form-group col-md-6">

            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Foto Meteran</label>
                <input
                type="file"
                name="meteran_out_photo2"
                id="input-file-max-fs"
                class="dropify"
                data-max-file-size="10M"
                data-id="{{ $meteranOut2 ? $meteranOut2->id : '' }}"
                data-default-file="{{ $meteranOut2 ? '/storage/'.$meteranOut2->id.'/'.$meteranOut2->file_name : '' }}"
                />
            </div>

        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Saldo Kas</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="cash_awal" style="text-align: right;" value="{{ old('cash_awal') ?? number_format($cash->amount)  }}" readonly>
            </div>
            <div class="form-group col-md-3">
                <label for="photo_profile">Sesuai</label><br>
                <label class="colorinput">
                    <input name="cash_status" type="radio" value="1" class="colorinput-input" {{ old('cash_status') ?? $opening->cash_status == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-3">
                <label for="photo_profile">Tidak Sesuai</label><br>
                <label class="colorinput">
                    <input name="cash_status" type="radio" value="0" class="colorinput-input" {{ old('cash_status') ?? $opening->cash_status == "0" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>

            <div class="form-group col-md-6">
                <label for="photo_profile">Jumlah Uang</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="cash_amount" style="text-align: right;" value="{{ old('cash_amount') ?? number_format($opening->cash_amount) }}">
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Disaksikan Oleh</label>
                 <select name="disaksikan_oleh" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($employee as $row)
                            @if( old('disaksikan_oleh') )
                                <option value="{{ $row->id }}" {{ old('disaksikan_oleh') == $row->id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @else
                                <option value="{{ $row->id }}" {{ $row->id == $opening->disaksikan_oleh ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @endif
                        @endforeach
                    </select>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Foto Uang</label>
                <input
                type="file"
                name="bukti_uang_photo"
                id="input-file-max-fs"
                class="dropify"
                data-max-file-size="10M"
                data-id="{{ $buktiUang ? $buktiUang->id : '' }}"
                data-default-file="{{ $buktiUang ? '/storage/'.$buktiUang->id.'/'.$buktiUang->file_name : '' }}"
                />
            </div>

        </div>
        <br>
        <div class="row">
           <div class="form-group col-md-12">
               <label>Kebersihan Toko</label>
           </div>
           <div class="form-group col-md-3">
                <label for="photo_profile">Menyapu Halaman Parkir </label><br>
                <label class="colorinput">
                    <input name="menyapu_halaman_parkir" type="checkbox" value="1" class="colorinput-input" {{ old('menyapu_halaman_parkir') ?? $opening->menyapu_halaman_parkir == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-3">
                <label for="photo_profile">Membersihan Toilet</label><br>
                <label class="colorinput">
                    <input name="membersihkan_toilet" type="checkbox" value="1" class="colorinput-input" {{ old('membersihkan_toilet') ?? $opening->membersihkan_toilet == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-3">
                <label for="photo_profile">Menyiram Tanaman</label><br>
                <label class="colorinput">
                    <input name="menyiram_tanaman" type="checkbox" value="1" class="colorinput-input" {{ old('menyiram_tanaman') ?? $opening->menyiram_tanaman == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
        </div>
        <hr>
         <div>
            B. Personal Team Shift 1 ( Server & Kasir )
        </div>
        <br>
        <div class="row">
           <div class="form-group col-md-1">
                <label for="photo_profile">Server</label><br>
                <label class="colorinput">
                    <input name="seragam_rapi_bersih_server" type="checkbox" value="1" class="colorinput-input" {{ old('seragam_rapi_bersih_server') ?? $opening->seragam_rapi_bersih_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
                <label for="photo_profile">Kasir</label><br>
                <label class="colorinput">
                    <input name="seragam_rapi_bersih_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('seragam_rapi_bersih_kasir') ?? $opening->seragam_rapi_bersih_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                    Memakai Seragam Rapi & Bersih ( Kaos dimasukan + Jeans Biru + Sepatu Sport )
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="memakai_masker_face_shield_server" type="checkbox" value="1" class="colorinput-input" {{ old('memakai_masker_face_shield_server') ?? $opening->memakai_masker_face_shield_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label>
                <label class="colorinput">
                    <input name="memakai_masker_face_shield_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('memakai_masker_face_shield_kasir') ?? $opening->memakai_masker_face_shield_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                    Memakai Masker + Face Shield
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="kebersihan_diri_server" type="checkbox" value="1" class="colorinput-input" {{ old('kebersihan_diri_server') ?? $opening->kebersihan_diri_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="kebersihan_diri_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('kebersihan_diri_kasir') ?? $opening->kebersihan_diri_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Perhatikan kebersihan diri sendiri ( tidak bau badan + rambut pendek + disisir rapi )
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="sering_cuci_tangan_server" type="checkbox" value="1" class="colorinput-input" {{ old('sering_cuci_tangan_server') ?? $opening->sering_cuci_tangan_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="sering_cuci_tangan_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('sering_cuci_tangan_kasir') ?? $opening->sering_cuci_tangan_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Cuci tangan sesering mungkin
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="sopan_terhadap_customer_server" type="checkbox" value="1" class="colorinput-input" {{ old('sopan_terhadap_customer_server') ?? $opening->sopan_terhadap_customer_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="sopan_terhadap_customer_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('sopan_terhadap_customer_kasir') ?? $opening->sopan_terhadap_customer_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Prilaku sopan terhadap customer ( sapa + salam pagi/siang/sore/malam )
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="pelayanan_maksimal_server" type="checkbox" value="1" class="colorinput-input" {{ old('pelayanan_maksimal_server') ?? $opening->pelayanan_maksimal_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="pelayanan_maksimal_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('pelayanan_maksimal_kasir') ?? $opening->pelayanan_maksimal_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Pelayanan maksimal ke customer ( mengambil galon kosong & mengantar galon yg sudah terisi ke kendraan )
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="bicara_dan_bercanda_seperlunya_server" type="checkbox" value="1" class="colorinput-input" {{ old('bicara_dan_bercanda_seperlunya_server') ?? $opening->bicara_dan_bercanda_seperlunya_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="bicara_dan_bercanda_seperlunya_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('bicara_dan_bercanda_seperlunya_kasir') ?? $opening->bicara_dan_bercanda_seperlunya_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Bicara seperlunya dan bercanda sepantasnya ( tidak terlihat berlebihan dihadapan customer )
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="makan_minum_di_ruang_yang_tersedia_server" type="checkbox" value="1" class="colorinput-input" {{ old('makan_minum_di_ruang_yang_tersedia_server') ?? $opening->makan_minum_di_ruang_yang_tersedia_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="makan_minum_di_ruang_yang_tersedia_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('makan_minum_di_ruang_yang_tersedia_kasir') ?? $opening->makan_minum_di_ruang_yang_tersedia_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Makan & minum di ruang yg telah disediakan ( tidak di area kerja )
                </label>
            </div>
        </div>
        <div class="row">
           <div class="form-group col-md-1">
            <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="tidak_menggunakan_hp_saat_kerja_server" type="checkbox" value="1" class="colorinput-input" {{ old('tidak_menggunakan_hp_saat_kerja_server') ?? $opening->tidak_menggunakan_hp_saat_kerja_server == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-1">
               <label for="photo_profile" style="visibility: hidden;">Server</label>
                <label class="colorinput">
                    <input name="tidak_menggunakan_hp_saat_kerja_kasir" type="checkbox" value="1" class="colorinput-input" {{ old('tidak_menggunakan_hp_saat_kerja_kasir') ?? $opening->tidak_menggunakan_hp_saat_kerja_kasir == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-10">
                <label for="photo_profile" style="visibility: hidden;">Kasir</label><br>
                <label class="colorinput">
                   Tidak dibenarkan menggunakan HP selama jam kerja ( Kecuali Kepala Toko )
                </label>
            </div>
        </div>
    </div>
    </div>


<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
