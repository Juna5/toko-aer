@extends('operational::layouts.app')

@section('operational::title', 'Opening')

@section('operational::breadcrumb-2')
@include('operational::include.breadcrum', [
'title' => 'Opening',
'active' => true,
'url' => route('operational.opening.index')
])
@endsection
@section('operational::breadcrumb-3')
@include('operational::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('operational.opening.edit', $opening->id)
])
@endsection

@section('operational::content')
<div class="row">
    <div class="container">
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card no-b no-r">
                    <div class="card-body">
                        <h5 class="card-title">
                            <div class="text-right">
                                <a href="{{ route('operational.opening.index') }}" class="btn btn-warning">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </a>
                            </div>
                        </h5>
                        <hr>
                        @include('flash::message')
                        @include('include.error-list')
                        <div class="row">
                            <div class="col-12">
                                <form action="{{ route('operational.opening.update', $opening->id) }}" method="POST" enctype="multipart/form-data">
                                    @method('PUT')
                                    @include('operational::opening.form', [
                                    'submitButtonText' => 'Update'
                                    ])
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('javascript')
<script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        @include('include.dropify-remove-image')
    </script>
@endpush
