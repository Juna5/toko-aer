@extends('operational::layouts.app')

@section('operational::title', 'Opening')

@section('operational::breadcrumb-2')
@include('operational::include.breadcrum', [
'title' => 'Opening',
'active' => true,
'url' => route('operational.opening.index')
])
@endsection

@section('operational::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary">{{ $row->code_number }}
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Employee</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->employee)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Opening Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($row->opening_date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Depot</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->depot_id ? optional($row->depot)->name : '-' }}</td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="col-md-6">
                                         <div class="section-title">Stock</div>
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Meteran In Awal</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_in_awal ?  number_format($row->meteran_in_awal) : '-' }}</td>
                                                    </tr>
                                                    
                                                    <tr>
                                                        <td width="30%">Meteran In Status</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_in_status == 0 ? 'Tidak Sesuai' : 'Sesuai' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran In</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_in ?  number_format($row->meteran_in) : '-' }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Bukti</td>
                                                        <td width="2%">:</td>
                                                        <td>
                                                            <img src="{{ asset($row->getFirstMediaUrl('meteran_in_photo')) }}" style="width:200px;height:200px;">
                                                            <br>
                                                            <span>
                                                                <br>
                                                                    <a href="{{ asset($row->getFirstMediaUrl('meteran_in_photo')) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                                                                        <i class="fa fa-file text-white" ></i> View File
                                                                    </a>
                                                                </span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran Out Awal</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_out_awal ?  number_format($row->meteran_out_awal) : '-' }} </td>
                                                    </tr>
                                                   
                                                    <tr>
                                                        <td width="30%">Meteran In Status</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->meteran_out_status == 0 ? 'Tidak Sesuai' : 'Sesuai' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Meteran In</td>
                                                        <td width="2%">:</td>
                                                        <td> {{ $row->meteran_out ?  number_format($row->meteran_out) : '-' }} </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Bukti</td>
                                                        <td width="2%">:</td>
                                                        <td>
                                                            <img src="{{ asset($row->getFirstMediaUrl('meteran_out_photo')) }}" style="width:200px;height:200px;">
                                                            <br>
                                                            <span>
                                                                <br>
                                                                    <a href="{{ asset($row->getFirstMediaUrl('meteran_out_photo')) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                                                                        <i class="fa fa-file text-white" ></i> View File
                                                                    </a>
                                                                </span>
                                                        </td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                        <div class="row">
                                           <div class="col-md-6">
                                             <div class="section-title">Kas</div>
                                                <div class="table-responsive">
                                                    <table class="table  table-hover table-md">
                                                        <tr>
                                                            <td width="30%">Kas Awal</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ number_format($row->cash_awal) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Kas Status</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ $row->cash_status == 0 ? 'Tidak Sesuai' : 'Sesuai' }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Kas Total</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ number_format($row->cash_amount) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Disaksikan Oleh</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ number_format($row->cash_amount) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Bukti</td>
                                                            <td width="2%">:</td>
                                                            <td>
                                                                <img src="{{ asset($row->getFirstMediaUrl('bukti_uang_photo')) }}" style="width:200px;height:200px;">
                                                                <br>
                                                                <span>
                                                                    <br>
                                                                        <a href="{{ asset($row->getFirstMediaUrl('bukti_uang_photo')) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                                                                            <i class="fa fa-file text-white" ></i> View File
                                                                        </a>
                                                                    </span>
                                                            </td>
                                                        </tr>
                                                        
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-md-6">
                                             <div class="section-title">Kebersihan Toko</div>
                                                <div class="table-responsive">
                                                    <table class="table  table-hover table-md">
                                                        @php
                                                        $check = 'fa-times';
                                                        $check2 = 'fa-times';
                                                        $check3 = 'fa-times';
                                                        if($row->menyapu_halaman_parkir == true){
                                                            $check = 'fa-check';
                                                        }
                                                        if($row->membersihan_toilet == true){
                                                            $check2 = 'fa-check';
                                                        }

                                                        if($row->meyiram_tanaman == true){
                                                            $check3 = 'fa-check';
                                                        }

                                                        @endphp
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $check }}"></i></td>
                                                            
                                                            <td width="80%">Menyapu Halaman Parkir</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $check2 }}"></i></td>
                                                          
                                                            <td width="80%">Membersihan Toilet</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $check3 }}"></i></td>
                                                            <td width="80%">Menyiram Tanaman</td>
                                                        </tr>                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                           <div class="col-md-6">
                                             <div class="section-title">Personal</div>
                                                <div class="table-responsive">
                                                    <table class="table  table-hover table-md">
                                                        <tr>
                                                            <td width="10%">Server</td>
                                                             <td width="10%">Kasir</td>
                                                             <td width="80%"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->seragam_rapi_bersih_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{$row->seragam_rapi_bersih_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Memakai Seragam Rapi & Bersih ( Kaos dimasukan + Jeans Biru + Sepatu Sport )</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->memakai_masker_face_shield_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->memakai_masker_face_shield_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Memakai Masker + Face Shield</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->kebersihan_diri_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->kebersihan_diri_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Perhatikan kebersihan diri sendiri ( tidak bau badan + rambut pendek + disisir rapi )</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->sering_cuci_tangan_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->sering_cuci_tangan_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Cuci tangan sesering mungkin</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->sopan_terhadap_customer_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->sopan_terhadap_customer_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Prilaku sopan terhadap customer ( sapa + salam pagi/siang/sore/malam )</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->pelayanan_maksimal_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->pelayanan_maksimal_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Pelayanan maksimal ke customer ( mengambil galon kosong & mengantar galon yg sudah terisi ke kendraan )</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->bicara_dan_bercanda_seperlunya_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->bicara_dan_bercanda_seperlunya_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Bicara seperlunya dan bercanda sepantasnya ( tidak terlihat berlebihan dihadapan customer )</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->makan_minum_di_ruang_yang_tersedia_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->makan_minum_di_ruang_yang_tersedia_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Makan & minum di ruang yg telah disediakan ( tidak di area kerja )</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10%"><i class="fa {{ $row->tidak_menggunakan_hp_saat_kerja_server == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                             <td width="10%"><i class="fa {{ $row->tidak_menggunakan_hp_saat_kerja_kasir == 'true' ? 'fa-check' : 'fa-times ' }}"></i></td>
                                                            <td width="80%">Tidak dibenarkan menggunakan HP selama jam kerja ( Kecuali Kepala Toko )</td>
                                                        </tr>


                                                        
                                                        
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                            <div class="col-md-3"></div>
                                        </div>
                                    </div>
                                </div>

                           
                                    <hr>
                                      <div class="text-md-right">
                                        <a href="{{ route('operational.opening.report',['from_date' => $from_date,'until_date'=>$until_date]) }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
