@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@yield('operational::title', config('operational.name'))</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></div>
                @yield('operational::breadcrumb-2')
                @yield('operational::breadcrumb-3')
                @yield('operational::breadcrumb-4')
            </div>
        </div>

        <div class="section-body">
            @yield('operational::content')
        </div>
    </section>
@endsection
