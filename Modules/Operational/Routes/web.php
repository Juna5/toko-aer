<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('operational.')->prefix('operational')->middleware(['impersonate', 'auth'])->group(function () {
    Route::get('/', 'OperationalController@view')->name('view');

 		#opening
        Route::resource('opening', 'OpeningController');
        Route::get('opening-report', 'OpeningController@report')->name('opening.report');
        Route::get('opening-detail/{id}', 'OpeningController@reportDetail')->name('opening.report-detail');
        
        #serahterima
        Route::resource('serah-terima', 'SerahTerimaController');
        Route::get('serah-terima-report', 'SerahTerimaController@report')->name('serah-terima.report');
        Route::get('serah-terima-detail/{id}', 'SerahTerimaController@reportDetail')->name('serah-terima.report-detail');
        #closing
    	Route::resource('closing', 'ClosingController');
        Route::get('closing-report', 'ClosingController@report')->name('closing.report');
        Route::get('closing-detail/{id}', 'ClosingController@reportDetail')->name('closing.report-detail');
});
