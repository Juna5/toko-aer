<?php

namespace Modules\Operational\DataTables;

use Modules\Operational\Entities\Opening;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class OpeningDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('employee_id', function ($data) {
                return $data->employee ? optional($data->employee)->name : '-';
            })
            ->editColumn('depot_id', function ($data) {
                return $data->depot ? optional($data->depot)->name : '-';
            })
            ->editColumn('opening_date', function ($data) {
                return format_d_month_y($data->opening_date);
            })
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('operational.opening.edit', [$data->id, 'employeeId' => request('employeeId')]) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('operational.opening.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>';
                return  $edit  .  $delete;
            })
            ->editColumn('active', function ($data) {
                if ($data->active == true) {
                    return '<div class="text-primary mb-2">Active</div>';
                } else {
                    return '<div class="text-danger mb-2">Not Active</div>';
                }
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StockTransactionDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Opening $model)
    {
        return $model->whereNull('deleted_at')->orderBy('opening_date', 'desc')->get()->take(5);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('opening-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            // ->orderBy(1)
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('opening_date')->title('Opening Date')->addClass('text-center'),
            Column::make('employee_id')->title('Opened By')->addClass('text-center'),
            Column::make('depot_id')->title('Depot')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'StockAdjusment_' . date('YmdHis');
    }
}
