<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('depot_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->date('closing_date')->nullable();

            $table->string('meteran_in_awal')->nullable();
            $table->string('meteran_in_akhir')->nullable();
            $table->string('meteran_in')->nullable();

            $table->string('meteran_out_awal')->nullable();
            $table->string('meteran_out_akhir')->nullable();
            $table->string('meteran_out')->nullable();

            $table->string('persediaan_akhir')->nullable();

            $table->boolean('drop_amplop_setoran_bank')->default(true);

            $table->double('cash_amount')->nullable();

            $table->integer('disiapkan_oleh')->nullable();
            $table->integer('disetujui_oleh')->nullable();

            $table->boolean('menyapu_mengepel_lantai')->default(true);
            $table->boolean('lap_etalase_kaca_meja_kasir')->default(true);
            $table->boolean('membersihkan_filter_air')->default(true);
            $table->boolean('menyemprot_disinfektan_diarea_serving')->default(true);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closings');
    }
}
