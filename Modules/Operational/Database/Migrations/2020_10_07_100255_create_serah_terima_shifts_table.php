<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSerahTerimaShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('serah_terima_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('depot_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->date('serah_terima_date')->nullable();

            $table->string('meteran_in_awal')->nullable();
            $table->string('meteran_in_akhir')->nullable();
            $table->string('meteran_in')->nullable();

            $table->string('meteran_out_awal')->nullable();
            $table->string('meteran_out_akhir')->nullable();
            $table->string('meteran_out')->nullable();

            $table->string('persediaan_akhir')->nullable();

            $table->boolean('drop_amplop_setoran_bank')->default(true);

            $table->double('cash_amount')->nullable();

            $table->integer('disiapkan_oleh')->nullable();
            $table->integer('disetujui_oleh')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('serah_terima_shifts');
    }
}
