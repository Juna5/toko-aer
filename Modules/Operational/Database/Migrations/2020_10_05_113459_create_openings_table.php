<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('openings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('depot_id')->nullable();
            $table->integer('employee_id')->nullable();
            $table->date('opening_date')->nullable();

            $table->string('meteran_in_awal')->nullable();
            $table->boolean('meteran_in_status')->default(true);
            $table->string('meteran_in')->nullable();

            $table->string('meteran_out_awal')->nullable();
            $table->boolean('meteran_out_status')->default(true);
            $table->string('meteran_out')->nullable();

            $table->double('cash_awal')->nullable();
            $table->boolean('cash_status')->default(true);
            $table->double('cash_amount')->nullable();
            $table->integer('disaksikan_oleh')->nullable();

            $table->boolean('menyapu_halaman_parkir')->default(true);
            $table->boolean('membersihkan_toilet')->default(true);
            $table->boolean('menyiram_tanaman')->default(true);

            $table->boolean('seragam_rapi_bersih_server')->default(true);
            $table->boolean('seragam_rapi_bersih_kasir')->default(true);

            $table->boolean('memakai_masker_face_shield_server')->default(true);
            $table->boolean('memakai_masker_face_shield_kasir')->default(true);

            $table->boolean('kebersihan_diri_server')->default(true);
            $table->boolean('kebersihan_diri_kasir')->default(true);

            $table->boolean('sering_cuci_tangan_server')->default(true);
            $table->boolean('sering_cuci_tangan_kasir')->default(true);

            $table->boolean('sopan_terhadap_customer_server')->default(true);
            $table->boolean('sopan_terhadap_customer_kasir')->default(true);

            $table->boolean('pelayanan_maksimal_server')->default(true);
            $table->boolean('pelayanan_maksimal_kasir')->default(true);

            $table->boolean('bicara_dan_bercanda_seperlunya_server')->default(true);
            $table->boolean('bicara_dan_bercanda_seperlunya_kasir')->default(true);

            $table->boolean('makan_minum_di_ruang_yang_tersedia_server')->default(true);
            $table->boolean('makan_minum_di_ruang_yang_tersedia_kasir')->default(true);

            $table->boolean('tidak_menggunakan_hp_saat_kerja_server')->default(true);
            $table->boolean('tidak_menggunakan_hp_saat_kerja_kasir')->default(true);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('openings');
    }
}
