<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeteranOutToSerahTerimaShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('serah_terima_shifts', function (Blueprint $table) {
            $table->string('meteran_out_awal2')->nullable();
            $table->string('meteran_out_akhir2')->nullable();
            $table->string('meteran_out2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('serah_terima_shifts', function (Blueprint $table) {
            $table->dropColumn('meteran_out_awal2');
            $table->dropColumn('meteran_out_akhir2');
            $table->dropColumn('meteran_out2');
        });
    }
}
