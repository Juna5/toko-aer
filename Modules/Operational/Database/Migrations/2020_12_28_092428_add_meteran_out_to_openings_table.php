<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMeteranOutToOpeningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('openings', function (Blueprint $table) {
            $table->string('meteran_out_awal2')->nullable();
            $table->boolean('meteran_out_status2')->default(true);
            $table->string('meteran_out2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('openings', function (Blueprint $table) {
            $table->dropColumn('meteran_out_awal2');
            $table->dropColumn('meteran_out_status2');
            $table->dropColumn('meteran_out2');
        });
    }
}
