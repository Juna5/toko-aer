<?php

namespace Modules\Operational\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\HR\Entities\Employee;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Closing extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;
    protected $fillable = [];

    public function depot()
	{
		return $this->belongsTo(WarehouseMaster::class, 'depot_id');
	}

	public function employee()
	{
		return $this->belongsTo(Employee::class, 'employee_id');
	}

	public function disiapkan()
	{
		return $this->belongsTo(Employee::class, 'disiapkan_oleh');
	}

	public function disetujui()
	{
		return $this->belongsTo(Employee::class, 'disetujui_oleh');
	}
}
