<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::name('hr.')->prefix('hr')->middleware(['impersonate', 'auth'])->group(function () {
    Route::name('shift-task.')->prefix('shift-task')->namespace('ShiftTask')->group(function () {
        Route::get('{id}/edit', 'ShiftTaskController@edit')->name('edit');
        Route::put('update/{id}', 'ShiftTaskController@update')->name('update.bro');
        Route::resource('/', 'ShiftTaskController');
    });
    Route::name('master.')->prefix('master')->group(function () {
        Route::get('index', 'HRController@index')->name('index');

        Route::namespace('Master')->group(function () {
            Route::resource('job-level', 'JobLevelController');
            Route::resource('religion', 'ReligionController');
            Route::resource('job-position', 'JobPositionController');
            Route::resource('division', 'DivisionController');
            Route::resource('department', 'DepartmentController');
            Route::resource('leave-type', 'LeaveTypeController');
            Route::resource('cost-center', 'CostCenterController');
            Route::resource('working-hour', 'WorkingHourController');
            Route::resource('location', 'LocationController');
        });
    });


    Route::resource('employee', 'EmployeeController');
    Route::name('setting.')->prefix('setting')->namespace('Setting')->group(function () {
        Route::get('/', 'SettingController@index')->name('setting-index');
        Route::get('shift_setting/get-status-employee', 'ShiftSettingController@getStatusEmployeeShift');
        Route::get('shift_setting/get-calendar/{year}/{mount}', 'ShiftSettingController@getCalendar');
        Route::resource('shift_setting', 'ShiftSettingController');
    });
    Route::name('leave.')->prefix('leave')->namespace('Leave')->group(function () {
        Route::get('view', 'ViewController')->name('view');
        Route::resource('leave-balance', 'LeaveBalanceController');
        Route::resource('entry', 'LeaveController');

        Route::resource('approval', 'LeaveApprovalController');
    });

     Route::resource('attendance_summary_report', 'AttendanceSummaryReportController');
     Route::name('report.')->prefix('report')->namespace('Report')->group(function () {
        Route::get('/', 'ViewReportController@index')->name('index');
        Route::resource('attendance_report', 'AttendanceReportController');
        Route::resource('attendance_report.report', 'AttendanceReportController@report');
        Route::resource('attendance_report.getDataReport', 'AttendanceReportController@getDataReport');
    });
});
