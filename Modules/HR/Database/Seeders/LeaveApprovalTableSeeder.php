<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class LeaveApprovalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add leave approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit leave approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete leave approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view leave approval', 'created_at' => now()],
        ]);
    }
}
