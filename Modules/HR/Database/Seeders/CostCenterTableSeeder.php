<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class CostCenterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add cost center', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit cost center', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete cost center', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view cost center', 'created_at' => now()],
        ]);
    }
}
