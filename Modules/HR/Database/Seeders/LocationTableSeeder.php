<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add location', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit location', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete location', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view location', 'created_at' => now()],
        ]);
    }
}
