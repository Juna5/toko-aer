<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class JobLevelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add job level', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit job level', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete job level', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view job level', 'created_at' => now()],
        ]);
    }
}
