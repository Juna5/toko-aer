<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class JobPositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add job position', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit job position', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete job position', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view job position', 'created_at' => now()],
        ]);
    }
}
