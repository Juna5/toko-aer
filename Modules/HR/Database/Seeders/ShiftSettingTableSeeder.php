<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ShiftSettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add shift setting', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit shift setting', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete shift setting', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view shift setting', 'created_at' => now()],
        ]);
    }
}
