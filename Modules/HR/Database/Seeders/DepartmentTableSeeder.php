<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add department', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit department', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete department', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view department', 'created_at' => now()],
        ]);
    }
}
