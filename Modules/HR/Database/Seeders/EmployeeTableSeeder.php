<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class EmployeeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add employee', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit employee', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete employee', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view employee', 'created_at' => now()],
        ]);
    }
}
