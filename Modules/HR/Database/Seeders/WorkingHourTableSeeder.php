<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class WorkingHourTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add working hour', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit working hour', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete working hour', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view working hour', 'created_at' => now()],
        ]);
    }
}
