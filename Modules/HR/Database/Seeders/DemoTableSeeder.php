<?php

namespace Modules\HR\Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\Division;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\JobLevel;
use Modules\HR\Entities\JobPosition;
use Modules\HR\Entities\LeaveType;
use Modules\HR\Entities\Religion;
use Faker\Factory as Faker;
use Modules\Setting\Entities\UserType;

class DemoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $faker = Faker::create('id_ID');

        Division::insert([
            'id' => '1','name' => 'Academic / Non Academic', 'description' => 'Academic / Non Academic'
        ]);

        $departments = ['MARKETING','PURCHASING', 'Administration', 'IT'];
        $positions = ['Administration Manager', 'Administration Staff', 'Business Management And Social Sciences Coordinator', 'Finance And Accounting Manager', 'Programmer'];
        $levels = ['Guru','Staff', 'Manager', 'Supervisor/Coordinator', 'Kepala Sekolah'];
        $leaveType = ['Cuti tahunan', 'Cuti menikah', 'Cuti menikahkan anak', 'Cuti khitan anak','Cuti baptis anak', 'Cuti melahirkan',
            'Cuti keguguran', 'Cuti anggota meninggal (kandung)', 'Cuti anggota keluarga tidak kandung meninggal', 'Izin keluar'
        ];
        $religions = ['Islam','Kristen','Katolik', 'Hindu', 'Buddha', 'Konghucu'];
        $userTypes = ['staff','teacher'];

        foreach ($departments as $department){
            Department::create([
                'name' => $department, 'description' => $department
            ]);
        }

        foreach ($positions as $position){
            JobPosition::create([
                'name' => $position, 'description' => $position
            ]);
        }

        foreach ($levels as $level){
            JobLevel::create([
                'name' => $level, 'description' => $level
            ]);
        }

        foreach ($leaveType as $type){
            LeaveType::create([
                'name' => $type, 'description' => $type
            ]);
        }

        foreach ($religions as $religion){
            Religion::create([
                'name' => $religion, 'description' => $religion
            ]);
        }

        foreach ($userTypes as $userType){
            UserType::create([
                'name' => $userType, 'description' => $userType
            ]);
        }

        for ($i = 0; $i < 100; $i++) {
            $user = new User;
            $user->name = $faker->name;
            $user->username = mt_rand(100000, 999999);
            $user->email = $faker->email;
            $user->password = bcrypt('keyboard');
            $user->user_type_id = UserType::inRandomOrder()->first()->id;
            $user->active = true;
            $user->save();

            $employee = new Employee();
            $employee->department_id = Department::first()->id;
            $employee->division_id = Division::inRandomOrder()->first()->id;
            $employee->religion_id = Religion::inRandomOrder()->first()->id;
            $employee->job_level_id = JobLevel::inRandomOrder()->first()->id;
            $employee->position_id = JobPosition::inRandomOrder()->first()->id;
            $employee->user_id = $user->id;
            $employee->nik = $user->username;
            $employee->name = $user->name;
            $employee->email = $user->email;
            $employee->gender = $faker->randomElement(['female', 'male']);
            $employee->is_local_or_expat = 'local';
            $employee->is_teacher_of_staff = $faker->randomElement(['teacher', 'staff']);
            $employee->marital_status = $faker->randomElement(['Single', 'Married']);
            $employee->active = true;
            $employee->save();
        }
    }
}
