<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class DivisionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add division', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit division', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete division', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view division', 'created_at' => now()],
        ]);
    }
}
