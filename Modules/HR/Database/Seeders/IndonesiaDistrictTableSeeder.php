<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IndonesiaDistrictTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $file = file_get_contents(base_path('Modules/HR/Database/Seeders/data/districts.txt'));
        $districts = unserialize($file);
        DB::table('districts')->insert($districts);
    }
}
