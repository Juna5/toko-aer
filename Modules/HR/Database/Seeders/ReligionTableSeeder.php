<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class ReligionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add religion', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit religion', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete religion', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view religion', 'created_at' => now()],
        ]);
    }
}
