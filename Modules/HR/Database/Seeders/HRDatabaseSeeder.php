<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\HR\Entities\JobPosition;

class HRDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(EmployeeTableSeeder::class);
        $this->call(JobLevelTableSeeder::class);
        $this->call(ReligionTableSeeder::class);
        $this->call(JobPositionTableSeeder::class);
        $this->call(DivisionTableSeeder::class);
        $this->call(DepartmentTableSeeder::class);
        $this->call(IndonesiaProvinceTableSeeder::class);
        $this->call(IndonesiaRegencyTableSeeder::class);
        $this->call(IndonesiaDistrictTableSeeder::class);
        $this->call(LeaveTypeTableSeeder::class);
        $this->call(LeaveBalanceTableSeeder::class);
        $this->call(LeaveTableSeeder::class);
        $this->call(LeaveApprovalTableSeeder::class);
        $this->call(CostCenterTableSeeder::class);
        $this->call(ShiftSettingTableSeeder::class);
        $this->call(SettingTableSeeder::class);
        $this->call(WorkingHourTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(ShiftTaskTableSeeder::class);

        // $this->call(DemoTableSeeder::class);
    }
}
