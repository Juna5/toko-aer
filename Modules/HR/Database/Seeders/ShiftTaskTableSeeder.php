<?php

namespace Modules\HR\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ShiftTaskTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add shift task', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit shift task', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete shift task', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view shift task', 'created_at' => now()],
        ]);
    }
}
