<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShiftTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id')->nullable();
            $table->integer('shift_id')->nullable();
            $table->date('date')->nullable();
            $table->string('time')->nullable();
            $table->integer('location_id')->nullable();
            $table->string('angka_meteran_air')->nullable();
            $table->string('serah_terima_tunai')->nullable();
            $table->string('serah_terima_non_tunai')->nullable();
            $table->string('total')->nullable();
            $table->boolean('is_seragam')->nullable()->default(false);
            $table->boolean('is_kebersihan')->nullable()->default(false);
            $table->boolean('is_pelanggan')->nullable()->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift_tasks');
    }
}
