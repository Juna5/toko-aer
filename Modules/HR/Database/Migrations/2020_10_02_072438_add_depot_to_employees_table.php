<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDepotToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->integer('depot_id')->nullable();
            $table->string('nama_kontak_darurat')->nullable();
            $table->string('status_kontak_darurat')->nullable();
            $table->string('nomor_kontak_darurat_1')->nullable();
            $table->string('nomor_kontak_darurat_2')->nullable();
            $table->string('email_kontak_darurat')->nullable();
            $table->text('alamat_kontak_darurat')->nullable();
            $table->string('kota_kontak_darurat')->nullable();
            $table->string('provinsi_kontak_darurat')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
             $table->dropColumn('depot_id');
             $table->dropColumn('nama_kontak_darurat');
             $table->dropColumn('status_kontak_darurat');
             $table->dropColumn('nomor_kontak_darurat_1');
             $table->dropColumn('nomor_kontak_darurat_2');
             $table->dropColumn('email_kontak_darurat');
             $table->dropColumn('alamat_kontak_darurat');
             $table->dropColumn('kota_kontak_darurat');
             $table->dropColumn('provinsi_kontak_darurat');
        });
    }
}
