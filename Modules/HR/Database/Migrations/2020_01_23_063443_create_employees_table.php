<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('division_id')->nullable();
            $table->integer('religion_id')->nullable();
            $table->integer('job_level_id')->nullable();
            $table->integer('position_id')->nullable();
            $table->integer('coordinator_id')->nullable();
            $table->integer('province_id')->nullable();
            $table->integer('regency_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->string('nik');
            $table->string('name');
            $table->string('email')->nullable();
            $table->enum('gender', ['female', 'male'])->nullable();
            $table->string('place_of_birth')->nullable();
            $table->date('dob')->nullable();
            $table->enum('is_local_or_expat', ['local', 'expat'])->nullable();
            $table->string('ktp')->nullable();
            $table->string('passport_no')->nullable();
            $table->date('passport_expired_date')->nullable();
            $table->string('npwp')->nullable();
            $table->date('hired_date')->nullable();
            $table->enum('is_employment_status', ['permanent', 'contract'])->nullable();
            $table->string('phone_number')->nullable();
            $table->string('mobile_number')->nullable();
            $table->text('address')->nullable();
            $table->text('address_ktp')->nullable();
            $table->string('marital_status')->nullable();
            $table->boolean('resign')->default(false);
            $table->date('contract_period_from')->nullable();
            $table->date('contract_period_until')->nullable();
            $table->boolean('active')->default(1);
            $table->string('working_hour')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
