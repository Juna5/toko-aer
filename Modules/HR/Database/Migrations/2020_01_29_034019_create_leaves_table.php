<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('employee_id');
            $table->integer('leave_type_id');
            $table->integer('leave_balance_id')->nullable();
            $table->integer('approver_to_go')->nullable();
            $table->integer('latest_approver')->nullable();
            $table->string('role_approval')->nullable();
            $table->string('code_number')->nullable();
            $table->date('from_date')->nullable();
            $table->date('until_date')->nullable();
            $table->string('from_hour')->nullable();
            $table->string('until_hour')->nullable();
            $table->text('reason')->nullable();
            $table->text('address')->nullable();
            $table->string('arrangement_cover')->nullable();
            $table->enum('status', ['waiting', 'approved', 'rejected']);
            $table->boolean('leaving_indonesia')->default(false);
            $table->boolean('discretion')->default(false);
            $table->text('rejected_reason')->nullable();
            $table->integer('rejected_by')->nullable();
            $table->date('rejected_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
