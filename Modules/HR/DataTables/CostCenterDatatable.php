<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\CostCenter;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CostCenterDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('active', function ($row) {
                return $row->active == 1 ? "Active" : "Inactive";
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('hr.master.cost-center.edit', $row->id) . '" class="btn btn-primary"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('hr.master.cost-center.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view cost center') ? $edit : '') . (userCan('delete cost center') ? $delete : '');
            });
    }

    public function query(CostCenter $model)
    {
        return $model->select('cost_centers.*');
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                ['extend' => 'print', 'className' => 'btn xs default mb-2', 'text' => '<i class="fa fa-print"></i>'],
                ['extend' => 'excel', 'className' => 'btn xs default mb-2', 'text' => '<i class="fa fa-file-excel"></i>'],
                ['extend' => 'reload', 'className' => 'btn xs default mb-2', 'text' => '<i class="fa fa-sync"></i>'],

            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            Column::make('cost_center_number')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),
            Column::make('active')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'CostCenter_' . date('YmdHis');
    }
}
