<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\Location;
use Modules\HR\Entities\Division;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\JobLevel;
use Modules\HR\Entities\Religion;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class LocationDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.master.location.edit', [$data->id, 'employeeId' => request('employeeId')]) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.master.location.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>';
                return (userCan('view location') ? $edit : '') . (userCan('delete location') ? $delete : '');
            })
            ->editColumn('active', function ($data) {
                if ($data->active == true) {
                    return '<div class="text-primary mb-2">Active</div>';
                } else {
                    return '<div class="text-danger mb-2">Not Active</div>';
                }
            })
            ->rawColumns(['action', 'active']);
    }

    public function query(Location $model)
    {
        return $model->all();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('locationdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
