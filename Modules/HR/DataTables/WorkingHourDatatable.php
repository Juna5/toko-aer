<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\WorkingHour;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class WorkingHourDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('active', function ($row) {
                return $row->active == 1 ? "Active" : "Inactive";
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('hr.master.working-hour.edit', $row->id) . '" class="btn btn-primary"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('hr.master.working-hour.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view working hour') ? $edit : '') . (userCan('delete working hour') ? $delete : '');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\WorkingHourDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(WorkingHour $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                ['extend' => 'print', 'className' => 'btn xs default mb-2', 'text' => '<i class="fa fa-print"></i>'],
                ['extend' => 'excel', 'className' => 'btn xs default mb-2', 'text' => '<i class="fa fa-file-excel"></i>'],
                ['extend' => 'reload', 'className' => 'btn xs default mb-2', 'text' => '<i class="fa fa-sync"></i>'],

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            Column::make('time_from')->addClass('text-center'),
            Column::make('time_until')->addClass('text-center'),
            Column::make('active')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'WorkingHour_' . date('YmdHis');
    }
}
