<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\ShiftTask;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ShiftTaskDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('location_id', function ($row) {
                return optional($row->location)->name;
            })
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date) . ' - '. $row->time;
            })
            ->editColumn('angka_meteran_air', function ($row) {
                return number_format($row->angka_meteran_air);
            })
            ->editColumn('serah_terima_tunai', function ($row) {
                return number_format($row->serah_terima_tunai);
            })
            ->editColumn('serah_terima_non_tunai', function ($row) {
                return number_format($row->serah_terima_non_tunai);
            })
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.shift-task.edit', $data->id) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.shift-task.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash text-white" aria-hidden="true"></i>
                            </a>';
                return (userCan('view shift task') ? $edit : '') . (userCan('delete shift task') ? $delete : '');
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EmployeeDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ShiftTask $model)
    {
        return $model->with(['employee', 'location'])
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('shifttaskdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('location_id')->addClass('text-center')->title('Location'),
            Column::make('date')->addClass('text-center'),
            Column::make('angka_meteran_air')->addClass('text-right'),
            Column::make('serah_terima_tunai')->addClass('text-right')->title('Serah Terima Uang Kas Tunai'),
            Column::make('serah_terima_non_tunai')->addClass('text-right')->title('Serah Terima Uang Kas Non Tunai'),
            Column::make('total')->addClass('text-right'),

            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
