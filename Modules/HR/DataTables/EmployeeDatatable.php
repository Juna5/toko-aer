<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class EmployeeDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('depot', function ($data) {
                return $data->depot ? optional($data->depot)->name : '-';
            })
            ->editColumn('position', function ($data) {
                return $data->position ? optional($data->position)->name : '-';
            })
            ->editColumn('status', function ($data) {
                $teacher = '<div class="text-warning mb-2">Teacher</div>';
                $staff = '<div class="text-primary mb-2">Staff</div>';

                if (empty($data->is_teacher_of_staff)) {
                    return '-';
                } elseif ($data->is_teacher_of_staff == 'teacher') {
                    return $teacher;
                } else {
                    return $staff;
                }
            })
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.employee.edit', $data->id) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.employee.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash text-white" aria-hidden="true"></i>
                            </a>';
                return (userCan('view employee') ? $edit : '') . (userCan('delete employee') ? $delete : '');
            })
            ->editColumn('active', function ($data) {
                if ($data->active == true) {
                    return '<div class="text-primary mb-2">Active</div>';
                } else {
                    return '<div class="text-danger mb-2">Not Active</div>';
                }
            })
            ->rawColumns(['action', 'active', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EmployeeDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Employee $model)
    {
        return $model->withoutGlobalScope('active')
            ->with(['department', 'position'])
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('employeedatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('nik')->addClass('text-center'),
            Column::make('name')->addClass('text-center'),
            // Column::make('department')->addClass('text-center'),
            Column::make('position')->addClass('text-center'),
            Column::make('depot')->addClass('text-center'),
            Column::make('active')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
