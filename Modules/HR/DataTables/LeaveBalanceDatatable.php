<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\LeaveBalance;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class LeaveBalanceDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('name', function ($data) {
                return optional($data->employee)->name;
            })
            ->editColumn('leave_type', function ($data) {
                return optional($data->leaveType)->name;
            })
            ->editColumn('from_date', function ($data) {
                return format_d_month_y($data->from_date);
            })
            ->editColumn('until_date', function ($data) {
                return format_d_month_y($data->until_date);
            })
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.leave.leave-balance.edit', $data->id) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.leave.leave-balance.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash text-white" aria-hidden="true"></i>
                            </a>';
                return (userCan('view leave balance') ? $edit : '') . (userCan('delete leave balance') ? $delete : '');
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EmployeeDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(LeaveBalance $model)
    {
        return $model->with(['employee', 'leaveType'])
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('employeedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            Column::make('leave_type')->addClass('text-center'),
            Column::make('from_date')->addClass('text-center'),
            Column::make('until_date')->addClass('text-center'),
            Column::make('beginning_balance')->addClass('text-center'),
            Column::make('used_balance')->addClass('text-center'),
            Column::make('remaining_balance')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
