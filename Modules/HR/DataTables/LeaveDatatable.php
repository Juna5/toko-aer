<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Leave;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class LeaveDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('leave_type', function ($data) {
                return $data->leaveType ? optional($data->leaveType)->name : '-';
            })
            ->editColumn('approver_to_go', function ($row) {
                return $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval);
            })
            ->editColumn('latest_approver', function ($data) {
                return $data->latestApprove ? optional($data->latestApprove)->name : '-';
            })
            ->editColumn('from_date', function ($data) {
                return format_d_month_y($data->from_date);
            })
            ->editColumn('until_date', function ($data) {
                return format_d_month_y($data->until_date);
            })
            ->editColumn('status', function ($data) {
                $waiting = '<span class="badge badge-success">Waiting</span>';
                $rejected = '<span class="badge badge-danger">Rejected</span>';
                $approved = '<span class="badge badge-info">Approved</span>';

                if (empty($data->status)){
                    return '-';
                } elseif ($data->status == 'waiting') {
                    return $waiting;
                } elseif ($data->status == 'approved') {
                    return $approved;
                }elseif ($data->status == 'rejected') {
                    return $rejected;
                }
            })
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.leave.entry.edit', $data->id) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.leave.entry.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash text-white" aria-hidden="true"></i>
                            </a>';

                $resubmit  = '<a href="' . route('hr.leave.entry.edit', [$data->id, 'resubmit' => true]) . '"
                                class="btn btn-xs btn-warning">
                                <i class="fa fa-redo-alt" aria-hidden="true"></i>
                            </a>';

                if ($data->status == 'waiting') {
                    return (userCan('view leave') ? $edit : '') . (userCan('delete leave') ? $delete : '');
                } elseif ($data->status == 'approved') {
                    return null;
                }elseif ($data->status == 'rejected') {
                    return (userCan('delete leave') ? $resubmit : '') . (userCan('delete leave') ? $delete : '');
                }
            })
            ->rawColumns(['action', 'active', 'status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\EmployeeDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Leave $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        return $model
            ->with(['leaveType', 'approveToGo'])
            ->where('employee_id', $employee->id)
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('employeedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->addClass('text-center'),
            Column::make('leave_type')->addClass('text-center'),
            Column::make('from_date')->addClass('text-center'),
            Column::make('until_date')->addClass('text-center'),
            Column::make('approver_to_go')->addClass('text-center'),
            Column::make('latest_approver')->addClass('text-center'),
            Column::make('status')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
