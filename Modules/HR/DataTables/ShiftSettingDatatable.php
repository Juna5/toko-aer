<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\ShiftSetting;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ShiftSettingDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('employee_id', function ($row) {
                return optional($row->employee)->nik.' - '.optional($row->employee)->name;
            })
            ->editColumn('calendar_id', function ($row) {
                return format_d_month_y(optional($row->calendar)->date);
            })
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.setting.shift_setting.edit', $data->id) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.setting.shift_setting.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>';
                return (userCan('view job position') ? $edit : '') . (userCan('delete job position') ? $delete : '');
            })

            ->rawColumns(['action']);
    }

    public function query(ShiftSetting $model)
    {
        $from_date = $this->from_date;
        $until_date = $this->until_date;
        $employee = $this->employee;

        return $model
        ->when($from_date, function($d) use($from_date) {
            $d->whereHas('calendar', function($c) use ($from_date) {
                $c->whereDate('date', '>=', $from_date);
            });
        })
        ->when($until_date, function($d) use($until_date) {
            $d->whereHas('calendar', function($c) use ($until_date) {
                $c->whereDate('date', '<=', $until_date);
            });
        })
        ->when($employee, function($e) use($employee) {
            $e->where('employee_id', $employee);
        })
        ->whereNull('deleted_at')->get();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('employeedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        // Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('employee_id')->title('Name')->addClass('text-center'),
            Column::make('calendar_id')->title('Calendar')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
