<?php

namespace Modules\HR\DataTables;

use Modules\HR\Entities\Employee;
use Modules\HR\Entities\JobLevel;
use Modules\HR\Entities\LeaveType;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class LeaveTypeDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('action', function ($data) {
                $edit  = '<a href="' . route('hr.master.leave-type.edit', [$data->id, 'employeeId' => request('employeeId')]) . '"
                                class="btn btn-xs btn-info">
                                <i class="fa fa-edit" aria-hidden="true"></i>
                            </a>';
                $delete = '<a data-href="' . route('hr.master.leave-type.destroy', $data->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                class="btn btn-xs btn-danger" style="margin-left: 10px">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </a>';
                return (userCan('view leave type') ? $edit : '') . (userCan('delete leave type') ? $delete : '');
            })
            ->editColumn('active', function ($data) {
                if ($data->active == true){
                    return '<div class="text-primary mb-2">Active</div>';
                } else {
                    return '<div class="text-danger mb-2">Not Active</div>';
                }
            })
            ->rawColumns(['action', 'active']);
    }

    public function query(LeaveType $model)
    {
        return $model->withoutGlobalScope('active')->select('leave_types.*');
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('employeedatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),
            Column::make('active')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Employee_' . date('YmdHis');
    }
}
