@extends('hr::layouts.app')

@section('hr::title', 'Shift Setting')

@section('hr::breadcrumb-2')
@include('hr::include.breadcrum', [
'title' => 'Setting',
'active' => true,
'url' => route('hr.setting.setting-index')
])
@endsection

@section('hr::breadcrumb-3')
@include('hr::include.breadcrum', [
'title' => 'Shift Setting',
'active' => true,
'url' => route('hr.setting.shift_setting.index')
])
@endsection

@section('hr::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <form action="{{ route('hr.setting.shift_setting.index') }}" method="GET">
                    <div class="form-row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">From Date:</label>
                                <input type="date" class="form-control m-input datepicker" id="from_date" name="from_date" value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">To Date:</label>
                                <input type="date" class="form-control m-input datepicker" id="until_date" name="until_date" value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Employee:</label>
                                <select class="form-control m-input select2" name="employee" id="employee">
                                    @foreach($employees as $row)
                                        @if(old('employee'))
                                            <option value="{{ $row->id }}" {{ $row->id == old('employee') ? 'selected' : '' }}>{{$row->name}} {{ $row->depot ? "(". optional($row->depot)->name . ")" : ""}}</option>
                                        @else
                                            <option value="{{ $row->id }}" {{ $row->id == request()->employee ? 'selected' : '' }}>{{$row->name}} {{ $row->depot ? "(". optional($row->depot)->name . ")" : ""}}</option>
                                        @endif

                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning fa-pull-right">Search</button>
                   
                </form>
                <div class="text-left">
                    <a href="{{ route('hr.setting.shift_setting.index') }}" class="btn btn-success">
                        <i class="fa fa-refresh" aria-hidden="true"></i> Refresh
                    </a>
                </div>
                <br>
                <br>
                    <div class="form-group pb-1">
                        <a href="{{ route('hr.setting.shift_setting.create') }}" class="btn btn-primary " title="Create New Shift Setting" data-placement="left" >
                            Create New
                        </a>
                    </div>
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
