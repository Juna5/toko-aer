@extends('hr::layouts.app')

@section('hr::title', 'Shift Setting')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Setting',
    'active' => true,
    'url' => route('hr.setting.setting-index')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Shift Setting',
    'active' => true,
    'url' => route('hr.setting.shift_setting.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            @include('flash::message')
                            <create-shift-setting>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
