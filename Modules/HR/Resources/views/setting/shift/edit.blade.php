@extends('hr::layouts.app')

@section('hr::title', 'Edit Shift Setting')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Setting',
    'active' => true,
    'url' => route('hr.setting.setting-index')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Shift Setting',
    'active' => true,
    'url' => route('hr.setting.shift_setting.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            @include('flash::message')
                            <form action="{{ route('hr.setting.shift_setting.update', $row->id) }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')

                                <div class="table-responsive">
                                    <a href="{{ route('hr.setting.shift_setting.index') }}" class="btn btn-info fa-pull-right m-1">Back</a>
                                    <button type="submit" class="btn btn-success fa-pull-right m-1">Save</button>
                                    <br><br><br>
                                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                                        <tr style="background-color:white ; color: grey" align="center">
                                            <th>Date</th>
                                            <th>Day</th>
                                            <th>{{ $row->description }}</th>
                                        </tr>
                                        <tr>
                                            <td>{{ $row->calendar->date }}</td>
                                            <td>{{ $row->calendar->days_of_week }}</td>
                                            <td>
                                                <select name="employee_id"  class="form-control select2" >
                                                    <option value=""></option>
                                                    @foreach ($employees as $employe)
                                                    <option value="{{ $employe->id }}" {{ $employe->id == $row->employee_id ? 'selected': '' }}>{{ $employe->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
