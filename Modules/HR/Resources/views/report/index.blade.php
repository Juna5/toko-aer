@extends('setting::layouts.master')

@section('setting::title', 'Report')

@section('setting::breadcrumb-2')
    @include('setting::include.breadcrumb', [
        'title' => 'Report',
        'active' => true,
        'url' => route('hr.report.index')
    ])
@endsection

@section('setting::content')
    <div class="section-body">
        <h2 class="section-title">Overview</h2>
        <p class="section-lead">
            HR -> Report Section
        </p>
        @include('flash::message')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-body">
                        <h4>Attendance Report</h4>
                        <p>Attendance Report List for Employee</p>
                            <a href="{{ route('hr.report.attendance_report.index') }}" class="card-cta">Go to Attendance Report <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-body">
                        <h4>Attendance Summary Report</h4>
                        <p>Attendance Summary Report List for Employee</p>
                            <a href="{{ route('hr.attendance_summary_report.index') }}" class="card-cta">Go to Attendance Summary Report <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
