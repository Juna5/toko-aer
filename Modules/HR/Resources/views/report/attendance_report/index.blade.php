@extends('hr::layouts.app')

@section('hr::title', 'Employee Attendance Report')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Report',
    'active' => true,
    'url' => route('hr.report.attendance_report.index')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Employee Attendance Report',
    'active' => true,
    'url' => route('hr.report.attendance_report.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <form action="{{ route('hr.report.attendance_report.index') }}" method="GET">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label>Employee</label>
                                        <select name="employee" id="employee" class="form-control select2" required>
                                            <option value=""></option>
                                            @foreach ($employee as $emp)
                                                <option value="{{ $emp->id }}"@if (request('employee') == $emp->id) {{ 'selected' }} @endif>{{ $emp->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label class="">Month</label>
                                        <select id="month" name="month" class="form-control select2" required>
                                            <option value="">--Please Select--</option>
                                            <option value="Jan" {{ request()->month == 'Jan' ? 'selected' : ''}}>Jan
                                            </option>
                                            <option value="Feb" {{ request()->month == 'Feb' ? 'selected' : ''}}>Feb
                                            </option>
                                            <option value="Mar" {{ request()->month == 'Mar' ? 'selected' : ''}}>Mar
                                            </option>
                                            <option value="Apr" {{ request()->month == 'Apr' ? 'selected' : ''}}>Apr
                                            </option>
                                            <option value="May" {{ request()->month == 'May' ? 'selected' : ''}}>May
                                            </option>
                                            <option value="Jun" {{ request()->month == 'Jun' ? 'selected' : ''}}>Jun
                                            </option>
                                            <option value="Jul" {{ request()->month == 'Jul' ? 'selected' : ''}}>Jul
                                            </option>
                                            <option value="Aug" {{ request()->month == 'Aug' ? 'selected' : ''}}>Aug
                                            </option>
                                            <option value="Sep" {{ request()->month == 'Sep' ? 'selected' : ''}}>Sep
                                            </option>
                                            <option value="Oct" {{ request()->month == 'Oct' ? 'selected' : ''}}>Oct
                                            </option>
                                            <option value="Nov" {{ request()->month == 'Nov' ? 'selected' : ''}}>Nov
                                            </option>
                                            <option value="Dec" {{ request()->month == 'Dec' ? 'selected' : ''}}>Dec
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Year</label>
                                        <select id="year" name="year" class="form-control select2">
                                            <option value=""></option>
                                            <option value="2020" {{ request('year') == '2020' ? 'selected' : '' }}>2020</option>
                                            <option value="2021" {{ request('year') == '2021' ? 'selected' : '' }}>2021</option>
                                            <option value="2022" {{ request('year') == '2022' ? 'selected' : '' }}>2022</option>
                                            <option value="2023" {{ request('year') == '2023' ? 'selected' : '' }}>2023</option>
                                            <option value="2024" {{ request('year') == '2024' ? 'selected' : '' }}>2024</option>
                                            <option value="2025" {{ request('year') == '2025' ? 'selected' : '' }}>2025</option>
                                        </select>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                            </form>
                            <br><br><br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                                    <tr style="background-color:white ; color: grey" align="center">
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Day</th>
                                        <th>Time In</th>
                                        <th>Time Out</th>
                                        <th>Late</th>
                                        <th>Latitude</th>
                                        <th>Longitude</th>
                                        <!-- <th>Overtime</th> -->
                                    </tr>
                                    @forelse($calendars as $index => $calendar)
                                    @php
                                        $time_in = getStaffAttendanceStatus(request('employee'), $calendar->id)->time_in ?? null;
                                        $time_out = getStaffAttendanceStatus(request('employee'), $calendar->id)->time_out ?? null;

                                        if ($time_in ==  "" or $time_out == ""){
                                            $bg = "#ffb0ab";
                                            $text = "white";
                                        }else{
                                            $bg = "";
                                            $text = "";
                                        }

                                        if ($time_in == "" && $time_out == ""){
                                            $bg = "";
                                            $text = "";
                                        }

                                        if($calendar->is_working_day == true){
                                            if ($time_in == "" &&  $time_out == ""){
                                                $bg = "#ffb0ab";
                                                $text = "white";
                                            }
                                        }else{
                                            if ($time_in == "" &&  $time_out == ""){
                                                $bg = "";
                                                $text = "";
                                            }
                                        }
                                        $latitude = Modules\HR\Entities\StaffAttendance::where('calendar_id', $calendar->id)->where('employee_id', request('employee'))->first();

                                    @endphp
                                        <tr align="center" style="background-color:{{ $bg  }};color:{{ $text }};">
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ format_d_month_y($calendar->date) }}</td>
                                            <td>{{ $calendar->days_of_week }}</td>
                                            <td>{{ getStaffAttendanceStatus(request('employee'), $calendar->id)->time_in ?? null }}</td>
                                            <td>{{ getStaffAttendanceStatus(request('employee'), $calendar->id)->time_out ?? null }}</td>
                                            <td style="color:red;">{{ getStaffAttendanceStatus(request('employee'), $calendar->id)->late ?? null }}</td>
                                            <td>{{ $latitude->latitude ?? '' }}</td>
                                            <td>{{ $latitude->longitude ?? '' }}</td>
                                            <!-- <td>{{ getStaffAttendanceStatus(request('employee'), $calendar->id)->overtime ?? null }}</td> -->
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="8" class="text-center">Data not found</td>
                                        </tr>
                                    @endforelse
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
