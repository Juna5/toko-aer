@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $workingHour->name }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Time From</label>
            <input type="text" class="form-control r-0 light s-12 clockPicker" name="time_from" value="{{ old('time_from') ?? $workingHour->time_from }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Time Until</label>
            <input type="text" class="form-control r-0 light s-12 clockPicker" name="time_until" value="{{ old('time_until') ?? $workingHour->time_until }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $workingHour->description }}</textarea>
        </div>
        <div class="form-group">
            <div class="control-label">Active</div>
            <div class="custom-switches-stacked mt-2">
                <label class="custom-switch">
                    <input type="radio" name="active" value="1" required class="custom-switch-input" {{ old('active') ?? $workingHour->active == "1" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Active</span>
                </label>
                <label class="custom-switch">
                    <input type="radio" name="active" value="0" required class="custom-switch-input" {{ old('active') ?? $workingHour->active == "0" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Inactive</span>
                </label>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>