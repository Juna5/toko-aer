@extends('hr::index')

@section('hr::master')
<form id="setting-form">
    <div class="card" id="settings-card">
        <div class="card-header">
            <h4>Cost Centre</h4>
        </div>
        @include('flash::message')

        <div class="card-header">
            <h4>

            </h4>
            <div class="card-header-form">
                <a href="{{ route('hr.master.cost-center.create') }}" class="btn btn-sm btn-info pd-x-15 btn-white btn-uppercase">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush