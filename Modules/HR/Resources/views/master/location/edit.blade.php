@extends('hr::layouts.app')

@section('hr::title', 'Edit Location')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Location',
    'active' => true,
    'url' => route('hr.master.location.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Edit',
    'active' => true,
    'url' => route('hr.master.location.edit', $location->id)
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.master.location.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.master.location.update', $location->id) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @include('hr::master.location.form', [
                                            'submitButtonText' => 'Update'
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
