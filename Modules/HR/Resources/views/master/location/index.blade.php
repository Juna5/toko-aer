@extends('hr::index')

@section('hr::master')
<form id="setting-form">
    <div class="card" id="settings-card">
        <div class="card-header">
            <h4>Location</h4>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
