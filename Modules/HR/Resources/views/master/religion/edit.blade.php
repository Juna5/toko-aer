@extends('hr::layouts.app')

@section('hr::title', 'Edit Religion')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Religion',
    'active' => true,
    'url' => route('hr.master.religion.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Edit',
    'active' => true,
    'url' => route('hr.master.religion.edit', $religion->id)
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.master.religion.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.master.religion.update', $religion->id) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @include('hr::master.religion.form', [
                                            'submitButtonText' => 'Update'
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection