@extends('hr::layouts.app')

@section('hr::title', 'Edit Job Level')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Job Level',
    'active' => true,
    'url' => route('hr.master.job-level.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Edit',
    'active' => true,
    'url' => route('hr.master.job-level.edit', $jobLevel->id)
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.master.job-level.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.master.job-level.update', $jobLevel->id) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @include('hr::master.job_level.form', [
                                            'submitButtonText' => 'Update'
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection