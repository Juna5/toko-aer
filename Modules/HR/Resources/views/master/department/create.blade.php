@extends('hr::layouts.app')

@section('hr::title', 'Add Department')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Department',
    'active' => true,
    'url' => route('hr.master.department.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Add',
    'active' => true,
    'url' => route('hr.master.department.create')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.master.department.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.master.department.store') }}" method="POST" enctype="multipart/form-data">
                                        @include('hr::master.department.form', [
                                            'department' => new \Modules\HR\Entities\Department
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection