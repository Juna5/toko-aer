@extends('hr::layouts.app')

@section('hr::title', 'Edit Employee')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Employee',
    'active' => true,
    'url' => route('hr.employee.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Edit',
    'active' => true,
    'url' => route('hr.employee.edit', $employee->id)
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.employee.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.employee.update', $employee->id) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @include('hr::employee.form', [
                                            'submitButtonText' => 'Update'
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
        $('#select-province').on('change', function(){
            $.post('{{ url('api/region/data') }}', {type: 'regencies', id: $('#select-province').val()}, function(e){
                $('#select-regency').html(e);
            });
            $('#select-district').html('');
        });
        $('#select-regency').on('change', function(){
            $.post('{{ url('api/region/data') }}', {type: 'districts', id: $('#select-regency').val()}, function(e){
                $('#select-district').html(e);
            });
        });
    </script>
@endpush