@extends('hr::layouts.app')

@section('hr::title', 'Employee')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Employee',
    'active' => true,
    'url' => route('hr.employee.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush