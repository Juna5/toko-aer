@csrf
<div class="card">
    <div class="card-header">
        <h4>Personal Info</h4>
    </div>
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Photo Profile</label>
            <input
            type="file"
            name="file"
            id="input-file-max-fs"
            class="dropify"
            data-max-file-size="1M"
            data-default-file="{{ asset($employee->getFirstMediaUrl('employee')) }}"
            />
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile"><span style="color:red">*</span> NIK</label>
                <input type="number" class="form-control r-0 light s-12" name="nik" value="{{ old('nik') ?? $employee->nik }}" required>
            </div>`
            <div class="form-group col-md-4">
                <label for="photo_profile">KTP</label>
                <input type="number" class="form-control r-0 light s-12" name="ktp" value="{{ old('ktp') ?? $employee->ktp }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile"><span style="color:red">*</span> Name</label>
                <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $employee->name }}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Place Of Birth</label>
                <input type="text" class="form-control r-0 light s-12" name="place_of_birth" value="{{ old('place_of_birth') ?? $employee->place_of_birth }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Date Of Birth</label>
                <input type="date" class="form-control r-0 light s-12" name="dob" value="{{ old('dob') ?? $employee->dob }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Gender</label>
                <select name="gender" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    <option value="male" {{ old('gender') ?? $employee->gender == 'male' ? 'selected' : '' }}>Male</option>
                    <option value="female" {{ old('gender') ?? $employee->gender == 'female' ? 'selected' : '' }}>Female</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Province</label>
                <select class="form-control r-0 light s-12 select2" name="province_id" id="select-province">
                    <option value="">--Please Select--</option>
                    @foreach($provinces as $item)
                    @if( old('province_id') )
                    <option value="{{ $item->id }}" {{ old('province_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                    @else
                    <option value="{{ $item->id }}" {{ $item->id == $employee->province_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                    @endif
                    @endforeach

                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Regency</label>
                <select class="form-control r-0 light s-12 select2" name="regency_id" id="select-regency">
                    <option value="">--Please Select--</option>
                    @foreach($regencies as $regency)
                    @if( old('regency_id') )
                    <option value="{{ $regency->id }}" {{ old('regency_id') == $regency->id ? 'selected' : ''  }}>{{ $regency->name  }}</option>
                    @else
                    <option value="{{ $regency->id }}" {{ $regency->id == $employee->regency_id ? 'selected' : ''  }}>{{ $regency->name  }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">District</label>
                <select class="form-control r-0 light s-12 select2" name="district_id" id="select-district">
                    <option value="">--Please Select--</option>
                    @foreach($districts as $district)
                    @if( old('district_id') )
                    <option value="{{ $district->id }}" {{ old('district_id') == $district->id ? 'selected' : ''  }}>{{ $district->name  }}</option>
                    @else
                    <option value="{{ $district->id }}" {{ $district->id == $employee->district_id ? 'selected' : ''  }}>{{ $district->name  }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Current Address</label>
                <textarea name="address" class="form-control r-0 light s-6" cols="5" rows="5">{{ old('address') ?? $employee->address }}</textarea>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">KTP Address</label>
                <textarea name="address_ktp" class="form-control r-0 light s-6" cols="5" rows="5">{{ old('address_ktp') ?? $employee->address_ktp }}</textarea>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Religion</label>
                <select name="religion_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($religions as $religion)
                    @if( old('religion_id') )
                    <option value="{{ $religion->id }}" {{ old('religion_id') == $religion->id ? 'selected' : ''  }}>{{ $religion->name  }}</option>
                    @else
                    <option value="{{ $religion->id }}" {{ $religion->id == $employee->religion_id ? 'selected' : ''  }}>{{ $religion->name  }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Marital Status</label>
                <select name="marital_status" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    <option value="Single" {{ old('marital_status') ?? $employee->marital_status == 'Single' ? 'selected' : '' }}>Single</option>
                    <option value="Married" {{ old('marital_status') ?? $employee->marital_status == 'Married' ? 'selected' : '' }}>Married</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile"><span style="color:red">*</span> Primary Email</label>
                <input type="text" class="form-control r-0 light s-12" name="email" value="{{ old('email') ?? $employee->email }}" required>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Nomor Handphone-1</label>
                <input type="number" class="form-control r-0 light s-12" name="phone_number" value="{{ old('phone_number') ?? $employee->phone_number }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Nomor Handphone-2</label>
                <input type="number" class="form-control r-0 light s-12" name="mobile_number" value="{{ old('mobile_number') ?? $employee->mobile_number }}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">NPWP</label>
                <input type="text" class="form-control r-0 light s-12" name="npwp" value="{{ old('npwp') ?? $employee->npwp }}">
            </div>

        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label>KTP</label>
                <input
                type="file"
                name="ktp_employee"
                id="input-file-max-fs"
                class="dropify"
                data-max-file-size="1M"
                data-default-file="{{ asset($employee->getFirstMediaUrl('ktp_employee')) }}"
                />
            </div>
            <div class="form-group col-md-4">
                <label>Kartu Keluarga</label>
                <input
                type="file"
                name="kartu_keluarga"
                id="input-file-max-fs"
                class="dropify"
                data-max-file-size="1M"
                data-default-file="{{ asset($employee->getFirstMediaUrl('kartu_keluarga')) }}"
                />
            </div>

        </div>
        <div class="card-header">
            <h4>Employee Info</h4>
        </div>
        <br>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Division</label>
                <select name="division_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($divisions as $division)
                    @if( old('division_id') )
                    <option value="{{ $division->id }}" {{ old('division_id') == $division->id ? 'selected' : ''  }}>{{ $division->name }}</option>
                    @else
                    <option value="{{ $division->id }}" {{ $division->id == $employee->division_id ? 'selected' : ''  }}>{{ $division->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Department</label>
                <select name="department_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($departments as $department)
                    @if( old('department_id') )
                    <option value="{{ $department->id }}" {{ old('department_id') == $department->id ? 'selected' : ''  }}>{{ $department->name }}</option>
                    @else
                    <option value="{{ $department->id }}" {{ $department->id == $employee->department_id ? 'selected' : ''  }}>{{ $department->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Depot</label>
                <select name="depot_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($depots as $depot)
                        @if( old('depot_id') )
                            <option value="{{ $depot->id }}" {{ old('depot_id') == $depot->id ? 'selected' : ''  }}>{{ $depot->name }}</option>
                        @else
                            <option value="{{ $depot->id }}" {{ $depot->id == $employee->depot_id ? 'selected' : ''  }}>{{ $depot->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-row">

            <div class="form-group col-md-4">
                <label for="photo_profile"><span style="color:red">*</span>User Type </label>
                <select name="user_type_id" class="form-control r-0 light s-12" required>
                    <option value="">Please Select</option>
                    @foreach($userTypes as $userType)
                        @if( old('user_type_id') )
                            <option value="{{ $userType->id }}" {{ old('user_type_id') == $userType->id ? 'selected' : ''  }}>{{ $userType->name }}</option>
                        @else
                            <option value="{{ $userType->id }}" {{ $userType->id == optional($employee->user)->user_type_id ? 'selected' : ''  }}>{{ $userType->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Position</label>
                <select name="position_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($positions as $position)
                    @if( old('position_id') )
                    <option value="{{ $position->id }}" {{ old('position_id') == $position->id ? 'selected' : ''  }}>{{ $position->name }}</option>
                    @else
                    <option value="{{ $position->id }}" {{ $position->id == $employee->position_id ? 'selected' : ''  }}>{{ $position->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Job Level</label>
                <select name="job_level_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($jobLevels as $jobLevel)
                    @if( old('job_level_id') )
                    <option value="{{ $jobLevel->id }}" {{ old('job_level_id') == $jobLevel->id ? 'selected' : ''  }}>{{ $jobLevel->name }}</option>
                    @else
                    <option value="{{ $jobLevel->id }}" {{ $jobLevel->id == $employee->job_level_id ? 'selected' : ''  }}>{{ $jobLevel->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Employee Status</label>
                <select name="is_employment_status" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    <option value="permanent"{{ old('is_employment_status') ?? $employee->is_employment_status == 'permanent' ? 'selected' : '' }}>Permanent</option>
                    <option value="contract" {{ old('is_employment_status') ?? $employee->is_employment_status == 'contract' ? 'selected' : '' }}>Contract</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Contract From</label>
                <input type="date" class="form-control r-0 light s-12" name="contract_period_from" value="{{ old('contract_period_from') ?? $employee->contract_period_from }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Contract Until</label>
                <input type="date" class="form-control r-0 light s-12" name="contract_period_until" value="{{ old('contract_period_until') ?? $employee->contract_period_until }}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Direct Reporting Officer</label>
                <select name="coordinator_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($employees as $coordinator)
                    @if( old('coordinator_id') )
                    <option value="{{ $coordinator->id }}" {{ old('coordinator_id') == $coordinator->id ? 'selected' : ''  }}>{{ $coordinator->name }}</option>
                    @else
                    <option value="{{ $coordinator->id }}" {{ $coordinator->id == $employee->coordinator_id ? 'selected' : ''  }}>{{ $coordinator->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-4">
                <div class="control-label">
                    <label for=""><span style="color:red">*</span> Working Hour</div></label>
                <select name="working_hour" class="form-control r-0 light s-12" required>
                    <option value="">Please Select</option>
                    <option value="Normal" {{ old('working_hour') ?? $employee->working_hour == 'Normal' ? 'selected' : '' }}>Normal 08:00-17:00</option>
                    <option value="Normal7" {{ old('working_hour') ?? $employee->working_hour == 'Normal7' ? 'selected' : '' }}>Normal 07:00-15:20</option>
                    <option value="Shift" {{ old('working_hour') ?? $employee->working_hour == 'Shift' ? 'selected' : '' }}>Shift</option>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Hired Date</label>
                <input type="date" class="form-control r-0 light s-12" name="hired_date" value="{{ old('hired_date') ?? $employee->hired_date }}">
            </div>
        </div>
        <div class="form-group">
            <div class="control-label">Status</div>
            <div class="custom-switches-stacked mt-2">
                <label class="custom-switch">
                    <input type="radio" name="active" value="1" required class="custom-switch-input" {{ old('active') ?? $employee->active == "1" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Active</span>
                </label>
                <label class="custom-switch">
                    <input type="radio" name="active" value="0" required class="custom-switch-input" {{ old('active') ?? $employee->active == "0" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Inactive</span>
                </label>
            </div>
        </div>

        <div class="card-header">
            <h4>Info Kontak Darurat</h4>
        </div>
        <br>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="photo_profile"><span style="color:red">*</span> Name</label>
                <input type="text" class="form-control r-0 light s-12" name="nama_kontak_darurat" value="{{ old('nama_kontak_darurat') ?? $employee->nama_kontak_darurat }}" required>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Status Kontak Darurat</label>
                <select name="status_kontak_darurat" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    <option value="Suami" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Suami' ? 'selected' : '' }}>Suami</option>
                    <option value="Istri" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Istri' ? 'selected' : '' }}>Istri</option>
                    <option value="Ayah" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Ayah' ? 'selected' : '' }}>Ayah</option>
                    <option value="Ibu" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Ibu' ? 'selected' : '' }}>Ibu</option>
                    <option value="Kakak" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Kakak' ? 'selected' : '' }}>Kakak</option>
                    <option value="Anak" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Anak' ? 'selected' : '' }}>Anak</option>
                    <option value="Paman" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Paman' ? 'selected' : '' }}>Paman</option>
                    <option value="Bibi" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Bibi' ? 'selected' : '' }}>Bibi</option>
                    <option value="Tentangga/RT" {{ old('status_kontak_darurat') ?? $employee->status_kontak_darurat == 'Tentangga/RT' ? 'selected' : '' }}>Tentangga/RT</option>
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Nomor Handphone-1</label>
                <input type="number" class="form-control r-0 light s-12" name="nomor_kontak_darurat_1" value="{{ old('nomor_kontar_darurat_1') ?? $employee->nomor_kontar_darurat_1 }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Nomor Handphone-2</label>
                <input type="number" class="form-control r-0 light s-12" name="nomor_kontak_darurat_2" value="{{ old('nomor_kontak_darurat_2') ?? $employee->nomor_kontak_darurat_2 }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Email</label>
                <input type="text" class="form-control r-0 light s-12" name="email_kontak_darurat" value="{{ old('email_kontak_darurat') ?? $employee->email_kontak_darurat }}">
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="photo_profile">Alamat</label>
                <textarea name="alamat_kontak_darurat" class="form-control r-0 light s-6" cols="5" rows="5">{{ old('alamat_kontak_darurat') ?? $employee->alamat_kontak_darurat }}</textarea>
            </div>

        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Kota</label>
                <input type="text" class="form-control r-0 light s-12" name="kota_kontak_darurat" value="{{ old('kota_kontak_darurat') ?? $employee->kota_kontak_darurat }}">
            </div>

        </div>
    </div>
</div>

<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
