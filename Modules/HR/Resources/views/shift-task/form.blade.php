@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Lokasi</label>
                <select name="location_id" class="form-control r-0 light s-12">
                    <option value="">Please Select</option>
                    @foreach($locations as $location)
                    @if( old('location_id') )
                    <option value="{{ $location->id }}" {{ old('location_id') == $location->id ? 'selected' : ''  }}>{{ $location->name }}</option>
                    @else
                    <option value="{{ $location->id }}" {{ $location->id == $shiftTask->location_id ? 'selected' : ''  }}>{{ $location->name }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Angka Meteran Air</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="angka_meteran_air" style="text-align: right;" value="{{ old('angka_meteran_air') ?? number_format($shiftTask->angka_meteran_air) }}">
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Serah Terima Uang Kas Tunai</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="serah_terima_tunai" style="text-align: right;" value="{{ old('serah_terima_tunai') ?? number_format($shiftTask->serah_terima_tunai) }}">
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Serah Terima Uang Kas Non Tunai</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="serah_terima_non_tunai" style="text-align: right;" value="{{ old('serah_terima_non_tunai') ?? number_format($shiftTask->serah_terima_non_tunai) }}">
            </div>
            <div class="form-group col-md-12">
                <label for="photo_profile">Total Penjualan</label>
                <input type="text" class="form-control r-0 light s-12 " data-type="currency" name="serah_terima_non_tunai" style="text-align: right;" value="{{ old('serah_terima_non_tunai') ?? number_format($shiftTask->serah_terima_non_tunai) }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Seragam</label><br>
                <label class="colorinput">
                    <input name="is_seragam" type="checkbox" value="1" class="colorinput-input" {{ old('is_seragam') ?? $shiftTask->is_seragam == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Kebersihan</label><br>
                <label class="colorinput">
                    <input name="is_kebersihan" type="checkbox" value="1" class="colorinput-input" {{ old('is_kebersihan') ?? $shiftTask->is_kebersihan == "1" ? 'checked' : '' }} />
                    <span class="colorinput-color bg-info"></span>
                </label>
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Pelanggan</label><br>
                <label class="colorinput">
                    <input name="is_pelanggan" type="checkbox" value="1" class="colorinput-input" {{ old('is_pelanggan') ?? $shiftTask->is_pelanggan == "1" ? 'checked' : '' }}/>
                    <span class="colorinput-color bg-info"></span>
                </label>

                <br>
            </div>
        </div>
        <div class="row">
            <div class='form-group col-md-12'>
                <a class="btn btn-primary white add-image-btn pull-right" data-name="photos"
                style="color: white"><i
                class="icon wb-plus"></i>Add More Photo</a>
            </div>
            @if($mediaCount == 0)
            @foreach (range(1, 4) as $i)
            <div class='form-group col-md-3'>
                <input type='file' name='file[{{ $i }}]' class="dropify" id='input-file-max-fs'
                data-plugin='dropify' data-height='160px' data-max-file-size='2M'
                data-allowed-file-extensions="png jpg jpeg bmp gif pdf"/>
            </div>
            @endforeach
            <span id="photos-btm"></span>
            @else
            @foreach($media as $index => $val)
            <div class='form-group col-md-3'>
                <input type="file" name='file[]' data-id="{{$val->id }}"
                data-default-file="{{ '/'.$val->disk.'/'.$val->id.'/'.$val->file_name }}"
                class="dropify" id='input-file-max-fs'
                data-plugin='dropify' data-height='160px' data-max-file-size='10M'
                data-allowed-file-extensions="png jpg jpeg bmp gif pdf"/>
            </div>
            @endforeach
            <span id="photos-btm"></span>
            @endif
        </div>
    </div>

</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
@push('javascript')
<script src="{{ asset('js/dropify.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.dropify').dropify();
        $('.add-image-btn').click(function () {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='2M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
    });
</script>
@endpush
