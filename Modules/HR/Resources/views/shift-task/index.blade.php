@extends('hr::layouts.app')

@section('hr::title', 'Shift Task')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Shift Task',
    'active' => true,
    'url' => route('hr.shift-task.index')
    ])
@endsection

@section('hr::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        <div class="card-header">
            <h4>Shift Task</h4>
        </div>
        @include('flash::message')

        <div class="card-header">
            <h4>

            </h4>
            <div class="card-header-form">
                <a href="{{ route('hr.shift-task.create') }}" class="btn btn-sm btn-info pd-x-15 btn-white btn-uppercase">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
