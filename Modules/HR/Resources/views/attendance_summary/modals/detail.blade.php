<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Code Number</th>
                    <th scope="col">From Date</th>
                    <th scope="col">Until Date</th>
                    <th scope="col">Reason</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($leaves as $index => $leave)
                    <tr>
                        <th scope="row">{{ $index +1 }}</th>
                        <td>{{ $leave->code_number }}</td>
                        <td>{{ format_d_month_y($leave->from_date) }}</td>
                        <td>{{ format_d_month_y($leave->until_date) }}</td>
                        <td>{{ $leave->reason }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
