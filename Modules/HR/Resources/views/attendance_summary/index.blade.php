@extends('hr::layouts.app')

@section('hr::title', 'Attendance Summary Report')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Attendance Summary Report',
    'active' => true,
    'url' => route('hr.attendance_summary_report.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')

                <div class="card-body p-4">
                    <form action="" class="" method="GET">
                        <div class="m-portlet__body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Month</label>
                                        <select name="month" class="form-control select2" required>
                                            <option value="">--Please Select--</option>
                                            <option value="Jan" {{ request()->month == 'Jan' ? 'selected' : ''}}>Jan
                                            </option>
                                            <option value="Feb" {{ request()->month == 'Feb' ? 'selected' : ''}}>Feb
                                            </option>
                                            <option value="Mar" {{ request()->month == 'Mar' ? 'selected' : ''}}>Mar
                                            </option>
                                            <option value="Apr" {{ request()->month == 'Apr' ? 'selected' : ''}}>Apr
                                            </option>
                                            <option value="May" {{ request()->month == 'May' ? 'selected' : ''}}>May
                                            </option>
                                            <option value="Jun" {{ request()->month == 'Jun' ? 'selected' : ''}}>Jun
                                            </option>
                                            <option value="Jul" {{ request()->month == 'Jul' ? 'selected' : ''}}>Jul
                                            </option>
                                            <option value="Aug" {{ request()->month == 'Aug' ? 'selected' : ''}}>Aug
                                            </option>
                                            <option value="Sep" {{ request()->month == 'Sep' ? 'selected' : ''}}>Sep
                                            </option>
                                            <option value="Oct" {{ request()->month == 'Oct' ? 'selected' : ''}}>Oct
                                            </option>
                                            <option value="Nov" {{ request()->month == 'Nov' ? 'selected' : ''}}>Nov
                                            </option>
                                            <option value="Dec" {{ request()->month == 'Dec' ? 'selected' : ''}}>Dec
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Year</label>
                                        @php
                                            $firstYear = (int) date('Y');
                                            $lastYear = $firstYear + 10;
                                        @endphp
                                        <select name="year" class="form-control select2">
                                            @for ($i=$firstYear;$i<=$lastYear;$i++)
                                                <option value="{{ $i }}" {{ request()->year == $i ? 'selected' : '' }}>{{ $i }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                    <br>
                    <hr>
                    <div class="table-responsive">
                        <h5>{{ request()->month }} {{ request()->year }}</h5>
                        <table class="table table-bordered table-hover striped" cellspacing="0">
                            <thead>
                            <tr>
                                <th class="text-center" style="">#</th>
                                <th class="text-center" style="">NIK</th>
                                <th class="text-center" style="">ID Absen</th>
                                <th class="text-center" style="">Name</th>
                                <th class="text-center" style="">Hadir</th>
                                <th class="text-center" style="">Telat</th>

                              
                            </tr>       
                            </thead>
                            <tbody>
                            @forelse($dataAttendance as $index => $data)
                                @php
                                    $month = (!empty(request('month'))) ? (request('month')) : ('');
                                    $year  = (!empty(request('year'))) ? (request('year')) : ('');

                                    if($month && $year){
                                    $calendar = \Modules\Setting\Entities\Calendar::where('month_name', $month)->where('year', $year)->get();
                                    $idCalendar = $calendar->pluck('id');
                                   
                                    $getCountLate = \Modules\HR\Entities\StaffAttendance::all();
                                   
                                    }

                                @endphp
                                <tr>
                                    <td class="text-center">{{ $index +1 }}</td>
                                    <td class="text-center"
                                        style="font-family: Arial, Helvetica, sans-serif;">{{ $data->nik }}</td>
                                    <td class="text-center"
                                        style="font-family: Arial, Helvetica, sans-serif;">{{ $data->absen_no ?? '-' }}</td>
                                    <td class="text-left" style="font-family: Arial, Helvetica, sans-serif;"><a
                                                href="{{ route('hr.report.attendance_report.index', ['employee'=> $data->id, 'month' => request()->month, 'year' => request()->year ] ) }}">{{ $data->name }}</a></td>
                                    <td class="text-center"
                                        style="font-family: Arial, Helvetica, sans-serif;">{{ getAttendanceCount($data->id, request('month'), request('year')) == "0" ? '-' : getAttendanceCount($data->id, request('month'), request('year')) }}</td>
                                   
                                    <td class="text-center"
                                        style="font-family: Arial, Helvetica, sans-serif;color:red;">{{ getAttendanceLateCount($data->id, request('month'), request('year')) == "0" ? '-' : getAttendanceLateCount($data->id, request('month'), request('year'))}}   {{ getAttendanceLateSum($data->id, request('month'), request('year')) == null ? ' ' :  "(" . getAttendanceLateSum($data->id, request('month'), request('year')) .")" }}  </td>
                                  
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="12" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('hr::attendance_summary.modals.index')
    <script>
        // show detail data in modal
        $('.modal-global').click(function (event) {
            event.preventDefault();

            var url = $(this).attr('href');
            $("#modal-global").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function (response) {
                    $("#modal-global").find('.modal-body').html(response);
                });
        });
    </script>

@endpush