<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <tr>
                <th>No</th>
                <th>:</th>
                <th>{{ $leave->code_number }}</th>
            </tr>
            <tr>
                <th>Employee</th>
                <th>:</th>
                <th>{{ optional($leave->employee)->name }}</th>
            </tr>
            <tr>
                <th>Leave Type</th>
                <th>: </th>
                <th>{{ optional($leave->leaveType)->name }}</th>
            </tr>
            <tr>
                <th>From Date</th>
                <th>:</th>
                <th>{{ format_d_month_y($leave->from_date) }}</th>
            </tr>
            <tr>
                <th>Until Date</th>
                <th>:</th>
                <th>{{ format_d_month_y($leave->until_date) }}</th>
            </tr>
            <tr>
                <th>Address</th>
                <th>:</th>
                <th>{{ $leave->address ?? '-' }}</th>
            </tr>
            <tr>
                <th>Reason</th>
                <th>:</th>
                <th>{{ $leave->reason }}</th>
            </tr>
            @if($leave->rejected_by)
                <tr>
                    <td width="30%">Rejected By</td>
                    <td width="2%">:</td>
                    <td>{{ optional($leave->rejectedBy)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Rejected At</td>
                    <td width="2%">:</td>
                    <td>{{ format_d_month_y($leave->rejected_at) }}</td>
                </tr>
                <tr>
                    <td width="30%">Rejected Reason</td>
                    <td width="2%">:</td>
                    <td>{{ $leave->rejected_reason }}</td>
                </tr>
            @endif
        </table>
        <legend class="text-center">History</legend>
        <table class="table table-hover table-bordered">
            <tr>
                <th>Employee</th>
                <th>Status</th>
                <th>Date</th>
                <th>Description</th>
            </tr>
            @forelse($histories as $history)
                <tr>
                    <td>{{ $history->employee ? optional($history->employee)->name : ($history->role != null ? $history->role: '-') }}</td>
                    @if(empty($history->date))
                        <td>{{ $history->is_approved ? 'Approved' : 'Waiting'  }}</td>
                    @else
                        <td>{{ $history->is_approved ? 'Approved' : 'Reject'  }}</td>
                    @endif
                    <td>{{ format_d_month_y($history->date)  }}</td>
                    <td>{{ $history->description  }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Data Not Found.</td>
                </tr>
            @endforelse
        </table>
        <div class="form-group">
            <label for="exampleFormControlTextarea3">Note</label>
            <textarea class="form-control" rows="7" name="description" onkeyup="changeValueDescription(this)"></textarea>
        </div>
    </div>
    <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <form action="{{ route('hr.leave.approval.update', $leave->id) }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="approve">
            <input type="hidden" name="description" class="description">
            <button type="submit" class="btn btn-primary" id="modal-btn-save">Approved</button>
        </form>
        <form action="{{ route('hr.leave.approval.update', $leave->id) }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="reject">
            <input type="hidden" name="description" class="description">
            <button type="submit" class="btn btn-danger" id="modal-btn-save">To Be Revised</button>
        </form>
    </div>
</div>
<script>
    function changeValueDescription(e) {
        let val = e.value
        $('.description').val(val)
    }
</script>
