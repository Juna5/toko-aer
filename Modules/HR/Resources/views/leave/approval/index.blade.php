@extends('hr::layouts.app')

@section('hr::title', 'Leave Approval')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Leave',
    'active' => true,
    'url' => route('hr.leave.view')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Leave Approval',
    'active' => true,
    'url' => route('hr.leave.approval.index')
    ])
@endsection

@section('hr::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Leave Approval</h4>
            </div>
            <div class="card-body p-0">
                @include('flash::message')
                <div class="table-responsive">
                    <table class="table table-striped" id="sortable-table">
                        <thead>
                        <tr class="text-center">
                            <th class="text-center">
                                <i class="fas fa-th"></i>
                            </th>
                            <th>Code Number</th>
                            <th>Name</th>
                            <th>Leave Type</th>
                            <th>From Date</th>
                            <th>Until Date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @forelse($leaves as $row)
                                <tr class="text-center">
                                    <td>
                                        <div class="sort-handler">
                                            <i class="fas fa-th"></i>
                                        </div>
                                    </td>
                                    <td>{{ $row['code_number'] }}</td>
                                    <td>{{ $row['employee']['name'] }}</td>
                                    <td>{{ $row['leave_type']['name'] }}</td>
                                    <td>{{ format_d_month_y($row['from_date']) }}</td>
                                    <td>{{ format_d_month_y($row['until_date']) }}</td>
                                    <td><div class="badge badge-success">{{ $row['status'] }}</div></td>
                                    <td>
                                        <a href="{{ route('hr.leave.approval.show', $row['id']) }}" class="btn btn-secondary modal-global">
                                            Detail
                                        </a>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="8" class="text-center">Data Not Found</td>
                                </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
    @include('hr::leave.approval.modals.index')
    <script>
        // show detail data in modal
        $('.modal-global').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-global").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function(response) {
                    $("#modal-global").find('.modal-body').html(response);
                });

        });
    </script>
@endpush
