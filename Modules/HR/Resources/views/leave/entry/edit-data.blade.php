@extends('hr::layouts.app')

@section('hr::title', 'Edit Leave Entry')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Leave Entry',
    'active' => true,
    'url' => route('hr.leave.entry.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Edit',
    'active' => true,
    'url' => route('hr.leave.entry.edit', $leave->id)
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.leave.entry.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.leave.entry.update', $leave->id) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @csrf
                                        <input type="hidden" name="resubmit" value="{{ request()->resubmit }}">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="form-group">
                                                    <label>Leave Type</label>
                                                    <input type="text" value="{{ optional($leave->leaveType)->name }}" class="form-control r-0 light s-12" disabled>
                                                    <input type="hidden" value="{{ $leave->leave_type_id }}">
                                                </div>
                                                <div class="form-group">
                                                    <label>Balance</label>
                                                    <input type="text"
                                                           class="form-control r-0 light s-12"
                                                           value="{{ format_d_month_y(optional($leave->leaveBalance)->from_date) .' s/d '. format_d_month_y(optional($leave->leaveBalance)->until_date) .' | '.
                                                                optional($leave->leaveType)->name .' | '. optional($leave->leaveBalance)->remaining_balance
                                                           }}"
                                                           readonly
                                                    >
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>From Date</label>
                                                        <input type="date" class="form-control datepicker" name="from_date" value="{{ old('from_date') ?? $leave->from_date }}" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Until Date</label>
                                                        <input type="date" class="form-control datepicker" name="until_date" value="{{ old('until_date') ?? $leave->until_date }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-row">
                                                    <div class="form-group col-md-6">
                                                        <label>From Hour</label>
                                                        <input type="text" id="from" class="form-control clockPicker" name="from_hour" value="{{ old('from_hour') ?? $leave->from_hour }}" disabled>
                                                    </div>
                                                    <div class="form-group col-md-6">
                                                        <label>Until Hour</label>
                                                        <input type="text" id="until" class="form-control clockPicker" name="until_hour" value="{{ old('until_hour') ?? $leave->until_hour }}" disabled>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Reason</label>
                                                    <textarea name="reason" id="" cols="30" rows="10" class="form-control" required>{{ old('reason') ?? $leave->reason }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Leaving Indonesia</label>
                                                    <select class="form-control" name="leaving_indonesia">
                                                        <option value="1" {{ old('leaving_indonesia') ?? $leave->leaving_indonesia == '1' ? 'selected' : '' }}>No</option>
                                                        <option value="0" {{ old('leaving_indonesia') ?? $leave->leaving_indonesia == '0' ? 'selected' : '' }}>Yes</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea name="address" id="" cols="30" rows="10" class="form-control">{{ old('address') ?? $leave->address }}</textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Arrangement Cover</label>
                                                    <input type="text" name="arrangement_cover" class="form-control" value="{{ old('arrangement_cover') ?? $leave->arrangement_cover }}">
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class='form-group col-md-12'>
                                                        <a class="btn btn-primary white add-image-btn pull-right" data-name="file" style="color: white"><i class="icon wb-plus"></i>Add Supporting Document</a>
                                                    </div>
                                                    @foreach($media as $index => $val)
                                                        <div class='form-group col-md-3'>
                                                            <input type="file" name='file[]' data-id="{{$val->id }}"
                                                                   data-default-file="{{ $val->getFullUrl() }}"
                                                                   class="dropify" id='input-file-max-fs'
                                                                   data-plugin='dropify' data-height='160px' data-max-file-size='5M'
                                                                   data-allowed-file-extensions="png jpg jpeg bmp gif pdf"/>
                                                        </div>
                                                    @endforeach
                                                    <span id="file-btm"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <button class="btn btn-primary btn-lg">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
            $('.add-image-btn').click(function () {
                var name = $(this).data('name');
                $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='2M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
                $('.dropify').dropify();
            });
            @include('include.dropify-remove-image')
        });
    </script>
@endpush
