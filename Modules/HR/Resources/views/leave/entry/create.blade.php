@extends('hr::layouts.app')

@section('hr::title', 'Add Leave')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Leave',
    'active' => true,
    'url' => route('hr.leave.entry.index')
    ])
@endsection
@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Add',
    'active' => true,
    'url' => route('hr.leave.entry.create')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('hr.leave.entry.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('hr.leave.entry.store') }}" method="POST" enctype="multipart/form-data">
                                        @include('hr::leave.entry.form', [
                                            'leave' => new \Modules\HR\Entities\Leave
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            if ($("#leave").change(function () {
                val = $(this).val();
                if (val == 1){
                    $('#balance').prop('disabled', false);
                    $('#from').prop('disabled', 'disabled');
                    $('#until').prop('disabled', 'disabled');
                } else if(val== 10){
                    $('#from').prop('disabled', false);
                    $('#until').prop('disabled', false);
                } else {
                    $('#balance').prop('disabled', 'disabled');
                    $('#from').prop('disabled', 'disabled');
                    $('#until').prop('disabled', 'disabled');
                }
            }));
            $('#balance').prop('disabled', 'disabled');
            $('#from').prop('disabled', 'disabled');
            $('#until').prop('disabled', 'disabled');

            $('.dropify').dropify();
            $('.add-image-btn').click(function () {
                var name = $(this).data('name');
                console.log(name);
                $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='2M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
                $('.dropify').dropify();
            });
        });
    </script>
@endpush
