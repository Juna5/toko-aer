@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group">
            <label>Leave Type</label>
            <select name="leave_type_id" id="leave" class="form-control r-0 light s-12 select2" required>
                <option value="">Please Select</option>
                @foreach($leaveTypes as $leaveType)
                    @if( old('leave_type_id') )
                        <option value="{{ $leaveType->id }}" {{ old('leave_type_id') == $leaveType->id ? 'selected' : ''  }}>{{ $leaveType->name }}</option>
                    @else
                        <option value="{{ $leaveType->id }}" {{ $leaveType->id == $leave->leave_type_id ? 'selected' : ''  }}>{{ $leaveType->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Balance</label>
            <select name="leave_balance_id" id="balance" class="form-control r-0 light s-12" required>
                <option value="">Please Select</option>
                @foreach($leaveBalances as $leaveBalance)
                    @if( old('leave_balance_id') )
                        <option value="{{ $leaveBalance->id }}" {{ old('leave_balance_id') == $leaveBalance->id ? 'selected' : ''  }}>{{ format_d_month_y($leaveBalance->from_date) .' s/d '. format_d_month_y($leaveBalance->until_date) .' | '. optional($leaveBalance->leaveType)->name .' | '. $leaveBalance->remaining_balance}}</option>
                    @else
                        <option value="{{ $leaveBalance->id }}" {{ $leaveBalance->id == $leave->leave_balance_id ? 'selected' : ''  }}>{{ format_d_month_y($leaveBalance->from_date) .' s/d '. format_d_month_y($leaveBalance->until_date) .' | '. optional($leaveBalance->leaveType)->name .' | '. $leaveBalance->remaining_balance}}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>From Date</label>
                <input type="date" class="form-control datepicker" name="from_date" value="{{ old('from_date') ?? $leave->from_date }}" required>
            </div>
            <div class="form-group col-md-6">
                <label>Until Date</label>
                <input type="date" class="form-control datepicker" name="until_date" value="{{ old('until_date') ?? $leave->until_date }}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label>From Hour</label>
                <input type="text" id="from" class="form-control clockPicker" name="from_hour" value="{{ old('from_hour') ?? $leave->from_hour }}" required>
            </div>
            <div class="form-group col-md-6">
                <label>Until Hour</label>
                <input type="text" id="until" class="form-control clockPicker" name="until_hour" value="{{ old('until_hour') ?? $leave->until_hour }}" required>
            </div>
        </div>
        <div class="form-group">
            <label>Reason</label>
            <textarea name="reason" id="" cols="30" rows="10" class="form-control" required>{{ old('reason') ?? $leave->reason }}</textarea>
        </div>
        <div class="form-group">
            <label>Leaving Indonesia</label>
            <select class="form-control" name="leaving_indonesia">
                <option value="1" {{ old('leaving_indonesia') ?? $leave->leaving_indonesia == '1' ? 'selected' : '' }}>No</option>
                <option value="0" {{ old('leaving_indonesia') ?? $leave->leaving_indonesia == '0' ? 'selected' : '' }}>Yes</option>
            </select>
        </div>
        <div class="form-group">
            <label>Address</label>
            <textarea name="address" id="" cols="30" rows="10" class="form-control">{{ old('address') ?? $leave->address }}</textarea>
        </div>
        <div class="form-group">
            <label>Arrangement Cover</label>
            <input type="text" name="arrangement_cover" class="form-control" value="{{ old('arrangement_cover') ?? $leave->arrangement_cover }}">
        </div>
        <div class="form-group m-form__group row">
            <div class='form-group col-md-12'>
                <a class="btn btn-primary white add-image-btn pull-right" data-name="file" style="color: white"><i class="icon wb-plus"></i>Add Supporting Document</a>
            </div>
            @foreach (range(1, 4) as $i)
                <div class='form-group col-md-3'>
                    <input type='file' name='file[{{ $i }}]' class="dropify" id='input-file-max-fs'
                           data-plugin='dropify' data-height='160px' data-max-file-size='5M'
                           data-allowed-file-extensions="png jpg jpeg bmp gif pdf"/>
                </div>
            @endforeach
            <span id="file-btm"></span>
        </div>
    </div>
</div>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
