@extends('hr::layouts.app')

@section('hr::title', 'Leave Entry')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Leave',
    'active' => true,
    'url' => route('hr.leave.view')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Leave Entry',
    'active' => true,
    'url' => route('hr.leave.entry.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <div class="table-responsive">
                        <table class="table table-striped" id="sortable-table">
                            <thead>
                            <tr>
                                <th class="text-center">
                                    <i class="fas fa-th"></i>
                                </th>
                                <th class="text-center">Leave Type</th>
                                <th class="text-center">From Date</th>
                                <th class="text-center">Until Date</th>
                                <th class="text-center">Beginning Balance</th>
                                <th class="text-center">Used Balance</th>
                                <th class="text-center">Remaining Balance</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($leaveBalances as $leaveBalance)
                                <tr>
                                    <td class="text-center">
                                        <div class="sort-handler">
                                            <i class="fas fa-th"></i>
                                        </div>
                                    </td>
                                    <td class="text-center">{{ optional($leaveBalance->leaveType)->name }}</td>
                                    <td class="text-center">
                                        {{ format_d_month_y($leaveBalance->from_date) }}
                                    </td>
                                    <td class="text-center">
                                        {{ format_d_month_y($leaveBalance->until_date) }}
                                    </td>
                                    <td class="text-center">
                                        {{ $leaveBalance->beginning_balance }}
                                    </td>
                                    <td class="text-center text-danger">
                                        {{ $leaveBalance->used_balance }}
                                    </td>
                                    <td class="text-center text-info">
                                        {{ $leaveBalance->remaining_balance }}
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td class="text-center" colspan="8">Data Not Found</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
