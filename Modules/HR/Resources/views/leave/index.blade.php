@extends('setting::layouts.master')

@section('setting::title', 'Leave')

@section('setting::breadcrumb-2')
    @include('setting::include.breadcrumb', [
        'title' => 'Leave',
        'active' => true,
        'url' => route('setting.index')
    ])
@endsection

@section('setting::content')
    <div class="section-body">
        <h2 class="section-title">Overview</h2>
        <p class="section-lead">
            Organize and adjust all settings about this site.
        </p>
        @include('flash::message')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-body">
                        <h4>Leave Entry</h4>
                        <p>Create leave for employee.</p>
                        @can('view leave')
                            <a href="{{ route('hr.leave.entry.index') }}" class="card-cta">Go to Leave Entry <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="card-body">
                        <h4>Leave Balance</h4>
                        <p>Setting up leave balance.</p>
                        @can('view leave balance')
                            <a href="{{ route('hr.leave.leave-balance.index') }}" class="card-cta">Go to Leave Balance <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-envelope-open-text"></i>
                    </div>
                    <div class="card-body">
                        <h4>Leave Approval</h4>
                        <p>All leave approval data.</p>
                        @can('view leave approval')
                            <a href="{{ route('hr.leave.approval.index') }}" class="card-cta">Go to Leave Approval <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
