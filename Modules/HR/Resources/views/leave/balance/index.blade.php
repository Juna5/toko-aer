@extends('hr::layouts.app')

@section('hr::title', 'Leave Balance')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Leave',
    'active' => true,
    'url' => route('hr.leave.view')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Leave Balance',
    'active' => true,
    'url' => route('hr.leave.leave-balance.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush