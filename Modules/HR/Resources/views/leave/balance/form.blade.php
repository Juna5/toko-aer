@csrf
<div class="card">
    <div class="card-body">
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="photo_profile">Employee</label>
                <select class="form-control r-0 light s-12 select2" name="employee_id" required>
                    <option value="">--Please Select--</option>
                    @foreach($employees as $employee)
                        @if( old('employee_id') )
                            <option value="{{ $employee->id }}" {{ old('employee_id') == $employee->id ? 'selected' : ''  }}>{{ $employee->name  }}</option>
                        @else
                            <option value="{{ $employee->id }}" {{ $employee->id == $leaveBalance->employee_id ? 'selected' : ''  }}>{{ $employee->name  }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Leave Type</label>
                <select class="form-control r-0 light s-12 select2" name="leave_type_id" required>
                    <option value="">--Please Select--</option>
                    @foreach($leaveTypes as $leaveType)
                        @if( old('leave_type_id') )
                            <option value="{{ $leaveType->id }}" {{ old('leave_type_id') == $leaveType->id ? 'selected' : ''  }}>{{ $leaveType->name  }}</option>
                        @else
                            <option value="{{ $leaveType->id }}" {{ $leaveType->id == $leaveBalance->leave_type_id ? 'selected' : ''  }}>{{ $leaveType->name  }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="photo_profile">From Date</label>
                <input type="date" class="form-control r-0 light s-12 datepicker" name="from_date" value="{{ old('from_date') ?? $leaveBalance->from_date }}" required>
            </div>
            <div class="form-group col-md-6">
                <label for="photo_profile">Until Date</label>
                <input type="date" class="form-control r-0 light s-12 datepicker" name="until_date" value="{{ old('until_date') ?? $leaveBalance->until_date }}" required>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-4">
                <label for="photo_profile">Beginning Balance</label>
                <input type="number" class="form-control r-0 light s-12" name="beginning_balance" value="{{ old('beginning_balance') ?? $leaveBalance->beginning_balance }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Used Balance</label>
                <input type="number" class="form-control r-0 light s-12" name="used_balance" value="{{ old('used_balance') ?? $leaveBalance->used_balance }}">
            </div>
            <div class="form-group col-md-4">
                <label for="photo_profile">Remaining Balance</label>
                <input type="number" class="form-control r-0 light s-12" name="remaining_balance" value="{{ old('remaining_balance') ?? $leaveBalance->remaining_balance }}">
            </div>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>