<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
	{!! Form::label($name) !!}
	@if($type == 'password')
	{!! Form::password($name, [
	'class' => ($errors->has($name)) ? 'form-control is-invalid' : 'form-control',
	'placeholder' => $placeholder ?? null,
	'disabled' => $disabled ?? false
	]) !!}
	@elseif($type == "date")
	{!! Form::$type($name, null, [
	'class' => ($errors->has($name)) ? 'form-control is-invalid datepicker' : 'form-control datepicker',
	'placeholder' => $placeholder ?? null,
	'disabled' => $disabled ?? false
	]) !!}
	@else
	{!! Form::$type($name, null, [
	'class' => ($errors->has($name)) ? 'form-control is-invalid' : 'form-control',
	'placeholder' => $placeholder ?? null,
	'disabled' => $disabled ?? false
	]) !!}
	@endif
	{!! $errors->first($name, '<div class="invalid-feedback d-block">:message</div>') !!}
</div>