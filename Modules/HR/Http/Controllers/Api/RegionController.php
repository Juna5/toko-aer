<?php

namespace Modules\HR\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\HR\Entities\District;
use Modules\HR\Entities\Regency;

class RegionController extends Controller
{
    public function data()
    {
        $set = request()->id;

        $regencies = Regency::where('province_id', $set)->get();                // Kabupaten
        $districts = District::where('regency_id', $set)->get();                 // Kelurahan

        switch(request()->type):
            case 'regencies':
                $return = '<option value="">--Please Select--</option>';
                foreach($regencies as $item)
                    $return .= "<option value='$item->id'>$item->name</option>";
                return $return;
                break;
            case 'districts':
                $return = '<option value="">--Please Select--</option>';
                foreach($districts as $item)
                    $return .= "<option value='$item->id'>$item->name</option>";
                return $return;
                break;
        endswitch;
    }
}
