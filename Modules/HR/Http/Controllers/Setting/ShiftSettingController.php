<?php

namespace Modules\HR\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\HR\DataTables\ShiftSettingDatatable;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\ShiftSetting;
use Modules\HR\Entities\WorkingHour;
use Modules\Setting\Entities\Calendar;

class ShiftSettingController extends Controller
{
    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index(ShiftSettingDatatable $dataTable)
    {
        $employees = Employee::where('active', true)->where('deleted_at', null)->get();

        $from_date = request()->from_date ?? null;
        $until_date = request()->until_date ?? null;
        $employee = request()->employee ?? null;

        return $dataTable->with(['from_date' => $from_date, 'until_date' => $until_date, 'employee' => $employee])->render('hr::setting.shift.index', compact('employees'));
    }

    public function getCalendar($year, $month)
    {
        $employee           = Employee::where('working_hour', 'Shift')->get();
        $employe            = Employee::where('working_hour', 'Shift')->first();
        $calendars          = [];
        $working_hour       = WorkingHour::all();

        if ($year && $month) {
            if ($employe) {
                $calendars  = Calendar::where('month_name', $month)
                ->where('year', $year)
                ->where('user_type_id', optional($employe->user)->user_type_id)
                ->orderBy('date', 'ASC')
                ->get();
            } else {
                $calendars = [];
            }
        }
        return \response()->json($calendars);
    }

    public function getStatusEmployeeShift()
    {
        $employee           = Employee::where('working_hour', 'Shift')->get();

        return \response()->json($employee);
    }

    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
        return view('hr::setting.shift.create');
    }

    /**
    * Store a newly created resource in storage.
    * @param Request $request
    * @return Response
    */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $calendar = request()->setting_shift;
            foreach (array_filter($calendar) as $key => $value) {
                if ($value['shift1']) {
                    foreach ($value['shift1'] as $p) {
                        $exist = ShiftSetting::where('employee_id', $p['id'])->where('calendar_id', $value['id'])->whereDescription('Shift 1')->first();
                        if (empty($exist)) {
                            $workingHour = WorkingHour::whereNull('deleted_at')->where('name', 'like', '%Shift 1%')->first();
                            ShiftSetting::create([
                                'employee_id'     => $p['id'],
                                'calendar_id'     => $value['id'],
                                'working_hour_id' => $workingHour->id,
                                'description'     => "Shift 1"
                            ]);
                        }
                    }
                }
                if ($value['shift2']) {
                    foreach ($value['shift2'] as $s) {
                        $exist = ShiftSetting::where('employee_id', $s['id'])->where('calendar_id', $value['id'])->whereDescription('Shift 2')->first();
                        if (empty($exist)) {
                            $workingHour = WorkingHour::whereNull('deleted_at')->where('name', 'like', '%Shift 2%')->first();
                            ShiftSetting::create([
                            'employee_id'     => $s['id'],
                            'calendar_id'     => $value['id'],
                            'working_hour_id' => $workingHour->id,
                            'description'     => "Shift 2"
                            ]);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return response()->json([
                'success'    => false,
                'message'    => $e->getMessage(),
                'line'       => $e->getLine(),
                'redirect'   => route('hr.setting.shift_setting.create'),
            ]);
        }
        DB::commit();

        flash('Your data has been saved successfully')->success();
        return response()->json([
        'success' => true,
        'redirect' => route('hr.setting.shift_setting.index')
        ]);
    }

    /**
    * Show the specified resource.
    * @param int $id
    * @return Response
    */
    public function show($id)
    {
        return view('hr::show'); 
    }

    /**
    * Show the form for editing the specified resource.
    * @param int $id
    * @return Response
    */
    public function edit($id)
    {
        $row = ShiftSetting::find($id);
        $employees           = Employee::where('working_hour', 'Shift')->get();
        return view('hr::setting.shift.edit', compact('row', 'employees'));
    }

    /**
    * Update the specified resource in storage.
    * @param Request $request
    * @param int $id
    * @return Response
    */
    public function update(Request $request, $id)
    {
        $row = ShiftSetting::findOrFail($id);
        $exist = ShiftSetting::where('employee_id', $request->employee_id)->where('calendar_id', $row->calendar_id)->whereDescription($row->description)->first();
        if ($row->employee_id != request('employee_id')) {
            if ($exist) {
                flash()->error('Ops... data already exist');
                return back();
            }
        }

        $row->employee_id = $request->employee_id;
        $row->save();
        flash('Your data has been saved successfully')->success();

        return redirect()->route('hr.setting.shift_setting.index');
    }

    /**
    * Remove the specified resource from storage.
    * @param int $id
    * @return Response
    */
    public function destroy(ShiftSetting $shiftSetting)
    {
        $shiftSetting->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
