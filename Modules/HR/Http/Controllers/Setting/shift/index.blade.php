@extends('hr::layouts.app')

@section('hr::title', 'Shift Setting')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
    'title' => 'Setting',
    'active' => true,
    'url' => route('hr.setting.setting-index')
    ])
@endsection

@section('hr::breadcrumb-3')
    @include('hr::include.breadcrum', [
    'title' => 'Shift Setting',
    'active' => true,
    'url' => route('hr.setting.shift_setting.index')
    ])
@endsection

@section('hr::content')
    <div class="row">
        <div class="container">
            <div class="row my-3">
                <div class="col-md-12">
                    <div class="card no-b no-r">
                        <div class="card-body">
                            <form action="{{ route('hr.setting.shift_setting.index') }}" method="GET">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label class="">Month</label>
                                        <select id="month" name="month" class="form-control select2" required>
                                            <option value="">--Please Select--</option>
                                            <option value="Jan" {{ request()->month == 'Jan' ? 'selected' : ''}}>Jan
                                            </option>
                                            <option value="Feb" {{ request()->month == 'Feb' ? 'selected' : ''}}>Feb
                                            </option>
                                            <option value="Mar" {{ request()->month == 'Mar' ? 'selected' : ''}}>Mar
                                            </option>
                                            <option value="Apr" {{ request()->month == 'Apr' ? 'selected' : ''}}>Apr
                                            </option>
                                            <option value="May" {{ request()->month == 'May' ? 'selected' : ''}}>May
                                            </option>
                                            <option value="Jun" {{ request()->month == 'Jun' ? 'selected' : ''}}>Jun
                                            </option>
                                            <option value="Jul" {{ request()->month == 'Jul' ? 'selected' : ''}}>Jul
                                            </option>
                                            <option value="Aug" {{ request()->month == 'Aug' ? 'selected' : ''}}>Aug
                                            </option>
                                            <option value="Sep" {{ request()->month == 'Sep' ? 'selected' : ''}}>Sep
                                            </option>
                                            <option value="Oct" {{ request()->month == 'Oct' ? 'selected' : ''}}>Oct
                                            </option>
                                            <option value="Nov" {{ request()->month == 'Nov' ? 'selected' : ''}}>Nov
                                            </option>
                                            <option value="Dec" {{ request()->month == 'Dec' ? 'selected' : ''}}>Dec
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label>Year</label>
                                        <select id="year" name="year" class="form-control select2">
                                            <option value=""></option>
                                            <option value="2020" {{ request('year') == '2020' ? 'selected' : '' }}>2020</option>
                                            <option value="2021" {{ request('year') == '2021' ? 'selected' : '' }}>2021</option>
                                            <option value="2022" {{ request('year') == '2022' ? 'selected' : '' }}>2022</option>
                                            <option value="2023" {{ request('year') == '2023' ? 'selected' : '' }}>2023</option>
                                            <option value="2024" {{ request('year') == '2024' ? 'selected' : '' }}>2024</option>
                                            <option value="2025" {{ request('year') == '2025' ? 'selected' : '' }}>2025</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-4">
                                        
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary">Search</button>
                                <a href="{{ route('hr.setting.setting-index') }}" class="btn btn-warning">Back</a>
                            </form>
                            <br><br><br>
                           
                            @include('flash::message')
                            <div class="table-responsive">
                           <form action="{{ route('hr.setting.shift_setting.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <button type="submit" class="btn btn-success fa-pull-right">Save</button>
                            <br><br><br>
                                <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                                    <tr style="background-color:white ; color: grey" align="center">
                                        <th>#</th>
                                        <th>Date</th>
                                        <th>Day</th>
                                        <th>Shift 1</th>
                                        <th>Shift 2</th>
                                    </tr>
                                    @forelse($calendars as $index => $c)
                                        <tr align="center" >
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ format_d_month_y($c->date) }}</td>
                                            <td>{{ $c->days_of_week }}</td>
                                            <td>
                                                <input type="hidden" invisible="" class="form-control r-0 light s-12" name="years" value="{{ request()->year }}">
                                                <input type="hidden" class="form-control r-0 light s-12" name="months" value="{{ request()->month }}">
                                                <input type="hidden" class="form-control r-0 light s-12" name="calendar_id[]" value="{{ old('calendar_id') ?? $c->id }}">
                                                <select name="shift1[]"  class="form-control select2" >
                                                    <option value=""></option>
                                                    @foreach ($employee as $pagis)
                                                    <option value="{{ $pagis->id }}" @if (getStatusEmployeeShift($c->id, 'Shift 1') == $pagis->id) {{ 'selected' }} @endif>{{ $pagis->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <select name="shift2[]"  class="form-control select2" >
                                                    <option value=""></option>
                                                    @foreach ($employee as $sores)
                                                    <option value="{{ $sores->id }}"@if (getStatusEmployeeShift($c->id, 'Shift 1') == $sores->id) {{ 'selected' }} @endif>{{ $sores->name }}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                        </tr>
                                        @empty
                                        <tr>
                                            <td colspan="8" class="text-center">Data not found</td>
                                        </tr>
                                    @endforelse
                                   
                                   
                                </table>
                            
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


