@extends('hr::layouts.app')

@section('hr::title', 'Setting')

@section('hr::breadcrumb-2')
    @include('hr::include.breadcrum', [
        'title' => 'Setting',
        'active' => true,
        'url' => route('hr.setting.setting-index')
    ])
@endsection

@section('hr::content')
    <div class="section-body">
        <h2 class="section-title">Overview</h2>
        <p class="section-lead">
            HR -> Setting Section
        </p>
        @include('flash::message')
        <div class="row">
            <div class="col-lg-6">
                @can('view shift setting')
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-body">
                        <h4>Shift </h4>
                        <p>Shift settings for employees</p>
                            <a href="{{ route('hr.setting.shift_setting.index') }}" class="card-cta">Go to Shift Setting <i class="fas fa-chevron-right"></i></a>
                    </div>
                </div>
                @endcan
            </div>


            <div class="col-lg-6">

            </div>
        </div>
    </div>
@endsection
