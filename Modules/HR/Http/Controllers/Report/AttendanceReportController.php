<?php

namespace Modules\HR\Http\Controllers\Report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Modules\HR\Entities\Employee;
use Modules\Setting\Entities\Calendar;

class AttendanceReportController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $employee           = Employee::all();
        $calendars          = [];
        $idEmployee         = request()->employee;
        $month              = request()->month;
        $year               = request()->year;

        if (isset(request()->employee)) {
            $employe    = Employee::findOrFail($idEmployee);
            $calendars  = Calendar::where('month_name', $month)
                ->where('year', $year)
                ->where('user_type_id', optional($employe->user)->user_type_id)
                ->orderBy('date', 'ASC')
                ->get();
        }



        return view('hr::report.attendance_report.index', compact('calendars', 'employee'));
    }
}
