<?php

namespace Modules\HR\Http\Controllers\Report;

use Illuminate\Routing\Controller;

class ViewReportController extends Controller
{
    public function index()
    {
        return view('hr::report.index');
    }
}
