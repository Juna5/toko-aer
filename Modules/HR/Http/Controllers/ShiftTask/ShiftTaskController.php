<?php

namespace Modules\HR\Http\Controllers\ShiftTask;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\ShiftTaskDatatable;
use Modules\HR\Entities\ShiftTask;
use Modules\HR\Entities\ShiftSetting;
use Modules\HR\Entities\Location;
use Modules\HR\Entities\Employee;
use Modules\HR\Http\Requests\ShiftTaskRequest;

class ShiftTaskController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ShiftTaskDatatable $datatable)
    {
        $this->hasPermissionTo('view shift task');
        return $datatable->render('hr::shift-task.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(ShiftTask $shiftTask)
    {
        $this->hasPermissionTo('add shift task');
        $locations = Location::all();
        $mediaCount = 0;
        return view('hr::shift-task.create', compact('locations', 'shiftTask', 'mediaCount'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ShiftTaskRequest $request)
    {
        $this->hasPermissionTo('add shift task');
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        $now = now()->format('Y-m-d');
        $shiftSetting = ShiftSetting::where('employee_id', $employee->id)
        ->whereHas('calendar', function($q) use ($now){
            $q->where('date', $now);
        })
        ->whereNotNull('working_hour_id')
        ->first();
        if (!$shiftSetting) {
            flash('Silahkan hub Admin')->error();
            return redirect()->back();
        }

        $row = ShiftTask::create([
            'employee_id'               => $employee->id,
            'shift_id'                  => $shiftSetting->working_hour_id,
            'date'                      => $now,
            'time'                      => now()->format('H:i'),
            'location_id'               => $request->location_id,
            'angka_meteran_air'         => str_replace(',', '', request('angka_meteran_air')),
            'serah_terima_tunai'        => str_replace(',', '', request('serah_terima_tunai')),
            'serah_terima_non_tunai'    => str_replace(',', '', request('serah_terima_non_tunai')),
            'total'                     => str_replace(',', '', request('total')),
            'is_seragam'                => $request->is_seragam,
            'is_kebersihan'             => $request->is_kebersihan,
            'is_pelanggan'              => $request->is_pelanggan,
        ]);

        if (request()->hasFile('file')) {
            foreach (request()->file as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('shiftTask');
            }
        }

        flash('Your data has been created')->success();
        return redirect()->route('hr.shift-task.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('hr::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->hasPermissionTo('edit shift task');
        $shiftTask = ShiftTask::find($id);
        $locations = Location::all();
        $media = $shiftTask->getMedia('shiftTask');
        $mediaCount = count($media);
        return view('hr::shift-task.edit', compact('shiftTask', 'locations', 'media', 'mediaCount'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ShiftTaskRequest $request, $id )
    {
        $this->hasPermissionTo('edit shift task');
        $shiftTask = ShiftTask::find($id);
        $shiftTask->location_id                 = $request->location_id;
        $shiftTask->angka_meteran_air           = str_replace(',', '', request('angka_meteran_air'));
        $shiftTask->serah_terima_tunai          = str_replace(',', '', request('serah_terima_tunai'));
        $shiftTask->serah_terima_non_tunai      = str_replace(',', '', request('serah_terima_non_tunai'));
        $shiftTask->total                       = str_replace(',', '', request('total'));
        $shiftTask->is_seragam                  = $request->is_seragam;
        $shiftTask->is_kebersihan               = $request->is_kebersihan;
        $shiftTask->is_pelanggan                = $request->is_pelanggan;
        $shiftTask->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.shift-task.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(ShiftTask $shiftTask)
    {
        $this->hasPermissionTo('delete shift task');

        $shiftTask->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
