<?php

namespace Modules\HR\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\HR\DataTables\EmployeeDatatable;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\District;
use Modules\HR\Entities\Division;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\JobLevel;
use Modules\HR\Entities\JobPosition;
use Modules\HR\Entities\Province;
use Modules\HR\Entities\Regency;
use Modules\HR\Entities\Location;
use Modules\HR\Entities\Religion;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\HR\Http\Requests\EmployeeRequest;
use Modules\Setting\Entities\UserType;

class EmployeeController extends Controller
{
    public function index(EmployeeDatatable $datatable)
    {
        $this->hasPermissionTo('view employee');

        return $datatable->render('hr::employee.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add employee');

        $religions = Religion::all();
        $divisions = Division::all();
        $departments = Department::all();
        $positions = JobPosition::all();
        $jobLevels = JobLevel::all();
        $employees = Employee::where('active', true)->get();
        $provinces = Province::all();
        $regencies = Regency::all();
        $districts = District::all();
        $userTypes = UserType::all();
        $depots = WarehouseMaster::all();

        return view('hr::employee.create', compact('religions', 'divisions', 'departments',
            'positions', 'jobLevels', 'employees', 'provinces', 'regencies', 'districts', 'userTypes', 'depots'));
    }

    public function store(EmployeeRequest $request)
    {
        $this->hasPermissionTo('add employee');

        DB::beginTransaction();
        $exists = Employee::where('nik', $request->nik)->first();
        if ($exists){
            flash('Data already exist')->error();
            return back();
        }

        try {
        $user_id =   User::updateOrCreate(
                [
                    'username' => $request->nik,
                    'email' => $request->email,
                    'user_type_id' => $request->user_type_id
                ],
                [
                    'name' => $request->name,
                    'password' => bcrypt('keyboard'),
                    'active' => $request->active
                ]
            );

        } catch(\Exception $e) {
            DB::rollback();
            flash('Ops, try again'. $e->getMessage())->error();
            return back();
        }

        try {
            $request->request->add(['user_id' => $user_id->id]);
            $employee = Employee::create($request->except(['file', 'user_type_id','ktp_employee','kartu_keluarga']));
            if ($request->hasFile('file')) {
                $employee->addMedia($request->file)->toMediaCollection('employee');
            }
            if ($request->hasFile('ktp_employee')) {
                $employee->addMedia($request->ktp_employee)->toMediaCollection('ktp_employee');
            }
            if ($request->hasFile('kartu_keluarga')) {
                $employee->addMedia($request->kartu_keluarga)->toMediaCollection('kartu_keluarga');
            }
        } catch(\Exception $e) {
            DB::rollback();
            flash('Ops, try again'. $e->getMessage())->error();
            return back();
        }
        DB::commit();

        flash('Your data has been saved successfully')->success();
        return redirect()->route('hr.employee.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit employee');

        $employee = Employee::withoutGlobalScopes()->find($id);
        // dd($employee);
        $religions = Religion::all();
        $divisions = Division::all();
        $departments = Department::all();
        $positions = JobPosition::all();
        $jobLevels = JobLevel::all();
        $employees = Employee::where('active', true)->get();
        $provinces = Province::all();
        $regencies = Regency::all();
        $userTypes = UserType::all();
        $districts = District::all();
        $depots = WarehouseMaster::all();

        return view('hr::employee.edit', compact('employee', 'religions', 'divisions',
        'departments', 'positions', 'jobLevels', 'employees', 'provinces', 'regencies', 'districts', 'userTypes', 'depots'));
    }

    public function update(Request $request, Employee $employee)
    {
        $this->hasPermissionTo('edit employee');

        $user = User::find($employee->user_id);
        $user->user_type_id = (int) $request->user_type_id;
        $user->save();

        $data = $request->except(['file', 'user_type_id','ktp_employee','kartu_keluarga']);
        tap($employee, function ($employee) use ($data) {
            return $employee->update($data);
        })->fresh();

        if ($request->hasFile('file')) {
            if ($employee->getMedia('employee')->first()) {
                $employee->getMedia('employee')->first()->delete();
            }
            $employee->addMedia($request->file)->toMediaCollection('employee');
        }

        if ($request->hasFile('ktp_employee')) {
            if ($employee->getMedia('ktp_employee')->first()) {
                $employee->getMedia('ktp_employee')->first()->delete();
            }
            $employee->addMedia($request->ktp_employee)->toMediaCollection('ktp_employee');
        }
        if ($request->hasFile('kartu_keluarga')) {
            if ($employee->getMedia('kartu_keluarga')->first()) {
                $employee->getMedia('kartu_keluarga')->first()->delete();
            }
            $employee->addMedia($request->kartu_keluarga)->toMediaCollection('kartu_keluarga');
        }

        flash('Your data has been updated successfully')->success();
        return redirect()->route('hr.employee.index');
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete employee');

        Employee::withoutGlobalScopes()->find($id)->delete();

        flash('Your data has been created')->success();
        return back();
    }
}
