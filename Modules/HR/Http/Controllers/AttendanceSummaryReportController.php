<?php

namespace Modules\HR\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Leave;
use Modules\HR\Entities\LeaveType;
use Modules\Setting\Entities\Calendar;

class AttendanceSummaryReportController extends Controller
{
    public function index()
    {
        $dataAttendance = [];

        $month = (!empty(request('month'))) ? (request('month')) : ('');
        $year  = (!empty(request('year'))) ? (request('year')) : ('');

        if ($month && $year) {
            $dataAttendance = Employee::with('user')
                ->where('active', true)
                ->get();
        }

        return view('hr::attendance_summary.index', compact('dataAttendance'));
    }

    public function create()
    {
        return view('hr::create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        $employee = Employee::find($id);
        $month = request('month');
        $year = request('year');
        $type = request('type');

        $calendars = Calendar::where('month_name', $month)
            ->where('year', $year)
            ->get();
        $date = $calendars->pluck('date')->toArray();
        $firstDate = reset($date);
        $lastDate = end($date);
        $leaveType = LeaveType::where('name', $type)->get()->pluck('id');

        $leaves = Leave::where('employee_id', $employee->id)
            ->where('from_date', '>=', $firstDate)
            ->where('until_date', '<=', $lastDate)
            ->where('leave_type_id', $leaveType)
            ->whereNull('deleted_at')
            ->get();

        return view('hr::attendance_summary.modals.detail', compact('leaves'));
    }

    public function edit($id)
    {
        return view('hr::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
