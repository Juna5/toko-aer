<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\WorkingHourDatatable;
use Modules\HR\Entities\WorkingHour;
use Modules\HR\Http\Requests\WorkingHourRequest;

class WorkingHourController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(WorkingHourDatatable $datatable)
    {
        $this->hasPermissionTo('view working hour');

        return $datatable->render('hr::master.working_hour.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(WorkingHour $workingHour)
    {
        $this->hasPermissionTo('add working hour');
        return view('hr::master.working_hour.create', compact('workingHour'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(WorkingHourRequest $request)
    {
        $this->hasPermissionTo('add working hour');

        WorkingHour::create([
            'name'                  => $request->name,
            'time_from'             => $request->time_from,
            'time_until'            => $request->time_until,
            'description'           => $request->description,
            'active'                => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.working-hour.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('hr::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(WorkingHour $workingHour)
    {
        $this->hasPermissionTo('edit working hour');

        return view('hr::master.working_hour.edit', compact('workingHour'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(WorkingHourRequest $request, WorkingHour $workingHour)
    {
        $this->hasPermissionTo('edit working hour');

        $workingHour->name               = $request->name;
        $workingHour->time_from          = $request->time_from;
        $workingHour->time_until         = $request->time_until;
        $workingHour->description        = $request->description;
        $workingHour->active             = $request->active;
        $workingHour->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.working-hour.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(WorkingHour $workingHour)
    {
        $this->hasPermissionTo('delete working hour');

        $workingHour->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
