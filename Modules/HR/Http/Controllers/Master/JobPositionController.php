<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\JobPositionDatatable;
use Modules\HR\Entities\JobPosition;
use Modules\HR\Http\Requests\MasterRequest;

class JobPositionController extends Controller
{
    public function index(JobPositionDatatable $datatable)
    {
        $this->hasPermissionTo('view job position');

        return $datatable->render('hr::master.job_position.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add job position');

        return view('hr::master.job_position.create');
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add job position');

        JobPosition::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'active'        => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.job-position.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(JobPosition $jobPosition)
    {
        $this->hasPermissionTo('edit job position');

        return view('hr::master.job_position.edit', compact('jobPosition'));
    }

    public function update(MasterRequest $request, JobPosition $jobPosition)
    {
        $this->hasPermissionTo('edit job position');

        $jobPosition->name          = $request->name;
        $jobPosition->description   = $request->description;
        $jobPosition->active        = $request->active;
        $jobPosition->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.job-position.index');
    }

    public function destroy(JobPosition $jobPosition)
    {
        $this->hasPermissionTo('delete job position');

        $jobPosition->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
