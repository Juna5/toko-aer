<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\LocationDatatable;
use Modules\HR\Entities\Location;
use Modules\HR\Entities\Division;
use Modules\HR\Http\Requests\MasterRequest;

class LocationController extends Controller
{
    public function index(LocationDatatable $datatable)
    {
        $this->hasPermissionTo('view location');

        return $datatable->render('hr::master.location.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add location');

        return view('hr::master.location.create');
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add location');

        Location::create([
            'name'          => $request->name,
            'description'   => $request->description,
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.location.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(Location $location)
    {
        $this->hasPermissionTo('edit location');
        return view('hr::master.location.edit', compact('location'));
    }

    public function update(MasterRequest $request, Location $location)
    {
        $this->hasPermissionTo('edit location');

        $location->name           = $request->name;
        $location->description    = $request->description;
        $location->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.location.index');
    }

    public function destroy(Location $location)
    {
        $this->hasPermissionTo('delete location');

        $location->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
