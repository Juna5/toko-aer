<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\JobLevelDatatable;
use Modules\HR\Entities\JobLevel;
use Modules\HR\Http\Requests\MasterRequest;

class JobLevelController extends Controller
{
    public function index(JobLevelDatatable $datatable)
    {
        $this->hasPermissionTo('view job level');

        return $datatable->render('hr::master.job_level.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add job level');

        return view('hr::master.job_level.create');
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add job level');

        JobLevel::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'active'        => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.job-level.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(JobLevel $jobLevel)
    {
        $this->hasPermissionTo('edit job level');

        return view('hr::master.job_level.edit', compact('jobLevel'));
    }

    public function update(MasterRequest $request, JobLevel $jobLevel)
    {
        $this->hasPermissionTo('edit job level');

        $jobLevel->name         = $request->name;
        $jobLevel->description  = $request->description;
        $jobLevel->active       = $request->active;
        $jobLevel->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.job-level.index');
    }

    public function destroy(JobLevel $jobLevel)
    {
        $this->hasPermissionTo('delete job level');

        $jobLevel->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
