<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\DivisionDatatable;
use Modules\HR\Entities\Division;
use Modules\HR\Http\Requests\MasterRequest;

class DivisionController extends Controller
{
    public function index(DivisionDatatable $datatable)
    {
        $this->hasPermissionTo('view division');

        return $datatable->render('hr::master.division.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add division');

        return view('hr::master.division.create');
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add division');

        Division::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'active'        => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.division.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(Division $division)
    {
        $this->hasPermissionTo('edit division');

        return view('hr::master.division.edit', compact('division'));
    }

    public function update(MasterRequest $request, Division $division)
    {
        $this->hasPermissionTo('edit division');

        $division->name         = $request->name;
        $division->description  = $request->description;
        $division->active       = $request->active;
        $division->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.division.index');
    }

    public function destroy(Division $division)
    {
        $this->hasPermissionTo('delete division');

        $division->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
