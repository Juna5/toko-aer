<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\ReligionDatatable;
use Modules\HR\Entities\Religion;
use Modules\HR\Http\Requests\MasterRequest;

class ReligionController extends Controller
{
    public function index(ReligionDatatable $datatable)
    {
        $this->hasPermissionTo('view religion');

        return $datatable->render('hr::master.religion.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add religion');

        return view('hr::master.religion.create');
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add religion');

        Religion::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'active'        => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.religion.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(Religion $religion)
    {
        $this->hasPermissionTo('edit religion');

        return view('hr::master.religion.edit', compact('religion'));
    }

    public function update(MasterRequest $request, Religion $religion)
    {
        $this->hasPermissionTo('edit religion');

        $religion->name         = $request->name;
        $religion->description  = $request->description;
        $religion->active       = $request->active;
        $religion->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.religion.index');
    }

    public function destroy(Religion $religion)
    {
        $this->hasPermissionTo('delete religion');

        $religion->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
