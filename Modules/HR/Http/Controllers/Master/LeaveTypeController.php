<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\LeaveTypeDatatable;
use Modules\HR\Entities\LeaveType;
use Modules\HR\Http\Requests\MasterRequest;

class LeaveTypeController extends Controller
{
    public function index(LeaveTypeDatatable $datatable)
    {
        $this->hasPermissionTo('view leave type');

        return $datatable->render('hr::master.leave_type.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add leave type');

        return view('hr::master.leave_type.create');
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add leave type');

        LeaveType::create([
            'name'          => $request->name,
            'description'   => $request->description,
            'active'        => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.leave-type.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit leave type');

        $leaveType = LeaveType::withoutGlobalScopes()->findOrFail($id);

        return view('hr::master.leave_type.edit', compact('leaveType'));
    }

    public function update(MasterRequest $request, LeaveType $leaveType)
    {
        $this->hasPermissionTo('edit leave type');

        $leaveType->name        = $request->name;
        $leaveType->description = $request->description;
        $leaveType->active      = $request->active;
        $leaveType->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.leave-type.index');
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete leave type');

        LeaveType::withoutGlobalScopes()->findOrFail($id)->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
