<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\CostCenterDatatable;
use Modules\HR\Entities\CostCenter;
use Modules\HR\Http\Requests\CostCenterRequest;

class CostCenterController extends Controller
{
    public function index(CostCenterDatatable $datatable)
    {
        $this->hasPermissionTo('view cost center');

        return $datatable->render('hr::master.cost_center.index');
    }

    public function create(CostCenter $costCenter)
    {
        $this->hasPermissionTo('add cost center');
        return view('hr::master.cost_center.create', compact('costCenter'));
    }

    public function store(CostCenterRequest $request)
    {
        $this->hasPermissionTo('add cost center');

        CostCenter::create([
            'name'                  => $request->name,
            'cost_center_number'    => $request->cost_center_number,
            'description'           => $request->description,
            'active'                => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.cost-center.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(CostCenter $costCenter)
    {
        $this->hasPermissionTo('edit cost center');

        return view('hr::master.cost_center.edit', compact('costCenter'));
    }

    public function update(CostCenterRequest $request, CostCenter $costCenter)
    {
        $this->hasPermissionTo('edit cost center');

        $costCenter->name               = $request->name;
        $costCenter->cost_center_number = $request->cost_center_number;
        $costCenter->description        = $request->description;
        $costCenter->active             = $request->active;
        $costCenter->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.cost-center.index');
    }

    public function destroy(CostCenter $costCenter)
    {
        $this->hasPermissionTo('delete cost center');

        $costCenter->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
