<?php

namespace Modules\HR\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\DepartmentDatatable;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\Division;
use Modules\HR\Http\Requests\MasterRequest;

class DepartmentController extends Controller
{
    public function index(DepartmentDatatable $datatable)
    {
        $this->hasPermissionTo('view department');

        return $datatable->render('hr::master.department.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add department');

        $divisions = Division::all();

        return view('hr::master.department.create', compact('divisions'));
    }

    public function store(MasterRequest $request)
    {
        $this->hasPermissionTo('add department');

        Department::create([
            'division_id'   => $request->division_id,
//            'cost_center_id' => $request->cost_center_id,
            'name'          => $request->name,
            'description'   => $request->description,
            'active'        => $request->active
        ]);

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.department.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(Department $department)
    {
        $this->hasPermissionTo('edit department');

        $divisions = Division::all();
        return view('hr::master.department.edit', compact('department', 'divisions'));
    }

    public function update(MasterRequest $request, Department $department)
    {
        $this->hasPermissionTo('edit department');

        $department->division_id    = $request->division_id;
//        $department->cost_center_id = $request->cost_center_id;
        $department->name           = $request->name;
        $department->description    = $request->description;
        $department->active         = $request->active;
        $department->save();

        flash('Your data has been created')->success();
        return redirect()->route('hr.master.department.index');
    }

    public function destroy(Department $department)
    {
        $this->hasPermissionTo('delete department');

        $department->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
