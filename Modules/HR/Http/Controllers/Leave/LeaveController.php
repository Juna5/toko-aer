<?php

namespace Modules\HR\Http\Controllers\Leave;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\HR\DataTables\LeaveDatatable;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Leave;
use Modules\HR\Entities\LeaveApproval;
use Modules\HR\Entities\LeaveBalance;
use Modules\HR\Entities\LeaveType;
use Modules\Setting\Entities\Workflow;

class LeaveController extends Controller
{
    protected $menu = 'leave';

    public function index(LeaveDatatable $dataTable)
    {
        $this->hasPermissionTo('view leave');

        $leaveBalances = [];
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if ($employee){
            $leaveBalances = LeaveBalance::with('leaveType')
                ->whereEmployeeId($employee->id)
                ->whereHas('leaveType', function ($query) {
                    $query->orderBy('name', 'asc');
                })
                ->get();
        }

        return $dataTable->render('hr::leave.entry.index', compact('leaveBalances'));
    }

    public function create()
    {
        $this->hasPermissionTo('add leave');

        $leaveTypes = LeaveType::all();

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        $leaveBalances = LeaveBalance::with('leaveType')
            ->whereEmployeeId($employee->id)
            ->whereHas('leaveType', function ($query) {
                $query->orderBy('name', 'asc');
            })
            ->get();

        return view('hr::leave.entry.create', compact('leaveTypes', 'leaveBalances'));
    }

    public function store(Request $request)
    {
        $this->hasPermissionTo('add leave');

        DB::beginTransaction();
        try {
            # request
            $fromDate = $request->from_date;
            $untilDate = $request->until_date;
            $userType = auth()->user()->user_type_id;
            $leaveBalance = $request->leave_balance_id;

            # check calendar & leave balance
            $checkCalendar = getWorkingDay($fromDate, $untilDate, $userType);
            $checkLeaveBalance = LeaveBalance::find($leaveBalance);
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            
            #workflow
            $firstApproval = getFirstApproval($this->menu, $employee->department_id, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $employee->department_id, $employee->division_id);

            # validation if choose cuti tahunan
            if ($checkLeaveBalance){
                if (now()->year < Carbon::parse($checkLeaveBalance->from_date)->format('Y')){
                    flash('Failed, cannot choose next year\'s balance.')->error();
                    return back();
                }

                if ($checkCalendar > $checkLeaveBalance->remaining_balance){
                    flash('Failed to create leave, Please check your remaining balance')->error();
                    return back();
                }
            }

            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('leave');
            $code_number = generate_code_sequence('LE', $sequence_number, 3);

            # store database
            $request->request->add([
                'employee_id' => $employee->id,
                'code_number' => $code_number,
                'status' => 'waiting',
                'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
                'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
            ]);
            $leave = Leave::create($request->except('file'));
            if ($request->hasFile('file')) {
                foreach ($request->file as $key) {
                    $leave->addMedia($key)->toMediaCollection('leaves');
                }
            }

            # update cuti tahunan balance
            if ($request->leave_type_id == 1){
                $staffLeaveBalance = $checkLeaveBalance;
                $staffLeaveBalance->used_balance = $checkLeaveBalance->used_balance + $checkCalendar;
                $staffLeaveBalance->remaining_balance = $checkLeaveBalance->remaining_balance - $checkCalendar;
                $staffLeaveBalance->save();
            }

            # leave approval
            foreach ($getApproval as $row) {
                LeaveApproval::create([
                    'leave_id' => $leave->id,
                    'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                    'role' => isset($row->role) ? $row->role : null,
                    'is_approved' => false,
                ]);
            }

        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        flash('Your data has been saved successfully')->success();
        return redirect()->route('hr.leave.entry.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit leave');

        $leave = Leave::findOrFail($id);
        $media = $leave->getMedia('leaves');

        return view('hr::leave.entry.edit-data', compact('leave', 'media'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit leave');

        DB::beginTransaction();
        try {
            # action for resubmit
            if (request()->resubmit) {
                \Approval::resubmit(Leave::class, LeaveApproval::class, $id, 'leave_id', [
                    'reason' => $request->reason,
                    'leaving_indonesia' => $request->leaving_indonesia,
                    'address' => $request->address,
                    'arrangement_cover' => $request->arrangement_cover,
                ]);

                if ($request->hasFile('file')) {
                    foreach ($request->file as $key) {
                        $leaveResubmit->addMedia($key)->toMediaCollection('leaves');
                    }
                }
            } else {
                # updated data
                $leave = Leave::findOrFail($id);
                $leave->reason = $request->reason;
                $leave->leaving_indonesia = $request->leaving_indonesia;
                $leave->address = $request->address;
                $leave->arrangement_cover = $request->arrangement_cover;
                $leave->save();

                if ($request->hasFile('file')) {
                    foreach ($request->file as $key) {
                        $leave->addMedia($key)->toMediaCollection('leaves');
                    }
                }
            }

        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        flash('Your data has been updated successfully')->success();
        return redirect()->route('hr.leave.entry.index');
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete leave');

        $leave = Leave::FindOrFail($id);

        # rollback leave balance
        if ($leave->leave_type_id == 1) {
            # delete approval
            LeaveApproval::where('leave_id', $leave->id)->delete();

            # check calendar & leave balance
            $userType = auth()->user()->user_type_id;
            $checkCalendar = getWorkingDay($leave->from_date, $leave->until_date, $userType);
            $checkLeaveBalance = LeaveBalance::find($leave->leave_balance_id);

            $staffLeaveBalance = $checkLeaveBalance;
            $staffLeaveBalance->used_balance = $checkLeaveBalance->used_balance - $checkCalendar;
            $staffLeaveBalance->remaining_balance = $checkLeaveBalance->remaining_balance + $checkCalendar;
            $staffLeaveBalance->save();
        }

        $leave->delete();

        flash('Your data has been deleted successfully')->error();
        return back();
    }
}
