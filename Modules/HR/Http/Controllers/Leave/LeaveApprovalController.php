<?php

namespace Modules\HR\Http\Controllers\Leave;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\Leave;
use Modules\HR\Entities\LeaveApproval;

class LeaveApprovalController extends Controller
{
    public function index()
    {
        $this->hasPermissionTo('view leave approval');

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if (empty($employee)){
            flash('Ops.. please check your employee id.')->error();
            return back();
        }

        $leaves = filterModuleByStatus(Leave::class, [
            'leaveType', 'employee'
        ]);
        return view('hr::leave.approval.index', compact('leaves'));
    }

    public function create()
    {
        $this->hasPermissionTo('add leave approval');

        return view('hr::create');
    }

    public function store(Request $request)
    {
        $this->hasPermissionTo('add leave approval');
    }

    public function show($id)
    {
        $this->hasPermissionTo('view leave approval');

        $leave = Leave::findOrFail($id);
        $histories = LeaveApproval::with(['employee', 'leave'])
            ->where('leave_id', $leave->id)
            ->get()
            ->sortBy('id');

        return view('hr::leave.approval.modals.detail', compact('leave', 'histories'));
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit leave approval');
        return view('hr::edit');
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit leave approval');

        $description = request('description');
        if (request()->has('approve')) {
            \Approval::action(Leave::class, LeaveApproval::class, $id, 'leave_id', $description);
        } else {
            \Approval::reject(Leave::class, LeaveApproval::class, $id, 'leave_id', $description);
        }

        flash('Your data has been saved successfully')->success();
        return back();
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete leave');
    }
}
