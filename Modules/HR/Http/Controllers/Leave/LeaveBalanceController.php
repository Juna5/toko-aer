<?php

namespace Modules\HR\Http\Controllers\Leave;

use App\Http\Controllers\Controller;
use Modules\HR\DataTables\LeaveBalanceDatatable;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\LeaveBalance;
use Modules\HR\Entities\LeaveType;
use Modules\HR\Http\Requests\LeaveBalanceRequest;

class LeaveBalanceController extends Controller
{
    public function index(LeaveBalanceDatatable $dataTable)
    {
        $this->hasPermissionTo('view leave balance');

        return $dataTable->render('hr::leave.balance.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add leave balance');

        $employees = Employee::all();
        $leaveTypes = LeaveType::all();

        return view('hr::leave.balance.create', compact('employees', 'leaveTypes'));
    }

    public function store(LeaveBalanceRequest $request)
    {
        $this->hasPermissionTo('add leave balance');

        LeaveBalance::create($request->all());

        flash('Your data has been created')->success();
        return redirect()->route('hr.leave.leave-balance.index');
    }

    public function show($id)
    {
        return view('hr::show');
    }

    public function edit(LeaveBalance $leaveBalance)
    {
        $this->hasPermissionTo('edit leave balance');

        $employees = Employee::all();
        $leaveTypes = LeaveType::all();

        return view('hr::leave.balance.edit', compact('leaveBalance', 'employees', 'leaveTypes'));
    }

    public function update(LeaveBalanceRequest $request, LeaveBalance $leaveBalance)
    {
        $this->hasPermissionTo('edit leave balance');

        $leaveBalance->update($request->all());

        flash('Your data has been updated successfully')->success();
        return redirect()->route('hr.leave.leave-balance.index');
    }

    public function destroy(LeaveBalance $leaveBalance)
    {
        $this->hasPermissionTo('delete leave balance');

        $leaveBalance->delete();

        flash('Your data has been deleted')->error();
        return back();
    }
}
