<?php

namespace Modules\HR\Http\Controllers\Leave;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ViewController extends Controller
{
    public function __invoke()
    {
        return view('hr::leave.index');
    }
}
