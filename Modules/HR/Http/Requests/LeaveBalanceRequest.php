<?php

namespace Modules\HR\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeaveBalanceRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'employee_id'   => 'required|integer',
            'leave_type_id'    => 'required|integer',
            'from_date'    => 'required|date',
            'until_date'    => 'required|date',
            'beginning_balance'    => 'required|numeric',
            'used_balance'    => 'required|numeric',
            'remaining_balance'    => 'required|numeric',
        ];
    }
}
