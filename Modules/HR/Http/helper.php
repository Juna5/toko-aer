<?php

use Illuminate\Support\Facades\DB;
use Modules\HR\Entities\Employee;
use Modules\Setting\Entities\Workflow;
use Modules\HR\Entities\ShiftSetting;
use Modules\HR\Entities\StaffAttendance;
use Modules\Setting\Entities\Calendar;

function getWorkingDay($fromDate, $untilDate, $userType)
{
    $getSchoolDay = \Modules\Setting\Entities\Calendar::whereBetween('date', [$fromDate, $untilDate])
        ->where('is_working_day', true)
        ->where('user_type_id', $userType)
        ->count();

    return $getSchoolDay;
}

function getFirstApproval($menu, $department, $division)
{
    $workflow = Workflow::where('application_name', $menu)
        ->where('department_id', $department)
        ->where('division_id', $division)
        ->orderBy('sequence', 'ASC')
        ->first();

    return $workflow;
}

function getAllApproval($menu, $department, $division)
{
    $workflow = Workflow::where('application_name', $menu)
        ->where('department_id', $department)
        ->where('division_id', $division)
        ->orderBy('sequence', 'ASC')
        ->get();

    return $workflow;
}

function employee()
{
    $employee = Employee::where('user_id', auth()->user()->id)->first();

    return $employee ?? null;
}


function getStaffAttendanceStatus($employee, $calendar)
{
    $getAttendance = StaffAttendance::with('employee', 'calendar')
        ->where('employee_id', $employee)
        ->where('calendar_id', $calendar)
        ->first();

    return $getAttendance;
}

function getStatusEmployeeShift($calendar, $type){
    $shiftSetting = ShiftSetting::where('calendar_id', $calendar)->where('description','like','%'.$type.'%')->first();
    return $shiftSetting ? $shiftSetting->employee_id : null;
}



function getAttendanceStatus($employee, $calendar)
{
    $getAttendance = \App\Models\Attendance::with('employee', 'calendar', 'company')
        ->where('employee_id', $employee)
        ->where('calendar_id', $calendar)
        ->first();

    return $getAttendance;
}

function getAttendanceCount($employee, $month, $year)
{
    $calendars = Calendar::where('month_name', $month)
        ->where('year', $year)
        ->get()->pluck('id');

    $getAttendance = StaffAttendance::with('employee', 'calendar')
        ->where('employee_id', $employee)
        ->whereIn('calendar_id', $calendars)
        ->get();

    return count($getAttendance);
}

function getAttendanceLateCount($employee, $month, $year)
{
    $calendars = Calendar::where('month_name', $month)
        ->where('year', $year)
        ->get()->pluck('id');

    $getAttendance = StaffAttendance::with('employee', 'calendar')
        ->where('employee_id', $employee)
        ->whereIn('calendar_id', $calendars)
        ->where('late', '!=', '')
        ->get();

    return count($getAttendance);
}

function getAttendanceLateSum($employee, $month, $year)
{
    $calendars = Calendar::where('month_name', $month)
        ->where('year', $year)
        ->get()->pluck('id');

    $getAttendance = StaffAttendance::with('employee', 'calendar')
        ->where('employee_id', $employee)
        ->whereIn('calendar_id', $calendars)
        ->where('late', '!=', '')
        ->selectRaw("sum(late::time) as total_sum")
        ->get();

    
    return $getAttendance[0]->total_sum;
}

function getQtyReport($depotId, $fromDate, $toDate)
{
    $countQty = DB::select("
        select p.name, sum(si.qty) as total, sum(si.sub_total - COALESCE(si.qty_voucher * 7000,0) ) as totamt, sum(si.qty_voucher) as voucherterpakai 
        from sales_orders SO 
        inner join sales_order_items si on so.id = si.sales_order_id
        inner join products p on p.id = si.product_id
        where p.code like 'G%' 
        and so.cancel_approved_at is null
        and so.date >= '$fromDate'::date
        and so.date <= '$toDate'::date
        and so.depot_id= $depotId
        group by 1
    ");
    
    return $countQty;

}
function getCancelApproveList($depotId)
{
    $countQty = DB::select("
        select * from sales_orders where 
        deleted_at is null 
        and cancel_status = true 
        and cancel_approved_id is null
        and depot_id= $depotId
    ");
    
    return $countQty;

}

function getAmtReport($depotId, $fromDate, $toDate)
{
    $countAmt = DB::select("
        select sum(si.sub_total - COALESCE(si.qty_voucher * 7000,0)) as totamt from sales_orders SO 
        inner join sales_order_items si on so.id = si.sales_order_id
        inner join products p on p.id = si.product_id
        where p.code like 'G%' 
        and so.cancel_approved_at is null
        and so.date >= '$fromDate'::date
        and so.date <= '$toDate'::date
        and so.depot_id= $depotId
        
    ");
    
    return $countAmt;

}

function getLeaderDepot($userID)
{
    $result = DB::select("
        select id, name from warehouse_masters where leader_id = $userID 
    ");
    
    return $result;

}

function getLeaveStatus($employee, $month, $year, $leaveType)
{
    $calendars = Calendar::where('month_name', $month)
        ->where('year', $year)
        ->get();
    $date = $calendars->pluck('date')->toArray();
    $firstDate = reset($date);
    $lastDate = end($date);
  
    if ($leaveType === "Ijin") {
        $leave = 'Ijin Keluar';
        $countLeave = \DB::select("
        select nik, x.empname, sum(numberworkingday) as total from
        (
            select distinct nik,e.name as empname, l.from_date, l.until_date, lt.name,
                 (select count(distinct date) from calendars where is_working_day = true and date between l.from_date and l.until_date ) as numberworkingday
                from employees e
                inner join leaves l on l.employee_id = e.id
                inner join leave_types lt on lt.id = l.leave_type_id 
                where l.from_date >= '$firstDate'::date and l.until_date <= '$lastDate'::date 
                and lt.name = '$leave'
                and e.id = $employee and l.deleted_at is null
                    ) X
            group by 1,2
    ");
    } elseif ($leaveType === "Other") {
        $leave = LeaveType::where('name', '!=', 'Ijin Keluar')->where('name', '!=', 'Cuti Tahunan')->where('name', '!=', 'Alpa')->where('name', '!=', 'Cuti Sakit')->get()->pluck('id');

        $string = '';

        foreach ($leave as $value) {
            $string .=  "'".$value."',";
        }

        $solution = substr($string, 0, -1);
        $countLeave = \DB::select("
        
            select nik, x.empname, sum(numberworkingday) as total from
        (
            select distinct nik,e.name as empname, l.from_date, l.until_date, lt.name,
                 (select count(distinct date) from calendars  where is_working_day = true and  date between l.from_date and l.until_date ) as numberworkingday
                from employees e
                inner join leaves l on l.employee_id = e.id
                inner join leave_types lt on lt.id = l.leave_type_id
                where l.from_date >= '$firstDate'::date and l.until_date <= '$lastDate'::date
               and lt.id in ($solution)
                and e.id =  $employee and l.deleted_at is null
                    ) X
            group by 1,2
    ");
    } else {
        $leave = $leaveType;
        $countLeave = \DB::select("
        select nik, x.empname, sum(numberworkingday) as total from
        (
            select distinct nik,e.name as empname, l.from_date, l.until_date, lt.name,
                 (select count(distinct date) from calendars where is_working_day = true and date between l.from_date and l.until_date ) as numberworkingday
                from employees e
                inner join leaves l on l.employee_id = e.id
                inner join leave_types lt on lt.id = l.leave_type_id 
                where l.from_date >= '$firstDate'::date and l.until_date <= '$lastDate'::date 
                and lt.name = '$leave'
                and e.id = $employee and l.deleted_at is null
                    ) X
            group by 1,2
    ");
    }
   

    return $countLeave ? $countLeave[0]->total : null;
}


