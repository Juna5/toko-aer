<?php

namespace Modules\HR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Setting\Entities\Calendar;

class ShiftSetting extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $with = ['calendar', 'employee'];

    public function calendar()
    {
        return $this->belongsTo(Calendar::class, 'calendar_id')->withoutGlobalScopes();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id')->withoutGlobalScopes();
    }
}
