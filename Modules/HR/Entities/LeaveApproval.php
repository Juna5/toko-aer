<?php

namespace Modules\HR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LeaveApproval extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function leave()
    {
        return $this->belongsTo(Leave::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
