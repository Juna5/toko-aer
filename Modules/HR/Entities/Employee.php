<?php

namespace Modules\HR\Entities;

use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Modules\Procurement\Entities\WarehouseMaster;

class Employee extends Model implements HasMedia
{
    use HasMediaTrait;

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('active', function (Builder $builder) {
            $builder->where('active', 1);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function division()
    {
        return $this->belongsTo(Division::class);
    }

    public function religion()
    {
        return $this->belongsTo(Religion::class);
    }

    public function jobLevel()
    {
        return $this->belongsTo(JobLevel::class);
    }

    public function position()
    {
        return $this->belongsTo(JobPosition::class, 'position_id');
    }

    public function coordinator()
    {
        return $this->belongsTo(Employee::class, 'coordinator_id');
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function regency()
    {
        return $this->belongsTo(Regency::class);
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }
    public function depot()
    {
        return $this->belongsTo(WarehouseMaster::class, 'depot_id');
    }
}
