<?php

namespace Modules\HR\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Setting\Entities\Calendar;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class StaffAttendance extends Model implements HasMedia
{
    use
        SoftDeletes,
        HasMediaTrait;

    protected $guarded = [];

    protected $with = ['calendar', 'employee'];

    public function calendar()
    {
        return $this->belongsTo(Calendar::class, 'calendar_id')->withoutGlobalScopes();
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id')->withoutGlobalScopes();
    }
}
