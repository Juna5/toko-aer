<?php

namespace Modules\Stock\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class StockTransactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add stock', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit stock', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete stock', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view stock', 'created_at' => now()],
        ]);
    }
}
