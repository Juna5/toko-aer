<?php

namespace Modules\Stock\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class StockTransferTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

       Permission::insert([
            ['guard_name' => 'web', 'name' => 'add stock transfer', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit stock transfer', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete stock transfer', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view stock transfer', 'created_at' => now()],
        ]);
    }
}
