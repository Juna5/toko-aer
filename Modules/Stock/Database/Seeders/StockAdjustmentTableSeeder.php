<?php

namespace Modules\Stock\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class StockAdjustmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

       Permission::insert([
            ['guard_name' => 'web', 'name' => 'add stock adjustment', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit stock adjustment', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete stock adjustment', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view stock adjustment', 'created_at' => now()],
        ]);
    }
}
