<?php

namespace Modules\Stock\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Transaction\Entities\DeliveryOrderMaterial;	

class StockTransaction extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function deliveryOrderMaterial()
	{
		return $this->belongsTo(DeliveryOrderMaterial::class,'delivery_order_id');
	}

	public function purchaseOrder()
	{
		return $this->belongsTo(PurchaseOrder::class);
	}

	 public function product()
	{
		return $this->belongsTo(Product::class);
	}

	 public function stock()
	{
		return $this->belongsTo(Stock::class);
	}
}
