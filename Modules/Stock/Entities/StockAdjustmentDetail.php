<?php

namespace Modules\Stock\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\WarehouseMaster;

class StockAdjustmentDetail extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function stockAdjustment()
	{
		return $this->belongsTo(StockAdjustment::class,'stock_adjustment_id');
	}

	public function product()
	{
		return $this->belongsTo(Product::class,'product_id');
	}

	 public function warehouse()
	{
		return $this->belongsTo(WarehouseMaster::class,'warehouse_id');
	}

}
