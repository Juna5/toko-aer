<?php

namespace Modules\Stock\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockAdjustment extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function details()
	{
		return $this->hasMany(StockAdjustmentDetail::class, 'stock_adjustment_id');
	}
}
