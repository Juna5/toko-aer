<?php

namespace Modules\Stock\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\WarehouseMaster;

class StockTransfer extends Model
{
    use SoftDeletes;

    protected $guarded = [];


    public function items()
	{
		return $this->hasMany(StockTransferDetail::class, 'stock_transfer_id');
	}

	public function origin()
	{
		return $this->belongsTo(WarehouseMaster::class, 'origin_id');
	}

	public function destination()
	{
		return $this->belongsTo(WarehouseMaster::class, 'destination_id');
	}
}
