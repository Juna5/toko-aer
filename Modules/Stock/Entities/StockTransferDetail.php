<?php

namespace Modules\Stock\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\WarehouseMaster;

class StockTransferDetail extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function stockTransfer()
	{
		return $this->belongsTo(StockTransfer::class,'stock_transfer_id');
	}

	public function product()
	{
		return $this->belongsTo(Product::class,'product_id');
	}

	 public function warehouseOrigin()
	{
		return $this->belongsTo(WarehouseMaster::class,'warehouse_id_origin');
	}

	 public function warehouseDestination()
	{
		return $this->belongsTo(WarehouseMaster::class,'warehouse_id_destination');
	}


}
