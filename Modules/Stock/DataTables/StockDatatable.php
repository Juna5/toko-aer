<?php

namespace Modules\Stock\DataTables;

use Modules\HR\Entities\Warehouse;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class StockDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('stock.stock.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('stock.stock.destroy', $row->id) . '" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal" style="margin-left: 10px; color: white !important;"><i class="fa fa-trash"></i></a>';
                return (userCan('edit stock') ? $edit : '') . (userCan('delete stock') ? $delete : '');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StockTransactionDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Stock $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('stockdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons([
                        Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            Column::make('warehouse_id')->title('IN')->addClass('text-center'),
            Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'StockTransaction_' . date('YmdHis');
    }
}
