<?php

namespace Modules\Stock\DataTables;

use Modules\Stock\Entities\StockAdjustment;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class StockAdjustmentDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)


             ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('stock.adjustment-detail', $row->id) . '">' . $row->code_number . '</a>';
                return $detail;
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('stock.stock-adjustment.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('stock.stock-adjustment.destroy', $row->id) . '" style="margin-left: 10px" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash text-white"></i></a>';
                return (userCan('edit stock adjustment') ? $edit : '') . (userCan('delete stock adjustment') ? $delete : '');
            })
            ->editColumn('date', function($row){
                return format_d_month_y($row->date);
            })
             ->rawColumns(['action','code_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StockTransactionDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(StockAdjustment $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('stockadjusmentdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons([
                        Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [


            Column::make('code_number'),
            Column::make('date')->addClass('text-center'),
            Column::make('description')->addClass('text-center'),

              Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'StockAdjusment_' . date('YmdHis');
    }
}
