<?php

namespace Modules\Stock\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Modules\Stock\Entities\StockTransaction;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;

class StockCheckingOutstandingDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.sales_order.detail', $row->id) . '" target="_blank">' . $row->code_number . '</a>';
                return $detail;
            })
            ->editColumn('department_id', function ($row) {
                return optional($row->department)->name;
            })
            ->editColumn('customer_id', function ($row) {
                $customer = optional($row->customer)->company_name . ' - ' . optional($row->customer)->pic_1;
                return $customer;
            })
            ->editColumn('salesman_id', function ($row) {
                return optional($row->salesman)->name;
            })
            ->editColumn('payment_term_id', function ($row) {
                return optional($row->paymentTerm)->name;
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee ? optional($row->employee)->name : '-';
            })
            ->addColumn('action', function ($row) {
                
                $openDiv = '<div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';
                $stockChecking = "";
                    
                        $stockChecking = '<a href="' . route('stock.stock-checking.create', ['id' => $row->id]) . '"
                        style="margin-left: 10px" class="dropdown-item" title="Create Stock Checking">
                        <i class="fa fa-file" style="color: #046ea8"></i> Stock Checking
                        </a>';
                    
                return $openDiv . $stockChecking . $closeDiv;
            })
            ->rawColumns(['action', 'status', 'code_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StockCheckingOutstandingDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(SalesOrder $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        return $model
            ->with(['items'])
            // ->where('employee_id', $employee->id)
            ->whereHas('items', function($item){
                $item->whereNull('stock_qty');
            })
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('stockcheckingoutstandingdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons([
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('customer_id')->title('Customer')->addClass('text-center'),
            Column::make('salesman_id')->title('Salesman')->addClass('text-center'),
           
            Column::make('payment_term_id')->title('Payment Term')->addClass('text-center'),
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'StockCheckingOutstanding_' . date('YmdHis');
    }
}
