<?php

namespace Modules\Stock\DataTables;

use Modules\Stock\Entities\StockTransaction;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\HR\Entities\Employee;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class StockInDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->items[0]->updated_at);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('transaction.sales_order.detail', $row->id) . '">' . $row->code_number . '</a>';
                return $row->code_number;
            })
            ->editColumn('department_id', function ($row) {
                return optional($row->department)->name;
            })
            ->editColumn('supplier_id', function ($row) {
                $customer = optional($row->supplier)->company_name ;
                return $customer;
            })
            // ->editColumn('salesman_id', function ($row) {
            //     return optional($row->salesman)->name;
            // })
            ->editColumn('employee_id', function ($row) {
                return $row->employee ? optional($row->employee)->name : '-';
            })
            ->editColumn('payment_term_id', function ($row) {
                return optional($row->paymentTerm)->name;
            })

            ->addColumn('action', function ($row) {
                
                $edit = '<a href="' . route('stock.stock-in.edit', $row->id) . '" style="margin-left: 10px" class="btn btn-info" title="Edit"><i class="fa fa-pencil-alt"  aria-hidden="true"></i></a>';
               $delete = '<a data-href="' . route('stock.stock-in.destroy', $row->id) . '" style="margin-left: 10px" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash text-white"></i></a>';
               return (userCan('edit stock transfer') ? $edit : '') . (userCan('delete stock transfer') ? $delete : '');
            })
            ->rawColumns(['action', 'status', 'code_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\StockCheckingDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseOrder $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        return $model
            ->with(['items'])
            // ->where('employee_id', $employee->id)
            ->whereHas('items', function($item){
                $item->whereNotNull('stock_qty');
            })->orderBy('updated_at','desc')
            // ->
            // latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('stockcheckingdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons([
                        Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
       return [
            Column::make('code_number')->title('PO Number')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('supplier_id')->title('Supplier')->addClass('text-center'),
            // Column::make('salesman_id')->title('Salesman')->addClass('text-center'),
           
            Column::make('payment_term_id')->title('Payment Term')->addClass('text-center'),

            
           
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'StockChecking_' . date('YmdHis');
    }
}
