<?php

use Illuminate\Support\Facades\DB;
use Modules\HR\Entities\Employee;
use Modules\Setting\Entities\Workflow;
use Modules\Stock\Entities\Stock;
use Modules\Procurement\Entities\Product;

function getTotalStock($product_id, $warehouse_id, $type)
{
    $stock = \Modules\Stock\Entities\StockTransaction::where('product_id', $product_id)->whereHas('stock',function($stocks) use($warehouse_id){
		$stocks->where('warehouse_id', $warehouse_id);
	})->where('type', $type)->selectRaw('sum(qty) as total_sum')->get();
    
    return $stock[0]->total_sum;
}

function getTotal($product_id, $warehouse_id)
{
	// $po = getTotalStock($product_id, $warehouse_id, 'IN');
	// $do = getTotalStock($product_id, $warehouse_id, 'OUT');
	// $total = $po - $do;
	$stocks = Stock::where('warehouse_id', $warehouse_id)->where('product_id', $product_id)->get()->sum('qty');

	return $stocks;
}

function getTotalStockWarehouse($warehouse_id, $type)
{
	$stock = \Modules\Stock\Entities\StockTransaction::with('stock')->whereHas('stock',function($stocks) use($warehouse_id){
		$stocks->where('warehouse_id', $warehouse_id);
	})->where('type', $type)->selectRaw('sum(qty) as total_sum')->get();
    
    return $stock[0]->total_sum;
}

function getTotalWarehouse($warehouse_id)
{
	$po = getTotalStockWarehouse($warehouse_id, 'IN');
	$do = getTotalStockWarehouse($warehouse_id, 'OUT');
	$total = $po - $do;
	
	return $total;
}


