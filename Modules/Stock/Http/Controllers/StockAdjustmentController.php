<?php

namespace Modules\Stock\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Stock\DataTables\StockAdjustmentDatatable;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Stock\Entities\StockAdjustment;
use Modules\Stock\Entities\StockAdjustmentDetail;
use Modules\Procurement\Entities\Product;
use Modules\Stock\Entities\StockTransaction;
use Modules\Stock\Entities\Stock;
use Carbon\Carbon;

class StockAdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(StockAdjustmentDatatable $datatable)
    {
        $this->hasPermissionTo('view stock adjustment');

        return $datatable->render('stock::stock_adjustment.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add stock adjustment');
        $product = Product::all();
        $warehouse =  WarehouseMaster::all();

        return view('stock::stock_adjustment.create', compact('product','warehouse'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
        $product = request()->product_id;
        $warehouse = request()->warehouse_id;
        $qty  = request()->qty;
        $type = request()->type;
        $date = request()->date;
        $desc = request()->description;
        # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('stock adjustment');
            $code_number = generate_code_sequence('SA', $sequence_number, 3);
            // dd($code_number);
            $stockAdjustment = StockAdjustment::create(
                [
                    'date' => $date,
                    'code_number' => $code_number,
                    'description' => $desc
                ]
            );

        foreach(array_filter($product) as $key => $value)
        {
            $data['product_id'] = $product[$key];

            $data['warehouse_id'] = $warehouse[$key];
            $data['qty'] = $qty[$key];
            $data['type'] = $type[$key];



            if($stockAdjustment){

                $stockTransaction = StockTransaction::where('product_id', $data['product_id'])->where('warehouse_id', $data['warehouse_id'])->get();
                foreach($stockTransaction as $st){
                    $st->qty = 0;
                    $st->in_hand = 0;
                    $st->save();
                }

                $stockTrans = new StockTransaction();
                $stockTrans->type = $data['type'];
                $stockTrans->qty = $data['qty'];
                $stockTrans->description   ='Stock Adjustment';
                $stockTrans->date = Carbon::now();
                $stockTrans->product_id = $data['product_id'];
                $stockTrans->warehouse_id = $data['warehouse_id'];
                $stockTrans->status = true;
                $stockTrans->in_hand = $data['qty'];

                $stock = Stock::where('product_id',$data['product_id'])->where('warehouse_id', $data['warehouse_id'])->first();


                 if($stock){
                    $stock->qty = $data['qty'];
                    $stock->save();
                    $stockTrans->stock_id = $stock->id;
                    $stockTrans->save();

                }else{
                    $stock = new Stock();
                    $stock->product_id = $data['product_id'];
                    $stock->warehouse_id = $data['warehouse_id'];
                    $stock->qty = $data['qty'];
                    $stock->status = true;
                    $stock->save();


                    $stockTrans->stock_id = $stock->id;
                    $stockTrans->save();
                }




                StockAdjustmentDetail::create(
                    [
                        'stock_adjustment_id' => $stockAdjustment->id,
                        'product_id'          => $data['product_id'],
                        'warehouse_id'        => $data['warehouse_id'],
                        'type'                => $data['type'],
                        'qty'                 => $data['qty'],
                    ]
                );
            }

        }
         DB::commit();
            noty()->success('Yeay!', 'Your entry has been added successfully');
            return redirect()->route('stock.stock-adjustment.index');
         } catch(\Exception $e) {
            DB::rollback();
            noty()->danger('Oops!', 'Please try again');
            return redirect()->route('stock.stock-adjustment.create');
        }



    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('stock::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $stockAdjustment = StockAdjustment::find($id);
        $item          = StockAdjustmentDetail::with('product','warehouse')->where('stock_adjustment_id',$id)->get();
        $product = Product::all();
        $warehouse =  WarehouseMaster::all();

        return view('stock::stock_adjustment.edit', compact('stockAdjustment','item','product','warehouse'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
        $product = request()->product_id;
        $warehouse = request()->warehouse_id;
        $qty  = request()->qty;
        $type = request()->type;
        $date = request()->date;
        $desc = request()->description;
        $detail_id = request()->detail_id;
        # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('stock adjustment');
            $code_number = generate_code_sequence('SA', $sequence_number, 3);
            // dd($code_number);
            $stockAdjustment = StockAdjustment::find($id);
            $stockAdjustment->date = $date;
            $stockAdjustment->description = $desc;
            $stockAdjustment->save();
        if($detail_id){
            foreach(array_filter($detail_id) as $key => $value)
            {
                $data['product_id'] = $product[$key];

                $data['warehouse_id'] = $warehouse[$key];
                $data['qty'] = $qty[$key];
                $data['type'] = $type[$key];
                $data['id']     = $detail_id[$key];



                if($stockAdjustment){
                    $stockTransaction = StockTransaction::where('product_id', $data['product_id'])->where('warehouse_id', $data['warehouse_id'])->get();
                    foreach($stockTransaction as $st){
                        $st->qty = 0;
                        $st->in_hand = 0;
                        $st->save();
                    }

                    $stockTrans = new StockTransaction();
                    $stockTrans->type = $data['type'];
                    $stockTrans->qty = $data['qty'];
                    $stockTrans->description   ='Stock Adjustment';
                    $stockTrans->date = Carbon::now();
                    $stockTrans->product_id = $data['product_id'];
                    $stockTrans->warehouse_id = $data['warehouse_id'];
                    $stockTrans->status = true;
                    $stockTrans->in_hand = $data['qty'];

                    $stock = Stock::where('product_id',$data['product_id'])->where('warehouse_id', $data['warehouse_id'])->first();
                     if($stock){
                        $stock->qty = $data['qty'];
                        $stock->save();
                        $stockTrans->stock_id = $stock->id;
                        $stockTrans->save();

                    }else{
                        $stock = new Stock();
                        $stock->product_id = $data['product_id'];
                        $stock->warehouse_id = $data['warehouse_id'];
                        $stock->qty = $data['qty'];
                        $stock->status = true;
                        $stock->save();


                        $stockTrans->stock_id = $stock->id;
                        $stockTrans->save();
                    }

                    $stockDetail = StockAdjustmentDetail::find($data['id']);
                    $stockDetail->product_id = $data['product_id'];
                    $stockDetail->warehouse_id = $data['warehouse_id'];
                    $stockDetail->type  =$data['type'];
                    $stockDetail->qty = $data['qty'];
                    $stockDetail->save();
                }

            }
        }else{
            foreach(array_filter($product) as $key => $value)
            {
                $data['product_id'] = $product[$key];

                $data['warehouse_id'] = $warehouse[$key];
                $data['qty'] = $qty[$key];
                $data['type'] = $type[$key];



                if($stockAdjustment){
                    $stockTransaction = StockTransaction::where('product_id', $data['product_id'])->where('warehouse_id', $data['warehouse_id'])->get();
                    foreach($stockTransaction as $st){
                        $st->qty = 0;
                        $st->in_hand = 0;
                        $st->save();
                    }

                    $stockTrans = new StockTransaction();
                    $stockTrans->type = $data['type'];
                    $stockTrans->qty = $data['qty'];
                    $stockTrans->description   ='Stock Adjustment';
                    $stockTrans->date = Carbon::now();
                    $stockTrans->product_id = $data['product_id'];
                    $stockTrans->warehouse_id = $data['warehouse_id'];
                    $stockTrans->status = true;
                    $stockTrans->in_hand = $data['qty'];

                    $stock = Stock::where('product_id',$data['product_id'])->where('warehouse_id', $data['warehouse_id'])->first();

                     if($stock){
                        $stock->qty = $data['qty'];
                        $stock->save();
                        $stockTrans->stock_id = $stock->id;
                        $stockTrans->save();

                    }else{
                        $stock = new Stock();
                        $stock->product_id = $data['product_id'];
                        $stock->warehouse_id = $data['warehouse_id'];
                        $stock->qty = $data['qty'];
                        $stock->status = true;
                        $stock->save();


                        $stockTrans->stock_id = $stock->id;
                        $stockTrans->save();
                    }

                    $stockDetail = new StockAdjustmentDetail();
                    $stockDetail->product_id = $data['product_id'];
                    $stockDetail->warehouse_id = $data['warehouse_id'];
                    $stockDetail->type  =$data['type'];
                    $stockDetail->qty = $data['qty'];
                    $stockDetail->save();
                }

            }
        }
         DB::commit();
            noty()->success('Yeay!', 'Your entry has been updated successfully');
            return redirect()->route('stock.stock-adjustment.index');
         } catch(\Exception $e) {
            DB::rollback();

            flash('Error processing request'.$e)->error();
            return redirect()->route('stock.stock-adjustment.edit',$id);
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete stock adjustment');

         $row = StockAdjustment::findOrFail($id);
         $detail = StockAdjustmentDetail::where('stock_adjustment_id', $row->id)->get();

         foreach($detail as $value){
            $stock = Stock::where('product_id', $value->product_id)->where('warehouse_id', $value->warehouse_id)->first();
            if($stock){
                $stock->qty = (int)$stock->qty - (int)$value->qty;
                $stock->save();

                $stockTransaction = StockTransaction::where('product_id',$value->product_id)->where('warehouse_id', $value->warehouse_id)->where('description','like','%Stock Adjustment%')->get();
                $qtyTotal = getTotal($value->product_id, $value->warehouse_id);

                if($stockTransaction){
                    foreach ($stockTransaction as $key => $trans) {
                        $trans->delete();
                    }
                }
            }

            $value->delete();

         }

         $row->delete();

         noty()->danger('Cool!', 'Your entry has been deleted');
         return redirect()->route('stock.stock-adjustment.index');
    }

    public function detail($id)
    {
        $stockAdjustment = StockAdjustment::find($id);
        $item          = StockAdjustmentDetail::with('product','warehouse')->where('stock_adjustment_id',$id)->get();

        return view('stock::stock_adjustment.detail', compact('stockAdjustment','item'));

    }

    public function report()
    {
        $row = StockAdjustment::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $row->whereBetween('date', [$fromDate, $untilDate])->orderBy('id', 'desc')->paginate(10);
        }

        return view('stock::stock_adjustment.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $row = StockAdjustment::findOrFail($id);
        $items = StockAdjustmentDetail::where('stock_adjustment_id', $id)->get();

        return view('stock::stock_adjustment.report.detail', compact('row', 'items'));
    }
}
