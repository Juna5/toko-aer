<?php

namespace Modules\Stock\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Stock\DataTables\StockInDatatable;
use Modules\Stock\Http\Requests\StockInRequest;
use Maatwebsite\Excel\Facades\Excel;


class StockInController extends Controller
{
    protected $menu = 'stock in';

    public function index(StockInDatatable $datatable)
    {
        $this->hasPermissionTo('view stock in');
        return $datatable->render('stock::stock.stock_in.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
     $this->hasPermissionTo('add stock in');

     return view('stock::stock.stock_in.create');
 }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StockInRequest $request)
    {


        $this->hasPermissionTo('add stock in');
        DB::beginTransaction();
        try {
            foreach ($request->products as $row) {
                if ($row['confirmQty'] > $row['purchaseQty']) {
                    return response()->json([
                        'success' => false,
                        'redirect' => route('stock.stock-in.create')
                    ]);
                }
            }


            foreach ($request->products as $row) {
                if (!$row['warehouse']) {
                    return response()->json([
                        'success' => 'notselectwarehouse',
                        'redirect' => route('stock.stock-in.create')
                    ]);
                }

                $poItem = PurchaseOrderItem::findOrFail($row['purchase_order_item_id']);
                
                $poItem->stock_qty = $row['confirmQty'];
                $poItem->warehouse_id = $row['warehouse'];
                $poItem->save();

                $stock = Stock::where('product_id', $poItem->product_id)->where('warehouse_id', $row['warehouse'])->first();


                if($stock){
                    $stock->qty = $stock->qty + $row['confirmQty'];
                    $stock->save();

                    $qtyTotal = getTotal($poItem->product_id, $row['warehouse']);
                    
                    $stockTransaction = new StockTransaction();
                    $stockTransaction->product_id = $poItem->product_id;
                    $stockTransaction->warehouse_id = $row['warehouse'];
                    $stockTransaction->purchase_order_id = $poItem->purchase_order_id;
                    $stockTransaction->date = \Carbon\Carbon::now();
                    $stockTransaction->type = 'IN';
                    $stockTransaction->qty = $row['confirmQty'];
                    $stockTransaction->status = true;
                    $stockTransaction->description = 'Stock In';
                    $stockTransaction->stock_id = $stock->id;
                    $stockTransaction->in_hand = $qtyTotal + $row['confirmQty'];
                    $stockTransaction->save();

                }else{
                    $stock = new Stock();
                    $stock->product_id = $poItem->product_id;
                    $stock->warehouse_id = $row['warehouse'];
                    $stock->qty = $row['confirmQty'];
                    $stock->status = true;
                    $stock->save();

                    $qtyTotal = getTotal($poItem->product_id, $row['warehouse']);

                    $stockTransaction = new StockTransaction();
                    $stockTransaction->product_id = $poItem->product_id;
                    $stockTransaction->warehouse_id = $row['warehouse'];
                    $stockTransaction->purchase_order_id = $poItem->purchase_order_id;
                    $stockTransaction->date = \Carbon\Carbon::now();
                    $stockTransaction->type = 'IN';
                    $stockTransaction->qty = $row['confirmQty'];
                    $stockTransaction->status = true;
                    $stockTransaction->description = 'Stock In';
                    $stockTransaction->stock_id = $stock->id;
                    $stockTransaction->in_hand = $qtyTotal + $row['confirmQty'];
                    $stockTransaction->save();
                }

            }

        } catch(\Exception $e)
        {
            DB::rollback();
            noty()->danger('Oops!', 'Please try again');
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
            'success' => true,
            'redirect' => route('stock.stock-in.index')
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id, StockInDatatable $datatable)
    {
        $this->hasPermissionTo('view stock in');
        return $datatable->render('stock::stock.stock_in.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('stock::stock.stock_in.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit stock in');

        DB::beginTransaction();
        try {
            foreach ($request->products as $row) {
                if ($row['confirmQty'] > $row['purchaseQty']) {
                    return response()->json([
                        'success' => false,
                        'redirect' => route('stock.stock-in.create')
                    ]);
                }
            }
            
            foreach ($request->products as $row) {


                // if ($row->purchase_order_item_id) {

                    // if (array_key_exists('id', $row)) {

                $poItem = PurchaseOrderItem::findOrFail($row['purchase_order_item_id']);

                $stock = Stock::where('product_id', $poItem->product_id)->where('warehouse_id', $row['warehouse'])->first();
                $stock->qty = $stock->qty - $poItem->stock_qty;
                $stock->save();

                $poItem->stock_qty = $row['confirmQty'];
                $poItem->warehouse_id = $row['warehouse'];
                $poItem->save();

                $stocks = Stock::where('product_id', $poItem->product_id)->where('warehouse_id', $row['warehouse'])->first();
                $stocks->qty = $stocks->qty + $row['confirmQty'];
                $stocks->save();
                if($stock){


                    $qtyTotal = getTotal($poItem->product_id, $row['warehouse']);

                    $stockTransaction = StockTransaction::where('purchase_order_id',$poItem->purchase_order_id)->where('product_id',$poItem->product_id)->where('warehouse_id', $row['warehouse'])->first();


                    if($stockTransaction){
                       $stockTransaction->warehouse_id = $row['warehouse'];
                       $stockTransaction->date = \Carbon\Carbon::now();
                       $stockTransaction->type = 'IN';
                       $stockTransaction->qty = $row['confirmQty'];
                       $stockTransaction->status = true;
                       $stockTransaction->description = 'Stock In';
                       $stockTransaction->stock_id = $stock->id;
                       $stockTransaction->in_hand = $qtyTotal + $row['confirmQty'];
                       $stockTransaction->save();

                   }else{
                     

                     $stockTransaction = new StockTransaction();
                     $stockTransaction->product_id = $poItem->product_id;
                     $stockTransaction->warehouse_id = $row['warehouse'];
                     $stockTransaction->purchase_order_id = $poItem->purchase_order_id;
                     $stockTransaction->date = \Carbon\Carbon::now();
                     $stockTransaction->type = 'IN';
                     $stockTransaction->qty = $row['confirmQty'];
                     $stockTransaction->status = true;
                     $stockTransaction->description = 'Stock In';
                     $stockTransaction->stock_id = $stock->id;
                     $stockTransaction->in_hand = $qtyTotal + $row['confirmQty'];
                     $stockTransaction->save();      
                 }



             }else{
                $stock = new Stock();
                $stock->product_id = $poItem->product_id;
                $stock->warehouse_id = $row['warehouse'];
                $stock->qty = $row['confirmQty'];
                $stock->status = true;
                $stock->save();

                $qtyTotal = getTotal($poItem->product_id, $row['warehouse']);

                $stockTransaction = StockTransaction::where('purchase_order_id',$poItem->purchase_order_id)->where('product_id',$poItem->product_id)->where('warehouse_id', $row['warehouse'])->first();
                if($stockTransaction){
                   $stockTransaction->warehouse_id = $row['warehouse'];
                   $stockTransaction->date = \Carbon\Carbon::now();
                   $stockTransaction->type = 'IN';
                   $stockTransaction->qty = $row['confirmQty'];
                   $stockTransaction->status = true;
                   $stockTransaction->description = 'Stock In';
                   $stockTransaction->stock_id = $stock->id;
                   $stockTransaction->in_hand = $qtyTotal + $row['confirmQty'];
                   $stockTransaction->save();
               }else{

                 $stockTransaction = new StockTransaction();
                 $stockTransaction->product_id = $poItem->product_id;
                 $stockTransaction->warehouse_id = $row['warehouse'];
                 $stockTransaction->purchase_order_id = $poItem->purchase_order_id;
                 $stockTransaction->date = \Carbon\Carbon::now();
                 $stockTransaction->type = 'IN';
                 $stockTransaction->qty = $row['confirmQty'];
                 $stockTransaction->status = true;
                 $stockTransaction->description = 'Stock In';
                 $stockTransaction->stock_id = $stock->id;
                 $stockTransaction->in_hand = $qtyTotal + $row['confirmQty'];
                 $stockTransaction->save();      
             }


                 // }
             // }
   }
}

}
catch(\Exception $e)
{
    DB::rollback();
    noty()->danger('Oops!', 'Please try again');
    return back();
}
DB::commit();

noty()->success('Yeay!', 'Your entry has been updated successfully');
return response()->json([
    'success' => true,
    'redirect' => route('stock.stock-in.index')
]);
}

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
     $this->hasPermissionTo('delete stock in');

     $po = PurchaseOrder::findOrFail($id);
     if($po){
        $item = PurchaseOrderItem::where('purchase_order_id', $po->id)->get();
        foreach ($item as $key => $value) {
            $stock = Stock::where('product_id', $value->product_id)->where('warehouse_id', $value->warehouse_id)->first();
            if($stock){
                $stock->qty = (int)$stock->qty - (int)$value->stock_qty;
                $stock->save();

                $stockTransaction = StockTransaction::where('purchase_order_id',$id)->where('product_id',$value->product_id)->where('warehouse_id', $value->warehouse_id)->get();
                $qtyTotal = getTotal($value->product_id, $value->warehouse_id);

                if($stockTransaction){
                    foreach ($stockTransaction as $key => $trans) {
                        $trans->delete();
                    }
                }
            }
            $value->stock_qty = null;
            $value->warehouse_id = null;
            $value->save();

        }
    }

    noty()->danger('Cool!', 'Your data has been deleted');
    return redirect()->route('stock.stock-in.index');
}

public function detail($id)
{
 $row = DeliveryOrderMaterial::findOrFail($id);
 $items = DeliveryOrderMaterialItem::with('salesOrderMaterialItem.product', 'salesOrderMaterialItem')->where('delivery_order_material_id', $id)->get();
 $approvals = DeliveryOrderMaterialApproval::where('delivery_order_material_id', $id)->orderBy('id', 'asc')->get();
 if($row){
    $media = $row->getMedia('delivery_order_material');
}else{
    $media = [];
}

return view('transaction::delivery_order_material.entry.detail', compact('row', 'items', 'approvals', 'media'));
}

public function salesOrderMaterial()
{
    $employee = Employee::where('user_id', auth()->user()->id)->first()->id;

    $row = SalesOrder::with('items')->where('employee_id', $employee)
    ->where('status', 'approved')
    ->whereHas('items', function ($q){
        $q->where('delivered_qty', '>' ,0);
    })
    ->get();
    return $row;
}

public function warehouse()
{
    $row = WarehouseMaster::all();
    return $row;
}

public function salesOrderMaterialItem()
{
    return SalesOrder::with(['department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])
    ->whereHas('items', function ($q) {
        $q->where('status', true);
    })
    ->find(request()->sales_order_id);
}

public function salesOrderMaterials(){
    return SalesOrder::with(['department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])->where('status','approved')
    ->whereHas('items', function ($q) {
        $q->where('status', true)->whereNull('stock_qty');
    })
    ->get();
}

    public function report()
    {
        $row = PurchaseOrder::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $row->whereBetween('date', [$fromDate, $untilDate])->orderBy('id', 'desc')->paginate(10);
        }

        return view('stock::stock.stock_in.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        
         return view('stock::stock.stock_in.report.detail', compact('row', 'items'));
    }

}
