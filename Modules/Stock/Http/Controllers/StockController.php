<?php

namespace Modules\Stock\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Stock\Entities\Stock;
use Modules\Procurement\Entities\Product;
use Modules\Stock\Entities\StockTransaction;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Modules\Transaction\Entities\DeliveryOrderMaterialItem;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class StockController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $this->hasPermissionTo('view stock');
        $warehouse = WarehouseMaster::all();
        return view('stock::stock.index', compact('warehouse'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add stock');
        $warehouse = WarehouseMaster::find(request()->warehouse_id);
        $purchaseOrder = PurchaseOrder::all();
        return view('stock::stock.create', compact('warehouse','purchaseOrder'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->hasPermissionTo('add stock');
        DB::beginTransaction();
        try {
        $warehouses = request()->warehouse_id; 
        $warehouse = WarehouseMaster::find(request()->warehouse_id); 
        // dd($warehouse);
        $type      = request()->type;
        $id_po_do  = request()->code_number;
        $checkeds  = request()->checkeds;

        foreach(array_filter($checkeds) as $key => $value)

        {
              $data['product_id'] = request('productId_'.$value);
                    $data['qty'] = request('qty_'.$value);
                    $data['description'] = request('description_'.$value);
                    $data['warehouse_id'] = $warehouses;
                    
                        $stockTemp  = Stock::updateOrCreate(
                                [
                                    'product_id' => $data['product_id'],
                                    'warehouse_id' => $warehouses,
                                ],
                                [
                                    
                                ]);
                            
                    
                   if($type == "IN"){
                        $qtyTotal = getTotal($data['product_id'], $warehouses);
                        $stock = StockTransaction::create(
                        [
                            'product_id'            => $data['product_id'],
                            'qty'                   => $data['qty'],
                            'description'           => $data['description'],
                            'warehouse_id'          => $data['warehouse_id'],
                            'purchase_order_id'     => $id_po_do,
                            'date'                  => date('Y-m-d'),
                            'type'                  => $type,
                            'in_hand'               => $qtyTotal +  $data['qty'],
                            'stock_id'              => $stockTemp->id,

                        ]);
                        $totalQty = getTotal($data['product_id'], $warehouses);
                        Stock::updateOrCreate(
                                [
                                    'product_id' => $data['product_id'],
                                    'warehouse_id' => $warehouses,
                                ],
                                [
                                     'qty' => $stock->in_hand,
                                ]);
                            
                  }else{
                    $qtyTotal = getTotal($data['product_id'], $warehouses);
                    if($data['qty'] > $qtyTotal){
                        flash('Error processing request, insufficient stock, please input first')->error();
                        return redirect()->route('stock.stock.create', ['warehouse_id' => $warehouses, $warehouse]);
                    }
                       $stock = StockTransaction::create(
                        [
                            'product_id'            => $data['product_id'],
                            'qty'                   => $data['qty'],
                            'description'           => $data['description'],
                            'warehouse_id'          => $data['warehouse_id'],
                            'delivery_order_id'     => $id_po_do,
                            'date'                  => date('Y-m-d'),
                            'type'                  => $type,
                            'in_hand'               => $qtyTotal - $data['qty'],
                            'stock_id'              => $stockTemp->id,
                            
                        ]);
                       $totalQty = getTotal($data['product_id'], $warehouses);
                        Stock::updateOrCreate(
                                [
                                    'product_id' => $data['product_id'],
                                    'warehouse_id' => $warehouses,
                                ],
                                [
                                    'qty' => $stock->in_hand,
                                ]);
                }
        }
           
            DB::commit();
            noty()->success('Yeay!', 'Your entry has been added successfully');
            return redirect()->route('stock.stock.show', $warehouses);
         } catch(\Exception $e) {
            DB::rollback();

            flash('Error processing request'.$e)->error();
            return redirect()->route('stock.stock.show', $warehouses);
        }
      
              
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {    $this->hasPermissionTo('view stock'); 
        $product = DB::select("SELECT product_id as product_id, stocks.warehouse_id as warehouse_id,products.name as name FROM stocks join products on products.id = stocks.product_id WHERE stocks.warehouse_id = '$id' group by product_id, stocks.warehouse_id,products.name");

        
        $products = DB::select("SELECT product_id as product_id, stock_transactions.warehouse_id as warehouse_id,products.name as name FROM stock_transactions join products on products.id = stock_transactions.product_id WHERE stock_transactions.warehouse_id = '$id' group by product_id, stock_transactions.warehouse_id,products.name");

        $warehouse = WarehouseMaster::find($id);
        return view('stock::stock.show', compact('id', 'warehouse','product','products'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('stock::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function codeNumber(){
       $type = request()->type;
      
       if($type == "IN"){
            $data = PurchaseOrder::where('status','approved')->get();
       }elseif($type == "OUT"){
            $data = DeliveryOrderMaterial::where('status','approved')->get();
       }        

       return response()->json($data);
    }

     public function productList(){
       $id = request()->id;
       $type = request()->type;
       if($type == "IN"){
            $data = PurchaseOrderItem::with('purchaseOrder','product')->where('purchase_order_id', $id)->get() ?? [];
       }elseif($type == "OUT"){
            $data = DeliveryOrderMaterialItem::with('deliveryOrderMaterial','salesOrderMaterialItem.product')->where('delivery_order_material_id', $id)->get() ?? [];
       }

       return response()->json($data);
    }

}
