<?php

namespace Modules\Stock\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;
use Barryvdh\DomPDF\Facade as PDF;
use App\Http\Controllers\Controller;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Transaction\Entities\QuotationMaterialItem;
use Modules\Stock\DataTables\StockCheckingDatatable;
use Modules\Stock\DataTables\StockCheckingOutstandingDatatable;
use Modules\Stock\Http\Requests\StockCheckingRequest;
use Maatwebsite\Excel\Facades\Excel;


class StockCheckingController extends Controller
{
    protected $menu = 'stock checking';

    public function index(StockCheckingDatatable $datatable)
    {
        $this->hasPermissionTo('view stock');
        return $datatable->render('stock::stock.stock_checking.index');
    }

    public function outstanding(StockCheckingOutstandingDatatable $datatable)
    {
        $this->hasPermissionTo('view stock');
        return $datatable->render('stock::stock.stock_checking.index_outstanding');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
     public function create()
     {
         $this->hasPermissionTo('add stock');

         return view('stock::stock.stock_checking.create');
     }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(StockCheckingRequest $request)
    {


        // $this->hasPermissionTo('add stock');
        DB::beginTransaction();
        try {
            foreach ($request->products as $row) {
                if ($row['confirmQty'] > $row['purchaseQty']) {
                    return response()->json([
                    'success' => false,
                    'redirect' => route('stock.stock-checking.create')
                    ]);
                }
            }

            foreach ($request->products as $row) {
                //update Sales Order Item
                $soItem = SalesOrderItem::findOrFail($row['sales_order_item_id']);
                $soItem->stock_qty = $row['confirmQty'];
                $soItem->warehouse_id = $row['warehouse'];
                $soItem->save();

            }

        } catch(\Exception $e)
        {
            DB::rollback();
            noty()->danger('Oops!', 'Please try again');
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('stock.stock-checking.index')
        ]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id, StockCheckingDatatable $datatable)
    {
        // $row = SalesOrder::find($id);

        // $media = $row->getMedia('delivery_order_material');

        // if($row){
        //     $media = $row->getMedia('delivery_order_material');
        //         } else {
        //             $media = '';
        //         }
        // return view('stock::stock_checking.attachment',compact('media'))->withMedia($media)->withRow($row);
        $this->hasPermissionTo('view stock');
        return $datatable->render('stock::stock.stock_checking.index');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('stock::stock.stock_checking.edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit stock');

        DB::beginTransaction();
        try {
            foreach ($request->products as $row) {
                if ($row['confirmQty'] > $row['purchaseQty']) {
                    return response()->json([
                    'success' => false,
                    'redirect' => route('stock.stock-checking.create')
                    ]);
                }
            }
            
            foreach ($request->products as $row) {
                // if ($row['item'] != null) {
                //     if (array_key_exists('id', $row)) {
                        $soItem = SalesOrderItem::findOrFail($row['sales_order_item_id']);
                        $soItem->stock_qty = $row['confirmQty'];
                        $soItem->warehouse_id = $row['warehouse_id'];
                        $soItem->save();
                //     }
                // }
            }

        }
        catch(\Exception $e)
        {
            DB::rollback();
            noty()->danger('Oops!', 'Please try again');
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('stock.stock-checking')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
     public function destroy($id)
     {
         $this->hasPermissionTo('delete sales order');

         // $row = DeliveryOrderMaterial::findOrFail($id);

         // $item = DeliveryOrderMaterialItem::where('delivery_order_material_id', $row->id)->get();
         $so = SalesOrder::find($id);
         $item = SalesOrderItem::where('sales_order_id', $so->id)->get();
         foreach ($item as $key => $value) {
             $value->stock_qty = null;
             $value->warehouse_id = null;
             $value->save();
         }

         // $cekSo = SalesOrderItem::where('sales_order_id', $row->sales_order_id)->get();
         // $pluckSo = $cekSo->pluck('delivered_qty')->toArray();
         // if(count($pluckSo) == 0){
         //     DeliveryOrderMaterial::where('sales_order_id', $row->sales_order_id)->update(['status_partial' => 1]);
         // }else{
         //     DeliveryOrderMaterial::where('sales_order_id', $row->sales_order_id)->update(['status_partial' => 0]);
         // }

         // DeliveryOrderMaterialApproval::where('delivery_order_material_id', $row->id)->delete();
         // $item->each->delete();
         // $row->delete();

         noty()->success('Yeay!', 'Your entry has been added successfully');
         return redirect()->route('transaction.delivery_order_material.index');
     }

     public function detail($id)
     {
         $row = DeliveryOrderMaterial::findOrFail($id);
         $items = DeliveryOrderMaterialItem::with('salesOrderMaterialItem.product', 'salesOrderMaterialItem')->where('delivery_order_material_id', $id)->get();
         $approvals = DeliveryOrderMaterialApproval::where('delivery_order_material_id', $id)->orderBy('id', 'asc')->get();
         if($row){
            $media = $row->getMedia('delivery_order_material');
        }else{
            $media = [];
        }

         return view('transaction::delivery_order_material.entry.detail', compact('row', 'items', 'approvals', 'media'));
     }

    public function salesOrderMaterial()
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first()->id;

        $row = SalesOrder::with('items')
        ->where('status', 'approved')
        ->whereHas('items', function ($q){
            $q->where('delivered_qty', '>' ,0);
        })
        ->get();
        return $row;
    }

    public function warehouse()
    {
        $row = WarehouseMaster::all();
        return $row;
    }

    public function salesOrderMaterialItem()
    {
        return SalesOrder::with(['department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])
        ->whereHas('items', function ($q) {
            $q->where('status', true);
        })
        ->find(request()->sales_order_id);
    }

    public function report()
    {
        $stockChecking = SalesOrder::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $stockChecking->whereBetween('date', [$fromDate, $untilDate])->get();
        }

        return view('stock::stock.stock_checking.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        
         return view('stock::stock.stock_checking.report.detail', compact('row', 'items'));
    }

    public function sales(){
        $row =  SalesOrder::with(['department', 'customer', 'salesman', 'paymentTerm', 'tax', 'items', 'items.product', 'items.uom'])->where('status','approved')
        ->whereHas('items', function ($q) {
            $q->where('status', true)->whereNull('deleted_at')->whereNull('stock_qty');
        })->whereNull('deleted_at')
        ->get();
        return response()->json($row);
    }
  
}
