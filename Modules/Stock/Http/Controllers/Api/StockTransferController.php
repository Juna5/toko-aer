<?php

namespace Modules\Stock\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Stock\Entities\StockTransfer;
use Modules\Stock\Entities\StockTransferDetail;


class StockTransferController extends Controller
{
    public function generateCode($table, $name)
    {
        # generate code number
        $sequence = new \App\Services\SequenceNumber;
        $sequence_number = $sequence->runSequenceNumber($table);
        $code_number = generate_code_sequence($name, $sequence_number, 3);

        return $code_number;
    }


    public function detailTransfer($id){
    	return StockTransfer::with('items','origin','destination','items.product')->where('id', $id)->get();
    }
}
