<?php

namespace Modules\Stock\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\Procurement\Entities\WarehouseMaster;

class WarehouseController extends Controller
{
    public function index()
    {
        $warehouse = WarehouseMaster::all();

        return $warehouse;
    }

}
