<?php

namespace Modules\Stock\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Stock\DataTables\StockTransferDatatable;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Stock\Entities\StockTransfer;
use Modules\Stock\Entities\StockTransferDetail;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;
use Modules\Procurement\Entities\Product;

class StockTransferController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(StockTransferDatatable $datatable)
    {
        $this->hasPermissionTo('view stock transfer');

        return $datatable->render('stock::stock_transfer.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add stock transfer');
        $product = Product::all();
        $warehouse =  WarehouseMaster::all();

        return view('stock::stock_transfer.create', compact('product','warehouse'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        DB::beginTransaction();
        try {
           foreach ($request->products as $row) {
                if ($row['quantity'] > $row['qty']) {
                    return response()->json([
                    'success' => false,
                    'qty' => $row['qty'],
                    'quanti' => $row['quantity'],
                    'redirect' => route('stock.stock-transfer.create')
                    ]);
                }
            }
  
        $date = request()->date;
        $desc = request()->description;
        $origin = WarehouseMaster::find(request()->origin_id);
        $destination = WarehouseMaster::find(request()->destination_id);

        # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('stock transfer');
            $code_number = generate_code_sequence('ST', $sequence_number, 3);

              $stockTransfer = StockTransfer::create(
                [
                    'date' => $date,
                    'code_number' => $code_number,
                    'description' => $desc,
                    'origin_id'   => $origin->id,
                    'destination_id' => $destination->id,
                ]
            );
  
              foreach($request->products as $item) {


                if($row['item'] != null) {

                  $getStock = Stock::where('product_id', $item['item'])->where('warehouse_id',$origin->id)->first();


                  if($getStock){
                    if($item['quantity'] <= $getStock->qty){
                      $stockTmp =  Stock::updateOrCreate(
                        [
                          'product_id'   =>  $item['item'],
                          'warehouse_id' =>  $destination->id,
                        ],
                        [
                          'qty'           => $item['qty'] - $item['quantity'],
                        ]);

                      $getStock->qty = $item['qty'] - $item['quantity'];
                      $getStock->save();

                      $qtyTotal = getTotal($item['item'], $origin->id);
                      $qtyTotalDestination = getTotal($item['item'], $destination->id);

                      $stockTransaction = StockTransaction::create(
                        [
                          'product_id'   => $item['item'],
                          'warehouse_id' => $origin->id,
                          'type'         => 'OUT',
                          'description'  => $desc ?? 'Stock transfer from warehouse '.$origin->name . ' to warehouse '.$destination->name,
                          'qty'   => $item['quantity'],
                          'in_hand'   => $qtyTotal - $item['quantity'],
                          'stock_id' => $getStock->id,
                          'date'     => date('Y-m-d'),

                        ]
                      );


                      $transaction = StockTransaction::create(
                        [
                          'product_id'   => $item['item'],
                          'warehouse_id' => $destination->id ,
                          'type'         => 'IN',
                          'description'  => $desc ?? 'Stock transfer from warehouse '.$origin->name . ' to warehouse '.$destination->name,
                          'qty'   => $item['quantity'],
                          'in_hand'   => $qtyTotalDestination + $item['quantity'],
                          'stock_id'  => $stockTmp->id,
                          'date'     => date('Y-m-d'),

                        ]
                      );

                    }else{
                      $stock = new Stock();
                      $stock->product_id = $item['item'];
                      $stock->warehouse_id = $destination->id;
                      $stock->qty = $item['quantity'];
                      $stock->save();

                      $transaction = StockTransaction::create(
                        [
                          'product_id'   => $item['item'],
                          'warehouse_id' => $destination->id ,
                          'type'         => 'IN',
                          'description'  => $desc ?? 'Stock transfer from warehouse '.$origin->name . ' to warehouse '.$destination->name,
                          'qty'   => $item['quantity'],
                          'in_hand'   => $qtyTotalDestination + $item['quantity'],
                          'stock_id'  => $stock->id,
                          'date'     => date('Y-m-d'),

                        ]
                      );
                    }
                    $stocks = Stock::where('product_id', $item['item'])->where('warehouse_id',$origin->id)->first();

                    if($stockTransfer){
                      StockTransferDetail::create(
                        [
                          'stock_transfer_id' => $stockTransfer->id,
                          'product_id'          => $item['item'],
                          'warehouse_id_origin'        => $origin->id,
                          'warehouse_id_destination'        => $destination->id,
                          'qty'                 => $item['quantity'], 
                          'in_hand' => $stocks->qty,
                        ]
                      );
                    }
                  }
                }
              }
                    
         } catch(\Exception $e) {
             DB::rollback();
            noty()->danger('Oops!', 'Please try again');
            return back();
        }

         DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('stock.stock-transfer.create')
        ]);


    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('stock::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $stockTransfer = StockTransfer::find($id);
        $item          = StockTransferDetail::with('product','warehouseOrigin','warehouseDestination')->where('stock_transfer_id', $id)->get();
        $product = Product::all();
        $warehouse =  WarehouseMaster::all();

        return view('stock::stock_transfer.edit', compact('stockTransfer','item','product','warehouse'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try {
                foreach ($request->products as $row) {
                  if ($row['quantity'] > $row['qty']) {
                      return response()->json([
                      'success' => false,
                      'qty' => $row['qty'],
                      'quanti' => $row['quantity'],
                      'redirect' => route('stock.stock-transfer.create')
                      ]);
                  }
                }

        $date = request()->date;
        $desc = request()->description;
        $origin = WarehouseMaster::find(request()->origin_id);
        $destination = WarehouseMaster::find(request()->destination_id);

        // # generate code number
        //     $sequence = new \App\Services\SequenceNumber;
        //     $sequence_number = $sequence->runSequenceNumber('stock transfer');
        //     $code_number = generate_code_sequence('ST', $sequence_number, 3);

            $stockTransfer = StockTransfer::findOrFail($id);
            $stockTransfer->date = $date;
            $stockTransfer->origin_id = $origin->id;
            $stockTransfer->destination_id = $destination->id;
            $stockTransfer->description = $desc;
            $stockTransfer->save();
  
              foreach($request->products as $item) {
                  $stockTransferDetail = StockTransferDetail::find($item['id']);
                  $sel = ($stockTransferDetail->qty * -1) + $item['quantity'];
                  if ($sel > 0 ) {
                    $itemSel= $stockTransferDetail->qty - $sel;
                  }else{
                    $itemSel = $stockTransferDetail->qty - $sel;
                  }

                  $stockTransferDetail->qty = $item['quantity'];
                  $stockTransferDetail->save();

                  $getStock = Stock::where('product_id', $item['product_id'])->where('warehouse_id', $origin->id)->first();
                  $stockDestination = Stock::where('product_id', $item['product_id'])->where('warehouse_id',$destination->id)->first();
                  $getStock->qty = $item['qty'] - $item['quantity'];
                      $getStock->save();

                  $stockDestination->qty = $item['qty'] + $item['quantity'];
                  $stockDestination->save();


                  $qtyTotal = getTotal($item['product_id'], $origin->id);
                  $qtyTotalDestination = getTotal($item['product_id'], $destination->id);
          }
        
         } catch(\Exception $e) {
            DB::rollback();

            noty()->danger('Oops!', 'Please try again');
            return redirect()->route('stock.stock-transfer.edit',$id);
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('stock.stock-transfer.create')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete stock transfer');

         $row = StockTransfer::findOrFail($id);
         StockTransferDetail::where('stock_transfer_id', $row->id)->delete();

         $row->delete();

         noty()->danger('Cool!', 'Your entry has been deleted');
         return redirect()->route('stock.stock-transfer.index');
    }

    public function detail($id)
    {
        $stockTransfer = StockTransfer::find($id);
        $item          = StockTransferDetail::with('product','warehouseOrigin','warehouseDestination')->where('stock_transfer_id',$id)->get();

        return view('stock::stock_transfer.detail', compact('stockTransfer','item'));

    }

    public function report()
    {
        $row = StockTransfer::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $row->whereBetween('date', [$fromDate, $untilDate])->orderBy('id', 'desc')->paginate(10);
        }

        return view('stock::stock_transfer.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $row = StockTransfer::findOrFail($id);
        $items = StockTransferDetail::where('stock_transfer_id', $id)->get();
        
        return view('stock::stock_transfer.report.detail', compact('row', 'items'));
    }
}
