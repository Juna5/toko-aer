<?php

namespace Modules\Stock\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Stock\Entities\Stock;
use Modules\Procurement\Entities\Product;
use Modules\Stock\Entities\StockTransaction;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Transaction\Entities\DeliveryOrderMaterial;
use Modules\Transaction\Entities\DeliveryOrderMaterialItem;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class StockTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('stock::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('stock::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $this->hasPermissionTo('view stock');
        $product = Product::find($id);
        $warehouse = WarehouseMaster::find(request()->warehouse_id);
        $perPage = 10;  
        $stockTransaction = StockTransaction::with('purchaseOrder','deliveryOrderMaterial','stock')->where('product_id', $product->id)->whereHas('stock',function($stocks) use($warehouse){
        $stocks->where('warehouse_id', $warehouse->id);
        })->orderBy('date','desc')->paginate($perPage);
     
        return view('stock::stock.view', compact('product','warehouse','stockTransaction'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->hasPermissionTo('edit stock');
        $product = Product::find($id);
        $warehouse = WarehouseMaster::find(request()->warehouse_id);
        return view('stock::stock.edit', compact('product','warehouse'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit stock');
    DB::beginTransaction();
        try {
       $po = PurchaseOrderItem::find($request->code_number);
       $do = DeliveryOrderMaterialItem::find($request->code_number);
       $type = request()->type;
       $warehouse =  request()->warehouse_id;
       $qty = getTotal($request->product_id, $request->warehouse_id);

       if($type == "IN"){
        StockTransaction::create([
            'product_id'        => $request->product_id,
            'warehouse_id'      => $request->warehouse_id,
            'purchase_order_id' => $po->purchase_order_id,
            'date'              => date('Y-m-d'),
            'qty'               => $request->qty, 
            'type'              => $request->type,
            'description'       => $request->description,
        ]);

    }else{
        if($request->qty > $qty){
                        flash('Error processing request')->error();
                        return redirect()->route('stock.stock.show', $warehouse);
        }
        StockTransaction::create([
            'product_id'        => $request->product_id,
            'warehouse_id'      => $request->warehouse_id,
            'delivery_order_id' => $do->delivery_order_material_id,
            'date'              => date('Y-m-d'),
            'qty'               => $request->qty, 
            'type'              => $request->type,
            'description'       => $request->description,
        ]);
    }
 
        Stock::updateOrCreate(
            [
                'product_id'   =>  $request->product_id,
                'warehouse_id' =>  $request->warehouse_id,
            ],
            [
                'qty' => $qty,
            ]);

            DB::commit();
            noty()->success('Yeay!', 'Your entry has been added successfully');
            return redirect()->route('stock.stock.show', $warehouse);
         } catch(\Exception $e) {
            DB::rollback();

            noty()->danger('Oops!', 'Error proccessing request');
            return redirect()->route('stock.stock.show', $warehouse);
        }

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajax(){
       $type = request()->type;
       $product = request()->product;
       if($type == "IN"){
            $data = PurchaseOrderItem::with('purchaseOrder','product')->where('product_id', $product)->get();
       }elseif($type == "OUT"){
            $data = DeliveryOrderMaterialItem::with('deliveryOrderMaterial','salesOrderMaterialItem.product')->whereHas('salesOrderMaterialItem', function($sales) use($product){
                $sales->where('product_id',$product);
            })->get();
       }        

       return response()->json($data);
    }

     public function qty(){
       $id = request()->id;
       $type = request()->type;
       if($type == "IN"){
            $data = PurchaseOrderItem::find($id);
       }elseif($type == "OUT"){
            $data = DeliveryOrderMaterialItem::find($id);
       }

       return response()->json($data);
    }
}
