@extends('stock::layouts.app')

@section('stock::title', 'Stock Checking Outstanding')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Stock Checking Outstanding',
    'active' => true,
    'url' => route('stock.stock-checking-outstanding')
    ])
@endsection

@section('stock::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab" href="{{ route('stock.stock-checking.index') }}" role="tab" aria-controls="home" aria-selected="false">Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="document-tab" data-toggle="tab" href="#outstanding" role="tab" aria-controls="outstanding" aria-selected="true">Outstanding</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        @include('stock::stock.stock_checking.tab.outstanding')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection