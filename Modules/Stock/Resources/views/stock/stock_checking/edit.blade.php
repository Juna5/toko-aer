@extends('stock::layouts.app')

@section('stock::title', 'Stock Checking')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Checking',
'active' => true,
'url' => route('stock.stock-checking.index')
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Edit',
'active' => false,
'url' => ''
])
@endsection
@section('stock::content')
<div class="row" id="pr-create">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('stock.stock-checking.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
        
                    <div class="col-12">
                         @if(request()->resubmit)
                        <stock-checking-edit :id="{{ request()->segment(3) }}" :resubmit="{{request()->resubmit}}"/>
                            @else
                            <stock-checking-edit :id="{{ request()->segment(3) }}" />
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
