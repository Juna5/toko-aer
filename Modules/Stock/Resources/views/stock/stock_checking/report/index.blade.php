@extends('stock::layouts.app')

@section('stock::title', 'Stock Checking Report')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Penjualan',
    'active' => true,
    'url' => route('billing.view')
    ])
@endsection

@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Stock Checking Report',
'active' => true,
'url' => route('stock.stock-checking-report')
])
@endsection

@section('stock::content')
	<div class="row">
        <div class="col-md-12">
            <div class="card no-b no-r">
                <div class="card-body">
                    <form action="{{ route('stock.stock-checking-report') }}" method="GET">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="from_date"
                                    name="from_date" value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">To Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="until_date"
                                    name="until_date" value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                    	<a href="{{ route('billing.view') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br><br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Customer</th>
                                <th>Salesman</th>
                                <th>Payment Term</th>
                                <th></th>
                            </tr>

                            @forelse($results as $index => $row)
                                <tr align="center" >
                                    <td>{{ $index +1 }}</td>
                                    <td><a href="{{ route('stock.stock-checking-report-detail',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->code_number }}</a></td>
                                    <td>{{ format_d_month_y($row->date) }}</td>
                                    <td>{{ $row->employee_id ? optional($row->employee)->name : '-' }}</td>
                                    <td>{{ optional($row->customer)->company_name }} - {{ optional($row->customer)->pic_1 }}</td>
                                    <td>{{ optional($row->salesman)->name }}</td>
                                    <td>{{ optional($row->paymentTerm)->name }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(request('from_date') && request('end_date'))
                            {{ $results->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
