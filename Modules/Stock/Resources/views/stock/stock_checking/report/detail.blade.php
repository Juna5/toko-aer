@extends('stock::layouts.app')

@section('stock::title', 'Stock Checking Report Detail')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Checking Report',
'active' => true,
'url' => route('stock.stock-checking-report')
])
@endsection

@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Stock Checking Report Detail',
'active' => true,
'url' => route('stock.stock-checking-report-detail', $row->id)
])
@endsection

@section('stock::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary ">{{ format_d_month_y($row->date) }} <br>{{ optional($row->employee)->name }}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-md-right">
                                                <div class="invoice-number text-primary ">{{ $row->code_number }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-md">
                                        
                                                    <tr>
                                                        <td width="30%">Customer</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->customer)->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Salesman</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->salesman)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Project</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->project_id ? optional($row->project)->name : '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Payment Term</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->paymentTerm)->code . ' - ' . optional($row->paymentTerm)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Currency</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->currency)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Description</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->description }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <div class="section-title">Item</div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-md">
                                            <tr>
                                                <th>Item</th>
                                                <th>UoM</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Dsc Rate (%)</th>
                                                <th>Dsc Amount</th>
                                                <th>Description</th>
                                                <th>Purchased Qty</th>
                                                <th>Confirm Qty</th>
                                            </tr>
                                            @foreach($items as $item)
                                            <tr >
                                                <td>{{ optional($item->product)->name }}</td>
                                                <td>{{ optional($item->uom)->name }}</td>
                                                <td align="right">{{ amount_international_with_comma($item->qty) }}</td>
                                                <td align="right">{{ amount_international_with_comma($item->price) }}</td>
                                                <td align="right">{{ $item->discount_rate }}</td>
                                                <td align="right">{{ amount_international_with_comma($item->discount_amount) }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td align="right">{{ amount_international_with_comma($item->delivered_qty) }}</td>
                                                <td align="right">{{ amount_international_with_comma($item->stock_qty) }}</td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="text-md-right">
                                        <a href="{{ route('stock.stock-checking-report') }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
