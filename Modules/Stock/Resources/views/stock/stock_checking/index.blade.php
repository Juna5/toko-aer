@extends('stock::layouts.app')

@section('stock::title', 'Stock Checking')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Stock Checking',
    'active' => true,
    'url' => route('stock.stock-checking.index')
    ])
@endsection

@section('stock::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="document-tab"  href="{{ route('stock.stock-checking-outstanding') }}" role="tab" aria-controls="outstanding" aria-selected="false">Outstanding</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        @include('stock::stock.stock_checking.tab.entry')
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection