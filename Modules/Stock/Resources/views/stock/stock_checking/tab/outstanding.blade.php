<div class="tab-pane active" id="outstanding" role="tabpanel" aria-labelledby="home-tab">
	<div class="card-body">		
        <h4 class="card-title" style="text-align: center;">
        	Outstanding
        </h4>
        <div class="table-responsive">
            {!! $dataTable->table() !!}
        </div>	                
	</div>
</div>

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush