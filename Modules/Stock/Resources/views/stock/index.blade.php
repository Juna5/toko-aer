@extends('setting::layouts.master')

@section('setting::title', 'Stock')

@section('setting::breadcrumb-2')
    @include('setting::include.breadcrumb', [
        'title' => 'Stock',
        'active' => true,
        'url' => route('stock.stock.index')
    ])
@endsection

@section('setting::content')
    <div class="section-body">
        {{-- <h2 class="section-title">Overview</h2>
        <p class="section-lead">
        	<br>
            Organize and adjust all settings about this site.
        </p> --}}
        @include('flash::message')
       {{-- <div class="row">
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-body">
                        <h4>Leave Entry</h4>
                        <p>Create leave for employee.</p>
                        @can('view leave')
                            <a href="{{ route('hr.leave.entry.index') }}" class="card-cta">Go to Leave Entry <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div> --}}

         <div class="row sortable-card ui-sortable">
        @foreach($warehouse as $data)
       
              <div class="col-12 col-md-6 col-lg-3">
                <div class="card card-primary">
                  <a href="{{ route('stock.stock.show', $data->id) }}">
                  <div class="card-header ui-sortable-handle text-center" >
                    <h4 class="text-center">{{ $data->name }}</h4>
                  </div>
                  </a>
                  <div class="card-body">
                    <table width="100%">
                      <tr>
                        <td>IN</td>
                        <td><span class="badge badge-primary">{{ getTotalStockWarehouse($data->id,'IN') ?? '0'}}</span></td>
                      </tr>
                      <tr>
                        <td>OUT</td>
                        <td><span class="badge badge-warning">{{ getTotalStockWarehouse($data->id,'OUT') ?? '0'}}</span></td>
                      </tr>
                      <tr>
                        <td>In Hand</td>
                        <td><span class="badge badge-success">{{ getTotalWarehouse($data->id) ?? '0' }}</span></td>
                      </tr>
                    </table>
                  </div>
                  <a href="{{ route('stock.stock.show', $data->id) }}" class="btn btn-primary">
                  See in details 
                  </a>
                </div>
              </div>

        
   		 @endforeach
</div> 
       

@endsection
