@extends('stock::layouts.app')

@section('stock::title', 'Edit Stock')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Product List',
'active' => true,
'url' => route('stock.stock.show',$warehouse->id)
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Edit',
'active' => false
])
@endsection


@section('stock::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            {{ Form::model($warehouse, array('route' => array('stock.stock.store'), 'method' => 'POST', 'files' => true)) }}
            <div class="card-body">
                <h5 class="card-title">
                   Stock Transactions
                    <div class="text-right">
                        <a href="{{ route('stock.stock.show',$warehouse->id) }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>

                @include('include.error-list')
                 @include('flash::message')
                 <div class="form-group pb-1">
                    <label for="photo_profile">Warehouse</label>
                        <select class="form-control " name="warehouse_id" id="warehouse_id" readonly>
                            <option value="{{ $warehouse->id }}" >{{ $warehouse->name  }}</option>
                        </select>
                </div>
                <!-- <div class="form-group pb-1">
                    <label for="photo_profile">Type</label>
                        <select class="form-control r-0 light s-12 select2" name="type" id="type" >
                            <option value="">Please Select</option>
                            <option value="IN">IN</option>
                            <option value="OUT">OUT</option>
                        </select>
                </div> -->
                <div class="form-group pb-1">
                    <label for="photo_profile">PO Number</label>
                        <select class="form-control r-0 light s-12 select2" name="code_number" id="code_number" >
                            <option value="">Please Select</option>
                            @foreach($purchaseOrder as $po)
                            <option value="{{ $po->id }}">{{ $po->code_number }}</option>
                            @endforeach
                        </select>
                </div>
                <table class="table table-striped" id="sortable-table" >
                    <thead>
                        <tr>
                            <th></th>
                            <th>Name</th>
                            <th>QTY</th>
                            <th>Description</th>
                        </tr>
                   </thead>
                    <tbody id="tab-stock">
                    </tbody>

                </table>
                {{-- <div class="form-group pb-1">
                    <label for="photo_profile">Qty</label>
                    <input type="text" class="form-control r-0 light s-12" name="qty" value="{{ old('qty')}}" id="qty">
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Description</label>
                    <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') }}</textarea>
                </div> --}}
            <hr>
            <div class="card-body">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script >


    $('#type').on('change', function(e){
      console.log(e);
      var type = e.target.value;

      $.get('{{ route('stock.stock.codeNumber') }}', {type: type}, function(data){
        console.log(data)
        $('#code_number').empty();
        $('#code_number').append('<option>Please Select</option>');
        $.each(data, function(index, subcatObj){

          $('#code_number').append('<option value="'+subcatObj.id+'">'+subcatObj.code_number+'</option>');
        });
      });
    });

    $('#code_number').on('change', function(e){
      console.log(e);
      var code = e.target.value;
        var no = 0;
      $.get('{{ route('stock.stock.product') }}', {id: code, type:'IN'}, function(data){
         $('#tab-stock').empty();
         $.each(data, function(index, subcatObj){

        var valqty = ('IN' == 'IN') ? subcatObj.qty : subcatObj.send_qty;
        var product = ('IN' == 'IN') ? subcatObj.product_id : subcatObj.sales_order_material_item.product_id;
        var names = ('IN' == 'IN') ? subcatObj.product.name : subcatObj.sales_order_material_item.product.name;
         no++;
            var newRow = $('<tr id="item'+no+'">' +
                                 '<td>' +
                                     '<input type="checkbox" id="form-field-1"  name="checkeds[]"  class="form-control" checked value="'+subcatObj.id+'"/>' +
                                '</td>' +
                                '<td>' +
                                    '<select class="form-control" name="productId_'+subcatObj.id+'" readonly="true">' +
                                            '<option value="'+product+'">'+names+'</option>' +
                                    '</select>' +
                                '</td>' +


                                '<td>' +
                                    '<input type="number" id="form-field-1" style="text-align:right"  name="qty_'+subcatObj.id+'"  class="qty form-control" id="qty"  value="'+subcatObj.qty+'"/>' +
                                '</td>' +
                                '<td>' +
                                    '<textarea class="form-control" name="description_'+subcatObj.id+'"></textarea>' +
                                '</td>' +

                            '</tr>' );

            $('#tab-stock').append(newRow);


           });


      });
    });

</script>
@endpush
