@extends('stock::layouts.app')

@section('stock::title', 'Edit Stock')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Product List',
'active' => true,
'url' => route('stock.stock.show',$warehouse->id)
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Edit',
'active' => false
])
@endsection


@section('stock::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            {{ Form::model($product, array('route' => array('stock.stock-transaction.update', $product->id), 'method' => 'PUT', 'files' => true)) }}
            <div class="card-body">
                <h5 class="card-title">
                   Stock Transactions
                    <div class="text-right">
                        <a href="{{ route('stock.stock.show',$warehouse->id) }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>

                @include('include.error-list')

                <div class="form-group pb-1">
                    <label for="photo_profile">Product</label>
                        <select class="form-control " name="product_id" id="product_id" readonly        >
                            <option value="{{ $product->id }}" >{{ $product->name  }}</option>
                        </select>
                </div>
                 <div class="form-group pb-1">
                    <label for="photo_profile">Warehouse</label>
                        <select class="form-control " name="warehouse_id" id="warehouse_id" readonly="">
                            <option value="{{ $warehouse->id }}" >{{ $warehouse->name  }}</option>
                        </select>
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Type</label>
                        <select class="form-control r-0 light s-12 select2" name="type" id="type" >
                            <option value="">Please Select</option>
                            <option value="IN">IN</option>
                            <option value="OUT">OUT</option>
                        </select>
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Code Number</label>
                        <select class="form-control r-0 light s-12 select2" name="code_number" id="code_number" >
                            <option value="">Please Select</option>
                        </select>
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Qty</label>
                    <input type="number" class="form-control r-0 light s-12" name="qty" style="text-align:right" value="{{ old('qty')}}" id="qty">
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Description</label>
                    <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') }}</textarea>
                </div>
            <hr>
            <div class="card-body">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script >


    $('#type').on('change', function(e){
      var type = e.target.value;

      $.get('{{ route('stock.stock-ajax') }}', {type: type, product: $('#product_id').val()}, function(data){
        $('#code_number').empty();
        $('#code_number').append('<option>Please Select</option>');
        $.each(data, function(index, subcatObj){
            var tipe = 'IN';
            var code = (type == 'IN') ? subcatObj.purchase_order.code_number : subcatObj.delivery_order_material.code_number;
            var product = (type == 'IN') ? subcatObj.product.name : subcatObj.sales_order_material_item.product.name
            var valqty = (type == 'IN') ? subcatObj.qty : subcatObj.qty;
          $('#code_number').append('<option value="'+subcatObj.id+'">'+code+' - '+product+' - '+valqty+'</option>');
        });
      });
    });

    $('#code_number').on('change', function(e){
      console.log(e);
      var code = e.target.value;
      $.get('{{ route('stock.stock-ajaxQty') }}', {id: code, type:$('#type').val()}, function(data){
            var valqty = ($('#type').val() == 'IN') ? data.qty : data.qty;
          $('#qty').val(valqty);


      });
    });

</script>
@endpush
