@extends('stock::layouts.app')

@section('stock::title', 'Stock')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Stock',
    'active' => true,
    'url' => route('stock.stock.show',$warehouse->id)
    ])
@endsection

@section('stock::breadcrumb-3')
    @include('stock::include.breadcrum', [
    'title' => 'Stock',
    'active' => false,
    'url' => '/',
    ])
@endsection


@section('stock::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{ $product->name }}</h4>
                <div class="card-header-form">
                    <a href="{{ route('stock.stock.show', $warehouse->id) }}" class="btn btn-sm btn-warning pd-x-15 btn-white btn-uppercase">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </a>        
                </div>
                
                               
                           
            </div>
            <div class="card-body  p-12">
                @include('flash::message')
                <div class="table-responsive">
                    <table class="table table-striped" id="sortable-table">
                        <thead>
                        <tr class="text-center">
                            
                            <th>#</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th>IN</th>
                            <th>OUT</th>
                            <th>In Hand</th>
                            <th>Description</th>
                            <th>PO Number</th>
                            <th>DO Number</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($stockTransaction as $index => $row)
                            <tr class="text-center">
                                
                                <td>{{ $index +1 }}</td>
                                <td>{{ optional($row->product)->name }}</td>
                                <td>{{ $row->date }}</td>
                                @if($row->type == 'IN')
                                <td>{{ $row->qty }}</td>
                                <td>0</td>
                                @else
                                <td>0</td>
                                <td>{{ $row->qty }}</td>
                                @endif
                                <td>{{ $row->in_hand ?? '0' }}</td>
                                <td>{{ $row->description }}</td>
                                <td>{{ optional($row->purchaseOrder)->code_number ?? '-' }}</td>
                                <td>{{ optional($row->deliveryOrderMaterial)->code_number ?? ' -' }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                  <div class="col-md-6 ">
                    {{ $stockTransaction->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

