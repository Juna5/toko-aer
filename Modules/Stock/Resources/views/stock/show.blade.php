@extends('stock::layouts.app')

@section('stock::title', 'Stock')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Warehouse List',
    'active' => true,
    'url' => route('stock.stock.index')
    ])
@endsection

@section('stock::breadcrumb-3')
    @include('stock::include.breadcrum', [
    'title' => 'Warehouse',
    'active' => true,
    'url' => route('stock.stock.show', $id)
    ])
@endsection

@section('stock::content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>{{ $warehouse->name }}</h4>

            <div class="card-header-form">
                <a href="{{ route('stock.stock.create',['warehouse_id' => $id]) }}" class="btn btn-sm btn-info pd-x-15 btn-white btn-uppercase">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add Stock
                </a>
                
                                <a href="{{ route('stock.stock.index') }}" class="btn btn-warning">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </a>
                
            </div>
            </div>
            <div class="card-body p-12">
                @include('flash::message')
                <div class="table-responsive">
                    <table class="table table-striped" id="sortable-table">
                        <thead>
                        <tr class="text-center">
                            
                            <th>#</th>
                            <th>Name</th>
                            <th>IN</th>
                            <th>OUT</th>
                            <th>In Hand</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($product as $index => $row)
                            <tr class="text-center">
                                
                                <td>{{ $index +1 }}</td>
                                <td>{{ $row->name }}</td>
                                <td>{{ getTotalStock($row->product_id,$row->warehouse_id,'IN') ?? '0' }}</td>
                                <td>{{ getTotalStock($row->product_id,$row->warehouse_id,'OUT') ?? '0' }}</td>
                                <td>{{ getTotal($row->product_id,$row->warehouse_id) ?? '0' }}</td>
                                <td>
                                    <a href="{{route('stock.stock-transaction.show', [$row->product_id, 'warehouse_id' => $id])}}" class="btn btn-warning">View Stock</a>
                                   {{--  <a href="{{route('stock.stock-transaction.edit', [$row->product_id, 'warehouse_id' => $id])}}" class="btn btn-success">Edit Stock</a> --}}</td>
                               
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

