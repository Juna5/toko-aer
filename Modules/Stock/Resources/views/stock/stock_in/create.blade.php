@extends('stock::layouts.app')

@section('stock::title', 'Stock In')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock In',
'active' => true,
'url' => route('stock.stock-in.index')
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('stock.stock-in.create')
])
@endsection
@section('stock::content')
<div class="row" id="pr-create">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('stock.stock-in.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        @if(request()->id)
                        <stock-in-create :po-id="{{request()->id}}"/>
                            @endif
                            <stock-in-create/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
