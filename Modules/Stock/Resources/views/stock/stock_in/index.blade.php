@extends('stock::layouts.app')

@section('stock::title', 'Stock In')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Stock In',
    'active' => true,
    'url' => route('stock.stock-in.index')
    ])
@endsection

@section('stock::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <div class="table-responsive table-striped">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush