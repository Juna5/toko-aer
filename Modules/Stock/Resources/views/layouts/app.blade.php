@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@yield('stock::title', config('stock.name'))</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></div>
                @yield('stock::breadcrumb-2')
                @yield('stock::breadcrumb-3')
            </div>
        </div>

        <div class="section-body">
            @yield('stock::content')
        </div>
    </section>
@endsection
