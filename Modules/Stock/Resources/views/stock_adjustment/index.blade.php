@extends('stock::layouts.app')

@section('stock::title', 'Stock Adjustment')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Stock Adjustment',
    'active' => true,
    'url' => route('stock.stock-adjustment.index')
    ])
@endsection

@section('stock::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body ">
                    <div class="table-responsive  table-striped">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush