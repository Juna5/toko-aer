@extends('stock::layouts.app')

@section('stock::title', 'Stock Adjustment Report Detail')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Adjustment Report',
'active' => true,
'url' => route('stock.stock-adjustment-report')
])
@endsection

@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Stock Adjustment Report Detail',
'active' => true,
'url' => route('stock.stock-adjustment-report-detail', $row->id)
])
@endsection

@section('stock::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary ">{{ format_d_month_y($row->date) }}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-md-right">
                                                <div class="invoice-number text-primary ">{{ $row->code_number }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($row->date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Remarks</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->description }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <div class="section-title">Item</div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-md">
                                            <tr>
                                                <th style="text-align: center;">Product</th>
                                                <th style="text-align: center;">Warehouse</th>
                                                <th style="text-align: center;">Qty</th>
                                                <th style="text-align: center;">Type</th>
                                            </tr>
                                            @foreach($items as $item)
                                            <tr>
                                                <td style="text-align: center;">{{ optional($item->product)->name }}</td>
                                                <td style="text-align: center;">{{ optional($item->warehouse)->code }} - {{ optional($item->warehouse)->name }}</td>
                                                <td style="text-align: right;">{{ amount_international_with_comma($item->qty) }}</td>
                                                <td style="text-align: center;">{{ $item->type }}</td>
                                            </tr>
                                            @endforeach
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="text-md-right">
                                        <a href="{{ route('stock.stock-adjustment-report') }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
