@extends('stock::layouts.app')

@section('stock::title', 'Stock Adjustment')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Adjustment',
'active' => true,
'url' => route('stock.stock-adjustment.index')
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Edit',
'active' => false
])
@endsection

@section('stock::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            {{ Form::model($stockAdjustment, array('route' => array('stock.stock-adjustment.update',$stockAdjustment->id), 'method' => 'PUT', 'files' => true)) }}
            <div class="card-body">
                <h5 class="card-title">
                   Stock Transactions
                    <div class="text-right">
                        <a href="{{ route('stock.stock-adjustment.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('include.error-list')
                 @include('flash::message')
                <div class="form-group pb-1">
                    <label for="photo_profile">Date</label>
                    <input type="text" class="form-control r-0 light s-12 datepicker" name="date" id="date" value="{{$stockAdjustment->date}}">
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Description</label>
                    <textarea class="form-control" name="description">{{ $stockAdjustment->description}}</textarea>
                </div>
                <button id="addText" class="btn btn-success" type="button">
                            <i class="ace-icon fa fa-plus bigger-110"></i>
                            Add Item
                </button>
                <br>
                <br>
                <br>
                <table class="table table-striped" id="sortable-table" >
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Warehouse</th>
                            <th>Qty</th>
                            <th>Type</th>
                            <th></th>
                        </tr>
                   </thead>
                    <tbody id="tab-stock">
                        @foreach($item as $i)
                        <tr>
                            <td>
                                <input type="hidden" name="detail_id[]" value="{{ $i->id }}" >
                                <select class="select2 form-control" name="product_id[]">
                                    @foreach($product as $item)
                                        <option value="{{ $item->id }}" {{ $i->product_id == $item->id ? 'selected': ''}}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                                <td><select class="form-control select2" name="warehouse_id[]">
                                    @foreach($warehouse as $value)
                                        <option value="{{ $value->id }}" {{ $i->warehouse_id == $value->id ? 'selected': ''}}>{{ $value->name }}</option>' +
                                    @endforeach
                                </select></td>
                                <td><input type="number" name="qty[]" class="form-control" value="{{$i->qty}}" /></td>
                                <td><select class="form-control select2" name="type[]">
                                        <option value="IN" {{ $i->type == 'IN' ? 'selected': ''}}>IN</option>
                                        <option value="OUT" {{ $i->type == 'OUT' ? 'selected': ''}}>OUT</option>

                                </select></td>
                                <td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            <hr>
            <div class="card-body">
                {{ Form::submit('Update', array('class' => 'btn btn-primary btn-lg')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script >
       $(document).ready(function () {


        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });
        });
  $(function(){
        var no = 0;
        $('#addText').click(function(event){
            event.preventDefault();
            no++;
            var newRow = $('<tr id="item'+no+'">' +

                                '<td>'+
                                    '<select class="select2 form-control" name="product_id[]">' +
                                        <?php foreach($product as $item){ ?>
                                            '<option value="{{ $item->id }}">{{ $item->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +

                                '</td>' +
                                '<td>' +
                                    ' <select class="form-control select2" name="warehouse_id[]">' +
                                        <?php foreach($warehouse as $value){ ?>
                                            '<option value="{{ $value->id }}">{{ $value->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +
                                '</td>' +

                                '<td>' +
                                    '<input type="number" id="form-field-1"  name="qty[]" class="form-control"  style="text-align:right"/>' +
                                '</td>' +
                                ' <td><select class="form-control select2" name="type[]">'+
                                            '<option value="IN">IN</option>'+
                                            '<option value="OUT">OUT</option>'+

                                    '</select></td>'+

                                '<td><button style="height:30px;width:25px;padding:2px;" class="btn btn-danger" onclick="remove('+no+')"><span ><i class="ace-icon fa fa-trash"></i></span></button>' +
                                '</td>' +

                            '</tr>' );
            $('#tab-stock').append(newRow);
            $('.select2').select2({
            placeholder: 'Please Select',
            containerCssClass: 'form-control',
            allowClear: true,
            width: '100%'
        });

        });
    });
    function remove(no){
        $('#item'+no).remove();
    }

    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('wheel.disableScroll', function (e) {
        e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('wheel.disableScroll')
    })
</script>
@endpush
