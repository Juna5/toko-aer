@extends('stock::layouts.app')

@section('stock::title', 'Stock Adjustment')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Adjustment',
'active' => true,
'url' => route('stock.stock-adjustment.index')
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Edit',
'active' => false
])
@endsection

@section('stock::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            {{ Form::model($warehouse, array('route' => array('stock.stock-adjustment.store'), 'method' => 'POST', 'files' => true)) }}
            <div class="card-body">
                <h5 class="card-title">
                   Stock Adjustment
                    <div class="text-right">
                        <a href="{{ route('stock.stock-adjustment.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('include.error-list')
                  @include('flash::message')
                <div class="form-group pb-1">
                    <label for="photo_profile">Date</label>
                    <input type="text" class="form-control r-0 light s-12 datepicker" name="date" id="date" value="{{ old('date') ? : now()->format('Y-m-d') }}">
                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Description</label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
                <button id="addText" class="btn btn-success" type="button">
                            <i class="ace-icon fa fa-plus bigger-110"></i>
                            Add Item
                </button>
                <br>
                <br>
                <br>
                <table class="table table-striped" id="sortable-table" >
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Warehouse</th>
                            <th>Qty</th>
                            <th>Type</th>
                            <th></th>
                        </tr>
                   </thead>
                    <tbody id="tab-stock">
                        <tr>
                            <td>
                            <select class="select2 form-control" name="product_id[]">
                                @foreach($product as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                            <td><select class="form-control select2" name="warehouse_id[]">
                                @foreach($warehouse as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>' +
                                @endforeach
                            </select></td>
                            <td style="text-align:right"><input type="text" name="qty[]" class="form-control" style="text-align:right"/></td>
                            <td><select class="form-control select2" name="type[]">
                                    <option value="IN">IN</option>
                                    <option value="OUT">OUT</option>
                            </select></td>
                            <td>
                        </td>
                        </tr>
                    </tbody>
                </table>
            <hr>
            <div class="card-body">
                {{ Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script >
       $(document).ready(function () {


        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });
        });
  $(function(){
        var no = 0;
        $('#addText').click(function(event){
            event.preventDefault();
            no++;
            var newRow = $('<tr id="item'+no+'">' +


                                '<td>'+
                                    '<select class="select2 form-control" name="product_id[]">' +
                                        <?php foreach($product as $item){ ?>
                                            '<option value="{{ $item->id }}">{{ $item->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +

                                '</td>' +
                                '<td>' +
                                    ' <select class="form-control select2" name="warehouse_id[]">' +
                                        <?php foreach($warehouse as $value){ ?>
                                            '<option value="{{ $value->id }}">{{ $value->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +
                                '</td>' +

                                '<td style="text-align:right">' +
                                    '<input type="numbe" id="form-field-1" style="text-align:right"  name="qty[]" class="form-control" style="text-align:right"/>' +
                                '</td>' +
                                ' <td><select class="form-control select2" name="type[]">'+
                                            '<option value="IN">IN</option>'+
                                            '<option value="OUT">OUT</option>'+

                                    '</select></td>'+

                                '<td><button style="height:30px;width:25px;padding:2px;" class="btn btn-danger" onclick="remove('+no+')"><span ><i class="ace-icon fa fa-trash"></i></span></button>' +
                                '</td>' +

                            '</tr>' );
            $('#tab-stock').append(newRow);
            $('.select2').select2({
            placeholder: 'Please Select',
            containerCssClass: 'form-control',
            allowClear: true,
            width: '100%'
        });

        });
    });
    function remove(no){
        $('#item'+no).remove();
    }

</script>
@endpush
