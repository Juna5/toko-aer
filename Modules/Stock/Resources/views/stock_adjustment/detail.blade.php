@extends('stock::layouts.app')

@section('stock::title', 'Stock Adjustment')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Adjustment',
'active' => true,
'url' => route('stock.stock-adjustment.index')
])
@endsection

@section('stock::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="invoice-number text-primary">{{ $stockAdjustment->code_number }}</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($stockAdjustment->date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Description</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $stockAdjustment->description }}</td>
                                                    </tr>
                                                    
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                            </div>

                           
                                    <div class="row mt-4">
                                        <div class="col-lg-12">
                                            <div class="section-title">Item</div>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-md">
                                                    <tr>
                                                        <th>Product</th>
                                                        <th>Warehouse</th>
                                                        <th>Qty</th>
                                                        <th>Type</th>
                                                    </tr>
                                                    @foreach($item as $value)
                                                    <tr>
                                                       <td>{{ optional($value->product)->name }}</td>
                                                       <td>{{ optional($value->warehouse)->name }}</td>
                                                       <td>{{ $value->qty }}</td>
                                                       <td>{{ $value->type }}</td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                      <div class="text-md-right">
                                        <a href="{{ route('stock.stock-adjustment.index') }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
