@extends('stock::layouts.app')

@section('stock::title', 'Stock Transfer')

@section('stock::breadcrumb-2')
@include('stock::include.breadcrum', [
'title' => 'Stock Transfer',
'active' => true,
'url' => route('stock.stock-transfer.index')
])
@endsection
@section('stock::breadcrumb-3')
@include('stock::include.breadcrum', [
'title' => 'Create',
'active' => false
])
@endsection

@section('stock::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('stock.stock-transfer.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <stock-transfer-create />
                    </div>
                </div>
            </div>
         <!-- {{ Form::model($warehouse, array('route' => array('stock.stock-transfer.store'), 'method' => 'POST', 'files' => true)) }}
            <div class="card-body">
                <h5 class="card-title">
                   Stock Transfer
                    <div class="text-right">
                        <a href="{{ route('stock.stock-transfer.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>

                @include('include.error-list')
                @include('flash::message')
                <div class="form-group pb-1">
                    <label for="photo_profile">Date</label>
                    <input type="text" class="form-control r-0 light s-12 datepicker" name="date" id="date" value="{{ old('date') }}">

                </div>
                <div class="form-group pb-1">
                    <label for="photo_profile">Remark</label>
                    <textarea class="form-control" name="description">{{ old('description') }}</textarea>
                </div>
               {{--  <button id="addText" class="btn btn-success" type="button">
                            <i class="ace-icon fa fa-plus bigger-110"></i>
                            Add Item
                </button> --}}
                 <br>
                <br>
                <br>
                <div style="overflow-x: scroll;">
                <table class="table table-striped" id="sortable-table" >
                    <thead>
                        <tr>
                            <th>Product</th>
                            <th>Origin Warehouse</th>
                            <th>Destination Warehouse</th>
                            <th>Qty</th>
                            <th></th>
                        </tr>
                   </thead>
                    <tbody id="tab-stock">
                         @foreach($product as $index => $p)
                        <tr id="item{{$index}}">
                            <td>

                            <select class="select2 form-group-control" name="product_id[]">
                                @foreach($product as $item)
                                    @if(old('product_id'))
                                        <option value="{{ $item->id }}" {{ old('product_id.'.$index) == $item->id ? 'selected':''}}>{{ $item->name }} </option>
                                    @else
                                    <option value="{{ $item->id }}" {{ $p->id == $item->id ? 'selected':''}}>{{ $item->name }} </option>
                                    @endif
                                @endforeach
                            </select>
                        </td>
                           <?php old('warehouse_id_origin[]') ?>
                            <td><select class="form-control select2" name="warehouse_id_origin[]">
                                @foreach($warehouse as $value)
                                    @if(old('warehouse_id_origin'))
                                        <option value="{{ $value->id }}" {{ old('warehouse_id_origin.'.$index) == $item->id ? 'selected':''}}>{{ $value->name }}</option>
                                    @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endif
                                @endforeach
                            </select></td>
                              <td><select class="form-control select2" name="warehouse_id_destination[]">
                                @foreach($warehouse as $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select></td>
                            <td><input type="number" name="qty[]" class="form-control" value="{{ old('qty.'.$index) }}" required="" style="text-align:right" /></td>
                            <td>
                            <button style="height:30px;width:25px;padding:2px;" class="btn btn-danger" onclick="remove({{$index}})"><span ><i class="ace-icon fa fa-trash"></i></span></button>
                        </td>
                        </tr>
                        @endforeach

                    </tbody>

                </table>
            </div>
            <hr>
            <div class="card-body">
                {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg')) }}
            </div>
          {{ Form::close() }}  -->
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script >
       $(document).ready(function () {


        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });
        });
  $(function(){
        var no = 0;
        $('#addText').click(function(event){
            event.preventDefault();
            no++;
            var newRow = $('<tr id="item'+no+'">' +


                                '<td>'+
                                    '<select class="select2 form-control" name="product_id[]">' +
                                        <?php foreach($product as $item){ ?>
                                            '<option value="{{ $item->id }}">{{ $item->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +

                                '</td>' +
                                '<td>' +
                                    ' <select class="form-control select2" name="warehouse_id_origin[]">' +
                                        <?php foreach($warehouse as $value){ ?>
                                            '<option value="{{ $value->id }}">{{ $value->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +
                                '</td>' +
                                 '<td>' +
                                    ' <select class="form-control select2" name="warehouse_id_destination[]">' +
                                        <?php foreach($warehouse as $value){ ?>
                                            '<option value="{{ $value->id }}">{{ $value->name }}</option>' +
                                        <?php } ?>
                                    '</select>' +
                                '</td>' +
                                '<td>' +
                                    '<input type="number" id="form-field-1"  name="qty[]" class="form-control" style="text-align:right"/>' +
                                '</td>' +


                                '<td><button style="height:30px;width:25px;padding:2px;" class="btn btn-danger" onclick="remove('+no+')"><span ><i class="ace-icon fa fa-trash"></i></span></button>' +
                                '</td>' +

                            '</tr>' );
            $('#tab-stock').append(newRow);
            $('.select2').select2({
            placeholder: 'Please Select',
            containerCssClass: 'form-control',
            allowClear: true,
            width: '100%'
        });

        });
    });
    function remove(no){
        $('#item'+no).remove();
    }

    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('wheel.disableScroll', function (e) {
        e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('wheel.disableScroll')
    })
</script>
@endpush
