@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $costCenter->name }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Cost Center Number</label>
            <input type="text" class="form-control r-0 light s-12" name="cost_center_number" value="{{ old('cost_center_number') ?? $costCenter->cost_center_number }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $costCenter->description }}</textarea>
        </div>
        <div class="form-group">
            <div class="control-label">Status</div>
            <div class="custom-switches-stacked mt-2">
                <label class="custom-switch">
                    <input type="radio" name="active" value="1" required class="custom-switch-input" {{ old('active') ?? $costCenter->active == "1" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Active</span>
                </label>
                <label class="custom-switch">
                    <input type="radio" name="active" value="0" required class="custom-switch-input" {{ old('active') ?? $costCenter->active == "0" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Inactive</span>
                </label>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>