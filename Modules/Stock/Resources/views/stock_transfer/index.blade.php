@extends('stock::layouts.app')

@section('stock::title', 'Stock Transfer')

@section('stock::breadcrumb-2')
    @include('stock::include.breadcrum', [
    'title' => 'Stock Transfer',
    'active' => true,
    'url' => route('stock.stock-transfer.index')
    ])
@endsection

@section('stock::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                @include('flash::message')
                <div class="card-body p-10">
                    <div class="table-responsive table-striped">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush