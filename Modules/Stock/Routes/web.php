<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::name('stock.')->prefix('stock')->middleware(['impersonate', 'auth'])->group(function () {

        Route::resource('stock', 'StockController');
        Route::resource('stock-transaction', 'StockTransactionController');
        Route::post('stock/get-data', 'StockController@getData')->name('stock-getdata');
        Route::get('/ajax-code', 'StockTransactionController@ajax')->name('stock-ajax');
        Route::get('/ajax-qty', 'StockTransactionController@qty')->name('stock-ajaxQty');

        Route::get('/ajax-codeStock', 'StockController@codeNumber')->name('stock.codeNumber');
        Route::get('/ajax-qtyStock', 'StockController@productList')->name('stock.product');

        Route::resource('stock-adjustment','StockAdjustmentController');
        Route::get('stock-adjustment-report', 'StockAdjustmentController@report')->name('stock-adjustment-report');
        Route::get('/stock-adjustment-report-detail/{id}', 'StockAdjustmentController@reportDetail')->name('stock-adjustment-report-detail');
        Route::get('/{id}/stock-adjustment-detail', 'StockAdjustmentController@detail')->name('adjustment-detail');
        
        Route::resource('stock-transfer','StockTransferController');
        Route::get('stock-transfer-report', 'StockTransferController@report')->name('stock-transfer-report');
        Route::get('/stock-transfer-report-detail/{id}', 'StockTransferController@reportDetail')->name('stock-transfer-report-detail');
        Route::get('/{id}/stock-transfer-detail', 'StockTransferController@detail')->name('transfer-detail');
        Route::put('/{id}', 'StockTransferController@update')->name('update');
        
        Route::resource('stock-checking','StockCheckingController');
        Route::get('stock-checking-outstanding', 'StockCheckingController@outstanding')->name('stock-checking-outstanding');
        Route::get('stock-checking-report', 'StockCheckingController@report')->name('stock-checking-report');
        Route::get('/stock-cheking/sales_order', 'StockCheckingController@sales')->name('sales');
        Route::get('/stock-checking-report-detail/{id}', 'StockCheckingController@reportDetail')->name('stock-checking-report-detail');

        Route::resource('stock-in','StockInController');
        Route::get('stock-in-report', 'StockInController@report')->name('stock-in-report');
        Route::get('/stock-in-report-detail/{id}', 'StockInController@reportDetail')->name('stock-in-report-detail');
        Route::put('stock-in-update/{id}', 'StockInController@update')->name('stock-in.update');
        // Route::get('/stock-in/sales_order_material','StockInController@salesOrderMaterials')->name('sales_order_material');
});
