<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/procurement', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->name('api.')->group(function () {
 
    Route::get('warehouse', 'WarehouseController@index')->name('stock.warehouse');
    Route::get('stock-transfer/{table}/{name}', 'StockTransferController@generateCode')->name('stock-transfer.generate-code');
    Route::get('stock/get-stock/{id}', 'StockController@getStock')->name('stock.get-stock');
    Route::get('stock/get-stock-detail/{id}','StockTransferController@detailTransfer')->name('get-stock-detail');

});
