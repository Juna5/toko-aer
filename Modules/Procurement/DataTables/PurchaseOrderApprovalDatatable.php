<?php

namespace Modules\Procurement\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Modules\Procurement\Entities\PurchaseOrderApproval;

class PurchaseOrderApprovalDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
           ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('procurement.purchase-order.detail', [$row->purchase_order_id, 'approval' => true]) . '">' . optional($row->purchaseOrder)->code_number . '</a>';
                return $detail;
            })
            ->editColumn('employee', function ($row) {
                return optional($row->employee)->name;
            })
            ->editColumn('title', function ($row) {
                return optional($row->purchaseOrder)->title;
            })
            ->editColumn('purchase_requisition_id', function ($row) {
                if($row->purchaseOrder->purchase_requisition_id){
                    $detail = '<a target="_blank" href="' . route('procurement.purchase_requisition.detail', $row->purchaseOrder->purchaseRequisition->id) . '">' . 
                    optional($row->purchaseOrder->purchaseRequisition)->code_number . '</a>';
                }else{
                   $detail = '-'; 
                }
                return $detail;
                
            })
            ->editColumn('is_approved', function ($row) {
                if ($row->is_approved == 0) {
                    return "Rejected";
                }elseif ($row->is_approved == 1) {
                    return "Approved";
                }
            })
            ->rawColumns(['status', 'code_number', 'purchase_requisition_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseOrderApprovalDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseOrderApproval $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        $row = $model
            ->with(['purchaseOrder', 'purchaseOrder.employee', 'purchaseOrder.purchaseRequisition'])
            ->where('employee_id', $employee->id)
            ->whereNotNull('date')
            ->latest()
            ->get()
            ->unique('purchase_order_id');
           
        return $row;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('purchaseorderapprovaldatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->parameters([
                        'order' => [
                            2,
                            'ASC'
                        ]
                    ])
                    ->buttons([
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number'),
            Column::make('purchase_requisition_id')->addClass('text-center')->title('Transfered From'),
            Column::make('employee')->addClass('text-center')->title('Created By'),
            Column::make('date')->addClass('text-center'),
            // Column::make('title')->addClass('text-center'),
            Column::make('is_approved')->addClass('text-center')->title('Approval Status'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseOrderApproval_' . date('YmdHis');
    }
}
