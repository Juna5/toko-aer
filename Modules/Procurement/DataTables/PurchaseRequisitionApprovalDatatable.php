<?php

namespace Modules\Procurement\DataTables;

use Modules\HR\Entities\Employee;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Modules\Procurement\Entities\PurchaseRequisitionApproval;
use Yajra\DataTables\Services\DataTable;

class PurchaseRequisitionApprovalDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('procurement.purchase_requisition.detail', [$row->purchase_requisition_id, 'approval' => true]) . '">' . optional($row->purchaseRequisition)->code_number . '</a>';
                return $detail;
            })
            ->editColumn('employee', function ($row) {
                return optional($row->employee)->name;
            })
            ->editColumn('title', function ($row) {
                return optional($row->purchaseRequisition)->title;
            })
            ->editColumn('is_approved', function ($row) {
                if ($row->is_approved == 0) {
                    return "Rejected";
                }elseif ($row->is_approved == 1) {
                    return "Approved";
                }
            })
            ->rawColumns(['status', 'code_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseRequisitionApprovalDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseRequisitionApproval $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        $row = $model
            ->with(['purchaseRequisition', 'purchaseRequisition.employee'])
            ->where('employee_id', $employee->id)
            ->whereNotNull('date')
            ->get()
            ->unique('purchase_requisition_id');

        return $row;

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('purchaserequisitionapprovaldatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->parameters([
                        'order' => [
                            2,
                            'ASC'
                        ]
                    ])
                    ->buttons([
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number'),
            Column::make('employee')->addClass('text-center')->title('Created By'),
            Column::make('date')->addClass('text-center'),
            // Column::make('title')->addClass('text-center'),
            Column::make('is_approved')->addClass('text-center')->title('Approval Status'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseRequisitionApproval_' . date('YmdHis');
    }
}
