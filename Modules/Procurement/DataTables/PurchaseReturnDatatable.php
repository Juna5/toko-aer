<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\PurchaseReturn;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PurchaseReturnDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('supplier_id', function ($row) {
                return $row->supplier->company_name;
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee_id ? optional($row->employee)->name : '-';
            })
            ->addColumn('action', function ($row) {
                $openDiv = '<div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';

                $edit = '<a href="' . route('procurement.purchase-return.edit', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="Edit"><i class="fa fa-edit" style="color: blue" aria-hidden="true"></i> Edit</a>';
                $delete = '<a data-href="' . route('procurement.purchase-return.destroy', $row->id) . '"data-toggle="modal" data-target="#confirm-delete-modal"  style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Delete">
                                <i class="fa fa-trash" style="color: red" aria-hidden="true"></i> Delete</a>';
                $adjustment = '<a data-href="' . route('procurement.purchase_return.adjustment', ['id' => $row->id]) . '" data-toggle="modal" data-target="#adjustment" id="modal_adjustment" data-id="' . $row->id . '" style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Stock Adjustment"><i class="fa fa-paperclip" style="color: green" aria-hidden="true"></i> Adjustment
                    </a>';

                return $openDiv . (userCan('view purchase return') ? $edit : '') . $adjustment . (userCan('delete purchase return') ? $delete : '');
            })
            ->rawColumns(['status', 'action', 'invoice_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseInvoiceDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return PurchaseReturn::with('supplier', 'employee')->get(['id', 'date', 'code_number', 'supplier_id', 'description', 'employee_id']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('purchaseinvoicedatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('supplier_id')->title('Supplier')->addClass('text-center'),
            Column::make('description')->title('Remarks')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseInvoice_' . date('YmdHis');
    }
}
