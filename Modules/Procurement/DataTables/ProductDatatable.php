<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use \Milon\Barcode\DNS1D;
// use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ProductDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addColumn('expired_date', function ($row) {
                return format_d_month_y($row->expired_date);
            })
            ->editColumn('productcategory_id', function ($row) {
                return optional($row->productCategory)->name;
            })
            ->editColumn('warehouse_id', function ($row) {
                return optional($row->warehouseMaster)->name;
            })
            ->editColumn('product_category_id', function ($row) {
                return optional($row->productCategory)->code. '-'. optional($row->productCategory)->description;
            })
            ->editColumn('location_id', function ($row) {
                return optional($row->location)->name;
            })
            // ->addColumn('barcode', function ($row) {
            //     return QrCode::size(100)->generate((string) $row->id);
            // })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.master.product.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('procurement.master.product.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view product') ? $edit : '') . (userCan('delete product') ? $delete : '');
            })
            ->rawColumns(['barcode', 'action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ProductDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::make('product_category_id')->addClass('text-center')->title('Product Category'),
            Column::make('code')->addClass('text-center')->title('Item Code'),
            Column::make('name')->addClass('text-center')->title('Item Name'),
             Column::make('description')->addClass('text-center'),
            Column::make('brand')->addClass('text-center'),
            // Column::make('warehouse_id')->title('Warehouse')->addClass('text-center'),
            // Column::make('barcode')->addClass('text-center'),
            // Column::make('expired_date')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
