<?php

namespace Modules\Procurement\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Modules\Procurement\Entities\PurchaseInvoice;

class PurchaseInvoiceDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('invoice_date', function ($row) {
                return format_d_month_y($row->invoice_date);
            })
            ->editColumn('invoice_due_date', function ($row) {
                return format_d_month_y($row->invoice_due_date);
            })
            ->editColumn('amount', function ($row) {
                return amount_international_with_comma($row->total);
            })
            ->editColumn('invoice_number', function ($row) {
                return '<a href="' . route('procurement.purchase_invoice.pdf', $row->id) . '" target="_blank">' . $row->invoice_number . '</a>';
            })
            ->editColumn('outstanding', function ($row) {
                return amount_international_with_comma($row->outstanding);
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee_id ? optional($row->employee)->name : '-';
            })
            ->editColumn('purchase_order_id', function ($row) {
                $detail = '<a target="_blank" href="' . route('procurement.purchase-order.detail', $row->purchaseOrder->id) . '">' .
                    optional($row->purchaseOrder)->code_number . '</a>';
                return $detail;
            })
            ->editColumn('status', function ($row) {
                if ($row->payment_status == 'Paid') {
                    return '<div class="badge badge-success">Paid</div>';
                } elseif ($row->payment_status == 'Paid Partially') {
                    return '<div class="badge badge-warning">Paid Partially</div>';
                } else {
                    return '<div class="badge badge-primary">Unpaid</div>';
                }
            })
            ->addColumn('action', function ($row) {
                $openDiv = '<div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';

                $view = '<a href="' . route('procurement.purchase_invoice.show', $row->id) . '" style="margin-left: 10px" class="dropdown-item" title="Edit"><i class="fa fa-eye" style="color: blue" aria-hidden="true"></i> Detail</a>';
                $delete = '<a data-href="' . route('procurement.purchase_invoice.destroy', $row->id) . '"data-toggle="modal" data-target="#confirm-delete-modal"  style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Delete">
                                <i class="fa fa-trash" style="color: red" aria-hidden="true"></i> Delete</a>';
                $pay = '<a href="#" style="margin-left: 10px" class="dropdown-item" 
                            id="pay_modal" 
                            data-id="' . $row->id . '"
                            data-toggle="modal" 
                            data-target="#modal-global" 
                            title="Edit"><i class="fa fa-plus" style="color: grey" aria-hidden="true"></i> Add Payment</a>';

                $removePayButton = $row->payment_status != 'Paid' ? (userCan('view purchase invoice') ? $pay : '') : null;

                return $openDiv . (userCan('view purchase invoice') ? $view : '') . (userCan('delete purchase invoice') ? $delete : '') . $removePayButton;
            })
            ->rawColumns(['status', 'action', 'invoice_number', 'purchase_order_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseInvoiceDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseInvoice $model)
    {
        return $model->with('employee')->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('purchaseinvoicedatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->parameters([
                'order' => [
                    0,
                    'DESC'
                ]
            ])
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('invoice_number')->title('Purchase Invoice Number')->addClass('text-center'),
            Column::make('purchase_order_id')->title('Transfered From')->addClass('text-center'),
            Column::make('invoice_date')->title('Date')->addClass('text-center'),
            Column::make('invoice_due_date')->title('Due Date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('amount')->addClass('text-right'),
            Column::make('outstanding')->addClass('text-right'),
            Column::make('status')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseInvoice_' . date('YmdHis');
    }
}
