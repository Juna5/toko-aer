<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\CreditDebitNote;
use Modules\Finance\Entities\InvoicePayment;
use Modules\Finance\Entities\Receivable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class APCreditDebitNoteDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('amount', function ($row) {
                return amount_international_with_comma($row->amount);
            })
            ->editColumn('payable_id', function ($row) {
                return $row->payable ? optional($row->payable)->code_number : '-';
            })
            ->editColumn('supplier_id', function ($row) {
                return optional($row->supplier)->company_name;
            })
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
             ->editColumn('employee_id', function ($row) {
                return $row->employee ? optional($row->employee)->name : '-';
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.ap_credit_debit_note.edit', $row->id) . '" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit" data-id="'.$row->id.'" >
                            <i class="fa fa-pencil-alt"></i>
                        </a>';
                $delete = '<a data-href="' . route('procurement.ap_credit_debit_note.destroy', $row->id) . '" style="margin-left: 10px" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash text-white"></i></a>';

                return (userCan('view credit note') ? $edit : '') . (userCan('delete credit note') ? $delete : '');
            })
            ->rawColumns(['action']);
    }

    public function query(CreditDebitNote $model)
    {
        return $model->with('receivable','receivable.details','employee','customer','supplier','payable','payable.details')->whereNotNull('supplier_id')->whereNull('deleted_at')->get();
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('payable_id')->title('A/P Payment')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('supplier_id')->title('Supplier')->addClass('text-center'),
            Column::make('amount')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'CreditDebitNote_' . date('YmdHis');
    }
}
