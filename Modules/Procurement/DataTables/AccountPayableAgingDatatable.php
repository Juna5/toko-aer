<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\PurchaseInvoice;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AccountPayableAgingDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
        ->of($query)
            ->editColumn('company_name', function ($row){
                return optional($row->purchaseOrder->supplier)->company_name;
            })
            ->editColumn('invoice_date', function ($row) {
                return format_d_month_y($row->invoice_date);
            })
            ->editColumn('invoice_due_date', function ($row) {
                return format_d_month_y($row->invoice_due_date);
            })
            ->editColumn('amount', function ($row) {
                return amount_international_with_comma($row->total);
            })
            ->editColumn('invoice_number', function ($row) {
                return '<a href="'.route('procurement.purchase_invoice.pdf', $row->id).'" target="_blank">'.$row->invoice_number.'</a>';
            })
            ->editColumn('30_days', function ($row) {
                return amount_international_with_comma(getAgingDaysPayable($row->id, $row->invoice_due_date, '30_days'));
            })
            ->editColumn('60_days', function ($row) {
                return amount_international_with_comma(getAgingDaysPayable($row->id, $row->invoice_due_date, '60_days'));
            })
            ->editColumn('90_days', function ($row) {
                 return amount_international_with_comma(getAgingDaysPayable($row->id, $row->invoice_due_date, '90_days'));
            })
            ->editColumn('unlimited_days', function ($row) {
                return amount_international_with_comma(getAgingDaysPayable($row->id, $row->invoice_due_date, 'unlimited_days'));
            })
            ->addColumn('action', function ($row) {
                $view = '<a href="' . route('procurement.purchase_invoice.show', $row->id) . '"
                            class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="far fa-eye"></i>';
                return (userCan('view detail account payable') ? $view : '');
            })
             ->rawColumns(['action','invoice_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\AccountReceivableDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseInvoice $model)
    {
        return $model->with('purchaseOrder.supplier')->where('outstanding','>','0')->whereNull('deleted_at')->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('accountpayableagingdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
         return [
            Column::make('company_name')->title('Supplier')->addClass('text-center'),
            Column::make('invoice_number')->addClass('text-center'),
            Column::make('invoice_date')->addClass('text-center'),
            Column::make('invoice_due_date')->title('Due Date')->addClass('text-center'),
            Column::computed('30_days')->title('0-30 Days')->addClass('text-center'),
            Column::computed('60_days')->title('31-60 Days')->addClass('text-center'),
            Column::computed('90_days')->title('61-91 Days')->addClass('text-center'),
            Column::computed('unlimited_days')->title('>90 Days')->addClass('text-center'),
            Column::make('amount')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AccountPayableAging_' . date('YmdHis');
    }
}
