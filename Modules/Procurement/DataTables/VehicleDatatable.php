<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\Vehicle;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class VehicleDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('car_registration_expiry_date', function ($row) {
                return format_d_month_y($row->car_registration_expiry_date);
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.master.vehicle.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('procurement.master.vehicle.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view vehicle') ? $edit : '') . (userCan('delete vehicle') ? $delete : '');
        });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\VehicleDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Vehicle $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('vehicledatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons([
                        Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            // Column::make('code')->addClass('text-center'),
            Column::make('name')->addClass('text-center'),
            // Column::make('description')->addClass('text-center'),
            Column::make('brand')->addClass('text-center'),
            Column::make('type')->addClass('text-center'),
            Column::make('car_registration_no')->addClass('text-center'),
            Column::make('car_registration_expiry_date')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Vehicle_' . date('YmdHis');
    }
}
