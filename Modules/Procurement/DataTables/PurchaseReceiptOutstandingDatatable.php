<?php

namespace Modules\Procurement\DataTables;

use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseReceipt;

class PurchaseReceiptOutstandingDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('total', function ($row) {
                return amount_international_with_comma($row->total);
            })
            ->editColumn('purchase_requisition_id', function ($row) {
                 $detail = '<a target="_blank" href="' . route('procurement.purchase_requisition.detail', $row->purchaseRequisition->id) . '">' .
                    optional($row->purchaseRequisition)->code_number . '</a>';
                return $detail;
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee_id ? optional($row->employee)->name : '-';
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a target="_blank" href="' . route('procurement.purchase-order.detail', $row->id) . '">' .
                   $row->code_number . '</a>';
                return $detail;
            })
            ->addColumn('action', function ($row) {
                $purchaseReceipt = PurchaseReceipt::where('purchase_order_id', $row->id)->first();
                $openDiv = '<div class="btn-group" role="group">
                <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';
                $pReceipt = "";
                    if ($purchaseReceipt == null) {
                        $pReceipt = '<a href="' . route('procurement.purchase_receipt.create', ['poId' => $row->id]) . '"
                        style="margin-left: 10px" class="dropdown-item" title="Create Purchase Receipt">
                        <i class="fa fa-file" style="color: #046ea8"></i> Purchase Receipt
                        </a>';
                    }
                return $openDiv . $pReceipt . $closeDiv;
            })
            ->rawColumns(['action', 'date','code_number','purchase_requisition_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseReceiptOutstandingDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseOrder $model)
    {
        $purchaseReceipt = PurchaseReceipt::whereNotNull('purchase_order_id')->get();

       return $purchaseOrder   = PurchaseOrder::with('employee', 'supplier', 'items')
            ->approved()
            ->whereNotIn('id', $purchaseReceipt->pluck('purchase_order_id'))
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('purchasereceiptoutstandingdatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons([
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number')->title('Purchase Order')->addClass('text-center'),
            Column::make('purchase_requisition_id')->title('Transfered From')->addClass('text-center'),
            Column::make('date')->title('Date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('
                text-center'),
            Column::make('total')->addClass('text-right'),
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseReceiptOutstanding_' . date('YmdHis');
    }
}
