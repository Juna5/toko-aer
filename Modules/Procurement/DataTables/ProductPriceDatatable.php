<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\ProductPrice;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductPriceDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->addColumn('product_id', function ($row) {
                return optional($row->product)->name;
            })
            ->addColumn('uom_id', function ($row) {
                return optional($row->uom)->name;
            })
            ->editColumn('price_category_id', function ($row) {
                return optional($row->priceCategory)->name;
            })
            ->editColumn('price', function ($row) {
                return amount_international_with_comma($row->price);
            })
            ->editColumn('profit_margin', function ($row) {
                return amount_international_with_comma($row->profit_margin);
            })
            ->editColumn('discount_amount', function ($row) {
                return amount_international_with_comma($row->discount_amount);
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.master.product_price.edit', $row->id) . '" class="btn btn-info" title="Edit entry"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('procurement.master.product_price.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal" title="Delete entry"><i class="fa fa-trash"></i></a>';
                $duplicate = '<a href="' . route('procurement.master.product_price.duplicate', $row->id) . '" class="btn btn-warning" style="margin-left: 10px" title="Duplicate entry"><i class="fa fa-copy"></i></a>';
                return (userCan('view product price') ? $edit : '') . (userCan('delete product price') ? $delete : '') . $duplicate;
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ProductPriceDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ProductPrice $model)
    {
        $current_uri = Request()->segment(4);
        return $model->where('product_id', $current_uri)->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('product_id')->addClass('text-center')->title('Product'),
            Column::make('uom_id')->addClass('text-center')->title('UoM'),
            Column::make('price_category_id')->addClass('text-center')->title('Price Category'),
            Column::make('price')->addClass('text-right'),
            Column::make('profit_margin')->addClass('text-right'),
            Column::make('discount')->title('Discount % (Rate)')->addClass('text-right'),
            Column::make('discount_amount')->title('Discount (Amount)')->addClass('text-right'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
