<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\PurchaseInvoice;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class AccountPayableDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
        ->of($query)
            ->editColumn('invoice_date', function ($row) {
                return format_d_month_y($row->invoice_date);
            })
            ->editColumn('invoice_due_date', function ($row) {
                return format_d_month_y($row->invoice_due_date);
            })
            ->editColumn('amount', function ($row) {
                return amount_international_with_comma($row->total);
            })
            ->editColumn('invoice_number', function ($row) {
                return '<a href="'.route('procurement.purchase_invoice.pdf', $row->id).'" target="_blank">'.$row->invoice_number.'</a>';
            })
            ->editColumn('outstanding', function ($row) {
                return amount_international_with_comma($row->outstanding);
            })
            ->editColumn('balance', function ($row) {
                return amount_international_with_comma($row->total - $row->outstanding);
            })
            ->editColumn('payment_status', function ($row) {
                if ($row->payment_status == 'Paid') {
                    return '<div class="badge badge-success">Paid</div>';
                } elseif ($row->payment_status == 'Paid Partially') {
                    return '<div class="badge badge-warning">Paid Partially</div>';
                } else {
                    return '<div class="badge badge-primary">Unpaid</div>';
                }
            })
            ->addColumn('action', function ($row) {
                $view = '<a href="' . route('procurement.purchase_invoice.show', $row->id) . '"
                            class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Detail"><i class="far fa-eye"></i>';
                return (userCan('view detail account payable') ? $view : '');
            })
             ->rawColumns(['payment_status', 'action','invoice_number']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\AccountReceivableDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseInvoice $model)
    {
        return $model->where('outstanding','>','0')->whereNull('deleted_at')->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('accountpayabledatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
         return [
            Column::make('invoice_number')->addClass('text-center'),
            Column::make('invoice_date')->addClass('text-center'),
            Column::make('invoice_due_date')->addClass('text-center'),
            Column::make('supplier_name')->addClass('text-center'), 
            Column::make('amount')->addClass('text-center'),
            Column::make('balance')->addClass('text-center')->title('Payment'),
            Column::make('outstanding')->addClass('text-center'),
            Column::make('payment_status')->title('Status')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'AccountPayable_' . date('YmdHis');
    }
}
