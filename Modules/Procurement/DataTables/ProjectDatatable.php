<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\Project;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProjectDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('order_date', function ($row) {
                return format_d_month_y($row->order_date);
            })
            ->editColumn('delivery_date', function ($row) {
                return format_d_month_y($row->delivery_date);
            })
            ->editColumn('employee_id', function ($row) {
                return optional($row->employee)->name;
            })
            ->editColumn('customer_id', function ($row) {
                return optional($row->customer)->company_name;
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.master.project.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('procurement.master.project.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view project') ? $edit : '') . (userCan('delete project') ? $delete : '');
            })
            ->rawColumns(['action']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\ProjectDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Project $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('projectdatatable-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons([
                        Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                        Button::make('print'),
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name')->addClass('text-center'),
            // Column::make('order_date')->addClass('text-center'),
            // Column::make('delivery_date')->addClass('text-center'),
            Column::make('employee_id')->title('Project Manager')->addClass('text-center'),
            Column::make('customer_id')->title('Customer')->addClass('text-center'),
            Column::make('status')->addClass('text-center'),
            Column::make('finish')->addClass('text-right'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Project_' . date('YmdHis');
    }
}
