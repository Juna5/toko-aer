<?php

namespace Modules\Procurement\DataTables;

use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\WarehouseMaster;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CustomerDatatable extends DataTable
{
        /**
         * Build DataTable class.
         *
         * @param mixed $query Results from query() method.
         * @return \Yajra\DataTables\DataTableAbstract
         */
        public function dataTable($query)
        {
                return datatables()
                        ->of($query)
                        ->addColumn('edit', function ($row) {
                                return '<a href="' . route('procurement.master.customer.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                       })
                        ->addColumn('delete', function ($row) {
                                return '<a data-href="' . route('procurement.master.customer.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                       })
                       ->rawColumns(['edit', 'delete']);
                }

        /**
         * Get query source of dataTable.
         *
         * @param \App\CustomerDatatable $model
         * @return \Illuminate\Database\Eloquent\Builder
         */
        public function query()
        {
                return DB::select(" select c.*, wm.name as depot_name from customers c
                        left join employees e on e.id = c.employee_id
                        left join warehouse_masters wm on wm.id = e.depot_id
                        where c.deleted_at is null 
                        and e.deleted_at is null");
        }

        /**
         * Optional method if you want to use html builder.
         *
         * @return \Yajra\DataTables\Html\Builder
         */
        public function html()
        {
            return $this->builder()
                ->columns($this->getColumns())
                ->minifiedAjax()
                ->dom('Bfrtip')
                ->buttons([
                    Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                    Button::make('print'),
                ]);
        }

        /**
         * Get columns.
         *
         * @return array
         */
        protected function getColumns()
        {
                $edit = userCan('edit customer');
                $delete = userCan('delete customer');
        
                return [
                        Column::make('company_name')->addClass('text-center'),
                        Column::make('email')->addClass('text-center'),
                        Column::make('pic_1')->title('Contact Person 1')->addClass('text-center'),
                        Column::make('total_voucher')->addClass('text-right'),
                        Column::make('depot_name')->addClass('text-left'),
                        Column::make('edit')->title('')->visible($edit ?? false)->orderable(false)->exportable(false)->printable(false)->addClass('text-center'),
                        Column::make('delete')->title('')->visible($delete ?? false)->orderable(false)->exportable(false)->printable(false)->addClass('text-center'),
                ];
        }

        /**
         * Get filename for export.
         *
         * @return string
         */
        protected function filename()
        {
                return 'Customer_' . date('YmdHis');
        }
}
