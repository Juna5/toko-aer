<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\BusinessIndustry;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BusinessIndustryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('active', function ($row) {
                return $row->active == 1 ? "Active" : "Inactive";
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.master.business_industry.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('procurement.master.business_industry.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view business industry') ? $edit : '') . (userCan('delete business industry') ? $delete : '');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\BusinessIndustry $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(BusinessIndustry $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('name'),
            // Column::make('description')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'BusinessIndustry_' . date('YmdHis');
    }
}
