<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\DebitNote;
use Modules\Finance\Entities\Payable;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class DebitNoteDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('amount', function ($row) {
                return amount_international_with_comma($row->amount);
            })
            ->editColumn('payable_id', function ($row) {
                return optional($row->payable)->code_number;
            })
            ->editColumn('supplier_id', function ($row) {
                return optional($row->supplier)->company_name;
            })
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee ? optional($row->employee)->name : '-';
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.debit_note.edit', $row->id) . '" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit" data-id="' . $row->id . '" >
                            <i class="fa fa-pencil-alt"></i>
                        </a>';
                $delete = '<a data-href="' . route('procurement.debit_note.destroy', $row->id) . '" style="margin-left: 10px" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash text-white"></i></a>';

                return (userCan('view debit note') ? $edit : '') . (userCan('delete debit note') ? $delete : '');
            })
            ->rawColumns(['action']);
    }

    public function query(DebitNote $model)
    {
        return $model->with('payable','payable.details')->whereHas('payable', function ($payable) {
            $payable->whereNull('deleted_at');
        })->whereHas('payable.details', function ($detail) {
            $detail->whereNull('deleted_at')->whereHas('purchaseInvoice', function($invoice){
                $invoice->whereNull('deleted_at');
            });
        })->whereNull('deleted_at')->get();
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('payable_id')->title('A/P Payment')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('employee_id')->title('Created By')->addClass('text-center'),
            Column::make('supplier_id')->title('Supplier')->addClass('text-center'),
            Column::make('amount')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'DebitNote_' . date('YmdHis');
    }
}
