<?php

namespace Modules\Procurement\DataTables;

use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\PurchaseOrder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PurchaseOrderDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->editColumn('code_number', function ($row) {
                $detail = '<a href="' . route('procurement.purchase-order.detail', $row->id) . '">' . $row->code_number . '</a>';
                return $detail;
            })
            ->editColumn('required_date', function ($row) {
                return format_d_month_y($row->required_date);
            })
            ->editColumn('department_id', function ($row) {
                return optional($row->department)->name;
            })
            ->editColumn('approver_to_go', function ($row) {
                return $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval);
            })
            ->editColumn('latest_approver', function ($row) {
                return $row->latestApprove ? optional($row->latestApprove)->name : '-';
            })
            ->editColumn('employee_id', function ($row) {
                return $row->employee ? optional($row->employee)->name : '-';
            })
            ->editColumn('status', function ($row) {
                $waiting = '<span class="badge badge-success">Waiting</span>';
                $rejected = '<span class="badge badge-danger">Rejected</span>';
                $approved = '<span class="badge badge-info">Approved</span>';

                if (empty($row->status)) {
                    return '-';
                } elseif ($row->status == 'waiting') {
                    return $waiting;
                } elseif ($row->status == 'approved') {
                    return $approved;
                } elseif ($row->status == 'rejected') {
                    return $rejected;
                }
            })
            ->editColumn('payment_status', function ($row) {
                if ($row->payment_status == 'Paid') {
                    return '<div class="badge badge-success">Paid</div>';
                } elseif ($row->payment_status == 'Paid Partially') {
                    return '<div class="badge badge-warning">Paid Partially</div>';
                } else {
                    return '<div class="badge badge-primary">Unpaid</div>';
                }
            })
            ->addColumn('action', function ($row) {
                $openDiv = '<div class="btn-group" role="group">
                    <button id="btnGroupDrop1" type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Menu">
                      <i class="fa fa-bars" aria-hidden="true"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">';
                $closeDiv = '</div></div>';

                $edit       = '<a href="' . route('procurement.purchase-order.edit', $row->id) . '"
                                style="margin-left: 10px" title="Edit" class="dropdown-item">
                                <i class="fa fa-edit" style="color: blue" aria-hidden="true"></i> Edit
                                </a>';

                $pdf        = '<a href="' . route('procurement.purchase-order.pdf', $row->id) . '"
                                style="margin-left: 10px" class="dropdown-item" title="View PDF">
                                <i class="fa fa-file-pdf" style="color: orange"></i> PDF
                            </a>';

                $delete     = '<a data-href="' . route('procurement.purchase-order.destroy', $row->id) . '" data-toggle="modal" data-target="#confirm-delete-modal"
                                style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Delete">
                                <i class="fa fa-trash" style="color: red" aria-hidden="true"></i> Delete
                            </a>';

                $resubmit  = '<a href="' . route('procurement.purchase-order.edit', [$row->id, 'resubmit' => true]) . '"
                                style="margin-left: 10px" class="dropdown-item" title="Resubmit">
                                <i class="fa fa-redo-alt" style="color: aqua" aria-hidden="true"></i> Resubmit
                            </a>';

                $view      = '<a href="' . route('procurement.purchase-order.detail-modal', $row->id) . '"
                                data-container="body" style="margin-left: 10px" class="dropdown-item" title="View">
                                <i class="fa fa-eye" style="color: brown" aria-hidden="true"></i> View
                            </a>';

                $attachment = '<a data-href="' . route('procurement.purchase-order.attachment', ['id' => $row->id]) . '" data-toggle="modal" data-target="#attachment" id="modal_attachment" data-id="'.$row->id.'" style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Add or view Attachment"><i class="fa fa-paperclip" style="color: green" aria-hidden="true"></i> Attachment
                                </a>';
                $accept = '<a data-href="' . route('procurement.purchase-order.accept', ['id' => $row->id]) . '" data-toggle="modal" data-target="#accept" id="modal_accept" data-id="'.$row->id.'" style="margin-left: 10px; cursor: pointer" class="dropdown-item" title="Add or view Attachment"><i class="fa fa-check" style="color: blue" aria-hidden="true"></i> Terima
                                </a>';

                if ($row->status == 'waiting') {
                    if ($row->latestApprove == null) {
                        return $openDiv . (userCan('edit purchase order') ? $edit : '') . (userCan('delete purchase order') ? $delete : '') . $pdf . $attachment . $closeDiv;
                    }else{
                        return $openDiv .  $pdf . $closeDiv;
                    }
                } elseif ($row->status == 'approved') {
                    return $openDiv . (userCan('edit purchase order') ? $view : '') . $pdf . $attachment . ($row->receiver_id ? '' : $accept) . $closeDiv;
                } elseif ($row->status == 'rejected') {
                    return $openDiv . (userCan('edit purchase order') ? $resubmit : '') . (userCan('delete purchase order') ? $delete : '') . $pdf . $attachment . $closeDiv;
                }
            })
            ->rawColumns(['action', 'status', 'code_number', 'payment_status', 'purchase_requisition_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PurchaseOrderDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PurchaseOrder $model)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        return $model
            ->with(['department', 'approveToGo', 'latestApprove'])
            ->where('employee_id', $employee->id)
            ->latest()
            ->get();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('purchaserequisitiondatatable-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->parameters([
                'order' => [
                    0,
                    'DESC'
                ]
            ])
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('code_number'),
            // Column::make('purchase_requisition_id')->addClass('text-center')->title('Transfered From'),
            // Column::make('title')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            // Column::make('department_id')->title('Department')->addClass('text-center'),
            Column::make('employee_id')->addClass('text-center')->title('Created By'),
            Column::make('approver_to_go')->addClass('text-center')->title('Next Approver')->addClass('text-center'),
            Column::make('latest_approver')->addClass('text-center')->title('Final Approver')->addClass('text-center'),
            Column::make('status')->addClass('text-center')->title('Approval Status'),
            Column::make('payment_status')->addClass('text-center'),
            Column::make('action')->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PurchaseOrder_' . date('YmdHis');
    }
}
