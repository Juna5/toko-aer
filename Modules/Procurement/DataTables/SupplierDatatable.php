<?php

namespace Modules\Procurement\DataTables;

use Modules\Procurement\Entities\Supplier;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class SupplierDatatable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('product_category_id', function ($row) {
                    return optional($row->productCategory)->name;
            })
            ->editColumn('payment_term_id', function ($row) {
                return optional($row->paymentTerm)->name;
            })
            ->editColumn('currency_id', function ($row) {
                return optional($row->currency)->name;
            })
            ->editColumn('deposit', function ($row) {
                return amount_international_with_comma($row->deposit);
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('procurement.master.supplier.edit', $row->id) . '" class="btn btn-info"><i class="fa fa-pencil-alt"></i></a>';
                $delete = '<a data-href="' . route('procurement.master.supplier.destroy', $row->id) . '" style="margin-left: 10px; color: white !important;" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash"></i></a>';
                return (userCan('view supplier') ? $edit : '') . (userCan('delete supplier') ? $delete : '');
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\UomDatatable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Supplier $model)
    {
        return $model->all();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('company_name')->addClass('text-center'),
            Column::make('name')->addClass('text-center'),
            // Column::make('roc')->title('ROC No.')->addClass('text-center'),
            Column::make('phone')->addClass('text-center'),
            Column::make('deposit')->addClass('text-right'),
            // Column::make('fax')->addClass('text-center'),
            // Column::make('email')->addClass('text-center'),
            // Column::make('product_category_id')->title('Product Category')->addClass('text-center'),
            // Column::make('currency_id')->title('Currency')->addClass('text-center'),
            // Column::make('payment_term_id')->title('payment Term')->addClass('text-center'),
            // Column::make('address')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Supplier_' . date('YmdHis');
    }
}
