<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('procurement.')->prefix('procurement')->middleware(['impersonate', 'auth'])->group(function () {
    Route::get('/', 'ProcurementController@view')->name('view');
    Route::get('open_purchases_report', 'ProcurementController@openPurchasesReport')->name('open_purchases_report');
    Route::get('overdue_purchases_report', 'ProcurementController@overduePurchasesReport')->name('overdue_purchases_report');
    Route::get('payments_sent_report', 'ProcurementController@paymentsSentReport')->name('payments_sent_report');

    Route::name('master.')->prefix('master')->group(function () {
        Route::get('index', 'ProcurementController@index')->name('index');

        Route::namespace('Master')->group(function () {
            Route::get('uom/export', 'UomController@export')->name('uom/export');
            Route::get('location/export', 'LocationController@export')->name('location/export');
            Route::get('product-category/export', 'ProductCategoryController@export')->name('product-category/export');
            Route::get('product/export', 'ProductController@export')->name('product/export');
            Route::get('price_category/export', 'PriceCategoryController@export')->name('price_category/export');
            Route::get('tax/export', 'TaxController@export')->name('tax/export');
            Route::get('payment_term/export', 'PaymentTermController@export')->name('payment_term/export');
            Route::get('payment_method/export', 'PaymentMethodController@export')->name('payment_method/export');
            Route::get('depot_master/export', 'WarehouseMasterController@export')->name('depot_master/export');
            Route::get('currency/export', 'CurrencyController@export')->name('currency/export');
            Route::get('vehicle/export', 'VehicleController@export')->name('vehicle/export');
            Route::get('business_role/export', 'BusinessRoleController@export')->name('business_role/export');
            Route::get('business_industry/export', 'BusinessIndustryController@export')->name('business_industry/export');
            Route::get('supplier/export', 'SupplierController@export')->name('supplier/export');
            Route::get('customer/export', 'CustomerController@export')->name('customer/export');
            Route::get('project/export', 'ProjectController@export')->name('project/export');
            Route::get('weight_class/export', 'WeightClassController@export')->name('weight_class.export');

            Route::resource('uom', 'UomController');
            Route::resource('customer_type', 'CustomerTypeController');
            Route::resource('location', 'LocationController');
            Route::resource('product-category', 'ProductCategoryController');
            Route::resource('supplier', 'SupplierController');
            Route::resource('weight_class', 'WeightClassController');
            Route::get('/showPurchaseInvoice/{id}', 'SupplierController@showPurchaseInvoice')->name('supplier.showPurchaseInvoice');
            Route::resource('product', 'ProductController');
            Route::post('product/subCategory', 'ProductController@subCategory')->name('product.sub-category');
            Route::get('/{id}/showSO', 'ProductController@showSO')->name('product.showSO');
            Route::resource('customer', 'CustomerController');
            Route::put('/{id}/customer', 'CustomerController@saveDocument')->name('customer.document');
            Route::put('/{id}/supplier', 'SupplierController@saveDocument')->name('supplier.document');
            Route::get('/{id}/showInvoice', 'CustomerController@showInvoice')->name('customer.showInvoice');
            Route::get('/{id}/showPr', 'ProductController@showPr')->name('product.showPr');
            Route::get('/{id}/showSo', 'ProductController@showSo')->name('product.showSo');
            Route::resource('price_category', 'PriceCategoryController');
            Route::resource('tax', 'TaxController');
            Route::resource('payment_term', 'PaymentTermController');
            Route::resource('depot_master', 'WarehouseMasterController');
            Route::resource('currency', 'CurrencyController');
            Route::resource('payment_method', 'PaymentMethodController');
            Route::resource('vehicle', 'VehicleController');
            Route::resource('product_sub_category', 'ProductSubCategoryController');
            Route::resource('product_price', 'ProductPriceController');
            Route::get('/{id}/duplicate', 'ProductPriceController@duplicate')->name('product_price.duplicate');
            Route::resource('business_role', 'BusinessRoleController');
            Route::resource('business_industry', 'BusinessIndustryController');
            Route::resource('project', 'ProjectController');
            Route::get('showSO/{id}', 'ProjectController@showSO')->name('project.showSO');
        });
    });

    Route::resource('report', 'ReportController');
    Route::resource('account-payable', 'AccountPayableController');
    Route::get('account-payable-report', 'AccountPayableController@report')->name('account-payable-report');
    Route::get('/account-payable-report-detail/{id}', 'AccountPayableController@reportDetail')->name('account-payable-report-detail');
    Route::resource('account_payable_aging', 'AccountPayableAgingController');

    Route::resource('purchase_invoice', 'PurchaseInvoiceController');
    Route::get('purchase_invoice_outstanding', 'PurchaseInvoiceController@outstanding')->name('purchase_invoice_outstanding');
    Route::get('purchase_invoice/pdf/{id}', 'PurchaseInvoiceController@pdf')->name('purchase_invoice.pdf');
    Route::get('purchase_invoice/payment-history/{id}', 'PurchaseInvoiceController@fetchPartial')->name('purchase_invoice.payment-history');
    Route::get('purchase_invoice/receivable_form/{id}', 'PurchaseInvoiceController@receiveForm')->name('purchase_invoice.receivable_form');
    Route::get('purchase_invoice_report', 'PurchaseInvoiceController@report')->name('purchase_invoice.report.index');
    Route::get('/showPurchaseReturn/{id}', 'PurchaseInvoiceController@showPurchaseReturn')->name('purchase_invoice.showPurchaseReturn');

    Route::resource('purchase_receipt', 'PurchaseReceiptController');
    Route::get('purchase_receipt_outstanding', 'PurchaseReceiptController@outstanding')->name('purchase_receipt_outstanding');
    Route::get('purchase_receipt/pdf/{id}', 'PurchaseReceiptController@pdf')->name('purchase_receipt.pdf');
    Route::get('purchase_receipt/payment-history/{id}', 'PurchaseReceiptController@fetchPartial')->name('purchase_receipt.payment-history');
    Route::get('purchase_receipt/receivable_form/{id}', 'PurchaseReceiptController@receiveForm')->name('purchase_receipt.receivable_form');
    Route::get('purchase_receipt_report', 'PurchaseReceiptController@report')->name('purchase_receipt.report.index');

    Route::resource('purchase-return', 'PurchaseReturnController');
    Route::get('/{id}/adjustment', 'PurchaseReturnController@adjustment')->name('purchase_return.showAdjustment');
    Route::post('adjustment', 'PurchaseReturnController@saveAdjustment')->name('purchase_return.adjustment');
    Route::get('purchase-return-report', 'PurchaseReturnController@report')->name('purchase-return-report');
    Route::get('/purchase-return-report-detail/{id}', 'PurchaseReturnController@reportDetail')->name('purchase-return-report-detail');

    Route::name('purchase_requisition.')->prefix('purchase_requisition')->namespace('PurchaseRequisition')->group(function () {
        Route::put('/{id}', 'PurchaseRequisitionController@update')->name('update');
        Route::get('view', 'ViewController')->name('view');
        Route::resource('/', 'PurchaseRequisitionController', ['except' => ['update', 'destroy', 'edit']]);
        Route::get('/{id}/detail', 'PurchaseRequisitionController@detail')->name('detail');
        Route::get('/{id}/report-detail', 'PurchaseRequisitionController@detailReport')->name('report-detail');
        Route::get('/{id}/edit', 'PurchaseRequisitionController@edit')->name('edit');
        Route::get('/{id}/show', 'PurchaseRequisitionController@show')->name('show');
        Route::delete('/{id}', 'PurchaseRequisitionController@destroy')->name('destroy');
        Route::post('attachment', 'PurchaseRequisitionController@saveAttachment')->name('attachment');

        Route::get('/department', 'PurchaseRequisitionController@department')->name('department');

        Route::get('/costCenter', 'PurchaseRequisitionController@costCenter')->name('costCenter');
        Route::get('/product', 'PurchaseRequisitionController@product')->name('product');
        Route::get('product-detail/{id}', 'PurchaseRequisitionController@productDetail')->name('product-detail');

        Route::get('/uom', 'PurchaseRequisitionController@uom')->name('uom');
        Route::get('/export', 'PurchaseRequisitionController@export')->name('export');

        Route::resource('approval', 'PurchaseRequisitionApprovalController');
        Route::get('report', 'PurchaseRequisitionController@report')->name('report');
        Route::get('/{id}/prDetail', 'PurchaseRequisitionController@getPrDetail')->name('prDetail');
        Route::get('/{id}/pdf', 'PurchaseRequisitionController@pdf')->name('pdf');
    });

    Route::name('purchase-order.')->prefix('purchase-order')->namespace('PurchaseOrder')->group(function () {
        Route::put('/{id}', 'PurchaseOrderController@update')->name('update');
        Route::get('view', 'ViewPurchaseOrderController')->name('view');
        Route::get('/{id}/edit', 'PurchaseOrderController@edit')->name('edit');

        Route::resource('/', 'PurchaseOrderController', ['except' => ['update', 'destroy', 'edit']]);
        Route::get('/export', 'PurchaseOrderController@export')->name('export');
        Route::get('/{id}/show', 'PurchaseOrderController@show')->name('show');
        Route::get('detail-modal/{id}', 'PurchaseOrderController@detailModal')->name('detail-modal');
        Route::post('attachment', 'PurchaseOrderController@saveAttachment')->name('attachment');
        Route::get('accept/{id}', 'PurchaseOrderController@accept')->name('accept');
        Route::post('saveAccept', 'PurchaseOrderController@saveAccept')->name('saveAccept');

        Route::get('/{id}/detail', 'PurchaseOrderController@detail')->name('detail');
        Route::get('/{id}/pdf', 'PurchaseOrderController@pdf')->name('pdf');
        Route::get('/{id}/edit', 'PurchaseOrderController@edit')->name('edit');
        Route::delete('/{id}', 'PurchaseOrderController@destroy')->name('destroy');
        Route::get('/get-po', 'PurchaseOrderController@purchaseOrder')->name('get-po');
        Route::get('/supplier', 'PurchaseOrderController@supplier')->name('supplier');
        Route::get('/supplier-detail', 'PurchaseOrderController@supplierDetail')->name('supplier-detail');


        Route::resource('approval', 'PurchaseOrderApprovalController');
        Route::get('report', 'PurchaseOrderController@report')->name('report');
        Route::get('/{id}/report-detail', 'PurchaseOrderController@detailReport')->name('report-detail');
    });

    Route::name('ap_credit_debit_note.')->prefix('ap_credit_debit_note')->group(function () {
        Route::resource('/', 'APCreditDebitNoteController');
        Route::get('/{id}/edit', 'APCreditDebitNoteController@edit')->name('edit');
        Route::put('/{id}', 'APCreditDebitNoteController@update')->name('update');
        Route::post('transaction/ap-credit-debit-note/payable', 'APCreditDebitNoteController@getPayable')->name('getPayable');
        Route::delete('/{id}', 'APCreditDebitNoteController@destroy')->name('destroy');
        Route::get('report', 'APCreditDebitNoteController@report')->name('report');
        Route::get('detail/{id}', 'APCreditDebitNoteController@reportDetail')->name('report-detail');
    });


    Route::name('debit_note.')->prefix('debit_note')->group(function () {
        Route::resource('/', 'DebitNoteController');
        Route::get('/{id}/edit', 'DebitNoteController@edit')->name('edit');
        Route::put('/{id}', 'DebitNoteController@update')->name('update');
        // Route::post('procurement/debit-note/payable', 'CreditNoteController@getPayable')->name('getPayable');
        Route::delete('/{id}', 'DebitNoteController@destroy')->name('destroy');
        Route::get('report', 'DebitNoteController@report')->name('report');
        Route::get('detail/{id}', 'DebitNoteController@reportDetail')->name('report-detail');
    });

    Route::resource('voucher', 'Voucher\\VoucherController');
    Route::get('template-voucher','Voucher\\VoucherController@templateVoucher')->name('voucher.template-voucher');

    Route::post('voucher-import','Voucher\\VoucherController@import')->name('voucher.import');
});
