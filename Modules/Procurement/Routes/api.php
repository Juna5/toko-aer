<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/procurement', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->name('api.')->group(function () {
    // Route::post('get-supplier', 'MasterController@getSupplier');
    // Route::post('get-purchase-invoice', 'MasterController@getPurchaseInvoice');
    // Route::get('all-supplier', 'MasterController@allSupplier');

    Route::get('procurement/purchase_requisition/{id}', 'PurchaseRequisitionController@edit')->name('procurement.purchase_requisition');
    Route::post('procurement/purchase_requisition/removeItem/{id}', 'PurchaseRequisitionController@removeItem')->name('procurement.purchase_requisition');

    Route::get('purchase-order', 'PurchaseOrderController@index');
    Route::get('purchase-order/supplier', 'PurchaseOrderController@supplier');
    Route::get('purchase-order/supplier-detail/{id}', 'PurchaseOrderController@supplierDetail')->name('purchase-order.supplier-detail');
    Route::get('purchase-order/department', 'PurchaseOrderController@department');
    Route::get('purchase-order/currency', 'PurchaseOrderController@currency');
    Route::get('purchase-order/project', 'PurchaseOrderController@project');
    Route::get('purchase-order/payment-method', 'PurchaseOrderController@paymentMethod');
    Route::get('purchase-order/payment-term', 'PurchaseOrderController@paymentTerm');
    Route::get('purchase-order/get-purchase-requisition/{id}', 'PurchaseOrderController@getPurchaseRequisition');
    Route::get('procurement/purchase_order/{id}', 'PurchaseOrderController@edit')->name('procurement.purchase_order');
    Route::post('procurement/purchase_order/removeItem/{id}', 'PurchaseOrderController@removeItem')->name('procurement.purchase_order');

    Route::post('procurement/product/sub-category', 'ProductController@subCategory')->name('procurement.product_sub_category');
    Route::get('procurement/product/product-detail/{id}', 'ProductController@productDetail')->name('procurement.product.product_detail');
    Route::get('procurement/product/product-price/{product_id}/{uom_id}', 'ProductController@productPrice')->name('procurement.product.product_price');
    Route::get('procurement/product/product-category', 'ProductController@productCategory')->name('procurement.product.product-category');
    Route::get('procurement/product/price-category', 'ProductController@priceCategory')->name('procurement.product.price-category');
    Route::get('payable-detail/{id}', 'DebitNoteController@getPayableDetail');

    Route::post('procurement/supplier', 'SupplierController@store')->name('procurement.supplier');
    Route::get('procurement/claim_voucher', 'ClaimVoucherController@index')->name('procurement.claim_voucher');

    Route::name('debit_note.')->prefix('debit_note')->group(function () {
        Route::post('/debit-note/payable', 'DebitNoteController@getPayable')->name('getPayable');
        Route::post('/debit-note/supplier', 'DebitNoteController@getSupplier')->name('getSupplier');
    });

    Route::get('purchase-order/{purchase_order}', 'PurchaseInvoiceController@getPurchaseOrder');

    Route::get('get-purchase-order', 'PurchaseOrderController@purchaseOrder');
    Route::get('detail-purchase-order', 'PurchaseOrderController@purchaseOrderDetail');

    Route::get('employee', 'EmployeeController@index');
});
