<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\BusinessIndustry;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BusinessIndustryExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.business_industry.export', [
            'rows' => BusinessIndustry::all()
        ]);
    }
}