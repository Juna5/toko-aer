<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\PaymentMethod;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PaymentMethodExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.payment-method.export', [
            'rows' => PaymentMethod::all()
        ]);
    }
}
