<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\ProductCategory;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProductCategoryExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.product-category.export', [
            'rows' => ProductCategory::all()
        ]);
    }
}
