<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\Tax;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class TaxExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.tax.export', [
            'rows' => Tax::all()
        ]);
    }
}
