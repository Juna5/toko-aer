<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\Customer;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CustomerExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.customer.export', [
            'rows' => Customer::all()
        ]);
    }
}