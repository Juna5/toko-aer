<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\PriceCategory;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PriceCategoryExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.price-category.export', [
            'rows' => PriceCategory::all()
        ]);
    }
}
