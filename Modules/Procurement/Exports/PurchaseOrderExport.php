<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\PurchaseOrder;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PurchaseOrderExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::purchase_order.entry.export', [
            'rows' => PurchaseOrder::all()
        ]);
    }
}
