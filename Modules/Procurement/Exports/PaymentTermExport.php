<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\PaymentTerm;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PaymentTermExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.payment_term.export', [
            'rows' => PaymentTerm::all()
        ]);
    }
}
