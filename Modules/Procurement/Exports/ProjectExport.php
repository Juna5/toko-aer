<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\Project;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProjectExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.project.export', [
            'rows' => Project::all()
        ]);
    }
}
