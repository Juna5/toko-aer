<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\Currency;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class CurrencyExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.currency.export', [
            'rows' => Currency::all()
        ]);
    }
}