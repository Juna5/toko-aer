<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\BusinessRole;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BusinessRoleExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.business_role.export', [
            'rows' => BusinessRole::all()
        ]);
    }
}