<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ProductExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.product.export', [
            'rows' => Product::all()
        ]);
    }
}
