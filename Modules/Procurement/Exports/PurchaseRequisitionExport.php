<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\PurchaseRequisition;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class PurchaseRequisitionExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::purchase_requisition.entry.export', [
            'rows' => PurchaseRequisition::all()
        ]);
    }
}
