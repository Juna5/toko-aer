<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\Supplier;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SupplierExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.supplier.export', [
            'rows' => Supplier::all()
        ]);
    }
}