<?php

namespace Modules\Procurement\Exports;

use Modules\Procurement\Entities\WarehouseMaster;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class WarehouseMasterExport implements FromView, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        return view('procurement::master.warehouse_master.export', [
            'rows' => WarehouseMaster::all()
        ]);
    }
}