<?php

use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\Invoice;
use Modules\Procurement\Entities\PurchaseInvoice;
use Modules\Procurement\Entities\Product;
use Modules\Stock\Entities\StockAdjustment;
use Modules\Stock\Entities\StockAdjustmentDetail;

function getAgingDaysPayable($id, $date, $day){
    $query = PurchaseInvoice::findOrFail($id);
    $date_now = date('Y-m-d');

    $date1 = new DateTime($date_now);
    $date2 = new DateTime($date);
    $interval = $date2->diff($date1);
    $days = $interval->format('%a');

    if($date >  $date_now){
        $total_day_amount = '0';
    }else{
        if($days > 0 && $days < 30){
        $day_type = '30_days';
        }elseif($days > 30 && $days < 60){
            $day_type = '60_days';
        }elseif($days > 60  && $days < 90){
            $day_type = '90_days';
        }elseif($days > 90){
            $day_type = 'unlimited_days';
        }else{
            $day_type = '0';
        }

        if($day_type == $day){
            $total_day_amount = $query->total;
        }else{
            $total_day_amount = 0;
        }
    }

    return $total_day_amount;
}

function getstockQtyAdj($product_id, $purchase_return_id){
    $query = StockAdjustment::where('purchase_return_id', $purchase_return_id)->get();
     $checkProduct = Product::where('name','LIKE','%'. $product_id .'%')->first();
    $detail = StockAdjustmentDetail::whereIn('stock_adjustment_id', $query->pluck('id'))->where('product_id', $checkProduct->id)->sum('qty');
    return $detail;
    // $stock = StockAdjustmentDetail::where('')
}
