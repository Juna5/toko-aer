<?php

namespace Modules\Procurement\Http\Controllers;

use App\Services\InvoiceStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\DataTables\PurchaseReturnDatatable;
use Modules\Procurement\Entities\PurchaseReturn;
use Modules\Procurement\Entities\PurchaseReturnDetail;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Procurement\Entities\Product;
use Modules\Stock\Entities\Stock;
use Modules\HR\Entities\Employee;
use Modules\Stock\Entities\StockAdjustment;
use Modules\Stock\Entities\StockAdjustmentDetail;
use Modules\Procurement\Entities\PurchaseInvoice;
use Modules\Procurement\Entities\PurchaseInvoiceDetail;
use Modules\Procurement\Entities\PurchaseInvoiceDetailItem;

class PurchaseReturnController extends Controller
{
    public function index(PurchaseReturnDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase return');
        return $datatable->render('procurement::purchase_return.index');
    }

    public function create()
    {
        $this->hasPermissionTo('view purchase return');
        return view('procurement::purchase_return.create');
    }

    public function store(Request $request)
    {
        $this->hasPermissionTo('add purchase return');
        DB::beginTransaction();
        $request->validate([
            'supplier_id' => 'required|integer',
            'purchase_invoice_id' => 'required|integer',
            'date' => 'required|date',
        ]);

        try {
            $purchaseInvoice = PurchaseInvoice::find($request->purchase_invoice_id);
            if ($request->grand_total > $purchaseInvoice->outstanding) {
                return response()->json([
                    'status' => 422,
                    'message' => 'Return Qty must be less than Purchase Qty.'
                ]);
            }

            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('purchase return');
            $code_number = generate_code_sequence('J/PRT', $sequence_number, 3);
            $employee = Employee::where('user_id', auth()->user()->id)->first();

            $purchaseReturn = PurchaseReturn::create([
                'code_number' => $request->code_number ?? $code_number,
                'supplier_id' => $request->supplier_id,
                'purchase_invoice_id' => $request->purchase_invoice_id,
                'date' => $request->date,
                'description' => $request->remarks,
                'grand_total' => $request->grand_total,
                'subtotal_exclude_discount' => $request->subtotal_exclude_discount,
                'discount_all_item' => $request->discount_all_item,
                'subtotal_include_discount' => $request->subtotal_include_discount,
                'discount_gst' => $request->discount_gst,
                'amount_gst' => $request->amount_gst,
                'employee_id' => $employee->id,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }

        try {
            $details = collect(request('details'));
            foreach ($details as $detail) {
                foreach ($purchaseInvoice->purchaseInvoiceDetails as $invoiceDetail) {
                    if ($detail['qty'] > $invoiceDetail->returnable_qty) {
                        return response()->json([
                            'status' => 422,
                            'message' => 'Returned Qty Cannot Bigger Than Current Qty.'
                        ]);
                    }
                }
                PurchaseReturnDetail::create([
                    'purchase_return_id' => $purchaseReturn->id,
                    'purchase_invoice_detail_id' => $detail['id'],
                    'item' => $detail['item'],
                    'uom' => $detail['uom'],
                    'qty' => $detail['qty'],
                    'price' => $detail['price'],
                    'discount_amount' => $detail['discount_amount'],
                    'sub_total' => $detail['sub_total']
                ]);

                # update purchase invoice returnable qty.
                $updateQty = new \App\Services\PurchaseReturn;
                $updateQty->updateReturnableQty($detail['id'], $detail['qty']);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }

        try {
            # sum total for get status invoice
            $invoiceStatus = new InvoiceStatus;
            $invoiceStatus->changeStatusInvoice($purchaseReturn, $purchaseReturn->purchase_invoice_id, PurchaseInvoice::class);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        DB::commit();

        return response()->json([
            'status' => 200,
            'message' => 'Data has been saved successfully'
        ]);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit purchase return');
        $return = PurchaseReturn::with('details', 'supplier', 'purchaseInvoice', 'details.purchaseInvoiceDetail')
            ->whereHas('details', function ($query) {
                $query->orderBy('created_at', 'ASC');
            })
            ->findOrFail($id);
        return view('procurement::purchase_return.edit', compact('return'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit purchase return');
        DB::beginTransaction();
        $request->validate([
            'supplier_id' => 'required|integer',
            'purchase_invoice_id' => 'required|integer',
            'date' => 'required|date',
        ]);

        try {
            $purchaseInvoice = PurchaseInvoice::find($request->purchase_invoice_id);
            if ($request->grand_total > $purchaseInvoice->outstanding) {
                return response()->json([
                    'status' => 422,
                    'message' => 'Return Qty must be less than Purchase Qty.'
                ]);
            }

            $purchaseReturn = PurchaseReturn::findOrFail($id);
            $purchaseReturn->update([
                'date' => $request->date,
                'description' => $request->remarks,
                'grand_total' => $request->grand_total,
                'subtotal_exclude_discount' => $request->subtotal_exclude_discount,
                'discount_all_item' => $request->discount_all_item,
                'subtotal_include_discount' => $request->subtotal_include_discount,
                'discount_gst' => $request->discount_gst,
                'amount_gst' => $request->amount_gst
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }

        try {
            $details = collect(request('details'));
            foreach ($details as $detail) {
                foreach ($purchaseInvoice->purchaseInvoiceDetails as $invoiceDetail) {
                    if ($detail['qty'] > $invoiceDetail->returnable_qty) {
                        return response()->json([
                            'status' => 422,
                            'message' => 'Returned Qty Cannot Bigger Than Current Qty.'
                        ]);
                    }
                }
                $purchaseInvoiceDetail = PurchaseInvoiceDetail::find($detail['purchase_invoice_detail_id']);

                $returnDetail = PurchaseReturnDetail::find($detail['id']);
                $returnDetail->update([
                    'qty' => $detail['qty'],
                    'price' => $detail['price'],
                    'discount_amount' => $detail['discount_amount'],
                    'sub_total' => $detail['sub_total']
                ]);

                # update purchase invoice returnable qty.
                $result = PurchaseReturnDetail::where('purchase_invoice_detail_id', $detail['purchase_invoice_detail_id'])->get();
                $purchaseInvoiceDetail->update([
                    'returnable_qty' =>  $detail['invoice_qty'] - $result->sum('qty')
                ]);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }

        try {
            # sum total for get status invoice
            $invoiceStatus = new InvoiceStatus;
            $invoiceStatus->changeStatusInvoice($purchaseReturn, $purchaseReturn->purchase_invoice_id, PurchaseInvoice::class);
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        DB::commit();

        return response()->json([
            'status' => 200,
            'message' => 'Data has been updated successfully'
        ]);
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete purchase return');
        DB::beginTransaction();
        try {
            $return = PurchaseReturn::find($id);

            # sum total for get and change status invoice and return back returnable qty on purchase invoice detail
            foreach ($return->details as $detail) {
                $purchaseInvoiceDetail = PurchaseInvoiceDetail::find($detail->purchase_invoice_detail_id);
                $purchaseInvoiceDetail->update([
                    'returnable_qty' => $purchaseInvoiceDetail->returnable_qty + $detail->qty
                ]);
            }
            $puchaseInvoice = PurchaseInvoice::find($return->purchase_invoice_id);
            $puchaseInvoice->update([
                'outstanding' => $puchaseInvoice->outstanding + $return->grand_total
            ]);
            $puchaseInvoice->outstanding == $puchaseInvoice->total ?
                $puchaseInvoice->update(['payment_status' => 'Unpaid']) :
                $puchaseInvoice->update(['payment_status' => 'Paid Partially']);

            $return->delete();
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
        DB::commit();

        noty()->danger('Cool!', 'Your data has been deleted');
        return back();
    }

    public function adjustment($id)
    {
        $row = PurchaseReturn::with('details')->find($id);
        $prDetail = PurchaseReturnDetail::where('purchase_return_id', $id)->get();
        $warehouse = WarehouseMaster::whereNull('deleted_at')->get();

        return view('procurement::purchase_return.stock_adjustment.adjustment', compact('row', 'prDetail', 'warehouse'))->withRow($row)->withPrDetail($prDetail)->withWarehouse($warehouse);
    }


    public function saveAdjustment()
    {
        $row = PurchaseReturn::find(request('id_purchase_return'));
        $prDetail = PurchaseReturnDetail::where('purchase_return_id', request('id_purchase_return'))->get();
        $warehouse = WarehouseMaster::whereNull('deleted_at')->get();
        $date = request()->date;
        $description = request()->description;
        $stock_qty = request()->stock_qty;
        $item = request()->item;

        $checked = request()->checked ?? [];

        $sequence = new \App\Services\SequenceNumber;
        $sequence_number = $sequence->runSequenceNumber('stock adjustment');
        $code_number = generate_code_sequence('SA', $sequence_number, 3);

        $stockAdjustment = StockAdjustment::create(
            [
                'date' => $date,
                'code_number' => $code_number,
                'description' => $description,
                'purchase_return_id' => request()->purchase_return_id
            ]
        );
        foreach (array_filter($checked) as $key => $value) {
            $checkProduct = Product::where('name', 'LIKE', '%' . $item[$key] . '%')->first();

            $data['qty'] = request('stock_qty_' . $value);
            $data['warehouse_id'] = request('warehouse_id_' . $value);
            $data['type'] = 'IN';

            $detail = PurchaseReturnDetail::where('purchase_return_id', request('purchase_return_id'))->where('item', 'LIKE', '%' . request('item_' . $value) . '%')->first();

            if ($data['qty'] > $detail->qty) {
                noty()->danger('Oops!', 'Your Stock qty cannot be greater than qty');
                return redirect()->back();
            }

            $checkStock = Stock::where('product_id', $checkProduct->id)->where('warehouse_id', $data['warehouse_id'])->first();
            $checkStock->qty = $checkStock->qty + $data['qty'];

            if ($stockAdjustment) {
                StockAdjustmentDetail::create(
                    [
                        'stock_adjustment_id' => $stockAdjustment->id,
                        'product_id'          => $checkProduct->id,
                        'warehouse_id'        => $data['warehouse_id'],
                        'type'                => $data['type'],
                        'qty'                 => $data['qty'],
                    ]
                );
            }
        }

        if ($stockAdjustment) {

            noty()->success('Yeay!', 'Your data have been saved successfully');
        }

        return redirect()->route('procurement.purchase-return.index', compact('row'));
    }
    public function report()
    {
        $row = PurchaseReturn::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $row->whereBetween('date', [$fromDate, $untilDate])->get();
        }

        return view('procurement::purchase_return.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $row = PurchaseReturn::with('employee')->findOrFail($id);
        $purchaseReturnDetail = PurchaseReturnDetail::where('purchase_return_id', $row->id)->get();

        return view('procurement::purchase_return.report.detail', compact('row', 'purchaseReturnDetail'));
    }
}
