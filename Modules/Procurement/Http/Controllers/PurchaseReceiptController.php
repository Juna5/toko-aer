<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Modules\Procurement\DataTables\PurchaseReceiptDatatable;
use Modules\Procurement\DataTables\PurchaseReceiptOutstandingDatatable;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Entities\PurchaseReceipt;
use Modules\Procurement\Entities\PurchaseReceiptDetail;
use Modules\Procurement\Entities\PurchaseReceiptDetailItem;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\JournalTotal;
use Modules\Finance\Entities\Receivable;
use Modules\Finance\Entities\ReceivableDetail;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;
use Modules\HR\Entities\Employee;
use Barryvdh\DomPDF\Facade as PDF;
use NumberFormatter;
use NumberToWords\NumberToWords;
use Yajra\Datatables\Datatables;

class PurchaseReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(PurchaseReceiptDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase receipt');
        return $datatable->render('procurement::purchase_receipt.index');
    }

    public function outstanding(PurchaseReceiptOutstandingDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase receipt');
        return $datatable->render('procurement::purchase_receipt.index_outstanding');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add purchase receipt');
        
        $purchaseReceipt = PurchaseReceipt::whereNotNull('purchase_order_id')->pluck('purchase_order_id');

        $purchaseOrder   = PurchaseOrder::with('employee', 'supplier', 'items')
            ->approved()
            ->whereNotIn('id', $purchaseReceipt)
            ->get();

        $paymentMethod   = PaymentMethod::where('active', true)->get();

        $warehouse = WarehouseMaster::whereNull('deleted_at')->get();

        return view('procurement::purchase_receipt.create', compact('purchaseReceipt', 'purchaseOrder', 'paymentMethod', 'warehouse'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $this->hasPermissionTo('add purchase receipt');

        DB::beginTransaction();
        $this->validate($request, [
            'invoice_date'      => 'required|date',
            'invoice_due_date'  => 'required|date|after:invoice_date',
            'payment_method_id' => 'required|integer',
            'purchase_order_id' => Rule::requiredIf($request->purchase_order_id != null),
        ]);

        $purchaseOrderId = $request->purchase_order_id;
        $total = 0;

        if (!empty($purchaseOrderId)){
            $totalAmountSales = PurchaseOrder::find($purchaseOrderId);
            $total = $totalAmountSales->total;

            $exist = PurchaseReceipt::where('purchase_order_id', $purchaseOrderId)->first();
            if ($exist) { flash('Data already exist')->error(); return  back();}
        }

        try {
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('invoice');
            $code_number = generate_code_sequence('INR', $sequence_number, 3);
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $purchaseReceipt = PurchaseReceipt::create([
                'invoice_number' => !empty($request->invoice_number) ? $request->invoice_number : $code_number,
                'invoice_due_date' => $request->invoice_due_date,
                'invoice_date' => $request->invoice_date,
                'payment_method_id' => $request->payment_method_id,
                'purchase_order_id' => !empty($purchaseOrderId) ? $purchaseOrderId : null,
                'payment_status' => 'Unpaid',
                'total' => $total,
                'supplier_name' => $request->supplier_name,
                'outstanding' => $total,
                'description' => $request->description,
                'employee_id' =>  $employee->id,
            ]);
        } catch(\Exception $e){
            DB::rollback();
            flash($e->getMessage())->error();
            return back();
        }

        try {
            if (! is_null($purchaseOrderId)){
                $purchaseOrder = PurchaseOrder::find($purchaseOrderId);
                $purchaseOrderDetail = PurchaseOrderItem::where('purchase_order_id', $purchaseReceipt->purchase_order_id)->get();
                $gst = ($purchaseReceipt->purchaseOrder->tax->rate / 100) * $purchaseReceipt->purchaseOrder->sub_total;
                $subTotal = 0;

                foreach ($purchaseOrderDetail as $row){
                    $subTotal += $row->sub_total;
                    PurchaseReceiptDetail::create([
                        'invoice_id' => $purchaseReceipt->id,
                        'item' => optional($row->product)->name,
                        'uom' => optional($row->uom)->code,
                        'qty' => $row->qty,
                        'price' => $row->price,
                        'discount_amount' => $row->discount_rate,
                        'sub_total' => $row->sub_total,
                    ]);
                }

                $discountAllItem = $purchaseOrder->discount_all_item;
                $subTotal2 = $subTotal - $discountAllItem;
                PurchaseReceiptDetailItem::create([
                    'invoice_id' => $purchaseReceipt->id,
                    'sub_total_exclude_disc' => optional($purchaseReceipt->purchaseOrder)->get_total,
                    'discount_all_item' => optional($purchaseReceipt->purchaseOrder)->discount_all_item,
                    'subtotal_detail' => $subTotal2,
                    'gst' => $purchaseReceipt->purchaseOrder->tax->rate,
                    'amount_gst' => $gst,
                    'total' => $purchaseReceipt->total,
                ]);
            }
        } catch(\Exception $e){
            DB::rollback();
            flash($e->getMessage())->error();
            return back();
        }

        try {
            // dd($request->all());
            $qty = $request->qty;

            $warehouse = $request->warehouse;
            $confirmQty = $request->confirmQty;
            $poItemId = $request->po_item_id;
            foreach($request->product_id as $key => $row) {
                if($confirmQty[$key] > $qty[$key] ){
                    noty()->danger('Oops','confirmQty cannot be greater than qty');
                    return back();
                }
            }

            foreach($request->product_id as $keys => $rows){
               
                $poItem = PurchaseOrderItem::findOrFail($poItemId[$key]);

                $stock = Stock::where('product_id', $rows)->where('warehouse_id', $warehouse[$keys])->first();
                
                if($stock){
                    $stock->qty = $stock->qty - $poItem->stock_qty;
                    $stock->save();

                    $poItem->stock_qty = $confirmQty[$keys];
                    $poItem->warehouse_id = $warehouse[$keys];
                    $poItem->save();

                    $stocks = Stock::where('product_id', $rows)->where('warehouse_id', $warehouse[$keys])->first();
                    $stocks->qty = $stocks->qty + $confirmQty[$keys];
                    $stocks->save();
                    $qtyTotal = getTotal($rows, $warehouse[$keys]);

                    $stockTransaction = StockTransaction::where('purchase_order_id',$poItem->purchase_order_id)->where('product_id',$rows)->where('warehouse_id', $warehouse[$keys])->first();
                    if($stockTransaction){
                       $stockTransaction->warehouse_id = $warehouse[$keys];
                       $stockTransaction->date = \Carbon\Carbon::now();
                       $stockTransaction->type = 'IN';
                       $stockTransaction->qty = $confirmQty[$keys];
                       $stockTransaction->status = true;
                       $stockTransaction->description = 'Stock In';
                       $stockTransaction->stock_id = $stock->id;
                       $stockTransaction->in_hand = $qtyTotal + $confirmQty[$keys];
                       $stockTransaction->save();

                   }else{
                     

                     $stockTransaction = new StockTransaction();
                     $stockTransaction->product_id = $rows;
                     $stockTransaction->warehouse_id = $warehouse[$keys];
                     $stockTransaction->purchase_order_id = $poItem->purchase_order_id;
                     $stockTransaction->date = \Carbon\Carbon::now();
                     $stockTransaction->type = 'IN';
                     $stockTransaction->qty = $confirmQty[$keys];
                     $stockTransaction->status = true;
                     $stockTransaction->description = 'Stock In';
                     $stockTransaction->stock_id = $stock->id;
                     $stockTransaction->in_hand = $qtyTotal + $confirmQty[$keys];
                     $stockTransaction->save();      
                 }



                 }else{
                    $stock = new Stock();
                    $stock->product_id = $rows;
                    $stock->warehouse_id = $warehouse[$keys];
                    $stock->qty = $confirmQty[$keys];
                    $stock->status = true;
                    $stock->save();

                    $qtyTotal = getTotal($poItem->product_id, $warehouse[$keys]);

                    $stockTransaction = StockTransaction::where('purchase_order_id', $poItem->purchase_order_id)->where('product_id',$rows)->where('warehouse_id', $warehouse[$keys])->first();
                    if($stockTransaction){
                       $stockTransaction->warehouse_id = $warehouse[$keys];
                       $stockTransaction->date = \Carbon\Carbon::now();
                       $stockTransaction->type = 'IN';
                       $stockTransaction->qty = $confirmQty[$keys];
                       $stockTransaction->status = true;
                       $stockTransaction->description = 'Stock In';
                       $stockTransaction->stock_id = $stock->id;
                       $stockTransaction->in_hand = $qtyTotal + $confirmQty[$keys];
                       $stockTransaction->save();
                       }else{

                         $stockTransaction = new StockTransaction();
                         $stockTransaction->product_id = $rows;
                         $stockTransaction->warehouse_id = $warehouse[$keys];
                         $stockTransaction->purchase_order_id = $poItem->purchase_order_id;
                         $stockTransaction->date = \Carbon\Carbon::now();
                         $stockTransaction->type = 'IN';
                         $stockTransaction->qty = $confirmQty[$keys];
                         $stockTransaction->status = true;
                         $stockTransaction->description = 'Stock In';
                         $stockTransaction->stock_id = $stock->id;
                         $stockTransaction->in_hand = $qtyTotal + $confirmQty[$keys];
                         $stockTransaction->save();      
                     }
                }
            }
            // dd($stock, $stockTransaction);

        } catch(\Exception $e){
            DB::rollback();
            flash($e->getMessage())->error();
            return back();
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.purchase_receipt.index');

    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $this->hasPermissionTo('view purchase receipt');
        
        $purchaseReceipt = PurchaseReceipt::with('purchaseOrder', 'paymentMethod')
            ->find($id);
        $purchaseReceiptDetails = PurchaseReceiptDetail::where('invoice_id', $purchaseReceipt->id)->get();
        $purchaseReceiptItem = PurchaseReceiptDetailItem::where('invoice_id', $purchaseReceipt->id)->first();

        return view('procurement::purchase_receipt.show', compact('purchaseReceipt', 'purchaseReceiptDetails', 'purchaseReceiptItem'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete purchase receipt');
        DB::beginTransaction();
        try{
            $purchaseReceipt = PurchaseReceipt::find($id);
            $purchaseReceipt->purchaseReceiptDetails()->delete();
            $purchaseReceipt->delete();


             if($purchaseReceipt){
                $item = PurchaseOrderItem::where('purchase_order_id', $purchaseReceipt->purchase_order_id)->get();
                foreach ($item as $key => $value) {
                    $stock = Stock::where('product_id', $value->product_id)->where('warehouse_id', $value->warehouse_id)->first();
                    if($stock){
                        $stock->qty = (int)$stock->qty - (int)$value->stock_qty;
                        $stock->save();

                        $stockTransaction = StockTransaction::where('purchase_order_id',$purchaseReceipt->purchase_order_id)->where('product_id',$value->product_id)->where('warehouse_id', $value->warehouse_id)->get();
                        $qtyTotal = getTotal($value->product_id, $value->warehouse_id);

                        if($stockTransaction){
                            foreach ($stockTransaction as $key => $trans) {
                                $trans->delete();
                            }
                        }
                    }
                    $value->stock_qty = null;
                    $value->warehouse_id = null;
                    $value->save();

                }
            }
        } catch (\Exception $error){
            DB::rollBack();
            noty()->danger('Cool!', 'Ops... try again.');
            return back();
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function pdf($id)
    {
        $this->hasPermissionTo('view purchase receipt');

        $purchaseReceipt = PurchaseReceipt::findOrFail($id);
        $purchaseReceiptDetail = PurchaseReceiptDetail::where('invoice_id', $purchaseReceipt->id)->first();
        $purchaseReceiptItem = PurchaseReceiptDetailItem::where('invoice_id', $purchaseReceipt->id)->first();

        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');
        $str = $numberTransformer->toWords($purchaseReceiptItem->total);
        $terbilang = 'Malaysia Ringgits : ' . strtoupper($str) . ' ONLY';

        # Load and set paper
        $pdf = PDF::loadView('procurement::purchase_receipt.pdf', compact('purchaseReceipt', 'purchaseReceiptDetail', 'purchaseReceiptItem', 'terbilang', 'numberToWords'));
        $pdf->setPaper('A4', 'potrait');

        return $pdf->stream("purchase_receipt.pdf");
    }

    public function fetchPartial($id)
    {
        $purchaseReceipt = PurchaseReceipt::with('purchaseOrder', 'paymentMethod')
            ->find($id);
        $payments = ReceivableDetail::where('invoice_id', $purchaseReceipt->id)->get();

        return view('procurement::purchase_receipt.partials.payment_history', compact('purchaseReceipt', 'payments'));
    }

    public function receiveForm($id)
    {
        $receive = PurchaseReceipt::with('purchaseOrder.supplier')->find($id);
        $chartOfAccount = ChartOfAccount::where('level', 3)->get();
        $poId = $receive->purchaseOrder ? $receive->purchaseOrder->supplier->id : $receive->deliveryOrder->customer->id;

        return view('procurement::purchase_receipt.receivable_form', compact('receive', 'chartOfAccount', 'poId'));
    }

    public function report()
    {
        $purchaseReceipt = PurchaseReceipt::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $purchaseReceipt->whereBetween('invoice_date', [$fromDate, $untilDate])->orderBy('invoice_date', 'desc')->paginate(10);
        }

        return view('procurement::purchase_receipt.report.index', compact('results'));
    }
}
