<?php

namespace Modules\Procurement\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\DataTables\DebitNoteDatatable;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\Payable;
use Modules\HR\Entities\Employee;
use Modules\Finance\Entities\JournalTotal;
use Modules\Procurement\Entities\DebitNote;
use Modules\Procurement\Entities\Supplier;
use Modules\Procurement\Http\Requests\DebitNoteRequest;

class DebitNoteController extends Controller
{
    public function index(DebitNoteDatatable $datatable)
    {
        $this->hasPermissionTo('view debit note');

        return $datatable->render('procurement::debit_note.index');
    }

    public function create()
    {
        $this->hasPermissionTo('view debit note');

        $banks = ChartOfAccount::with('chartOfAccountType')->where('level', 3)->get();
        $charts = ChartOfAccount::with('chartOfAccountType')->where('level', 3)->get();
        $suppliers = Supplier::all();

        return view('procurement::debit_note.create', compact('banks', 'charts','suppliers'));
    }

    public function store(DebitNoteRequest $request)
    {
        $this->hasPermissionTo('add debit note');

        DB::beginTransaction();
        try {
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('debit note');
            $code_number = generate_code_sequence('J/DN', $sequence_number, 3);

            $journal = Journal::create([
                'journal_number' => $code_number,
                'journal_date' => $request->date,
                'description' => 'Debit Note' .' | '. $request->description,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            JournalItem::create([
                'journal_id' => $journal->id,
                'chart_of_account_id' => $request->bank,
                'description' => $request->description,
                'debit' => $request->amount,
                'credit' => 0,
            ]);

            JournalItem::create([
                'journal_id' => $journal->id,
                'chart_of_account_id' => $request->charge_of_account_id,
                'description' => $request->description,
                'debit' => 0,
                'credit' => $request->amount,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            JournalTotal::create([
                'journal_id' => $journal->id,
                'total_credit' => $request->amount,
                'total_debit' => $request->amount,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $debit_note = new DebitNote();
            $debit_note->charge_of_account_bank_id = $request->bank;
            $debit_note->charge_of_account_id = $request->charge_of_account_id;
            $debit_note->journal_id = $journal->id;
            $debit_note->supplier_id = $request->supplier_id;
            $debit_note->payable_id = $request->payable_id;
            $debit_note->date = $request->date;
            $debit_note->amount = $request->amount;
            $debit_note->description = $request->description;
            $debit_note->employee_id = $employee->id;
            $debit_note->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.debit_note.index');
    }

    public function show($id)
    {
        return view('procurement::show');
    }

    public function edit(DebitNote $debitNote, $id)
    {
        $this->hasPermissionTo('edit debit note');

        $banks = ChartOfAccount::with('chartOfAccountType')->where('level', 3)->get();
        $charts = ChartOfAccount::with('chartOfAccountType')->where('level', 3)->get();
        $suppliers = Supplier::all();
        $debit_note = DebitNote::findOrFail($id);
        $payables = Payable::all();
        $payableDetail = Payable::with('journal', 'supplier', 'details', 'chartOfAccount','details.invoice')->whereHas('details.purchaseInvoice', function($invoice){
            $invoice->whereNull('deleted_at');
        })->find($debit_note->payable_id);
        if(!$payableDetail == null){
            $grand_total = $payableDetail->details->sum('amount');
        }else{
            $grand_total = 0;

        }

        return view('procurement::debit_note.edit', compact('banks', 'charts', 'debit_note','payables','payableDetail','grand_total','suppliers'));
    }

    public function update(DebitNoteRequest $request, $id)
    {
        $this->hasPermissionTo('edit debit note');

        DB::beginTransaction();
        try {
            $debit_note = DebitNote::findOrFail($id);
            $debit_note->charge_of_account_bank_id = $request->bank;
            $debit_note->charge_of_account_id = $request->charge_of_account_id;
            $debit_note->supplier_id = $request->supplier_id;
            $debit_note->payable_id = $request->payable_id;
            $debit_note->date = $request->date;
            $debit_note->amount = $request->amount;
            $debit_note->description = $request->description;
            $debit_note->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $journal = Journal::find($debit_note->journal_id);
            $journal->journal_date = $request->date;
            $journal->description = 'Debit Note' .' | '. $request->description;
            $journal->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $journalItems = JournalItem::where('journal_id', $journal->id)->get();
            $journalItems->map(function ($item, $key) use ($request) {
                if ($item->debit != 0 && $item->credit == 0){
                    $item = JournalItem::find($item->id);
                    $item->chart_of_account_id = in_array($item->chart_of_account_id, [8,9]) ? $request->bank : $request->charge_of_account_id;
                    $item->description = $request->description;
                    $item->debit = $request->amount;
                    $item->save();
                } else if($item->debit == 0 && $item->credit != 0){
                    $item = JournalItem::find($item->id);
                    $item->chart_of_account_id = in_array($item->chart_of_account_id, [8,9]) ? $request->bank : $request->charge_of_account_id;
                    $item->description = $request->description;
                    $item->credit = $request->amount;
                    $item->save();
                }
            });
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $total = JournalTotal::where('journal_id', $journal->id)->firstOrFail();
            $total->total_credit = $request->amount;
            $total->total_debit = $request->amount;
            $total->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.debit_note.index');
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete debit note');

        DB::beginTransaction();
        try {
            $debit_note = DebitNote::findOrFail($id);
            $journal = Journal::findOrFail($debit_note->journal_id);
            $items = JournalItem::where('journal_id', $journal->id)->get();
            foreach ($items as $item) {
                $item->delete();
            }
            $journal->delete();
            JournalTotal::where('journal_id', $journal->id)->first()->delete();
            $debit_note->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function report()
    {
        $debitNote = DebitNote::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $debitNote->whereBetween('date', [$fromDate, $untilDate])->get();
        }

        return view('procurement::debit_note.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $debitNote = DebitNote::with('employee')->findOrFail($id);
        $banks = ChartOfAccount::with('chartOfAccountType')->where('level', 3)->get();
        $charts = ChartOfAccount::with('chartOfAccountType')->where('level', 3)->get();
        $suppliers = Supplier::all();
        $payables = Payable::all();
        $payableDetail = Payable::with('journal', 'supplier', 'details', 'chartOfAccount','details.purchaseInvoice')->find($debitNote->payable_id);
        $grand_total = $payableDetail->details->sum('amount');

        return view('procurement::debit_note.report.detail', compact('debitNote', 'banks', 'charts', 'suppliers', 'payables', 'payableDetail', 'grand_total'));
    }
}
