<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\HR\Entities\Employee;
use Modules\Finance\Entities\Payable;
// use Modules\Finance\Entities\PayableDetail;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseInvoice;


class ProcurementController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('procurement::index');
    }

    public function view()
    {
        return view('procurement::view');
    }

    public function openPurchasesReport()
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();

        $purchaseOrder = PurchaseOrder::where('employee_id', $employee->id)
            ->orWhere('approver_to_go', $employee->id)
            ->orWhere('latest_approver', $employee->id)
            ->orderBy('date', 'DESC')
            ->get();

        $total = PurchaseOrder::whereNull('deleted_at')
            ->where('employee_id', $employee->id)
            ->orWhere('approver_to_go', $employee->id)
            ->orWhere('latest_approver', $employee->id)
            ->sum('total');

        return view('procurement::open_purchases_report', compact('purchaseOrder', 'total'));
    }

    public function overduePurchasesReport()
    {
        $invoice = PurchaseInvoice::where('payment_status', '!=', 'Paid')->orderBy('id', 'DESC')->get();

        $total = PurchaseInvoice::whereNull('deleted_at')
            ->where('payment_status', '!=', 'Paid')
            ->sum('outstanding');

        return view('procurement::overdue_purchases_report', compact('invoice', 'total'));
    }

    public function paymentsSentReport()
    {
        $firstDayOfMonth = \Carbon\Carbon::now()->startOfMonth()->toDateString();
        $lastdayOfMonth = \Carbon\Carbon::now()->endOfMonth()->toDateString();
        $time = \Carbon\Carbon::now();

        $payable = PayableDetail::whereBetween('payment_date', [$firstDayOfMonth, $lastdayOfMonth])->get();

        $total = PayableDetail::whereNull('deleted_at')
            ->whereBetween('payment_date', [$firstDayOfMonth, $lastdayOfMonth])
            ->sum('amount');

        return view('procurement::payments_sent_report', compact('payable', 'total'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
