<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Modules\Finance\Entities\Payable;
use Modules\Finance\Entities\PayableDetail;
use Modules\Procurement\DataTables\PurchaseInvoiceDatatable;
use Modules\Procurement\DataTables\PurchaseInvoiceOutstandingDatatable;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Entities\PurchaseInvoice;
use Modules\Procurement\Entities\PurchaseInvoiceDetail;
use Modules\Procurement\Entities\PurchaseInvoiceDetailItem;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\HR\Entities\Employee;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\JournalTotal;
use Modules\Finance\Entities\Receivable;
use Modules\Finance\Entities\ReceivableDetail;
use Barryvdh\DomPDF\Facade as PDF;
use Modules\Procurement\Entities\PurchaseReturn;
use Modules\Procurement\Entities\PurchaseReturnDetail;
use NumberFormatter;
use NumberToWords\NumberToWords;

class PurchaseInvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(PurchaseInvoiceDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase invoice');
        return $datatable->render('procurement::purchase_invoice.index');
    }

    public function outstanding(PurchaseInvoiceOutstandingDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase invoice');
        return $datatable->render('procurement::purchase_invoice.index_outstanding');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {   
        $this->hasPermissionTo('add purchase invoice');
        
        $purchaseInvoice = PurchaseInvoice::whereNotNull('purchase_order_id')->pluck('purchase_order_id');
        
        $purchaseOrder = PurchaseOrder::with('employee', 'supplier', 'items')
            ->approved()
            ->whereNotIn('id', $purchaseInvoice)
            ->get();

        $paymentMethod = PaymentMethod::where('active', true)->get();

        return view('procurement::purchase_invoice.create', compact('purchaseInvoice', 'purchaseOrder', 'paymentMethod'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->hasPermissionTo('add purchase invoice');

        DB::beginTransaction();
        $this->validate($request, [
            'invoice_date'      => 'required|date',
            'invoice_due_date'  => 'required|date|after:invoice_date',
            'payment_method_id' => 'required|integer',
            'purchase_order_id' => Rule::requiredIf($request->purchase_order_id != null),
        ]);

        $purchaseOrderId = $request->purchase_order_id;
        $total = 0;

        if (!empty($purchaseOrderId)){
            $totalAmountSales = PurchaseOrder::find($purchaseOrderId);
            $total = $totalAmountSales->total;

            $exist = PurchaseInvoice::where('purchase_order_id', $purchaseOrderId)->first();
            if ($exist) { flash('Data already exist')->error(); return  back();}
        }

        try {
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('invoice');
            $code_number = generate_code_sequence('INP', $sequence_number, 3);
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $purchaseInvoice = PurchaseInvoice::create([
                'invoice_number' => !empty($request->invoice_number) ? $request->invoice_number : $code_number,
                'invoice_due_date' => $request->invoice_due_date,
                'invoice_date' => $request->invoice_date,
                'payment_method_id' => $request->payment_method_id,
                'purchase_order_id' => !empty($purchaseOrderId) ? $purchaseOrderId : null,
                'payment_status' => 'Unpaid',
                'total' => $total,
                'supplier_name' => $request->supplier_name,
                'outstanding' => $total,
                'description' => $request->description,
                'employee_id' => $employee->id,
            ]);
        } catch(\Exception $e){
            DB::rollback();
            flash($e->getMessage())->error();
            return back();
        }

        try {
            if (! is_null($purchaseOrderId)){
                $purchaseOrder = PurchaseOrder::find($purchaseOrderId);
                $purchaseOrderDetail = PurchaseOrderItem::where('purchase_order_id', $purchaseInvoice->purchase_order_id)->get();
                $gst = ($purchaseInvoice->purchaseOrder->tax->rate / 100) * $purchaseInvoice->purchaseOrder->sub_total;
                $subTotal = 0;

                foreach ($purchaseOrderDetail as $row){
                    $subTotal += $row->sub_total;
                    PurchaseInvoiceDetail::create([
                        'invoice_id' => $purchaseInvoice->id,
                        'item' => optional($row->product)->name,
                        'uom' => optional($row->uom)->code,
                        'qty' => $row->qty,
                        'returnable_qty' => $row->qty,
                        'price' => $row->price,
                        'discount_amount' => $row->discount_rate,
                        'sub_total' => $row->sub_total,
                    ]);
                }

                $discountAllItem = $purchaseOrder->discount_all_item;
                $subTotal2 = $subTotal - $discountAllItem;
                PurchaseInvoiceDetailItem::create([
                    'invoice_id' => $purchaseInvoice->id,
                    'sub_total_exclude_disc' => optional($purchaseInvoice->purchaseOrder)->get_total,
                    'discount_all_item' => optional($purchaseInvoice->purchaseOrder)->discount_all_item,
                    'subtotal_detail' => $subTotal2,
                    'gst' => $purchaseInvoice->purchaseOrder->tax->rate,
                    'amount_gst' => $gst,
                    'total' => $purchaseInvoice->total,
                ]);
            }
        } catch(\Exception $e){
            DB::rollback();
            flash($e->getMessage())->error();
            return back();
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.purchase_invoice.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $this->hasPermissionTo('view purchase invoice');
        
        $purchaseInvoice = PurchaseInvoice::with('purchaseOrder', 'paymentMethod')
            ->find($id);
        $purchaseInvoiceDetails = PurchaseInvoiceDetail::where('invoice_id', $purchaseInvoice->id)->get();
        $purchaseInvoiceItem = PurchaseInvoiceDetailItem::where('invoice_id', $purchaseInvoice->id)->first();
        

        $purchaseReturn = PurchaseReturn::where('purchase_invoice_id', $purchaseInvoice->id)->get();

        if(!empty($purchaseReturn[0])){
            $purchaseReturnDetail = PurchaseReturnDetail::where('purchase_return_id', $purchaseReturn[0]->id)->get();
        }else{
            $purchaseReturnDetail = [];
        }
        return view('procurement::purchase_invoice.show', compact('purchaseInvoice', 'purchaseInvoiceDetails', 'purchaseInvoiceItem', 'purchaseReturn', 'purchaseReturnDetail'));
    }

    public function showPurchaseReturn($id)
    {
        $this->hasPermissionTo('view purchase return');
        
        $purchaseReturn = PurchaseReturn::findOrFail($id);
        $purchaseReturnDetail = PurchaseReturnDetail::where('purchase_return_id', $id)->get();

        return view('procurement::purchase_invoice.partials.modals.purchase_return.detail', compact('purchaseReturn', 'purchaseReturnDetail'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete purchase invoice');
        DB::beginTransaction();
        try {
            $invoice = PurchaseInvoice::find($id);

            # delete receivable and journal
            $receivableDetails = PayableDetail::where('invoice_id', $invoice->id)->get();
            if (!$receivableDetails->isEmpty()){
                foreach ($receivableDetails as $receivableDetail){
                    $journal = Journal::find($receivableDetail->journal_id);
                    if (!is_null($journal)){
                        $journal->delete();
                    }

                    $payable = Payable::find($receivableDetail->payable_id);
                    if (!is_null($payable)){
                        $payable->delete();
                    }
                    $receivableDetail->delete();
                }
            }

            # delete purchase entry
            $purchaseReturn = PurchaseReturn::firstWhere('purchase_invoice_id', $invoice->id);
            if (!is_null($purchaseReturn)){
                $purchaseReturn->delete();
            }
        } catch(\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            # delete invoice
            PurchaseInvoiceDetail::where('invoice_id', $invoice->id)->delete();
            $invoice->delete();
        } catch(\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function pdf($id)
    {
        $this->hasPermissionTo('view purchase invoice');

        $purchaseInvoice = PurchaseInvoice::findOrFail($id);
        $purchaseInvoiceDetail = PurchaseInvoiceDetail::where('invoice_id', $purchaseInvoice->id)->first();
        $purchaseInvoiceItem = PurchaseInvoiceDetailItem::where('invoice_id', $purchaseInvoice->id)->first();
        $invoiceItem = PurchaseInvoiceDetailItem::where('invoice_id', $purchaseInvoice->id)->first();

        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');


        $str = $numberTransformer->toWords($purchaseInvoiceItem->total);
        $terbilang = 'Malaysia Ringgits : ' . strtoupper($str) . ' ONLY';

        # Load and set paper
        $pdf = PDF::loadView('procurement::purchase_invoice.pdf', compact('purchaseInvoice', 'purchaseInvoiceDetail', 'purchaseInvoiceItem', 'terbilang', 'numberToWords','invoiceItem'));

        $pdf->setPaper('A4', 'potrait');

        return $pdf->stream("purchase_invoice.pdf");
    }

    public function fetchPartial($id)
    {
        $purchaseInvoice = PurchaseInvoice::with('purchaseOrder', 'paymentMethod')
            ->find($id);

        $payments = PayableDetail::where('invoice_id', $purchaseInvoice->id)->get();

        return view('procurement::purchase_invoice.partials.payment_history', compact('purchaseInvoice', 'payments'));
    }

    public function receiveForm($id)
    {
        $receive = PurchaseInvoice::with('purchaseOrder.supplier','employee')->find($id);
        $chartOfAccount = ChartOfAccount::where('level', 3)->get();
        $poId = $receive->purchaseOrder ? $receive->purchaseOrder->supplier->id : $receive->deliveryOrder->customer->id;

        return view('procurement::purchase_invoice.receivable_form', compact('receive', 'chartOfAccount', 'poId'));
    }

    public function report()
    {
        $purchaseInvoice = PurchaseInvoice::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $purchaseInvoice->whereBetween('invoice_date', [$fromDate, $untilDate])->orderBy('id', 'desc')->paginate(10);
        }

        return view('procurement::purchase_invoice.report.index', compact('results'));
    }
}
