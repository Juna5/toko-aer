<?php

namespace Modules\Procurement\Http\Controllers\PurchaseOrder;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseRequisition;
use Modules\Procurement\Exports\PurchaseOrderExport;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\PurchaseOrderApproval;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Supplier;
use Barryvdh\DomPDF\Facade as PDF;
use Modules\Stock\Entities\Stock;
use Modules\Procurement\Entities\Product;
use Illuminate\Http\Request;
use Modules\Stock\Entities\StockAdjustment;
use Modules\Stock\Entities\StockAdjustmentDetail;
use Modules\Stock\Entities\StockTransaction;
use Illuminate\Http\Response;
use Modules\Procurement\DataTables\PurchaseOrderDatatable;
use Modules\Procurement\Http\Requests\PurchaseOrderRequest;
use Maatwebsite\Excel\Facades\Excel;
use App\Mail\ApproverEmail;
use Carbon\Carbon;

use Mail;

class PurchaseOrderController extends Controller
{
    protected $menu = 'purchase order';

    public function index(PurchaseOrderDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase order');
        return $datatable->render('procurement::purchase_order.entry.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add purchase order');
        $product = Product::where('name', 'ilike', "Air")->whereOr('name', 'ilike', "air")->first();
        $user = auth()->user()->id;
        $employee = Employee::where('user_id', $user)->first();

        $getTotal = getTotal($product->id, $employee->depot_id) ?? '0';
        return view('procurement::purchase_order.entry.create', compact('getTotal'));
    }

    public function store(PurchaseOrderRequest $request)
    {
        $this->hasPermissionTo('add purchase order');
        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            if (!$employee->depot_id) {
                noty()->danger('Depot Null');
                return response()->json([
                'success' => true,
                'redirect' => back()
                ]);
            }
            $now = now()->format('H:i');

            if($now >= "07:00" && $now <= "14:00"){
                $shift = 1;
            }else{
                $shift = 2;
            }
            $firstApproval = getFirstApproval($this->menu, $employee->department_id, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $employee->department_id, $employee->division_id);
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('purchase order');
            $code_number = generate_code_sequence('PO', $sequence_number, 3);
            $po = PurchaseOrder::create([
            'purchase_requisition_id' => 0,
            'code_number' => $code_number,
            'title' => "-",
            'date' => $request->date,
            'time' => $request->time,
            'supplier_id' => $request->supplier_id,
            'currency_id' => $request->currency_id,
            'payment_method_id' => $request->payment_method_id,
            'employee_id' => $employee->id,
            'status' => 'waiting',
            'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
            'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
            'department_id' => $employee->department_id,
            'payment_term_id' => $request->payment_term_id,
            'get_total' => $request->get_total,
            'discount_all_item' => (int) $request->discount_all_item,
            'sub_total' => $request->sub_total,
            'tax_id' => $request->tax_id,
            'total' => $request->total,
            'payment_status' => 'Unpaid',
            'depot_id' => $employee->depot_id,
            'shift' => $shift,
            ]);

            foreach ($request->products as $row) {
                $subTotal = ($row['price'] * $row['qty']) - ($row['discountAmount'] ? $row['discountAmount'] : 0 ) - ($row['discountRate'] ? (($row['discountRate'] / 100) * $row['price']) : 0) * $row['qty'];
                if ($row['item'] != null && $row['check'] == true) {
                    PurchaseOrderItem::create([
                    'purchase_order_id' => $po->id,
                    'product_id' => $row['item'],
                    'qty' => $row['qty'],
                    'uom_id' => $row['uom'],
                    'price' => $row['price'],
                    'discount_rate' => $row['discountRate'],
                    'discount_amount' => $row['discountAmount'],
                    'sub_total' => $subTotal,
                    'description' => $row['description']
                    ]);
                }
            }

            foreach ($getApproval as $row) {
                PurchaseOrderApproval::create([
                'purchase_order_id' => $po->id,
                'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                'role' => isset($row->role) ? $row->role : null,
                'is_approved' => false,
                ]);
            }

            // if ($firstApproval->approver_id){

            // Mail::to($firstApproval->approver->email)->send(new ApproverEmail($po));
            // }

        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return response()->json([
            'success' => false,
            'redirect' => '',
            'message' => $e->getMessage(),
            ]);
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('procurement.purchase-order.index')
        ]);
    }


    public function show($id)
    {
        $row = PurchaseOrder::find($id);

        $media = $row->getMedia('purchase_order');

        if($row){
            $media = $row->getMedia('purchase_order');

        } else {
            $media = '';
        }
        return view('procurement::purchase_order.entry.modals.attachment',compact('media'))->withMedia($media)->withRow($row);
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit purchase order');

        return view('procurement::purchase_order.entry.edit');
    }

    public function update(PurchaseOrderRequest $request, $id)
    {
        $this->hasPermissionTo('edit purchase order');

        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();

            # action for resubmit
            if (request()->resubmit) {
                \Approval::resubmit(PurchaseOrder::class, PurchaseOrderApproval::class, $id, 'purchase_order_id', [
                'time'             => $request->time,
                'employee_id'             => $employee->id,
                'date'              => $request->date,
                'supplier_id'     => $request->supplier_id,
                'currency_id'    => $request->currency_id,
                'payment_term_id'    => $request->payment_term_id,
                'payment_method_id'    => $request->payment_method_id,
                'get_total'    => $request->get_total,
                'discount_all_item'    => $request->discount_all_item,
                'sub_total'    => $request->sub_total,
                'tax_id'    => $request->tax_id,
                'total'    => $request->total,
                'project_id' => $request->project_id,
                ]);
            }else{
                # updated data
                $pr = PurchaseOrder::findOrFail($id);
                $pr->date = $request->date;
                $pr->employee_id = $employee->id;
                $pr->time = $request->time;
                $pr->supplier_id = $request->supplier_id;
                $pr->currency_id = $request->currency_id;
                $pr->payment_term_id = $request->payment_term_id;
                $pr->payment_method_id = $request->payment_method_id;
                $pr->get_total = $request->get_total;
                $pr->discount_all_item = $request->discount_all_item;
                $pr->sub_total = $request->sub_total;
                $pr->tax_id = $request->tax_id;
                $pr->total = $request->total;
                $pr->project_id = $request->project_id;
                $pr->save();
            }
            foreach ($request->products as $item) {
                if ($item['item'] != null) {
                    $subTotal = ($item['price'] * $item['qty']) - ($item['discountAmount'] ? $item['discountAmount'] : 0 ) - ($item['discountRate'] ? (($item['discountRate'] / 100) * $item['price']) : 0) * $item['qty'];
                    if (array_key_exists('id', $item)) {
                        $poItem = PurchaseOrderItem::find($item['id']);
                        $poItem->update([
                        'product_id' => $item['item'],
                        'qty' => $item['qty'],
                        'uom_id' => $item['uom'],
                        'price' => $item['price'],
                        'discount_rate' => $item['discountRate'],
                        'discount_amount' => $item['discountAmount'],
                        'sub_total' => $subTotal,
                        'description' => $item['description']
                        ]);
                    } else {
                        PurchaseOrderItem::create([
                        'purchase_order_id' => $id,
                        'product_id' => $item['item'],
                        'qty' => $item['qty'],
                        'uom_id' => $item['uom'],
                        'price' => $item['price'],
                        'discount_rate' => $item['discountRate'],
                        'discount_amount' => $item['discountAmount'],
                        'sub_total' => $subTotal,
                        'description' => $item['description']
                        ]);
                    }
                }
            }
        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return response()->json([
            'success' => false,
            'redirect' => '',
            'message' => $e->getMessage(),
            ]);
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('procurement.purchase_requisition.index')
        ]);
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete purchase order');

        $p = PurchaseOrder::findOrFail($id);

        PurchaseOrderApproval::where('purchase_order_id', $p->id)->delete();
        PurchaseOrderItem::where('purchase_order_id', $p->id)->delete();

        $p->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return redirect()->route('procurement.purchase-order.index');
    }

    public function detail($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $approvals = PurchaseOrderApproval::where('purchase_order_id', $id)->orderBy('id', 'asc')->get();
        if($row){
            $media = $row->getMedia('purchase_order');
        }else{
            $media = [];
        }

        return view('procurement::purchase_order.entry.detail', compact('row', 'items', 'approvals', 'media'));
    }

    public function report(){
        $purchaseOrder  = [];
        $roleName       = auth()->user()->getRoleNames();

        $employee       = Employee::where('user_id', auth()->user()->id)->first();

        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $status         = request()->status;
        $page           = 10;

        if($fromDate && $untilDate && $employee){
            if($status == 'all'){
                $purchaseOrder = PurchaseOrder::withoutGlobalScopes()
                ->with( 'employee', 'department', 'purchaseRequisition')
                ->whereBetween('date', [$fromDate, $untilDate])
                ->where('employee_id', $employee->id)
                ->orderBy('date','desc')
                ->orWhere('approver_to_go', $employee->id)
                ->orWhere('latest_approver', $employee->id)
                ->where('deleted_at', null)
                ->paginate($page);

            }else{
                $purchaseOrder = PurchaseOrder::withoutGlobalScopes()
                ->with( 'employee', 'department', 'purchaseRequisition')
                ->whereBetween('date', [$fromDate, $untilDate])
                ->where('employee_id', $employee->id)
                ->orderBy('date','desc')
                ->where('status', $status)
                ->orWhere('approver_to_go', $employee->id)
                ->orWhere('latest_approver', $employee->id)
                ->where('deleted_at', null)
                ->paginate($page);
            }
        }

        return view('procurement::report.purchase_order', compact('purchaseOrder'));

    }

    public function detailReport($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $approvals = PurchaseOrderApproval::where('purchase_order_id', $id)->orderBy('id', 'asc')->get();
        $from_date = request('from_date');
        $until_date = request('until_date');

        if($row){
            $media = $row->getMedia('purchase_order');
        }else{
            $media = [];
        }

        return view('procurement::report.purchase_order_detail', compact('row', 'items', 'approvals','from_date','until_date', 'media'));
    }

    public function detailModal($id)
    {
        $this->hasPermissionTo('view purchase order');

        $po = PurchaseOrder::find($id);

        return view('procurement::purchase_order.show', compact('po'));
    }

    public function pdf($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $histories = PurchaseOrderApproval::where('purchase_order_id', $id)->orderBy('id', 'asc')->get();

        $pdf = PDF::loadView('procurement::purchase_order.entry.pdf', compact('row', 'items', 'histories'));
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream($row->code_number . ".pdf");

    }

    public function saveAttachment()
    {
        $row = PurchaseOrder::find(request('id_purchase_order'));

        if (request()->hasFile('attachment')) {
            foreach (request()->attachment as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('purchase_order');
            }
        }

        return redirect()->route('procurement.purchase-order.index', compact('row'));
    }

    public function export()
    {
        return Excel::download(new PurchaseOrderExport, 'PurchaseOrder.xlsx');
    }

    public function purchaseOrder(){
        $employee = Employee::where('user_id', auth()->user()->id)->first()->id;
        $query = PurchaseOrder::with('employee', 'supplier', 'items.product', 'items.uom', 'tax')->where('employee_id', $employee)->whereNull('deleted_at')->whereHas('items', function($item){
            $item->whereNull('deleted_at');
        })->get();

        return response()->json($query);
    }

    public function supplierDetail(){
        $query = Supplier::findOrFail(request()->supplier_id);
        return response()->json($query);
    }

    public function supplier(){
        $query = Supplier::whereNull('deleted_at')->get();
        return response()->json($query);
    }

    public function accept($id)
    {
        $row = PurchaseOrder::find($id);
        $employees = Employee::all();
        return view('procurement::purchase_order.entry.modals.accept', compact('row', 'employees'));
    }

    public function saveAccept()
    {
        DB::beginTransaction();
        try {
            $po = PurchaseOrder::find(request('id_purchase_order'));
            $po->receiver_id = request('receiver_id');
            $po->save();

            $purchaseOrderItem = PurchaseOrderItem::where('purchase_order_id', $po->id)->get();

            $product = request()->product_id;
            $warehouse = request()->warehouse_id;
            $qty  = request()->qty;
            $type = request()->type;
            $date = request()->date;
            $desc = request()->description;
            # generate code number
                // $sequence = new \App\Services\SequenceNumber;
                // $sequence_number = $sequence->runSequenceNumber('stock adjustment');
                // $code_number = generate_code_sequence('SA', $sequence_number, 3);
                // $stockAdjustment = StockAdjustment::create(
                //     [
                //         'date' => now()->format('Y-m-d'),
                //         'code_number' => $code_number,
                //         'description' => "PO"
                //     ]
                // );

            foreach($purchaseOrderItem as $key => $value)
            {

                    // $stockTrans = new StockTransaction();
                    // $stockTrans->type = "IN";
                    // $stockTrans->qty = $value->qty;
                    // $stockTrans->description   = 'Stock Adjustment';
                    // $stockTrans->date = Carbon::now();
                    // $stockTrans->product_id = $value->product_id;
                    // $stockTrans->warehouse_id = optional($po->employee)->depot_id;
                    // $stockTrans->status = true;
                    // $stockTrans->in_hand = $value->qty;

                    // $stock = Stock::where('product_id', $value->product_id)->where('warehouse_id', optional($po->employee)->depot_id)->first();
                    // $products = Product::findOrFail($value->product_id);


                    //  if($stock){
                    //     $stock->qty = (int) $stock->qty + (int) $value->qty;
                    //     $stock->in = (int) $stock->in + (int) $value->qty;
                    //     $stock->save();

                    // }else{
                    //     $stock = new Stock();
                    //     $stock->product_id = $value->product_id;
                    //     $stock->warehouse_id = optional($po->employee)->depot_id;
                    //     $stock->in = $value->qty; 
                    //     $stock->qty = $value->qty;
                    //     $stock->status = true;
                    //     $stock->save();

                    //     // $stockTrans->stock_id = $stock->id;
                    //     // $stockTrans->save();
                    // }

                    // $stockTransaction = new StockTransaction();
                    // $stockTransaction->type = "IN";
                    // $stockTransaction->qty = (int) $value->qty;
                    // $stockTransaction->description   = 'Purchase Order '. $products->name .' No '. $po->code_number;
                    // $stockTransaction->date = Carbon::now();
                    // $stockTransaction->product_id = $value->product_id;
                    // $stockTransaction->warehouse_id = optional($po->employee)->depot_id;
                    // $stockTransaction->purchase_order_id = $po->id;
                    // $stockTransaction->stock_id = $stock->id;
                    // $stockTransaction->status = true;
                    // $stockTransaction->in_hand = (int) $stock->qty;
                    // $stockTransaction->save();




                    // // StockAdjustmentDetail::create(
                    // //     [
                    // //         'stock_adjustment_id' => $stockAdjustment->id,
                    // //         'product_id'          => $value->product_id,
                    // //         'warehouse_id'        => optional($po->employee)->depot_id,
                    // //         'type'                => "IN",
                    // //         'qty'                 => $value->qty,
                    // //     ]
                    // // );
                $product = Product::findOrFail($value->product_id);
                $seal = Product::whereName('Seal')->orWhere('code', 'SL')->first();
                $tissu = Product::whereName('Tissue Basah')->orWhere('code', 'TB')->first();
                $galon = Product::whereName('Galon Kosong')->orWhere('code', 'GK')->first();
                $air = Product::whereName('Air')->first();

                if($product->code == 'GU' || $product->code == 'GL' || $product->code == 'GK'){

                    if (!$galon) {
                        $galon = Product::create(['code' => 'GK', 'name' => 'Galon Kosong', 'brand' => 'AerPlus', 'product_category_id' => 2, 'product_sub_category_id' => 3, 'uom_id' => 10, 'description' => 'Galon Kosong']);
                    }

                    $stockSeal = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('product_id', $seal->id)->first();
                    $stockGalon  = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('product_id', $galon->id)->first();
                    $stockTissu  = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('product_id', $tissu->id)->first();
                    $stockProduct  = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('product_id', $value->product_id)->first();
                    $stockAir  = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('description', 'Stock Air')->first();

                    if (!$stockSeal) {
                        $stockSeal = Stock::create(['product_id' => $seal->id, 'warehouse_id' => optional($po->employee)->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } 
                    if (!$stockGalon) {
                        $stockGalon = Stock::create(['product_id' => $galon->id, 'warehouse_id' => optional($po->employee)->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } 
                    if (!$stockTissu) {
                        $stockTissu = Stock::create(['product_id' => $tissu->id, 'warehouse_id' => optional($po->employee)->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } 
                    if (!$stockProduct) {
                        $stockProduct = Stock::create(['product_id' => $product->id, 'warehouse_id' => optional($po->employee)->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } 

                    $jml = $value->qty;


                    if (!$stockAir) {
                        $stockAir = Stock::create(['warehouse_id' => optional($po->employee)->depot_id, 'qty' => 8000, 'in' => 8000, 'description' => 'Stock Air', 'product_id' => $air ? $air->id : 9]);
                    } 

                    if ($product->code == 'GU') {
                        $req_air = $stockAir;
                        $req_air->product_id = $air ? $air->id : 9;
                        $req_air->qty = $req_air->qty + $jml;
                        $req_air->status = true;
                        $req_air->in = $req_air->in + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = optional($po->employee)->depot_id;
                        $trans->type = 'IN';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Purchase Order Air No : ' . $po->code_number;
                        $trans->in_hand =  $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->purchase_order_id = $po->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty + $jml;
                        $req_seal->in = (int) $req_seal->in + $jml;
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = optional($po->employee)->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'IN';
                        $transSeal->qty = (int) $jml;
                        $transSeal->status  = true;
                        $transSeal->description = 'Purchase Order Seal No : ' . $po->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->purchase_order_id = $po->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty + $jml;
                        $req_tissu->in = (int) $req_tissu->in + $jml;
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = optional($po->employee)->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'IN';
                        $transTissu->qty = (int) $jml;
                        $transTissu->status  = true;
                        $transTissu->description = 'Purchase Order Tissu Basah No : ' . $po->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->purchase_order_id = $po->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty + $jml;
                        $req_produk->in = (int) $req_produk->in + $jml;
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = optional($po->employee)->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'IN';
                        $transProduk->qty = (int) $jml;
                        $transProduk->status  = 1;
                        $transProduk->description = 'Purchase Order ' . $product->name . ' No : ' . $po->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->purchase_order_id = $po->id;
                        $transProduk->save();
                    }
                    if ($product->code == 'GL') {
                        $req_air = $stockAir;
                        $req_air->product_id = $air ? $air->id : 9;
                        $req_air->qty =  $req_air->qty + $jml;
                        $req_air->status = true;
                        $req_air->in =  $req_air->in + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = optional($po->employee)->depot_id;
                        $trans->type = 'IN';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Purchase Order Air No : ' . $po->code_number;
                        $trans->in_hand = $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->purchase_order_id = $po->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty + $jml;
                        $req_seal->in = (int) $req_seal->in + $jml;
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = optional($po->employee)->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'IN';
                        $transSeal->qty = (int) $jml;
                        $transSeal->status  = true;
                        $transSeal->description = 'Purchase Order Seal No : ' . $po->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->purchase_order_id = $po->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty + $jml;
                        $req_tissu->in = (int) $req_tissu->in + $jml;
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = optional($po->employee)->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'IN';
                        $transTissu->qty = (int) $jml;
                        $transTissu->status  = true;
                        $transTissu->description = 'Purchase Order Tissu Basah No : ' . $po->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->purchase_order_id = $po->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty + $jml;
                        $req_produk->in = (int) $req_produk->in + $jml;
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = optional($po->employee)->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'IN';
                        $transProduk->qty = (int) $jml;
                        $transProduk->status  = 1;
                        $transProduk->description = 'Purchase Order ' . $product->name . ' No : ' . $po->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->purchase_order_id = $po->id;
                        $transProduk->save();

                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty + $jml;
                        $req_galon->in = (int) $req_galon->in + (int) $jml;
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = optional($po->employee)->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'IN';
                        $transGalon->qty = (int) $jml;
                        $transGalon->status  = 1;
                        $transGalon->description = 'Purchase Order Galon  No : ' . $po->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->spurchase_order_id = $po->id;
                        $transGalon->save();
                    }

                    if ($product->code == 'GK') {
                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty + $jml;
                        $req_galon->in = (int) $req_galon->in + $jml;
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = optional($po->employee)->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'IN';
                        $transGalon->qty = (int) $jml;
                        $transGalon->status  = 1;
                        $transGalon->description = 'Purchase Order  No : ' . $po->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->purchase_order_id = $po->id;
                        $transGalon->save();
                    }
                }else{
                    if($product->name == 'Air'){
                      $stock = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('product_id', $product->id)->where('description','Stock Air')->first();

                        if(!$stock){
                           $stock = Stock::create(['product_id' => $product->id, 'warehouse_id' => optional($po->employee)->depot_id, 'qty' => 1000, 'in' => 1000,'description' => 'Stock Air']); 
                        }  
                    }else{
                        $stock = Stock::where('warehouse_id', optional($po->employee)->depot_id)->where('product_id', $product->id)->first();

                        if(!$stock){
                           $stock = Stock::create(['product_id' => $product->id, 'warehouse_id' => optional($po->employee)->depot_id, 'qty' => 1000, 'in' => 1000]); 
                        }  
                    }
                    
                    $jml = $value->qty;

                    $reqs = $stock;
                    $reqs->qty = $reqs->qty + $jml;
                    $reqs->status = true;
                    $reqs->in = $reqs->in + $jml;
                    $reqs->save();

                    $trans = new StockTransaction();
                    $trans->warehouse_id = optional($po->employee)->depot_id;
                    $trans->product_id = $galon->id;
                    $trans->type = 'IN';
                    $trans->qty = (int) $jml;
                    $trans->status  = 1;
                    $trans->description = 'Purchase Order '.$product->name.'  No : ' . $po->code_number;
                    $trans->in_hand = $reqs->qty;
                    $trans->stock_id = $reqs->id;
                    $trans->purchase_order_id = $po->id;
                    $trans->save();
                }       


        }
        DB::commit();
            noty()->success('Yeay!', 'Your entry has been added successfully');
            return redirect()->back();
        } catch(\Exception $e) {
            DB::rollback();
            noty()->danger('Oops!', 'Please try again '.$e->getLine());
            return redirect()->back();
        }
    }

}
