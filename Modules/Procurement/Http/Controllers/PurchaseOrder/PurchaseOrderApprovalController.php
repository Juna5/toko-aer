<?php

namespace Modules\Procurement\Http\Controllers\PurchaseOrder;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\PurchaseOrderApproval;
use Modules\Procurement\DataTables\PurchaseOrderApprovalDatatable;

class PurchaseOrderApprovalController extends Controller
{
    public function index(PurchaseOrderApprovalDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase order approval');

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if (empty($employee)){
            flash('Ops.. please check your employee id.')->error();
            return back();
        }

        $purchaseOrders = filterModuleByStatus(PurchaseOrder::class, [
            'employee', 'department', 'purchaseRequisition'
        ]);
        return $datatable->render('procurement::purchase_order.approval.index', compact('purchaseOrders'));
    }


    public function show($id)
    {
        $this->hasPermissionTo('view purchase order approval');

        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $histories = PurchaseOrderApproval::where('purchase_order_id', $id)
            ->get()
            ->sortBy('id');
         if($row){
            $media = $row->getMedia('purchase_order') ?? [];
        }else{
            $media = [];
        }

        return view('procurement::purchase_order.approval.modals.detail', compact('row', 'items','histories','media'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit purchase order approval');

        $description = request('description');
        if (request()->has('approve')) {
            \Approval::action(PurchaseOrder::class, PurchaseOrderApproval::class, $id, 'purchase_order_id', $description);
        } else {
            \Approval::reject(PurchaseOrder::class, PurchaseOrderApproval::class, $id, 'purchase_order_id', $description);
        }

        flash('Your entry has been updated successfully')->success();
        return back();
    }
}
