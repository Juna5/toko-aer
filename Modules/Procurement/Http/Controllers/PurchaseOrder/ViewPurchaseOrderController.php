<?php

namespace Modules\Procurement\Http\Controllers\PurchaseOrder;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ViewPurchaseOrderController extends Controller
{
    public function __invoke()
    {
        return view('procurement::purchase_order.view');
    }
}
