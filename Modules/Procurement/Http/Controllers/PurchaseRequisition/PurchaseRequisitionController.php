<?php

namespace Modules\Procurement\Http\Controllers\PurchaseRequisition;

use Illuminate\Http\Request;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\CostCenter;
use Modules\HR\Entities\Employee;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Uom;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\PurchaseRequisition;
use Modules\Procurement\Entities\PurchaseRequisitionItem;
use Modules\Procurement\Exports\PurchaseRequisitionExport;
use Modules\Procurement\Entities\PurchaseRequisitionApproval;
use Modules\Procurement\DataTables\PurchaseRequisitionDatatable;
use Modules\Procurement\Http\Requests\PurchaseRequisitionRequest;
use Maatwebsite\Excel\Facades\Excel;
use Barryvdh\DomPDF\Facade as PDF;
use App\Mail\ApproverEmail;
use Mail;

class PurchaseRequisitionController extends Controller
{
    protected $menu = 'purchase requisition';

    public function index(PurchaseRequisitionDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase requisition');

        return $datatable->render('procurement::purchase_requisition.entry.index');
    }


    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
        $this->hasPermissionTo('add purchase requisition');

        return view('procurement::purchase_requisition.entry.create');
    }

    /**
    * Store a newly created resource in storage.
    * @param Request $request
    * @return Response
    */
    public function store(PurchaseRequisitionRequest $request)
    {
        $this->hasPermissionTo('add purchase requisition');

        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $firstApproval = getFirstApproval($this->menu, $employee->department_id, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $employee->department_id, $employee->division_id);
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('purchase requisition');
            $code_number = generate_code_sequence('PR', $sequence_number, 3);
            $pr = PurchaseRequisition::create([
            'employee_id' => $employee->id,
            'code_number' => $code_number,
            'status' => 'waiting',
            'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
            'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
            'title' => $request->title,
            'submission_date' => $request->submission_date,
            'required_date' => $request->required_date,
            'department_id' => $employee->department_id,
            'cost_center_id' => 1,
            'project_id' => $request->project_id
            ]);

            foreach ($request->products as $row) {
                if ($row['item'] != null) {
                    PurchaseRequisitionItem::create([
                    'purchase_requisition_id' => $pr->id,
                    'product_id' => $row['item'],
                    'qty' => (int)$row['qty'],
                    'uom_id' => $row['uom'],
                    'remarks' => $row['remark']
                    ]);
                }
            }

            foreach ($getApproval as $row) {
                PurchaseRequisitionApproval::create([
                'purchase_requisition_id' => $pr->id,
                'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                'role' => isset($row->role) ? $row->role : null,
                'is_approved' => false,
                ]);
            }

            if ($firstApproval->approver_id){
                
            Mail::to($firstApproval->approver->email)->send(new ApproverEmail($pr));
            }

        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('procurement.purchase_requisition.index')
        ]);
    }

    /**
    * Show the specified resource.
    * @param int $id
    * @return Response
    */
    public function show($id)
    {
        $row = PurchaseRequisition::find($id);

        $media = $row->getMedia('purchase_requisition');

        if($row){
            $media = $row->getMedia('purchase_requisition');

                } else {
                    $media = '';
                }
        return view('procurement::purchase_requisition.entry.modals.attachment',compact('media'))->withMedia($media)->withRow($row);
    }

    /**
    * Show the form for editing the specified resource.
    * @param int $id
    * @return Response
    */
    public function edit($id)
    {
        $this->hasPermissionTo('edit purchase requisition');

        return view('procurement::purchase_requisition.entry.edit');
    }

    /**
    * Update the specified resource in storage.
    * @param Request $request
    * @param int $id
    * @return Response
    */
    public function update(PurchaseRequisitionRequest $request, $id)
    {
        $this->hasPermissionTo('edit purchase requisition');

        DB::beginTransaction();
        try {
            # action for resubmit
            if (request()->resubmit) {
                \Approval::resubmit(PurchaseRequisition::class, PurchaseRequisitionApproval::class, $id, 'purchase_requisition_id', [
                    // 'title'             => $request->title,
                    'submission_date'   => $request->submission_date,
                    'required_date'     => $request->required_date,
                    'cost_center_id'    => 1,
                    'project_id' => $request->project_id
                ]);

                foreach ($request->products as $item) {
                    if ($item['item'] != null) {
                        if (array_key_exists('id', $item)) {
                            $prItem = PurchaseRequisitionItem::find($item['id']);
                            $prItem->update([
                            'product_id' => $item['item'],
                            'qty' => $item['qty'],
                            'uom_id' => $item['uom'],
                            'remarks' => $item['remark']
                            ]);
                        } else {
                            PurchaseRequisitionItem::create([
                            'purchase_requisition_id' => $id,
                            'product_id' => $item['item'],
                            'qty' => $item['qty'],
                            'uom_id' => $item['uom'],
                            'remarks' => $item['remark']
                            ]);
                        }
                    }
                }
            }else{
                # updated data
                $pr = PurchaseRequisition::findOrFail($id);
                // $pr->title = $request->title;
                $pr->submission_date = $request->submission_date;
                $pr->required_date = $request->required_date;
                $pr->project_id = $request->project_id;
                // $pr->cost_center_id = $request->cost_center_id;
                $pr->save();

                foreach ($request->products as $row) {
                    if ($row['item'] != null) {
                        if (array_key_exists('id', $row)) {
                            $item = PurchaseRequisitionItem::find($row['id']);
                            $item->update([
                            'product_id' => $row['item'],
                            'qty' => $row['qty'],
                            'uom_id' => $row['uom'],
                            'remarks' => $row['remark']
                            ]);
                        } else {
                            PurchaseRequisitionItem::create([
                            'purchase_requisition_id' => $pr->id,
                            'product_id' => $row['item'],
                            'qty' => $row['qty'],
                            'uom_id' => $row['uom'],
                            'remarks' => $row['remark']
                            ]);
                        }
                    }
                }
            }
        } catch(\Exception $e)
        {
            DB::rollback();
            flash('Ops, try again: '.$e->getMessage())->error();
            return back();
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return response()->json([
        'success' => true,
        'redirect' => route('procurement.purchase_requisition.index')
        ]);
    }

    /**
    * Remove the specified resource from storage.
    * @param int $id
    * @return Response
    */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete purchase requisition');

        $p = PurchaseRequisition::findOrFail($id);

        PurchaseRequisitionApproval::where('purchase_requisition_id', $p->id)->delete();
        PurchaseRequisitionItem::where('purchase_requisition_id', $p->id)->delete();

        $p->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return redirect()->route('procurement.purchase_requisition.index');
    }

    public function detail($id)
    {
        $row = PurchaseRequisition::findOrFail($id);
        $items = PurchaseRequisitionItem::where('purchase_requisition_id', $id)->get();
        $approvals = PurchaseRequisitionApproval::where('purchase_requisition_id', $id)->orderBy('id', 'asc')->get();
        if($row){
            $media = $row->getMedia('purchase_requisition');
        }else{
            $media = [];
        }

        return view('procurement::purchase_requisition.entry.detail', compact('row', 'items', 'approvals', 'media'));
    }

    public function department()
    {
        $departments = Department::get();

        return $departments;
    }

    public function costCenter()
    {
        $costCenters = CostCenter::get();

        return $costCenters;
    }

    public function product()
    {
        $product = Product::get();
        return $product;
    }

    public function productDetail($id)
    {
        $product = Product::find($id);
        return $product;
    }

    public function uom()
    {
        $uom = Uom::get();
        return $uom;
    }

    public function report(){
        $purchaseRequisition = [];
        $roleName       = auth()->user()->getRoleNames();

        $employee       = Employee::where('user_id', auth()->user()->id)->first();

        $fromDate       = request()->from_date;
        $untilDate      = request()->until_date;
        $status         = request()->status;
        $page           = 10;

        if($fromDate && $untilDate && $employee){
            if($status == 'all'){
                $purchaseRequisition = PurchaseRequisition::withoutGlobalScopes()
                ->with('employee', 'department', 'costCenter')
                ->whereBetween('submission_date', [$fromDate, $untilDate])
                ->where('employee_id', $employee->id)
                ->orWhere('approver_to_go', $employee->id)
                ->orWhere('latest_approver', $employee->id)
                ->where('deleted_at', null)
                ->orderBy('submission_date', 'desc')
                ->paginate($page);

            }else{
                $purchaseRequisition = PurchaseRequisition::withoutGlobalScopes()
                ->with('employee', 'department', 'costCenter')
                ->whereBetween('submission_date', [$fromDate, $untilDate])
                ->where('employee_id', $employee->id)
                ->where('status', $status)
                ->orWhere('approver_to_go', $employee->id)
                ->orWhere('latest_approver', $employee->id)
                ->where('deleted_at', null)
                ->orderBy('submission_date', 'desc')
                ->paginate($page);
            }
        }
        return view('procurement::report.purchase_requisition', compact('purchaseRequisition'));

    }

    public function detailReport($id)
    {
        $row = PurchaseRequisition::findOrFail($id);
        $items = PurchaseRequisitionItem::where('purchase_requisition_id', $id)->get();
        $approvals = PurchaseRequisitionApproval::where('purchase_requisition_id', $id)->orderBy('id', 'asc')->get();
        $from_date = request('from_date');
        $until_date = request('until_date');

        if($row){
            $media = $row->getMedia('purchase_requisition');
        }else{
            $media = [];
        }

        return view('procurement::report.purchase_requisition_detail', compact('row', 'items', 'approvals','from_date','until_date', 'media'));
    }

    public function saveAttachment()
    {
        $row = PurchaseRequisition::find(request('id_purchase_requisition'));

        if (request()->hasFile('attachment')) {
            foreach (request()->attachment as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('purchase_requisition');
            }
        }

        return redirect()->route('procurement.purchase_requisition.index', compact('row'));
    }

    public function getPrDetail($id)
    {
        $row = PurchaseRequisition::findOrFail($id);
        $items = PurchaseRequisitionItem::where('purchase_requisition_id', $id)->get();
        $approvals = PurchaseRequisitionApproval::where('purchase_requisition_id', $id)->orderBy('id', 'asc')->get();
        if($row){
            $media = $row->getMedia('purchase_requisition');
        }else{
            $media = [];
        }
        return view('procurement::purchase_order.approval.modals.pr_detail',compact('media'))->withMedia($media)->withRow($row)->withItems($items)->withApprovals($approvals);
    }

    public function export()
    {
        return Excel::download(new PurchaseRequisitionExport, 'PurchaseRequisition.xlsx');
    }

    public function pdf($id)
    {
        $row = PurchaseRequisition::findOrFail($id);
        $items = PurchaseRequisitionItem::where('purchase_requisition_id', $id)->get();
        $histories = PurchaseRequisitionApproval::where('purchase_requisition_id', $id)->orderBy('id', 'asc')->get();

        $pdf = PDF::loadView('procurement::purchase_requisition.entry.pdf', compact('row', 'items', 'histories'));
        $pdf->setPaper('A4', 'potrait');
        return $pdf->stream($row->code_number . ".pdf");

    }

}
