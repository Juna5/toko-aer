<?php

namespace Modules\Procurement\Http\Controllers\PurchaseRequisition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\PurchaseRequisition;
use Modules\Procurement\Entities\PurchaseRequisitionItem;
use Modules\Procurement\Entities\PurchaseRequisitionApproval;
use Modules\Procurement\DataTables\PurchaseRequisitionApprovalDatatable;

class PurchaseRequisitionApprovalController extends Controller
{
    public function index(PurchaseRequisitionApprovalDatatable $datatable)
    {
        $this->hasPermissionTo('view purchase requisition approval');

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if (empty($employee)){
            flash('Ops.. please check your employee id.')->error();
            return back();
        }
        
        $purchaseRequisitions = filterModuleByStatus(PurchaseRequisition::class, [
            'employee', 'department', 'costCenter'
        ]);
        
        return $datatable->render('procurement::purchase_requisition.approval.index', compact('purchaseRequisitions'));
    }


    public function show($id)
    {
        $this->hasPermissionTo('view purchase requisition approval');

        $row = PurchaseRequisition::findOrFail($id);
        $items = PurchaseRequisitionItem::where('purchase_requisition_id', $id)->get();
        $histories = PurchaseRequisitionApproval::where('purchase_requisition_id', $id)
            ->get()
            ->sortBy('id');
        if($row){
            $media = $row->getMedia('purchase_requisition') ?? [];
        }else{
            $media = [];
        }

        return view('procurement::purchase_requisition.approval.modals.detail', compact('row', 'items','histories','media'));
    }

    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit purchase requisition approval');

        $description = request('description');
        if (request()->has('approve')) {
            \Approval::action(PurchaseRequisition::class, PurchaseRequisitionApproval::class, $id, 'purchase_requisition_id', $description);
        } else {
            \Approval::reject(PurchaseRequisition::class, PurchaseRequisitionApproval::class, $id, 'purchase_requisition_id', $description);
        }

        flash('Your entry has been updated successfully')->success();
        return back();
    }
}
