<?php

namespace Modules\Procurement\Http\Controllers\PurchaseRequisition;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class ViewController extends Controller
{
    public function __invoke()
    {
        return view('procurement::purchase_requisition.index');
    }
}
