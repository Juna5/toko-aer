<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Modules\Procurement\DataTables\PurchaseInvoiceDatatable;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Entities\PurchaseInvoice;
use Modules\Procurement\Entities\PurchaseInvoiceDetail;
use Modules\Procurement\Entities\PurchaseInvoiceDetailItem;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\JournalTotal;
use Modules\Finance\Entities\Receivable;
use Modules\Finance\Entities\ReceivableDetail;
use Modules\Procurement\DataTables\AccountPayableDatatable;
use Barryvdh\DomPDF\Facade as PDF;
use NumberFormatter;

class AccountPayableController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(AccountPayableDatatable $datatable)
    {
        $this->hasPermissionTo('view account payable');

        return $datatable->render('procurement::account_payable.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    // public function detailReceivable(AccountPayableDatatable $datatable, $param)
    // {
    //     return $datatable->with(['customer_name' => $param])->render('finance::account_receivable.detail');
    // }

    public function report()
    {
        $row = PurchaseInvoice::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $row->whereBetween('invoice_date', [$fromDate, $untilDate])->get();
        }

        return view('procurement::account_payable.report.index', compact('results'));
    }
}
