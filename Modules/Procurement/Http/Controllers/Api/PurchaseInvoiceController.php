<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Procurement\Entities\PurchaseOrder;

class PurchaseInvoiceController extends Controller
{
    public function getPurchaseOrder($id)
    {
        $purchaseOrder = PurchaseOrder::with('employee', 'supplier', 'items.product', 'items.uom', 'tax')->find($id);
        return response()->json($purchaseOrder);
    }
}
