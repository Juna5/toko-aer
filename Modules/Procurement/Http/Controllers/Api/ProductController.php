<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\ProductPrice;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Entities\ProductSubCategory;

class ProductController extends Controller
{
    public function subCategory()
    {
        $set = request()->id;
        $subCategory = ProductSubCategory::where('product_category_id', $set)->get();

        switch (request()->type):
            case 'product_category':
                $return = '<option value="">--Please Select--</option>';
        foreach ($subCategory as $temp) {
            $return .= "<option value='$temp->id'>$temp->name</option>";
        }
        return $return;
        break;
        endswitch;
    }

    public function productCategory()
	{
		return ProductCategory::all();
	}

    public function priceCategory()
	{
		return PriceCategory::all();
	}

    public function productDetail($id)
    {
        return Product::with('uom')->findOrFail($id);
    }

    public function productPrice($productId, $uomId)
    {
        return ProductPrice::where('product_id', $productId)->where('uom_id', $uomId)->first();
    }
}
