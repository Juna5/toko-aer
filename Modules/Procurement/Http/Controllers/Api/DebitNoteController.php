<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Finance\Entities\Payable;
use Modules\Procurement\Entities\Supplier;


class DebitNoteController extends Controller
{

    public function getPayable()
    {
        $set = request()->id;
        $payable = Payable::with('journal', 'supplier', 'details', 'chartOfAccount','details.invoice')->where('supplier_id', $set)->whereHas('details', function($detail){
            $detail->whereNull('deleted_at')->whereHas('purchaseInvoice', function($purchaseInvoice){
                $purchaseInvoice->whereNull('deleted_at');
            });
        })->get();

        switch (request()->type):
            case 'payable':
                $return = '<option value="">--Please Select--</option>';
        foreach ($payable as $temp) {
            $return .= "<option value='$temp->id'>$temp->code_number</option>";
        }
        return $return;
        break;
        endswitch;
    }

    public function getPayableDetail($id)
    {
        $payable = Payable::with('journal', 'supplier', 'details', 'chartOfAccount','details.invoice','details.purchaseInvoice')->whereHas('details.purchaseInvoice', function($invoice){
            $invoice->whereNull('deleted_at');
        })->find($id);

        return response()->json($payable);
    }

    public function getSupplier(){
        $id = request()->id;
        $supplier = Supplier::findOrFail($id);
        return $supplier;
    }

}
