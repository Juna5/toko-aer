<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\HR\Entities\Employee;

class EmployeeController extends Controller
{
    public function index()
    {
      $employees = Employee::all();

        return response()->json($employees);
    }


}
