<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Supplier;
use Modules\Procurement\Http\Requests\SupplierRequest;

class SupplierController extends Controller
{
    public function store(SupplierRequest $request)
    {
        $supplier = Supplier::create([
            'company_name'        => $request->company_name,
            'name'                => $request->name,
            'phone'               => $request->phone,
            'fax'                 => $request->fax,
            'email'               => $request->email,
            'currency_id'         => $request->currency_id,
            'price_category_id'   => $request->price_category_id,
            'payment_term_id'     => $request->payment_term_id,
            'address'             => $request->address,
            'description'         => $request->description,
            'deposit'             => $request->deposit,
        ]);

        return response()->json([
			'success' => true
		]);

    }
}
