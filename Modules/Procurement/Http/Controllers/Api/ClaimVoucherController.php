<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use Modules\Procurement\Entities\Voucher;
use Modules\Procurement\Http\Requests\SupplierRequest;
use App\Http\Controllers\ApiController;

class ClaimVoucherController extends ApiController
{
    public function index(Request $request)
    {
        $row = Voucher::whereDepotId(request('depot_id'))->whereCode(request('code'))->first();
        $data = [
            'data' => $row,
            'message' => $row ? $row->sales_order_id ? "Voucher sudah dipakai" : "Voucher belum dipakai" : "Voucher tidak tersedia"
        ];
        return $this->makeResponse($data);

    }
}
