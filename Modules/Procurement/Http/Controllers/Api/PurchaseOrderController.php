<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\PurchaseRequisition;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\Supplier;
use Modules\Procurement\Entities\Project;

class PurchaseOrderController extends Controller
{
    public function index()
    {
      //   $purchaseRequisition = PurchaseRequisition::where('status', 'approved')
      //   ->where(function ($query) {
      //     $exist = PurchaseOrder::whereNotNull('purchase_requisition_id')
      //     ->whereNull('deleted_at')
      //     ->where(function ($q) {
      //         $q->where('status', 'waiting')->orWhere('status', 'approved');
      //     })
      //     ->get()
      //     ->pluck('purchase_requisition_id');
      //     $query->whereNotIn('id', $exist);
      // })->get();
      $purchaseRequisition = PurchaseRequisition::all();

        return response()->json($purchaseRequisition);
    }

    public function supplier()
    {
        $supplier = Supplier::all();

        return response()->json($supplier);
    }

    public function department()
    {
        $department = Department::where('active', true)->get();

        return response()->json($department);
    }

    public function currency()
    {
        $currency = Currency::where('active', true)->get();

        return response()->json($currency);
    }

    public function paymentMethod()
    {
        $paymentMethod = PaymentMethod::where('active', true)->get();

        return response()->json($paymentMethod);
    }

    public function paymentTerm()
    {
        $paymentTerm = PaymentTerm::all();

        return response()->json($paymentTerm);
    }

    public function getPurchaseRequisition($id)
    {
        $getPurchaseRequisition = PurchaseRequisition::with(['project','department', 'costCenter','items', 'items.product', 'items.uom'])
            ->find($id);

        return response()->json($getPurchaseRequisition);
    }

    public function edit($id)
	{
		return PurchaseOrder::with(['department', 'items', 'items.product', 'items.uom', 'purchaseRequisition', 'supplier', 'currency', 'paymentTerm', 'paymentMethod','project'])->find($id);
	}

    public function removeItem($id)
	{
		PurchaseOrderItem::find($id)->forceDelete();
		return response()->json([
			'success' => true
		]);
	}

    public function supplierDetail($id)
    {
        $supplier = Supplier::with('currency','paymentTerm')->find($id);

        return response()->json($supplier);
    }

    public function project()
    {
        $project = Project::all();

        return $project;
    }


    public function purchaseOrder(){
      $employee = Employee::where('user_id', auth()->user()->id)->first()->id;
      $query = PurchaseOrder::with('employee', 'supplier', 'items.product', 'items.uom', 'tax')->where('employee_id', $employee)->whereNull('deleted_at')->get();

      return response()->json(auth()->user()->id);
    }

    public function purchaseOrderDetail(){
      $id = request()->purchase_order_id; 
      $query = PurchaseOrder::with('employee', 'supplier', 'items.product', 'items.uom', 'tax')->whereNull('deleted_at')->find($id);
      return response()->json($query);
    }

}
