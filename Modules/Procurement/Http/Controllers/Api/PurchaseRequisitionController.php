<?php

namespace Modules\Procurement\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Procurement\Entities\PurchaseRequisition;
use Modules\Procurement\Entities\PurchaseRequisitionItem;

class PurchaseRequisitionController extends Controller
{
    public function edit($id)
	{
		return PurchaseRequisition::with(['department', 'costCenter', 'items', 'items.product', 'items.uom', 'project'])->find($id);
	}

    public function removeItem($id)
	{
		PurchaseRequisitionItem::find($id)->forceDelete();
		return response()->json([
			'success' => true
		]);
	}
}
