<?php

namespace Modules\Procurement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Modules\Procurement\Entities\PurchaseInvoice;
use Modules\Procurement\Entities\PurchaseInvoiceDetail;
use Modules\Procurement\Entities\PurchaseInvoiceDetailItem;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\DataTables\AccountPayableAgingDatatable;

class AccountPayableAgingController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(AccountPayableAgingDatatable $datatable)
    {
        $this->hasPermissionTo('view a/p aging report');

        return $datatable->render('procurement::account_payable.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('procurement::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('procurement::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    // public function detailReceivable(AccountPayableDatatable $datatable, $param)
    // {
    //     return $datatable->with(['customer_name' => $param])->render('finance::account_receivable.detail');
    // }
}
