<?php

namespace Modules\Procurement\Http\Controllers\Voucher;

use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Voucher;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Procurement\Entities\Customer;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Rap2hpoutre\FastExcel\FastExcel;

class VoucherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        $voucher = Voucher::when($keyword, function ($query) use ($keyword) {
            $query->where('depot_id', 'LIKE', "%$keyword%")
                ->orWhere('code', 'LIKE', "%$keyword%")
                ->orWhere('amount', 'LIKE', "%$keyword%")
                ->orWhere('no_telp', 'LIKE', "%$keyword%");
        })->orderBy('updated_at','desc')->paginate($perPage);

        return view('procurement::voucher.index', compact('voucher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $depot = WarehouseMaster::where('deleted_at', null)->get()->pluck('name', 'id')->prepend('Please Select', '');
        return view('procurement::voucher.create', compact('depot'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Voucher::create($requestData);

        $notification = ['message' => 'Your data has been added successfully', 'alert-type' => 'success'];
        return redirect()->route('procurement.voucher.index')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $voucher = Voucher::findOrFail($id);
        $depot = WarehouseMaster::where('deleted_at', null)->get()->pluck('name', 'id')->prepend('Please Select', '');
        return view('procurement::voucher.create', compact('voucher', 'depot'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $depot = WarehouseMaster::where('deleted_at', null)->get()->pluck('name', 'id')->prepend('Please Select', '');
        $voucher = Voucher::findOrFail($id);
        return view('procurement::voucher.edit', compact('voucher', 'depot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $voucher = Voucher::findOrFail($id);
        $voucher->update($requestData);

        $notification = ['message' => 'Your data has been updated successfully', 'alert-type'  => 'success'];
        return redirect()->route('procurement.voucher.index')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Voucher::destroy($id);

        $notification = ['message' => 'Your data has been deleted successfully', 'alert-type' => 'error'];
        return redirect()->back()->with($notification);
    }

    public function import(Request $request){
        $this->validate($request, [
            'file' => 'required'
        ]);

        $extensions = ["xls","xlsx"];
        $fileupload = [$request->file('file')->getClientOriginalExtension()];

        # if file contains .xls format
        try {
            DB::beginTransaction();
            if (in_array($fileupload[0], $extensions)) {
                (new FastExcel)->import(request()->file('file'), function ($reader) {
                    if (!empty($reader['Depot']) && !empty($reader['Code']) && !empty($reader['Amount'])) {
                        $depot = WarehouseMaster::where('name', 'ilike', $reader['Depot'])->whereNull('deleted_at')->first(); 
                        if(!$depot){
                            $depot = new WarehouseMaster();
                            $depot->code = $reader['Depot'];
                            $depot->name = $reader['Depot'];
                            $depot->save();
                        }
                        $customer = Customer::where('phone', $reader['No Telp'])->whereNull('deleted_at')->first();
                        $date = !empty($reader['Expired Date']) ? $reader['Expired Date'] : null;
                        $voucher = Voucher::updateOrCreate(
                            [
                                'depot_id'      => $depot->id,
                                'code'          => $reader['Code'],
                            ],
                            [
                                'amount'        => $reader['Amount'],
                                'expired_date'  => $date,
                                'no_telp'       => $reader['No Telp'],
                                'customer_id'   => $customer ? $customer->id : null,
                            ]
                        );
                    }
                });

            } else {
                flash('File must be .xlsx format.')->error();
                return redirect()->back();
            }

            DB::commit();
            $notification = ['message' => 'Your data has been saved successfuly.', 'alert-type' => 'success'];
            flash('Your data has been saved successfuly.')->success();
            return redirect()->back()->with($notification);
                        
        } catch (\Exception $e) {
            DB::rollback();
            $notification = ['message' => $e->getMessage(), 'alert-type' => 'error'];
            return redirect()->back()->with($notification);
        }
    }

    public function templateVoucher(){
        $file = public_path().'/download/Template Voucher.xlsx';

        $headers = array('Contect-type : application/xlsx');

        return response()->download($file, 'TemplateVoucher.xlsx', $headers);
    }
}
