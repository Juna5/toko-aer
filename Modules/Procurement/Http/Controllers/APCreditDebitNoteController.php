<?php

namespace Modules\Procurement\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Procurement\DataTables\CreditNoteDatatable;
use Modules\Procurement\DataTables\DebitNoteDatatable;
use Modules\Procurement\DataTables\APCreditDebitNoteDatatable;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Finance\Entities\Cost;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\JournalItem;
use Modules\Finance\Entities\JournalTotal;
use Modules\Finance\Entities\Receivable;
use Modules\Finance\Entities\Payable;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\Supplier;
use Modules\Transaction\Entities\CreditNote;
use Modules\Procurement\Entities\DebitNote;
use Modules\Procurement\Entities\CreditDebitNote;
use Modules\Transaction\Http\Requests\CreditNoteRequest;
use Modules\Procurement\Http\Requests\DebitNoteRequest;
use Modules\Procurement\Http\Requests\CreditDebitNoteRequest;

class APCreditDebitNoteController extends Controller
{
    public function index(APCreditDebitNoteDatatable $datatable)
    {
        $this->hasPermissionTo('view credit note');
        return $datatable->render('procurement::ap_credit_debit_note.index');
    }

    public function create()
    {
        $this->hasPermissionTo('view credit note');

        $banks = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $charts = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $customers = Customer::all();
        $suppliers = Supplier::all();
       

        return view('procurement::ap_credit_debit_note.create', compact('banks', 'charts','customers','suppliers'));
    }

    public function store(CreditDebitNoteRequest $request)
    {
        $this->hasPermissionTo('add credit note');

        DB::beginTransaction();
        try {
            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('credit note');
            $code_number = generate_code_sequence('J/CN', $sequence_number, 3);

            if($request->cn_dn = 'Credit'){
                $journal = Journal::create([
                    'journal_number' => $code_number,
                    'journal_date' => $request->date,
                    'description' => 'Credit Note' .' | '. $request->description,
                ]);
            }else{
                $journal = Journal::create([
                    'journal_number' => $code_number,
                    'journal_date' => $request->date,
                    'description' => 'Debit Note' .' | '. $request->description,
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            if($request->cn_dn = 'Credit'){
                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->bank,
                    'description' => $request->description,
                    'debit' => 0,
                    'credit' => $request->amount,
                ]);

                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->charge_of_account_id,
                    'description' => $request->description,
                    'debit' => $request->amount,
                    'credit' => 0,
                ]);
            }else{
                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->bank,
                    'description' => $request->description,
                    'debit' => $request->amount,
                    'credit' => 0,
                ]);

                JournalItem::create([
                    'journal_id' => $journal->id,
                    'chart_of_account_id' => $request->charge_of_account_id,
                    'description' => $request->description,
                    'debit' => 0,
                    'credit' => $request->amount,
                ]);
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            if($request->cn_dn = 'Credit'){
               JournalTotal::create([
                'journal_id' => $journal->id,
                'total_credit' => $request->amount,
                'total_debit' => $request->amount,
                ]); 
            }else{
               JournalTotal::create([
                'journal_id' => $journal->id,
                'total_credit' => $request->amount,
                'total_debit' => $request->amount,
                ]);
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            
            if($request->cn_dn = 'Credit'){
                $employee = Employee::where('user_id', auth()->user()->id)->first();
                $credit_note = new CreditDebitNote();
                $credit_note->charge_of_account_bank_id = $request->bank;
                $credit_note->charge_of_account_id = $request->charge_of_account_id;
                $credit_note->journal_id = $journal->id;
                $credit_note->supplier_id = $request->supplier_id;
                $credit_note->payable_id = $request->payable_id;
                $credit_note->date = $request->date;
                $credit_note->amount = $request->amount;
                $credit_note->description = $request->description;
                $credit_note->employee_id = $employee->id;
                $credit_note->is_credit_or_debit = $request->cn_dn;
                $credit_note->save();
            }else{
                $employee = Employee::where('user_id', auth()->user()->id)->first();
                $debit_note = new CreditDebitNote();
                $debit_note->charge_of_account_bank_id = $request->bank;
                $debit_note->charge_of_account_id = $request->charge_of_account_id;
                $debit_note->journal_id = $journal->id;
                $debit_note->supplier_id = $request->supplier_id;
                $debit_note->payable_id = $request->payable_id;
                $debit_note->date = $request->date;
                $debit_note->amount = $request->amount;
                $debit_note->description = $request->description;
                $debit_note->employee_id = $employee->id;
                $debit_note->is_credit_or_debit = $request->cn_dn;
                $debit_note->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.ap_credit_debit_note.index');
    }

    public function show($id)
    {
        return view('procurement::show');
    }

    public function edit(CreditDebitNote $ap_credit_debit_note, $id)
    {
       
        $this->hasPermissionTo('edit credit note');

        $ap_credit_debit_note = CreditDebitNote::findOrFail($id);

        $banks = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $charts = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();

        $suppliers = Supplier::all();
        // $debit_note = DebitNote::findOrFail($id);
        $payables = Payable::all();
        $payableDetail = Payable::with('journal', 'supplier', 'details', 'chartOfAccount','details.invoice')->whereHas('details.purchaseInvoice', function($invoice){
            $invoice->whereNull('deleted_at');
        })->find($ap_credit_debit_note->payable_id);
        if(!$payableDetail == null){
            $grand_total = $payableDetail->details->sum('amount');
        }else{
            $grand_total = 0;

        }
       
         return view('procurement::ap_credit_debit_note.edit', compact('banks', 'charts','payables','payableDetail','grand_total','suppliers','ap_credit_debit_note'));
    }

    public function update(CreditDebitNoteRequest $request, $id)
    {
        $this->hasPermissionTo('edit credit note');
        DB::beginTransaction();
        try {
            if($request->cn_dn = 'Credit'){
                $credit_note = CreditDebitNote::findOrFail($id);
                $credit_note->charge_of_account_bank_id = $request->bank;
                $credit_note->charge_of_account_id = $request->charge_of_account_id;
                $credit_note->supplier_id = $request->supplier_id;
                $credit_note->payable_id = $request->payable_id;
                $credit_note->date = $request->date;
                $credit_note->amount = $request->amount;
                $credit_note->description = $request->description;
                $credit_note->save();
            }else{
                $credit_note = CreditDebitNote::findOrFail($id);
                $credit_note->charge_of_account_bank_id = $request->bank;
                $credit_note->charge_of_account_id = $request->charge_of_account_id;
                $credit_note->supplier_id = $request->supplier_id;
                $credit_note->payable_id = $request->payable_id;
                $credit_note->date = $request->date;
                $credit_note->amount = $request->amount;
                $credit_note->description = $request->description;
                $credit_note->save();
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
           
        }

        try {
            if($request->cn_dn = 'Credit'){
                $journal = Journal::find($credit_note->journal_id);
                $journal->journal_date = $request->date;
                $journal->description = 'Credit Note' .' | '. $request->description;
                $journal->save();
            }else{
                $journal = Journal::find($credit_note->journal_id);
                $journal->journal_date = $request->date;
                $journal->description = 'Debit Note' .' | '. $request->description;
                $journal->save();
            }
            
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            
        }

        try {
            $journalItems = JournalItem::where('journal_id', $journal->id)->get();
            $journalItems->map(function ($item, $key) use ($request) {
                if ($item->debit == 0 && $item->credit != 0){
                    $item = JournalItem::find($item->id);
                    $item->chart_of_account_id = in_array($item->chart_of_account_id, [8,9]) ? $request->bank : $request->charge_of_account_id;
                    $item->description = $request->description;
                    $item->credit = $request->amount;
                    $item->save();
                } else if($item->debit != 0 && $item->credit == 0){
                    $item = JournalItem::find($item->id);
                    $item->chart_of_account_id = in_array($item->chart_of_account_id, [8,9]) ? $request->bank : $request->charge_of_account_id;
                    $item->description = $request->description;
                    $item->debit = $request->amount;
                    $item->save();
                }
            });
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
           
        }

        try {
            $total = JournalTotal::where('journal_id', $journal->id)->firstOrFail();
            $total->total_credit = $request->amount;
            $total->total_debit = $request->amount;
            $total->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
            
        }
        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.ap_credit_debit_note.index');
    }

    public function destroy($id)
    {

        $this->hasPermissionTo('delete credit note');

        DB::beginTransaction();
        try {
            $credit_debit_note = CreditDebitNote::findOrFail($id);
            $journal = Journal::findOrFail($credit_debit_note->journal_id);
            $items = JournalItem::where('journal_id', $journal->id)->get();
            foreach ($items as $item) {
                $item->delete();
            }
            if($journal){
                $journal->delete();
                $journalTotal = JournalTotal::where('journal_id', $journal->id)->first();
                if($journalTotal){
                    $journalTotal->delete();
                }
            }
            
            $credit_debit_note->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

     public function getPayable()
    {
        $set = request()->id;
        $payable = Payable::where('supplier_id', $set)->get();

        switch (request()->type):
            case 'payable':
                $return = '<option value="">--Please Select--</option>';
        foreach ($payable as $temp) {
            $return .= "<option value='$temp->id'>$temp->code_number</option>";
        }
        return $return;
        break;
        endswitch;
    }

    public function report()
    {
        $creditNote = CreditNote::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');

        if (isset($fromDate) && isset($untilDate)) {
            $results =  $creditNote->whereBetween('date', [$fromDate, $untilDate])->get();
        }

        return view('procurement::credit_note.report.index', compact('results'));
    }

    public function reportDetail($id)
    {
        $creditNote = CreditNote::with('employee')->findOrFail($id);
        $banks = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $charts = ChartOfAccount::with('chartOfAccountType')
            ->where('level', 3)
            ->get();
        $customers = Customer::all();
        $receivables = Receivable::all();
        $receivableDetail = Receivable::with('invoice', 'journal', 'customer', 'details', 'chartOfAccount','details.invoice')->find($creditNote->receivable_id);
        $grand_total = $receivableDetail->details->sum('amount');

        return view('procurement::credit_note.report.detail', compact('creditNote', 'banks', 'charts','customers','receivables','receivableDetail','grand_total'));
    }
}
