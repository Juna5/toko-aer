<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\CurrencyExport;
use Modules\Procurement\DataTables\CurrencyDatatable;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Http\Requests\CurrencyRequest;
use Maatwebsite\Excel\Facades\Excel;

class CurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(CurrencyDatatable $datatable)
    {
        $this->hasPermissionTo('view currency');
        return $datatable->render('procurement::master.currency.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add currency');
        return view('procurement::master.currency.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CurrencyRequest $request)
    {
        $this->hasPermissionTo('add currency');
        Currency::create([
            'code'              => $request->code,
            'name'              => $request->name,
            'description'       => $request->description,
            'active'            => $request->active
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.currency.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Currency $currency)
    {
        $this->hasPermissionTo('edit currency');
        return view('procurement::master.currency.edit', compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CurrencyRequest $request, Currency $currency)
    {
        $this->hasPermissionTo('edit currency');

        $currency->code          = $request->code;
        $currency->name          = $request->name;
        $currency->description   = $request->description;
        $currency->active        = $request->active;
        $currency->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.currency.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Currency $currency)
    {
        $this->hasPermissionTo('delete currency');
        $currency->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new CurrencyExport, 'Currency.xlsx');
    }
}
