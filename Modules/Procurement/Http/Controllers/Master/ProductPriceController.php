<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\DataTables\ProductPriceDatatable;
use Modules\Procurement\Entities\Uom;
use Modules\Procurement\Entities\ProductPrice;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Http\Requests\ProductPriceRequest;

class ProductPriceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProductPriceDatatable $datatable)
    {
        $this->hasPermissionTo('view product price');
        return $datatable->render('procurement::master.product_price.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('procurement::master.product_price.create', [
            'product'       => Product::all(),
            'priceCategory' => PriceCategory::all(),
            'uom'        => Uom::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProductPriceRequest $request)
    {
        $this->hasPermissionTo('add product price');

        $row = ProductPrice::create([
            'product_id'            => request('product_id'),
            'price_category_id'     => request('price_category_id'),
            'uom_id'                => request('uom_id'),
            'price'                 => request('price'),
            'profit_margin'         => request('profit_margin'),
            'discount'              => request('discount'),
            'discount_amount'       => request('discount_amount'),
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.product.edit', request('product_id'));
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->hasPermissionTo('edit product price');
        $productPrice       = ProductPrice::find($id);
        $product            = Product::all();
        $priceCategory      = PriceCategory::all();
        $uom                = Uom::all();
        return view('procurement::master.product_price.edit', compact('product', 'productPrice', 'priceCategory', 'uom'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ProductPriceRequest $request, $id)
    {
        $this->hasPermissionTo('edit product price');
        $row = tap(ProductPrice::find($id))->update(request()->all())->fresh();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.product.edit', $row->product_id);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete product price');

        ProductPrice::find($id)->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function duplicate($id)
    {
        $productPrice       = ProductPrice::find($id);
        $product            = Product::all();
        $priceCategory      = PriceCategory::all();
        $uom                = Uom::all();

        $duplicate = $productPrice->replicate()->save();

        noty()->success('Yeay!', 'Your entry has been duplicated successfully');
        return view('procurement::master.product_price.duplicate', compact('productPrice', 'product', 'priceCategory', 'uom', 'duplicate'));
    }

}
