<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\PaymentMethodExport;
use Modules\Procurement\DataTables\PaymentMethodDatatable;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Http\Requests\PaymentMethodRequest;
use Maatwebsite\Excel\Facades\Excel;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(PaymentMethodDatatable $datatable)
    {
        $this->hasPermissionTo('view payment method');
        return $datatable->render('procurement::master.payment-method.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add payment method');
        return view('procurement::master.payment-method.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(PaymentMethodRequest $request)
    {
        $this->hasPermissionTo('add uom');
        PaymentMethod::create([
            'code'              => $request->code,
            'name'              => $request->name,
            'description'       => $request->description,
            'active'            => $request->active
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.payment_method.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(PaymentMethod $paymentMethod)
    {
        $this->hasPermissionTo('edit payment method');
        return view('procurement::master.payment-method.edit', compact('paymentMethod'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(PaymentMethodRequest $request, PaymentMethod $paymentMethod)
    {
        $this->hasPermissionTo('edit uom');

        $paymentMethod->code          = $request->code;
        $paymentMethod->name          = $request->name;
        $paymentMethod->description   = $request->description;
        $paymentMethod->active        = $request->active;
        $paymentMethod->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.payment_method.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(PaymentMethod $paymentMethod)
    {
        $this->hasPermissionTo('delete payment method');
        $paymentMethod->delete();

        noty()->success('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new PaymentMethodExport, 'PaymentMethod.xlsx');
    }
}
