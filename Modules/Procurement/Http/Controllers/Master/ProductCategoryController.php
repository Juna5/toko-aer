<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\ProductCategoryExport;
use Modules\Procurement\DataTables\ProductCategoryDatatable;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Http\Requests\ProductCategoryRequest;
use Maatwebsite\Excel\Facades\Excel;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProductCategoryDatatable $datatable)
    {
        $this->hasPermissionTo('view product category');
        return $datatable->render('procurement::master.product-category.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add product category');
        return view('procurement::master.product-category.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProductCategoryRequest $request)
    {
        $this->hasPermissionTo('add product category');
        ProductCategory::create($request->all());

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.product-category.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(ProductCategory $productCategory)
    {
        $this->hasPermissionTo('edit product category');
        return view('procurement::master.product-category.edit', compact('productCategory'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ProductCategoryRequest $request, ProductCategory $productCategory)
    {
        $this->hasPermissionTo('edit product category');

        $productCategory->code          = $request->code;
        $productCategory->name          = $request->name;
        $productCategory->description   = $request->description;
        $productCategory->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.product-category.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(ProductCategory $productCategory)
    {
        $this->hasPermissionTo('delete product category');
        $productCategory->delete();

        noty()->danger('Cool!', 'Your data has been deleted');
        
        return back();
    }

    public function export()
    {
        return Excel::download(new ProductCategoryExport, 'ProductCategory.xlsx');
    }
}
