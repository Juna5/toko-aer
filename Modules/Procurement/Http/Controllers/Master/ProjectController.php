<?php

namespace Modules\Procurement\Http\Controllers\Master;

use DB;
use Maatwebsite\Excel\Facades\Excel;
Use App\Http\Controllers\Controller;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\Project;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\PurchaseOrderApproval;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Procurement\Exports\ProjectExport;
use Modules\Procurement\DataTables\ProjectDatatable;
use Modules\Procurement\Http\Requests\ProjectRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProjectDatatable $datatable)
    {
        $this->hasPermissionTo('view project');
        return $datatable->render('procurement::master.project.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {   
        $this->hasPermissionTo('add project');

        $projectManager = Employee::all();
        $customer       = Customer::all();

        return view('procurement::master.project.create', compact('projectManager', 'customer'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProjectRequest $request)
    {
        $this->hasPermissionTo('add project');

        $project = Project::create([
            'name'          => $request->name,
            'order_date'    => '2020-08-13',
            'delivery_date' => '2020-08-13',
            'employee_id'   => $request->employee_id,
            'customer_id'   => $request->customer_id,
            'status'        => $request->status,
            'finish'        => $request->finish,
        ]);
        
        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.project.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $histories = PurchaseOrderApproval::where('purchase_order_id', $id)
            ->get()
            ->sortBy('id');
        if ($row) {
            $media = $row->getMedia('purchase_order');
        } else {
            $media = [];
        }

        return view('procurement::master.project.tab.modals.purchase_order.detail', compact('row', 'items', 'histories', 'media'));
    }

    public function showSO($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)
            ->get()
            ->sortBy('id');
        if ($row) {
            $media = $row->getMedia('sales_order');
        } else {
            $media = [];
        }

        return view('procurement::master.project.tab.modals.sales_order.detail', compact('row', 'items', 'approvals', 'media'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(ProjectDatatable $datatable, $id)
    {
        $this->hasPermissionTo('edit project');
        $project        = Project::find($id);
        $projectManager = Employee::all();
        $customer       = Customer::all();

        $totalPO        = DB::table('purchase_orders')
            ->join('projects', 'purchase_orders.project_id', '=', 'projects.id')
            ->whereNull('purchase_orders.deleted_at')  
            ->where('project_id', $id)   
            ->sum('purchase_orders.total');

        $purchaseOrder  = PurchaseOrder::with('project', 'items')->whereHas('items', function ($q) use ($id) {
            $q->whereProjectId($id);
        })->get();

        $totalSO        = DB::table('sales_orders')
            ->join('projects', 'sales_orders.project_id', '=', 'projects.id')
            ->whereNull('sales_orders.deleted_at')
            ->where('project_id', $id)
            ->sum('sales_orders.total');

        $salesOrder     = SalesOrder::with('project', 'items')->whereHas('items', function ($q) use ($id) {
            $q->whereProjectId($id);
        })->get();
        
        return $datatable->render('procurement::master.project.edit', compact('project', 'projectManager', 'customer', 'totalPO', 'purchaseOrder', 'totalSO', 'salesOrder'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ProjectRequest $request, $id)
    {
        $this->hasPermissionTo('edit project');

        $project = Project::findOrFail($id);

        $project->name          = $request->name;
        $project->order_date    = '2020-08-13';
        $project->delivery_date = '2020-08-13';
        $project->employee_id   = $request->employee_id;
        $project->customer_id   = $request->customer_id;
        $project->status        = $request->status;
        $project->finish        = $request->finish;
        $project->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.project.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete project');

        $project = Project::find($id);
        $project->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return redirect()->route('procurement.master.project.index');
    }

    public function export()
    {
        return Excel::download(new ProjectExport, 'Project.xlsx');
    }
}
