<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\TaxExport;
use Modules\Procurement\DataTables\TaxDatatable;
use Modules\Procurement\Entities\Tax;
use Modules\Procurement\Http\Requests\TaxRequest;
use Maatwebsite\Excel\Facades\Excel;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(TaxDatatable $datatable)
    {
        $this->hasPermissionTo('view tax');
        return $datatable->render('procurement::master.tax.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add tax');
        return view('procurement::master.tax.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(TaxRequest $request)
    {
        $this->hasPermissionTo('add tax');
        Tax::create([
            'code'              => $request->code,
            'name'              => $request->name,
            'description'       => $request->description,
            'rate'              => $request->rate,
            'from_date'         => $request->from_date,
            'until_date'        => $request->until_date,

        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.tax.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Tax $tax)
    {
        $this->hasPermissionTo('edit tax');
        return view('procurement::master.tax.edit', compact('tax'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(TaxRequest $request, Tax $tax)
    {
        $this->hasPermissionTo('edit tax');

        $tax->code          = $request->code;
        $tax->name          = $request->name;
        $tax->description   = $request->description;
        $tax->rate         = $request->rate;
        $tax->from_date   = $request->from_date;
        $tax->until_date   = $request->until_date;
        $tax->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.tax.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Tax $tax)
    {
        $this->hasPermissionTo('delete tax');
        $tax->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new TaxExport, 'Tax.xlsx');
    }
}
