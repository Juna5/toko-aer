<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\ProductExport;
use Modules\Procurement\DataTables\ProductDatatable;
use Modules\Procurement\DataTables\ProductPriceDatatable;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Entities\ProductSubCategory;
use Modules\Procurement\Entities\ProductPrice;
use Modules\Procurement\Entities\Supplier;
use Modules\Procurement\Entities\Uom;
use Modules\Procurement\Entities\WeightClass;
use Modules\Procurement\Entities\Location;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\PurchaseOrderApproval;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Procurement\Http\Requests\ProductRequest;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProductDatatable $datatable)
    {
        $this->hasPermissionTo('view product');
        return $datatable->render('procurement::master.product.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add product');
        return view('procurement::master.product.create', [
            'productCategory'    => ProductCategory::all(),
            'productSubCategory' => ProductSubCategory::all(),
            'location'           => Location::all(),
            'priceCategory'      => PriceCategory::all(),
            'warehouseMaster'    => WarehouseMaster::all(),
            'supplier'           => Supplier::all(),
            'uom'                => Uom::all(),
            'weightClass'        => WeightClass::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {
        $this->hasPermissionTo('add product');
            
            if(!$request->uom_id || !$request->code || !$request->code){
                noty()->success('Oops!', 'Name, Code and Uom cannot be empty');
                $row = null;
                return back();
            }elseif($request->uom_id){
                 $row = Product::create([
                    'code'                    => request('code'),
                    'name'                    => request('name'),
                    'brand'                   => request('brand'),
                    'product_category_id'     => request('product_category_id'),
                    'product_sub_category_id' => request('product_sub_category_id'),
                    'price_category_id'       => request('price_category_id'),
                    'uom_id'                  => request('uom_id'),
                    'dimension'               => request('dimension'),
                    'weight'                  => request('weight'),
                    'price'                   => request('price'),
                    'discount'                => request('discount'),
                    'description'             => request('description'),
                    'warehouse_id'            => request('warehouse_id'),
                    'supplier_id'             => request('supplier_id'),
                    'model'                   => request('model'),
                    'sku'                     => request('sku'),
                    'weight_class_id'         => request('weight_class_id'),
                    'manufacturer'            => request('manufacturer'),
                    'detail_description'      => request('detail_description'),
                    'size_packing'            => request('size_packing'),
                    'require_shipping'        => request('require_shipping'),
                    'length_class'            => request('length_class'),
                    'related_product'         => request('related_product'),
                ]);
            }
                
        if (request()->hasFile('file')) {
            foreach (request()->file as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('productImage');
            }
        }

        if($row){
            noty()->success('Yeay!', 'Your entry has been added successfully');
            return redirect()->route('procurement.master.product.index');
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $histories = PurchaseOrderApproval::where('purchase_order_id', $id)
            ->get()
            ->sortBy('id');
        if ($row) {
            $media = $row->getMedia('purchase_order');
        } else {
            $media = [];
        }

        return view('procurement::master.product.tab.modals.purchase_order.detail', compact('row', 'items', 'histories', 'media'));
    }

    public function showSO($id)
    {
        $row = SalesOrder::findOrFail($id);
        $items = SalesOrderItem::where('sales_order_id', $id)->get();
        $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
        if ($row) {
            $media = $row->getMedia('sales_order');
        } else {
            $media = [];
        }

        return view('procurement::master.product.tab.modals.sales_order.detail', compact('row', 'items', 'approvals', 'media'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(ProductPriceDatatable $datatable, $id)
    {
        $this->hasPermissionTo('edit product');
        $product            = Product::find($id);
        $media              = $product->getMedia('productImage');
        $document           = $product->getMedia('productDocument');
        $productCategory    = ProductCategory::all();
        $productSubCategory = ProductSubCategory::all();
        $location           = Location::all();
        $priceCategory      = PriceCategory::all();
        $warehouseMaster    = WarehouseMaster::all();
        $supplier           = Supplier::all();
        $uom                = Uom::all();
        $weightClass        = WeightClass::all();
        $purchaseOrder      = PurchaseOrder::with('employee')->whereHas('items', function ($q) use ($id) {
            $q->whereProductId($id);
        })->orderBy('id', 'DESC')
            ->get();

        $salesOrder = SalesOrder::with('employee')->whereHas('items', function ($q) use ($id) {
            $q->whereProductId($id);
        })->orderBy('id', 'DESC')
            ->get();

        $stock = Stock::with('warehouse')->where('product_id', $id)->get();

        $priceHistory = SalesOrderItem::with('product', 'salesOrder')->whereHas('salesOrder', function ($q) use ($id) {
            $q->whereProductId($id);
        })->orderBy('id', 'DESC')
            ->take(20)
            ->get();

        $priceHistoryPo = PurchaseOrderItem::with('product', 'purchaseOrder')->whereHas('purchaseOrder', function ($q) use ($id) {
            $q->whereProductId($id);
        })->orderBy('id', 'DESC')
            ->take(20)
            ->get();

        return $datatable->render('procurement::master.product.edit', compact('document', 'salesOrder', 'purchaseOrder', 'product', 'media', 'productCategory', 'productSubCategory', 'location', 'priceCategory', 'warehouseMaster', 'supplier', 'uom', 'stock', 'priceHistory', 'priceHistoryPo','weightClass'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->hasPermissionTo('edit product');
        if($request->dimension || $request->weight || $request->weight_class_id || $request->detail_description){
        $row = tap(Product::find($id))->update(request()->except('file', 'document', 'documents'))->fresh();
        }else{
            $row = tap(Product::find($id))->update(request()->except('file', 'document', 'documents','weight','weight_class_id','dimension','detail_description'))->fresh();
        }
        if (request()->hasFile('file')) {
            foreach (request()->file as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('productImage');
            }
        }
        if (request()->hasFile('document')) {
            foreach (request()->document as $key) {
                $row->addMedia($key)->preservingOriginal()->toMediaCollection('productDocument');
            }
        }

        if (request()->documents) {
             noty()->success('Yeay!', 'Your entry has been edited successfully');
            return redirect()->back();
        } else {
            noty()->success('Yeay!', 'Your entry has been updated successfully');
            return redirect()->route('procurement.master.product.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete product');

        Product::withoutGlobalScopes()->find($id)->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new ProductExport, 'Product.xlsx');
    }

    public function subCategory()
    {
        $id = request()->id;

        $subCategory = ProductSubCategory::where('product_category_id', $id)->get();
         $return = '<option value="">--Please Select--</option>';
        foreach ($subCategory as $temp) {
            $return .= "<option value='$temp->id'>$temp->name</option>";
        }

        return $return;
    }


}
