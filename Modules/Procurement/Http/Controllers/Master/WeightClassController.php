<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\DataTables\WeightClassDatatable;
use Modules\Procurement\Entities\WeightClass;
use Modules\Procurement\Exports\WeightClassExport;
use Modules\Procurement\Http\Requests\WeightClassRequest;
use Maatwebsite\Excel\Facades\Excel;

class WeightClassController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(WeightClassDatatable $datatable)
    {
        $this->hasPermissionTo('view weight class');
        
        return $datatable->render('procurement::master.weight_class.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add weight class');
        return view('procurement::master.weight_class.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(WeightClassRequest $request)
    {
        $this->hasPermissionTo('add weight class');

        WeightClass::create([
            'code'              => $request->name,
            'name'              => $request->name,
            'description'       => $request->description,
            'active'            => $request->active
        ]);

        
        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.weight_class.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(WeightClass $weightClass)
    {
        $this->hasPermissionTo('edit weight class');
        return view('procurement::master.weight_class.edit', compact('weightClass'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(WeightClassRequest $request, WeightClass $weightClass)
    {
        $this->hasPermissionTo('edit weight class');

        $weightClass->code          = $request->name;
        $weightClass->name          = $request->name;
        $weightClass->description   = $request->description;
        $weightClass->active        = $request->active;
        $weightClass->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.weight_class.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(WeightClass $weightClass)
    {
        $this->hasPermissionTo('delete weight class');
        $weightClass->delete();
    
        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new WeightClassExport, 'WeightClass.xlsx');
    }
}
