<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\BusinessRoleExport;
use Modules\Procurement\DataTables\BusinessRoleDataTable;
use Modules\Procurement\Entities\BusinessRole;
use Modules\Procurement\Http\Requests\BusinessRoleRequest;
use Maatwebsite\Excel\Facades\Excel;

class BusinessRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(BusinessRoleDataTable $datatable)
    {
        $this->hasPermissionTo('view business role');
        return $datatable->render('procurement::master.business_role.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add business role');
        return view('procurement::master.business_role.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(BusinessRoleRequest $request)
    {
        $this->hasPermissionTo('add business role');
        BusinessRole::create([
            'name'              => $request->name,
            'description'       => $request->description,
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.business_role.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(BusinessRole $businessRole)
    {
        $this->hasPermissionTo('edit business role');
        return view('procurement::master.business_role.edit', compact('businessRole'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(BusinessRoleRequest $request, BusinessRole $businessRole)
    {
        $this->hasPermissionTo('edit business role');
        $businessRole->name          = $request->name;
        $businessRole->description   = $request->description;
        $businessRole->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.business_role.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(BusinessRole $businessRole)
    {
        $this->hasPermissionTo('delete business role');
        $businessRole->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new BusinessRoleExport, 'BusinessRole.xlsx');
    }
}
