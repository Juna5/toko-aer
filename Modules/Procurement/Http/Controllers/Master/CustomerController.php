<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\CustomerExport;
use Modules\Procurement\DataTables\CustomerDatatable;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\BusinessRole;
use Modules\Procurement\Entities\BusinessIndustry;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Finance\Entities\Invoice;
use Modules\Finance\Entities\InvoiceDetailItem;
use Modules\Procurement\Http\Requests\CustomerRequest;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
        /**
         * Display a listing of the resource.
         * @return Response
         */
        public function index(CustomerDatatable $datatable)
        {
                $this->hasPermissionTo('view customer');
                return $datatable->render('procurement::master.customer.index');
        }

        /**
         * Show the form for creating a new resource.
         * @return Response
         */
        public function create()
        {
                $this->hasPermissionTo('add customer');
                $priceCategory = PriceCategory::all();
                $businessIndustry = BusinessIndustry::all();
                $salesPerson = Employee::all();
                return view('procurement::master.customer.create', compact('businessIndustry', 'priceCategory', 'salesPerson'));
        }

        /**
         * Store a newly created resource in storage.
         * @param Request $request
         * @return Response
         */
        public function store(CustomerRequest $request)
        {
                $this->hasPermissionTo('add customer');
                $customer = Customer::create([
                        'company_name'         => $request->company_name,
                        'roc_no'               => $request->roc_no,
                        'employee_id'          => $request->employee_id,
                        'address'              => $request->address,
                        'email'                => $request->email,
                        'phone'                => $request->phone,
                        'fax'                  => $request->fax,
                        'pic_1'                => $request->pic_1,
                        'pic_position_1'       => $request->pic_position_1,
                        'pic_phone_1'          => $request->pic_phone_1,
                        'pic_2'                => $request->pic_2,
                        'pic_position_2'       => $request->pic_position_2,
                        'pic_phone_2'          => $request->pic_phone_2,
                        'credit_limit'         => $request->credit_limit,
                        'deposit'              => $request->deposit,
                        'price_category_id'  => $request->price_category_id,
                        'description'          => $request->description,
                        'business_industry_id' => $request->business_industry_id,
                        'next_contact_date'    => $request->next_contact_date,
                        'last_contact_date'    => $request->last_contact_date,
                        'website'              => $request->website,
                        'block'                => $request->block,
                ]);

                if (request()->hasFile('attachment')) {
                foreach (request()->attachment as $key) {
                    $customer->addMedia($key)->preservingOriginal()->toMediaCollection('customer');
                }
            }

                noty()->success('Yeay!', 'Your entry has been added successfully');
                return redirect()->route('procurement.master.customer.index');
        }

        /**
         * Show the specified resource.
         * @param int $id
         * @return Response
         */
        public function show($id)
        {
            $row = SalesOrder::findOrFail($id);
            $items = SalesOrderItem::where('sales_order_id', $id)->get();
            $approvals = SalesOrderApproval::where('sales_order_id', $id)->orderBy('id', 'asc')->get();
            if($row){
                $media = $row->getMedia('sales_order');
            }else{
                $media = [];
            }

            return view('procurement::master.customer.modals.sales_order.detail', compact('row', 'items', 'approvals', 'media'));
        }

        public function showInvoice($id)
        {
            $row = Invoice::findOrFail($id);
            $items = InvoiceDetailItem::where('invoice_id', $row->id)->first();


            return view('procurement::master.customer.modals.invoice.detail', compact('row', 'items'));
        }

        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return Response
         */
        public function edit(Customer $customer)
        {
                $this->hasPermissionTo('edit customer');
                $priceCategory = PriceCategory::all();;
                $businessIndustry = BusinessIndustry::all();
                $salesPerson = Employee::all();
                $salesOrder = SalesOrder::where('customer_id', $customer->id)->orderBy('date','DESC')->get();
                // $invoice = Invoice::whereHas('salesOrder', function($q) use($customer) {
                //     $q->where('customer_id', $customer->id);
                // })
                // ->orWhereHas('deliveryOrder', function($q) use($customer) {
                //     $q->where('customer_id', $customer->id);
                // })
                // ->get();
                $document  = $customer ? $customer->getMedia('customerDocument') : '';

                $media = $customer ? $customer->getMedia('customer') : '';

                return view('procurement::master.customer.edit', compact('salesOrder', 'businessIndustry', 'salesPerson', 'customer', 'priceCategory', 'media','document'));
        }

        /**
         * Update the specified resource in storage.
         * @param Request $request
         * @param int $id
         * @return Response
         */
        public function update(CustomerRequest $request, Customer $customer)
        {
                $this->hasPermissionTo('edit customer');

                $customer->company_name         = $request->company_name;
                $customer->employee_id          = $request->employee_id;
                $customer->roc_no               = $request->roc_no;
                $customer->address              = $request->address;
                $customer->email                = $request->email;
                $customer->phone                = $request->phone;
                $customer->fax                  = $request->fax;
                $customer->pic_1                = $request->pic_1;
                $customer->pic_position_1       = $request->pic_position_1;
                $customer->pic_phone_1          = $request->pic_phone_1;
                $customer->pic_2                = $request->pic_2;
                $customer->pic_position_2       = $request->pic_position_2;
                $customer->pic_phone_2          = $request->pic_phone_2;
                $customer->price_category_id  = $request->price_category_id;
                $customer->credit_limit         = $request->credit_limit;
                $customer->deposit              = $request->deposit;
                $customer->description          = $request->description;
                $customer->business_industry_id = $request->business_industry_id;
                $customer->next_contact_date    = $request->next_contact_date;
                $customer->last_contact_date    = $request->last_contact_date;
                $customer->website              = $request->website;
                $customer->save();

                if (request()->hasFile('attachment')) {
                    foreach (request()->attachment as $key) {
                        $customer->addMedia($key)->preservingOriginal()->toMediaCollection('customer');
                    }
                }

                noty()->success('Yeay!', 'Your entry has been updated successfully');
                return redirect()->route('procurement.master.customer.index');
        }

        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return Response
         */
        public function destroy(Customer $customer)
        {
                $this->hasPermissionTo('delete customer');
                $customer->delete();

                noty()->danger('Cool!', 'Your entry has been deleted');
                return back();
        }

        public function export()
        {
            return Excel::download(new CustomerExport, 'Customer.xlsx');
        }

        public function saveDocument($id)
        {
            $customer = Customer::find($id);

            if (request()->hasFile('document')) {
                foreach (request()->document as $key) {
                    $customer->addMedia($key)->preservingOriginal()->toMediaCollection('customerDocument');
                }
            }

            noty()->success('Yeay!', 'Your entry has been edited successfully');
            return redirect()->back();
        }
}
