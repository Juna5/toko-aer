<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\LocationExport;
use Modules\Procurement\DataTables\LocationDatatable;
use Modules\Procurement\Entities\Location;
use Modules\Procurement\Http\Requests\LocationRequest;
use Maatwebsite\Excel\Facades\Excel;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(LocationDatatable $datatable)
    {
        $this->hasPermissionTo('view location');
        return $datatable->render('procurement::master.location.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add location');
        return view('procurement::master.location.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(LocationRequest $request)
    {
        $this->hasPermissionTo('add location');

        Location::create($request->all());
        
        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.location.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Location $location)
    {
        $this->hasPermissionTo('edit location');
        return view('procurement::master.location.edit', compact('location'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(LocationRequest $request, Location $location)
    {
        $this->hasPermissionTo('edit location');

        $location->code         = $request->code;
        $location->name         = $request->name;
        $location->description  = $request->description;
        $location->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.location.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Location $location)
    {
        $this->hasPermissionTo('delete location');
        $location->delete();

        noty()->danger('Cool!', 'Your data has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new LocationExport, 'Location.xlsx');
    }
}
