<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\BusinessIndustryExport;
use Modules\Procurement\DataTables\BusinessIndustryDataTable;
use Modules\Procurement\Entities\BusinessIndustry;
use Modules\Procurement\Http\Requests\BusinessIndustryRequest;
use Maatwebsite\Excel\Facades\Excel;

class BusinessIndustryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(BusinessIndustryDataTable $datatable)
    {
        $this->hasPermissionTo('view business industry');
        return $datatable->render('procurement::master.business_industry.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add business industry');
        return view('procurement::master.business_industry.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(BusinessIndustryRequest $request)
    {
        $this->hasPermissionTo('add business industry');
        BusinessIndustry::create([
            'name'        => $request->name,
            'description' => $request->description,
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.business_industry.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(BusinessIndustry $businessIndustry)
    {
        $this->hasPermissionTo('edit business industry');
        return view('procurement::master.business_industry.edit', compact('businessIndustry'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(BusinessIndustryRequest $request, BusinessIndustry $businessIndustry)
    {
        $this->hasPermissionTo('edit business industry');
        $businessIndustry->name          = $request->name;
        $businessIndustry->description   = $request->description;
        $businessIndustry->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.business_industry.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(BusinessIndustry $businessIndustry)
    {
        $this->hasPermissionTo('delete business industry');
        $businessIndustry->delete();

        noty()->success('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new BusinessIndustryExport, 'BusinessIndustry.xlsx');
    }
}
