<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\PriceCategoryExport;
use Modules\Procurement\DataTables\PriceCategoryDatatable;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Http\Requests\PriceCategoryRequest;
use Maatwebsite\Excel\Facades\Excel;

class PriceCategoryController extends Controller
{
        /**
         * Display a listing of the resource.
         * @return Response
         */
        public function index(PriceCategoryDatatable $datatable)
        {
                $this->hasPermissionTo('view price category');
                return $datatable->render('procurement::master.price-category.index');
        }

        /**
         * Show the form for creating a new resource.
         * @return Response
         */
        public function create()
        {
                $this->hasPermissionTo('add price category');
                return view('procurement::master.price-category.create');
        }

        /**
         * Store a newly created resource in storage.
         * @param Request $request
         * @return Response
         */
        public function store(PriceCategoryRequest $request)
        {
                $this->hasPermissionTo('add price category');
                PriceCategory::create([
                        'code'              => $request->code,
                        'name'              => $request->name,
                        'description'       => $request->description,
                ]);

                noty()->success('Yeay!', 'Your entry has been added successfully');
                return redirect()->route('procurement.master.price_category.index');
        }

        /**
         * Show the specified resource.
         * @param int $id
         * @return Response
         */
        public function show($id)
        {
                return view('procurement::show');
        }

        /**
         * Show the form for editing the specified resource.
         * @param int $id
         * @return Response
         */
        public function edit(PriceCategory $priceCategory)
        {
                $this->hasPermissionTo('edit price category');
                return view('procurement::master.price-category.edit', compact('priceCategory'));
        }

        /**
         * Update the specified resource in storage.
         * @param Request $request
         * @param int $id
         * @return Response
         */
        public function update(PriceCategoryRequest $request, PriceCategory $priceCategory)
        {
                $this->hasPermissionTo('edit price category');

                $priceCategory->code          = $request->code;
                $priceCategory->name          = $request->name;
                $priceCategory->description   = $request->description;
                $priceCategory->save();

                noty()->success('Yeay!', 'Your entry has been updated successfully');
                return redirect()->route('procurement.master.price_category.index');
        }

        /**
         * Remove the specified resource from storage.
         * @param int $id
         * @return Response
         */
        public function destroy(PriceCategory $priceCategory)
        {
                $this->hasPermissionTo('delete price category');
                $priceCategory->delete();
               
                noty()->danger('Cool!', 'Your entry has been deleted');
                return back();
        }

        public function export()
    {
        return Excel::download(new PriceCategoryExport, 'PriceCategory.xlsx');
    }
}
