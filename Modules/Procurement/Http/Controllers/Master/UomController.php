<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\UomExport;
use Modules\Procurement\DataTables\UomDatatable;
use Modules\Procurement\Entities\Uom;
use Modules\Procurement\Http\Requests\UomRequest;
use Maatwebsite\Excel\Facades\Excel;

class UomController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(UomDatatable $datatable)
    {
        $this->hasPermissionTo('view uom');
        
        return $datatable->render('procurement::master.uom.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add uom');
        return view('procurement::master.uom.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(UomRequest $request)
    {
        $this->hasPermissionTo('add uom');

        Uom::create([
            'code'              => $request->name,
            'name'              => $request->name,
            'description'       => $request->description,
            'active'            => $request->active
        ]);

        
        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.uom.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Uom $uom)
    {
        $this->hasPermissionTo('edit uom');
        return view('procurement::master.uom.edit', compact('uom'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UomRequest $request, Uom $uom)
    {
        $this->hasPermissionTo('edit uom');

        $uom->code          = $request->name;
        $uom->name          = $request->name;
        $uom->description   = $request->description;
        $uom->active        = $request->active;
        $uom->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.uom.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(Uom $uom)
    {
        $this->hasPermissionTo('delete uom');
        $uom->delete();
    
        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new UomExport, 'Uom.xlsx');
    }
}
