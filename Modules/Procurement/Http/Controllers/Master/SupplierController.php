<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\SupplierExport;
use Modules\Procurement\Entities\Supplier;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\Procurement\Entities\PurchaseOrderItem;
use Modules\Procurement\Entities\PurchaseInvoice;
use Modules\Procurement\Entities\PurchaseInvoiceDetailItem;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Procurement\Entities\PurchaseOrderApproval;
use Modules\Procurement\DataTables\SupplierDatatable;
use Modules\Procurement\Http\Requests\SupplierRequest;
use Maatwebsite\Excel\Facades\Excel;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(SupplierDatatable $datatable)
    {
        $this->hasPermissionTo('view supplier');
        return $datatable->render('procurement::master.supplier.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add supplier');
        $priceCategory = PriceCategory::all();
        $currency = Currency::all();
        $paymentTerm = PaymentTerm::all();
        return view('procurement::master.supplier.create', compact('priceCategory', 'currency', 'paymentTerm'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(SupplierRequest $request)
    {
        $this->hasPermissionTo('add supplier');
        $supplier = Supplier::create([
            'company_name'        => $request->company_name,
            'name'                => $request->name,
            'roc'                 => $request->roc,
            'phone'               => $request->phone,
            'fax'                 => $request->fax,
            'email'               => $request->email,
            'address'             => $request->address,
            'description'         => $request->description,
            'price_category_id'   => $request->price_category_id,
            'currency_id'         => $request->currency_id,
            'payment_term_id'     => $request->payment_term_id,
            'deposit'             => $request->deposit,
        ]);

        if (request()->hasFile('attachment')) {
                foreach (request()->attachment as $key) {
                    $supplier->addMedia($key)->preservingOriginal()->toMediaCollection('supplier');
                }
            }

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.supplier.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        $row = PurchaseOrder::findOrFail($id);
        $items = PurchaseOrderItem::where('purchase_order_id', $id)->get();
        $histories = PurchaseOrderApproval::where('purchase_order_id', $id)
            ->get()
            ->sortBy('id');
        if($row){
            $media = $row->getMedia('purchase_order');
        }else{
            $media = [];
        }

        return view('procurement::master.supplier.modals.detail', compact('row', 'items', 'histories', 'media'));
    }

    public function showPurchaseInvoice($id)
        {
            $row = PurchaseInvoice::findOrFail($id);
            $items = PurchaseInvoiceDetailItem::where('invoice_id', $row->id)->first();
            
            return view('procurement::master.supplier.modals.purchase_invoice.detail', compact('row', 'items'));
        }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Supplier $supplier)
    {
        $this->hasPermissionTo('edit supplier');
        $priceCategory = PriceCategory::all();
        $purchaseOrder = PurchaseOrder::with('employee')->where('supplier_id', $supplier->id)->orderBy('date','DESC')->get();
        $purchaseInvoice = PurchaseInvoice::whereHas('purchaseOrder', function($q) use($supplier) {
            $q->where('supplier_id', $supplier->id);
        })->get();

        $currency = Currency::all();
        $paymentTerm = PaymentTerm::all();
        if ($supplier){
             $media = $supplier->getMedia('supplier');

        } else {
            $media = '';
        }

        $document  = $supplier ? $supplier->getMedia('supplierDocument') : '';

        return view('procurement::master.supplier.edit', compact('paymentTerm', 'currency', 'supplier', 'priceCategory', 'media', 'purchaseOrder', 'purchaseInvoice', 'document'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(SupplierRequest $request, Supplier $supplier)
    {
        $this->hasPermissionTo('edit supplier');

        $supplier->company_name         = $request->company_name;
        $supplier->name                 = $request->name;
        $supplier->roc                  = $request->roc;
        $supplier->phone                = $request->phone;
        $supplier->fax                  = $request->fax;
        $supplier->email                = $request->email;
        $supplier->address              = $request->address;
        $supplier->description          = $request->description;
        $supplier->price_category_id    = $request->price_category_id;
        $supplier->currency_id          = $request->currency_id;
        $supplier->payment_term_id      = $request->payment_term_id;
        $supplier->deposit              = $request->deposit;
        $supplier->save();

        if (request()->hasFile('attachment')) {
                foreach (request()->attachment as $key) {
                    $supplier->addMedia($key)->preservingOriginal()->toMediaCollection('supplier');
                }
            }

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.supplier.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete supplier');
        Supplier::findOrFail($id)->delete();
        flash('Your data has been deleted')->error();
        noty()->danger('Cool!', 'Your entry has been added successfully');
        return back();
    }

    public function export()
    {
        return Excel::download(new SupplierExport, 'Supplier.xlsx');
    }

     public function saveDocument($id)
        {
            $supplier = Supplier::find($id);

            if (request()->hasFile('document')) {
                foreach (request()->document as $key) {
                    $supplier->addMedia($key)->preservingOriginal()->toMediaCollection('supplierDocument');
                }
            }
            
            noty()->success('Yeay!', 'Your entry has been edited successfully');
            return redirect()->back();
        }
}
