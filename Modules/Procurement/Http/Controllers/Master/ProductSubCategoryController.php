<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Entities\ProductSubCategory;
use Modules\Procurement\DataTables\ProductSubCategoryDataTable;
use Modules\Procurement\Http\Requests\ProductSubCategoryRequest;

class ProductSubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(ProductSubCategoryDataTable $datatable)
    {   
        $this->hasPermissionTo('view product sub category');
        return $datatable->render('procurement::master.product_sub_category.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {   
        $this->hasPermissionTo('add product sub category');
        $productSubCategory = ProductSubCategory::all();
        $productCategory    = ProductCategory::all();

        return view('procurement::master.product_sub_category.create', compact('productSubCategory', 'productCategory'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(ProductSubCategoryRequest $request)
    {   
        $this->hasPermissionTo('add product sub category');
        ProductSubCategory::create([
            'product_category_id'   => $request->product_category_id,
            'code'                  => $request->code,
            'name'                  => $request->name,
            'description'           => $request->description,
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.product_sub_category.index', ['category' => $request->product_category_id]);
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {   
        $this->hasPermissionTo('edit product sub category');
        $productSubCategory = ProductSubCategory::findOrFail($id);
        $productCategory    = ProductCategory::all();

        return view('procurement::master.product_sub_category.edit', compact('productSubCategory', 'productCategory'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(ProductSubCategoryRequest $request, $id)
    {   
        $this->hasPermissionTo('edit product sub category');
        $productSubCategory = ProductSubCategory::findOrFail($id);

        $productSubCategory->code                = $request->code;
        $productSubCategory->name                = $request->name;
        $productSubCategory->description         = $request->description;
        $productSubCategory->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');

        return redirect()->route('procurement.master.product_sub_category.index', ['category' => $productSubCategory->product_category_id]);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {    
        $this->hasPermissionTo('delete product sub category');
        $productSubCategory = ProductSubCategory::find($id);
        $productSubCategory->delete();
        
        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }
}
