<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use App\User;
use Modules\Procurement\Exports\WarehouseMasterExport;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Procurement\DataTables\WarehouseMasterDatatable;
use Modules\Procurement\Http\Requests\WarehouseMasterRequest;
use Maatwebsite\Excel\Facades\Excel;

class WarehouseMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(WarehouseMasterDatatable $datatable)
    {
        $this->hasPermissionTo('view warehouse master');
        return $datatable->render('procurement::master.warehouse_master.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add warehouse master');
        $users = User::all();
        return view('procurement::master.warehouse_master.create', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(WarehouseMasterRequest $request)
    {
        $this->hasPermissionTo('add warehouse master');
        WarehouseMaster::create([
            'code'           => $request->code,
            'name'           => $request->name,
            'address'        => $request->address,
            'pic'            => $request->pic,
            'phone'          => $request->phone,
            'leader_id'          => $request->leader_id,
            // 'description'    => $request->description,
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.depot_master.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $this->hasPermissionTo('edit warehouse master');
        $warehouseMaster = WarehouseMaster::find($id);
        $users = User::all();
        return view('procurement::master.warehouse_master.edit', compact('warehouseMaster', 'users'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(WarehouseMasterRequest $request, $id)
    {
        $this->hasPermissionTo('edit warehouse master');
        $warehouseMaster = WarehouseMaster::find($id);
        $warehouseMaster->code           = $request->code;
        $warehouseMaster->name           = $request->name;
        $warehouseMaster->address        = $request->address;
        $warehouseMaster->pic            = $request->pic;
        $warehouseMaster->phone          = $request->phone;
        $warehouseMaster->leader_id          = $request->leader_id;
        // $warehouseMaster->description    = $request->description;
        $warehouseMaster->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.depot_master.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete warehouse master');
        $warehouseMaster = WarehouseMaster::find($id);
        $warehouseMaster->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new WarehouseMasterExport, 'WarehouseMaster.xlsx');
    }
}
