<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\DataTables\CustomerTypeDatatable;
use Modules\Procurement\Entities\CustomerType;
use Modules\Procurement\Http\Requests\CustomerTypeRequest;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(CustomerTypeDatatable $datatable)
    {
        $this->hasPermissionTo('view customer type');

        return $datatable->render('procurement::master.customer_type.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add customer type');
        return view('procurement::master.customer_type.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CustomerTypeRequest $request)
    {
        $this->hasPermissionTo('add customer type');

        CustomerType::create([
            'customer'              => $request->customer,
            'reseller'              => $request->reseller,
        ]);


        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.customer_type.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(CustomerType $customerType)
    {
        $this->hasPermissionTo('edit customer type');
        return view('procurement::master.customer_type.edit', compact('customerType'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CustomerTypeRequest $request, CustomerType $customerType)
    {
        $this->hasPermissionTo('edit customer type');

        $customerType->customer          = $request->customer;
        $customerType->reseller          = $request->reseller;
        $customerType->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.customer_type.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(CustomerType $customerType)
    {
        $this->hasPermissionTo('delete customer type');
        $customerType->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }
}
