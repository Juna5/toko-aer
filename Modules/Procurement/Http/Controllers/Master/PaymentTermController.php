<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\PaymentTermExport;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Procurement\DataTables\PaymentTermDatatable;
use Modules\Procurement\Http\Requests\PaymentTermRequest;
use Maatwebsite\Excel\Facades\Excel;

class PaymentTermController extends Controller
{
    /**
    * Display a listing of the resource.
    * @return Response
    */
    public function index(PaymentTermDatatable $datatable)
    {
        $this->hasPermissionTo('view payment term');
        return $datatable->render('procurement::master.payment_term.index');
    }

    /**
    * Show the form for creating a new resource.
    * @return Response
    */
    public function create()
    {
        $this->hasPermissionTo('add payment term');
        return view('procurement::master.payment_term.create');
    }

    /**
    * Store a newly created resource in storage.
    * @param Request $request
    * @return Response
    */
    public function store(PaymentTermRequest $request)
    {
        $this->hasPermissionTo('add payment term');
        PaymentTerm::create([
            'code'           => $request->code,
            'name'           => $request->name,
            'description'    => $request->description,
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.payment_term.index');
    }

    /**
    * Show the specified resource.
    * @param int $id
    * @return Response
    */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
    * Show the form for editing the specified resource.
    * @param int $id
    * @return Response
    */
    public function edit(PaymentTerm $paymentTerm)
    {
        $this->hasPermissionTo('edit payment term');
        return view('procurement::master.payment_term.edit', compact('paymentTerm'));
    }

    /**
    * Update the specified resource in storage.
    * @param Request $request
    * @param int $id
    * @return Response
    */
    public function update(PaymentTermRequest $request, PaymentTerm $paymentTerm)
    {
        $this->hasPermissionTo('edit payment term');

        $paymentTerm->code           = $request->code;
        $paymentTerm->name           = $request->name;
        $paymentTerm->description    = $request->description;
        $paymentTerm->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.payment_term.index');
    }

    /**
    * Remove the specified resource from storage.
    * @param int $id
    * @return Response
    */
    public function destroy(PaymentTerm $paymentTerm)
    {
        $this->hasPermissionTo('delete payment term');
        $paymentTerm->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function export()
    {
        return Excel::download(new PaymentTermExport, 'PaymentTerm.xlsx');
    }
}
