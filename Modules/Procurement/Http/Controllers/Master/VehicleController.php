<?php

namespace Modules\Procurement\Http\Controllers\Master;

use App\Http\Controllers\Controller;
use Modules\Procurement\Exports\VehicleExport;
use Modules\Procurement\Entities\Vehicle;
use Modules\Procurement\Http\Requests\VehicleRequest;
use Modules\Procurement\DataTables\VehicleDatatable;
use Maatwebsite\Excel\Facades\Excel;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(VehicleDatatable $datatable)
    {
        $this->hasPermissionTo('view vehicle');
        $vehicle = Vehicle::all();

        return $datatable->render('procurement::master.vehicle.index', compact('vehicle'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $this->hasPermissionTo('add vehicle');
        return view('procurement::master.vehicle.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(VehicleRequest $request)
    {
        $this->hasPermissionTo('add vehicle');
        Vehicle::create([
           'code'                           => $request->code,
           'name'                           => $request->name,
           'description'                    => $request->description,
           'brand'                          => $request->brand,
           'type'                           => $request->type,
           'car_registration_no'            => $request->car_registration_no,
           'car_registration_expiry_date'   => $request->car_registration_expiry_date, 
        ]);

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('procurement.master.vehicle.index');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('procurement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit(Vehicle $vehicle)
    {
        $this->hasPermissionTo('edit vehicle');

        return view('procurement::master.vehicle.edit', compact('vehicle'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(VehicleRequest $request, $id)
    {
        $this->hasPermissionTo('edit vehicle');
        $vehicle = vehicle::findOrFail($id);
        

        $vehicle->code                           = $request->code;
        $vehicle->name                           = $request->name;
        $vehicle->description                    = $request->description;
        $vehicle->brand                          = $request->brand;
        $vehicle->type                           = $request->type;
        $vehicle->car_registration_no            = $request->car_registration_no;
        $vehicle->car_registration_expiry_date   = $request->car_registration_expiry_date;
        $vehicle->save();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('procurement.master.vehicle.index');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete vehicle');
       
        $vehicle = Vehicle::find($id);
        $vehicle->delete();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return redirect()->route('procurement.master.vehicle.index');
    }

    public function export()
    {
        return Excel::download(new VehicleExport, 'Vehicle.xlsx');
    }
}
