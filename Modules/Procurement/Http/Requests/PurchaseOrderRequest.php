<?php

namespace Modules\Procurement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PurchaseOrderRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'title'             => 'required',
            // 'department_id'     => 'required',
            // 'purchase_requisition_id'    => 'required',
            'date'    => 'required',
            'supplier_id'     => 'required',
            'currency_id'     => 'required',
            'payment_method_id'     => 'required',
            'payment_term_id'     => 'required',
            // 'project_id'     => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
