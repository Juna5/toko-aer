<?php

namespace Modules\Procurement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SupplierRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                  => 'required',
            'company_name'          => 'required',
            'phone'                 => 'required',
            'price_category_id'     => 'required',
            'email'                 => 'required',
            'address'               => 'required',
            'payment_term_id'       => 'required',
            'currency_id'           => 'required',
        ];

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
