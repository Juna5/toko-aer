<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Entities\Location;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Procurement\Entities\PriceCategory;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Entities\Tax;
use Modules\Procurement\Entities\Uom;
use Modules\Procurement\Entities\WarehouseMaster;

class DemoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $uoms = ['Pcs', 'Kg', 'Box', 'Cup'];
        $locations = ['Jakarta', 'Bali', 'Medan', 'Malaysia'];
        $ProductCategories = ['Semen', 'Batu', 'Kayu', 'Pasir'];
        $PriceCategories = ['net 30 days', 'net 10 day'];
        $PaymentTerms = ['net 30 days', 'net 10 day'];
        $PaymentMethods = ['Trf Bank', 'Credit Card', 'Cash'];
        $currencies = ['Rupiah', 'Ringgit'];

        foreach ($uoms as $uom){
            Uom::create([
                'code' => $uom,
                'name' => $uom,
                'description' => $uom,
            ]);
        }

        foreach ($locations as $location){
            Location::create([
                'name' => $location,
                'description' => $location,
            ]);
        }

        foreach ($ProductCategories as $category){
            ProductCategory::create([
                'code' => $category,
                'name' => $category,
                'description' => $category,
            ]);
        }

        foreach ($PriceCategories as $price){
            PriceCategory::create([
                'code' => $price,
                'name' => $price,
                'description' => $price,
            ]);
        }

        foreach ($PaymentTerms as $term){
            PaymentTerm::create([
                'code' => $term,
                'name' => $term,
                'description' => $term,
            ]);
        }

        foreach ($PaymentMethods as $method){
            PaymentMethod::create([
                'code' => $method,
                'name' => $method,
                'description' => $method,
            ]);
        }

        foreach ($currencies as $currency){
            Currency::create([
                'code' => $currency,
                'name' => $currency,
                'description' => $currency,
            ]);
        }

        Tax::create([
            'code' => 'PPN',
            'name' => 'PPN',
            'description' => 'PPN',
            'rate' => '5',
            'from_date' => '2020-01-01',
            'until_date' => '2021-01-01',
        ]);

        WarehouseMaster::create([
           'code' => 'Jakarta',
           'name' => 'Jakarta',
            'address' => 'jalan kedamaian no 666'
        ]);

        WarehouseMaster::create([
            'code' => 'Bali',
            'name' => 'Bali',
            'address' => 'jalan yang lurus no 666'
        ]);
    }
}
