<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PriceGategoryTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Permission::insert([
                        ['guard_name' => 'web', 'name' => 'add price category', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'edit price category', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'delete price category', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'view price category', 'created_at' => now()],
                ]);
        }
}
