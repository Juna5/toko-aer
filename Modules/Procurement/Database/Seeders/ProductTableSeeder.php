<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add product', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit product', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete product', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view product', 'created_at' => now()],
        ]);
    }
}
