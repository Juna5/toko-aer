<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PaymentTermTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add payment term', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit payment term', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete payment term', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view payment term', 'created_at' => now()],
        ]);
    }
}