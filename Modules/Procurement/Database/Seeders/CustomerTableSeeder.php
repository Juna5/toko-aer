<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class CustomerTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Permission::insert([
                        ['guard_name' => 'web', 'name' => 'add customer', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'edit customer', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'delete customer', 'created_at' => now()],
                        ['guard_name' => 'web', 'name' => 'view customer', 'created_at' => now()],
                ]);
        }
}
