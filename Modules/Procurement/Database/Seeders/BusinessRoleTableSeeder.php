<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class BusinessRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add business role', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit business role', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete business role', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view business role', 'created_at' => now()],
        ]);
    }
}
