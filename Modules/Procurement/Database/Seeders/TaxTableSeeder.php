<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class TaxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Permission::insert([
            ['guard_name' => 'web', 'name' => 'add tax', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit tax', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete tax', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view tax', 'created_at' => now()],
        ]);
    }
}
