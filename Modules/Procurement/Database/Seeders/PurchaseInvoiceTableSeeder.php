<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PurchaseInvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add purchase invoice', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit purchase invoice', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete purchase invoice', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view purchase invoice', 'created_at' => now()],
        ]);
    }
}
