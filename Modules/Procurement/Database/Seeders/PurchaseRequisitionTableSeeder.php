<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PurchaseRequisitionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Permission::insert([
            ['guard_name' => 'web', 'name' => 'add purchase requisition', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit purchase requisition', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete purchase requisition', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view purchase requisition', 'created_at' => now()],
        ]);
    }
}
