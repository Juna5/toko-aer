<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class WarehouseMasterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add warehouse master', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit warehouse master', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete warehouse master', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view warehouse master', 'created_at' => now()],
        ]);
    }
}
