<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class WeightClassTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Permission::insert([
            ['guard_name' => 'web', 'name' => 'add weight class', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit weight class', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete weight class', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view weight class', 'created_at' => now()],
        ]);
    }
}
