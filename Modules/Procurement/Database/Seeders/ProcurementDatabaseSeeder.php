<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProcurementDatabaseSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
                Model::unguard();

                $this->call(UomTableSeeder::class);
                $this->call(ProductCategoryTableSeeder::class);
                $this->call(SupplierSeedTableSeeder::class);
                $this->call(PurchaseRequisitionTableSeeder::class);
                $this->call(ProductTableSeeder::class);
                $this->call(PurchaseRequisitionApprovalTableSeeder::class);
                $this->call(CustomerTableSeeder::class);
                $this->call(PriceGategoryTableSeeder::class);
                $this->call(TaxTableSeeder::class);
                $this->call(PaymentTermTableSeeder::class);
                $this->call(WarehouseMasterTableSeeder::class);
                $this->call(PurchaseOrderTableSeeder::class);
                $this->call(PurchaseOrderApprovalTableSeeder::class);
                $this->call(PaymentMethodTableSeeder::class);
                $this->call(CurrencyTableSeeder::class);
                $this->call(VehicleTableSeeder::class);
                $this->call(ProductSubCategoryTableSeeder::class);
                $this->call(ProductPriceTableSeeder::class);
                $this->call(BusinessRoleTableSeeder::class);
                $this->call(BusinessIndustryTableSeeder::class);
                $this->call(ProjectTableSeeder::class);
                $this->call(PurchaseInvoiceTableSeeder::class);
                $this->call(PurchaseReceiptTableSeeder::class);
                $this->call(AccountPayableTableSeeder::class);
                $this->call(PurchaseReturnTableSeeder::class);
                $this->call(DebitNoteTableSeeder::class);
                $this->call(AccountPayableAgingTableSeeder::class);
                $this->call(DemoTableSeeder::class);

                // $this->call(WeightClassTableSeeder::class);
        }
}
