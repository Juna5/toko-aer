<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class PurchaseReturnTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add purchase return', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit purchase return', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete purchase return', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view purchase return', 'created_at' => now()],
        ]);
    }
}
