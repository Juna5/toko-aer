<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class VehicleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add vehicle', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit vehicle', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete vehicle', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view vehicle', 'created_at' => now()],
        ]);
    }
}
