<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class PurchaseOrderApprovalTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add purchase order approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit purchase order approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete purchase order approval', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view purchase order approval', 'created_at' => now()],
        ]);
    }
}
