<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class AccountPayableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'view account payable', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view detail account payable', 'created_at' => now()],
        ]);
    }
}
