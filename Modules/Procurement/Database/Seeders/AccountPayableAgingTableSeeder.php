<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class AccountPayableAgingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'view a/p aging report', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view a/p aging report detail', 'created_at' => now()],
        ]);
    }
}
