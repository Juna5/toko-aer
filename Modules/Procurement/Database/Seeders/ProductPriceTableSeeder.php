<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ProductPriceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add product price', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit product price', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete product price', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view product price', 'created_at' => now()],
        ]);
    }
}
