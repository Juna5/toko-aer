<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class SupplierSeedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add supplier', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit supplier', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete supplier', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view supplier', 'created_at' => now()],
        ]);
    }
}
