<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ProductSubCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add product sub category', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit product sub category', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete product sub category', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view product sub category', 'created_at' => now()],
        ]);
    }
}
