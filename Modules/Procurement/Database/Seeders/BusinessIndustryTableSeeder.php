<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class BusinessIndustryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add business industry', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit business industry', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete business industry', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view business industry', 'created_at' => now()],
        ]);
    }
}
