<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PaymentMethodTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add payment method', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit payment method', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete payment method', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view payment method', 'created_at' => now()],
        ]);
    }
}
