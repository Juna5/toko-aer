<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class CustomerTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Permission::insert([
            ['guard_name' => 'web', 'name' => 'add customer type', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit customer type', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete customer type', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view customer type', 'created_at' => now()],
        ]);
    }
}
