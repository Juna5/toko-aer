<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PurchaseReceiptTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add purchase receipt', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit purchase receipt', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete purchase receipt', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view purchase receipt', 'created_at' => now()],
        ]);
    }
}
