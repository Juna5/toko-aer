<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add project', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit project', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete project', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view project', 'created_at' => now()],
        ]);
    }
}
