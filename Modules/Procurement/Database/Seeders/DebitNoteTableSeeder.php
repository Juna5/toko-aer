<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class DebitNoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add debit note', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit debit note', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete debit note', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view debit note', 'created_at' => now()],
        ]);
    }
}
