<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class UomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Permission::insert([
            ['guard_name' => 'web', 'name' => 'add uom', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit uom', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete uom', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view uom', 'created_at' => now()],
        ]);
    }
}
