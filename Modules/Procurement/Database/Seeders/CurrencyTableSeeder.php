<?php

namespace Modules\Procurement\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add currency', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit currency', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete currency', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view currency', 'created_at' => now()],
        ]);
    }
}
