<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModelToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->string('model')->nullable();
            $table->string('sku')->nullable();
            $table->integer('weight_class_id')->nullable();
            $table->string('manufacturer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropCollumn('model');
            $table->dropCollumn('sku');
            $table->dropCollumn('weight_class_id');
            $table->dropCollumn('manufacturer');
        });
    }
}
