<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_requisition_id');
            $table->string('code_number');
            $table->string('title');
            $table->date('date');
            $table->integer('supplier_id');
            $table->integer('currency_id');
            $table->integer('payment_method_id');
            $table->integer('employee_id');
            $table->enum('status', ['waiting', 'approved', 'rejected']);
            $table->integer('approver_to_go')->nullable();
            $table->string('role_approval')->nullable();
            $table->integer('department_id');
            $table->integer('payment_term_id');
            $table->float('get_total')->nullable();;
            $table->float('discount_all_item')->nullable();
            $table->float('sub_total')->nullable();
            $table->string('tax_id')->nullable();
            $table->float('total')->nullable();

            $table->integer('latest_approver')->nullable();
            $table->boolean('is_print')->default(false);
            $table->text('rejected_reason')->nullable();
            $table->integer('rejected_by')->nullable();
            $table->date('rejected_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_orders');
    }
}
