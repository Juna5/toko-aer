<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessRoleToCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('business_role_id')->nullable();
            $table->integer('business_industry_id')->nullable();
            $table->date('next_contact_date')->nullable();
            $table->date('last_contact_date')->nullable();
            $table->text('next_action')->nullable();
            $table->string('website')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('business_role_id')->nullable();
            $table->dropColumn('business_industry_id')->nullable();
            $table->dropColumn('next_contact_date')->nullable();
            $table->dropColumn('last_contact_date')->nullable();
            $table->dropColumn('next_action')->nullable();
            $table->dropColumn('website')->nullable();
        });
    }
}
