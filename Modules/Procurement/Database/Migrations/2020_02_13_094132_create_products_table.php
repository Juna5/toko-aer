<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('product_category_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('price_category_id')->nullable();
            $table->string('code');
            $table->string('name');
            $table->string('brand')->nullable();
            $table->string('uom_1')->nullable();
            $table->string('uom_2')->nullable();
            $table->double('unit_conversion')->nullable();
            $table->string('dimension')->nullable();
            $table->string('weight')->nullable();
            $table->string('barcode')->nullable();
            $table->string('price')->nullable();
            $table->string('discount')->nullable();
            $table->date('expired_date')->nullable();
            $table->text('description')->nullable();
            $table->integer('warehouse_id')->nullable();
            $table->string('size_packing')->nullable();
            $table->string('require_shipping')->nullable();
            $table->string('length_class')->nullable();
            $table->string('related_product')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
