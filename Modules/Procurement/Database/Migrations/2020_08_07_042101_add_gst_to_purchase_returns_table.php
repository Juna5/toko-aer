<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGstToPurchaseReturnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_returns', function (Blueprint $table) {
            $table->float('subtotal_exclude_discount')->nullable();
            $table->float('discount_all_item')->nullable();
            $table->float('subtotal_include_discount')->nullable();
            $table->float('discount_gst')->nullable();
            $table->float('amount_gst')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_returns', function (Blueprint $table) {
            $table->dropColumn('subtotal_exclude_discount');
            $table->dropColumn('discount_all_item');
            $table->dropColumn('subtotal_include_discount');
            $table->dropColumn('discount_gst');
            $table->dropColumn('amount_gst');
        });
    }
}
