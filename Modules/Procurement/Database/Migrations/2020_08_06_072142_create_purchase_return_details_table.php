<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_return_id')->nullable();
            $table->string('item')->nullable();
            $table->string('uom')->nullable();
            $table->string('qty')->nullable();
            $table->string('price')->nullable();
            $table->string('discount_amount')->nullable();
            $table->string('sub_total')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_return_details');
    }
}
