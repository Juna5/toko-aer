<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('company_name');
            $table->text('address')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('pic_1')->nullable();
            $table->string('pic_position_1')->nullable();
            $table->string('pic_phone_1')->nullable();
            $table->date('pic_dob_1')->nullable();
            $table->string('pic_2')->nullable();
            $table->string('pic_position_2')->nullable();
            $table->string('pic_phone_2')->nullable();
            $table->date('pic_dob_2')->nullable();
            $table->integer('credit_limit')->nullable();
            $table->integer('location_id')->nullable();
            $table->integer('price_category_id')->nullable();
            $table->integer('product_category_id')->nullable();
            $table->text('description')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
