<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditDebitNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_debit_notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('charge_of_account_bank_id')->nullable();
            $table->integer('charge_of_account_id')->nullable();
            $table->integer('supplier_id')->nullable();
            $table->integer('customer_id')->nullable();
            $table->integer('journal_id')->nullable();
            $table->integer('payable_id')->nullable();
            $table->integer('receivable_id')->nullable();
            $table->date('date')->nullable();
            $table->double('amount')->nullable();
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_debit_notes');
    }
}
