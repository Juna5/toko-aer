<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequisitionApprovalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requisition_approvals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_requisition_id');
            $table->integer('employee_id')->nullable();
            $table->string('role')->nullable();
            $table->boolean('is_approved');
            $table->date('date')->nullable();
            $table->text('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requisition_approvals');
    }
}
