<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequisitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requisitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->integer('employee_id');
            $table->string('code_number');
            $table->date('submission_date');
            $table->date('required_date');
            $table->integer('department_id');
            $table->integer('cost_center_id');
            $table->enum('status', ['waiting', 'approved', 'rejected']);
            $table->integer('approver_to_go')->nullable();
            $table->integer('latest_approver')->nullable();
            $table->string('role_approval')->nullable();
            $table->boolean('is_print')->default(false);
            $table->text('rejected_reason')->nullable();
            $table->integer('rejected_by')->nullable();
            $table->date('rejected_at')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requisitions');
    }
}
