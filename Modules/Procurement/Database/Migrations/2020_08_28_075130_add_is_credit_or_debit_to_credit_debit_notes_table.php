<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIsCreditOrDebitToCreditDebitNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credit_debit_notes', function (Blueprint $table) {
            $table->string('is_credit_or_debit')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credit_debit_notes', function (Blueprint $table) {
            $table->dropCollumn('is_credit_or_debit');
        });
    }
}
