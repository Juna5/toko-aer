@extends('procurement::layouts.app')

@section('procurement::title', 'Pembelian')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Pembelian',
'active' => true,
'url' => route('procurement.view')
])
@endsection

@section('procurement::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">

        <!-- <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/purchase_request_2.png') }}" class="img-fluid">
                            <div class=" font-weight-bold">PR</div>
                            <div class="text-muted text-small">
                                @can('view purchase requisition')
                                <a href="{{ route('procurement.purchase_requisition.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view purchase requisition approval')
                                <a href="{{ route('procurement.purchase_requisition.approval.index') }}" class="card-cta">Approval</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view purchase requisition')
                                <a href="{{ route('procurement.purchase_requisition.report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/purchase_order.png') }}" class="">
                            <div class=" font-weight-bold">Pembelian</div>
                            <div class="text-muted text-small">
                                @can('view purchase order')
                                <a href="{{ route('procurement.purchase-order.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view purchase order approval')
                                <a href="{{ route('procurement.purchase-order.approval.index') }}" class="card-cta">Approval</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view purchase order')
                                <a href="{{ route('procurement.purchase-order.report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/purchase_receipt.png') }}" class="">
                            <div class=" font-weight-bold">Purchase Receipt</div>
                            <div class="text-muted text-small">
                                @can('view purchase receipt')
                                <a href="{{ route('procurement.purchase_receipt.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view purchase receipt')
                                <a href="{{ route('procurement.purchase_receipt.report.index') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/purchase_invoice.png') }}" class="">
                            <div class=" font-weight-bold">Purchase Invoice</div>
                            <div class="text-muted text-small">
                                @can('view purchase invoice')
                                <a href="{{ route('procurement.purchase_invoice.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                @endcan
                                @can('view purchase invoice')
                                <a href="{{ route('procurement.purchase_invoice.report.index') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/purchase_return_2.png') }}" class="">
                            <div class=" font-weight-bold">Purchase Return</div>
                            <div class="text-muted text-small">
                                @can('view purchase return')
                                <a href="{{ route('procurement.purchase-return.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                <a href="{{ route('procurement.purchase-return-report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/account_payable.png') }}" class="">
                            <div class=" font-weight-bold">A/P</div>
                            <div class="text-muted text-small">
                                @can('view account payable')
                                <a href="{{ route('procurement.account-payable.index') }}" class="card-cta">View</a>
                                @endcan
                                {{-- <div class="bullet"></div>
                                    <a href="{{ route('procurement.account-payable-report') }}" class="card-cta">Reports</a> --}}
                                @can('view a/p aging report')
                                <div class="bullet"></div>
                                <a href="{{ route('procurement.account_payable_aging.index') }}" class="card-cta">A/P Aging Report</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/ap_deposit.png') }}" class="">
                            <div class=" font-weight-bold">A/P Deposit</div>
                            <div class="text-muted text-small">
                                @can('view payable deposit')
                                    <a href="{{ route('finance.payment.payable-deposit.index') }}" class="card-cta">Entry</a>
                                    <div class="bullet"></div>
                                    <a href="#" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/payable_payment.png') }}" class="">
                            <div class=" font-weight-bold">A/P Payment</div>
                            <div class="text-muted text-small">
                                @can('view payable')
                                <a href="{{ route('finance.payment.payable.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                <a href="{{ route('finance.payment.payable-report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/stock_in.png') }}" class="">
                            <div class=" font-weight-bold">Stock In</div>
                            <div class="text-muted text-small">
                                @can('view stock in')
                                <a href="{{ route('stock.stock-in.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                <a href="{{ route('stock.stock-in-report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/procurement/ap_credit_debit_note.png') }}" class="">
                            <div class="font-weight-bold">A/P CN | DN</div>
                            <div class="text-muted text-small">
                                @can('view debit note')
                                <a href="{{ route('procurement.ap_credit_debit_note.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                <a href="{{ route('procurement.debit_note.report') }}" class="card-cta">Reports</a>
                                @endcan
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
@endsection
