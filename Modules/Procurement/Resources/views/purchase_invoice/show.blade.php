@extends('procurement::layouts.app')

@section('procurement::title', 'Detail')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Invoice',
        'active' => true,
        'url' => route('procurement.purchase_invoice.index')
    ])
@endsection

@section('procurement::breadcrumb-4')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Detail',
        'active' => false,
    ])
@endsection

@section('procurement::content')
    <div class="row">
        @include('procurement::purchase_invoice.partials.header_detail')
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">Purchase Invoice</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="purchase-return-tab2" data-toggle="tab" href="#purchase-return2" role="tab" aria-controls="purchase-return" aria-selected="false">Purchase Return</a>
                        </li>
                        <li class="nav-item">

                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">Payment History</a>
                        </li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            @include('procurement::purchase_invoice.partials.purchase_invoice_detail')
                        </div>
                        <div class="tab-pane fade" id="purchase-return2" role="tabpanel" aria-labelledby="purchase-return">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-hover table-md">
                                                <tr>
                                                    <th class="text-center">
                                                        <i class="fas fa-th"></i>
                                                    </th>
                                                    <th style="text-align:center;">Code Number</th>
                                                    <th style="text-align:center;">Date</th>
                                                    <th style="text-align: center;">Supplier</th>
                                                    <th style="text-align:center;">Action</th>
                                                </tr>                   
                                                @forelse($purchaseReturn as $index => $row)
                                                    <tr>
                                                        <td style="text-align: center;">{{ $index +1 }}</td>
                                                        <td style="text-align:center;">{{ $row->code_number }}</td>
                                                        <td style="text-align:center;">{{ format_d_month_y($row->date) }}</td>
                                                        <td style="text-align:center;">{{ optional($row->supplier)->company_name }}</td>
                                                        <td style="text-align:center;">
                                                            <a href="{{ route('procurement.purchase_invoice.showPurchaseReturn', $row['id']) }}" class="btn btn-info modal-purchase-return">
                                                                Detail
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @empty
                                                    <tr>
                                                        <td colspan="6" style="text-align: center;">Data not found.</td>
                                                    </tr>
                                                @endforelse
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div id="js-history"></div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('procurement::purchase_invoice.partials.modals.purchase_return.index')
    <script>
        const data = {!! json_encode($purchaseInvoice->id) !!}
        function fetchHistory(){
            fetch(`/procurement/purchase_invoice/payment-history/${data}`)
                .then(response => response.text())
                .then(html => {
                    document.querySelector('#js-history').innerHTML = html
                })
                .catch()
        }
        fetchHistory()
    </script>
    <script>
        // show detail data in modal purchase return
        $('.modal-purchase-return').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-purchase-return").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function(response) {
                    $("#modal-purchase-return").find('.modal-body').html(response);
                });

        });
    </script>
@endpush
