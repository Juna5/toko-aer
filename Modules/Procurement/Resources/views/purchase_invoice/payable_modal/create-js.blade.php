<script>
    $('.separator.currency').number(true, 2);
    $('.separator').not('.separator.currency').number(true, 0);
    $('.separator').keyup(function() {
        $(this).next('.separator-hidden').val($(this).val());
    });

    $(document).on('click', '#pay_modal', function(){
        const id = $(this).data('id');

        $.ajax({
            url: `/api/get-purchase-invoice/${id}`,
            type: "GET",
            success: function(result) {
                const customerId = result.purchase_order.supplier_id;
                const remarks = `Payment From, ${result.purchase_order.supplier.company_name}`;
                $('#outstanding').val(result.outstanding);
                $('#amount').val(result.outstanding);
                $('#remarks').val(remarks);
                $('#invoice_id').val(result.id);
                $('#customer_id').val(customerId);
            }
        });

        $.ajax({
            url: `/api/chart-of-account/for-payment`,
            type: "GET",
            success: function(response) {
                if (response.status_code === 200){
                    var option = "<option>Please select</option>"
                    response.data.map(res => {
                        option += `<option value=${res.id}>${res.name}</option>`
                    })
                }

                document.getElementById('chart_of_account').innerHTML = option;
            }
        });

        $("#submit_form").submit(function(e) {
            e.preventDefault();

            const date = $('#date').val();
            const amountData = $('#amount').val();
            const amount = parseInt(amountData);
            const remarks = $('#remarks').val();
            const coa = $( "#chart_of_account option:selected" ).val();
            const invoiceId = $('input#invoice_id').val();
            const customerId = $('input#customer_id').val();
            const outstandingData =  $('#outstanding').val();
            const outstanding = parseInt(outstandingData);

            const journal = {
                journal_date: date,
                description: remarks,
                journal_items: [
                    {
                        chart_of_account_id: coa,
                        debit: 0,
                        credit: amount
                    },
                    {
                        chart_of_account_id: 71,
                        debit: amount,
                        credit: 0
                    }
                ],
                total_debit: amount,
                total_credit: amount,
            };

            const dataInvoice = [{
                invoice_id: invoiceId,
                date,
                amount: outstanding,
                payment_amount: amount,
            }];

            let data = {};
            data['supplier_id'] = customerId;
            data['date'] = date;
            data['chart_of_account_id'] = coa;
            data['remarks'] = remarks;
            data['details'] = dataInvoice;
            data['grand_total'] = amount;
            data['journal'] = journal;

            $.ajax({
                url: '/api/payable-data',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                type: "POST",
                dataType: 'json',
                data: data,
                beforeSend  : function() {
                    $(".form-error").html('').hide();
                    $("#button_edit").val('Loading ...');
                    $("#button_edit").prop("disabled", true);
                },
                error: function (xhr, status) {
                    $('#validation-errors-edit').html('');
                    $.each(xhr.responseJSON.errors, function(key,value) {
                        $('#validation-errors-edit').append('<div class="alert alert-danger">'+value+'</div');
                    });
                },
                success: function(result) {
                    if (result.status_code === 200){
                        toastr.success('Your data has been saved successfully',{
                            // tap to dismiss
                            tapToDismiss: true,
                            // toast class
                            toastClass: 'toast',
                            // duration of animation
                            showDuration: 300,
                            // easing function
                            showEasing: 'swing',
                            // duration of animation
                            hideDuration: 1000,
                            // easing function
                            hideEasing: 'swing',
                            // close animation
                            closeMethod: true,
                            // you can customize icons here
                            iconClasses: {
                                error: 'toast-error',
                                info: 'toast-info',
                                success: 'toast-success',
                                warning: 'toast-warning'
                            },
                            iconClass: 'toast-info',
                            // toast-top-center, toast-bottom-center, toast-top-full-width
                            // toast-bottom-full-width, toast-top-left, toast-bottom-right
                            // toast-bottom-left, toast-top-right
                            positionClass: 'toast-top-right',
                            // set timeOut and extendedTimeOut to 0 to make it sticky
                            timeOut: 5000,
                            // title class
                            titleClass: 'toast-title',
                            // message class
                            messageClass: 'toast-message',
                            // allows HTML content in the toast?
                            escapeHtml: false,
                            // target container
                            target: 'body',
                            // close button
                            closeHtml: '<button type="button">close</button>',
                            // place the newest toast on the top
                            newestOnTop: true,
                            // revent duplicate toasts
                            preventDuplicates: false,
                            // shows progress bar
                            progressBar: false
                        });
                        window.location.href = "/procurement/purchase_invoice";
                    }
                },
                complete: function() {
                    $("#button_edit").val('Submit');
                    $("#button_edit").prop("disabled", false);
                }
            });
        });
    });
</script>