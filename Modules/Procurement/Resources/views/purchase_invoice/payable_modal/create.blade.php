<div class="modal fade" id="modal-global" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-header">
                <legend class="text-center">Create Payable</legend>
            </div>

            <div class="modal-body" id="modal-body">
                <form id="submit_form">
                    <div id="validation-errors-edit"></div>
                    <input type="hidden" id="invoice_id">
                    <input type="hidden" id="customer_id">
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group">
                            <label>Date:</label>
                            <input type="date" id="date" class="form-control m-input" required>
                        </div>
                    </div>
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group">
                            <label>Chart Of Account</label>
                            <select id="chart_of_account" class="form-control m-input" required></select>
                        </div>
                    </div>
                    <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group">
                            <label>Outstanding</label>
                            <input type="text" class="form-control separator currency" style="text-align: right" id="outstanding" placeholder="1.000.000" disabled>
                            <input type="hidden" id="outstanding" class="separator-hidden">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <div class="form-group m-form__group">
                            <label>Amount</label>
                            <input type="text" class="form-control separator currency" id="amount" style="text-align: right" placeholder="1.000.000">
                            <input type="hidden" id="amount" class="separator-hidden">
                        </div>
                    </div>
                    <div class="form-group m-form__group">
                        <div class="form-group m-form__group">
                            <label>Remarks</label>
                            <textarea class="form-control" id="remarks" cols="30" rows="10"></textarea>
                        </div>
                    </div>
                    <input id="button_edit" type="submit" class="btn btn-primary" value="Submit" />
                </form>
            </div>
        </div>
    </div>
</div>
