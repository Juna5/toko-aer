@extends('procurement::layouts.app')

@section('procurement::title', 'Create Purchase Invoice')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Invoice',
        'active' => true,
        'url' => route('procurement.purchase_invoice.index')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Create Purchase Invoice',
        'active' => true,
        'url' => route('procurement.purchase_invoice.create')
    ])
@endsection

@section('procurement::content')
@include('flash::message')
@include('include.error-list')
<div class="section-body">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('procurement.purchase_invoice.index') }}" class="btn btn-warning">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </a>
                </div>
                <div class="card-body">
                    <form action="{{ route('procurement.purchase_invoice.store') }}" method="POST" enctype="multipart/form-data">
                        @include('procurement::purchase_invoice.partials.form', [
                            'purchaseInvoice' => new \Modules\Procurement\Entities\PurchaseInvoice()
                        ])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
    <script>
        $(document).ready(function(){
            var str = $("#po_id").val();
            $.ajax({
                    url: '/api/purchase-order/' + str,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)

                        $('#userTable').show();
                        $('#userTable tbody').empty(); 

                        var len = 0;
                        len = Object.keys(response).length;

                        len != 0 ? $("#btnSubmit").attr("disabled", false) : $("#btnSubmit").attr("disabled", true);

                        const supplier = response.supplier.company_name;
                        $('#supplier').val(supplier)
                        $('#description').val(`Purchase Invoice, ${supplier}`)

                        let items = response.items
                        let markup = items.map(item =>
                            `
                                <tr>
                                     <td align="center">${item.product.name}</td>
                                     <td align="center">${item.uom.code}</td>
                                     <td align="center">${item.qty}</td>
                                     <td align="right">${addCommas(item.price)}</td>
                                     <td align="right">${item.discount_rate == null ? '0' : item.discount_rate}%</td>
                                     <td align="right">${addCommas(item.sub_total)}</td>
                                </tr>
                            `
                        ).join('')
                        let root = document.getElementById('content')
                        root.innerHTML = markup

                        const subtotalExcludeDisc = response.get_total;
                        const dicAllItem = response.discount_all_item;
                        const subTotal = response.sub_total;
                        const tax = response.tax.rate;
                        const gst = (tax / 100) * subTotal;
                        const total = response.total;

                        var tr_str = `
                            <tr>
                                <td colspan="5" align="right">Subtotal Exclude Discount All Items</td>
                                <td  align="right">${addCommas(subtotalExcludeDisc)}</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">Discount All Items</td>
                                <td  align="right">${dicAllItem}%</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">Sub Total</td>
                                <td  align="right">${addCommas(subTotal)}</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">GST ${tax}%</td>
                                <td align="right">${addCommas(gst)}</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">Total</td>
                                <td  align="right"  style="color:#5b35c4; font-weight: bold;">${addCommas(total)}</td>
                            </tr>
                        `
                        const bottom = document.getElementById('bottom')
                        bottom.innerHTML = tr_str
                    }
                })

            $('#home-tab3').on('click', function(){
                $('#userTable tbody').empty();
                $('#bottom').empty();
                $('#po_id').val('');
                $('#supplier').val('');
            });

            $('#po_id').on('change', function(){
                $('#userTable tbody').empty();
                const id = $(this).val();
                $.ajax({
                    url: '/api/purchase-order/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        $('#userTable').show();
                        $('#userTable tbody').empty(); // Empty <tbody>

                        var len = 0;
                        len = Object.keys(response).length;

                        len != 0 ? $("#btnSubmit").attr("disabled", false) : $("#btnSubmit").attr("disabled", true);

                        const supplier = response.supplier.company_name;
                        $('#supplier').val(supplier)
                        $('#description').val(`Purchase Invoice, ${supplier}`)

                        let items = response.items
                        let markup = items.map(item =>
                            `
                                <tr>
                                     <td align="center">${item.product.name}</td>
                                     <td align="center">${item.uom.code}</td>
                                     <td align="center">${item.qty}</td>
                                     <td align="right">${addCommas(item.price)}</td>
                                     <td align="right">${item.discount_rate == null ? '0' : item.discount_rate}%</td>
                                     <td align="right">${addCommas(item.sub_total)}</td>
                                </tr>
                            `
                        ).join('')
                        let root = document.getElementById('content')
                        root.innerHTML = markup

                        const subtotalExcludeDisc = response.get_total;
                        const dicAllItem = response.discount_all_item;
                        const subTotal = response.sub_total;
                        const tax = response.tax.rate;
                        const gst = (tax / 100) * subTotal;
                        const total = response.total;

                        var tr_str = `
                            <tr>
                                <td colspan="5" align="right">Subtotal Exclude Discount All Items</td>
                                <td  align="right">${addCommas(subtotalExcludeDisc)}</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">Discount All Items</td>
                                <td  align="right">${dicAllItem}%</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">Sub Total</td>
                                <td  align="right">${addCommas(subTotal)}</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">GST ${tax}%</td>
                                <td align="right">${addCommas(gst)}</td>
                            </tr>
                            <tr>
                                <td colspan="5" align="right">Total</td>
                                <td  align="right"  style="color:#5b35c4; font-weight: bold;">${addCommas(total)}</td>
                            </tr>
                        `
                        const bottom = document.getElementById('bottom')
                        bottom.innerHTML = tr_str
                    }
                })
            })

            function addCommas(nStr) {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
        });
    </script>
@endpush
