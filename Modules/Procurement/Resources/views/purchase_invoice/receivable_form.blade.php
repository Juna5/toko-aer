@extends('setting::layouts.master')

@section('setting::title', 'Create Receivable')

@section('setting::breadcrumb-2')
    @include('setting::include.breadcrumb', [
    'title' => 'Receivable',
    'active' => true,
    'url' => route('finance.payment.receivable.index')
    ])
@endsection

@section('setting::breadcrumb-3')
    @include('setting::include.breadcrumb', [
    'title' => 'Create Receivable',
    'active' => false,
    'url' => '#'
    ])
@endsection

@section('setting::content')
    @include('flash::message')
    @include('include.error-list')
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <div class="text-right">
                            <a href="{{ route('procurement.purchase_invoice.show', $receive->id) }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                    <form action="{{ route('finance.payment.receivable.store') }}" method="POST">
                        @csrf
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Name</label>
                                    <input type="text" name="supplier_id" class="form-control" value="{{ $receive->company_name }}" readonly>
                                    <input type="hidden" name="supplier_id" value="{{ $poId }}">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Purchase Invoice</label>
                                    <input type="text" class="form-control" value="{{ $receive->invoice_number }}" readonly>
                                    <input type="hidden" name="invoice_id" value="{{ $receive->id }}">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="journal_number">Journal Number</label>
                                    <input type="text" class="form-control" name="journal_number" placeholder="[Auto Generate]">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputPassword4">Journal Date</label>
                                    <input type="date" class="form-control datepicker" name="journal[journal_date]" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">Description</label>
                                    <textarea name="journal[description]" class="form-control" id="description" cols="30" rows="10"></textarea>
                                </div>
                            </div>

                            {{--dynamic form--}}
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <table class="table table-bordered table-hover" id="tab_logic">
                                                <thead>
                                                <tr>
                                                    <th class="text-center"> Chart Of Account </th>
                                                    <th class="text-center"> Description </th>
                                                    <th class="text-center"> Debit </th>
                                                    <th class="text-center"> Credit </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr id='addr0'>
                                                    <td>
                                                        <select name="chart_of_account[]" class="form-control" id="coa_1" required>
                                                            <option value="">Please Select</option>
                                                            @foreach($chartOfAccount as $chart)
                                                                <option value="{{ $chart->id }}">{{ $chart->code .' - '. $chart->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <textarea name="description[]" class="form-control" cols="30" rows="10"></textarea>
                                                    </td>
                                                    <td>
                                                        <input type="number" name='debit[]' placeholder='0.00' class="form-control debit" step="0.00" min="0" value="0" required/>
                                                    </td>
                                                    <td>
                                                        <input type="number" name='credit[]' placeholder='0.00' class="form-control credit" value="0" required/>
                                                    </td>
                                                </tr>
                                                <tr id='addr0'>
                                                    <td>
                                                        <select name="chart_of_account[]" class="form-control" id="coa_2" required>
                                                            <option value="">Please Select</option>
                                                            @foreach($chartOfAccount as $chart)
                                                                <option value="{{ $chart->id }}">{{ $chart->code .' - '. $chart->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <textarea name="description[]" class="form-control" cols="30" rows="10"></textarea>
                                                    </td>
                                                    <td>
                                                        <input type="number" name='debit[]' placeholder='0.00' class="form-control debit" step="0.00" min="0" value="0" required/>
                                                    </td>
                                                    <td>
                                                        <input type="number" name='credit[]' placeholder='0.00' class="form-control credit" value="0" required/>
                                                    </td>
                                                </tr>
                                                <tr id='addr1'></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row clearfix">
                                        <div class="col-md-12">
                                            <button id="add_row" type="button" class="btn btn-info float-left">Add Row</button>
                                            <button id='delete_row' type="button" class="float-right btn btn-warning">Delete Row</button>
                                        </div>
                                    </div>
                                    <div class="row clearfix" style="margin-top:20px">
                                        <div class="col-md-12 float-right" style="display: flex;justify-content: flex-end;justify-items: flex-end">
                                            <div class="mr-5">
                                                <p class="text-center"><b>Total Debit</b></p>
                                                <input type="text" name='total_debit' placeholder='0.00' class="form-control" id="sub_total" />
                                            </div>
                                            <div>
                                                <p class="text-center"><b>Total Credit</b></p>
                                                <input type="text" name='total_credit' placeholder='0.00' class="form-control" id="sub_total_credit" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--end dynamic--}}
                            <hr>
                            <div class="fa-pull-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script>
        $(document).ready(function(){
            var i=1;
            $("#add_row").click(function(){
                b=i-1;
                $('#addr'+i).html($('#addr'+b).html());
                $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
                i++;
            });
            $("#delete_row").click(function(){
                if(i>1){
                    $("#addr"+(i-1)).html('');
                    i--;
                }
                calc();
            });

            $('#tab_logic tbody').on('keyup change',function(){
                calc();
            });
        });

        function calc()
        {
            $('#tab_logic tbody tr').each(function(i, element) {
                var html = $(this).html();
                if(html!='')
                {
                    calc_total();
                }
            });
        }

        function addCommas(nStr) {
            nStr += '';
            var x = nStr.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        }

        function calc_total()
        {
            total=0;
            totalCredit=0;

            $('.debit').each(function() {
                total += parseInt($(this).val());
            });
            $('#sub_total').val(addCommas(total));

            $('.credit').each(function() {
                totalCredit += parseInt($(this).val());
            });
            $('#sub_total_credit').val(addCommas(totalCredit));
        }
    </script>
@endpush
