<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table table-striped" id="sortable-table">
            <thead>
            <tr>
                <th style="text-align: center;">
                    <i class="fas fa-th"></i>
                </th>
                <th style="text-align: center;">Purchase Invoice No.</th>
                <th style="text-align: center;">Payment Date</th>
                <th style="text-align: center;">Amount</th>
            </tr>
            </thead>
            <tbody>
            @forelse($payments as $index => $payment)
                <tr>
                    <td style="text-align: center;">{{ $index +1 }}</td>
                    <td style="text-align: center;">{{ $payment->purchaseInvoice->invoice_number ?? '-' }}</td>
                    <td style="text-align: center;">
                        {{ format_d_month_y($payment->payment_date) }}
                    </td>
                    <td style="text-align: right;">{{ amount_international_with_comma($payment->amount) }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center">Data not found.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
