@csrf
<div class="form-row">
    <div class="form-group col-md-6">
        <label>Invoice No.</label>
        <input type="text" name="invoice_number" class="form-control" placeholder="Auto Generate" disabled>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label>Invoice Date</label>
        <input type="date" name="invoice_date" value="{{ old('invoice_date') ?? $purchaseInvoice->invoice_date ? : now()->format('Y-m-d') }}" class="form-control datepicker" required>
    </div>
    <div class="form-group col-md-6">
        <label>Invoice Due Date</label>
        <input type="date" name="invoice_due_date" value="{{ old('invoice_due_date') ?? $purchaseInvoice->invoice_due_date ? : now()->endOfMonth()->format('Y-m-d') }}" class="form-control datepicker" required>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label>Payment Method</label>
        <select name="payment_method_id" class="form-control select2" required>
            <option value="">Please Select</option>
            @foreach($paymentMethod as $method)
                @if( old('payment_method_id') )
                    <option value="{{ $method->id }}" {{ old('payment_method_id') == $method->id ? 'selected' : ''  }}>{{ $method->code .' - '. $method->name  }}</option>
                @else
                    <option value="{{ $method->id }}" {{ $method->id == $purchaseInvoice->payment_method_id ? 'selected' : ''  }}>{{ $method->code .' - '. $method->name  }}</option>
                @endif
            @endforeach
        </select>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-12">
        <label>Remark</label>
        <textarea name="description" class="form-control" cols="30" rows="10" id="description"></textarea>
    </div>
</div>
{{-- <ul class="nav nav-pills" id="myTab3" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab3" data-toggle="tab" href="#home3" role="tab" aria-controls="home" aria-selected="true">PO</a>
    </li>
</ul> --}}
<div class="tab-content" id="myTabContent2">
    <div class="tab-pane fade show active" id="home3" role="tabpanel" aria-labelledby="home-tab3">
        <div class="form-group">
            <label for="so">PO No.</label>
            <select name="purchase_order_id" class="form-control" id="po_id">
                <option value="">Please Select</option>
                @foreach($purchaseOrder as $purchase)
                    <option value="{{ $purchase->id }}" {{$purchase->id == request()->poId ? 'selected' : ''}}>{{ $purchase->code_number }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>
<div class="form-row">
    <div class="form-group col-md-6">
        <label>Supplier</label>
        <input type="text" name="supplier_name" class="form-control" id="supplier" required>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-striped" id='userTable'>
        <thead>
        <tr>
            <th class="text-center">Item</th>
            <th class="text-center">UoM</th>
            <th class="text-center">Qty</th>
            <th class="text-right">Price</th>
            <th class="text-right">Discount Amount</th>
            <th class="text-right">SubTotal</th>
        </tr>
        </thead>
        <tbody id="content"></tbody>
        <tfoot id="bottom"></tfoot>
    </table>
</div>
<button type="submit" class="btn btn-lg btn-primary float-left" id="btnSubmit" disabled>Save</button>
