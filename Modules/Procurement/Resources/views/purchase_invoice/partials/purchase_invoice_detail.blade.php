<div class="section-body">
    <div class="invoice">
        <div class="invoice-print">
            <div class="row">
                <div class="col-lg-12">
                    <div class="invoice-title">
                        <h2>Purchase Invoice</h2>
                        <div class="invoice-number text-primary"> {{ $purchaseInvoice->invoice_number }}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <strong>Billed From:</strong><br>
                                @if (! is_null($purchaseInvoice->purchase_order_id))
                                    {{ $purchaseInvoice->purchaseOrder->supplier->company_name }}<br>
                                    {{ $purchaseInvoice->purchaseOrder->supplier->address }}<br>
                                    {{-- {{ $purchaseInvoice->purchaseOrder->supplier->location->name }} --}}
                                @endif
                            </address>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <address>
                                <strong>Purchase Invoice Date:</strong><br>
                                {{ \Carbon\Carbon::parse($purchaseInvoice->invoice_date)->isoFormat('MMMM Do YYYY') }}<br><br>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <strong>Payment Method:</strong><br>
                                {{ optional($purchaseInvoice->paymentMethod)->name }}
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="section-title">Order Summary</div>
                    <p class="section-lead">All items here cannot be deleted.</p>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-md">
                            <tr>
                                <th data-width="40">#</th>
                                <th>Item</th>
                                <th class="text-center">Uom</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Discount Amount</th>
                                <th class="text-right">Subtotal</th>
                            </tr>
                            @foreach($purchaseInvoiceDetails as $index => $data)
                                <tr>
                                    <td>{{ $index +1 }}</td>
                                    <td>{{ $data->item }}</td>
                                    <td class="text-center">{{ $data->uom }}</td>
                                    <td class="text-center">{{ $data->qty }}</td>
                                    <td class="text-right">{{ amount_international_with_comma($data->price) }}</td>
                                    <td class="text-right">{{ $data->discount_amount ?? '0' }}%</td>
                                    <td class="text-right">{{ amount_international_with_comma($data->sub_total) }}</td>
                                </tr>
                            @endforeach
                            @php

                            @endphp
                            <tr>
                                <td colspan="6" align="right">Subtotal Exclude Discount All Items</td>
                                <td  align="right">{{ amount_international_with_comma($purchaseInvoiceItem->sub_total_exclude_disc) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Discount All Items (RM)</td>
                                <td  align="right">{{ amount_international_with_comma($purchaseInvoiceItem->discount_all_item) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Sub Total</td>
                                <td  align="right">{{ amount_international_with_comma($purchaseInvoiceItem->subtotal_detail) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">GST {{ $purchaseInvoiceItem->gst }}%</td>
                                <td align="right">{{ amount_international_with_comma($purchaseInvoiceItem->amount_gst) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Total</td>
                                <td  align="right"  style="color:#5b35c4; font-weight: bold;">{{ amount_international_with_comma($purchaseInvoiceItem->total) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="text-md-right">
            <div class="float-lg-left mb-lg-0 mb-3">
                <a href="{{ url()->previous() }}" class="btn btn-warning btn-icon icon-left"><i class="fas fa-arrow-left"></i> Back</a>
            </div>
            <a href="{{ route('procurement.purchase_invoice.pdf', $purchaseInvoice->id) }}" target="_blank" class="btn btn-danger btn-icon icon-left"><i class="fas fa-print"></i> Print</a>
        </div>
    </div>
</div>

