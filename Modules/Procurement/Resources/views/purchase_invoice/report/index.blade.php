@extends('procurement::layouts.app')

@section('procurement::title', 'Purchase Invoice Report')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
        'title' => 'Procurement',
        'active' => true,
        'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Purchase Invoice Report',
    'url' => route('procurement.purchase_invoice.report.index'),
    'active' => true,
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <form action="{{ route('procurement.purchase_invoice.report.index') }}" method="GET">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From Date</label>
                                    <input type="date" name="from_date" class="form-control datepicker"
                                           value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">To Date</label>
                                    <input type="date" name="until_date" class="form-control datepicker"
                                           value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                        <a href="{{ route('procurement.view') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br><br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Purchase Invoice Number</th>
                                <th>Transfered From</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Created By</th>
                                <th>Amount</th>
                                <th>Outstanding</th>
                                <th>Status</th>
                            </tr>
                            @forelse($results as $index => $row)
                                @php
                                    if ($row->payment_status == 'Paid') {
                                        $status = 'badge badge-success';
                                    } elseif ($row->payment_status == 'Paid Partially') {
                                        $status = 'badge badge-warning';
                                    } elseif ($row->payment_status == 'Unpaid') {
                                        $status = 'badge badge-primary';
                                    }
                                @endphp
                                <tr align="center">
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('procurement.purchase_invoice.show', $row->id) }}"
                                           target="_blank">{{ $row->invoice_number }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('procurement.purchase-order.detail', $row->purchaseOrder->id) }}"
                                           target="_blank">{{ $row->purchaseOrder->code_number }}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->invoice_date) }}</td>
                                    <td>{{ format_d_month_y($row->invoice_due_date) }}</td>
                                    <td>{{ $row->employee_id ? optional($row->employee)->name : '-' }}</td>
                                    
                                    <td>{{ amount_international_with_comma($row->total) }}</td>
                                    <td>{{ amount_international_with_comma($row->outstanding) }}</td>
                                    <td><span class="{{$status}}">{{ $row->payment_status }}</span></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(request('from_date') && request('end_date'))
                                {{ $results->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection