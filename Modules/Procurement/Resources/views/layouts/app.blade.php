@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@yield('procurement::title', config('procurement.name'))</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></div>
                @yield('procurement::breadcrumb-2')
                @yield('procurement::breadcrumb-3')
                @yield('procurement::breadcrumb-4')
            </div>
        </div>

        <div class="section-body">
            @yield('procurement::content')
        </div>
    </section>
@endsection
