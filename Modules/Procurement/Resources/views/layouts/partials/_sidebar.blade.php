<div class="col-md-4">
    <div class="card">
        <div class="card-header">
            <h4>Go To</h4>
        </div>
        <div class="card-body">
            <div id="accordion">
                <div class="accordion">

                    <!-- <div class="accordion-body" id="panel-body-1" data-parent="#accordion">
                        <div class="pricing-details">
                            @can('view supplier')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.supplier.index') }}">Supplier</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view customer')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.customer.index') }}">Customer</a>
                                    </div>
                                </div>
                            @endcan
                        </div>
                    </div> -->
                </div>
                <div class="accordion">
                    <div class="accordion-header" role="button" data-toggle="collapse" data-target="#panel-body-2">
                        <h4>General</h4>
                    </div>
                    <div class="accordion-body collapse" id="panel-body-2" data-parent="#accordion">
                        <div class="pricing-details">
                            @can('view uom')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.uom.index') }}">UoM</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view location')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.location.index') }}">Location</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view product category')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.product-category.index') }}">Product Category</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view product')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.product.index') }}">Product</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view price category')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.price_category.index') }}">Price Category</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view tax')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                <a href="{{ route('procurement.master.tax.index') }}">Tax</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view payment term')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.payment_term.index') }}">Payment Term</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view payment method')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.payment_method.index') }}">Payment Method</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view warehouse master')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.warehouse_master.index') }}">Warehouse Master</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view currency')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.currency.index') }}">Currency</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view vehicle')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.vehicle.index') }}">Vehicle</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view business role')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.business_role.index') }}">Business Role</a>
                                    </div>
                                </div>
                            @endcan
                            @can('view business industry')
                                <div class="pricing-item">
                                    <div class="pricing-item-label">
                                        <a href="{{ route('procurement.master.business_industry.index') }}">Business Industry</a>
                                    </div>
                                </div>
                            @endcan
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
