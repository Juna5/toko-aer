@extends('setting::layouts.master')

@section('setting::title', 'PO')

@section('setting::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Pembelian',
    'active' => true,
    'url' => route('procurement.purchase_requisition.view')
    ])
@endsection

@section('setting::content')
    <div class="section-body">
        @include('flash::message')
        <div class="row">
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-clipboard-list"></i>
                    </div>
                    <div class="card-body">
                        <h4>Pembelian Entry</h4>
                        <p>Create Pembelian.</p>
                        @can('view PO')
                            <a href="{{ route('procurement.purchase-order.index') }}" class="card-cta">Go to PO Entry <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-envelope-open-text"></i>
                    </div>
                    <div class="card-body">
                        <h4>Pembelian Approval</h4>
                        <p>All PO approval data.</p>
                        @can('view PO approval')
                            <a href="{{ route('procurement.purchase-order.approval.index') }}" class="card-cta">Go to PO Approval <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-folder-open"></i>
                    </div>
                    <div class="card-body">
                        <h4>Pembelian Reports</h4>
                        <p>Pembelian Reports List </p>
                        @can('view PO')
                            <a href="{{ route('procurement.purchase-order.report') }}" class="card-cta">See in details <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
