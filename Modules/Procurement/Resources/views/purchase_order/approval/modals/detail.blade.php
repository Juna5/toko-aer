<div class="row">
    <div class="col-lg-12">
        <table class="table  table-hover table-md">
            <tr>
                <td width="30%">Employee</td>
                <td width="2%">:</td>
                <td>{{ optional($row->employee)->name }}</td>
            </tr>
            <!-- <tr>
                <td width="30%">Title</td>
                <td width="2%">:</td>
                <td>{{ $row->title }}</td>
            </tr> -->
            @if($row->purchase_requisition_id != 0)
            <tr>
                <td width="30%">PR</td>
                <td width="2%">:</td>
                <td> <a href="{{ route('procurement.purchase_requisition.prDetail', $row->purchase_requisition_id) }}" class="modal-globals" data-toggle="modal" data-target="#modal-globals">{{ optional($row->purchaseRequisition)->code_number }}</a></td>
            </tr>
            @endif
            <tr>
                <td width="30%">Project</td>
                <td width="2%">:</td>
                <td>{{ $row->project_id ? optional($row->project)->name : '-' }}</td>
            </tr>
            <tr>
                <td width="30%">Date</td>
                <td width="2%">:</td>
                <td>{{ format_d_month_y($row->date) }}</td>
            </tr>
            <tr>
                <td width="30%">Supplier</td>
                <td width="2%">:</td>
                <td>{{ optional($row->supplier)->name }}</td>
            </tr>
            <!-- <tr>
                <td width="30%">Department</td>
                <td width="2%">:</td>
                <td>{{ optional($row->department)->name }}</td>
            </tr> -->
            <tr>
                <td width="30%">Currency</td>
                <td width="2%">:</td>
                <td>{{ optional($row->currency)->name }}</td>
            </tr>
            <tr>
                <td width="30%">Payment Method</td>
                <td width="2%">:</td>
                <td>{{ optional($row->paymentMethod)->name }}</td>
            </tr>
            <tr>
                <td width="30%">Payment Term</td>
                <td width="2%">:</td>
                <td>{{ optional($row->paymentTerm)->name }}</td>
            </tr>
            <tr>
                <td width="30%">Status</td>
                <td width="2%">:</td>
                <td>{{ ucfirst($row->status) }}</td>
            </tr>
            <tr>
                <td width="30%">Final Approver</td>
                <td width="2%">:</td>
                <td>{{ optional($row->latestApprove)->name }}</td>
            </tr>
            <tr>
                <td width="30%">Next Approver</td>
                <td width="2%">:</td>
                <td>{{ $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval) }}</td>
            </tr>
            @if($row->rejected_by)
                <tr>
                    <td width="30%">Rejected By</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->rejectedBy)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Rejected At</td>
                    <td width="2%">:</td>
                    <td>{{ format_d_month_y($row->rejected_at) }}</td>
                </tr>
                <tr>
                    <td width="30%">Rejected Reason</td>
                    <td width="2%">:</td>
                    <td>{{ $row->rejected_reason }}</td>
                </tr>
            @endif
        </table>
        <legend class="text-center">Item</legend>
        <table class="table table-striped table-hover table-md">
            <tr>
                <th>Item Description and Specification</th>
                <th>UoM</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>
                    Dsc (%)<br>
                    Dsc Amount
                </th>
                <th>Description</th>
                <th>Sub Total</th>
            </tr>
            @foreach($items as $item)
            <tr>
                <td>{{ optional($item->product)->name }} </td>
                <td>{{ optional($item->uom)->name }}</td>
                <td align="right">{{ number_format($item->qty) }}</td>
                <td align="right">{{ number_format($item->price) }}</td>
                <td align="{{ $item->discount_amount ? 'right' : 'center' }}">{{ $item->discount_rate ? $item->discount_rate . '%' : '' }}  <br><br> {{ $item->discount_amount ? number_format($item->discount_amount) : '' }}  </td>
                <td>{{ $item->description }}</td>
                <td align="right">{{ number_format($item->sub_total) }}</td>
            </tr>
            @endforeach
            <tr>
                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Subtotal Exclude Discount All Items</td>
                <td align="right">{{ number_format($row->get_total) }}</td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Discount All Items</td>
                <td align="right">
                    {{ number_format($row->discount_all_item) }}
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Sub Total</td>
                <td align="right">
                    {{ number_format($row->sub_total) }}
                </td>
            </tr>
            <tr>
                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">GST  {{ optional($row->tax)->rate }} %</td>
                <td align="right">
                    {{ number_format((optional($row->tax)->rate / 100) *  $row->sub_total) }}
                </td>

            </tr>
            <tr style="font-size:13px; font-weight:bold">
                <td colspan="6" align="right">Total</td>
                <td align="right" style="color:#5b35c4; font-size:20px">
                    {{number_format($row->total) }}
                </td>
            </tr>
        </table>
        <legend class="text-center">Attachment</legend>
        <div class="form-group m-form__group row">
        @foreach($media as $index => $med)
            <div class='form-group col-md-2'>
                <div style="display:block;position:relative;overflow:hidden;width:100%;max-width:100%;height:200px;padding:5px 10px;font-size:14px;line-height:22px;color:#777;background-color:#FFF;background-image:none;text-align:center;">
                    <img src="{{ asset($med->getFullUrl()) }}" data-default-file="{{ asset($med->getFullUrl()) }}" style="top:50%;-webkit-transform:translate(0,-50%);transform:translate(0,-50%);position:relative;max-width:100%;max-height:100%;background-color:#FFF;-webkit-transition:border-color .15s linear;transition:border-color .15s linear;">

                </div>
                <span>
                <br>
                    <a href="{{ asset($med->getFullUrl()) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                        <i class="fa fa-file text-white" ></i> View File
                    </a>
                </span>
            </div>
            @endforeach
        </div>
        <legend class="text-center">History</legend>
        <table class="table table-hover table-bordered">
            <tr>
                <th>Employee</th>
                <th>Status</th>
                <th>Date</th>
                <th>Description</th>
            </tr>
            @forelse($histories as $history)
                <tr>
                    @php
                        if(empty($history->date)){
                          $status = $history->is_approved ? 'Approved' : 'Waiting';
                        }else{
                          $status = $history->is_approved ? 'Approved' : 'Reject';
                        }

                        if($status == 'Approved'){
                           $img = 'check.png';
                        }elseif($status == 'Waiting'){
                            $img = 'circle.png';
                        }else{
                            $img = 'close.png';
                        }

                    @endphp
                    <td><img src="{{asset('images/'.$img)}}" alt=""> {{ $history->employee ? optional($history->employee)->name : ($history->role != null ? $history->role: '-') }} </td>

                    @if(empty($history->date))
                        <td>{{ $history->is_approved ? 'Approved' : 'Waiting'  }}</td>
                    @else
                        <td>{{ $history->is_approved ? 'Approved' : 'Reject'  }}</td>
                    @endif
                    <td>{{ format_d_month_y($history->date)  }}</td>
                    <td>{{ $history->description  }}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="4">Data Not Found.</td>
                </tr>
            @endforelse
        </table>
        <div class="form-group">
            <label for="exampleFormControlTextarea3">Note</label>
            <textarea class="form-control" rows="7" name="description" onkeyup="changeValueDescription(this)"></textarea>
        </div>
    </div>
    <div class="modal-footer" id="modal-footer" style="margin-left: 57%">
        <form action="{{ route('procurement.purchase-order.approval.update', $row->id) }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="approve">
            <input type="hidden" name="description" class="description">
            <button type="submit" class="btn btn-info" id="modal-btn-save">Approved</button>
        </form>
        <form action="{{ route('procurement.purchase-order.approval.update', $row->id) }}" method="POST">
            @csrf
            @method('PUT')
            <input type="hidden" name="reject">
            <input type="hidden" name="description" class="description">
            <button type="submit" class="btn btn-danger" id="modal-btn-save">To Be Revised</button>
        </form>
        <button type="button" class="btn btn-warning" data-dismiss="modal">Back</button>
    </div>
</div>
<script>
    function changeValueDescription(e) {
        let val = e.value
        $('.description').val(val)
    }
</script>

    @include('procurement::purchase_order.approval.modals.index_pr')
    @include('procurement::purchase_order.approval.modals.index')
    <script>
     $(document).on('click', '.modal-globals', function(){
        var url = $(this).attr('href');

          $('#modal-globals').modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function(response) {
                    $("#modal-globals").find('.modal-body').html(response);
                });

        });
        $('.modal-globals').on('show.bs.modal', function () {
            var modalParent = $(this).attr('data-modal-parent');
            $(modalParent).css('opacity', 1);
        });
        $(document).on('click', '#btn-close', function(e){
            $("#modal-globals").modal("hide");
            $("#modal-global").modal("show");
            $('.modal').css('overflow-y', 'auto');
            e.stopPropagation();
        });




    </script>
