
<div class="invoice-number text-primary text-center">{{ $row->code_number }}</div>
<br>
<div class="row">
    <div class="col-lg-12">
        <div class="invoice-title">

            <table class="table  table-hover table-md">
                <tr>
                    <td width="30%">Employee</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->employee)->name }}</td>
                </tr>
                <!-- <tr>
                <td width="30%">Title</td>
                <td width="2%">:</td>
                <td>{{ $row->title }}</td>
            </tr> -->
            <tr>
                <td width="30%">Submission Date</td>
                <td width="2%">:</td>
                <td>{{ format_d_month_y($row->submission_date) }}</td>
            </tr>
            <tr>
                <td width="30%">Required Date</td>
                <td width="2%">:</td>
                <td>{{ format_d_month_y($row->required_date) }}</td>
            </tr>
            <!-- <tr>
            <td width="30%">Department</td>
            <td width="2%">:</td>
            <td>{{ optional($row->department)->name }}</td>
        </tr> -->
        <tr>
            <td width="30%">Cost Center</td>
            <td width="2%">:</td>
            <td>{{ optional($row->costCenter)->name }}</td>
        </tr>
        <tr>
            <td width="30%">Status</td>
            <td width="2%">:</td>
            <td>{{ ucfirst($row->status) }}</td>
        </tr>
        <tr>
            <td width="30%">Final Approver</td>
            <td width="2%">:</td>
            <td>{{ optional($row->latestApprove)->name }}</td>
        </tr>
        <tr>
            <td width="30%">Next Approver</td>
            <td width="2%">:</td>
            <td>{{ $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval) }}</td>
        </tr>
        @if($row->rejected_by)
        <tr>
            <td width="30%">Rejected By</td>
            <td width="2%">:</td>
            <td>{{ optional($row->rejectedBy)->name }}</td>
        </tr>
        <tr>
            <td width="30%">Rejected At</td>
            <td width="2%">:</td>
            <td>{{ format_d_month_y($row->rejected_at) }}</td>
        </tr>
        <tr>
            <td width="30%">Rejected Reason</td>
            <td width="2%">:</td>
            <td>{{ $row->rejected_reason }}</td>
        </tr>
        @endif
    </table>

    <br>

    <legend class="text-center">Item</legend>

    <table class="table table-striped table-hover table-md">
        <tr>
            <th>Item</th>
            <th>Description</th>
            <th>UoM</th>
            <th>Qty</th>
        </tr>
        @foreach($items as $item)
        <tr>
            <td>{{ optional($item->product)->name }}</td>
            <td>{{ $item->remarks }}</td>
            <td>{{ optional($item->uom)->name }}</td>
            <td align="center">{{ $item->qty }}</td>
        </tr>
        @endforeach

    </table>
    <br>

    <legend class="text-center">Attachment</legend>
    <div class="form-group m-form__group row">
        @foreach($media as $index => $med)
        <div class='form-group col-md-2'>
            <div style="display:block;position:relative;overflow:hidden;width:100%;max-width:100%;height:200px;padding:5px 10px;font-size:14px;line-height:22px;color:#777;background-color:#FFF;background-image:none;text-align:center;">
                <img src="{{ asset($med->getFullUrl()) }}" data-default-file="{{ asset($med->getFullUrl()) }}" style="top:50%;-webkit-transform:translate(0,-50%);transform:translate(0,-50%);position:relative;max-width:100%;max-height:100%;background-color:#FFF;-webkit-transition:border-color .15s linear;transition:border-color .15s linear;">

            </div>
            <span>
                <br>
                <a href="{{ asset($med->getFullUrl()) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                    <i class="fa fa-file text-white" ></i> View File
                </a>
            </span>
        </div>
        @endforeach
    </div>
    <br>

    <legend class="text-center">History</legend>

    <table class="table table-striped table-hover table-md">
        <tr>
            <th>Employee</th>
            <th>Status</th>
            <th>Date</th>
            <th>Description</th>
        </tr>
        @foreach($approvals as $approve)
        <tr>
            @php
            if(empty($approve->date)){
                $status = $approve->is_approved ? 'Approved' : 'Waiting';
            }else{
                $status = $approve->is_approved ? 'Approved' : 'Reject';
            }

            if($status == 'Approved'){
                $img = 'check.png';
            }elseif($status == 'Waiting'){
                $img = 'circle.png';
            }else{
                $img = 'close.png';
            }

            @endphp
            <td><img src="{{asset('images/'.$img)}}" alt=""> {{ $approve->role}} {{ $approve->role &&  optional($approve->employee)->name ?'-': '' }} {{  optional($approve->employee)->name }}</td>
            <td>{{ $approve->is_approved ? "Approved" : ($row->status == 'rejected' ? 'Rejected' : 'Waiting') }}</td>
            <td>{{ format_d_month_y($approve->date) }}</td>
            <td>{{ $approve->description }}</td>
        </tr>
        @endforeach
    </table>

    <div class="modal-footer" id="modal-footer">
        <button type="button" class="btn btn-warning" id="btn-close" data-target="#modal-globals">Close</button>

    </div>
</div>
</div>
