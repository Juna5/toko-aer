@extends('procurement::layouts.app')

@section('procurement::title', 'PO Approval')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Procurement',
    'active' => true,
    'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'PO Approval',
    'active' => true,
    'url' => route('procurement.purchase-order.approval.index')
    ])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Approval</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">History</a>
                    </li>
                </ul>
            </div>
            <div class="card-body p-0">
                @include('flash::message')
                <div class="tab-content" id="myTabContent">
                     <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="table-responsive">
                            <table class="table table-striped" id="sortable-table">
                                <thead>
                                <tr class="text-center">
                                    <th class="text-center">
                                        <i class="fas fa-th"></i>
                                    </th>
                                    <th>Code Number</th>
                                    <th>Created By</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($purchaseOrders as $row)
                                    <tr class="text-center">
                                        <td>
                                            <div class="sort-handler">
                                                <i class="fas fa-th"></i>
                                            </div>
                                        </td>
                                        <td>{{ $row['code_number'] }}</td>
                                        <td>{{ $row['employee']['name'] }}</td>
                                        <td>{{ format_d_month_y($row['date']) }}</td>
                                        <td>
                                            <a href="{{ route('procurement.purchase-order.approval.show', $row['id']) }}" class="btn btn-info modal-global">
                                                Detail
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="card-body p-10">
                            <div class="table-responsive">
                                {!! $dataTable->table() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
    @include('procurement::purchase_order.approval.modals.index')
    <script>
        // show detail data in modal
        $('.modal-global').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-global").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function(response) {
                    $("#modal-global").find('.modal-body').html(response);
                });

        });
    </script>

@endpush
