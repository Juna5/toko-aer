<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Payment History</h4>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped" id="sortable-table">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Invoice No.</th>
                            <th>Payment Date</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($po->payables as $order)
                        <tr>
                            <td>{{ $loop->index +1 }}</td>
                            <td>{{ $order->purchaseOrder->code_number ?? '-' }}</td>
                            <td>{{ format_d_month_y($order->date)}}</td>
                            <td>{{ amount_international_with_comma($order->amount) }}</td>
                        </tr>
                        @empty
                        <tr><td colspan="6" class="text-center">Data not found.</td></tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
