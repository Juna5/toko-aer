<head>
    <meta charset="utf-8">
    <style media="screen">
    .margin {
        border: 1px solid black;
        /* padding: 2px; */
        font-family: calibri, sans-serif;
        /* padding-bottom: 100px; */
        font-size: 13px;
    }
    div.footer {
        position: fixed;
        bottom: 0;
    }
    .judul{
        margin-left: 10px;
        margin-top: 15px;
        font-size: 20px
    }
    .late{
        margin-left: 10px;
    }
    .topright {
        position: absolute;
        top: 8px;
        right: 32%;
        font-size: 20px;
    }
    .as{
        margin-left: 10px;
        margin-top: 20px;
    }



    </style>
</head>
<body>
    <div class="margin">
        <div class="judul">
            <img src="{{ asset('images/logo.png') }}" width="20%" />
            <br><br><br>
        </div>
        <table class="topright" >
            <tr>
                <td>SengLiy</td>
            </tr>
            <tr>
                <td><b>PO</b></td>
            </tr>
            <tr>
                <td>{{$row->code_number}}</td>
            </tr>
        </table>
        <hr style="">

        <div class="as">
            <table class="table" width="100%">
                @if($row->purchase_requisition_id != 0)
                <tr>
                    <td width="30%">PR</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->purchaseRequisition)->code_number }}</td>
                </tr>
                @endif
                <tr>
                    <td width="30%">Project</td>
                    <td width="2%">:</td>
                    <td>{{ $row->project_id ? optional($row->project)->name : '-' }}</td>
                </tr>
                <tr>
                    <td width="30%">Date</td>
                    <td width="2%">:</td>
                    <td>{{ format_d_month_y($row->date) }}</td>
                </tr>
                <tr>
                    <td width="30%">Supplier</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->supplier)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Department</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->department)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Currency</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->currency)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Payment Method</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->paymentMethod)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Payment Term</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->paymentTerm)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Status</td>
                    <td width="2%">:</td>
                    <td>{{ ucfirst($row->status) }}</td>
                </tr>
                <tr>
                    <td width="30%">Final Approver</td>
                    <td width="2%">:</td>
                    <td>{{ optional($row->latestApprove)->name }}</td>
                </tr>
                <tr>
                    <td width="30%">Next Approver</td>
                    <td width="2%">:</td>
                    <td>{{ $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval) }}</td>
                </tr>
                @if($row->rejected_by)
                    <tr>
                        <td width="30%">Rejected By</td>
                        <td width="2%">:</td>
                        <td>{{ optional($row->rejectedBy)->name }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Rejected At</td>
                        <td width="2%">:</td>
                        <td>{{ format_d_month_y($row->rejected_at) }}</td>
                    </tr>
                    <tr>
                        <td width="30%">Rejected Reason</td>
                        <td width="2%">:</td>
                        <td>{{ $row->rejected_reason }}</td>
                    </tr>
                @endif
            </table>
            <br>
            <table width="100%" cellpadding="5" style="border:1px solid black; margin-left:10px; margin-right:23px; border-collapse: collapse">
                <tr style="border:1px solid black;background-color:papayawhip">
                    <th style="border:1px solid black;margin-left: 10px;text-align:center">Item Description and Specification</th>
                    <th style="border:1px solid black;text-align:center">UoM</th>
                    <th style="border:1px solid black;text-align:center">Quantity</th>
                    <th style="border:1px solid black;text-align:center">Price</th>
                    <th style="border:1px solid black;text-align:center">
                        Dsc (%)<br>
                    Dsc Amount</th>
                    <th style="border:1px solid black;text-align:center">Description</th>
                    <th style="border:1px solid black;text-align:center">Sub Total</th>
                </tr>
                @forelse($items as $item)
                <tr style="border:1px solid black">
                    <td style="border:1px solid black">{{ optional($item->product)->name }} </td>
                    <td style="border:1px solid black">{{ optional($item->uom)->name }}</td>
                    <td style="border:1px solid black" align="right">{{ number_format($item->qty) }}</td>
                    <td style="border:1px solid black" align="right">{{ number_format($item->price) }}</td>
                    <td style="border:1px solid black" align="right">{{ $item->discount_rate ? $item->discount_rate : 0 }} % <br><br> {{ number_format($item->discount_amount) }}  </td>
                    <td style="border:1px solid black">{{ $item->description }}</td>
                    <td style="border:1px solid black" align="right">{{ number_format($item->sub_total) }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" style="border:1px solid black">Data Not Found.</td>
                </tr>
                @endforelse
                <tr>
                    <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Subtotal Exclude Discount All Items</td>
                    <td align="right">{{ number_format($row->get_total) }}</td>
                </tr>
                <tr>
                    <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Discount All Items</td>
                    <td align="right">
                        {{ number_format($row->discount_all_item) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Sub Total</td>
                    <td align="right">
                        {{number_format($row->sub_total) }}
                    </td>
                </tr>
                <tr>
                    <td colspan="6" align="right" style="font-size:13px; font-weight:bold">GST  {{ optional($row->tax)->rate }} %</td>
                    <td align="right">
                        {{ number_format((optional($row->tax)->rate / 100) *  $row->sub_total) }}
                    </td>

                </tr>
                <tr style="font-size:13px; font-weight:bold">
                    <td colspan="6" align="right">Total</td>
                    <td align="right" style="color:#5b35c4; font-size:20px">
                        {{ number_format($row->total) }}
                    </td>
                </tr>


            </table>

            <h3 class="align-center">Approval History</h3>
            <table class="" width="95%" cellpadding="5" style="border:1px solid black; margin-left:10px; margin-right:-10px; border-collapse: collapse">
                <tr style="border:1px solid black;background-color:papayawhip">
                    <th style="border:1px solid black;text-align:center">Employee</th>
                    <th style="border:1px solid black;text-align:center">Status</th>
                    <th style="border:1px solid black;text-align:center">Date</th>
                    <th style="border:1px solid black;text-align:center">Time </th>
                    <th style="border:1px solid black;text-align:center">Description</th>

                </tr>
                @forelse($histories as $row)
                <tr style="border:1px solid black;">
                    <td style="border:1px solid black;">{{ $row->employee->name  }}</td>
                    @if(empty($row->date))
                    <td style="border:1px solid black;">{{ $row->is_approved ? 'Approved' : 'Waiting'  }}</td>
                    @else
                    <td style="border:1px solid black;">{{ $row->is_approved ? 'Approved' : 'Reject'  }}</td>
                    @endif
                    <td style="border:1px solid black;">{{ format_d_month_y($row->date)  }}</td>
                    @if(empty($row->date))
                    <td style="border:1px solid black;"></td>
                    @else
                    <td style="border:1px solid black;">{{ $row->updated_at->format('H:i') }}</td>
                    @endif

                    <td style="border:1px solid black;">{{ $row->description }}</td>

                </tr>
                @empty
                <tr>
                    <td colspan="5" style="border:1px solid black;">Data Not Found.</td>
                </tr>
                @endforelse
            </table>
            <br>
        </div>
    </div>
</body>
