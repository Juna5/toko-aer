@extends('procurement::layouts.app')

@section('procurement::title', 'Pembelian')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Procurement',
    'active' => true,
    'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Pembelian',
    'active' => true,
    'url' => route('procurement.purchase-order.index')
    ])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('procurement::content')
    <div class="card" id="settings-card">
        @include('flash::message')
        <div class="card-body">
            <div class="card">
                <div class="form-group pb-1">
                    <a href="{{ route('procurement.purchase-order.export') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                        <img src="{{asset('images/excel.png')}}" alt="">
                    </a>
                </div>
                <div class="table-responsive">
                    {!! $dataTable->table() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('procurement::purchase_order.entry.modals.index')
    @include('shared.wrapperDatatable')
    <script>
        // show detail data in modal
        $('body').on('click', '.btn-show', function (event) {
            event.preventDefault();

            var me = $(this),
                url = me.attr('href');
            title = me.attr('title');

            $('#modal-title').text(title);
            $('#modal-btn-save').addClass('hide');

            $.ajax({
                url: url,
                dataType: 'html',
                success: function (response) {
                    $('#modal-body').html(response);
                }
            });

            $('#modal').modal('show');
        });
    </script>

    <script>
     $(document).on('click', '#modal_attachment', function(){
        var valId = $(this).data('id');
        $('#id_purchase_order').val(valId);
        var requestUrl = '{{ route('procurement.purchase-order.show', ':id') }}';
        var getData = requestUrl.replace(':id', valId);

          $('#attachment').modal('hide');

        $.ajax({
            url: getData,
            type: "GET",
            success: function(media) {

           $("#attachment").find('.modal-body').html(media)

            }
        });
    });

    $(document).on('click', '#modal_accept', function(){
       var valId = $(this).data('id');
       $('#id_purchase_order').val(valId);
       var requestUrl = '{{ route('procurement.purchase-order.accept', ':id') }}';
       var getData = requestUrl.replace(':id', valId);

         $('#accept').modal('hide');
       $.ajax({
           url: getData,
           type: "GET",
           success: function(formHtml) {
          $("#accept").find('.modal-body').html(formHtml)

           }
       });
   });
    </script>

    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
        $('.dropify').dropify();
        $('.add-image-btn').click(function() {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='20M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
    </script>
@endpush
