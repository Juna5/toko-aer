@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Pembelian')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Procurement',
'active' => true,
'url' => route('procurement.view')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Pembelian',
'active' => true,
'url' => route('procurement.purchase-order.index')
])
@endsection
@section('procurement::breadcrumb-4')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.purchase-order.edit', request()->segment(3))
])
@endsection
@section('procurement::content')

<div class="row" id="pr-create">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.purchase-order.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        @if(request()->resubmit)
                            <purchase-order-edit :id="{{ request()->segment(3) }}" :resubmit="{{request()->resubmit}}"/>
                        @endif
                        <purchase-order-edit :id="{{ request()->segment(3) }}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
