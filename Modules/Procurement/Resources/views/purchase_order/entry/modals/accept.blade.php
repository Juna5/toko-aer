<form action="{{ route('procurement.purchase-order.saveAccept') }}" method="POST" enctype="multipart/form-data" id="modal-body">
    @csrf
    @method('POST')
    <input type="hidden" name="id_purchase_order" id="id_purchase_order" value="{{ $row->id}}">
    <div class="form-group">
        <label for="">Receiver</label>
        <select class="form-control select2" name="receiver_id">
            @foreach($employees as $employee)
            <option value="{{ $employee->id }}">{{ $employee->name }}</option>
            @endforeach
        </select>
    </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="modal-btn-save">Submit</button>
        </div>
</form>
