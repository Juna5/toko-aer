<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Code Number</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Employee</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">PR</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Project</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Supplier</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Currency</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Payment Method</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Payment Term</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Next Approver</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Final Approver</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Status</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Payment Status</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->code_number}}</td>
    <td>{{format_d_month_y($row->date)}}</td>
    <td>{{optional($row->employee)->name}}</td>
    <td>{{optional($row->purchaseRequisition)->code_number}}</td>
    <td>{{optional($row->project)->name}}</td>
    <td>{{$row->date}}</td>
    <td>{{optional($row->supplier)->name}}</td>
    <td>{{optional($row->currency)->name}}</td>
    <td>{{optional($row->paymentMethod)->name}}</td>
    <td>{{optional($row->paymentTerm)->name}}</td>
    <td>{{optional($row->approveToGo)->name}}</td>
    <td>{{optional($row->latestApprove)->name}}</td>
    <td>{{$row->status}}</td>
    <td>{{$row->payment_status}}</td>
  </tr>
  @endforeach
</table>
