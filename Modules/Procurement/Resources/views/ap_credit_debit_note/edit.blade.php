@extends('procurement::layouts.app')

@section('procurement::title', 'Edit A/P CN | DN')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'A/P CN | DN',
'active' => true,
'url' => route('procurement.ap_credit_debit_note.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.ap_credit_debit_note.edit', $ap_credit_debit_note->id)
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.ap_credit_debit_note.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('procurement.ap_credit_debit_note.update', $ap_credit_debit_note->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="card">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Bank Account <span style="color:red">*</span></label>
                                        <select name="bank" id="leave" class="form-control r-0 light s-12 select2" required>
                                            <option value="">Please Select</option>
                                            @foreach($banks as $bank)
                                            @if( old('bank') )
                                            <option value="{{ $bank->id }}" {{ old('bank') == $bank->id ? 'selected' : ''  }}>{{ $bank->name }}</option>
                                            @else
                                            <option value="{{ $bank->id }}" {{ $ap_credit_debit_note->charge_of_account_bank_id == $bank->id ? 'selected' : '' }}>{{ $bank->name }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Chart Of Account <span style="color:red">*</span></label>
                                        <select name="charge_of_account_id" id="balance" class="form-control r-0 light s-12 select2" required>
                                            <option value="">Please Select</option>
                                            @foreach($charts as $chart)
                                            @if( old('charge_of_account_id') )
                                            <option value="{{ $chart->id }}" {{ old('charge_of_account_id') == $chart->id ? 'selected' : ''  }}>{{ $chart->getFullName() }}</option>
                                            @else
                                            <option value="{{ $chart->id }}" {{ $ap_credit_debit_note->charge_of_account_id == $chart->id ? 'selected' : '' }}>{{ $bank->name }}-{{ $chart->getFullName() }}</option>
                                            @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Supplier <span style="color:red">*</span></label>
                                                <select name="supplier_id" id="supplier" class="form-control r-0 light s-12 select2" required>
                                                    <option value="">Please Select</option>
                                                    @foreach($suppliers as $supplier)
                                                    <option value="{{ $supplier->id }}" {{ $ap_credit_debit_note->supplier_id == $supplier->id ? 'selected' : '' }}>{{ $supplier->company_name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                       <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>A/P Payment Number <span style="color:red">*</span></label>
                                                    <select name="payable_id" id="payable" class="form-control r-0 light s-12 select2">
                                                        <option value="">Please Select</option>
                                                        @foreach($payables as $payable)
                                                        <option value="{{ $payable->id }}" {{ $ap_credit_debit_note->payable_id == $payable->id ? 'selected' : '' }}>{{ $payable->code_number }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Date <span style="color:red">*</span></label>
                                                <input type="date" class="form-control datepicker" name="date" value="{{ old('date') ?? $ap_credit_debit_note->date}}" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="">Amount: <span style="color:red">*</span></label>
                                                <input type="text" class="form-control separator currency" value="{{ old('amount') ?? $ap_credit_debit_note->amount }}" placeholder="1.000.000" required style="text-align:right;">
                                                <input type="hidden" name="amount" class="separator-hidden" value="{{ old('amount') ?? $ap_credit_debit_note->amount }}" style="text-align:right;">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" id="" cols="30" rows="10" class="form-control" placeholder="cth: Parking at West Coast Mall, gasoline 5 gal, etc">{{ old('description') ?? $ap_credit_debit_note->description }}</textarea>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped" id='userTable'>
                                        <thead>
                                            <tr>
                                                <th class="text-center"> Invoice</th>
                                                <th class="text-center"> Date</th>
                                                <th class="text-center"> Total Amount</th>
                                                <th class="text-center"> Amount Due</th>
                                                <th class="text-center"> Payment</th>
                                            </tr>
                                        </thead>
                                        <tbody id="content">
                                             @if($payableDetail)
                                            @foreach($payableDetail->details as $detail)
                                            <tr>
                                                <td align="center">{{ optional($detail->invoice)->invoice_number}}</td>
                                                <td align="center">{{ optional($detail->invoice)->invoice_date}}</td>
                                                <td align="right">{{ amount_international_with_comma(optional($detail->invoice)->total) }}</td>

                                                <td align="right">{{ amount_international_with_comma(optional($detail->invoice)->outstanding) }}</td>
                                                <td align="right">{{ amount_international_with_comma($detail->amount) }}</td>
                                            </tr>
                                            @endforeach
                                            @else

                                            @endif
                                        </tbody>
                                    </table>
                                </div>

                                <div class="row clearfix" style="margin-top:20px">
                                    <div class="col-md-12">
                                        <div class="float-right">
                                            <table class="table table-bordered table-hover" id="tab_logic_total">
                                                <tbody id="grand_total">
                                                    <tr>
                                                        <th class="text-center">Grand Total</th>
                                                        <td class="text-center">
                                                            {{ amount_international_with_comma($grand_total) }}
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Update' }}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script type="text/javascript">

    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }


    $(document).ready(function() {
        $('.separator.currency').number(true, 2);
        $('.separator').not('.separator.currency').number(true, 0);
        $('.separator').keyup(function() {
            $(this).next('.separator-hidden').val($(this).val());

        });
    });
         $('#supplier').on('change', function(e){
            $('#userTable tbody').empty();
              $('#grand_total').empty();
            $.post('{{ route('api.debit_note.getPayable') }}', {type:'payable', id: $('#supplier').val()}, function(e){
                 console.log(e)

                $('#payable').html(e);
            })

            $.post('{{ route('api.debit_note.getSupplier') }}', {type:'supplier', id: $('#supplier').val()}, function(e){
                console.log(e)
               $('#description').html(e.company_name);
            })
        })



    $('#payable').on('change', function() {

        $('#userTable tbody').empty();
          $('#grand_total').empty();
        const id = $(this).val();

        $.ajax({
            url: '/api/payable-detail/' + id,
            type: 'get',
            dataType: 'json',
            success: function(response) {
                console.log(response)

                $('#userTable').show();
                $('#userTable tbody').empty(); // Empty <tbody>

                var len = 0;
                len = Object.keys(response).length;

                len != 0 ? $("#btnSubmit").attr("disabled", false) : $("#btnSubmit").attr("disabled", true);


                let items = response.details
                let markup = items.map(item =>

                    `
                                <tr>
                                     <td align="center">${item.invoice.invoice_number}</td>
                                     <td align="center">${item.invoice.invoice_date}</td>
                                     <td align="right">${addCommas(item.invoice.total)}</td>

                                     <td align="right">${addCommas(item.invoice.outstanding)}</td>
                                     <td align="right">${addCommas(item.amount)}</td>
                                </tr>
                            `
                ).join('')
                let root = document.getElementById('content')
                root.innerHTML = markup

                const grand_total = items.reduce((a, b) => a + parseFloat(b.amount), 0);
                console.log(grand_total)
                $('.separator.currency').val(grand_total);
                $('.separator-hidden').val(grand_total);

                var tr_str = `
                            <tr>
                                <th class="text-center">Grand Total</th>
                                <td class="text-center">
                                          ${addCommas(grand_total)}
                                </td>
                            </tr>
                        `
                const bottom = document.getElementById('grand_total')
                bottom.innerHTML = tr_str
            }
        })
    })

    function addCommas(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>
@endpush
