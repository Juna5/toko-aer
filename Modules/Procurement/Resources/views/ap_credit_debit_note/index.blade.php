@extends('procurement::layouts.app')

@section('procurement::title', 'A/P Credit Debit Note')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Procurement',
    'active' => true,
    'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'A/P Credit Debit Note',
    'active' => true,
    'url' => route('procurement.ap_credit_debit_note.index')
    ])
@endsection

@section('procurement::content')
    @include('flash::message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
