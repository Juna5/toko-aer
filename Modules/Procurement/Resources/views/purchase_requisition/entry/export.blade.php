<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Code Number</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Employee</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Submission Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Required Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Project</th>
    <!-- <th style="background-color:#e2e25f; font-weight: bold;" align="center">Cost Center</th> -->
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Next Approver</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Final Approver</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Status</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->code_number}}</td>
    <td>{{format_d_month_y($row->submission_date)}}</td>
    <td>{{optional($row->employee)->name}}</td>
    <td>{{$row->submission_date}}</td>
    <td>{{$row->required_date}}</td>
    <td>{{ optional($row->project)->name}}</td>
    <!-- <td>{{optional($row->costCenter)->name}}</td> -->
    <td>{{optional($row->approveToGo)->name}}</td>
    <td>{{optional($row->latestApprove)->name}}</td>
    <td>{{$row->status}}</td>
  </tr>
  @endforeach
</table>
