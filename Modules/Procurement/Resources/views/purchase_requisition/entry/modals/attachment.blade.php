<form action="{{ route('procurement.purchase_requisition.attachment', $row->id) }}" method="POST" enctype="multipart/form-data" id="modal-body">
    @csrf
    @method('POST')
    <div class="form-group"><h5>Attachment for PR: {{$row->code_number}}</h5></div>
        <div class="form-group m-form__group row">
            <div class="form-group col-md-12">
                <a class="btn btn-primary white add-image-btn" data-name="attachment" style="color: white"><i class="icon wb-plus"></i>Add More Attachment</a>
                <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
            </div>
            <input type="hidden" name="id_purchase_requisition" id="id_purchase_requisition" value="{{ $row->id}}">
            @if(!empty($media))
                @foreach($media as $index => $val)
                    <div class='form-group col-md-3'>
                        <input type="file" name='attachment[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getFullUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf">
                        <span>
                            <br>
                                <a href="{{ asset($val->getFullUrl()) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                                    <i class="fa fa-file text-white" ></i> View File
                                </a>
                        </span>
                    </div>
                @endforeach
                    @else
                        @foreach (range(1, 4) as $i)
                            <div class='form-group col-md-3'>
                                <input type='file' name='attachment[{{ $i }}]' class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf">
                            </div>
                        @endforeach
                    @endif

                    <span id="attachment-btm"></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="modal-btn-save">Submit</button>
        </div>
</form>
<script type="text/javascript" src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
        $('.dropify').dropify();
        $('.add-image-btn').click(function() {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
    </script>
