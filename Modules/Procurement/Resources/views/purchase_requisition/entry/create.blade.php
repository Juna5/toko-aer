@extends('procurement::layouts.app')

@section('procurement::title', 'Add PR')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Procurement',
'active' => true,
'url' => route('procurement.view')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'PR',
'active' => true,
'url' => route('procurement.purchase_requisition.index')
])
@endsection
@section('procurement::breadcrumb-4')
@include('procurement::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('procurement.purchase_requisition.create')
])
@endsection
@section('procurement::content')
<div class="row" id="pr-create">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.purchase_requisition.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                <div class="row">
                    <div class="col-12">
                        <purchaserequisition-create />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
