@extends('setting::layouts.master')

@section('setting::title', 'PR')

@section('setting::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'PR',
'active' => true,
'url' => route('procurement.purchase_requisition.view')
])
@endsection

@section('setting::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-clipboard-list"></i>
                </div>
                <div class="card-body">
                    <h4>PR Entry</h4>
                    <p>Create PR.</p>
                    @can('view purchase requisition')
                    <a href="{{ route('procurement.purchase_requisition.index') }}" class="card-cta">Go to PR Entry <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card card-large-icons">
                <div class="card-icon bg-primary text-white">
                    <i class="fas fa-envelope-open-text"></i>
                </div>
                <div class="card-body">
                    <h4>PR Approval</h4>
                    <p>All PR approval data.</p>
                    @can('view purchase requisition approval')
                    <a href="{{ route('procurement.purchase_requisition.approval.index') }}" class="card-cta">Go to PR Approval <i class="fas fa-chevron-right"></i></a>
                    @endcan
                </div>
            </div>
        </div>

        <div class="col-lg-6">
                <div class="card card-large-icons">
                    <div class="card-icon bg-primary text-white">
                        <i class="fas fa-folder-open"></i>
                    </div>
                    <div class="card-body">
                        <h4>PR Reports</h4>
                        <p>PR Reports List </p>
                        @can('view purchase requisition')
                            <a href="{{ route('procurement.purchase_requisition.report') }}" class="card-cta">See in details <i class="fas fa-chevron-right"></i></a>
                        @endcan
                    </div>
                </div>
            </div>

    </div>
</div>
@endsection
