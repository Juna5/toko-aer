@extends('procurement::layouts.app')

@section('procurement::title', 'Report')

@section('procurement::breadcrumb-2')
	@include('procurement::include.breadcrum', [
		'title' => 'Report',
		'active' => false,
		'url' => '',
	])
@endsection

@section('procurement::content')
	<div class="section-body">
		<div class="row">

			<div class="col-lg-6">
				<div class="card card-large-icons">
					<div class="card-icon bg-primary text-white">
						<i class="fas fa-calendar-alt"></i>
					</div>
					<div class="card-body">
						<h4>PR Reports</h4>
						<p>PR Reports List </p>
						@can('view purchase requisition')
							<a href="{{ route('procurement.purchase_requisition.report') }}" class="card-cta">See in details <i class="fas fa-chevron-right"></i></a>
						@endcan
					</div>
				</div>
			</div>


			<div class="col-lg-6">
				<div class="card card-large-icons">
					<div class="card-icon bg-primary text-white">
						<i class="fas fa-user"></i>
					</div>
					<div class="card-body">
						<h4>PO Reports</h4>
						<p>PO Reports List </p>
						@can('view purchase order')
							<a href="{{ route('procurement.purchase-order.report') }}" class="card-cta">See in details <i class="fas fa-chevron-right"></i></a>
						@endcan
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
