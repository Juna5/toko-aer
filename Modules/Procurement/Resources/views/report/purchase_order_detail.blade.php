@extends('procurement::layouts.app')

@section('procurement::title', 'Pembelian')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Pembelian',
'active' => true,
'url' => route('procurement.purchase-order.index')
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="invoice-number text-primary">{{ $row->code_number }}</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table  table-hover table-md">
                                                    <tr>
                                                        <td width="30%">Employee</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->employee)->name }}</td>
                                                    </tr>
                                                    <!-- <tr>
                                                        <td width="30%">Title</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->title }}</td>
                                                    </tr> -->
                                                    @if($row->purchase_requisition_id != 0)
                                                    <tr>
                                                        <td width="30%">PR</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->purchaseRequisition)->code_number }}</td>
                                                    </tr>
                                                    @endif
                                                    <tr>
                                                        <td width="30%">Project</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->project_id ? optional($row->project)->name : '-' }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($row->date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Supplier</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->supplier)->name }}</td>
                                                    </tr>
                                                    <!-- <tr>
                                                        <td width="30%">Department</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->department)->name }}</td>
                                                    </tr> -->
                                                    <tr>
                                                        <td width="30%">Currency</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->currency)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Payment Method</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->paymentMethod)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Payment Term</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->paymentTerm)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Status</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ ucfirst($row->status) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Final Approver</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->latestApprove)->name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="30%">Next Approver</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval) }}</td>
                                                    </tr>
                                                    @if($row->rejected_by)
                                                        <tr>
                                                            <td width="30%">Rejected By</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ optional($row->rejectedBy)->name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Rejected At</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ format_d_month_y($row->rejected_at) }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td width="30%">Rejected Reason</td>
                                                            <td width="2%">:</td>
                                                            <td>{{ $row->rejected_reason }}</td>
                                                        </tr>
                                                    @endif
                                                </table>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-md-12">
                                    <div class="section-title">Item</div>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-md">
                                            <tr>
                                                <th>Item Description and Specification</th>
                                                <th>UoM</th>
                                                <th>Quantity</th>
                                                <th>Price</th>
                                                <th>
                                                    Dsc (%)<br>
                                                    Dsc Amount
                                                </th>
                                                <th>Description</th>
                                                <th>Sub Total</th>
                                            </tr>
                                            @foreach($items as $item)
                                            <tr>
                                                <td>{{ optional($item->product)->name }} </td>
                                                <td>{{ optional($item->uom)->name }}</td>
                                                <td align="right">{{ number_format($item->qty) }}</td>
                                                <td align="right">{{ number_format($item->price) }}</td>
                                                <td align="right">{{ $item->discount_rate ? number_format($item->discount_rate).'%' : '' }}  <br><br> {{ $item->discount_amount ? number_format($item->discount_amount) : '' }}  </td>
                                                <td>{{ $item->description }}</td>
                                                <td align="right">{{ number_format($item->sub_total) }}</td>
                                            </tr>
                                            @endforeach
                                            <tr>
                                                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Subtotal Exclude Discount All Items</td>
                                                <td align="right">{{ number_format($row->get_total) }}</td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Discount All Items</td>
                                                <td align="right">
                                                    {{ number_format($row->discount_all_item) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">Sub Total</td>
                                                <td align="right">
                                                    {{ number_format($row->sub_total) }}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6" align="right" style="font-size:13px; font-weight:bold">GST  {{ optional($row->tax)->rate }} %</td>
                                                <td align="right">
                                                    {{ number_format((optional($row->tax)->rate / 100) *  $row->sub_total) }}
                                                </td>

                                            </tr>
                                            <tr style="font-size:13px; font-weight:bold">
                                                <td colspan="6" align="right">Total</td>
                                                <td align="right" style="color:#5b35c4">
                                                    {{ number_format($row->total) }}
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="section-title">Attachment</div>
                                    <div class="form-group m-form__group row">
                                    @foreach($media as $index => $med)
                                        <div class='form-group col-md-2'>
                                            <div style="display:block;position:relative;overflow:hidden;width:100%;max-width:100%;height:200px;padding:5px 10px;font-size:14px;line-height:22px;color:#777;background-color:#FFF;background-image:none;text-align:center;">
                                                <img src="{{ asset($med->getFullUrl()) }}" data-default-file="{{ asset($med->getFullUrl()) }}" style="top:50%;-webkit-transform:translate(0,-50%);transform:translate(0,-50%);position:relative;max-width:100%;max-height:100%;background-color:#FFF;-webkit-transition:border-color .15s linear;transition:border-color .15s linear;">

                                            </div>
                                            <span>
                                            <br>
                                                <a href="{{ asset($med->getFullUrl()) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                                                    <i class="fa fa-file text-white" ></i> View File
                                                </a>
                                            </span>
                                        </div>
                                        @endforeach
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-lg-12">
                                            <div class="section-title">History</div>
                                            <div class="table-responsive">
                                                <table class="table table-striped table-hover table-md">
                                                    <tr>
                                                        <th>Employee</th>
                                                        <th>Status</th>
                                                        <th>Date</th>
                                                        <th>Description</th>
                                                    </tr>
                                                    @foreach($approvals as $approve)
                                                    <tr>
                                                        <td>{{ $approve->role}} {{ $approve->role &&  optional($approve->employee)->name ?'-': '' }} {{  optional($approve->employee)->name }}</td>
                                                        <td>{{ $approve->is_approved ? "Approved" : ($row->status == 'rejected' ? 'Rejected' : 'Waiting') }}</td>
                                                        <td>{{ format_d_month_y($approve->date) }}</td>
                                                        <td>{{ $approve->description }}</td>
                                                    </tr>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                      <div class="text-md-right">
                                        <a href="{{ route('procurement.purchase-order.report',['form_date' => $from_date, 'until_date'=> $until_date]) }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                                      </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @endsection
