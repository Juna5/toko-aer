@extends('procurement::layouts.app')

@section('procurement::title', 'Debit Note Report')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
        'title' => 'Procurement',
        'active' => true,
        'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Debit Note Report',
    'url' => route('procurement.debit_note.report'),
    'active' => true,
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-md-12">
            <div class="card no-b no-r">
                <div class="card-body">
                    <form action="{{ route('procurement.debit_note.report') }}" method="GET">
                        <div class="form-row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From Date</label>
                                    <input type="date" name="from_date" class="form-control datepicker" value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">To Date</label>
                                    <input type="date" name="until_date" class="form-control datepicker" value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                        <a href="{{ route('procurement.view') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br><br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>A/P Payment Number</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Supplier</th>
                                <th>Amount</th>
                            </tr>
                            @forelse($results as $index => $row)
                            <tr align="center" >
                                <td>{{ $index +1 }}</td>
                                <td>
                                    <a href="{{ route('procurement.debit_note.report-detail', $row->id) }}" target="_blank">{{ optional($row->payable)->code_number }}</a>
                                </td>
                                <td>{{ format_d_month_y($row->date) }}</td>
                                <td>{{ $row->employee_id ? optional($row->employee)->name : '-' }}</td>
                                <td>{{ optional($row->supplier)->company_name }}</td>
                                <td style="text-align: right;">{{ amount_international_with_comma($row->amount) }}</td>
                            </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(request('from_date') && request('end_date'))
                                {{ $results->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
