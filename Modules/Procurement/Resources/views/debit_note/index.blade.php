@extends('procurement::layouts.app')

@section('procurement::title', 'Debit Note')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Debit Note',
    'active' => true,
    'url' => route('procurement.debit_note.index')
    ])
@endsection

@section('procurement::content')
    @include('flash::message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
