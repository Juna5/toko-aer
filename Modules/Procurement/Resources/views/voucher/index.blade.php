@extends('procurement::layouts.app')

@section('procurement::title', 'Voucher')

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
    <style type="text/css">
        #form-import{
            display:none;
        }
    </style>
@endpush

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Voucher',
    'url' => route('procurement.voucher.index'),
    'active' => true,
    ])
@endsection

@section('procurement::content')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Voucher</div>
                    <div class="card-body">
                        <a href="{{ route('procurement.voucher.create') }}" class="btn btn-success mb-4" title="Add New Voucher">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>

                        <a href="{{ route('procurement.voucher.template-voucher') }}" class="btn btn-warning mb-4">Download Template</a>
                        <a id="import" class="btn btn-info mb-4 pull-right">Import</a>
                        <div id="form-import" class="mb-4">
                            <form action="{{ route('procurement.voucher.import') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="file" name="file">
                                <button type="submit">Upload</button>
                            </form>
                        </div>

                        {!! Form::open(['method' => 'GET', 'url' => route('procurement.voucher.index'), 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-append">
                                <button class="btn btn-dark" type="submit">
                                    Search
                                </button>
                            </span>
                        </div>
                        {!! Form::close() !!}



                        <div class="table-responsive py-4">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Depot</th>
                                        <th>Code</th>
                                        <th>Customer Telp</th>
                                        <th>Amount</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @forelse($voucher as $item)
                                    <tr>
                                        <td>{{ $loop->index + 1 }}</td>
                                        <td>{{ optional($item->depot)->name }}</td>
                                        <td>{{ $item->code }}</td>
                                        <td>{{ $item->no_telp }}</td>
                                        <td>{{ amount_international_with_comma($item->amount) }}</td>
                                        <td>
                                            {{-- <a href="{{ route('procurement.voucher.show', $item->id) }}" title="View Voucher"><button class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a> --}}
                                            <a href="{{ route('procurement.voucher.edit', $item->id) }}" title="Edit Voucher"><button class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => route('procurement.voucher.destroy', $item->id),
                                                'style' => 'display:inline'
                                            ]) !!}
                                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                        'type' => 'submit',
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'Delete Voucher',
                                                        'onclick'=>'return confirm("Confirm delete?")'
                                                )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="{{ config('laravelmanthra.view_columns_number') + 2 }}">
                                            Data not found, <a href="{{ route('procurement.voucher.create') }}"> create new </a>
                                        </td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $voucher->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    @include('manthra::include.flash_message')
@endsection
@push('javascript')
<script type="text/javascript">
    $("#import").click(function(e){
        $("#form-import").toggle();
    });
</script>
@endpush

