@extends('procurement::layouts.app')

@section('procurement::title', 'Voucher')

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endpush

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Voucher',
    'url' => route('procurement.voucher.index'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Edit',
    'url' => '',
    'active' => false,
    ])
@endsection

@section('procurement::content')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Edit Voucher #{{ $voucher->id }}
                        <div class="text-right">
                            <a href="{{ route('procurement.voucher.index') }}" title="Back">
                                <button class="btn btn-warning">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">

                        {!! Form::model($voucher, [
                            'method' => 'PATCH',
                            'url' => route('procurement.voucher.update', $voucher->id),
                            'class' => 'form-horizontal',
                            'files' => true,
                            'onsubmit' => "submitButton.disabled = true"
                        ]) !!}

                        @include ('procurement::voucher.form', ['submitButtonText' => 'Update'])

                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
