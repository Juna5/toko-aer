@extends('procurement::layouts.app')

@section('procurement::title', 'Voucher')

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endpush

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Voucher',
    'url' => route('procurement.voucher.index'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Show',
    'url' => '',
    'active' => false,
    ])
@endsection

@section('procurement::content')
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Voucher {{ $voucher->id }}
                        <div class="text-right">
                            <a href="{{ route('procurement.voucher.index') }}" title="Back">
                                <button class="btn btn-warning">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                </button>
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        <a href="{{ url('/voucher/' . $voucher->id . '/edit') }}" title="Edit Voucher">
                            <button class="btn btn-primary">
                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                            </button>
                        </a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['voucher', $voucher->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger',
                                    'title' => 'Delete Voucher',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}

                        <div class="table-responsive py-4">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $voucher->id }}</td>
                                    </tr>
                                    <tr><th> Depot Id </th><td> {{ $voucher->depot_id }} </td></tr><tr><th> Code </th><td> {{ $voucher->code }} </td></tr><tr><th> Amount </th><td> {{ $voucher->amount }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
