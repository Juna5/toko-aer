<div class="form-group {{ $errors->has('depot_id') ? 'has-error' : ''}}">
    {!! Form::label('depot_id', 'Depot', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('depot_id', $depot, null, ['class' => 'form-control select2', 'id' => 'select-level']) !!}

        {!! $errors->first('depot_id', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('code', 'Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('code', null, ['class' => ($errors->has('code')) ? 'form-control is-invalid' : 'form-control']) !!}

        {!! $errors->first('code', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
    {!! Form::label('amount', 'Amount', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('amount', null, ['class' => ($errors->has('amount')) ? 'form-control is-invalid' : 'form-control']) !!}

        {!! $errors->first('amount', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('expired_date') ? 'has-error' : ''}}">
    {!! Form::label('expired_date', 'Expired Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('expired_date', null, ['class' => ($errors->has('expired_date')) ? 'form-control is-invalid datepicker' : 'form-control datepicker']) !!}

        {!! $errors->first('expired_date', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', [
            'class' => 'btn btn-primary',
            'name' => 'submitButton'
        ]) !!}
    </div>
</div>
