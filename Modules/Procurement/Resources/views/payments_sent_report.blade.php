@extends('procurement::layouts.app')

@section('procurement::title', 'Payments Sent Last 30 Days Report')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Procurement',
    'active' => true,
    'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Payments Sent Last 30 Days Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Transfered From</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Supplier</th>
                                <th>Amount</th>
                            </tr>

                            @forelse($payable as $index => $row)
                                <tr align="center" >
                                    <td>{{ $index +1 }}</td>
                                    <td><a href="{{ route('finance.payment.payable-report-detail',[ $row->payable->id]) }}" target="_blank">{{ $row->payable->code_number }}</a></td>
                                    <td>
                                        <a href="{{ route('procurement.purchase_invoice.show', $row->purchaseInvoice->id) }}"
                                           target="_blank">{{ $row->purchaseInvoice->invoice_number }}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->payment_date) }}</td>
                                    <td>{{ $row->payable->employee ? optional($row->payable->employee)->name : '-' }}</td>
                                    <td>{{ optional($row->purchaseInvoice)->supplier_name }}</td>
                                    <td align="right">{{ amount_international_with_comma($row->amount) }}</td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="row clearfix" style="margin-top:20px">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <table class="table table-bordered table-hover" id="tab_logic_total">
                                        <tbody id="grand_total">
                                        <tr>
                                            <th class="text-center">Grand Total</th>
                                            <td class="text-center">
                                                {{ amount_international_with_comma($total) }}   
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-left" style="margin-bottom: 2%">
                            <a href="{{ route('procurement.view') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection