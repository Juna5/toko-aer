@extends('procurement::layouts.app')

@section('procurement::title', 'Open Purchases Report')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Procurement',
    'active' => true,
    'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Open Purchases Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Transfered From</th>
                                <th>Date</th>
                                <th>Created By</th>
                                <th>Approval Status</th>
                                <th>Total</th>
                            </tr>

                            @forelse($purchaseOrder as $index => $row)
                                @php
                                    if (empty($row->status)) {
                                        $status = "";
                                    } elseif ($row->status == 'waiting') {
                                        $status = 'badge badge-success';
                                    } elseif ($row->status == 'approved') {
                                        $status = 'badge badge-info';
                                    } elseif ($row->status == 'rejected') {
                                        $status = 'badge badge-danger';
                                    }
                                @endphp
                                <tr align="center">
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('procurement.purchase-order.report-detail',[ $row->id]) }}" target="_blank">{{ $row->code_number }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('procurement.purchase_requisition.report-detail',[ $row->purchaseRequisition->id]) }}" target="_blank">{{ $row->purchaseRequisition->code_number }}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->date) }}</td>
                                    <td>{{ optional($row->employee)->name }}</td>
                                    <td><span class="{{$status}}">{{ ucfirst($row->status) }}</span></td>
                                    <td>{{ amount_international_with_comma($row->total) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="row clearfix" style="margin-top:20px">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <table class="table table-bordered table-hover" id="tab_logic_total">
                                        <tbody id="grand_total">
                                        <tr>
                                            <th class="text-center">Grand Total</th>
                                            <td class="text-center">
                                                {{ amount_international_with_comma($total) }}   
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-left" style="margin-bottom: 2%">
                            <a href="{{ route('procurement.view') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection