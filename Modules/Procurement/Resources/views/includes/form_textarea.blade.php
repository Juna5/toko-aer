<div class="form-group {{ $errors->has($name) ? 'has-error' : '' }}">
	{!! Form::label($name) !!}
	{!! Form::textarea($name, null, [
		'class' => ($errors->has($name)) ? 'form-control is-invalid' : 'form-control',
		'placeholder' => $placeholder ?? null,
		'style' => 'height: 150px'
	]) !!}
	{!! $errors->first($name, '<div class="invalid-feedback d-block">:message</div>') !!}
</div>
