@extends('procurement::layouts.app')

@section('procurement::title', 'General Settings')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'General Settings',
'active' => true,
'url' => route('procurement.master.index')
])
@endsection

@section('procurement::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
        @can('view uom')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.uom.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-clipboard-list" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">UoM</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        <!-- @can('view location')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.location.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-map-marked-alt" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Location</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan -->
        @can('view product category')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.product-category.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-folder-open" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Product Category</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view product')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.product.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-box" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Product</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view price category')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.price_category.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-money-bill-wave-alt" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Price Category</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view tax')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.tax.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-percent" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Tax</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view payment term')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.payment_term.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-comment-dollar" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Payment Term</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view payment method')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.payment_method.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-credit-card" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Payment Method</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view warehouse master')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.depot_master.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-warehouse" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Depot Master</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view currency')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.currency.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-dollar-sign" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Currency</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view vehicle')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.vehicle.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-truck" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Vehicle</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view business role')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.business_role.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-archive" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Business Role</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view business industry')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.business_industry.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-chart-area" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Business Industry</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view weight class')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.weight_class.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-list" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Weight Class</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view customer type')
        <div class="col-lg-3">
            <a href="{{ route('procurement.master.customer_type.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-list" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Customer Type</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
        @can('view exclude product')
        <div class="col-lg-3">
            <a href="{{ route('setting.exclude-product.index') }}" class="text-white">
                <div class="card border-radius card-icons" style="background-color:#186271">
                    <div class="card-body">
                        <div class="row">
                            <div class="col text-center">
                                <i class="fas fa-list" style="font-size: 64px"></i>
                                <div class=" font-weight-bold text-icons title">Exclude Product</div>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        @endcan
    </div>
</div>
@endsection