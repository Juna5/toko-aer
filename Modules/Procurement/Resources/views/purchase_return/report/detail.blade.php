@extends('procurement::layouts.app')

@section('procurement::title', 'Purchase Return Report Detail')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
        'title' => 'Procurement',
        'active' => true,
        'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Purchase Return Report',
    'url' => route('procurement.purchase-return-report'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-4')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Return Report Detail',
        'active' => true,
        'url' => route('procurement.purchase-return-report', $row->id)
    ])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-12">
        <div class="card">
            @include('flash::message')
            <div class="card-body p-10">
                <div class="section-body">
                    <div class="invoice">
                        <div class="invoice-print">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="invoice-title">
                                        <h2>Detail</h2>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="invoice-number text-primary ">{{ format_d_month_y($row->date) }}
                                                </div>
                                            </div>
                                            <div class="col-md-6 text-md-right">
                                                <div class="invoice-number text-primary ">{{ $row->code_number }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-md">
                                                    <tr>
                                                        <td width="40%">Supplier</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->supplier)->company_name }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Memo of Credit/Debit</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ optional($row->purchaseInvoice)->invoice_number }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Date</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ format_d_month_y($row->date) }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td width="40%">Remarks</td>
                                                        <td width="2%">:</td>
                                                        <td>{{ $row->description }}</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-md">
                                    <tr>
                                        <th data-width="40">#</th>
                                        <th>Item</th>
                                        <th class="text-center">Qty</th>
                                        <th class="text-center">UoM</th>
                                        <th class="text-right">Price</th>
                                        <th class="text-right">Discount (%)</th>
                                        <th class="text-right">Sub Total</th>
                                    </tr>
                                    @foreach($purchaseReturnDetail as $index => $data)
                                        <tr>
                                            <td>{{ $index +1 }}</td>
                                            <td>{{ $data->item }}</td>
                                            <td class="text-center">{{ amount_international_with_comma($data->qty) }}</td>
                                            <td class="text-center">{{ $data->uom }}</td>
                                            <td class="text-right">{{ amount_international_with_comma($data->price) }}</td>
                                            <td class="text-right">{{ $data->discount_amount ?? '0' }}%</td>
                                            <td class="text-right">{{ amount_international_with_comma($data->sub_total) }}</td>
                                        </tr>
                                    @endforeach
                                   
                                    <tr>
                                        <td colspan="6" style="text-align: right; font-weight: bold;">Subtotal Exclude Discount All Items</td>
                                        <td align="right">{{ amount_international_with_comma($row->subtotal_exclude_discount) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: right; font-weight: bold;">Discount All Items (RM)</td>
                                        <td align="right">{{ amount_international_with_comma($row->discount_all_item) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: right; font-weight: bold;">Sub Total</td>
                                        <td align="right">{{ amount_international_with_comma($row->subtotal_include_discount) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: right; font-weight: bold;">GST {{ $row->discount_gst }}%</td>
                                        <td align="right">{{ amount_international_with_comma($row->amount_gst) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="text-align: right; font-weight: bold;">Total</td>
                                        <td  align="right"  style="color:#5b35c4; font-weight: bold;">{{ amount_international_with_comma($row->grand_total) }}</td>
                                    </tr>
                                </table>
                            </div>
                            <hr>
                            <div class="text-md-right">
                                <a href="{{ route('procurement.purchase-return-report') }}" class="btn btn-warning btn-icon icon-left"><i class="la la-arrow-left"></i> Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
