@extends('procurement::layouts.app')

@section('procurement::title', 'Purchase Return')

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endpush

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Return',
        'active' => true,
        'url' => route('procurement.purchase-return.index')
    ])
@endsection

@section('procurement::breadcrumb-4')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Return Edit',
        'active' => true,
        'url' => route('procurement.purchase-return.edit', $return->id)
    ])
@endsection

@push('stylesheet')
    <style>
        input[type=number] {
            text-align:right;
        }
    </style>
@endpush

@section('procurement::content')
    @include('flash::message')
    @include('include.error-list')
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <purchase-return-edit :return="{{ $return }}"></purchase-return-edit>
                </div>
            </div>
        </div>
    </div>
@endsection