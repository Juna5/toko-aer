@extends('procurement::layouts.app')

@section('procurement::title', 'Purchase Return')

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/toastr.min.css') }}">
@endpush

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Return',
        'active' => true,
        'url' => route('procurement.purchase-return.index')
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover table-stripped']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script src="{{ asset('js/toastr.min.js') }}"></script>
    @include('procurement::purchase_requisition.entry.modals.index')
    @include('shared.wrapperDatatable')

    @include('procurement::purchase_return.stock_adjustment.index')

    <script>
     $(document).on('click', '#modal_adjustment', function(){
        console.log('sukses')
        var valId = $(this).data('id');
        $('#id_purchase_return').val(valId);
        var requestUrl = '{{ route('procurement.purchase_return.showAdjustment', ':id') }}';
        var getData = requestUrl.replace(':id', valId);

          $('#attachment').modal('hide');

        $.ajax({
            url: getData,
            type: "GET",
            success: function(adj) {

           $("#adjustment").find('.modal-body').html(adj)

            }
        });
    });
    </script>
@endpush
