<form action="{{ route('procurement.purchase_return.adjustment', $row->id) }}" method="POST" enctype="multipart/form-data" id="modal-body">
    @csrf
    @method('POST')
    <div class="form-group"><h5>Stock Adjustment for Purchase Return</h5>
        <h6 style="color:#a63028;">{{$row->code_number}}</h6></div>
        <div class="card">
            <div class="row">
                <div class="col-md-6">
                    <label>Date</label>
                    <div><input type="text" name="date" class="datepicker form-control" value="{{ date('yy-m-d') }}"></div>
                </div>
                <div class="col-md-6">
                    <label>Remark</label>
                    <textarea class="form-control" name="description"></textarea>
                </div>
                <div class="col-md-6">
                    <div><input type="hidden" name="purchase_return_id" value="{{ $row->id }}"></div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-md">
                        <tr>
                            <th></th>
                            <th>#</th>
                            <th>Item</th>
                            <th>Qty</th>
                            <th>UoM</th>
                            <th>Price</th>
                            <th>Stock Qty</th>
                            <th width="22%">Warehouse</th>
                        </tr>
                            <tbody>
                                @forelse($prDetail as $index => $detail)
                                <tr>
                                <td><input type="checkbox" name="checked[]" value="{{ $detail->id }}"></td>
                                <td>{{ $index +1 }}</td>
                                <td>{{ $detail->item }}</td>
                                <td>{{ $detail->qty }}</td> 
                                <td>{{ $detail->uom }}</td>
                                <td>{{ amount_international_with_comma($detail->price) }}</td>
                                <td><input type="hidden" name="item_{{ $detail->id }}" class="form-control" value="{{ $detail->item }}"><input type="text" name="stock_qty_{{ $detail->id }}" class="form-control" value="{{  $detail->qty - getstockQtyAdj($detail->item, $row->id)   >= 0 ?  $detail->qty - getstockQtyAdj($detail->item, $row->id) : $detail->qty}}"></td>
                                <td width="22%"><select name="warehouse_id_{{ $detail->id }}" class="form-control r-0 light s-12 select2">
                                    @foreach($warehouse as $ware)
                                        <option value="{{ $ware->id }}">{{ $ware->name }}</option>
                                    @endforeach
                                </select></td>
                                </tr>
                                @endforeach
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary" id="modal-btn-save">Submit</button>
        </div>
    </div>
</form>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2({
            placeholder: 'Please Select',
            containerCssClass: 'form-control',
            allowClear: true,
            width: '100%'
        });

        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });
    });
</script>
