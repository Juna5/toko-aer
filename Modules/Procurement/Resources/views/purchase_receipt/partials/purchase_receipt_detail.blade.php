<div class="section-body">
    <div class="invoice">
        <div class="invoice-print">
            <div class="row">
                <div class="col-lg-12">
                    <div class="invoice-title">
                        <h2>Purchase Receipt</h2>
                        <div class="invoice-number text-primary"> {{ $purchaseReceipt->invoice_number }}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <strong>Billed From:</strong><br>
                                @if (! is_null($purchaseReceipt->purchase_order_id))
                                    {{ $purchaseReceipt->purchaseOrder->supplier->company_name }}<br>
                                    {{ $purchaseReceipt->purchaseOrder->supplier->address }}<br>
                                    {{-- {{ $purchaseReceipt->purchaseOrder->supplier->location->name }} --}}
                                @endif
                            </address>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <address>
                                <strong>Purchase Receipt Date:</strong><br>
                                {{ \Carbon\Carbon::parse($purchaseReceipt->invoice_date)->isoFormat('MMMM Do YYYY') }}<br><br>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <strong>Payment Method:</strong><br>
                                {{ optional($purchaseReceipt->paymentMethod)->name }}
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="section-title">Order Summary</div>
                    <p class="section-lead">All items here cannot be deleted.</p>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-md">
                            <tr>
                                <th data-width="40">#</th>
                                <th>Item</th>
                                <th class="text-center">Uom</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Discount Amount</th>
                                <th class="text-right">Subtotal</th>
                            </tr>
                            @foreach($purchaseReceiptDetails as $index => $data)
                                <tr>
                                    <td>{{ $index +1 }}</td>
                                    <td>{{ $data->item }}</td>
                                    <td class="text-center">{{ $data->uom }}</td>
                                    <td class="text-center">{{ $data->qty }}</td>
                                    <td class="text-right">{{ amount_international_with_comma($data->price) }}</td>
                                    <td class="text-right">{{ $data->discount_amount ?? '0' }}%</td>
                                    <td class="text-right">{{ amount_international_with_comma($data->sub_total) }}</td>
                                </tr>
                            @endforeach
                            @php

                            @endphp
                            <tr>
                                <td colspan="6" align="right">Subtotal Exclude Discount All Items</td>
                                <td  align="right">{{ amount_international_with_comma($purchaseReceiptItem->sub_total_exclude_disc) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Discount All Items (RM)</td>
                                <td  align="right">{{ amount_international_with_comma($purchaseReceiptItem->discount_all_item) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Sub Total</td>
                                <td  align="right">{{ amount_international_with_comma($purchaseReceiptItem->subtotal_detail) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">GST {{ $purchaseReceiptItem->gst }}%</td>
                                <td align="right">{{ amount_international_with_comma($purchaseReceiptItem->amount_gst) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Total</td>
                                <td  align="right"  style="color:#5b35c4; font-weight: bold;">{{ amount_international_with_comma($purchaseReceiptItem->total) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="text-md-right">
            <div class="float-lg-left mb-lg-0 mb-3">
                {{-- <a href="{{ route('procurement.purchase_receipt.receivable_form', $purchaseReceipt->id) }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-credit-card"></i> Process
                    Payment
                </a> --}}
                <a href="{{ route('procurement.purchase_receipt.index') }}" class="btn btn-warning btn-icon icon-left"><i class="fas fa-arrow-left"></i> Back</a>
            </div>
            <a href="{{ route('procurement.purchase_receipt.pdf', $purchaseReceipt->id) }}" target="_blank" class="btn btn-danger btn-icon icon-left"><i class="fas fa-print"></i> Print</a>
        </div>
    </div>
</div>
