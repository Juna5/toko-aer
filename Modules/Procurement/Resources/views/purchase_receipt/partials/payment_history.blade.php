<div class="card-body p-0">
    <div class="float-right m-3">
        <a href="{{ route('procurement.purchase_receipt.receivable_form', $purchaseReceipt->id) }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-credit-card"></i> Process
            Payment
        </a>
    </div>
    <div class="table-responsive">
        <table class="table table-striped" id="sortable-table">
            <thead>
            <tr>
                <th class="text-center">
                    <i class="fas fa-th"></i>
                </th>
                <th>Purchase Receipt No.</th>
                <th>Payment Date</th>
                <th>Amount</th>
            </tr>
            </thead>
            <tbody>
            @forelse($payments as $index => $payment)
                <tr>
                    <td class="text-center">{{ $index +1 }}</td>
                    <td>{{ $payment->purchaseReceipt->invoice_number ?? '-' }}</td>
                    <td>
                        {{ format_d_month_y($payment->payment_date) }}
                    </td>
                    <td>{{ amount_international_with_comma($payment->amount) }}</td>
                </tr>
                @empty
                <tr>
                    <td colspan="6" class="text-center">Data not found.</td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
</div>
