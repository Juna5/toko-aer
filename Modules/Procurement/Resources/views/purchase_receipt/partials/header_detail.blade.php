<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div style="display: flex;justify-content: space-between">
                <div>
                    <p>Supplier <br> <b style="font-size: 20px">{{ $purchaseReceipt->purchaseOrder->supplier->company_name }}</b></p>
                </div>
                <div>
                    <p>Date <br> <b style="font-size: 20px">{{ format_d_month_y($purchaseReceipt->invoice_date) }}</b></p>
                </div>
                <div>
                    <p>Due Date <br> <b style="font-size: 20px">{{ format_d_month_y($purchaseReceipt->invoice_due_date) }}</b></p>
                </div>
                <div>
                    <p>Total <br> <b style="font-size: 20px;">{{ amount_international_with_comma($purchaseReceipt->total) }}</b></p>
                </div>
                {{-- <div>
                    <p>Outstanding <br> <b style="font-size: 20px;">{{ amount_international_with_comma($purchaseReceipt->outstanding) }}</b></p>
                </div> --}}
            </div>
        </div>
    </div>
</div>
