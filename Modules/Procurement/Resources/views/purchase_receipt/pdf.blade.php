<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Purchase Receipt</title>
    <style>
        @font-face {
            /* font-family: SourceSansPro;
            src: url(SourceSansPro-Regular.ttf); */
            font-family: 'Open Sans', sans-serif;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #940000;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 100%;
            height: 29.7cm;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-family: 'Open Sans', sans-serif;
            /* font-family: Arial, sans-serif; */
            font-size: 12px;
            /* font-family: SourceSansPro; */
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
            margin-top: 8px;
        }

        #logo img {
            height: 70px;
        }

        #company {
            width: 100%;
            float: right;
            text-align: right;
        }

        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            border-left: 6px solid #940000;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            width: 100%;
            float: right;
            text-align: right;
        }

        #invoice h1 {
            font-family: 'Open Sans', sans-serif;
            color: #940000;
            font-size: 14px;
            /* line-height: 1em; */
            font-weight: normal;
            margin: 0  0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            font-family: 'Open Sans', sans-serif;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #940000;;
        }

        #notices{
            padding-left: 6px;
            border-left: 6px solid #940000;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    <div id="logo">
         <img src="{{ asset('images/logo.png') }}" width="20%" height="20%" />
    </div>
    <div id="company">
        <h2 class="name">Sengliy</h2>
        <div>No. 28, Jalan Tiram 14,</div>
        <div>Taman Perindustrian Tiram,</div>
        <div>Jalan Sungai Tiram,</div>
        <div>81800 Ulu Tiram Johor, Malaysia.</div>
        <div>Tel : +607 861 2730 Fax : +607 863 2730</div>
        <div><a href="mailto:company@example.com">enquiry@sengliy.com.my</a></div>
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">PURCHASE RECEIPT FROM:</div>
            <h2 class="name">
                @if (! is_null($purchaseReceipt->purchase_order_id))
                    {{ $purchaseReceipt->purchaseOrder->supplier->company_name ?? null }}
                @endif
            </h2>
            <div class="address">
                @if (! is_null($purchaseReceipt->purchase_order_id))
                    {{ $purchaseReceipt->purchaseOrder->supplier->address ?? null }}
                @endif
            </div>
            <div class="address">Phone: 
                @if (! is_null($purchaseReceipt->purchase_order_id))
                    {{ $purchaseReceipt->purchaseOrder->supplier->phone ?? null }}
                @endif
            </div>
            <div class="address">Fax: 
                @if (! is_null($purchaseReceipt->purchase_order_id))
                    {{ $purchaseReceipt->purchaseOrder->supplier->fax ?? null }}
                @endif
            </div>
            <div class="email">
                <a href="mailto:john@example.com">
                    @if (! is_null($purchaseReceipt->purchase_order_id))
                        {{ $purchaseReceipt->purchaseOrder->supplier->email ?? null }}
                    @endif
                </a>
            </div>
        </div>
        <div id="invoice">
            <h1>PURCHASE RECEIPT #{{ $purchaseReceipt->invoice_number }}</h1>    
            <div class="date">{{ $purchaseReceipt->getInvoiceDate() }}</div>
            <div class="date">Due Date: {{ $purchaseReceipt->getInvoiceDueDate() }}</div>
        </div>
    </div>
    <table>
        <thead>
        <tr>
            <th class="service">Item</th>
            <th>Remark</th>
            <th>Quantity</th>
            {{-- <th class="desc">Uom</th> --}}
            <th>Price</th>
            <th>Discount</th>
            <th>Subtotal</th>
        </tr>
        </thead>
        <tbody>
        @forelse($purchaseReceipt->purchaseReceiptDetails as $index => $item)
            <tr>
                <td class="service" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #940000;  line-height: 18px;  vertical-align: top;">{{ $item->item }}</td>
                <td class="service" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #940000;  line-height: 18px;  vertical-align: top;">{{ $item->description }}</td>
                <td class="unit" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ $item->qty }} {{ $item->uom }}</td>
                {{-- <td class="desc" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;"></td> --}}
                <td class="qty" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ amount_international_with_comma($item->price) }}</td>
                <td class="total" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ $item->discount_amount ?? 0 }}%</td>
                <td class="total" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ amount_international_with_comma($item->sub_total) }}</td>
            </tr>
        @empty
            <tr>
                <td colspan="6" style="text-align: center">Data not found</td>
            </tr>
        @endforelse
        <tr>
            <td colspan="5" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">Subtotal (Excl Discount)</td>
            <td class="total"  style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">{{ amount_international_with_comma($purchaseReceiptItem->sub_total_exclude_disc) }}</td>
        </tr>
        <tr>
            <td colspan="5" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">Discount All Items (RM)</td>
            <td class="total"  style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">{{ amount_international_with_comma($purchaseReceiptItem->discount_all_item) }}</td>
        </tr>
        <tr>
            <td colspan="5" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">Sub Total</td>
            <td class="total"  style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">{{ amount_international_with_comma($purchaseReceiptItem->subtotal_detail) }}</td>
        </tr>
        <tr>
            <td colspan="5" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">GST {{ $item->gst }}%</td>
            <td class="total"  style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;">{{ amount_international_with_comma($purchaseReceiptItem->amount_gst) }}</td>
        </tr>
        <tr>
            <td colspan="5" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;" class="grand total text-bold"><strong>Grand Total</strong></td>
            <td class="grand total text-bold"  style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;"><strong>{{ amount_international_with_comma($purchaseReceiptItem->total) }}</strong></td>
        </tr>
        <tr>
            <td colspan="6" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;" class="text-bold">{{ $terbilang }}</td>
        </tr>
        </tbody>
    </table>
    <div id="notices">
        <div  class="notice">Notes:</div>
        <div style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:left;" >
            <ul>
                <li>All cheques should be crossed and made payable to <br>
                    SENG LIY ENGINEERING (M) SDN BHD <br>
                    Bank A/C No.: Public Bank Bhd 3186795305
                </li>
                <li>Goods sold are neither returnable nor refundable</li>
            </ul>
        </div>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SENG LIY ENGINEERING (M) SDN BHD</p>
    </div>
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
