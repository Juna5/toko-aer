@extends('procurement::layouts.app')

@section('procurement::title', 'Create Purchase Receipt')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Receipt',
        'active' => true,
        'url' => route('procurement.purchase_receipt.index')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Create Purchase Receipt',
        'active' => true,
        'url' => route('procurement.purchase_receipt.create')
    ])
@endsection

@push('stylesheet')
 <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>
@endpush

@section('procurement::content')
@include('flash::message')
@include('include.error-list')
<div class="section-body">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <a href="{{ route('procurement.purchase_receipt.index') }}" class="btn btn-warning">
                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                    </a>
                </div>
                <div class="card-body">
                    <form action="{{ route('procurement.purchase_receipt.store') }}" method="POST" enctype="multipart/form-data">
                        @include('procurement::purchase_receipt.partials.form', [
                            'purchaseReceipt' => new \Modules\Procurement\Entities\PurchaseReceipt()
                        ])
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
    <script>
        $(document).ready(function(){
             var str = $("#po_id").val();
            $.ajax({
                url: '/api/purchase-order/' + str,
                type: 'get',
                dataType: 'json',
                success: function (response) {
                    console.log(response)

                    $('#userTable').show();
                    $('#userTable tbody').empty();

                    var len = 0;
                    len = Object.keys(response).length;

                    len != 0 ? $("#btnSubmit").attr("disabled", false) : $("#btnSubmit").attr("disabled", true);

                    const supplier = response.supplier.company_name;
                    $('#supplier').val(supplier)
                    var warehouse = {!! json_encode($warehouse->toArray(), JSON_HEX_TAG) !!};
                    let warehouses = warehouse.map((ware, index) =>
                        `<option value="${ware.id}">${ware.name}</option>`
                        ).join('')
                    let items = response.items
                    const ware = $(this)

                    let markup = items.map((item, i) =>
                        `
                            <tr id="${i}">
                                 <td align="center">${i+1} <input type="hidden" class="form-control" style="text-align:right;" value="${item.id}" name="po_item_id[]" required></td>
                                 <td align="center">${item.product.name} <input type="hidden" class="form-control" style="text-align:right;" value="${item.product_id}" name="product_id[]" required></td>
                                 <td align="center">${item.uom.code}</td>
                                 <td align="center">${item.qty} <input type="hidden" class="form-control" style="text-align:right;" value="${item.qty}" name="qty[]" required></td>
                                 <td align="center">${addCommas(item.price)}</td>
                                 <td align="center" width="12%"><input type="text" class="form-control" style="text-align:right;" value="${item.qty}" name="confirmQty[]" required></td>
                                 <td align="right" width="15%"><select class="form-control select2 select-warehouse" name="warehouse[]" id="select-warehouse" required>
                                    </select>
                                 </td>
                                 <td align="right">${item.discount_rate == null ? '0' : item.discount_rate}%</td>
                                 <td align="right">${addCommas(item.sub_total)}</td>
                            </tr>
                        `
                    ).join('')
                    let root = document.getElementById('content')
                    root.innerHTML = markup
                    let wareroot = document.getElementsByClassName('select-warehouse')
                    for(var i=0; i < wareroot.length; i++) {
                        wareroot[i].innerHTML = warehouses
                    }
                    

                    const subtotalExcludeDisc = response.get_total;
                    const dicAllItem = response.discount_all_item;
                    const subTotal = response.sub_total;
                    const tax = response.tax.rate;
                    const gst = (tax / 100) * subTotal;
                    const total = response.total;

                    var tr_str = `
                        <tr>
                            <td colspan="8" align="right">Subtotal Exclude Discount All Items</td>
                            <td  align="right">${addCommas(subtotalExcludeDisc)}</td>
                        </tr>
                        <tr>
                            <td colspan="8" align="right">Discount All Items</td>
                            <td  align="right">${dicAllItem}%</td>
                        </tr>
                        <tr>
                            <td colspan="8" align="right">Sub Total</td>
                            <td  align="right">${addCommas(subTotal)}</td>
                        </tr>
                        <tr>
                            <td colspan="8" align="right">GST ${tax}%</td>
                            <td align="right">${addCommas(gst)}</td>
                        </tr>
                        <tr>
                            <td colspan="8" align="right">Total</td>
                            <td  align="right"  style="color:#5b35c4; font-weight: bold;">${addCommas(total)}</td>
                        </tr>
                    `
                    const bottom = document.getElementById('bottom')
                    bottom.innerHTML = tr_str
                }
            })

            $('#home-tab3').on('click', function(){
                $('#userTable tbody').empty();
                $('#bottom').empty();
                $('#po_id').val('');
                $('#supplier').val('');
            });
            $('.select2').select2({
                placeholder: 'Please Select',
                containerCssClass: 'form-control',
                allowClear: true,
                width: '100%'
            });

            $('#po_id').on('change', function(){
                $('#userTable tbody').empty();
                $('#userTable tfoot').empty();

                const id = $(this).val();
                $.ajax({
                    url: '/api/purchase-order/' + id,
                    type: 'get',
                    dataType: 'json',
                    success: function (response) {
                        console.log(response)

                        $('#userTable').show();
                        $('#userTable tbody').empty();

                        var len = 0;
                        len = Object.keys(response).length;

                        len != 0 ? $("#btnSubmit").attr("disabled", false) : $("#btnSubmit").attr("disabled", true);

                        const supplier = response.supplier.company_name;
                        $('#supplier').val(supplier)
                        var warehouse = {!! json_encode($warehouse->toArray(), JSON_HEX_TAG) !!};
                        let warehouses = warehouse.map((ware, index) =>
                            `<option value="${ware.id}">${ware.name}</option>`
                            ).join('')
                        let items = response.items
                        const ware = $(this)

                        let markup = items.map((item, i) =>
                            `
                                <tr id="${i}">
                                     <td align="center">${i+1} <input type="hidden" class="form-control" style="text-align:right;" value="${item.id}" name="po_item_id[]" required></td>
                                     <td align="center">${item.product.name} <input type="hidden" class="form-control" style="text-align:right;" value="${item.product_id}" name="product_id[]" required></td>
                                     <td align="center">${item.uom.code}</td>
                                     <td align="center">${item.qty} <input type="hidden" class="form-control" style="text-align:right;" value="${item.qty}" name="qty[]" required></td>
                                     <td align="center">${addCommas(item.price)}</td>
                                     <td align="center" width="12%"><input type="text" class="form-control" style="text-align:right;" value="${item.qty}" name="confirmQty[]" required></td>
                                     <td align="right" width="15%"><select class="form-control select2 select-warehouse" name="warehouse[]" id="select-warehouse" required>
                                        </select>
                                     </td>
                                     <td align="right">${item.discount_rate == null ? '0' : item.discount_rate}%</td>
                                     <td align="right">${addCommas(item.sub_total)}</td>
                                </tr>
                            `
                        ).join('')
                        let root = document.getElementById('content')
                        root.innerHTML = markup
                        let wareroot = document.getElementsByClassName('select-warehouse')
                        for(var i=0; i < wareroot.length; i++) {
                            wareroot[i].innerHTML = warehouses
                        }
                        

                        const subtotalExcludeDisc = response.get_total;
                        const dicAllItem = response.discount_all_item;
                        const subTotal = response.sub_total;
                        const tax = response.tax.rate;
                        const gst = (tax / 100) * subTotal;
                        const total = response.total;

                        var tr_str = `
                            <tr>
                                <td colspan="8" align="right">Subtotal Exclude Discount All Items</td>
                                <td  align="right">${addCommas(subtotalExcludeDisc)}</td>
                            </tr>
                            <tr>
                                <td colspan="8" align="right">Discount All Items</td>
                                <td  align="right">${dicAllItem}%</td>
                            </tr>
                            <tr>
                                <td colspan="8" align="right">Sub Total</td>
                                <td  align="right">${addCommas(subTotal)}</td>
                            </tr>
                            <tr>
                                <td colspan="8" align="right">GST ${tax}%</td>
                                <td align="right">${addCommas(gst)}</td>
                            </tr>
                            <tr>
                                <td colspan="8" align="right">Total</td>
                                <td  align="right"  style="color:#5b35c4; font-weight: bold;">${addCommas(total)}</td>
                            </tr>
                        `
                        const bottom = document.getElementById('bottom')
                        bottom.innerHTML = tr_str
                    }
                })
            })

            function addCommas(nStr) {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
        });
    </script>
@endpush
