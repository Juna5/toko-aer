@extends('procurement::layouts.app')

@section('procurement::title', 'Purchase Receipt Outstanding')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Receipt Outstanding',
        'active' => true,
        'url' => route('procurement.purchase_receipt_outstanding')
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">                
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="home-tab" href="{{ route('procurement.purchase_receipt.index') }}" role="tab" aria-controls="home" aria-selected="false">Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="document-tab" data-toggle="tab" href="#outstanding" role="tab" aria-controls="outstanding" aria-selected="true">Outstanding</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                         @include('procurement::purchase_receipt.tab.outstanding')
                    </div>
                </div>                
            </div>
        </div>
    </div>
@endsection