@extends('procurement::layouts.app')

@section('procurement::title', 'Purchase Receipt')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Receipt',
        'active' => true,
        'url' => route('procurement.purchase_receipt.index')
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            @include('flash::message')
            <div class="card">                
                <div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Entry</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="document-tab"  href="{{ route('procurement.purchase_receipt_outstanding') }}" role="tab" aria-controls="outstanding" aria-selected="false">Outstanding</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        @include('procurement::purchase_receipt.tab.entry')
                    </div>
                </div>                
            </div>
        </div>
    </div>
@endsection
