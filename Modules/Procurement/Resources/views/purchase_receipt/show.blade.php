@extends('procurement::layouts.app')

@section('procurement::title', 'Detail')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Purchase Receipt',
        'active' => true,
        'url' => route('procurement.purchase_receipt.index')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Detail',
        'active' => false,
    ])
@endsection

@section('procurement::content')
    <div class="row">
        @include('procurement::purchase_receipt.partials.header_detail')
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    {{-- <ul class="nav nav-tabs" id="myTab2" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">View Purchase Receipt</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">All Payment</a>
                        </li>
                    </ul> --}}
                    <div class="tab-content tab-bordered" id="myTab3Content">
                        <div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
                            @include('procurement::purchase_receipt.partials.purchase_receipt_detail')
                        </div>
                        <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
                            <div id="js-history"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script>
        const data = {!! json_encode($purchaseReceipt->id) !!}
        function fetchHistory(){
            fetch(`/procurement/purchase_receipt/payment-history/${data}`)
                .then(response => response.text())
                .then(html => {
                    document.querySelector('#js-history').innerHTML = html
                })
                .catch()
        }
        fetchHistory()
    </script>
@endpush
