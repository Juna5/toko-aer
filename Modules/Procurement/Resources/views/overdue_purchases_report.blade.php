@extends('procurement::layouts.app')

@section('procurement::title', 'Overdue Purchases Report')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'Procurement',
    'active' => true,
    'url' => route('procurement.view')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Overdue Purchases Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Purchase Invoice Number</th>
                                <th>Transfered From</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                                <th>Created By</th>
                                <th>Outstanding</th>
                                <th>Status</th>
                            </tr>

                            @forelse($invoice as $index => $row)
                                @php
                                    if ($row->payment_status == 'Paid') {
                                        $status = 'badge badge-success';
                                    } elseif ($row->payment_status == 'Paid Partially') {
                                        $status = 'badge badge-warning';
                                    } elseif ($row->payment_status == 'Unpaid') {
                                        $status = 'badge badge-primary';
                                    }
                                @endphp
                                <tr align="center">
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('procurement.purchase_invoice.show', $row->id) }}"
                                           target="_blank">{{ $row->invoice_number }}</a>
                                    </td>
                                    <td>
                                        <a href="{{ route('procurement.purchase-order.detail', $row->purchaseOrder->id) }}"
                                           target="_blank">{{ $row->purchaseOrder->code_number }}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->invoice_date) }}</td>
                                    <td>{{ format_d_month_y($row->invoice_due_date) }}</td>
                                    <td style="text-align: right;">{{ amount_international_with_comma($row->total) }}</td>
                                    <td>{{ $row->employee_id ? optional($row->employee)->name : '-' }}</td>
                                    <td style="text-align: right;">{{ amount_international_with_comma($row->outstanding) }}</td>
                                    <td><span class="{{ $status }}">{{ ucfirst($row->payment_status) }}</span></td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="row clearfix" style="margin-top:20px">
                            <div class="col-md-12">
                                <div class="float-right">
                                    <table class="table table-bordered table-hover" id="tab_logic_total">
                                        <tbody id="grand_total">
                                        <tr>
                                            <th class="text-center">Grand Total</th>
                                            <td class="text-center">
                                                {{ amount_international_with_comma($total) }}   
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="text-left" style="margin-bottom: 2%">
                            <a href="{{ route('procurement.view') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection