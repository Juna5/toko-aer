<div class="tab-pane fade" id="so" role="tabpanel" aria-labelledby="so-tab">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-md">
                        <tr>
                            <th style="text-align:center;">Code Number</th>
                            <th style="text-align:center;">Date</th>
                            <th style="text-align:center;">Employee</th>
                            <th style="text-align:center;">Total Amount</th>
                            <th style="text-align:center;">Action</th>
                        </tr>
                        @foreach($salesOrder as $row)
                        <tr>
                            <td style="text-align:center;">{{ $row->code_number }}</td>
                            <td style="text-align:center;">{{ format_d_month_y($row->date) }}</td>
                            <td style="text-align:center;">{{ optional($row->employee)->name }}</td>
                            <td style="text-align:right;">{{ amount_international_with_comma($row->total) }}</td>
                            <td style="text-align:center;">
                                <a href="{{ route('procurement.master.customer.show', $row['id']) }}" class="btn btn-info modal-sales_order">
                                    Detail
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@push('javascript')
@include('procurement::master.customer.modals.sales_order.index')
<script>

        // show detail data in modal sales order
        $('.modal-sales_order').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-sales_order").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
            .done(function(response) {
                $("#modal-sales_order").find('.modal-body').html(response);
            });

        });
    </script>
    @endpush                    