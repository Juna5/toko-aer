@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Product Price')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Product Price',
'active' => true,
'url' => route('procurement.master.product.edit', $productPrice->product_id)
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.master.product_price.edit', $productPrice->id)
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">

                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">

                        {{ Form::model($productPrice, array('route' => array('procurement.master.product_price.update', $productPrice->id), 'method' => 'PUT', 'files' => true)) }}
                        <div class="card-body">
                            <h5 class="card-title">
                                Edit Product Price
                                <div class="text-right">
                                    <a href="{{ route('procurement.master.product.edit', ['product' => $productPrice->product_id]) }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            <div class="form-group pb-1">
                                <label for="photo_profile">UoM</label>
                                <select class="form-control r-0 light s-12 select2" name="uom_id">
                                    <option value="">--Please Select--</option>
                                    @foreach($uom as $row)
                                    @if(old('uom_id'))
                                    <option value="{{ $row->id }}" {{ old('uom_id') == $row->id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                                    @else
                                    <option value="{{ $row->id }}" {{ $row->id == $productPrice->uom_id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group pb-1">
                                <label for="photo_profile">Price Category</label>
                                <select class="form-control r-0 light s-12 select2" name="price_category_id">
                                    <option value="">--Please Select--</option>
                                    @foreach($priceCategory as $item)
                                    @if( old('price_category_id') )
                                    <option value="{{ $item->id }}" {{ old('price_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                    @else
                                    <option value="{{ $item->id }}" {{ $item->id == $productPrice->price_category_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group pb-1">
                                        <label for="photo_profile">Price</label>
                                        <input type="text" style="text-align: right;" class="form-control separator currency" value="{{ $productPrice->price }}">
                                        <input type="hidden" name="price" class="separator-hidden" value="{{ old('price') ?? $productPrice->price }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group pb-1">
                                        <label for="photo_profile">Profit Margin</label>
                                        <input type="text" style="text-align: right;" class="form-control separator currency" value="{{ $productPrice->profit_margin }}">
                                        <input type="hidden" name="profit_margin" class="separator-hidden" value="{{ old('profit_margin') ?? $productPrice->profit_margin }}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group pb-1">
                                        <label for="photo_profile">Discount % (Rate)</label>
                                        <input type="number" style="text-align: right;" class="form-control" max="100" min="0" name="discount" placeholder="Max: 100" value="{{ old('discount') ?? $productPrice->discount}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group pb-1">
                                        <label for="photo_profile">Discount (Amount)</label>
                                        <input type="text" style="text-align: right;" class="form-control separator currency" name="discount_amount" placeholder="IDR" value="{{ old('discount_amount') ?? $productPrice->discount_amount}}">
                                        <input type="hidden" name="discount_amount" class="separator-hidden" value="{{ old('discount_amount') ?? $productPrice->discount_amount }}">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-body">
                                {{ Form::submit('Update', array('class' => 'btn btn-primary btn-lg')) }}
                            </div>
                            {{ Form::close() }}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
