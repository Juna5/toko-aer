@extends('procurement::layouts.app')

@section('procurement::title', 'Product Price')

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Product Price',
    'active' => true,
    'url' => route('procurement.master.product_price.index')
    ])
@endsection

@section('procurement::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        @include('flash::message')

        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
