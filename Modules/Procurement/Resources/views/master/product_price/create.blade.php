@extends('procurement::layouts.app')

@section('procurement::title', 'Add Product Price')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Product Price',
'active' => true,
'url' => route('procurement.master.product.edit', Request()->product)
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('procurement.master.product_price.create', ['product' => Request()->product])
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.master.product.edit', Request()->product) }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                {{ Form::open(['route' => 'procurement.master.product_price.store', 'files' => true]) }}
                <div class="card-body">
                    <input type="hidden" name="product_id" value="{{ Request()->product }}">
                    <div class="form-group pb-1">
                        <label for="photo_profile">UoM</label>
                        <select class="form-control r-0 light s-12 select2" name="uom_id">
                            <option value="">--Please Select--</option>
                            @foreach($uom as $row)
                            <option value="{{ $row->id }}" {{ old('uom_id') == $row->id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group pb-1">
                        <label for="photo_profile">Price Category</label>
                        <select class="form-control r-0 light s-12 select2" name="price_category_id">
                            <option value="">--Please Select--</option>
                            @foreach($priceCategory as $item)
                            <option value="{{ $item->id }}" {{ old('price_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Price</label>
                                <input type="text" style="text-align: right;" class="form-control separator currency" value="{{ old('price') }}">
                                <input type="hidden" name="price" class="separator-hidden" value="{{ old('price') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Profit Margin</label>
                                <input type="text" style="text-align: right;" class="form-control separator currency" value="{{ old('profit_margin') }}">
                                <input type="hidden" name="profit_margin" class="separator-hidden" value="{{ old('profit_margin') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Discount % (Rate)</label>
                                <input type="number" max="100" min="0" style="text-align: right;" class="form-control" name="discount" placeholder="Max: 100" value="{{ old('discount') }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Discount (Amount)</label>
                                <input type="text" style="text-align: right;" class="form-control separator currency" name="discount_amount" placeholder="IDR" value="{{ old('discount_amount') }}">
                                <input type="hidden" name="discount_amount" class="separator-hidden" value="{{ old('discount_amount') }}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg')) }}
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>

    </div>
</div>
</div>

@endsection
