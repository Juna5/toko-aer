@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Supplier')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Supplier',
'active' => true,
'url' => route('procurement.master.supplier.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.master.supplier.edit', $supplier->id)
])
@endsection

@push('stylesheet')
<link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('procurement::content')
<div class="row">
    <div class="col-12 col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Supplier</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">PO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Purchase Invoice</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="document" aria-selected="false">Document</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="card-body">
                            <h5 class="card-title">
                                <div class="text-right">
                                    <a href="{{ route('procurement.master.supplier.index') }}" class="btn btn-warning">
                                        <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                    </a>
                                </div>
                            </h5>
                            <hr>
                            @include('flash::message')
                            @include('include.error-list')
                            <div class="row">
                                <div class="col-12">
                                    <form action="{{ route('procurement.master.supplier.update', $supplier->id) }}" method="POST" enctype="multipart/form-data">
                                        @method('PUT')
                                        @include('procurement::master.supplier.form', [
                                        'submitButtonText' => 'Update'
                                        ])
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @include('procurement::master.components.tab.pr')
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover table-md">
                                            <tr>
                                                <th style="text-align:center;">Purchase Invoice Number</th>
                                                <th style="text-align:center;">Supplier Name</th>
                                                <th style="text-align:center;">Total Amount</th>
                                                <th style="text-align:center;">Action</th>
                                            </tr>
                                            @foreach($purchaseInvoice as $row)
                                                <tr>
                                                    <td style="text-align:center;">{{ $row->invoice_number }}</td>
                                                    <td style="text-align:center;">{{ $row->supplier_name }}</td>
                                                    <td style="text-align:right;">{{ amount_international_with_comma($row->total) }}</td>
                                                    <td style="text-align:center;">
                                                        <a href="{{ route('procurement.master.supplier.showPurchaseInvoice', $row['id']) }}" class="btn btn-info modal-purchase-invoice">
                                                            Detail
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="document-tab">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    @include('flash::message')
                                    @include('include.error-list')
                                      {{ Form::model($supplier, array('route' => array('procurement.master.supplier.document', $supplier->id), 'method' => 'PUT', 'files' => true)) }}
                                        <div class="card-body">
                                            <div class="form-group m-form__group row">
                                                <div class='form-group col-md-12'>
                                                    <a class="btn btn-primary white add-image-btn-document pull-right" data-name="document" style="color: white"><i class="icon wb-plus"></i>Add More Document</a>
                                                    <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
                                                </div>
                                               {{--  <input type="hidden" name="documents" value="1"> --}}

                                                @foreach($document as $index => $val)

                                                <div class='form-group col-md-3'>
                                                    <input type="file" name='document[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getFullUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf" />
                                                </div>
                                                @endforeach
                                                <span id="document-btm"></span>
                                            </div>
                                            <hr>
                                            <div class="card-body">
                                                {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg')) }}
                                            </div>
                                        {{ Form::close() }}
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

    @push('javascript')
        @include('procurement::master.supplier.modals.index')
        @include('procurement::master.supplier.modals.purchase_invoice.index')
        <script>
        $('.dropify').dropify();
        $('.add-image-btn-document').click(function() {
            var name = $(this).data('name');
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
            $('.dropify').dropify();
        });
        @include('include.dropify-remove-image')
        // show detail data in modal purchase order
        $('.modal-po').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-po").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function(response) {
                    $("#modal-po").find('.modal-body').html(response);
                });
        });

        // show detail data in modal purchase invoice
        $('.modal-purchase-invoice').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-purchase-invoice").modal('show');

            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'html',
            })
                .done(function(response) {
                    $("#modal-purchase-invoice").find('.modal-body').html(response);
                });

        });
        </script>

        <script src="{{ asset('js/dropify.min.js') }}"></script>
        <script>
            $(document).ready(function(){
                $('.dropify').dropify();
            });
            $('.dropify').dropify();
            $('.add-image-btn').click(function() {
                var name = $(this).data('name');
                $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
                $('.dropify').dropify();
            });
            @include('include.dropify-remove-image')
        </script>
    @endpush
