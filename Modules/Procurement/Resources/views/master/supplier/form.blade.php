@csrf
<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group pb-1">
                    <label for="photo_profile">Company Name</label>
                    <input type="text" class="form-control r-0 light s-12" name="company_name" value="{{ old('company_name') ?? $supplier->company_name }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Supplier Name</label>
                    <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $supplier->name }}">
                </div>
            </div>
            {{-- <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">ROC No.</label>
                    <input type="text" class="form-control r-0 light s-12 " name="roc" value="{{ old('roc') ?? $supplier->roc }}">
                </div>
            </div> --}}
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Phone</label>
                        <input type="text" class="form-control r-0 light s-12" name="phone" value="{{ old('phone') ?? $supplier->phone }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Fax</label>
                    <input type="text" class="form-control r-0 light s-12" name="fax" value="{{ old('fax') ?? $supplier->fax }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Email</label>
                    <input type="email" class="form-control r-0 light s-12" name="email" value="{{ old('email') ?? $supplier->email }}">
                </div>
            </div>
        </div>
       
        
        <div class="row">
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Price Category</label>
                    <select name="price_category_id" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($priceCategory as $item)
                            @if( old('price_category_id') )
                                <option value="{{ $item->id }}" {{ old('price_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @else
                                <option value="{{ $item->id }}" {{ $item->id == $supplier->price_category_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Currency</label>
                    <select name="currency_id" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($currency as $item)
                            @if( old('currency_id') )
                                <option value="{{ $item->id }}" {{ old('currency_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @else
                                <option value="{{ $item->id }}" {{ $item->id == $supplier->currency_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Payment Term</label>
                    <select name="payment_term_id" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($paymentTerm as $item)
                            @if( old('payment_term_id') )
                                <option value="{{ $item->id }}" {{ old('payment_term_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @else
                                <option value="{{ $item->id }}" {{ $item->id == $supplier->payment_term_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label>Deposit</label>
                    <input type="text" class="form-control separator currency" style="text-align: right;" value="{{ $supplier->deposit }}">
                    <input type="hidden" name="deposit" class="separator-hidden" value="{{ old('deposit') ?? $supplier->deposit }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Address</label>
                    <textarea name="address" id="" cols="50" rows="4" style="height: 20%" class="form-control r-0 light s-12">{{ old('address') ?? $supplier->address }}</textarea>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Description</label>
                    <textarea name="description" id="" cols="50" rows="4" style="height: 20%" class="form-control r-0 light s-12">{{ old('description') ?? $supplier->description }}</textarea>
                </div>
            </div>
        </div>
        
            <div class="form-group m-form__group row">
                <div class="form-group col-md-12">
                    <a class="btn btn-primary white add-image-btn" data-name="attachment" style="color: white"><i class="icon wb-plus"></i>Add More Attachment</a>
                    <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
                </div>

                @if(!empty($media))
                    @foreach($media as $index => $val)
                    <div class='form-group col-md-3'>
                        <input type="file" name='attachment[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getFullUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf">
                        <span>
                            <br>
                                <a href="{{ asset($val->getFullUrl()) }}"
                                class="btn btn-success white add-image-btn pull-right" target="_blank">
                                    <i class="fa fa-file text-white" ></i> View File
                                </a>
                        </span>
                    </div>
                    @endforeach

                @else
                    @foreach (range(1, 4) as $i)
                    <div class='form-group col-md-3'>
                        <input type='file' name='attachment[{{ $i }}]' class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf">
                    </div>
                    @endforeach
                @endif
                    <span id="attachment-btm"></span> 
            </div>
        
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>