<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Company Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Phone</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Fax</th>
    {{-- <th style="background-color:#e2e25f; font-weight: bold;" align="center">ROC No.</th> --}}
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Product Category</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Email</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Currency</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Payment Term</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Address</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->company_name}}</td>
    <td>{{$row->phone}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row->fax}}</td>
    {{-- <td>{{$row->roc}}</td> --}}
    <td>{{optional($row->productCategory)->name}}</td>
    <td>{{$row->email}}</td>
    <td>{{optional($row->currency)->name}}</td>
    <td>{{optional($row->paymentTerm)->name}}</td>
    <td>{{$row->address}}</td>
    <td>{{$row->description}}</td>
  </tr>
  @endforeach
</table>