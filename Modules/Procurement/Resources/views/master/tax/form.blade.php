@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Code</label>
            <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') ?? $tax->code }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $tax->name }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $tax->description }}</textarea>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Rate</label>
            <textarea name="rate" id="" cols="30" rows="10" class="form-control r-0 light s-12" style="text-align: right;">{{ old('rate') ?? $tax->rate }}</textarea>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">From Date</label>
            <input type="date" class="form-control datepicker" name="from_date" datepicker value="{{ old('from_date') ?? $tax->from_date ? : now()->format('Y-m-d') }}">
        </div>
         <div class="form-group pb-1">
            <label for="photo_profile">To Date</label>
            <input type="date" class="form-control datepicker" name="until_date" datepicker value="{{ old('until_date') ?? $tax->until_date ? : now()->endOfMonth()->format('Y-m-d') }}">
        </div>
        
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>