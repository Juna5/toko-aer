<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Code</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Rate</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">From Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">To Date</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->code}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row->description}}</td>
    <td>{{$row->rate}}</td>
    <td>{{$row->from_date}}</td>
    <td>{{$row->until_date}}</td>
  </tr>
  @endforeach
</table>