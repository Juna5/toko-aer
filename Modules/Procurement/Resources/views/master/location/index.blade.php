@extends('procurement::layouts.app')

@section('procurement::title', 'Location')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'General Settings',
    'active' => true,
    'url' => route('procurement.master.index')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Location',
    'active' => true,
    'url' => route('procurement.master.location.index')
    ])
@endsection

@section('procurement::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        @include('flash::message')

        <div class="card-body">
            <div class="form-group pb-1">
                <a href="{{ route('procurement.master.location/export') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                    <img src="{{asset('images/excel.png')}}" alt="">
                </a>
            </div>
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
