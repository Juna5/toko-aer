@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Business Role')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Business Role',
'active' => true,
'url' => route('procurement.master.business_role.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.master.business_role.edit', $businessRole->id)
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.master.business_role.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('procurement.master.business_role.update', $businessRole->id) }}" method="POST" enctype="multipart/form-data">
                            @method('PUT')
                            @include('procurement::master.business_role.form', [
                            'submitButtonText' => 'Update'
                            ])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
