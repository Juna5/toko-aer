@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Code</label>
            <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') ?? $product->code }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $product->name }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Brand</label>
            <input type="text" class="form-control r-0 light s-12" name="brand" value="{{ old('brand') ?? $product->brand }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Product Category</label>
            <select class="form-control r-0 light s-12 select2" name="productcategory_id">
                <option value="">--Please Select--</option>
                @foreach($productCategory as $item)
                @if( old('productcategory_id') )
                <option value="{{ $item->id }}" {{ old('productcategory_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                @else
                <option value="{{ $item->id }}" {{ $item->id == $product->productcategory_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">UoM 1</label>
            <input type="text" class="form-control r-0 light s-12" name="uom_1" value="{{ old('uom_1') ?? $product->uom_1 }}">
        </div>
        {{-- <div class="form-group pb-1">
            <label for="photo_profile">UoM 2</label>
            <input type="text" class="form-control r-0 light s-12" name="uom_2" value="{{ old('uom_2') ?? $product->uom_2 }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Unit Conversion</label>
            <input type="text" class="form-control r-0 light s-12" name="unit_conversion" value="{{ old('unit_conversion') ?? $product->unit_conversion }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Location</label>
            <select class="form-control r-0 light s-12 select2" name="location_id">
                <option value="">--Please Select--</option>
                @foreach($location as $item)
                @if( old('location_id') )
                <option value="{{ $item->id }}" {{ old('location_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                @else
                <option value="{{ $item->id }}" {{ $item->id == $product->location_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                @endif
                @endforeach
            </select>
        </div>  --}}
        <div class="form-group pb-1">
            <label for="photo_profile">Warehouse</label>
            <select class="form-control r-0 light s-12 select2" name="warehouse_id">
                <option value="">--Please Select--</option>
                @foreach($warehouseMaster as $item)
                    @if( old('warehouse_id') )
                    <option value="{{ $item->id }}" {{ old('warehouse_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                    @else
                    <option value="{{ $item->id }}" {{ $item->id == $product->warehouse_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Dimension</label>
            <input type="text" class="form-control r-0 light s-12" name="dimension" value="{{ old('dimension') ?? $product->dimension }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Weight</label>
            <input type="text" class="form-control r-0 light s-12" name="weight" value="{{ old('weight') ?? $product->weight }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Expired Date</label>
            <input type="text" class="form-control datepicker" name="expired_date" datepicker value="{{ old('expired_date') ?? $product->expired_date}}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Price Category</label>
            <select class="form-control r-0 light s-12 select2" name="price_category_id">
                <option value="">--Please Select--</option>
                @foreach($priceCategory as $item)
                @if( old('price_category_id') )
                <option value="{{ $item->id }}" {{ old('price_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                @else
                <option value="{{ $item->id }}" {{ $item->id == $product->price_category_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                @endif
                @endforeach
            </select>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Price</label>
            <input type="text" class="form-control" name="price" datepicker value="{{ old('price') ?? $product->price}}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Discount</label>
            <input type="text" class="form-control" name="discount" datepicker value="{{ old('discount') ?? $product->discount}}">
        </div>
        <div class="form-group m-form__group row">
            <div class='form-group col-md-12'>
                <a class="btn btn-primary white add-image-btn pull-right" data-name="file" style="color: white"><i class="icon wb-plus"></i>Add More Photo</a>
                <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
            </div>
            @foreach($media as $index => $val)
            <div class='form-group col-md-3'>
                <input type="file" name='file[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf" />
            </div>
            @endforeach
            <span id="file-btm"></span>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $product->description }}</textarea>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Submit' }}</button>
</div>
@push('javascript')
<script type="text/javascript">
    $('.dropify').dropify();
    $('.add-image-btn').click(function() {
        var name = $(this).data('name');
        console.log(name);
        $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
        $('.dropify').dropify();
    });
    @include('purchaserequisition::inc.dropify-remove-image')
</script>
@endpush