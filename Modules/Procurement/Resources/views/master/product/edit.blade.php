@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Product')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Product',
'active' => true,
'url' => route('procurement.master.product.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.master.product.edit', $product->id)
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-12 col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="edit-detail-tab" data-toggle="tab" href="#edit-detail" role="tab" aria-controls="edit-detail" aria-selected="false">Detail</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document-tab" data-toggle="tab" href="#product_price" role="tab" aria-controls="product_price" aria-selected="false">Price</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document-tab" data-toggle="tab" href="#price_history" role="tab" aria-controls="price_history" aria-selected="false">Price History</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">PO</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="so-tab" data-toggle="tab" href="#so" role="tab" aria-controls="so" aria-selected="false">Sales Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="location-tab" data-toggle="tab" href="#location" role="tab" aria-controls="location" aria-selected="false">Location</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="document-tab" data-toggle="tab" href="#document" role="tab" aria-controls="document" aria-selected="false">Document</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    @include('procurement::master.product.tab.product')
                    @include('procurement::master.product.tab.product_price')
                    @include('procurement::master.product.tab.price_history')
                    @include('procurement::master.components.tab.pr')
                    @include('procurement::master.components.tab.so')
                    @include('procurement::master.product.tab.location')
                    @include('procurement::master.product.tab.document')
                    @include('procurement::master.product.tab.edit-detail-product')
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('javascript')
    @include('procurement::master.product.tab.modals.purchase_order.index')
    @include('procurement::master.product.tab.modals.sales_order.index')

        <script>
            $('#btn-add-sub').hide()
            $('#selectCategory').on('change', function(e){
                $('#category').val(e.target.value)
                var id = e.target.value
                $('#product_category_id').val(id)
                $.post('{{ route('api.procurement.product_sub_category') }}', {type:'product_category', id: $('#selectCategory').val()}, function(e){
                    $('#selectSubCategory').html(e);
                })
                $('#btn-add-sub').show()
            })

            $(document).ready(function() {
                $("#button").click(function(e) {
                   var id = $('#category').val()
                   e.preventDefault();
                   window.open('{{ route('procurement.master.product_sub_category.create') }}?categoryId='+id, "_blank")
                });
            });
        </script>
        <script>
        // show detail data in modal purchase order
        $('.modal-po').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-po").modal('show');

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                })
                .done(function(response) {
                    $("#modal-po").find('.modal-body').html(response);
                });

        });
    </script>

    <script>
        $('.modal-so').click(function(event) {
            event.preventDefault();

            var url = $(this).attr('href');

            $("#modal-so").modal('show');

            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'html',
                })
                .done(function(response) {
                    $("#modal-so").find('.modal-body').html(response);
                });

        });
    </script>

    <script type="text/javascript">
        $('.dropify').dropify();
        $('.add-image-btn').click(function() {
            var name = $(this).data('name');
            console.log(name);
            $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
            $('.dropify').dropify();
        });
        $('.add-image-btn-document').click(function() {
            var name = $(this).data('name');
            console.log(name);

            $('#' + name + '-btm').before("<div class='form-group col-md-4'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
            $('.dropify').dropify();
        });
    </script>
    @endpush
