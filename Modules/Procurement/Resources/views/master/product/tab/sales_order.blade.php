<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <tr>
                            <th>Code Number</th>
                            <th>Employee</th>
                            <th>Action</th>
                        </tr>
                        @foreach($salesOrder as $row)
                            <tr>
                                <td>{{ $row->code_number }}</td>
                                <td>{{ optional($row->employee)->name }}</td>
                                <td>
                                    <a href="{{ route('procurement.master.product.showSO', $row['id']) }}" class="btn btn-info modal-so">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
