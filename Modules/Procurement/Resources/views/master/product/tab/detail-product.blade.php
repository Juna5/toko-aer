 <div class="tab-pane  show" id="detail" role="tabpanel" aria-labelledby="detail-tab">
 	<div class="card-body">
 		<div class="row">
 			<div class="col-md-12">
 				<div class="row">
 					<div class="col-md-3">
 						<div class="form-group pb-1">
 							<label for="photo_profile">Dimension</label>
 							<input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="dimension" value="{{ old('dimension') }}" id="dimension">
 						</div>
 					</div>
 					<div class="col-md-3">
 						<div class="form-group pb-1">
 							<label for="photo_profile">Weight</label>
 							<input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="weight" value="{{ old('weight') }}" id="weight">
 						</div>
 					</div>
 					<div class="col-md-3">
 						<div class="form-group pb-1">
 							<label for="photo_profile">Weight Class</label>
 							<select class="form-control r-0 light s-12 select2" name="weight_class_id">
 								<option value="">--Please Select--</option>
 								@foreach($weightClass as $wc)
 								<option value="{{ $wc->id }}" {{ old('weight_class_id') == $wc->id ? 'selected' : ''  }}>{{ $wc->name  }}</option>
 								@endforeach
 							</select>
 						</div>
 					</div>
 				</div>
 				<div class="form-group pb-1">
 					<label for="photo_profile">Further & More Detailed Description</label>
 					<textarea name="detail_description" id="" cols="30" rows="10" style="height:300px;" class="form-control r-0 light s-12">{{ old('detail_description') }}</textarea>
 				</div>
 			</div>
 		</div>
 	</div>
 </div>
