<div class="row">
    <div class="col-lg-12">
        <div class="invoice-title">
            <div class="row">
                <div class="col-md-6">
                    <div class="invoice-number text-primary ">{{ format_d_month_y($row->date) }} <br>{{ optional($row->employee)->name }} </div>
                </div>
                <div class="col-md-6 text-md-right">
                    <div class="invoice-number text-primary ">{{ $row->code_number }}</div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table table-hover table-md">
                        <!-- <tr>
                            <td width="30%">Title</td>
                            <td width="2%">:</td>
                            <td>{{ $row->title }}</td>
                        </tr> -->
                        @if($row->quotation_material_id)
                        <tr>
                            <td width="30%">Quotation</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->quotationMaterial)->code_number }}</td>
                        </tr>
                        @endif
                        <tr>
                            <td width="30%">Customer</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->customer)->company_name }}</td>
                        </tr>
                        <tr>
                            <td width="30%">Salesman</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->salesman)->name }}</td>
                        </tr>
                        <!-- <tr>
                            <td width="30%">Department</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->department)->name }}</td>
                        </tr> -->
                        <tr>
                            <td width="30%">Payment Term</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->paymentTerm)->code . ' - ' . optional($row->paymentTerm)->name }}</td>
                        </tr>
                        <tr>
                            <td width="30%">Currency</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->currency)->name }}</td>
                        </tr>
                        <tr>
                            <td width="30%">Description</td>
                            <td width="2%">:</td>
                            <td>{{ $row->description }}</td>
                        </tr>


                    </table>
                </div>
            </div>
            <div class="col-md-6">
                <div class="table-responsive">
                    <table class="table  table-hover table-md">

                        <tr>
                            <td width="30%">Status</td>
                            <td width="2%">:</td>
                            <td>{{ ucfirst($row->status) }}</td>
                        </tr>
                        <tr>
                            <td width="30%">Final Approver</td>
                            <td width="2%">:</td>
                            <td>{{ optional($row->latestApprove)->name }}</td>
                        </tr>
                        <tr>
                            <td width="30%">Next Approver</td>
                            <td width="2%">:</td>
                            <td>{{ $row->approveToGo ? optional($row->approveToGo)->name : ($row->role_approval == null ? '-' : $row->role_approval) }}</td>
                        </tr>
                        @if($row->rejected_by)
                            <tr>
                                <td width="30%">Rejected By</td>
                                <td width="2%">:</td>
                                <td>{{ optional($row->rejectedBy)->name }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Rejected At</td>
                                <td width="2%">:</td>
                                <td>{{ format_d_month_y($row->rejected_at) }}</td>
                            </tr>
                            <tr>
                                <td width="30%">Rejected Reason</td>
                                <td width="2%">:</td>
                                <td>{{ $row->rejected_reason }}</td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<legend class="text-center">Item</legend>
<div class="table-responsive">
<table class="table table-striped table-hover table-md">
    <tr>
        <th>Item</th>
        <th>UoM</th>
        <th>Qty</th>
        <th>Price</th>
        <th>Dsc Rate (%)</th>
        <th>Dsc Amount</th>
        <th>Description</th>
        <th>Sub Total</th>
    </tr>
    @foreach($items as $item)
    <tr>
        <td>{{ optional($item->product)->name }}</td>
        <td>{{ optional($item->uom)->name }}</td>
        <td>{{ $item->qty }}</td>
        <td  align="right">{{ number_format($item->price) }}</td>
        <td>{{ $item->discount_rate }}</td>
        <td  align="right">{{ number_format($item->discount_amount) }}</td>
        <td>{{ $item->description }}</td>
        <td  align="right">{{ number_format($item->sub_total) }}</td>
    </tr>
    @endforeach
    <tr>
        <td colspan="7" align="right">Subtotal Exclude Discount All Items</td>
        <td  align="right">{{ number_format($row->get_total) }}</td>
    </tr>
    <tr>
        <td colspan="7" align="right">Discount All Items </td>
        <td  align="right">{{ number_format($row->discount_all_item) }}</td>
    </tr>
    <tr>
        <td colspan="7" align="right">Sub Total</td>
        <td  align="right">{{ number_format($row->sub_total) }}</td>
    </tr>
    <tr>
        <td colspan="6" align="right">GST</td>
        <td align="right"> {{ optional($row->tax)->rate}}%</td>
        <td  align="right">{{ number_format(($row->tax->rate / 100) * $row->sub_total) }}</td>
    </tr>
    <tr>
        <td colspan="7" align="right">Total</td>
        <td  align="right"  style="color:#5b35c4; font-weight: bold;">{{ number_format($row->total) }}</td>
    </tr>
</table>
</div>

    <legend class="text-center">Attachment</legend>
    <div class="form-group m-form__group row">
    @foreach($media as $index => $med)
        <div class='form-group col-md-2'>
            <div style="display:block;position:relative;overflow:hidden;width:100%;max-width:100%;height:200px;padding:5px 10px;font-size:14px;line-height:22px;color:#777;background-color:#FFF;background-image:none;text-align:center;">
                <img src="{{ asset($med->getFullUrl()) }}" data-default-file="{{ asset($med->getFullUrl()) }}" style="top:50%;-webkit-transform:translate(0,-50%);transform:translate(0,-50%);position:relative;max-width:100%;max-height:100%;background-color:#FFF;-webkit-transition:border-color .15s linear;transition:border-color .15s linear;">

            </div>
            <span>
            <br>
                <a href="{{ asset($med->getFullUrl()) }}" class="btn btn-success white add-image-btn pull-right" target="_blank">
                    <i class="fa fa-file text-white" ></i> View File
                </a>
            </span>
        </div>
    @endforeach
    </div>

<div class="col-lg-12">
    <legend class="text-center">History</legend>
    <div class="table-responsive">
        <table class="table table-striped table-hover table-md">
            <tr>
                <th>Employee</th>
                <th>Status</th>
                <th>Date</th>
                <th>Description</th>
            </tr>
            @foreach($approvals as $approve)
            <tr>
                <td>{{ $approve->role}} {{ $approve->role &&  optional($approve->employee)->name ?'-': '' }} {{  optional($approve->employee)->name }}</td>
                <td>{{ $approve->is_approved ? "Approved" : ($row->status == 'rejected' ? 'Rejected' : 'Waiting') }}</td>
                <td>{{ format_d_month_y($approve->date) }}</td>
                <td>{{ $approve->description }}</td>
            </tr>
            @endforeach
        </table>
    </div>
</div>
<hr>
<div class="modal-footer">
<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
</div>
</div>
