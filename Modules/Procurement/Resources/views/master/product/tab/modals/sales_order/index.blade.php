<div class="modal fade" id="modal-so" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-header">
                <legend class="text-center">Sales Order Detail</legend>
            </div>

            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>
