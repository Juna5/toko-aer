<div class="tab-pane fade" id="document" role="tabpanel" aria-labelledby="location-tab">
    <div class="card-body">

        <hr>
        @include('flash::message')
        @include('include.error-list')
        <div class="row">
            <div class="col-12">
                {{ Form::model($product, array('route' => array('procurement.master.product.update', $product->id), 'method' => 'PUT', 'files' => true)) }}
                <div class="card-body">
                    <div class="form-group m-form__group row">
                        <div class='form-group col-md-12'>
                            <a class="btn btn-primary white add-image-btn-document pull-right" data-name="document" style="color: white"><i class="icon wb-plus"></i>Add More Document</a>
                            <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
                        </div>
                        <input type="hidden" name="documents" value="1">
                        @foreach($document as $index => $val)
                        <div class='form-group col-md-3'>
                            <input type="file" name='document[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getFullUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf" />
                        </div>
                        @endforeach
                        <span id="document-btm"></span>
                    </div>
                    <hr>
                    <div class="card-body">
                        {{ Form::submit('Submit', array('class' => 'btn btn-primary btn-lg')) }}
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>

@push('javascript')
<script type="text/javascript">
    $('.dropify').dropify();
    $('.add-image-btn').click(function() {
        var name = $(this).data('name');
        $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
        $('.dropify').dropify();
    });
    @include('include.dropify-remove-image')
</script>
@endpush
