<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                        <tr>
                            <th>Code Number</th>
                            <th>Employee</th>
                            <th>Action</th>
                        </tr>
                        @foreach($purchaseOrder as $row)
                            <tr>
                                <td>{{ $row->code_number }}</td>
                                <td>{{ optional($row->employee)->name }}</td>
                                <td>
                                    <a href="{{ route('procurement.master.product.show', $row['id']) }}" class="btn btn-info modal-po">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
                {{-- {{ $purchaseOrder->links() }} --}}
            </div>
        </div>
    </div>
</div>
