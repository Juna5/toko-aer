<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div class="card-body">

        <hr>
        @include('flash::message')
        @include('include.error-list')
        <div class="row">
            <div class="col-12">

                {{ Form::model($product, array('route' => array('procurement.master.product.update', $product->id), 'method' => 'PUT', 'files' => true)) }}
                <div class="card-body">
                    <h5 class="card-title">
                        Edit Product
                        <div class="text-right">
                            <a href="{{ route('procurement.master.product.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </h5>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Name</label>
                                <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $product->name }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Code</label>
                                <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') ?? $product->code }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                             <div class="form-group pb-1">
                                <label for="photo_profile">Brand</label>
                                <input type="text" class="form-control r-0 light s-12" name="brand" value="{{ old('brand') ?? $product->brand }}">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="category">
                    <div class="text-right" id="btn-add-sub">
                        <a href="{{ route('procurement.master.product_sub_category.create') }}?categoryId=" class="btn btn-info" target="_blank" id="button">
                            <i class="fa fa-plus"></i>
                            Add New Sub Category
                        </a>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Product Category</label>
                                <select class="form-control r-0 light s-12 select2" name="product_category_id" id="selectCategory">
                                    <option value="">--Please Select--</option>
                                    @foreach($productCategory as $item)
                                        @if( old('product_category_id') )
                                        <option value="{{ $item->id }}" {{ old('product_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                        @else
                                        <option value="{{ $item->id }}" {{ $item->id == $product->product_category_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Product Sub Category</label>
                                <select class="form-control r-0 light s-12 select2" name="product_sub_category_id" id="selectSubCategory">
                                    <option value="">-- Please Select --</option>
                                    @foreach($productSubCategory as $item)
                                    <option value="{{ $item->id }}" {{ $product->product_sub_category_id == $item->id ? 'selected':''}}>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">UoM</label>
                                <select class="form-control r-0 light s-12 select2" name="uom_id" required>
                                    <option value="">--Please Select--</option>
                                    @foreach($uom as $item)
                                        @if( old('uom_id') )
                                        <option value="{{ $item->id }}" {{ old('uom_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                        @else
                                        <option value="{{ $item->id }}" {{ $item->id == $product->uom_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Manufacturer</label>
                                <input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="manufacturer" value="{{ old('manufacturer') ?? $product->manufacturer }}">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                            <div class="col-md-3">
                                 <div class="form-group pb-1">
                                    <label for="photo_profile">Size / Packing</label>
                                    <input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="size_packing" value="{{ old('size_packing') ?? $product->size_packing }}">
                                </div>
                            </div>
                            
                        </div>
                            {{-- <div class="form-group pb-1">
                        <label for="photo_profile">Supplier</label>
                        <select class="form-control r-0 light s-12 select2" name="supplier_id">
                            <option value="">--Please Select--</option>
                            @foreach($supplier as $item)
                                @if( old('supplier_id') )
                                <option value="{{ $item->id }}" {{ old('supplier_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @else
                                <option value="{{ $item->id }}" {{ $item->id == $product->supplier_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>  --}}




                    {{-- <div class="form-group pb-1">
                        <label for="photo_profile">UoM 2</label>
                        <input type="text" class="form-control r-0 light s-12" name="uom_2" value="{{ old('uom_2') ?? $product->uom_2 }}">
                    </div>
                    <div class="form-group pb-1">
                        <label for="photo_profile">Unit Conversion</label>
                        <input type="text" class="form-control r-0 light s-12" name="unit_conversion" value="{{ old('unit_conversion') ?? $product->unit_conversion }}">
                    </div>
                    <div class="form-group pb-1">
                        <label for="photo_profile">Location</label>
                        <select class="form-control r-0 light s-12 select2" name="location_id">
                            <option value="">--Please Select--</option>
                            @foreach($location as $item)
                                @if( old('location_id') )
                                <option value="{{ $item->id }}" {{ old('location_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @else
                                <option value="{{ $item->id }}" {{ $item->id == $product->location_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div> --}}
                    {{-- <div class="form-group pb-1">
                        <label for="photo_profile">Warehouse</label>
                        <select class="form-control r-0 light s-12 select2" name="warehouse_id">
                            <option value="">--Please Select--</option>
                            @foreach($warehouseMaster as $item)
                                @if( old('warehouse_id') )
                                <option value="{{ $item->id }}" {{ old('warehouse_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @else
                                <option value="{{ $item->id }}" {{ $item->id == $product->warehouse_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div> --}}

                   {{-- <div class="form-group pb-1">
                        <label for="photo_profile">Expired Date</label>
                        <input type="text" class="form-control datepicker" name="expired_date" datepicker value="{{ old('expired_date') ?? $product->expired_date}}">
                    </div> --}}
                    {{-- <div class="form-group pb-1">
                        <label for="photo_profile">Supplier</label>
                        <select class="form-control r-0 light s-12 select2" name="supplier_id">
                            <option value="">--Please Select--</option>
                            @foreach($supplier as $item)
                                @if( old('supplier_id') )
                                <option value="{{ $item->id }}" {{ old('supplier_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @else
                                <option value="{{ $item->id }}" {{ $item->id == $product->supplier_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                @endif
                            @endforeach

                        </select>
                    </div>  --}}
                    <div class="form-group m-form__group row" style="margin-top: 2%">
                        <div class='form-group col-md-12'>
                            <a class="btn btn-primary white add-image-btn pull-right" data-name="file" style="color: white"><i class="icon wb-plus"></i>Add More Photo</a>
                        </div>
                        @foreach($media as $index => $val)
                        <div class='form-group col-md-3'>
                            <input type="file" name='file[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getFullUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='2M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf" />
                        </div>
                        @endforeach
                        <span id="file-btm"></span>
                    </div>
                    <div class="form-group pb-1">
                        <label for="photo_profile">Description</label>
                        <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12" style="height:100px;">{{ old('description') ?? $product->description }}</textarea>
                    </div>

                    <hr>
                    <div class="card-body">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-lg')) }}
                    </div>
                    {{ Form::close() }}

                </div>
            </div>
        </div>
    </div>
</div>
