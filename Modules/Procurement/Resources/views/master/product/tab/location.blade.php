<div class="tab-pane fade" id="location" role="tabpanel" aria-labelledby="location-tab">
     <div class="card-body">
        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table class="table table-striped table-md">
                        <tr>
                            <th>Warehouse</th>
                            <th>IN</th>
                            <th>OUT</th>
                            <th>In Hand</th>
                        </tr>
                        @foreach($stock as $row)
                            <tr>
                            	<td>{{ optional($row->warehouse)->name }}</td>
                                <!-- <td>{{ getTotalStock($row->product_id,$row->warehouse_id,'IN') ?? '0' }}</td>
                                <td>{{ getTotalStock($row->product_id,$row->warehouse_id,'OUT') ?? '0' }}</td>
                                <td>{{ getTotal($row->product_id,$row->warehouse_id) ?? '0' }}</td> -->
                                <td>{{ $row->in ?? '0' }}</td>
                                <td>{{ $row->out ?? '0' }}</td>
                                <td>{{ $row->qty ?? '0' }}</td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
