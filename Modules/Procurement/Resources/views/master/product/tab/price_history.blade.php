<div class="tab-pane fade" id="price_history" role="tabpanel" aria-labelledby="home-tab">
    <div class="card-body">
        <div class="card-body">
            <h4 class="card-title" style="text-align: center;">Penjualan History</h4>
            <hr>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                <tr>
                    <th style="text-align: center;">Customer Name</th>
                    <th style="text-align: center;">Date</th>
                    <th style="text-align: center;">Price</th>
                    <th style="text-align: center;">Amount</th>
                    <th style="text-align: center;">Sales Order Number</th>
                    <th style="text-align: center;">Action</th>
                </tr>
                @foreach($priceHistory as $row)
                @if(empty($row->status == 0))
                <tr>
                    <td style="text-align: center;">{{ optional($row->salesOrder->customer)->pic_1 }}</td>
                    <td style="text-align: center;">{{ format_d_month_y($row->salesOrder->date) }}</td>
                    <td style="text-align: right;">{{ amount_international_with_comma($row->price) }}</td>
                    <td style="text-align: right;">{{ amount_international_with_comma($row->sub_total) }}</td>
                    <td style="text-align: center;">{{ $row->salesOrder->code_number }}</td>
                    <td style="text-align: center;">
                        <a href="{{ route('procurement.master.customer.show', $row->sales_order_id) }}" class="btn btn-info modal-sales_order">
                            Detail
                        </a>
                    </td>
                </tr>
                @endif
                @endforeach
            </table>
        </div>
        {{-- {{ $priceHistory->links() }} --}}
    </div>
    <hr>
    <div class="card-body">
        <div class="card-body">
            <h4 class="card-title" style="text-align: center;">Procurement History</h4>
            <hr>
        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">
                <tr>
                    <th style="text-align: center;">Supplier Name</th>
                    <th style="text-align: center;">Date</th>
                    <th style="text-align: center;">Price</th>
                    <th style="text-align: center;">Amount</th>
                    <th style="text-align: center;">PO Number</th>
                    <th style="text-align: center;">Action</th>
                </tr>
                @foreach($priceHistoryPo as $row)
                <tr>
                    <td style="text-align: center;">{{ optional($row->purchaseOrder->supplier)->company_name }}</td>
                    <td style="text-align: center;">{{ format_d_month_y($row->purchaseOrder->date) }}</td>
                    <td style="text-align: right;">{{ amount_international_with_comma($row->price) }}</td>
                    <td style="text-align: right;">{{ amount_international_with_comma($row->sub_total) }}</td>
                    <td style="text-align: center;">{{ $row->purchaseOrder->code_number }}</td>
                    <td style="text-align: center;">
                        <a href="{{ route('procurement.master.supplier.show', $row->purchase_order_id) }}" class="btn btn-info modal-po">
                            Detail
                        </a>
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
        {{-- {{ $priceHistoryPo->links() }} --}}
    </div>
</div>
