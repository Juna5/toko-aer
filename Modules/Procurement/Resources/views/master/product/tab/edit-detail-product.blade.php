 <div class="tab-pane  show" id="edit-detail" role="tabpanel" aria-labelledby="edit-detail-tab">
        {{ Form::model($product, array('route' => array('procurement.master.product.update', $product->id), 'method' => 'PUT', 'files' => true)) }}
    <div class="card-body">
        <div class="row">
        	<div class="col-md-12">
        		<div class="row">
        			<div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Dimension</label>
                                <input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="dimension" value="{{ old('dimension') ?? $product->dimension }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Weight</label>
                                <input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="weight" value="{{ old('weight') ?? $product->weight }}">
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group pb-1">
                                <label for="photo_profile">Weight Class</label>
                                <select class="form-control r-0 light s-12 select2" name="weight_class_id">
                                    <option value="">--Please Select--</option>
                                    @foreach($weightClass as $wc)
                                        @if( old('weight_class_id') )
                                        <option value="{{ $wc->id }}" {{ old('weight_class_id') == $wc->id ? 'selected' : ''  }}>{{ $wc->name  }}</option>
                                        @else
                                        <option value="{{ $wc->id }}" {{ $wc->id == $product->weight_class_id ? 'selected' : ''  }}>{{ $wc->name  }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
        		</div>
        		 <div class="form-group pb-1">
                        <label for="photo_profile">Further & More Detailed Description</label>
                        <textarea name="detail_description" id="" cols="30" rows="10" class="form-control r-0 light s-12" style="height:300px;">{{ old('detail_description') ?? $product->detail_description }}</textarea>
                    </div>
            </div>
             <hr>
                    <div class="card-body">
                        {{ Form::submit('Update', array('class' => 'btn btn-primary btn-lg')) }}
                    </div>
                    {{ Form::close() }}
        </div>
    </div>
</div>
