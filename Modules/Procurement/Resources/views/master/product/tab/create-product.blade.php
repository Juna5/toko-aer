<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
    <div class="row">
        <div class="col-md-12">
            <div class="card no-b no-r">
                <div class="card-body">
                    @include('flash::message')
                    @include('include.error-list')
                    <h5 class="card-title">
                        <div class="text-right">
                            <a href="{{ route('procurement.master.product.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </h5>
                    <hr>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">Name</label>
                                    <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') }}" required id="name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">Code</label>
                                    <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') }}" required id="code">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">Brand</label>
                                    <input type="text" class="form-control r-0 light s-12" name="brand" value="{{ old('brand') }}">
                                </div>
                            </div>
                        </div>
                        <input type="hidden" id="category">
                        <div class="text-right" id="btn-add-sub">
                            <a href="{{ route('procurement.master.product_sub_category.create') }}?categoryId=" class="btn btn-info" target="_blank" id="button">
                                <i class="fa fa-plus"></i>
                                Add New Sub Category
                            </a>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">Product Category</label>
                                    <select class="form-control r-0 light s-12 select2" name="product_category_id" id="selectCategory">
                                        <option value="">--Please Select--</option>
                                        @foreach($productCategory as $item)
                                        <option value="{{ $item->id }}" {{ old('product_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">Product Sub Category</label>
                                    <select class="form-control r-0 light s-12 select2" name="product_sub_category_id" id="selectSubCategory">
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">UoM</label>
                                    <select class="form-control r-0 light s-12 select2" name="uom_id" id="uom_id">
                                        <option value="">--Please Select--</option>
                                        @foreach($uom as $item)
                                        <option value="{{ $item->id }}" {{ old('uom_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group pb-1">
                                    <label for="photo_profile">Manufacturer</label>
                                    <input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="manufacturer" value="{{ old('manufacturer') }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                 <div class="form-group pb-1">
                                    <label for="photo_profile">Size / Packing</label>
                                    <input type="text" class="form-control r-0 light s-12" style="text-align: right;" name="size_packing" value="{{ old('size_packing') }}">
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group m-form__group row">
                            <div class='form-group col-md-12'>
                                <a class="btn btn-primary white add-image-btn pull-right" data-name="file" style="color: white"><i class="icon wb-plus"></i>Add More Photo</a>
                                <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
                            </div>
                            @foreach (range(1, 4) as $i)
                            <div class='form-group col-md-3'>
                                <input type='file' name='file[{{ $i }}]' class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf" />
                            </div>
                            @endforeach
                            <span id="file-btm"></span>
                        </div>
                        <div class="form-group pb-1">
                            <label for="photo_profile">Description</label>
                            <textarea name="description" id="" cols="30" rows="10" style="height:100px;" class="form-control r-0 light s-12">{{ old('description') }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
