<div class="tab-pane fade" id="product_price" role="tabpanel" aria-labelledby="home-tab">
    <div class="card-body">

        <form id="setting-form">
            <div class="card" id="settings-card">
                @include('flash::message')
                <div class="card-header">
                    <h4>

                    </h4>
                    <div class="card-header-form">
                        <a href="{{ route('procurement.master.product_price.create', ['product' => Request()->segment(4)]) }}" class="btn btn-sm btn-info pd-x-15 btn-white btn-uppercase">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                    </div>
                </div>
                <div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
