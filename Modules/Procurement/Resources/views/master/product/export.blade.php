<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Code</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Brand</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Product Category</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">UoM</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Dimension</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Weight</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Supplier</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->code}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row->brand}}</td>
    <td>{{optional($row->productCategory)->name}}</td>
    <td>{{$row->uom}}</td>
    <td>{{$row->dimension}}</td>
    <td>{{$row->weight}}</td>
    <td>{{optional($row->supplier)->name}}</td>
    <td>{{$row->description}}</td>
  </tr>
  @endforeach
</table>