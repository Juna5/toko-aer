@extends('procurement::layouts.app')

@section('procurement::title', 'Add Product')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Product',
'active' => true,
'url' => route('procurement.master.product.index')
])
@endsection
    <style>
        #bt {
            display: none;
        }
        .red {
            display:none;
        }
    </style>
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('procurement.master.product.create')
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-12 col-sm-12 col-lg-12">
        <div class="card">
           {{ Form::open(['route' => 'procurement.master.product.store', 'files' => true]) }}
           <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Product</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="detail-tab"  href="#detail" role="tab" aria-controls="detail" aria-selected="true" disabled>Detail</a>
                </li>

            </ul>
            <div class="tab-content" id="myTabContent">
                @include('procurement::master.product.tab.create-product')
                @include('procurement::master.product.tab.detail-product')
            </div>
            <hr>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">
                        {{ Form::submit('Save', array('class' => 'btn btn-primary btn-lg')) }}
                    </div>
                    <div class="col-md-4">
                       <a href="#" class="btn btn-success btn-lg" id="bt">Save with detail</a>
                   </div>
               </div> 
               {{ Form::close() }}
           </div>
       </div>
   </div>
</div>

@endsection
@push('javascript')
<script type="text/javascript">
 $(document).ready(function(){
    $('#home-tab').on('click',function(){
        if ($('#name').val() !== '' && $('#code').val() !== ''){
           $('#bt').show();
       }
   }).trigger('home-tab');
});
 $('.dropify').dropify();
 $('.add-image-btn').click(function() {
    var name = $(this).data('name');
    $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif' /></div>");
    $('.dropify').dropify();
});
 @include('include.dropify-remove-image')

 $('#bt').click(function() {
    $('#detail-tab').toggleClass("active");
    $('#detail').toggleClass("active");
    $('#home-tab').toggleClass("active");
    $('#home').toggleClass("active");
    $('#bt').hide()
});  

 $('#btn-add-sub').hide()

 $('#selectCategory').on('change', function(e){
    $('#category').val(e.target.value)
    var id = e.target.value
    $('#product_category_id').val(id)
    $.post('{{ route('api.procurement.product_sub_category') }}', {type:'product_category', id: $('#selectCategory').val()}, function(e){
        $('#selectSubCategory').html(e);
    })
    $('#btn-add-sub').show()
})

 $(document).ready(function(){


    $('input').on('input',function(){
        if ($('#name').val() !== '' && $('#code').val() !== '' && !$('#detail').hasClass('active') && !$('#detail-tab').hasClass('active')){
           $('#bt').show();
       }

       if ($('#name').val() == '' ||  $('#code').val() == ''){
        $('#bt').hide();

    }



}).trigger('input');
});

    $(document).ready(function() {
        $("#button").click(function(e) {
           var id = $('#category').val()
           e.preventDefault();
           window.open('{{ route('procurement.master.product_sub_category.create') }}?categoryId='+id, "_blank")
        });
    });
</script>
@endpush
