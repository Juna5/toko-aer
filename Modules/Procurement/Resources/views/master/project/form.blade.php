@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label>Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $project->name }}">
        </div>
        {{-- <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>Order Date</label>
                    <input type="date" class="form-control r-0 light s-12 datepicker" name="order_date" value="{{ old('order_date') ?? $project->order_date }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>Delivery Date</label>
                    <input type="date" class="form-control r-0 light s-12 datepicker" name="delivery_date" value="{{ old('delivery_date') ?? $project->delivery_date }}">
                </div>
            </div>
        </div> --}}
        <div class="form-group pb-1">
            <label>Project Manager</label>
            <select name="employee_id" class="form-control r-0 light s-12 select2" required>
                <option value="">Please Select</option>
                @foreach($projectManager as $row)
                    @if( old('employee_id') )
                        <option value="{{ $row->id }}" {{ old('employee_id') == $row->id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                    @else
                        <option value="{{ $row->id }}" {{ $row->id == $project->employee_id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group pb-1">
            <label>Customer</label>
            <select name="customer_id" class="form-control r-0 light s-12 select2" required>
                <option value="">Please Select</option>
                @foreach($customer as $row)
                    @if( old('customer_id') )
                        <option value="{{ $row->id }}" {{ old('customer_id') == $row->id ? 'selected' : ''  }}>{{ $row->company_name  }}</option>
                    @else
                        <option value="{{ $row->id }}" {{ $row->id == $project->customer_id ? 'selected' : ''  }}>{{ $row->company_name  }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>Status</label>
                    <input type="text" class="form-control r-0 light s-12" name="status" value="{{ old('status') ?? $project->status }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>% Finish</label>
                    <input type="text" class="form-control r-0 light s-12" name="finish" value="{{ old('finish') ?? $project->finish }}">
                </div>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>