@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Project')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Project',
'active' => true,
'url' => route('procurement.master.project.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.master.project.edit', $project->id)
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-12 col-sm-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="project-tab" data-toggle="tab" href="#project" role="tab" aria-controls="project" aria-selected="true">Project</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="po-tab" data-toggle="tab" href="#po" role="tab" aria-controls="po" aria-selected="false">PO Report</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="so-tab" data-toggle="tab" href="#so" role="tab" aria-controls="so" aria-selected="false">SO Report</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    @include('procurement::master.project.tab.project')
                    @include('procurement::master.project.tab.purchase_order')
                    @include('procurement::master.project.tab.sales_order')
                    @include('procurement::master.components.tab.pr')
                    @include('procurement::master.components.tab.so')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
@include('procurement::master.project.tab.modals.purchase_order.index')
@include('procurement::master.project.tab.modals.sales_order.index')
@endpush

<script>
    $('.modal-po').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modal-po").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            $("#modal-po").find('.modal-body').html(response);
        });

    });
</script>

<script>
    $('.modal-so').click(function(event) {
        event.preventDefault();

        var url = $(this).attr('href');

        $("#modal-so").modal('show');

        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'html',
        })
        .done(function(response) {
            $("#modal-so").find('.modal-body').html(response);
        });

    });
</script>
