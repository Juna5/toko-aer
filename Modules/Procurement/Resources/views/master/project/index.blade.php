@extends('procurement::layouts.app')

@section('procurement::title', 'Project')

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Project',
    'active' => true,
    'url' => route('procurement.master.project.index')
    ])
@endsection

@section('procurement::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        @include('flash::message')

        <div class="card-body">
            <div class="form-group pb-1">
                <a href="{{ route('procurement.master.project/export') }}" class="tl-tip" title="Export to Excel" data-placement="left" data-original-title="Excel">
                    <img src="{{asset('images/excel.png')}}" alt="">
                </a>
            </div>
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
