<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Order Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Delivery Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Project Manager</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Customer</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Status</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">% Finish</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{ $row->name }}</td>
    <td>{{ format_d_month_y($row->order_date) }}</td>
    <td>{{ format_d_month_y($row->delivery_date) }}</td>
    <td>{{ optional($row->employee)->name }}</td>
    <td>{{ optional($row->customer)->company_name }}</td>
    <td>{{ ucfirst($row->status) }}</td>
    <td align="right">{{ $row->finish }}</td>
  </tr>
  @endforeach
</table>