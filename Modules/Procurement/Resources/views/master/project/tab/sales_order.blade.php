<div class="tab-pane fade" id="so" role="tabpanel" aria-labelledby="home-tab">
	<div class="card-body">
		<h3 class="card-title" style="text-align: center;">
            <label>{{ $project->name }}</label>
        </h3>
        <h6 class="card-title" style="text-align: center;">
            <label>RM. {{ amount_international_with_comma($totalSO) }}</label>
        </h6>
        <hr>
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-striped" cellspacing="0" style="margin-top: 2%">
                <tr>
                    <th style="text-align: center;">Code Number</th>
                    <th style="text-align: center;">Date</th>
                    <th style="text-align: center;">Employee</th>
                    <th style="text-align: center;">Total Amount</th>
                    <th style="text-align: center;">Action</th>
                </tr>
               @foreach($salesOrder as $row)
                <tr>
                    <td style="text-align: center;">{{ $row->code_number }}</td>
                    <td style="text-align: center;">{{ format_d_month_y($row->date) }}</td>
                    <td style="text-align: center;">{{ optional($row->employee)->name }}</td>
                    <td style="text-align: right;">{{ amount_international_with_comma($row->total) }}</td>
                    <td style="text-align: center;">
                        <a href="{{ route('procurement.master.project.showSO', $row['id']) }}" class="btn btn-info modal-sales_order">
                            Detail
                        </a>
                    </td>
                </tr>
            @endforeach
            </table>
        </div>
    </div>
</div>