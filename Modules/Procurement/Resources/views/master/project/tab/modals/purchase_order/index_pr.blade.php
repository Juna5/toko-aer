<div class="modal fade" id="modal-globals" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" data-modal-parent="#modal-global" tabindex="-1" role="dialog" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" id="modal-header">
            	 <button type="button" class="close" id="btn-close">&times;</button>
                <legend class="text-center">PR Detail</legend>

            </div>


            <div class="modal-body" id="modal-body">
            </div>
        </div>
    </div>
</div>
