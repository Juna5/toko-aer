@extends('procurement::layouts.app')

@section('procurement::title', 'Product Sub Category')

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Product Sub Category',
    'active' => true,
    'url' => route('procurement.master.product_sub_category.index')
    ])
@endsection

@section('procurement::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        <div class="card-header">
            <h4>Product Sub Category</h4>
            <div class="card-header-form">           
                <a href="{{ route('procurement.master.product-category.index') }}" class="btn btn-warning">
                    <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                </a>
                <a href="{{ route('procurement.master.product_sub_category.create', ['categoryId' => request('category')]) }}" class="btn btn-info">
                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                </a>
            </div>
        </div>
        @include('flash::message')

        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
