@csrf
<div class="card">
    <div class="card-body">
        <input type="hidden" name="product_category_id" value="{{ request('categoryId') }}">
        <div class="form-group pb-1">
            <label>Code</label>
            <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') ?? $productSubCategory->code }}">
        </div>
        <div class="form-group pb-1">
            <label>Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $productSubCategory->name }}">
        </div>
        <div class="form-group pb-1">
            <label>Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $productSubCategory->description }}</textarea>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>