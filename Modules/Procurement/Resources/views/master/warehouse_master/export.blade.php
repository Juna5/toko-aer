<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Address</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">PIC</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Phone</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->name}}</td>
    <td>{{$row->address}}</td>
    <td>{{$row->pic}}</td>
    <td>{{$row->phone}}</td>
  </tr>
  @endforeach
</table>