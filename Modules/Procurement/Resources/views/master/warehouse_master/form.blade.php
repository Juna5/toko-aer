@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Leader</label>
            <select name="leader_id" class="form-control r-0 light s-12 select2" required>
                <option value="">Please Select</option>
                @foreach($users as $user)
                    @if( old('leader_id') )
                        <option value="{{ $user->id }}" {{ old('leader_id') == $user->id ? 'selected' : ''  }}>{{ $user->name  }}</option>
                    @else
                        <option value="{{ $user->id }}" {{ $user->id == $warehouseMaster->leader_id ? 'selected' : ''  }}>{{ $user->name  }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group pb-1">
            <label for="code">Code</label>
            <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') ?? $warehouseMaster->code }}">
        </div>
        <div class="form-group pb-1">
            <label for="name">Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $warehouseMaster->name }}">
        </div>
        <div class="form-group pb-1">
            <label for="address">Address</label>
            <textarea name="address" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('address') ?? $warehouseMaster->address }}</textarea>
        </div>
        <div class="form-group pb-1">
            <label for="pic">PIC</label>
            <textarea name="pic" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('pic') ?? $warehouseMaster->pic }}</textarea>
        </div>
        <div class="form-group pb-1">
            <label for="phone">Phone</label>
            <textarea name="phone" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('phone') ?? $warehouseMaster->phone }}</textarea>
        </div>
       {{--  <div class="form-group pb-1">
            <label for="description">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $warehouseMaster->description }}</textarea>
        </div> --}}
    </div>
</div>
<hr>
    <div class="card-body">
        <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
    </div>