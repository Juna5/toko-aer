<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->name}}</td>
    <td>{{$row->description}}</td>
  </tr>
  @endforeach
</table>