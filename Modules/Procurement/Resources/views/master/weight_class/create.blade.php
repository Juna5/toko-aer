@extends('procurement::layouts.app')

@section('procurement::title', 'Add Weight Class')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Weight Class',
'active' => true,
'url' => route('procurement.master.weight_class.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('procurement.master.weight_class.create')
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.master.weight_class.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('procurement.master.weight_class.store') }}" method="POST" enctype="multipart/form-data">
                            @include('procurement::master.weight_class.form', [
                            'weightClass' => new \Modules\Procurement\Entities\WeightClass
                            ])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
