@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Company Name</label>
            <input type="text" class="form-control r-0 light s-12" name="company_name" value="{{ old('company_name') ?? $customer->company_name }}">
        </div>
        <div class="row">
            {{-- <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">ROC No.</label>
                    <input type="text" class="form-control r-0 light s-12" name="roc_no" value="{{ old('roc_no') ?? $customer->company_name }}">
                </div>
            </div> --}}
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Sales Person</label>
                    <select name="employee_id" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($salesPerson as $row)
                            @if( old('employee_id') )
                                <option value="{{ $row->id }}" {{ old('employee_id') == $row->id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @else
                                <option value="{{ $row->id }}" {{ $row->id == $customer->employee_id ? 'selected' : ''  }}>{{ $row->name  }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group pb-1">
                    <label for="photo_profile">Address</label>
                    <textarea name="address" id="" cols="50" rows="4" style="height: 20%" class="form-control r-0 light s-12">{{ old('address') ?? $customer->address }}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Phone</label>
                    <input type="text" class="form-control r-0 light s-12" name="phone" value="{{ old('phone') ?? $customer->phone }}">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group pb-1">
                    <label for="photo_profile">Fax</label>
                    <input type="text" class="form-control r-0 light s-12" name="fax" value="{{ old('fax') ?? $customer->fax }}">
                </div>
            </div>
            <div class="col-md-4">
                 <div class="form-group pb-1">
                    <label for="photo_profile">Email</label>
                    <input type="email" class="form-control r-0 light s-12" name="email" value="{{ old('email') ?? $customer->email }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="">Contact Person 1</label>
            </div>
            <div class="col-md-6">
                <label for="">Contact Person 2 </label>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>Name</label>
                    <input type="text" class="form-control r-0 light s-12" name="pic_1" value="{{ old('pic_1') ?? $customer->pic_1 }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>Name</label>
                    <input type="text" class="form-control r-0 light s-12" name="pic_2" value="{{ old('pic_2') ?? $customer->pic_2 }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Position</label>
                    <input type="text" class="form-control r-0 light s-12" name="pic_position_1" value="{{ old('pic_position_1') ?? $customer->pic_position_1 }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Position</label>
                    <input type="text" class="form-control r-0 light s-12" name="pic_position_2" value="{{ old('pic_position_2') ?? $customer->pic_position_2 }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Phone</label>
                    <input type="text" class="form-control r-0 light s-12" name="pic_phone_1" value="{{ old('pic_phone_1') ?? $customer->pic_phone_1 }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Phone</label>
                    <input type="text" class="form-control r-0 light s-12" name="pic_phone_2" value="{{ old('pic_phone_2') ?? $customer->pic_phone_2 }}">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Price Category</label>
                    <select name="price_category_id" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($priceCategory as $item)
                            @if( old('price_category_id') )
                                <option value="{{ $item->id }}" {{ old('price_category_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @else
                                <option value="{{ $item->id }}" {{ $item->id == $customer->price_category_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Business Industry</label>
                    <select name="business_industry_id" class="form-control r-0 light s-12 select2" required>
                        <option value="">Please Select</option>
                        @foreach($businessIndustry as $item)
                        @if( old('business_industry_id') )
                        <option value="{{ $item->id }}" {{ old('business_industry_id') == $item->id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                        @else
                        <option value="{{ $item->id }}" {{ $item->id == $customer->business_industry_id ? 'selected' : ''  }}>{{ $item->name  }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Credit Limit</label>
                    <input type="text" class="form-control separator currency" style="text-align: right;" value="{{ $customer->credit_limit }}">
                    <input type="hidden" name="credit_limit" class="separator-hidden" value="{{ old('credit_limit') ?? $customer->credit_limit }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label>Deposit</label>
                    <input type="text" class="form-control separator currency" style="text-align: right;" value="{{ $customer->deposit }}">
                    <input type="hidden" name="deposit" class="separator-hidden" value="{{ old('deposit') ?? $customer->deposit }}">
                </div>
            </div>
        </div>
               
        <div class="row">
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Next Contact Date</label>
                    <input type="date" class="form-control r-0 light s-12 datepicker" name="next_contact_date" value="{{ old('next_contact_date') ?? $customer->next_contact_date ? : now()->format('Y-m-d') }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group pb-1">
                    <label for="photo_profile">Last Contact Date</label>
                    <input type="date" class="form-control r-0 light s-12 datepicker" name="last_contact_date" value="{{ old('last_contact_date') ?? $customer->last_contact_date ? : now()->endOfMonth()->format('Y-m-d') }}">
                </div>
            </div>
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Website</label>
            <textarea name="website" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('website') ?? $customer->website }}</textarea>
        </div> --}}
        <div class="form-group pb-1">
            <label for="photo_profile">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $customer->description }}</textarea>
        </div>
        <div class="form-group m-form__group row">
            <div class="form-group col-md-12">
                <a class="btn btn-primary white add-image-btn" data-name="attachment" style="color: white"><i class="icon wb-plus"></i>Add More Attachment</a>
                <label for="size" style="margin-left: 2%"><span style="color:red">*</span>Max. upload file size: 5 MB</label>
            </div>

            @if(!empty($media))
                @foreach($media as $index => $val)
                <div class='form-group col-md-3'>
                    <input type="file" name='attachment[]' data-id="{{$val->id }}" data-default-file="{{ asset($val->getFullUrl()) }}" class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf">
                    <span>
                        <br>
                            <a href="{{ asset($val->getFullUrl()) }}"
                            class="btn btn-success white add-image-btn pull-right" target="_blank">
                                <i class="fa fa-file text-white" ></i> View File
                            </a>
                    </span>
                </div>
                @endforeach

            @else
                @foreach (range(1, 4) as $i)
                <div class='form-group col-md-3'>
                    <input type='file' name='attachment[{{ $i }}]' class="dropify" id='input-file-max-fs' data-plugin='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions="png jpg jpeg bmp gif pdf">
                </div>
                @endforeach
            @endif
                <span id="attachment-btm"></span>
        </div>
        <div class="form-group pb-1">
            <label for="block">Block</label>
            <div class="custom-switches-stacked mt-2">
                <label class="custom-switch">
                    <input type="radio" name="block" value="1" required class="custom-switch-input" {{ old('block') ?? $customer->block == "1" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Yes</span>
                </label>
                <label class="custom-switch">
                    <input type="radio" name="block" value="0" required class="custom-switch-input" checked {{ old('block') ?? $customer->block == "0" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">No</span>
                </label>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>
