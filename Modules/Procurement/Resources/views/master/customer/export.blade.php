<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Company Name</th>
    {{-- <th style="background-color:#e2e25f; font-weight: bold;" align="center">ROC No.</th> --}}
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Address</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Email</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Phone</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Fax</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Contact Person 1 Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Contact Person 2 Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Position 1</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Position 2</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Phone 1</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Phone 2</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Product Category</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Credit Limit</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Business Industry</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Next Contact Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Last Contact Date</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Website</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->company_name}}</td>
    <td>{{$row->roc}}</td>
    <td>{{$row->address}}</td>
    <td>{{$row->email}}</td>
    <td>{{$row->phone}}</td>
    <td>{{$row->fax}}</td>
    <td>{{$row->pic_1}}</td>
    <td>{{$row->pic_2}}</td>
    <td>{{$row->pic_position_1}}</td>
    <td>{{$row->pic_position_2}}</td>
    <td>{{$row->pic_phone_1}}</td>
    <td>{{$row->pic_phone_2}}</td>
    <td>{{optional($row->productCategory)->name}}</td>
    <td>{{$row->credit_limit}}</td>
    <td>{{optional($row->businessIndustry)->name}}</td>
    <td>{{$row->next_contact_date}}</td>
    <td>{{$row->last_contact_date}}</td>
    <td>{{$row->website}}</td>
    <td>{{$row->description}}</td>
  </tr>
  @endforeach
</table>