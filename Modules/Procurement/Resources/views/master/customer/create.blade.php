@extends('procurement::layouts.app')

@section('procurement::title', 'Add Customer')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Customer',
'active' => true,
'url' => route('procurement.master.customer.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('procurement.master.customer.create')
])
@endsection

@push('stylesheet')
    <link rel="stylesheet" href="{{ asset('css/dropify.min.css') }}">
@endpush

@section('procurement::content')
<div class="row">
    <div class="col-md-12"> 
        <div class="card no-b no-r"> 
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.master.customer.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('procurement.master.customer.store') }}" method="POST" enctype="multipart/form-data">
                            @include('procurement::master.customer.form', [
                            'customer' => new \Modules\Procurement\Entities\Customer
                            ])
                        </form>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>
@endsection

@push('javascript')
    <script src="{{ asset('js/dropify.min.js') }}"></script>
    <script>
        $(document).ready(function(){
            $('.dropify').dropify();
        });
            $('.dropify').dropify();
                $('.add-image-btn').click(function() {
                    var name = $(this).data('name');
                    console.log(name);
                    $('#' + name + '-btm').before("<div class='form-group col-md-3'><input type='file' name='" + name + "[]' id='input-file-max-fs' class='dropify' data-height='160px' data-max-file-size='5M' data-allowed-file-extensions='png jpg jpeg bmp gif pdf' /></div>");
                    $('.dropify').dropify();
                });
                @include('include.dropify-remove-image')
    </script>
@endpush
