<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div style="display: flex;justify-content: space-between">
                <div>
                    <p>Customer <br> <b style="font-size: 15px">{{ $row->salesOrder->customer->company_name ?? $row->deliveryOrder->customer->company_name  }}</b></p>
                </div>
                <div>
                    <p>Date <br> <b style="font-size: 15px">{{ format_d_month_y($row->invoice_date) }}</b></p>
                </div>
                <div>
                    <p>Due Date <br> <b style="font-size: 15px">{{ format_d_month_y($row->invoice_due_date) }}</b></p>
                </div>
                <div>
                    <p>Total <br> <b style="font-size: 15px; text-align: right;">{{ amount_international_with_comma($row->total) }}</b></p>
                </div>
                <div>
                    <p>Outstanding <br> <b style="font-size: 15px; text-align: right;">{{ amount_international_with_comma($row->outstanding) }}</b></p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="section-body">
    <div class="invoice">
        <div class="invoice-print">
            <div class="row">
                <div class="col-lg-12">
                    <div class="invoice-title">
                        <h2>Invoice</h2>
                        <div class="invoice-number">No #{{ $row->invoice_number }}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <strong>Billed To:</strong><br>
                                @if (! is_null($row->sales_order_id))
                                    {{ $row->salesOrder->customer->company_name }}<br>
                                    {{ $row->salesOrder->customer->address }}<br>
                                @endif
                                @if (! is_null($row->delivery_order_id))
                                    {{ $row->deliveryOrder->customer->company_name }}<br>
                                    {{ $row->deliveryOrder->customer->address }}<br>
                                @endif
                            </address>
                        </div>
                        <div class="col-md-6 text-md-right">
                            <address>
                                <strong>Invoice Date:</strong><br>
                                {{ \Carbon\Carbon::parse($row->invoice_date)->isoFormat('MMMM Do YYYY') }}<br><br>
                            </address>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <address>
                                <strong>Payment Method:</strong><br>
                                {{ optional($row->paymentMethod)->name }}
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-md-12">
                    <div class="section-title">Order Summary</div>
                    <p class="section-lead">All items here cannot be deleted.</p>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-md">
                            <tr>
                                <th data-width="40">#</th>
                                <th>Item</th>
                                <th class="text-center">Uom</th>
                                <th class="text-center">Quantity</th>
                                <th class="text-right">Price</th>
                                <th class="text-right">Discount Amount</th>
                                <th class="text-right">Subtotal</th>
                            </tr>
                            @foreach($row->invoiceDetails as $index => $data)
                                <tr>
                                    <td>{{ $index +1 }}</td>
                                    <td>{{ $data->item }}</td>
                                    <td class="text-center">{{ $data->uom }}</td>
                                    <td class="text-center">{{ $data->qty }}</td>
                                    <td class="text-right">{{ amount_international_with_comma($data->price) }}</td>
                                    <td class="text-right">{{ $data->discount_amount ?? '0' }}%</td>
                                    <td class="text-right">{{ amount_international_with_comma($data->sub_total) }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="6" align="right">Subtotal Exclude Discount All Items</td>
                                <td  align="right">{{ amount_international_with_comma($items->sub_total_exclude_disc) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Discount All Items</td>
                                <td  align="right">{{ $items->discount_all_item }}%</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Sub Total</td>
                                <td  align="right">{{ amount_international_with_comma($items->subtotal_detail) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">GST {{ $items->gst }}%</td>
                                <td align="right">{{ amount_international_with_comma($items->amount_gst) }}</td>
                            </tr>
                            <tr>
                                <td colspan="6" align="right">Total</td>
                                <td  align="right"  style="color:#5b35c4; font-weight: bold;">{{ amount_international_with_comma($items->total) }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
    </div>
</div>
