<table>
  <tr style="width: 40%">
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Code</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Name</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Description</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Brand</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Type</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Car Registration Number</th>
    <th style="background-color:#e2e25f; font-weight: bold;" align="center">Car Registration Expiry Date</th>
  </tr>
  @foreach($rows as $row)
  <tr>
    <td>{{$row->code}}</td>
    <td>{{$row->name}}</td>
    <td>{{$row->description}}</td>
    <td>{{$row->brand}}</td>
    <td>{{$row->type}}</td>
    <td>{{$row->car_registration_no}}</td>
    <td>{{$row->car_registration_expiry_date}}</td>
  </tr>
  @endforeach
</table>