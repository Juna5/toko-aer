@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="code">Code</label>
            <input type="text" class="form-control r-0 light s-12" name="code" value="{{ old('code') ?? $vehicle->code }}">
        </div>
        <div class="form-group pb-1">
            <label for="name">Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $vehicle->name }}">
        </div>
        <div class="form-group pb-1">
            <label for="description">Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $vehicle->description }}</textarea>
        </div>
        <div class="form-group pb-1">
            <label for="brand">Brand</label>
            <input type="text" class="form-control r-0 light s-12" name="brand" value="{{ old('brand') ?? $vehicle->brand}}">
        </div>
        <div class="form-group pb-1">
            <label for="type">Type</label>
            <input type="text" class="form-control r-0 light s-12" name="type" value= "{{ old('type') ?? $vehicle->type }}">
        </div>
        <div class="form-group pb-1">
            <label for="car_registration_no">Car Registration Number</label>
            <input type="text" class="form-control r-0 light s-12" name="car_registration_no" value = "{{old('car_registration_no') ?? $vehicle->car_registration_no}}">
        </div>
        <div class="form-group pb-1">
            <label for="car_registration_expiry_date">Car Registration Expiry Date</label>
            <input type="date" class="form-control m-input datepicker" name="car_registration_expiry_date" value="{{old('car_registration_expiry_date') ?? $vehicle->car_registration_expiry_date ? : now()->format('Y-m-d') }}">
        </div>
    </div>
</div>
<hr>
    <div class="card-body">
        <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
    </div>
