@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label>Name</label>
            <input type="text" class="form-control r-0 light s-12" name="name" value="{{ old('name') ?? $businessIndustry->name }}">
        </div>
        <div class="form-group pb-1">
            <label>Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control r-0 light s-12">{{ old('description') ?? $businessIndustry->description }}</textarea>
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>