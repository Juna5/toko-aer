@extends('procurement::layouts.app')

@section('procurement::title', 'Customer Type')

@section('procurement::breadcrumb-2')
    @include('procurement::include.breadcrum', [
    'title' => 'General Settings',
    'active' => true,
    'url' => route('procurement.master.index')
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::include.breadcrum', [
    'title' => 'Customer Type',
    'active' => true,
    'url' => route('procurement.master.customer_type.index')
    ])
@endsection

@section('procurement::content')
<form id="setting-form">
    <div class="card" id="settings-card">
        @include('flash::message')

        <div class="card-body">
            <div class="table-responsive">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</form>
@endsection

@push('javascript')
@include('shared.wrapperDatatable')
@endpush
