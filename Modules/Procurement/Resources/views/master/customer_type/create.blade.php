@extends('procurement::layouts.app')

@section('procurement::title', 'Add Customer Type')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Customer Type',
'active' => true,
'url' => route('procurement.master.customer_type.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Add',
'active' => true,
'url' => route('procurement.master.customer_type.create')
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.master.customer_type.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('procurement.master.customer_type.store') }}" method="POST" enctype="multipart/form-data">
                            @include('procurement::master.customer_type.form', [
                            'customerType' => new \Modules\Procurement\Entities\CustomerType
                            ])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
