@extends('procurement::layouts.app')

@section('procurement::title', 'Edit Customer Type')

@section('procurement::breadcrumb-2')
@include('procurement::include.breadcrum', [
'title' => 'Customer Type',
'active' => true,
'url' => route('procurement.master.customer_type.index')
])
@endsection
@section('procurement::breadcrumb-3')
@include('procurement::include.breadcrum', [
'title' => 'Edit',
'active' => true,
'url' => route('procurement.master.customer_type.edit', $customerType->id)
])
@endsection

@section('procurement::content')
<div class="row">
    <div class="col-md-12">
        <div class="card no-b no-r">
            <div class="card-body">
                <h5 class="card-title">
                    <div class="text-right">
                        <a href="{{ route('procurement.master.customer_type.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                </h5>
                <hr>
                @include('flash::message')
                @include('include.error-list')
                <div class="row">
                    <div class="col-12">
                        <form action="{{ route('procurement.master.customer_type.update', $customerType->id) }}" method="POST" enctype="multipart/form-data">
                            @method('PUT')
                            @include('procurement::master.customer_type.form', [
                            'submitButtonText' => 'Update'
                            ])
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
