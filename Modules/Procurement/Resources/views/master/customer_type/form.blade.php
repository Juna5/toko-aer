@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group pb-1">
            <label for="photo_profile">Customer</label>
            <input type="text" class="form-control r-0 light s-12" name="customer" value="{{ old('customer') ?? $customerType->customer }}">
        </div>
        <div class="form-group pb-1">
            <label for="photo_profile">Reseller</label>
            <input type="text" class="form-control r-0 light s-12" name="reseller" value="{{ old('reseller') ?? $customerType->reseller }}">
        </div>
    </div>
</div>
<hr>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>
