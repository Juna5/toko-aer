@extends('procurement::layouts.app')

@section('procurement::title', 'Account Payable')

@section('procurement::breadcrumb-2')
    @include('procurement::includes.breadcrumb', [
    'title' => 'Procurement',
    'url' => route('procurement.view'),
    'active' => true,
    ])
@endsection

@section('procurement::breadcrumb-3')
    @include('procurement::includes.breadcrumb', [
        'title' => 'Account Payable',
        'active' => true,
        'url' => route('procurement.account-payable.index')
    ])
@endsection

@section('procurement::content')
    <div class="row">
        <div class="col-12">

            @include('flash::message')

            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table(['class' => 'table table-bordered table-hover table-stripped']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
