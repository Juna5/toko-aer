<div class="modal fade" id="confirm-preview-invoice" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" role="form" id="confirm-preview-invoice-action">
                {!! csrf_field() !!}
                {!! method_field('preview-invoice') !!}
                <div class="modal-header">
                    <h4 class="modal-title">preview-invoice Confirmation</h4>
                </div>
                <div class="modal-body" style="margin: auto; width:50%">
                    <a href="{{route('finance.student.invoice.preview', ['invHeaderID' => 'optional']) }}" class="btn btn-info" target="_blank">Preview 1</a>
                    <a href="{{route('finance.student.invoice.preview', ['invHeaderID' => 'notOptional']) }}" class="btn btn-info" target="_blank">Preview 2</a>
                </div>
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    $('#confirm-preview-invoice').on('show.bs.modal', function(event){
        $('#confirm-preview-invoice-action').attr('action', $(event.relatedTarget).data('href'));
    });
</script>