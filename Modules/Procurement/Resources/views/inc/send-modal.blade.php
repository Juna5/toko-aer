<div class="modal fade" id="confirm-send-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="" method="post" role="form" id="confirm-send-modal-action">
                {!! csrf_field() !!}
                {!! method_field('SEND') !!}
                <div class="modal-header">
                    <h4 class="modal-title">Send Confirmation</h4>
                </div>
                {{-- @php
                     $rows = Illuminate\Support\Facades\DB::connection('sqlsrv_invoice')->select('exec SP_InvHeader_SelectByCurrentYear');
                @endphp --}}
                {{-- <div class="modal-body" style="margin: auto; width:35%">
                    <a href="{{  route('finance.student.invoice.sendMail', , [ $row->invHeaderID, $row->regFatherEmail ? $row->regFatherEmail : "no-reply@jny.sch.id", $row->regMotherEmail ? $row->regMotherEmail : "no-reply@jny.sch.id"]) }}" class="btn btn-danger">Send Optional</a>
                    <a href="{{  route('finance.student.invoice.sendMail', [ $row->invHeaderID, $row->regFatherEmail ? $row->regFatherEmail : "no-reply@jny.sch.id", $row->regMotherEmail ? $row->regMotherEmail : "no-reply@jny.sch.id"]) }}" class="btn btn-danger">Send</a>
                </div>
                <div class="modal-footer">
                </div> --}}
            </form>
        </div>
    </div>
</div>
<script>
    $('#confirm-send-modal').on('show.bs.modal', function(event){
        $('#confirm-send-modal-action').attr('action', $(event.relatedTarget).data('href'));
    });
</script>