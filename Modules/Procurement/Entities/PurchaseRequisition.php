<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\HR\Entities\CostCenter;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\Employee;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class PurchaseRequisition extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $guarded = [];

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }


    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function costCenter()
    {
        return $this->belongsTo(CostCenter::class);
    }

    public function items()
	{
		return $this->hasMany(PurchaseRequisitionItem::class, 'purchase_requisition_id');
	}

    public function latestApprove()
    {
        return $this->belongsTo(Employee::class, 'latest_approver');
    }

    public function approveToGo()
    {
        return $this->belongsTo(Employee::class, 'approver_to_go');
    }

    public function rejectedBy()
    {
        return $this->belongsTo(Employee::class, 'rejected_by');
    }
    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }
}
