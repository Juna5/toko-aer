<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrderItem extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function product()
	{
		return $this->belongsTo(Product::class);
	}

    public function uom()
	{
		return $this->belongsTo(Uom::class);
	}

	public function purchaseOrder()
	{
		return $this->belongsTo(PurchaseOrder::class);
	}

	public function warehouse()
	{
		return $this->belongsTo(WarehouseMaster::class);
	}
}
