<?php

namespace Modules\Procurement\Entities;

use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\Cusstomer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
	use SoftDeletes;
	
    protected $guarded = [];

    public function employee()
    {
    	return $this->belongsTo(Employee::class);
    }

    public function customer()
    {
    	return $this->belongsTo(Customer::class);
    }
}
