<?php

namespace Modules\Procurement\Entities;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WarehouseMaster extends Model
{
	use SoftDeletes;
	
    protected $guarded = [];

    public function leader()
    {
        return $this->belongsTo(User::class, 'leader_id');
    }
}
