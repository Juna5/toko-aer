<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Finance\Entities\Journal;
use Modules\Finance\Entities\ChartOfAccount;
use Modules\Finance\Entities\Receivable;
use Modules\Finance\Entities\Payable;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\Supplier;
use Modules\HR\Entities\Employee;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as ContractsAuditable;

class CreditDebitNote extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function chartOfAccountBank()
    {
        return $this->belongsTo(ChartOfAccount::class, 'charge_of_account_bank_id');
    }

    public function chartOfAccount()
    {
        return $this->belongsTo(ChartOfAccount::class, 'charge_of_account_id');
    }

    public function journal()
    {
        return $this->belongsTo(Journal::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function receivable()
    {
        return $this->belongsTo(Receivable::class, 'receivable_id');
    }

    public function payable()
    {
        return $this->belongsTo(Payable::class, 'payable_id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
