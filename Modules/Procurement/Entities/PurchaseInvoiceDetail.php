<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseInvoiceDetail extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function purchaseInvoice()
    {
    	return $this->belongsTo(PurchaseInvoice::class);
    }
}
