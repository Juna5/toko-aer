<?php

namespace Modules\Procurement\Entities;

// use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\HR\Entities\Employee;

class PurchaseReturn extends Model
{
    use SoftDeletes;
    // use SoftDeletes, CascadeSoftDeletes;

    protected $guarded = [];

    protected $cascadeDeletes = ['details'];

    protected $dates = ['deleted_at'];

    public function details()
    {
        return $this->hasMany(PurchaseReturnDetail::class);
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function purchaseInvoice()
    {
        return $this->belongsTo(PurchaseInvoice::class);
    }

    public function purchaseInvoiceDetail()
    {
        return $this->belongsTo(PurchaseInvoiceDetail::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class);
    }
}
