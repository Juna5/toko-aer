<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Modules\Procurement\Entities\ProductCategory;
use Modules\Procurement\Entities\Location;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Procurement\Entities\Supplier;

class Product extends Model implements HasMedia
{
    use SoftDeletes;
    use HasMediaTrait;

    protected $guarded = [];

    protected $with = ['productCategory', 'location', 'warehouseMaster'];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function location()
    {
        return $this->belongsTo(Location::class);
    }

    public function priceCategory()
    {
        return $this->belongsTo(PriceCategory::class);
    }

    public function warehouseMaster()
    {
        return $this->belongsTo(WarehouseMaster::class, 'warehouse_id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }

    public function uom()
    {
        return $this->belongsTo(Uom::class, 'uom_id');
    }

    public function weightClass()
    {
        return $this->belongsTo(WeightClass::class, 'weight_class_id');
    }

    public function productPrice()
    {
        return $this->hasMany(ProductPrice::class)->whereHas('priceCategory', function($price){
            $price->where('code','Retail')->orWhere('name','Retail');
        });
    }
}
