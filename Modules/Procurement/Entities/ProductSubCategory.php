<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\ProductCategory;

class ProductSubCategory extends Model
{
	use SoftDeletes;

    protected $guarded = [];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class , 'product_category_id');
    }
}
