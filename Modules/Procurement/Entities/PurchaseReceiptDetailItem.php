<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseReceiptDetailItem extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function purchaseReceipt()
    {
        return $this->belongsTo(PurchaseReceipt::class, 'invoice_id');
    }
}
