<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'vouchers';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['depot_id', 'code', 'amount', 'expired_date','no_telp','customer_id'];

    public function depot()
    {
        return $this->belongsTo(WarehouseMaster::class, 'depot_id');
    }
}
