<?php

namespace Modules\Procurement\Entities;

use Modules\HR\Entities\Employee;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseRequisitionApproval extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function employee()
	{
		return $this->belongsTo(Employee::class);
	}

	public function purchaseRequisition()
	{
		return $this->belongsTo(PurchaseRequisition::class);
	}
}
