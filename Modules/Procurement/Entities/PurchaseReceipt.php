<?php

namespace Modules\Procurement\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Modules\Procurement\Entities\PaymentMethod;
use Modules\Procurement\Entities\PurchaseOrder;
use Modules\HR\Entities\Employee;

class PurchaseReceipt extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function getLimitDescription()
    {
    	return Str::words($this->description, '5');
    }

    public function purchaseReceiptDetails()
    {
        return $this->hasMany(PurchaseReceiptDetail::class, 'invoice_id');
    }

    public function getInvoiceDate()
    {
    	return Carbon::parse($this->invoice_date)->format('M d, Y');
    }

    public function getInvoiceDueDate()
    {
    	return Carbon::parse($this->invoice_due_date)->format('M d, Y');
    }

    public function paymentMethod()
    {
    	return $this->belongsTo(PaymentMethod::class);
    }

    public function purchaseOrder()
    {
    	return $this->belongsTo(PurchaseOrder::class);
    }

    public function payments()
    {
        return $this->hasMany(Receivable::class);
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }
}
