<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Uom extends Model
{
	use SoftDeletes;

    protected $guarded = [];
}
