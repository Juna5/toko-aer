<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
	use SoftDeletes;
	
    protected $fillable = ['code', 'name', 'description', 'brand', 'type', 'car_registration_no', 'car_registration_expiry_date'];
}
