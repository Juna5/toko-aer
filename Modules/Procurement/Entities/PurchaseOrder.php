<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Finance\Entities\Payable;
use Modules\HR\Entities\Department;
use Modules\HR\Entities\Employee;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class PurchaseOrder extends Model implements HasMedia
{
    use SoftDeletes, HasMediaTrait;

    protected $guarded = [];

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function items()
	{
		return $this->hasMany(PurchaseOrderItem::class, 'purchase_order_id');
	}

    public function latestApprove()
    {
        return $this->belongsTo(Employee::class, 'latest_approver');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'supplier_id');
    }
    public function tax()
    {
        return $this->belongsTo(Tax::class, 'tax_id');
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class, 'currency_id');
    }

    public function paymentTerm()
    {
        return $this->belongsTo(PaymentTerm::class, 'payment_term_id');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class, 'payment_method_id');
    }

    public function purchaseRequisition()
    {
        return $this->belongsTo(PurchaseRequisition::class, 'purchase_requisition_id');
    }

    public function approveToGo()
    {
        return $this->belongsTo(Employee::class, 'approver_to_go');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id');
    }

    public function rejectedBy()
    {
        return $this->belongsTo(Employee::class, 'rejected_by');
    }

    public function payables()
    {
        return $this->hasMany(Payable::class);
    }

    public function project()
    {
        return $this->belongsTo(Project::class, 'project_id');
    }

    public function scopeApproved($query)
    {
        return $query->where('status', 'approved');
    }

}
