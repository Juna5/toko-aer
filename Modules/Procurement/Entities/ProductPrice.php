<?php

namespace Modules\Procurement\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductPrice extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function product()
	{
		return $this->belongsTo(Product::class);
	}
    public function priceCategory()
	{
		return $this->belongsTo(PriceCategory::class);
	}

	public function uom()
	{
		return $this->belongsTo(Uom::class);
	}
}
