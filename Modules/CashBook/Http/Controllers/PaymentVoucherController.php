<?php

namespace Modules\CashBook\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\CashBook\Entities\Cash;
use Modules\CashBook\Entities\CashDetail;
use Modules\HR\Entities\Employee;
use Modules\CashBook\DataTables\PaymentVoucherDatatable;
use Modules\CashBook\Http\Requests\PaymentVoucherRequest;
use NumberToWords\NumberToWords;
use Barryvdh\DomPDF\Facade as PDF;

class PaymentVoucherController extends Controller
{
    public function index(PaymentVoucherDatatable $datatable)
    {
        // $this->hasPermissionTo('view payment voucher');

        return $datatable->render('cashbook::payment-voucher.index');
    }

    public function create()
    {
        // $this->hasPermissionTo('view payment voucher');

        return view('cashbook::payment-voucher.create');
    }

    public function store(PaymentVoucherRequest $request)
    {
        // $this->hasPermissionTo('add payment voucher');

        DB::beginTransaction();

        try {

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            if ($employee) {
                $cashes = Cash::where('depot_id', $employee->depot_id)->first();

                if ($cashes) {
                    $cash = $cashes;
                } else {
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
            } else {
                noty()->danger('Oops', 'Please set your depot location');
                return redirect()->route('cashbook.payment-voucher.create');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $payment_voucher = new CashDetail();
            $payment_voucher->cash_id = $cash->id;
            $payment_voucher->vendor = $request->vendor;
            $payment_voucher->date = $request->date;
            $payment_voucher->amount_out = $request->amount;
            $payment_voucher->employee_id = $employee->id;
            $payment_voucher->description = $request->description;
            $payment_voucher->saldo = $cash->amount - $request->amount;
            $payment_voucher->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('payment voucher');
            $code_number = generate_code_sequence('PV', $sequence_number, 3);

            $cash = $cash;
            $cash->amount = $cash->amount - $request->amount;
            $cash->code_number = $code_number;
            $cash->date = now();
            $cash->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('cashbook.payment-voucher.index');
    }

    public function show($id)
    {
        return view('cashbook::show');
    }

    public function edit($id)
    {
        // $this->hasPermissionTo('edit payment voucher');
        $paymentVoucher = CashDetail::find($id);

        return view('cashbook::payment-voucher.edit', compact('paymentVoucher'));
    }

    public function update(PaymentVoucherRequest $request, $id)
    {
        // $this->hasPermissionTo('edit payment voucher');

        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $payment_voucher = CashDetail::findOrFail($id);
            $cashes = Cash::findOrFail($payment_voucher->cash_id)->first();
            if ($employee) {
                if ($cashes) {

                    $cash = $cashes;
                    $cash->amount = $cash->amount + $payment_voucher->amount_out;
                    $cash->save();
                } else {
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
            } else {
                noty()->danger('Oops', 'Please set your depot location');
                return redirect()->route('cashbook.payment-voucher.create');
            }
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {

            $payment_voucher->vendor = $request->vendor;
            $payment_voucher->date = $request->date;
            $payment_voucher->amount_out = $request->amount;
            $payment_voucher->employee_id = $employee->id;
            $payment_voucher->description = $request->description;
            $payment_voucher->saldo = $cash->amount - $request->amount;
            $payment_voucher->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $cash = $cash;
            $cash->amount = $cash->amount - $request->amount;
            $cash->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('cashbook.payment-voucher.index');
    }

    public function destroy($id)
    {
        // $this->hasPermissionTo('delete payment voucher');

        DB::beginTransaction();
        try {
            $payment_voucher = CashDetail::findOrFail($id);

            $cash = Cash::findOrFail($payment_voucher->cash_id);
            $cash->amount = $cash->amount + $payment_voucher->amount_out;
            $cash->save();


            $payment_voucher->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function pdf(Cash $paymentVoucher)
    {
        // $this->hasPermissionTo('view payment voucher');

        $total = CashDetail::where('cash_id', $paymentVoucher->id)->get();
        $getTotal = $total->sum('amount_in');
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');

        $str = $numberTransformer->toWords($getTotal);
        $terbilang = 'IDR : ' . strtoupper($str) . ' ONLY';

        # Load and set paper
        $pdf = PDF::loadView('cashbook::payment-voucher.pdf', compact('paymentVoucher', 'terbilang'));
        $pdf->setPaper('A4', 'potrait');

        return $pdf->stream("payment_voucher.pdf");
    }

    public function report()
    {
        $paymentVoucher = Cash::query();
        $paymentVoucherDetail = CashDetail::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');
        $total = 0;


        if (isset($fromDate) && isset($untilDate)) {
            $results =  $paymentVoucher->whereBetween('date', [$fromDate, $untilDate])->orderBy('id', 'desc')->paginate(10);
            $rvid =  $paymentVoucher->whereBetween('date', [$fromDate, $untilDate])->get();
            $total = $paymentVoucherDetail->whereNull('deleted_at')->whereIn('cash_id', $rvid->pluck('id'))->sum('amount_out');
        }

        return view('cashbook::payment-voucher.report.report', compact('results','total'));
    }
}
