<?php

namespace Modules\CashBook\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Modules\CashBook\Entities\Cash;
use Modules\CashBook\Entities\CashDetail;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\CashBook\DataTables\ReceiptVoucherDatatable;
use Modules\CashBook\Http\Requests\ReceiptVoucherRequest;
use NumberToWords\NumberToWords;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class ReceiptVoucherController extends Controller
{
    public function index(ReceiptVoucherDatatable $datatable)
    {
        // $this->hasPermissionTo('view payment voucher');

        return $datatable->render('cashbook::receipt-voucher.index');
    }

    public function create()
    {
        // $this->hasPermissionTo('view payment voucher');
        $depots = WarehouseMaster::all();
        $select = Employee::whereUserId(auth()->user()->id)->first();
        return view('cashbook::receipt-voucher.create', compact('depots', 'select'));
    }

    public function store(ReceiptVoucherRequest $request)
    {
        // $this->hasPermissionTo('add payment voucher');

        DB::beginTransaction();

        try {

            $employee = Employee::where('user_id', auth()->user()->id)->first();
            // if($employee){
                $cashes = Cash::where('depot_id', $request->depot_id)->first();

                if($cashes){
                    $cash = $cashes;
                }else{
                    $cash = new Cash();
                    $cash->depot_id = $request->depot_id;
                    $cash->save();
                }
            // }else{
            //     noty()->danger('Oops','Please set your depot location');
            //     return redirect()->route('cashbook.receipt-voucher.create');
            // }

        } catch (\Exception $e){
            DB::rollback();
            throw $e;
        }

        try {
            $receipt_voucher = new CashDetail();
            $receipt_voucher->cash_id = $cash->id;
            $receipt_voucher->category = $request->category;
            $receipt_voucher->date = $request->date;
            $receipt_voucher->amount_in = $request->amount;
            $receipt_voucher->employee_id = $employee->id;
            $receipt_voucher->description = $request->description;
            $receipt_voucher->saldo = $cash->amount - $request->amount;
            $receipt_voucher->save();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('receipt voucher');
            $code_number = generate_code_sequence('RV', $sequence_number, 3);

            $cash = $cash;
            $cash->amount = $cash->amount - $request->amount;
            $cash->code_number = $code_number;
            $cash->date = now();
            $cash->save();

        } catch (\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been added successfully');
        return redirect()->route('cashbook.receipt-voucher.index');
    }

    public function show($id)
    {
        return view('cashbook::show');
    }

    public function edit($id)
    {
        // $this->hasPermissionTo('edit payment voucher');
        $receiptVoucher = CashDetail::find($id);
        $depots = WarehouseMaster::all();
        return view('cashbook::receipt-voucher.edit', compact('receiptVoucher', 'depots'));
    }

    public function update(ReceiptVoucherRequest $request, $id)
    {
        // $this->hasPermissionTo('edit payment voucher');

        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', auth()->user()->id)->first();
            $receipt_voucher = CashDetail::findOrFail($id);
            $cashes = Cash::findOrFail($receipt_voucher->cash_id)->first();
            // if($employee){
                if($cashes){

                    $cash = $cashes;
                    $cash->amount = $cash->amount - $receipt_voucher->amount_in;
                    $cash->save();

                }else{
                    $cash = new Cash();
                    $cash->depot_id = $request->depot_id;
                    $cash->save();
                }
            // }else{
            //     noty()->danger('Oops','Please set your depot location');
            //     return redirect()->route('cashbook.payment-voucher.create');
            // }


        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {

            $receipt_voucher->category = $request->category;
            $receipt_voucher->date = $request->date;
            $receipt_voucher->amount_in = $request->amount;
            $receipt_voucher->employee_id = $employee->id;
            $receipt_voucher->description = $request->description;
            $receipt_voucher->saldo = $cash->amount + $request->amount;
            $receipt_voucher->save();

        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }

        try {
            $cash = $cash;
            $cash->amount = $cash->amount + $request->amount;
            $cash->save();

        } catch (\Exception $e){
            DB::rollback();
            throw $e;
        }

        DB::commit();

        noty()->success('Yeay!', 'Your entry has been updated successfully');
        return redirect()->route('cashbook.receipt-voucher.index');
    }

    public function destroy($id)
    {
        // $this->hasPermissionTo('delete payment voucher');

        DB::beginTransaction();
        try {
            $receipt_voucher = CashDetail::findOrFail($id);

            $cash = Cash::findOrFail($receipt_voucher->cash_id);
            $cash->amount = $cash->amount - $receipt_voucher->amount_in;
            $cash->save();


            $receipt_voucher->delete();
        } catch (\Exception $e) {
            DB::rollback();
            throw $e;
        }
        DB::commit();

        noty()->danger('Cool!', 'Your entry has been deleted');
        return back();
    }

    public function pdf(Cash $receiptVoucher, Request $request)
    {
        // $this->hasPermissionTo('view payment voucher');
        $total = CashDetail::where('cash_id', $receiptVoucher->id)->get();
        $getTotal = $total->sum('amount_in');
        $numberToWords = new NumberToWords();
        $numberTransformer = $numberToWords->getNumberTransformer('en');

        $str = $numberTransformer->toWords($getTotal);
        $terbilang = 'IDR : ' . strtoupper($str) . ' ONLY';

        # Load and set paper
        $pdf = PDF::loadView('cashbook::receipt-voucher.pdf', compact('receiptVoucher', 'terbilang'));
        $pdf->setPaper('A4', 'potrait');

        return $pdf->stream("payment_voucher.pdf");
    }

    public function report()
    {
        $receiptVoucher = Cash::query();
        $receiptVoucherDetail = CashDetail::query();
        $results = [];
        $fromDate = request('from_date');
        $untilDate = request('until_date');
        $total = 0;


        if (isset($fromDate) && isset($untilDate)) {

            $rvid =  $receiptVoucher->whereBetween('date', [$fromDate, $untilDate])->get();
            $total = $receiptVoucherDetail->whereNull('deleted_at')->whereIn('cash_id', $rvid->pluck('id'))->sum('amount_in');
            $detail = $receiptVoucherDetail->whereNull('deleted_at')->whereIn('cash_id', $rvid->pluck('id'))->whereNotNull('amount_in');
            $results =  $receiptVoucher->whereBetween('date', [$fromDate, $untilDate])->orderBy('id', 'desc')->whereIn('id', $detail->pluck('cash_id'))->paginate(10);
        }

        return view('cashbook::receipt-voucher.report.report', compact('results','total'));
    }
}
