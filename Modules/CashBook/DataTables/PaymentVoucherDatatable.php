<?php

namespace Modules\CashBook\DataTables;

use Modules\CashBook\Entities\CashDetail;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PaymentVoucherDatatable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->of($query)
            ->editColumn('amount_out', function ($row) {
                return amount_international_with_comma($row->amount_out);
            })
            ->editColumn('date', function ($row) {
                return format_d_month_y($row->date);
            })
            ->addColumn('action', function ($row) {
                $edit = '<a href="' . route('cashbook.payment-voucher.edit', $row->id) . '" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit">
                            <i class="fa fa-pencil-alt"></i>
                        </a>';
                $delete = '<a data-href="' . route('cashbook.payment-voucher.destroy', $row->id) . '" style="margin-left: 10px" class="btn btn-danger" data-toggle="modal" data-target="#confirm-delete-modal"><i class="fa fa-trash text-white"></i></a>';

                return  $edit  . $delete ;
            })
            ->rawColumns(['action']);
    }

    public function query(CashDetail $model)
    {
        return $model->whereNotNull('amount_out')->get();
    }

    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->buttons([
                Button::make('create')->text('<i class="fa fa-plus" /> Add New'),
                Button::make('print'),
            ]);
    }

    protected function getColumns()
    {
        return [
            Column::make('description')->addClass('text-center'),
            Column::make('date')->addClass('text-center'),
            Column::make('amount_out')->title('Amount')->addClass('text-center'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'PaymentVoucher_' . date('YmdHis');
    }
}
