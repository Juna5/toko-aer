<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('cashbook.')->prefix('cashbook')->middleware(['impersonate', 'auth'])->group(function () {
    Route::get('/', 'CashBookController@view')->name('view');

    Route::resource('receipt-voucher', 'ReceiptVoucherController');
	Route::resource('payment-voucher', 'PaymentVoucherController');
	Route::get('payment-voucher-pdf/{payment_voucher}', 'PaymentVoucherController@pdf')->name('payment-voucher.pdf');
    Route::get('payment-voucher-report', 'PaymentVoucherController@report')->name('payment-voucher.report');
    Route::get('receipt-voucher-pdf/{receipt_voucher}', 'ReceiptVoucherController@pdf')->name('receipt-voucher.pdf');
    Route::get('receipt-voucher-report', 'ReceiptVoucherController@report')->name('receipt-voucher.report');
});
