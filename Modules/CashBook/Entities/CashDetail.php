<?php

namespace Modules\CashBook\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\CashBook\Entities\Cash;

class CashDetail extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function cash()
    {
        return $this->belongsTo(Cash::class);
    }

}
