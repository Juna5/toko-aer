<?php

namespace Modules\CashBook\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\CashBook\Entities\CashDetail;

class Cash extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function depot()
	{
		return $this->belongsTo(WarehouseMaster::class, 'depot_id');
	}

	public function cashDetail()
    {
        return $this->hasMany(CashDetail::class);
    }

}
