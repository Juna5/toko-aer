@extends('cashbook::layouts.app')

@section('cashbook::title', 'Cash Book')

@section('cashbook::breadcrumb-2')
@include('cashbook::include.breadcrum', [
'title' => 'Cash Book',
'active' => true,
'url' => route('cashbook.view')
])
@endsection

@section('cashbook::content')
<div class="section-body">
    @include('flash::message')
    <div class="row">
         <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/png/001-salary.png') }}" class="">
                            <div class=" font-weight-bold">Bayar</div>
                            <div class="text-muted text-small">
                                @can('view receipt voucher')
                                @endcan
                                <a href="{{ route('cashbook.payment-voucher.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                <a href="{{ route('cashbook.payment-voucher.report') }}" class="card-cta">Reports</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card border-radius">
                <div class="card-body">
                    <div class="row">
                        <div class="col text-center">
                            <img alt="image" src="{{ asset('images/png/002-cash.png') }}" class="">
                            <div class=" font-weight-bold">Terima</div>
                            <div class="text-muted text-small">
                                @can('view payment voucher')
                                 @endcan
                                <a href="{{ route('cashbook.receipt-voucher.index') }}" class="card-cta">Entry</a>
                                <div class="bullet"></div>
                                <a href="{{ route('cashbook.receipt-voucher.report') }}" class="card-cta">Reports</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection