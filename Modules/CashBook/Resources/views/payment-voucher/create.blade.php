@extends('cashbook::layouts.app')

@section('cashbook::title', 'Add Payment Voucher')

@section('cashbook::breadcrumb-2')
    @include('cashbook::include.breadcrum', [
    'title' => 'CashBook',
    'active' => true,
    'url' => route('cashbook.view')
    ])
@endsection

@section('cashbook::breadcrumb-3')
    @include('cashbook::include.breadcrum', [
    'title' => 'Payment Voucher',    
    'active' => true,
    'url' => route('cashbook.payment-voucher.index')
    ])
@endsection

@section('cashbook::breadcrumb-4')
    @include('cashbook::include.breadcrum', [
    'title' => 'Add',
    'active' => true,
    'url' => route('cashbook.payment-voucher.create')
    ])
@endsection

@section('cashbook::content')
    <div class="row">
        <div class="col-md-12">
            <div class="card no-b no-r">
                <div class="card-body">
                    <h5 class="card-title">
                        <div class="text-right">
                            <a href="{{ route('cashbook.payment-voucher.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </h5>
                    <hr>
                    @include('flash::message')
                    @include('include.error-list')
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('cashbook.payment-voucher.store') }}" method="POST" enctype="multipart/form-data">
                                @include('cashbook::payment-voucher.form', [
                                    'paymentVoucher' => new \Modules\CashBook\Entities\CashDetail()
                                ])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.separator.currency').number(true, 2);
            $('.separator').not('.separator.currency').number(true, 0);
            $('.separator').keyup(function() {
                $(this).next('.separator-hidden').val($(this).val());
            });
        });
    </script>
@endpush
