@extends('cashbook::layouts.app')

@section('cashbook::title', 'Report Payment Voucher')

@section('cashbook::breadcrumb-2')
    @include('cashbook::include.breadcrum', [
    'title' => 'CashBook',
    'active' => true,
    'url' => route('cashbook.view')
    ])
@endsection

@section('cashbook::breadcrumb-3')
    @include('cashbook::include.breadcrum', [
    'title' => 'Payment Voucher',    
    'active' => true,
    'url' => route('cashbook.payment-voucher.index')
    ])
@endsection

@section('cashbook::breadcrumb-4')
    @include('cashbook::include.breadcrum', [
    'title' => 'Report',
    'active' => false,
    'url' => '',
    ])
@endsection

@section('cashbook::content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <form action="{{ route('cashbook.payment-voucher.report') }}" method="GET">
                        <div class="form-row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">From Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="from_date"
                                           name="from_date"
                                           value="{{ old('from_date', request('from_date') ? request('from_date') : now()->firstOfMonth()->format('Y-m-d') ) }}">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">To Date:</label>
                                    <input type="date" class="form-control m-input datepicker" id="until_date"
                                           name="until_date"
                                           value="{{ old('until_date', request('until_date') ? request('until_date') : now()->endOfMonth()->format('Y-m-d')) }}">
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary fa-pull-right">Search</button>
                    </form>
                    <div class="text-left">
                        <a href="{{ route('cashbook.payment-voucher.index') }}" class="btn btn-warning">
                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                        </a>
                    </div>
                    <br>
                    <br>
                    @if (isset(request()->from_date) && isset(request()->until_date))
                    <h4>Total : {{ amount_international_with_comma($total) }}</h4>
                    @endif

                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-striped" cellspacing="0" id="datatable">

                            <tr style="background-color:white ; color: grey" align="center">
                                <th>#</th>
                                <th>Code Number</th>
                                <th>Date</th>
                                <!-- <th>Payment To</th> -->
                                <th>Amount</th>
                            </tr>

                            @forelse($results as $index => $row)
                                <tr align="center">
                                    <td>{{ $index +1 }}</td>
                                    <td>
                                        <a href="{{ route('cashbook.payment-voucher.pdf',[ $row->id,'from_date'=>request('from_date'), 'until_date'=>request('until_date')]) }}" target="_blank">{{ $row->code_number }}</a>
                                    </td>
                                    <td>{{ format_d_month_y($row->date) }}</td>
                                    <!-- <td>{{ $row->payment_to }}</td> -->
                                    <td>{{ amount_international_with_comma(amount_payment_voucher($row->id)) }}</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="9" class="text-center">Data not found</td>
                                </tr>
                            @endforelse
                        </table>
                        <div class="col-md-6 ">
                            @if(!empty($results))
                                {{ $results->appends(request()->query())->links() }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
