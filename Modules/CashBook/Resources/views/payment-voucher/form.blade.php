@csrf
<div class="card">
    <div class="card-body">
        <div class="form-group">
            <label>Vendor</label>
            <input type="text" name="vendor" value="{{ old('vendor') ?? $paymentVoucher->vendor }}" placeholder="cth: Petronas, Wallmart, etc" class="form-control r-0 light s-12">
        </div>
       
        <div class="form-group">
            <label>Date <span style="color:red">*</span></label>
            <input type="date" class="form-control datepicker" name="date" value="{{ old('date') ?? $paymentVoucher->date ? : now()->format('Y-m-d') }}" required>
        </div>
        <div class="form-group">
            <label class="">Amount: <span style="color:red">*</span></label>
            <input type="text" class="form-control separator currency" style="text-align: right;" value="{{ old('amount') ?? $paymentVoucher->amount_out }}" placeholder="1.000.000" required>
            <input type="hidden" name="amount" class="separator-hidden" value="{{ old('amount') ?? $paymentVoucher->amount_out }}" required>
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control" placeholder="cth: Parking at West Coast Mall, gasoline 5 gal, etc">{{ old('description') ?? $paymentVoucher->description }}</textarea>
        </div>
    </div>
</div>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>
