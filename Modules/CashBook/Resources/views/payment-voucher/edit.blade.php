@extends('cashbook::layouts.app')

@section('cashbook::title', 'Edit Payment Voucher')

@section('cashbook::breadcrumb-2')
    @include('cashbook::include.breadcrum', [
    'title' => 'CashBook',
    'active' => true,
    'url' => route('cashbook.view')
    ])
@endsection

@section('cashbook::breadcrumb-3')
    @include('cashbook::include.breadcrum', [
    'title' => 'Payment Voucher',
    'active' => true,
    'url' => route('cashbook.payment-voucher.index')
    ])
@endsection

@section('cashbook::breadcrumb-4')
    @include('cashbook::include.breadcrum', [
    'title' => 'Edit',
    'active' => true,
    'url' => route('cashbook.payment-voucher.edit', $paymentVoucher->id)
    ])
@endsection

@section('cashbook::content')
    <div class="row">
        <div class="col-md-12">
            <div class="card no-b no-r">
                <div class="card-body">
                    <h5 class="card-title">
                        <div class="text-right">
                            <a href="{{ route('cashbook.payment-voucher.index') }}" class="btn btn-warning">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </h5>
                    <hr>
                    @include('flash::message')
                    @include('include.error-list')
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('cashbook.payment-voucher.update', $paymentVoucher->id) }}" method="POST" enctype="multipart/form-data">
                                @method('PUT')
                                @include('cashbook::payment-voucher.form', [
                                    'submitButtonText' => 'Update'
                                ])
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $('.separator.currency').number(true, 2);
            $('.separator').not('.separator.currency').number(true, 0);
            $('.separator').keyup(function() {
                $(this).next('.separator-hidden').val($(this).val());
            });
        });
    </script>
@endpush
