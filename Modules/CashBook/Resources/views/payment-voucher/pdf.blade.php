<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Invoice</title>
    <style>
        @font-face {
            /* font-family: SourceSansPro;
            src: url(SourceSansPro-Regular.ttf); */
            font-family: 'Open Sans', sans-serif;
        }

        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        a {
            color: #940000;
            text-decoration: none;
        }

        body {
            position: relative;
            width: 100%;
            height: 29.7cm;
            margin: 0 auto;
            color: #555555;
            background: #FFFFFF;
            font-family: 'Open Sans', sans-serif;
            /* font-family: Arial, sans-serif; */
            font-size: 12px;
            /* font-family: SourceSansPro; */
        }

        header {
            padding: 10px 0;
            margin-bottom: 20px;
            border-bottom: 1px solid #AAAAAA;
        }

        #logo {
            float: left;
            margin-top: 8px;
        }

        #logo img {
            height: 70px;
        }

        #company {
            width: 100%;
            float: right;
            text-align: right;
        }

        #details {
            margin-bottom: 50px;
        }

        #client {
            padding-left: 6px;
            border-left: 6px solid #940000;
            float: left;
        }

        #client .to {
            color: #777777;
        }

        h2.name {
            font-size: 1.4em;
            font-weight: normal;
            margin: 0;
        }

        #invoice {
            width: 100%;
            float: right;
            text-align: right;
        }

        #invoice h1 {
            font-family: 'Open Sans', sans-serif;
            color: #940000;
            font-size: 14px;
            /* line-height: 1em; */
            font-weight: normal;
            margin: 0 0 10px 0;
        }

        #invoice .date {
            font-size: 1.1em;
            color: #777777;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
            font-family: 'Open Sans', sans-serif;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #940000;;
        }

        #notices {
            padding-left: 6px;
            border-left: 6px solid #940000;
        }

        #notices .notice {
            font-size: 1.2em;
        }

        footer {
            color: #777777;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #AAAAAA;
            padding: 8px 0;
            text-align: center;
        }
    </style>
</head>
<body>
<header class="clearfix">
    @php
        $logo = \App\Models\Logo::where('active', true)->first();
    @endphp
    <div id="logo">
        <img src="{{  $logo ? asset('uploads/path/$logo->path')  : asset('images/default_logo.png')}}" width="20%" height="20%"/>
    </div>
    <div id="company">
       {{--  <h2 class="name">Sengliy</h2>
        <div>No. 28, Jalan Tiram 14,</div>
        <div>Taman Perindustrian Tiram,</div>
        <div>Jalan Sungai Tiram,</div>
        <div>81800 Ulu Tiram Johor, Malaysia.</div>
        <div>Tel : +607 861 2730 Fax : +607 863 2730</div>
        <div><a href="mailto:company@example.com">enquiry@sengliy.com.my</a></div> --}}
    </div>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">RECEIVED FROM:</div>
            <h2 class="name">
                {{ $paymentVoucher->payment_to }}
            </h2>
        </div>
        <div id="invoice">
            <h1>Code Number #{{ $paymentVoucher->code_number }}</h1>
            <div class="date">Date:{{ format_d_month_y($paymentVoucher->date) }}</div>
        </div>
    </div>
    <i>Payment Issued</i>
    <table style="border: 1px solid black;">
        <tr>
            <th style="border: 1px solid black;">Payment By</th>
            <!-- <th style="border: 1px solid black;">Cheque No.</th> -->
            <th style="border: 1px solid black;">Payment Amount</th>
        </tr>
        <tr>
            <td style="text-align: center;border: 1px solid black;">{{ 'Cash' }}</td>
            {{-- <td style="text-align: center;border: 1px solid black;">{{ $paymentVoucher->details[0]->paymentMethod->name }}</td> --}}
            {{--  <td style="text-align: center;border: 1px solid black;">{{ $paymentVoucher->details[0]->cheque_no ?? '-' }}</td> --}}
            <td style="text-align: center;border: 1px solid black;">{{ amount_international_with_comma($paymentVoucher->cashDetail[0]->amount_out) }}</td>
        </tr>
    </table>

    <table>
        <thead>
        <tr>
            <th class="service">Acc No.</th>
            <th class="service">Description</th>
            <th style="text-align: right">Amount</th>
        </tr>
        </thead>
        <tbody>
        @foreach($paymentVoucher->cashDetail as $index => $item)
            @if($item->amount_out != null)
            <tr>
                <td class="service" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ $item->coa ? $item->coa->code.' - '.$item->coa->name : '-'   }}</td>
                <td class="service" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ $item->description }}</td>
                <td class="total" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #646a6e;  line-height: 18px;  vertical-align: top;">{{ amount_international_with_comma($item->amount_out) }}</td>
            </tr>
            @endif
        @endforeach
        <tr>
            <td colspan="2">
                TOTAL
            </td>
            <td>
                {{ amount_international_with_comma(\Modules\CashBook\Entities\CashDetail::where('cash_id', $paymentVoucher->id)->get()->sum('amount_out') }}
            </td>
        </tr>
        <tr>
            <td colspan="6" style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:right;" class="text-bold">{{ $terbilang }}</td>
        </tr>
        </tbody>
    </table>
    {{-- <div id="notices">
        <div class="notice">Notes:</div>
        <div style="font-size: 12px; font-family: 'Open Sans', sans-serif; color: #000; line-height: 19px; vertical-align: top; text-align:left;">
            <ul>
                <li>All cheques should be crossed and made payable to <br>
                    SENG LIY ENGINEERING (M) SDN BHD <br>
                    Bank A/C No.: Public Bank Bhd 3186795305
                </li>
                <li>Goods sold are neither returnable nor refundable</li>
            </ul>
        </div>
        <br>
        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SENG LIY ENGINEERING (M) SDN BHD</p>
    </div> --}}
</main>
<footer>
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</body>
</html>
