@extends('cashbook::layouts.app')

@section('cashbook::title', 'Payment Voucher')

@section('cashbook::breadcrumb-2')
    @include('cashbook::include.breadcrum', [
    'title' => 'CashBook',
    'active' => true,
    'url' => route('cashbook.view')
    ])
@endsection

@section('cashbook::breadcrumb-3')
    @include('cashbook::include.breadcrum', [
    'title' => 'Payment Voucher',
    'active' => true,
    'url' => route('cashbook.payment-voucher.index')
    ])
@endsection

@section('cashbook::content')
    @include('flash::message')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-4">
                    <div class="table-responsive">
                        {!! $dataTable->table() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('javascript')
    @include('shared.wrapperDatatable')
@endpush
