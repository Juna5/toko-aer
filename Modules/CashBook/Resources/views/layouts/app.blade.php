@extends('layouts.app')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@yield('cashbook::title', config('cashbook.name'))</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item"><a href="{{ route('home') }}">Dashboard</a></div>
                @yield('cashbook::breadcrumb-2')
                @yield('cashbook::breadcrumb-3')
                @yield('cashbook::breadcrumb-4')
            </div>
        </div>

        <div class="section-body">
            @yield('cashbook::content')
        </div>
    </section>
@endsection
