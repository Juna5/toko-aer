@csrf
<div class="card">
    <div class="card-body">

        <div class="form-group">
            <label>Depot <span style="color:red">*</span></label>
            <select name="depot_id" id="balance" class="form-control r-0 light s-12" required>
                <option value="">Please Select</option>
                @foreach($depots as $depot)
                    @if( old('depot_id') )
                        <option value="{{ $depot->id }}" {{ old('depot_id') == $depot->id ? 'selected' : ''  }}>{{ $depot->name }}</option>
                    @elseif(request()->segment(4) == 'edit')
                        <option value="{{ $depot->id }}"{{ $depot->id == $receiptVoucher->cash->depot_id ? 'selected' : ''  }}> {{ $depot->name }}</option>
                    @else
                        <option value="{{ $depot->id }}"{{ $select->depot_id ==  $depot->id ? 'selected' : '' }} >{{ $depot->name }}</option>
                    @endif
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Date <span style="color:red">*</span></label>
            <input type="date" class="form-control datepicker" name="date" value="{{ old('date') ?? $receiptVoucher->date ? : now()->format('Y-m-d') }}" required>
        </div>
        <div class="form-group">
            <label class="">Amount: <span style="color:red">*</span></label>
            <input type="text" class="form-control separator currency" style="text-align: right;" value="{{ old('amount') ?? $receiptVoucher->amount_in }}" placeholder="1.000.000" required>
            <input type="hidden" name="amount" class="separator-hidden" value="{{ old('amount') ?? $receiptVoucher->amount_in }}" required>
        </div>
        <div class="form-group">
            <label>Category</label>
            <input type="text" name="category" value="{{ old('category') ?? $receiptVoucher->category }}" placeholder="cth: investment, loan, etc" class="form-control r-0 light s-12">
        </div>
        <div class="form-group">
            <label>Description</label>
            <textarea name="description" id="" cols="30" rows="10" class="form-control" placeholder="cth: Cash, Bank Transfer, etc">{{ old('description') ?? $receiptVoucher->description }}</textarea>
        </div>
    </div>
</div>
<div class="card-body">
    <button class="btn btn-primary btn-lg">{{ $submitButtonText ?? 'Save' }}</button>
</div>
