<?php

namespace Modules\CashBook\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class ReceiptVoucherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add receipt voucher', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit receipt voucher', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete receipt voucher', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view receipt voucher', 'created_at' => now()],
        ]);
    }
}
