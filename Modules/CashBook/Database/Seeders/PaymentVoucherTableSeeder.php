<?php

namespace Modules\CashBook\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PaymentVoucherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add payment voucher', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit payment voucher', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete payment voucher', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view payment voucher', 'created_at' => now()],
        ]);
    }
}
