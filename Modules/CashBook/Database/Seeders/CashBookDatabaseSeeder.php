<?php

namespace Modules\CashBook\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CashBookDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
         $this->call(PaymentVoucherTableSeeder::class);
         $this->call(ReceiptVoucherTableSeeder::class);
    }
}
