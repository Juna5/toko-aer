<?php

namespace Modules\Setting\Services;

use Illuminate\Support\Facades\DB;

class Approval
{
    public static function action($modelDefault, $modelApproval, $params, $check_default, $description = null)
    {
        try {
            DB::beginTransaction();

            // default status
            $approved = 'approved';

            $default = $modelDefault::withoutGlobalScopes()->find($params);

            # 1. GET id current approver
            $curr = $modelApproval::withoutGlobalScopes()
                ->where($check_default, $default->id)
                ->where('employee_id', $default->approver_to_go)
                ->whereNull('date')
                ->orderBy('id', 'asc')
                ->first();

            # 2. GET next approver
            $next = $modelApproval::withoutGlobalScopes()
                ->where($check_default, $default->id)
                ->where('id', '>', $curr->id)
                ->orderBy('id', 'asc')
                ->first();

            # 3. GET last sequence
            $last = $modelApproval::withoutGlobalScopes()
                ->where($check_default, $default->id)
                ->orderBy('id', 'desc')
                ->first();

            # 4. Save record
            $data = $default;
            $data->latest_approver = employee()->id;

            if ($last->id == $curr->id) {
                $data->approver_to_go   = null;
                $data->role_approval    = null;
                $data->status           = $approved;
            } else {
                $data->approver_to_go   = $next->employee_id ? $next->employee_id : null;
                $data->role_approval    = $next->role ? $next->role : null;
            }

            $data->save();

            $approval               = $modelApproval::find($curr->id);
            $approval->is_approved  = true;
            $approval->date         = now()->format('Y-m-d');
            $approval->employee_id  = employee()->id;
            $approval->description  = $description;
            $approval->save();

            DB::commit();
            return;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public static function reject($modelDefault, $modelApproval, $params, $check_default, $description = null)
    {
        try {
            DB::beginTransaction();

            $status = 'rejected';

            $default = $modelDefault::withoutGlobalScopes()->findOrFail($params);
            $default->update([
                'approver_to_go'    => null,
                'role_approval'     => null,
                'status'            => $status,
                'rejected_reason'   => $description,
                'rejected_by'       => employee()->id,
                'rejected_at'       => now()
            ]);

            $approval = $modelApproval::where($check_default, $default->id)->first();
            $approval->update([
                'is_approved'   => false,
                'date'          => now()->format('Y-m-d'),
                'description'   => $description
            ]);

            DB::commit();
            return;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public static function resubmit($modelDefault, $modelApproval, $params, $check_default, $entities = [])
    {
        try {
            DB::beginTransaction();

            $status = 'waiting';
            $approval = $modelApproval::withoutGlobalScopes()->where($check_default, $params)->orderBy('id', 'ASC')->get();

            foreach ($approval as $value) {
                $isRoleAndEmployee = $value->role && $value->employee_id ? true : false;
                $isRoleNotEmployee = $value->role && !$value->employee_id ? true : false;
                $isEmployeeNotRole = !$value->role && $value->employee_id ? true : false;

                $newVal                 = $value;
                $newVal->role           = $value->role ? $value->role : null;
                $newVal->employee_id    = $isRoleAndEmployee ? null : ($isEmployeeNotRole ? $value->employee_id : null);
                $newVal->is_approved    = false;
                $newVal->date           = null;
                $newVal->description    = null;
                $newVal->save();
            }

            $firstApproval = $modelApproval::withoutGlobalScopes()->where($check_default, $params)->orderBy('id', 'ASC')->first();

            $mandatory_entities = [
                'status'            => $status,
                'latest_approver'   => null,
                'approver_to_go'    => $firstApproval->role ? null : $firstApproval->employee_id,
                'role_approval'     => $firstApproval->role ? $firstApproval->role : null
            ];

            $modelDefault::findOrFail($params)->update(array_merge($mandatory_entities, $entities));

            DB::commit();
            return;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
