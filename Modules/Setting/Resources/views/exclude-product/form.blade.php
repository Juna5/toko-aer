<div class="card-body p-4">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group {{ $errors->has('product_id') ? 'has-error' : '' }}">
				{!! Form::label('Product') !!} <br>
				<select class="form-control select2" name="product_id">
					<option value="">Please Select</option>
					@foreach ($products as $product)
					<option value="{{ $product->id }}" {{ isset($row) ? ($row->product_id == $product->id ? 'selected' : null) : (old('product_id') == $product->id ? 'selected' : null) }}>
						{{ $product->name }}
					</option>
					@endforeach
				</select>
				{!! $errors->first('product_id', '<div class="invalid-feedback d-block">:message</div>') !!}
			</div>
		</div>
		<div class="col-md-12">
			<div class="form-group {{ $errors->has('depot_id') ? 'has-error' : '' }}">
				{!! Form::label('Depot') !!} <br>
				<select class="form-control select2" name="depot_id">
					<option value="">Please Select</option>
					@foreach ($warehouses as $warehouse)
					<option value="{{ $warehouse->id }}" {{ isset($row) ? ($row->depot_id == $warehouse->id ? 'selected' : null) : (old('depot_id') == $warehouse->id ? 'selected' : null) }}>
						{{ $warehouse->name }}
					</option>
					@endforeach
				</select>
				{!! $errors->first('depot_id', '<div class="invalid-feedback d-block">:message</div>') !!}
			</div>
		</div>
	</div>
</div>
@include('setting::include.button')