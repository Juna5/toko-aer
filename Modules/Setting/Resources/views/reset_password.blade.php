@extends('setting::layouts.master')

@section('setting::title', 'Reset Password')

@section('setting::breadcrumb-2')
@include('setting::include.breadcrumb', [
'title' => 'Setting',
'active' => true,
'url' => route('setting.reset-password.index')
])
@endsection

@section('setting::content')
<div class="section-body">
	{!! Form::open([
	'route' => 'setting.reset-password.store',
	'onsubmit' => "submitButton.disabled = true"
	]) !!}
	<div class="card">
		<div class="card-body">

			<div class="row">
				<div class="col-md-12">
					@include('flash::message')
					@include('include.error-list')

					@include('setting::include.input', [
					'type' => 'password',
					'name' => 'current_password',
					'title' => 'Current Password',
					'placeholder' => ''
					])
				</div>
				<div class="col-md-12">
					@include('setting::include.input', [
					'type' => 'password',
					'name' => 'new_password',
					'title' => 'New Password',
					'placeholder' => ''
					])
				</div>
				<div class="col-md-12">
					@include('setting::include.input', [
					'type' => 'password',
					'name' => 'new_password_confirmation',
					'title' => 'Confirm New Password',
					'placeholder' => ''
					])
				</div>

				<div class="col-md-12">
					<div class="card">
						<div class="card-body bg-light">
							<ul>
								<li>Your password must be between 6 and 30 characters.</li>
								<li>Your password must contain at least one uppercase, or capital, letter (ex: A, B, etc.) </li>
								<li>Your password must contain at least one lowercase letter. </li>
								<li>Your password must contain at least one number digit (ex: 0, 1, 2, 3, etc.)</li>
								<li>Your password must contain at least one special character -for example: $, #, @, !,%,^,&,*,(,) - mainly the special characters are located on the top row of the keyboard on the same line as the numbers 0 through 9.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			@include('setting::include.button')
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection