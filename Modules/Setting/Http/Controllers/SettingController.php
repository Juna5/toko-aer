<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Routing\Controller;

class SettingController extends Controller
{
    public function __invoke()
    {
        return view('setting::index');
    }
}
