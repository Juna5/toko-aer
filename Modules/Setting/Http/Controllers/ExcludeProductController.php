<?php

namespace Modules\Setting\Http\Controllers;

use App\Http\Controllers\Controller;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\Setting\Entities\ExcludeProduct;
use Modules\Setting\Datatables\ExcludeProductDatatable;
use Modules\Setting\Http\Requests\ExcludeProduct\CreateRequest;

class ExcludeProductController extends Controller
{
    public function index(ExcludeProductDatatable $datatable)
    {
        $this->hasPermissionTo('view exclude product');
        return $datatable->render('setting::exclude-product.index');
    }

    public function create()
    {
        $this->hasPermissionTo('add exclude product');
        $products = Product::all();
        $warehouses = WarehouseMaster::all();
        return view('setting::exclude-product.create', compact('products', 'warehouses'));
    }

    public function store(CreateRequest $request)
    {
        $this->hasPermissionTo('add exclude product');
        ExcludeProduct::create($request->all());

        flash('Your data has been created')->success();
        return redirect()->route('setting.exclude-product.index');
    }

    public function show($id)
    {
        return view('setting::exclude-product.show');
    }

    public function edit($id)
    {
        $this->hasPermissionTo('edit exclude product');
        $row = ExcludeProduct::find($id);
        $products = Product::all();
        $warehouses = WarehouseMaster::all();
        return view('setting::exclude-product.edit', [
            'row' => $row,
            'products' => $products,
            'warehouses' => $warehouses,
        ]);
    }

    public function update(CreateRequest $request, $id)
    {
        $this->hasPermissionTo('edit exclude product');
        ExcludeProduct::find($id)->update($request->all());
        flash('Your data has been updated')->success();
        return redirect()->route('setting.exclude-product.index');
    }

    public function destroy($id)
    {
        $this->hasPermissionTo('delete exclude product');
        ExcludeProduct::find($id)->delete();
        flash('Your data has been deleted')->error();
        return redirect()->route('setting.exclude-product.index');
    }
}
