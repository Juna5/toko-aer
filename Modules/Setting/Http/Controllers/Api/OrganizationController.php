<?php

namespace Modules\Setting\Http\Controllers\Api;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Entities\Calendar;

class OrganizationController extends Controller
{
    public function getYear()
    {
        $year = Calendar::distinct('year')->pluck('year');
        return response()->json($year);
    }
}
