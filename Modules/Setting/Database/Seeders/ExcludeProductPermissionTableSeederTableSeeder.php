<?php

namespace Modules\Setting\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission;

class ExcludeProductPermissionTableSeederTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        app()['cache']->forget('spatie.permission.cache');

        Permission::insert([
            ['guard_name' => 'web', 'name' => 'add exclude product', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'edit exclude product', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'delete exclude product', 'created_at' => now()],
            ['guard_name' => 'web', 'name' => 'view exclude product', 'created_at' => now()],
        ]);
    }
}
