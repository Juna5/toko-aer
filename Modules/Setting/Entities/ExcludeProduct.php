<?php

namespace Modules\Setting\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Procurement\Entities\Product;
use Modules\Procurement\Entities\WarehouseMaster;

class ExcludeProduct extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function depot()
    {
        return $this->belongsTo(WarehouseMaster::class, 'depot_id');
    }
}
