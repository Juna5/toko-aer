<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SequenceNumber extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public static function currentSequence($name)
    {
        return self::where('name', $name)->first()->current;
    }
}
