<?php

namespace App\Services;
use Illuminate\Support\Facades\Log;

class SequenceNumber
{
    public function runSequenceNumber($name)
    {
        # Listing Menus
        $menu = config('codyway.menu');

        # Check if menu is exist
        $check = (bool) in_array($name, $menu);
        if ($check){
            # Check in database if name is exist or not.
            # If not create new one.
            $seqdb = \App\Models\SequenceNumber::where('name', $name)
                ->where('year', date('Y'))
                ->where('month', date('m'))
                ->first();

            # If exist update sequence + 1
            if ($seqdb) {
                $seqdb->current = $seqdb->current + 1;
                $seqdb->save();
                return $seqdb->current;
            } else {
                $sq = \App\Models\SequenceNumber::create([
                    'name' => $name,
                    'month' => date('m'),
                    'year' => date('Y')
                ]);
                return $sq->current;
            }

        } else {
            Log::error($name.' is not exist in menu.');
            return redirect()->back();
        }
    }
}