<?php

namespace App\Http\Controllers;

use App\Models\Logo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LogoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->hasPermissionTo('view logo');

        $keyword = $request->get('search');
        $perPage = 25;

        $logo = Logo::when($keyword, function ($query) use ($keyword) {
            $query->where('path', 'LIKE', "%$keyword%")
                ->orWhere('active', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%");
        })->paginate($perPage);

        return view('logo.index', compact('logo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $this->hasPermissionTo('add logo');

        return view('logo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->hasPermissionTo('add logo');

        $this->validate($request, [
            'path' => 'required'
        ]);
        $requestData = $request->all();

        if ($request->active) {
            Logo::where('active', true)->update(['active' => false]);
        }

        if ($request->hasFile('path')) {
            $path = $request->path;
            $uploadPath = public_path('/uploads/path');

            $extension = $path->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $path->move($uploadPath, $fileName);
            $requestData['path'] = $fileName;
        }

        Logo::create($requestData);

        $notification = ['message' => 'Your data has been added successfully', 'alert-type' => 'success'];
        return redirect('logo')->with($notification);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $logo = Logo::findOrFail($id);
        return view('logo.show', compact('logo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $this->hasPermissionTo('edit logo');

        $logo = Logo::findOrFail($id);
        return view('logo.edit', compact('logo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $this->hasPermissionTo('edit logo');

        $this->validate($request, [
            'path' => 'required'
        ]);
        $requestData = $request->all();


        $logo = Logo::findOrFail($id);

        if ($request->active) {
            Logo::where('active', true)->update(['active' => false]);
        }

        if ($request->hasFile('path')) {
            $path = $request->path;
            $uploadPath = public_path('/uploads/path');

            unlink($uploadPath.'/'.$logo->path);

            $extension = $path->getClientOriginalExtension();
            $fileName = rand(11111, 99999) . '.' . $extension;

            $path->move($uploadPath, $fileName);
            $requestData['path'] = $fileName;
        }

        $logo->update($requestData);

        $notification = ['message' => 'Your data has been updated successfully', 'alert-type' => 'success'];
        return redirect('logo')->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->hasPermissionTo('delete logo');

        $logo = Logo::find($id);

        $filePath = public_path('/uploads/path');
        unlink($filePath.'/'.$logo->path);

        $logo->delete();

        $notification = ['message' => 'Your data has been deleted successfully', 'alert-type' => 'error'];
        return redirect('logo')->with($notification);
    }
}
