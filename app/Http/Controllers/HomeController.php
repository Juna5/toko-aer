<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Setting\Entities\Calendar;
use Modules\HR\Entities\StaffAttendance;
use Illuminate\Support\Carbon;
use Modules\HR\Entities\Employee;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\HR\Entities\ShiftSetting;
use Modules\HR\Entities\WorkingHour;
use Modules\Stock\Entities\Stock;
use Modules\Transaction\Entities\SalesOrder;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        if(request()->ajax()) {
            $start = (!empty($_GET["start"])) ? ($_GET["start"]) : ('');
            $end = (!empty($_GET["end"])) ? ($_GET["end"]) : ('');
            $data = Calendar::whereDate('date', '>=', $start)->whereDate('date',   '<=', $end)->get();
            return response()->json($data);
        }

        $depot = WarehouseMaster::where('deleted_at', null)->get()->count();
        $employee = Employee::where('deleted_at', null)->where('active', true)->get()->count();

        $stocks = Stock::where('description', 'Stock Air')->whereNotNull('warehouse_id')->get();

        $salesOrder = SalesOrder::where('date', now())->where('deleted_at', null)->orderBy('updated_at', 'ASC')->get()->take(5);

        return view('home', compact('employee','depot','salesOrder','stocks'));

    }
    public function showAbsent()
    {

        $date = Carbon::now();
        $calendars = Calendar::where('date', $date)->where('user_type_id', auth()->user()->user_type_id)->first();
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if($employee){
            $shiftSetting = ShiftSetting::where('employee_id', $employee->id)->where('calendar_id', $calendars->id)->first();
            $shiftAttendance = StaffAttendance::where('calendar_id', $calendars->id)->where('employee_id', $employee->id)->where('date', $date)->first();
        }else{
            $shiftSetting = null;
            $shiftAttendance= null;
        }
        $time = Carbon::parse()->format("H:i:s");
        return view('absent.form', compact('employee','shiftSetting','date','time','shiftAttendance'))->withEmployee($employee)->withShiftSetting($shiftSetting)->withDate($date)->withTime($time)->withShiftAttendance($shiftAttendance);
    }

    public function showAbsentCheckout()
    {
        $date = Carbon::now();
        $calendars = Calendar::where('date', $date)->where('user_type_id', auth()->user()->user_type_id)->first();
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if($employee){
            $shiftSetting = ShiftSetting::where('employee_id', $employee->id)->where('calendar_id', $calendars->id)->first();
            $shiftAttendance = StaffAttendance::where('calendar_id', $calendars->id)->where('employee_id', $employee->id)->where('date', $date)->first();
        }else{
            $shiftSetting = null;
            $shiftAttendance= null;
        }
        $time = date("H:i:s");
        return view('absent.checkout', compact('employee','shiftSetting','date','time','shiftAttendance'))->withEmployee($employee)->withShiftSetting($shiftSetting)->withDate($date)->withTime($time)->withShiftAttendance($shiftAttendance);
    }

    public function storeAbsent(){

        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if($employee){
            $date = Carbon::now()->format('d M Y');
            $time = date("H:i:s");
            $calendar = Calendar::where('date', $date)->where('user_type_id', auth()->user()->user_type_id)->first();
            $checkAbsent = StaffAttendance::where('calendar_id', $calendar->id)->where('employee_id', $employee->id)->where('date', $date)->first();
            $shiftSetting = ShiftSetting::where('employee_id', $employee->id)->where('calendar_id', $calendar->id)->first();
            if($employee->working_hour == 'Shift'){
                if($shiftSetting){
                    $workingHour = WorkingHour::find($shiftSetting->working_hour_id);
                    if(request()->time_in){
                        if($workingHour){
                            $newTimeLate = date("H:i:s", strtotime($workingHour->time_from));
                            if(request()->time_in > date("H:i:s", strtotime($workingHour->time_from))){
                            $late = Carbon::parse($newTimeLate)->diff(Carbon::parse(request()->time_in))->format('%H:%I:%S');
                            }else{
                                $late = '';
                                $overtime = '';
                            }
                        }else{
                            $late = '';
                            $overtime = '';
                        }
                    }else{
                        // if($workingHour){
                        //     $newTimeOvertime = date("H:i:s", strtotime($workingHour->time_until));
                        //     $overtime = Carbon::parse(request()->time_out)->diff(Carbon::parse(request()->$newTimeOvertime))->format('%H:%I:%S');
                        // }else{
                        //     $overtime = '';
                        // }
                        $late = '';
                        $overtime = '';
                    }
                }else{
                    noty()->danger('Oops!', 'Shift setting Anda belum di buat');
                    return view('home');
                }

            }else{
                $workingHour = WorkingHour::where('name', $employee->working_hour)->first();
                if($workingHour){
                    if(request()->time_in != null){

                        $newTimeLate = date("H:i:s", strtotime($workingHour->time_from));
                        if(request()->time_in > date("H:i:s", strtotime($workingHour->time_from))){
                            $late = Carbon::parse($newTimeLate)->diff(Carbon::parse(request()->time_in))->format('%H:%I:%S');
                        }else{
                            $late = '';
                            $overtime = '';
                        }
                        $overtime = '';
                    }else{
                        $overtime = '';
                    }

                    // if(request()->time_out != null){
                    //     $newTimeOvertime = date("H:i:s", strtotime($workingHour->time_until));
                    //     $overtime = Carbon::parse(request()->time_out)->diff(Carbon::parse(request()->$newTimeOvertime))->format('%H:%I:%S');
                    //     $late = '';
                    // }else{
                        $overtime = '';
                    //     $late = '';
                    // }

                }else{
                    noty()->danger('Oops!', 'Pengaturan jam kerja Anda tidak tersedia');
                    return view('home');
                }
            }
             $overtime = '';
            if($checkAbsent){
                if(request()->time_in){
                    if($checkAbsent->time_in == null){
                        $checkAbsent->time_in = request()->time_in;
                        $checkAbsent->ip_address = request()->ip_address;
                        $checkAbsent->date = $date;
                        $checkAbsent->latitude = request()->latitude;
                        $checkAbsent->longitude = request()->longitude;
                        $checkAbsent->late = $late;
                        $checkAbsent->overtime = $overtime;
                        $checkAbsent->employee_id = $employee->id;
                        $checkAbsent->calendar_id = $calendar->id;
                    }
                    $checkAbsent->save();
                }else{
                    if($checkAbsent->time_out == null){
                        $checkAbsent->time_out = request()->time_out;
                        $checkAbsent->ip_address = request()->ip_address;
                        $checkAbsent->date = $date;
                        $checkAbsent->latitude = request()->latitude;
                        $checkAbsent->longitude = request()->longitude;
                        $checkAbsent->late = $late;
                        $checkAbsent->overtime = $overtime;
                        $checkAbsent->employee_id = $employee->id;
                        $checkAbsent->calendar_id = $calendar->id;
                    }
                    $checkAbsent->save();
                }

            }else{
                $checkAbsent = new StaffAttendance();
                if(request()->time_in){
                    if($checkAbsent->time_in == null){
                        $checkAbsent->time_in = request()->time_in;
                        $checkAbsent->ip_address = request()->ip_address;
                        $checkAbsent->date = $date;
                        $checkAbsent->latitude = request()->latitude;
                        $checkAbsent->longitude = request()->longitude;
                        $checkAbsent->late = $late;
                        $checkAbsent->overtime = $overtime;
                        $checkAbsent->employee_id = $employee->id;
                        $checkAbsent->calendar_id = $calendar->id;
                    }
                    $checkAbsent->save();
                }else{
                    if($checkAbsent->time_out == null){
                        $checkAbsent->time_out = request()->time_out;
                        $checkAbsent->ip_address = request()->ip_address;
                        $checkAbsent->date = $date;
                        $checkAbsent->latitude = request()->latitude;
                        $checkAbsent->longitude = request()->longitude;
                        $checkAbsent->late = $late;
                        $checkAbsent->overtime = $overtime;
                        $checkAbsent->employee_id = $employee->id;
                        $checkAbsent->calendar_id = $calendar->id;
                    }
                    $checkAbsent->save();
                }
            }

            noty()->success('Yeay!', 'Your entry has been saved successfully');
            return view('home');

        }else{
            noty()->danger('Oops!', "Employee doesnt exsist");
            return view('home');
        }
    }
}
