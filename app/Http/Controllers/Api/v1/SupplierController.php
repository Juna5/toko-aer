<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Modules\Procurement\Entities\Supplier;

class SupplierController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return json response
     */
    public function index()
    {
        if (request()->id) {
            $supplier = Supplier::with('productCategory', 'paymentTerm', 'currency')->findOrFail(request()->id);
            return $this->makeResponse($supplier);
        } else {
            $supplier = Supplier::with('productCategory', 'paymentTerm', 'currency')->get();
            return $this->makeResponse($supplier);
        }
    }
}
