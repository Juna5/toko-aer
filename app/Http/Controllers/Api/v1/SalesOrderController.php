<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\ApiController;
use Modules\Procurement\Entities\Currency;
use Modules\Procurement\Entities\Product;
use Modules\Transaction\Entities\SalesOrder;
use Modules\Transaction\Entities\SalesOrderItem;
use Modules\Procurement\Entities\PaymentTerm;
use Modules\Transaction\Entities\SalesOrderApproval;
use Modules\Stock\Entities\Stock;
use Modules\Stock\Entities\StockTransaction;
use Modules\Procurement\Entities\Tax;
use Modules\Procurement\Entities\Voucher;
use Modules\Procurement\Entities\Customer;

class SalesOrderController extends ApiController
{
    protected $menu = 'sales order material';

    public function index()
    {
        if (request()->user_id) {
            $employee = Employee::where('user_id', request()->user_id)->first();
            $so = SalesOrder::with(['items.product', 'depot', 'department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm', 'voucher']);
            if ($employee) {
                $so->whereNull('cancel_approved_at')->where('depot_id', $employee->depot_id)->orderBy('updated_at', 'desc');
            }
            return $this->makeResponse($so->get());
        } else {
            $so = SalesOrder::with(['items.product', 'depot', 'department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm', 'voucher'])->whereNull('cancel_approved_at')->orderBy('updated_at', 'desc')->get();
            return $this->makeResponse($so);
        }
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', $request->user_id)->first();
            if (!$employee) return $this->respondValidationError("Please check your id, employee doesn't exist");
            if (!$employee->depot_id) return $this->respondValidationError("Please set depot first for " . $employee->name);

            if ($request->voucher_code) {
                if (count(array_unique($request->voucher_code)) < count($request->voucher_code)) {
                    return $this->makeResponse([
                        'message'    => 'Voucher code tidak boleh sama',
                    ]);
                }

                $voucherTerpakai = [];
                for ($i = 0; $i <= count($request->voucher_code) - 1; $i++) {
                    $checkVoucher = Voucher::where('code', $request->voucher_code[$i])->where('depot_id', $employee->depot_id)->whereNull('sales_order_id')->first();
                    if (!$checkVoucher) {
                        array_push($voucherTerpakai, $request->voucher_code[$i]);
                    } else {
                        if ($checkVoucher->expired_date != null) {
                            array_push($voucherTerpakai, $request->voucher_code[$i]);
                        }
                    }
                }
                if (!empty($voucherTerpakai)) {
                    return $this->makeResponse([
                        'message'    => 'Voucher ' . implode(', ', $voucherTerpakai) . ' tidak tersedia / sudah terpakai / sudah habis masa berlaku',
                    ]);
                }
            }

            $currency = Currency::where('code', 'IDR')->first();
            if (!$currency) return $this->respondValidationError("currency doesn't exist");

            $now = now()->format('H:i');

            if ($now >= "07:00" && $now <= "14:00") {
                $shift = 1;
            } else {
                $shift = 2;
            }
            $firstApproval = getFirstApproval($this->menu, $employee->department_id ?? 1, $employee->division_id);
            $getApproval = getAllApproval($this->menu, $request->department_id ?? 1, $employee->division_id);
            $tax = Tax::where('code', 'PPN')->first();

            # generate code number
            $sequence = new \App\Services\SequenceNumber;
            $sequence_number = $sequence->runSequenceNumber('sales order material');
            $code_number = generate_code_sequence('SO', $sequence_number, 3);

            $paymentTerm = PaymentTerm::where('code', 'Cash')->first();
            $so = SalesOrder::create([
                'employee_id' => $employee->id,
                'code_number' => $code_number,
                'status' => 'waiting',
                'approver_to_go' => isset($firstApproval) ? $firstApproval->approver_id : null,
                'role_approval' => isset($firstApproval) ? $firstApproval->role : null,
                'date' => now()->format('Y-m-d'),
                'title' => $code_number,
                'customer_id' => $request->customer_id,
                'currency_id' => $currency->id,
                'salesman_id' => $employee->id,
                'department_id' => $employee->department_id,
                'get_total' => (int) $request->total,
                'discount_all_item' => (int) $request->discount_all_item,
                'sub_total' => $request->sub_total,
                'tax_id' => $request->tax_id ?? $tax->id ?? null,
                'total' => (int) $request->total,
                'description' => $request->sales_description,
                'payment_term_id' => $paymentTerm ? $paymentTerm->id : 3,
                'depot_id' => $employee->depot_id,
                'shift' => $shift,
                'payment_amount' => $request->payment_amount,
                'payment_change' => $request->payment_change,
            ]);
        } catch (\Exception $e) {
            return $this->respondValidationError('Create failed, error:' . $e);
        }
        try {
            for ($i = 0; $i < count($request->product_id); $i++) {
                $details = new SalesOrderItem();
                $details->sales_order_id = $so->id;
                $details->product_id = $request->product_id[$i];
                $details->qty = (int) $request->qty[$i];
                $details->delivered_qty = (int) $request->qty[$i];
                $details->uom_id = $request->uom_id[$i];
                $details->price = $request->price[$i];
                $details->discount_rate = $request->discount_rate[$i];
                $details->discount_amount = $request->discount_amount[$i];
                $details->sub_total = $request->sub_total_product[$i];
                $details->description = $request->description[$i];
                if($request->product_id[$i] == 5){
                    $details->qty_voucher = $request->total_voucher ? $request->total_voucher : null;
                }
                $details->save();

                $product = Product::findOrFail($request->product_id[$i]);
                $seal = Product::whereName('Seal')->orWhere('code', 'SL')->first();
                $tissu = Product::whereName('Tissue Basah')->orWhere('code', 'TB')->first();
                $galon = Product::whereName('Galon Kosong')->orWhere('code', 'GK')->first();
                $air = Product::whereName('Air')->first();

                if ($product->code == 'GU' || $product->code == 'GL' || $product->code == 'GK') {

                    if (!$galon) {
                        $galon = Product::create(['code' => 'GK', 'name' => 'Galon Kosong', 'brand' => 'AerPlus', 'product_category_id' => 2, 'product_sub_category_id' => 3, 'uom_id' => 10, 'description' => 'Galon Kosong']);
                    }

                    $stockSeal = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $seal->id)->first();
                    $stockGalon  = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $galon->id)->first();
                    $stockTissu  = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $tissu->id)->first();
                    $stockProduct  = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $request->product_id[$i])->first();
                    $stockAir  = Stock::where('warehouse_id', $employee->depot_id)->where('description', 'Stock Air')->first();

                    if (!$stockSeal) {
                        $stockSeal = Stock::create(['product_id' => $seal->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } elseif ($stockSeal->qty < (int) $request->qty[$i]) {
                        return $this->respondValidationError('Stok Seal Tidak Mencukupi');
                    }
                    if (!$stockGalon) {
                        $stockGalon = Stock::create(['product_id' => $galon->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } elseif ($stockGalon->qty < (int) $request->qty[$i]) {
                        return $this->respondValidationError('Stok Galon Tidak Mencukupi');
                    }
                    if (!$stockTissu) {
                        $stockTissu = Stock::create(['product_id' => $tissu->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } elseif ($stockTissu->qty < (int) $request->qty[$i]) {
                        return $this->respondValidationError('Stok Tissu Tidak Mencukupi');
                    }
                    if (!$stockProduct) {
                        $stockProduct = Stock::create(['product_id' => $product->id, 'warehouse_id' => $employee->depot_id, 'qty' => 1000, 'in' => 1000]);
                    } elseif ($stockProduct->qty < (int) $request->qty[$i]) {
                        return $this->respondValidationError('Stok ' . $product->name . ' Tidak Mencukupi');
                    }

                    $jml = $request->qty[$i] * 19.5;


                    if (!$stockAir) {
                        $stockAir = Stock::create(['warehouse_id' => $employee->depot_id, 'qty' => 8000, 'in' => 8000, 'description' => 'Stock Air', 'product_id' => $air ? $air->id : 9]);
                    } elseif ($stockAir->qty < $jml) {
                        return $this->respondValidationError('Stok Air Tidak Mencukupi');
                    }

                    if ($product->code == 'GU') {
                        $req_air = $stockAir;
                        $req_air->product_id = $air ? $air->id : 9;
                        $req_air->qty = $req_air->qty - $jml;
                        $req_air->status = true;
                        $req_air->out = $req_air->out + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = $employee->depot_id;
                        $trans->type = 'OUT';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Sales Order Air No : ' . $so->code_number;
                        $trans->in_hand =  $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->sales_order_id = $so->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty - (int) $request->qty[$i];
                        $req_seal->out = (int) $req_seal->out + (int) $request->qty[$i];
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = $employee->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'OUT';
                        $transSeal->qty = (int) $request->qty[$i];
                        $transSeal->status  = true;
                        $transSeal->description = 'Sales Order Seal No : ' . $so->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->sales_order_id = $so->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty - (int) $request->qty[$i];
                        $req_tissu->out = (int) $req_tissu->out + (int) $request->qty[$i];
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = $employee->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'OUT';
                        $transTissu->qty = (int) $request->qty[$i];
                        $transTissu->status  = true;
                        $transTissu->description = 'Sales Order Tissu Basah No : ' . $so->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->sales_order_id = $so->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty - (int) $request->qty[$i];
                        $req_produk->out = (int) $req_produk->out + (int) $request->qty[$i];
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = $employee->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'OUT';
                        $transProduk->qty = (int)$request->qty[$i];
                        $transProduk->status  = 1;
                        $transProduk->description = 'Sales Order ' . $product->name . ' No : ' . $so->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->sales_order_id = $so->id;
                        $transProduk->save();
                    }
                    if ($product->code == 'GL') {
                        $req_air = $stockAir;
                        $req_air->product_id = $air ? $air->id : 9;
                        $req_air->qty =  $req_air->qty - $jml;
                        $req_air->status = true;
                        $req_air->out =  $req_air->out + $jml;
                        $req_air->save();

                        $trans = new StockTransaction();
                        $trans->warehouse_id = $employee->depot_id;
                        $trans->type = 'OUT';
                        $trans->qty = $jml;
                        $trans->status  = 1;
                        $trans->description = 'Sales Order Air No : ' . $so->code_number;
                        $trans->in_hand = $req_air->qty;
                        $trans->stock_id = $req_air->id;
                        $trans->sales_order_id = $so->id;
                        $trans->save();

                        $req_seal = $stockSeal;
                        $req_seal->qty = (int) $req_seal->qty - (int) $request->qty[$i];
                        $req_seal->out = (int) $req_seal->out + (int) $request->qty[$i];
                        $req_seal->status = true;
                        $req_seal->save();

                        $transSeal = new StockTransaction();
                        $transSeal->warehouse_id = $employee->depot_id;
                        $transSeal->product_id = $seal->id;
                        $transSeal->type = 'OUT';
                        $transSeal->qty = (int) $request->qty[$i];
                        $transSeal->status  = true;
                        $transSeal->description = 'Sales Order Seal No : ' . $so->code_number;
                        $transSeal->in_hand = $req_seal->qty;
                        $transSeal->stock_id = $req_seal->id;
                        $transSeal->sales_order_id = $so->id;
                        $transSeal->save();

                        $req_tissu = $stockTissu;
                        $req_tissu->qty = (int) $req_tissu->qty - (int) $request->qty[$i];
                        $req_tissu->out = (int) $req_tissu->out + (int) $request->qty[$i];
                        $req_tissu->status = true;
                        $req_tissu->save();

                        $transTissu = new StockTransaction();
                        $transTissu->warehouse_id = $employee->depot_id;
                        $transTissu->product_id = $tissu->id;
                        $transTissu->type = 'OUT';
                        $transTissu->qty = (int) $request->qty[$i];
                        $transTissu->status  = true;
                        $transTissu->description = 'Sales Tissu Basah No : ' . $so->code_number;
                        $transTissu->in_hand = $req_tissu->qty;
                        $transTissu->stock_id = $req_tissu->id;
                        $transTissu->sales_order_id = $so->id;
                        $transTissu->save();

                        $req_produk = $stockProduct;
                        $req_produk->qty = (int) $req_produk->qty - (int) $request->qty[$i];
                        $req_produk->out = (int) $req_produk->out + (int) $request->qty[$i];
                        $req_produk->status = true;
                        $req_produk->save();

                        $transProduk = new StockTransaction();
                        $transProduk->warehouse_id = $employee->depot_id;
                        $transProduk->product_id = $product->id;
                        $transProduk->type = 'OUT';
                        $transProduk->qty = (int) $request->qty[$i];
                        $transProduk->status  = 1;
                        $transProduk->description = 'Sales ' . $product->name . ' No : ' . $so->code_number;
                        $transProduk->in_hand = $req_produk->qty;
                        $transProduk->stock_id = $req_produk->id;
                        $transProduk->sales_order_id = $so->id;
                        $transProduk->save();

                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty - (int) $request->qty[$i];
                        $req_galon->out = (int) $req_galon->out + (int) $request->qty[$i];
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = $employee->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'OUT';
                        $transGalon->qty = (int) $request->qty[$i];
                        $transGalon->status  = 1;
                        $transGalon->description = 'Sales Galon  No : ' . $so->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->sales_order_id = $so->id;
                        $transGalon->save();
                    }

                    if ($product->code == 'GK') {
                        $req_galon = $stockGalon;
                        $req_galon->qty = (int) $req_galon->qty - (int) $request->qty[$i];
                        $req_galon->out = (int) $req_galon->out + (int) $request->qty[$i];
                        $req_galon->status = true;
                        $req_galon->save();

                        $transGalon = new StockTransaction();
                        $transGalon->warehouse_id = $employee->depot_id;
                        $transGalon->product_id = $galon->id;
                        $transGalon->type = 'OUT';
                        $transGalon->qty = (int) $request->qty[$i];
                        $transGalon->status  = 1;
                        $transGalon->description = 'Sales Galon  No : ' . $so->code_number;
                        $transGalon->in_hand = $req_galon->qty;
                        $transGalon->stock_id = $req_galon->id;
                        $transGalon->sales_order_id = $so->id;
                        $transGalon->save();
                    }
                }elseif($product->code == 'GB' || $product->code == 'GV'
                || $product->code == 'G7'
                || $product->code == 'G8'
                || $product->code == 'G10'
                ){
                    $v25 = 25;
                    $v50 = 50;
                    $total = 0;
                    $customer = Customer::findOrfail($request->customer_id);
                    if($customer){
                        $cus = $customer;
                        // if($product->code == 'GB'){
                        //     $cus->total_voucher = ($cus->total_voucher) + ($request->qty[$i] * $product->size_packing);
                        // }
                        // if($product->code == 'GV'){
                        //     $cus->total_voucher = ($cus->total_voucher) + ($request->qty[$i] * $product->size_packing);
                        // }
                        $cus->total_voucher = ($cus->total_voucher) + ($request->qty[$i] * $product->size_packing);
                        $cus->save();
                    }

                } else {
                    $jml = $request->qty[$i];

                    if ($product->name == 'Air') {
                        $stock = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $product->id)->where('description', 'Stock Air')->first();

                        if (!$stock)
                            return $this->respondValidationError('Stock ' . $product->name . ' tidak mencukupi, silahkan melakukan proses pembelian terlebih dahulu');
                    } else {
                        $stock = Stock::where('warehouse_id', $employee->depot_id)->where('product_id', $product->id)->first();
                        if (!$stock)
                            return $this->respondValidationError('Stock ' . $product->name . ' tidak mencukupi, silahkan melakukan proses pembelian terlebih dahulu');
                    }
                    if ($stock->qty < $jml)
                        return $this->respondValidationError('Stock ' . $product->name . ' tidak mencukupi, silahkan melakukan proses pembelian terlebih dahulu');


                    $reqs = $stock;
                    $reqs->qty = $reqs->qty - $jml;
                    $reqs->status = true;
                    $reqs->out = $reqs->out + $jml;
                    $reqs->save();

                    $trans = new StockTransaction();
                    $trans->warehouse_id = $employee->depot_id;
                    $trans->product_id = $galon->id;
                    $trans->type = 'OUT';
                    $trans->qty = (int) $request->qty[$i];
                    $trans->status  = 1;
                    $trans->description = 'Sales Order ' . $product->name . '  No : ' . $so->code_number;
                    $trans->in_hand = $reqs->qty;
                    $trans->stock_id = $reqs->id;
                    $trans->sales_order_id = $so->id;
                    $trans->save();
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }
        try {
            foreach ($getApproval as $row) {
                SalesOrderApproval::create([
                    'sales_order_id' => $so->id,
                    'employee_id' => isset($row->approver_id) ? $row->approver_id : null,
                    'role' => isset($row->role) ? $row->role : null,
                    'is_approved' => false,
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }
        try {
            if ($so) {
                if ($request->voucher_code) {
                    if (count(array_unique($request->voucher_code)) < count($request->voucher_code)) {
                        return $this->makeResponse([
                            'message'    => 'Voucher code tidak boleh sama',
                        ]);
                    }
                    $voucherTerpakai = [];
                    for ($i = 0; $i <= count($request->voucher_code) - 1; $i++) {
                        $checkVoucher = Voucher::where('code', $request->voucher_code[$i])->where('depot_id', $employee->depot_id)->whereNull('sales_order_id')->first();
                        if (!$checkVoucher) {
                            array_push($voucherTerpakai, $request->voucher_code[$i]);
                        } else {
                            if ($checkVoucher->expired_date != null) {
                                array_push($voucherTerpakai, $request->voucher_code[$i]);
                            }
                        }
                    }
                    if (!empty($voucherTerpakai)) {
                        return $this->makeResponse([
                            'message'    => 'Voucher ' . implode(', ', $voucherTerpakai) . ' tidak tersedia / sudah terpakai / sudah habis masa berlaku',
                        ]);
                    }

                    for ($i = 0; $i <= count($request->voucher_code) - 1; $i++) {
                        $checkVoucher = Voucher::where('code', $request->voucher_code[$i])->where('depot_id', $employee->depot_id)->whereNull('sales_order_id')->first();
                        $checkVoucher->sales_order_id = $so->id;
                        $checkVoucher->save();
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        try {
            if($request->total_voucher){
                $customer = Customer::findOrfail($request->customer_id);

                if($customer){
                    if($customer->total_voucher >= $request->total_voucher){
                        $customer->total_voucher = $customer->total_voucher - $request->total_voucher;
                        $customer->save();
                    }else{
                        return $this->makeResponse('Jumlah voucher kurang, silahkan lakukan pembelian terlebih dahulu');
                    }
                }else{
                    return $this->makeResponse('Customer tidak ditemukan');
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }
        DB::commit();
        $salesOrder = SalesOrder::with(['items.product', 'depot', 'department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm', 'voucher'])->findOrFail($so->id);
        $voucher = Voucher::where('sales_order_id', $so->id)->first();

        return $this->respondCreated($salesOrder);
    }

    public function destroy($id)
    {

        // $this->hasPermissionTo('delete sales order');

        $row = SalesOrder::findOrFail($id);


        SalesOrderApproval::where('sales_order_id', $row->id)->delete();
        SalesOrderItem::where('sales_order_id', $row->id)->delete();

        $st = StockTransaction::where('sales_order_id', $row->id)->get();
        if ($st) {
            foreach ($st as $data) {
                $stock = Stock::findOrfail($data->stock_id);
                if ($stock) {
                    if ($stock->description == 'Stock Air') {
                        $jml = (int) $data->qty;
                    } else {

                        $jml = (int) $data->qty;
                    }

                    $stock = $stock;
                    $stock->qty = (int) $stock->qty + (int) $jml;
                    $stock->out = (int) $stock->out - (int) $jml;
                    $stock->save();
                }
                $data->delete();
            }
        }

        $row->delete();

        return $this->makeResponse(null, 'data has been deleted successfuly');
    }

    public function transactionList(Request $request)
    {
        $salesOrder = 0;
        $date = now();
        $code = $request->product_code;
        $depot = $request->depot_id;

        if ($code) {
            $so = SalesOrder::with(['items.product', 'depot', 'department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm', 'voucher'])->when($date, function($query) use($date){
                $query->where('date' , $date);
            })
            ->when($depot, function($query) use($depot){
                $query->where('depot_id' , $depot);
            })
            ->whereNull('cancel_approved_at')
            ->orderBy('updated_at', 'desc')->where('deleted_at', null)->get();
            $soi = SalesOrderItem::whereIn('sales_order_id', $so->pluck('id'))->whereHas('product', function ($query) use ($code) {
                $query->where('code', $code);
            })->where('deleted_at', null)->get();
            $salesOrder = $soi->sum('qty');
        }
        return $this->makeResponse($salesOrder);
    }

    public function assignVoucher(Request $request)
    {
        $this->validate($request, [
            'customer_id' => 'required',
            'voucher'  => 'required',
        ]);

        $voucher = Voucher::where('code', 'like', '%' . $request->voucher . '%')->get();
        foreach ($voucher as $index => $row) {
            $row->customer_id = $request->customer_id;
            $row->save();
        }

        return $this->makeResponse(null, 'Success assign voucher');
    }

    public function transactionListPerDepot(Request $request) {
        $salesOrder = 0;
        $date = $request->date;
        $code = $request->product_code;
        $depot = $request->depot_id;

        $so = SalesOrder::with(['items.product', 'depot', 'department', 'approveToGo', 'latestApprove', 'customer', 'salesman', 'paymentTerm', 'voucher'])->when($date, function($query) use($date){
                $query->where('date' , $date);
            })
            ->when($depot, function($query) use($depot){
                $query->where('depot_id' , $depot);
            })
            ->whereNull('cancel_approved_at')
            ->orderBy('updated_at', 'desc')->where('deleted_at', null)->get();
        $soi = SalesOrderItem::whereIn('sales_order_id', $so->pluck('id'))->whereHas('product', function ($query) use ($code) {
            $query->when($code, function($q) use($code){
                $q->where('code' , $code);
            });
        })->where('deleted_at', null)->get();

        return $this->makeResponse($so, null, [], 'success', $so->sum('total'), $soi->sum('qty'));
    }

    Public function transactionSummaryPerDepot(Request $request)
    {
        $fromDate = $request->from_date;
        $untilDate = $request->until_date;
        $depot = $request->depot_id;

        $countQty = getQtyReport($depot,$fromDate, $untilDate);
        
        return $this->makeResponse($countQty);

    }

    Public function transactionAmtSummaryPerDepot(Request $request)
    {
        $fromDate = $request->from_date;
        $untilDate = $request->until_date;
        $depot = $request->depot_id;

        $countAmt = getAmtReport($depot,$fromDate, $untilDate);
        
        return $this->makeResponse($countAmt);

    }

    Public function getLeaderDepot(Request $request)
    {
        
        $user = $request->user_id;

        $result = getLeaderDepot($user);
        
        return $this->makeResponse($result);

    }

    Public function cancelApprovedListPerDepot(Request $request)
    {
        
        $depot = $request->depot_id;

        $CancelApproveList = getCancelApproveList($depot);
        
        return $this->makeResponse($CancelApproveList);

    }

    public function cancel(Request $request, $id)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if(!$employee){
            return $this->respondWithError("Employee tidak ada!");
        }

        $row = SalesOrder::find($id);
        $row->cancel_status = true;
        $row->cancel_reason = $request->cancel_reason;
        $row->cancelled_id = $employee->id;
        $row->cancelled_at = now();
        $row->save();

        return $this->makeResponse(null, 'Success cancel transaction');

    }

    public function cancelApproved(Request $request, $id)
    {
        $employee = Employee::where('user_id', auth()->user()->id)->first();
        if(!$employee){
            return $this->respondWithError("Employee tidak ada!");
        }

        $row = SalesOrder::find($id);
        $row->cancel_approved_id = $employee->id;
        $row->cancel_approved_at = now();
        $row->save();

        return $this->makeResponse(null, 'Success approved');

    }

}
