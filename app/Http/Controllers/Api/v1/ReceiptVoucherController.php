<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\HR\Entities\Employee;
use Modules\CashBook\Entities\Cash;
use Modules\CashBook\Entities\CashDetail;
use Illuminate\Support\Facades\DB;
use Modules\CashBook\Http\Requests\ReceiptVoucherRequest;

class ReceiptVoucherController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return json response
     */
    public function index()
    {
        if (request()->id) {
            $cash = Cash::with('depot', 'cashDetail')->findOrFail(request()->id);
            return $this->makeResponse($cash);
        } else {
            $cash = Cash::with('depot', 'cashDetail')
                ->get();
            return $this->makeResponse($cash);
        }
    }

    public function show()
    {
        $id = request()->id;
        $cash = Cash::with('depot', 'cashDetail')->findOrFail(request()->id);

        if (!$cash) return $this->respondNotFound();

        return $this->makeResponse($cash);
    }

    public function store(ReceiptVoucherRequest $request)
    {
        DB::beginTransaction();

        try {
            $employee = Employee::where('user_id', $request->user_id)->first();
            if (!$employee) return $this->respondValidationError('Oops', 'Please set your depot location');
            $cashes = Cash::where('depot_id', $employee->depot_id)->first();
            if ($cashes) {
                $cash = $cashes;
            } else {
                $cash = new Cash();
                $cash->depot_id = $employee->depot_id;
                $cash->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        try {
            $receipt_voucher = new CashDetail();
            $receipt_voucher->cash_id = $cash->id;
            $receipt_voucher->vendor = $request->vendor;
            $receipt_voucher->date = $request->date;
            $receipt_voucher->amount_out = $request->amount;
            $receipt_voucher->employee_id = $employee->id;
            $receipt_voucher->description = $request->description;
            $receipt_voucher->saldo = (int) $cash->amount - (int) $request->amount;
            $receipt_voucher->save();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        try {
            $cash = $cash;
            $cash->amount = (int) $cash->amount - (int) $request->amount;
            $cash->save();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        DB::commit();

        return $this->respondCreated();
    }

    public function update(ReceiptVoucherRequest $request)
    {
        // $this->hasPermissionTo('edit payment voucher');

        DB::beginTransaction();
        try {
            $employee = Employee::where('user_id', $request->user_id)->first();
            $cashes = Cash::findOrFail($request->cash_id);
            $receipt_voucher = CashDetail::where('cash_id', $cashes->id)->get();
            $countAmountOut = $receipt_voucher->count('amount_out');
            if ($employee) {
                if ($cashes) {
                    $cash = $cashes;
                    $cash->amount = (int) $cash->amount + (int) $countAmountOut;
                    $cash->save();
                } else {
                    $cash = new Cash();
                    $cash->depot_id = $employee->depot_id;
                    $cash->save();
                }
            } else {
                noty()->danger('Oops', 'Please set your depot location');
                return redirect()->route('cashbook.payment-voucher.create');
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        try {
            foreach ($receipt_voucher as $pv) {
                $pv->vendor = $request->vendor;
                $pv->date = $request->date;
                $pv->amount_out = $request->amount;
                $pv->employee_id = $employee->id;
                $pv->description = $request->description;
                $pv->saldo = (int) $cash->amount - (int) $request->amount;
                $pv->save();
            }
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        try {
            $cash = $cash;
            $cash->amount = (int) $cash->amount - (int) $request->amount;
            $cash->save();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }

        DB::commit();

        return $this->respondCreated();
    }

    public function destroy(Request $request)
    {
        // $this->hasPermissionTo('delete payment voucher');

        DB::beginTransaction();
        try {
            $id = $request->id;
            $cash = Cash::findOrFail($id);
            $receipt_voucher = CashDetail::where('cash_id', $cash->id)->get();
            $countAmountOut = $receipt_voucher->count('amount_out');

            $cash->amount = (int) $cash->amount + (int)$countAmountOut;
            $cash->save();
            $receipt_voucher->delete();
        } catch (\Exception $e) {
            DB::rollback();
            return $this->respondValidationError('Create failed, error:' . $e->getMessage());
        }
        DB::commit();

        return $this->respondCreated();
    }
}
