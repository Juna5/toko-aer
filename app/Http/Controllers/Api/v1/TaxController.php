<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Modules\Procurement\Entities\Tax;

class TaxController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return json response
     */
    public function index()
    {
        $tax = Tax::where('code', 'PPN')->first();
        return $this->makeResponse($tax);
    }
}
