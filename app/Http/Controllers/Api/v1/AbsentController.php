<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Setting\Entities\Calendar;
use Modules\HR\Entities\StaffAttendance;
use Illuminate\Support\Carbon;
use App\User;
use Modules\HR\Entities\Employee;
use Modules\HR\Entities\ShiftSetting;
use Modules\HR\Entities\WorkingHour;

class AbsentController extends ApiController
{
    public function index(Request $request)
    {
        $user = User::findOrFail(request()->user_id);
        $date = request()->date;
        $employee = Employee::where('user_id', $user->id)->first();
        if (!$employee) return $this->respondValidationError("Please check your id, employee doesn't exist");
        $calendar = Calendar::where('date', $date)->where('user_type_id', $user->user_type_id)->first();
        if (!$calendar) return $this->respondValidationError("Please check your id, calendar doesn't exist");
        $absent = StaffAttendance::where('calendar_id', $calendar->id)->where('employee_id', $employee->id)->where('date', $date)->first();
        if (!$absent) return $this->respondValidationError($employee->name . " belum absen pada tanggal ini");
        return $this->makeResponse($absent);
    }

    public function checkin(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required'
        ]);

        $user = User::findOrFail($request->user_id);
        if (!$user)  return $this->respondValidationError("Please check your id, user doesn't exist");

        $employee = Employee::where('user_id', $request->user_id)->first();
        if (!$employee) return $this->respondValidationError("Please check your id, employee doesn't exist");

        $date = Carbon::now()->format('Y-m-d');
        $time = Carbon::now()->format("H:i:s");
        $calendar = Calendar::where('date', $date)->where('user_type_id', $user->user_type_id)->first();
        if (!$calendar) return $this->respondValidationError("Please check your id, calendar doesn't exist");

        if ($employee->working_hour == 'Shift') {
            $shiftSetting = ShiftSetting::where('employee_id', $employee->id)->where('calendar_id', $calendar->id)->first();
            if (!$shiftSetting) return $this->respondValidationError('Shift setting Anda untuk tanggal ' . $date . ' belum di buat, silahkan hubungi admin untuk tindak lanjut');

            $workingHour = WorkingHour::find($shiftSetting->working_hour_id);
        } else {
            $workingHour = WorkingHour::where('name', $employee->working_hour)->first();
        }

        if (!$workingHour) return $this->respondValidationError('Pengaturan jam kerja Anda tidak tersedia');

        $whTimeFrom = Carbon::create($workingHour->time_from)->format('H:i:s');
        $whTimeFrom1 = Carbon::create($workingHour->time_from)->addMinute()->format('H:i:s');

        $late = '';
        $overtime = '';
        if ($time > $whTimeFrom1) {
            $late = Carbon::parse($whTimeFrom)->diff($time)->format('%H:%I:%S');
        }

        $checkAbsent = StaffAttendance::where('calendar_id', $calendar->id)->where('employee_id', $employee->id)->where('date', $date)->first();

        if ($checkAbsent) {
            // $checkAbsent->ip_address = '0000';
            // $checkAbsent->latitude ='0000';
            // $checkAbsent->longitude = '0000';
            $checkAbsent->save();
        } else {
            $checkAbsent = new StaffAttendance();
            $checkAbsent->time_in = $time;
            // $checkAbsent->ip_address = '0000';
            $checkAbsent->date = $date;
            // $checkAbsent->latitude = '0000';
            // $checkAbsent->longitude = '0000';
            $checkAbsent->late = $late;
            $checkAbsent->overtime = $overtime;
            $checkAbsent->employee_id = $employee->id;
            $checkAbsent->calendar_id = $calendar->id;
            $checkAbsent->save();
        }

        if ($request->hasFile('foto_absen_in')) {
            if ($checkAbsent->getMedia('foto_absen_in')->first()) {
                $checkAbsent->getMedia('foto_absen_in')->first()->delete();
            }
            $checkAbsent->addMedia($request->foto_absen_in)->preservingOriginal()->toMediaCollection('foto_absen_in');
        }
        $checkAbsent = $checkAbsent->refresh();
        $fotoAbsenIn = $checkAbsent->getFirstMedia('foto_absen_in');

        $all = [];
        $media = $fotoAbsenIn ? $fotoAbsenIn->getFullUrl() : '';
        array_push($all, [
            'data' => $checkAbsent,
            'foto_absen_in' => $media,

        ]);

        return $this->respondCreated($all);
    }

    public function checkout(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required'
        ]);

        $user = User::findOrFail($request->user_id);
        if (!$user)  return $this->respondValidationError("Please check your id, user doesn't exist");

        $employee = Employee::where('user_id', $request->user_id)->first();
        if (!$employee) return $this->respondValidationError("Please check your id, employee doesn't exist");

        $date = Carbon::now()->format('Y-m-d');
        $time = Carbon::now()->format("H:i:s");
        $calendar = Calendar::where('date', $date)->where('user_type_id', $user->user_type_id)->first();
        if (!$calendar) return $this->respondValidationError("Please check your id, calendar doesn't exist");

        if ($employee->working_hour == 'Shift') {
            $shiftSetting = ShiftSetting::where('employee_id', $employee->id)->where('calendar_id', $calendar->id)->first();
            if (!$shiftSetting) return $this->respondValidationError('Shift setting Anda untuk tanggal ' . $date . ' belum di buat, silahkan hubungi admin untuk tindak lanjut');

            $workingHour = WorkingHour::find($shiftSetting->working_hour_id);
        } else {
            $workingHour = WorkingHour::where('name', $employee->working_hour)->first();
        }

        if (!$workingHour) return $this->respondValidationError('Pengaturan jam kerja Anda tidak tersedia');

        $whTimeUntil = Carbon::create($workingHour->time_until)->format('H:i:s');
        $whTimeUntil1 = Carbon::create($workingHour->time_until)->addMinute()->format('H:i:s');

        $late = '';
        $overtime = '';
        if ($time > $whTimeUntil1) {
            $overtime = Carbon::parse($whTimeUntil)->diff($time)->format('%H:%I:%S');
        }

        $checkAbsent = StaffAttendance::where('calendar_id', $calendar->id)->where('employee_id', $employee->id)->where('date', $date)->first();

        if ($checkAbsent) {
            $checkAbsent->time_out = $time;
            $checkAbsent->overtime = $overtime;
            $checkAbsent->ip_address = '0000';
            $checkAbsent->latitude =  '0000';
            $checkAbsent->longitude = '0000';
            $checkAbsent->save();
        } else {
            $checkAbsent = new StaffAttendance();
            $checkAbsent->time_out = $time;
            $checkAbsent->ip_address = '0000';
            $checkAbsent->date = $date;
            $checkAbsent->latitude = '0000';
            $checkAbsent->longitude = '0000';
            $checkAbsent->late = $late;
            $checkAbsent->overtime = $overtime;
            $checkAbsent->employee_id = $employee->id;
            $checkAbsent->calendar_id = $calendar->id;
            $checkAbsent->save();
        }

        if ($request->hasFile('foto_absen_out')) {
            if ($checkAbsent->getMedia('foto_absen_out')->first()) {
                $checkAbsent->getMedia('foto_absen_out')->first()->delete();
            }
            $checkAbsent->addMedia($request->foto_absen_out)->preservingOriginal()->toMediaCollection('foto_absen_out');
        }

        $checkAbsent = $checkAbsent->refresh();
        $fotoAbsenOut = $checkAbsent->getFirstMedia('foto_absen_out');

        $all = [];
        $media = $fotoAbsenOut ? $fotoAbsenOut->getFullUrl() : '';
        array_push($all, [
            'data' => $checkAbsent,
            'foto_absen_out' => $media,

        ]);

        return $this->respondCreated($all);
    }
}
