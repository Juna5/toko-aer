<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Procurement\Entities\Customer;
use Modules\Procurement\Entities\WarehouseMaster;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class CustomerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return json response
     */
    public function index()
    {
        $employee = Employee::where('user_id', request()->user_id)->first();

        $depot_id = $employee ? $employee->depot_id : null;
        if (request()->id) {
            $customers = Customer::with('location', 'priceCategory', 'businessIndustry', 'employee.depot')->whereHas('employee', function($query) use($depot_id){
                $query->when($depot_id, function($q) use($depot_id) {
                    $q->where('depot_id', $depot_id);
                });
            })->findOrFail(request()->id);
            return $this->makeResponse($customers);
        } else {
            $customers = Customer::with('location', 'priceCategory', 'businessIndustry', 'employee.depot')->whereHas('employee', function($query) use($depot_id){
                $query->when($depot_id, function($q) use($depot_id) {
                    $q->where('depot_id', $depot_id);
                });
            })->orderBy('updated_at','desc')->get();
            return $this->makeResponse($customers);
            $customers->get();
        }
        if (!$customers) return $this->respondNotFound();
    }

    public function show()
    {
        $id = request()->id;
        $customer = Customer::with('location', 'priceCategory', 'businessIndustry', 'employee.depot')->findorFail($id);

        if (!$customer) return $this->respondNotFound();

        return $this->makeResponse($customer);
    }

    public function store(Request $request)
    {
        $employee = Employee::where('user_id', $request->user_id)->first();
        if (!$employee) return $this->respondValidationError("Please check your id, employee doesn't exist");

        $checkCustomer = Customer::whereRaw("UPPER(company_name) LIKE '%". strtoupper($request->company_name)."%'")->where('phone',$request->phone)->first();
        if ($checkCustomer) return $this->respondValidationError("Customer " .$request->company_name. " - ".$request->phone." exist");

        $customer = Customer::create([
            'company_name'         => $request->company_name,
            'phone'                => $request->phone,
            'employee_id'          => $employee->id,
        ]);

        $customers = Customer::with('location', 'priceCategory', 'businessIndustry', 'employee.depot')->findOrFail($customer->id);

        return $this->makeResponse($customers, null);
    }
}
