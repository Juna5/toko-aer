<?php

namespace App\Http\Controllers\Api\v1\Auth;

use App\Http\Controllers\ApiController;
use App\User;
use Illuminate\Http\Request;
use Modules\Procurement\Entities\Customer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class RegisterController extends ApiController
{
    public function register(Request $request)
    {
        $this->validate($request, [
            // 'username' => 'required|unique:users',
            'password' => 'required|min:6|max:20',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required',
            'dob' => 'nullable|date',
            'address' => 'nullable'
        ]);

        DB::beginTransaction();
        try {

            $checkCustomer = Customer::whereRaw("UPPER(company_name) LIKE '%". strtoupper($request->company_name)."%'")->where('phone',$request->phone)->first();
            if ($checkCustomer) return $this->respondValidationError("Customer " .$request->company_name. " - ".$request->phone." exist");
        
            $user = User::create([
                'name' => $request->name,
                'username' => $request->email,
                'password' => Hash::make($request->password),
                'active' => true,
                'user_type_id' => 2,
                'email' => $request->email
            ]);

            $rand = str_random(6);

            $qr = generateRandomStringBasedOnDB(Customer::where('qr_code', $rand)->exists(), 6);

            $customer = Customer::create([
                'company_name'         => $request->name,
                'phone'                => $request->phone,
                'email'                => $request->email,
                'dob'                  => $request->dob,
                'user_id'              => $user->id,
                'address'              => $request->address,
                'qr_code'              => $qr
            ]);

            DB::commit();
            return $this->makeResponse($customer, 'Success');

        } catch (\Exception $e){
            DB::rollback();

            return $this->respondValidationError('Register failed, error:' . $e->getMessage());
        }
    }
}
