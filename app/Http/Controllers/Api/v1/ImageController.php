<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\Models\Media;

class ImageController extends Controller
{
    public function destroy(Request $request)
    {
        $this->validate($request, [
            'id' => 'required|integer'
        ]);

        $image = Media::findOrFail($request->input('id'));
        $image->delete();

        return response()->json('Image has been deleted');
    }
}
