<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Procurement\Entities\Product;
use Illuminate\Support\Facades\DB;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Modules\Setting\Entities\ExcludeProduct;

class ProductController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return json response
     */
    public function index()
    {
        $depotId = request()->depot_id;
        if($depotId){
            $exclude = ExcludeProduct::where('depot_id', $depotId)->whereNull('deleted_at')->get();
            $products = Product::with('productCategory', 'location', 'priceCategory', 'warehouseMaster', 'supplier', 'uom', 'weightClass','productPrice')
            ->whereNotIn('id', $exclude->pluck('product_id'))
            ->orderByRaw('product_category_id, code asc')
            ->get();
        }else{
            $products = Product::with('productCategory', 'location', 'priceCategory', 'warehouseMaster', 'supplier', 'uom', 'weightClass','productPrice')
            ->orderByRaw('product_category_id, code asc')
            ->get();
        }

        return $this->makeResponse($products);
    }

    public function show()
    {
        $id = request()->id;
        $product = Product::with('productCategory', 'location', 'priceCategory', 'warehouseMaster', 'supplier', 'uom', 'weightClass','productPrice')->findorFail($id);

        if (!$product) return $this->respondNotFound();

        return $this->makeResponse($product);
    }
}
