<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use Modules\Procurement\Entities\Customer;
use Modules\HR\Entities\Employee;
use Illuminate\Support\Facades\DB;

class ProfileController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return json response
     */
    public function index()
    {
        $user = auth()->user();
        if($user->user_type_id == 1){
            return $this->respondServerError("You don't have permission to access this");
        }

        $customer = Customer::with('location', 'priceCategory', 'businessIndustry', 'employee.depot', 'user')->where('user_id', $user->id)->first();
        if (!$customer) return $this->respondNotFound();

        return $this->makeResponse($customer);
        }

    public function show()
    {
        $user = auth()->user();
        if($user->user_type_id == 1){
            return $this->respondServerError("You don't have permission to access this");
        }
        $id = request()->id;
        $customer = Customer::with('location', 'priceCategory', 'businessIndustry', 'employee.depot')->findorFail($id);

        if (!$customer) return $this->respondNotFound();

        return $this->makeResponse($customer);
    }
}
