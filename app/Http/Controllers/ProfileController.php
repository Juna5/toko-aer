<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Profile;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = Profile::where('user_id', auth()->user()->id)->first();
        return view('profile.index', compact('profile'));
    }

    public function store(Request $request)
    {
        Profile::where('user_id', auth()->user()->id)->update([
            'pob' => $request->pob,
            'dob' => $request->dob,
            'gender' => $request->gender,
            'address' => $request->address,
            'phone' => $request->phone,
            'mobile_phone' => $request->mobile_phone,
            'nationality' => $request->nationality,
            'religion' => $request->religion,
        ]);

        if (request()->hasFile('avatar')) {
            $file = request()->file('avatar');
            $fileExtension = $file->getClientOriginalExtension();
            $mimeType = $file->getClientMimeType();
            $fileSize = $file->getClientSize();
            $name = uniqid().'.'.$fileExtension;

            $this->uploadAndRemove($file, $name, auth()->user()->avatar, '/');

            auth()->user()->update([
                'avatar' => $name
            ]);
        }

        flash('Your data has been updated')->success();
        return redirect()->back();
    }
}
