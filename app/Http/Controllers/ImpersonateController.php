<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use NicoAudy\Impersonate\Impersonate;
use Yajra\DataTables\DataTables;

class ImpersonateController extends Controller
{
     public function index()
     {
          return view('impersonate.index');
     }

     public function impersonate($id)
     {
          $user = User::find($id);
          if ($user->role <> 'Super User') {
               auth()->user()->setImpersonating($user->id);
          } else {
               flash('Impersonate disabled for this role')->error();
          }
          return redirect()->route('home');
     }

     public function stopImpersonating()
     {
          auth()->user()->stopImpersonating();
          flash('Welcome Back')->success();
          return redirect()->route('home');
     }

     public function datatable()
     {
         $rows = User::where('id', '<>', auth()->user()->id)->get();
         return Datatables::of($rows)
             ->editColumn('username', function ($row) {
                 $detail = '<a href="' . route('impersonate.impersonate', ['id' => $row->id]) . '" >' . $row->username . '</a>';
                 return $detail;
             })
             ->rawColumns(['username'])
             ->make(true);
     }
}
