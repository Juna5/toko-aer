<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Modules\HR\Entities\Employee;
use Modules\CashBook\Entities\CashDetail;

// function buat_active($uri, $output = 'active')
// {
//     if (is_array($uri)) {
//         foreach ($uri as $u) {
//             if (Route::is($u)) {
//                 return $output;
//             }
//         }
//     } else {
//         if (Route::is($uri)) {
//             return $output;
//         }
//     }
// }
if (!function_exists('set_active')) {
    function set_active($uri, $output = 'active')
    {
        if (is_array($uri)) {
            foreach ($uri as $u) {
                if (Route::is($u)) {
                    return $output;
                }
            }
        } else {
            if (Route::is($uri)) {
                return $output;
            }
        }
    }
}

if (!function_exists('get_file_from_storage')) {
    function get_file_from_storage($folder, $filename)
    {
        $path = $folder. $filename;
        return Storage::url($path);
    }
}

if (!function_exists('userCan')) {
    function userCan($permission)
    {
        return auth()->user()->can($permission);
    }
}

if (!function_exists('format_d_month_y')) {
    function format_d_month_y($datetime)
    {
        if (!is_null($datetime)) {
            return \Carbon\Carbon::parse($datetime)->format('d M Y');
        }
    }
}

// function barcode($number){

// }
if (!function_exists('country_list')) {
    function country_list()
    {
        $path = public_path() . '/country-list.json';
        $json = json_decode(file_get_contents($path));
        return $json;
    }
}

if (!function_exists('generate_code_sequence')) {
function generate_code_sequence($identifier, $latestOrder, $digit)
{
    $last = $latestOrder + 1;

    return str_pad($last, $digit, "0", STR_PAD_LEFT) . '/' . $identifier . '/AIR/' . date('m') . '/' . date('Y');
}
}

if (!function_exists('format_d_month_y_time')) {
function format_d_month_y_time($datetime)
{
    if (!is_null($datetime)) {
        return \Carbon\Carbon::parse($datetime)->format('d M Y H:i');
    }
}
}

if (!function_exists('penyebut')) {
function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
        $temp = penyebut($nilai - 10). " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai/10)." puluh". penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai/100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai/1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai/1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai/1000000000) . " milyar" . penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai/1000000000000) . " trilyun" . penyebut(fmod($nilai,1000000000000));
    }
    return $temp;
}
}

if (!function_exists('terbilang')) {
function terbilang($nilai) {
    if($nilai<0) {
        $hasil = "minus ". trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}
}

if (!function_exists('amount_international_with_comma')) {
function amount_international_with_comma($number)
{
    return number_format($number, 0, ',', ',');
}
}

if (!function_exists('filterModuleByStatusCount')) {
function filterModuleByStatusCount($model, $status = 'waiting')
{
    $employee = Employee::where('user_id', auth()->user()->id)->first();

    $roleNames = auth()->user()->getRoleNames();
    $query_on_roles = $model::whereStatus($status)
        ->whereIn('role_approval', $roleNames)
        ->get();

    $query_on_user = $model::whereStatus($status)
        ->where('approver_to_go', $employee ? $employee->id : 0)
        ->get();

    $trans_roles_to_array = json_decode(json_encode($query_on_roles), true);
    $trans_users_to_array = json_decode(json_encode($query_on_user), true);

    // dd($roleNames, $trans_roles_to_array, $trans_users_to_array);
    $query = array_merge($trans_roles_to_array, $trans_users_to_array);
    return collect($query)->count();
}
}

if (!function_exists('filterModuleByStatus')) {
function filterModuleByStatus($model, $relation = [], $status = 'waiting')
{
    $employee = Employee::where('user_id', auth()->user()->id)->first();

    $roleNames = auth()->user()->getRoleNames();
    $query_on_roles = $model::with($relation)->whereStatus('waiting')
        ->whereIn('role_approval', $roleNames)
        ->get();
    $query_on_user = $model::with($relation)->whereStatus('waiting')
        ->where('approver_to_go', $employee->id)
        ->get();

    $trans_roles_to_array = json_decode(json_encode($query_on_roles), true);
    $trans_users_to_array = json_decode(json_encode($query_on_user), true);

    $query = array_merge($trans_roles_to_array, $trans_users_to_array);

    return collect($query);
}
}

if (!function_exists('amount_receipt_voucher')) {
function amount_receipt_voucher($id){
    $receiptVoucher = CashDetail::where('cash_id', $id)->get();

    return $receiptVoucher->sum('amount_in');
}
}

if (!function_exists('amount_payment_voucher')) {
function amount_payment_voucher($id){
    $paymentVoucher = CashDetail::where('cash_id', $id)->get();

    return $paymentVoucher->sum('amount_out');
}
}

if (!function_exists('generateRandomStringBasedOnDB')) {
    function generateRandomStringBasedOnDB($query, $length)
    {
        $random_string = str_random($length);

        while ($query) {
            $random_string = str_random($length);
        }
        return $random_string;
    }
}
