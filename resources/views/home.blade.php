@extends('layouts.app')
@push('stylesheet')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.css" />
<!-- <link rel="stylesheet" type="text/css" href=""> -->

@endpush
@section('content')
@php
  $user = auth()->user();
@endphp
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <h3 style="font-family: monospace;"><span style="font-weight: thin;color:#b0b0b0;">Welcome Back,</span><strong>{{ ucfirst($user->name) }}</strong></h2>
                </div>
            </div>
        </div>
    </div>

<!-- @if(auth()->user()->user_type_id)
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Absent</h4>
                  </div>
                  <div class="card-body">
                      <div class="fc-overflow row">
                          <div class="col-lg-4">
                              <a href="" data-toggle="modal" data-target="#absent" id="modal_absent" style="margin-left: 10px; cursor: pointer" class="dropdown-item">
                                  <div class="card border-radius card-icons" style="background-color:#186271;height:150px">
                                      <div class="card-body">
                                          <div class="row">
                                              <div class="col text-center">
                                                  <i class="fas fa-clipboard-list" style="font-size: 64px;color:white"></i>
                                                  <br>
                                                  <br>
                                                  <div class=" font-weight-bold text-icons title" style="color:white">CHECK IN</div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </a>
                          </div>
                          <div class="col-lg-4">
                                <a href="" data-toggle="modal" data-target="#checkout" id="modal_checkout" style="margin-left: 10px; cursor: pointer" class="dropdown-item">
                                    <div class="card border-radius card-icons" style="background-color:red; height:150px">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col text-center">
                                                    <i class="fas fa-clipboard-list" style="font-size: 64px;color:white"></i>
                                                    <br>
                                                    <br>
                                                    <div class=" font-weight-bold text-icons title" style="color:white">CHECK OUT</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Calendar</h4>
                    </div>
                    <div class="card-body">
                        <div class="fc-overflow">
                          <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> -->

    <div class="section-body">
        <div class="row">
            <div class="col-12">
               <div class="row">
                    <div class="col-lg-8 col-md-12 col-sm-12 col-12">
                        <div class="row">
                          <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                              <div class="card card-statistic-1">
                                  <div class="card-icon" style="background-color: #6777ef !important">
                                      <i class="fa fa-user" style="color: white; font-size:30px;"></i>
                                  </div>
                                  <div class="card-wrap">
                                      <div class="card-header">
                                          <h4>Employee</h4>
                                      </div>
                                      <div class="card-body">
                                          {{ $employee }}
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                              <div class="card card-statistic-1">
                                  <div class="card-icon" style="background-color: #ffa426 !important">
                                      <i class="fa fa-university" style="color: white;font-size:30px;"></i>
                                  </div>
                                  <div class="card-wrap">
                                      <div class="card-header">
                                          <h4>Depot</h4>
                                      </div>
                                      <div class="card-body">
                                          {{ $depot }}
                                      </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                              <div class="card">
                                  <div class="card-header"><strong><a href="{{ route('transaction.sales_order.report.index')}}" target="_blank">Penjualan</a></strong></div>
                                  <div class="card-body">
                                      <div class="table-responsive table-invoice">
                                          <table class="table table-striped">
                                            <tbody>
                                              <tr>
                                                <th>Code Number</th>
                                                <th>Date</th>
                                                <th>Created By</th>
                                                <th>Depot</th>
                                                <th>Total</th>
                                              </tr>
                                              @foreach($salesOrder as $index => $so)
                                                <tr align="center">
                                                    <td>{{ $so->code_number }}</td>
                                                    <td>{{ format_d_month_y($so->date) }}</td>
                                                    <td>{{ optional($so->employee)->name }}</td>
                                                    <td>{{ optional($so->depot)->name }}</td>
                                                    <td align="right">{{ amount_international_with_comma($so->total ,0,",",",") }}</td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                          </table>
                                        </div>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                          <div class="card">
                            <div class="card-body" >
                              <div id="calendar"></div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </div>
            </div>
        </div>
        <div class="row">
            @php
                $employee = \Modules\HR\Entities\Employee::where('user_id', $user->id)->first();
            @endphp
            @foreach($stocks as $row)
                @if($employee)
                    @if($employee->depot_id == $row->warehouse_id)
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="card card-statistic-2">
                                <div class="card-icon shadow-primary bg-success"><i class="fas fa-database"></i></div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Depot {{ optional($row->warehouse)->name }}</h4>
                                    </div>
                                    <div class="card-body">
                                       {{ amount_international_with_comma($row->qty) }} <span style="font-size:13px;">Liter</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @else
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="card card-statistic-2">
                            <div class="card-icon shadow-primary bg-success"><i class="fas fa-database"></i></div>
                            <div class="card-wrap">
                                <div class="card-header">
                                    <h4>Depot {{ optional($row->warehouse)->name }}</h4>
                                </div>
                                <div class="card-body">
                                   {{ amount_international_with_comma($row->qty) }} <span style="font-size:13px;">Liter</span>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                    
            @endforeach
        </div>
    </div> 
</div>
@endsection
@push('javascript')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js" integrity="sha256-4iQZ6BVL4qNKlQ27TExEhBN1HFPvAvAMbFavKKosSWQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.js"></script>

@include('absent.checkoutmodal')
@include('absent.modal')
<script>
  $(document).ready(function () {
      var SITEURL = "{{url('/home')}}";
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      var calendar = $('#calendar').fullCalendar({
          editable: true,
          // events: SITEURL,
          // eventRender: function (event, element, view) {
          //     const fontWeight = event.description == "Working Day" ? '' : 'bold';
          // element.find('.fc-title').append(`
          //    <span style="font-size: 15px; color: white; font-weight: ${fontWeight}" class="text-center">${event.description} </span>
          //    `);
          // },
          displayEventTime: true,
          editable: true,
          eventColor: 'white',
          textColor: 'red',
          selectable: true,
          selectHelper: true,
      });
  });

  $(document).on('click', '#modal_absent', function(){
      var requestUrl = '{{ route('home.show_absent') }}';
      $('#absent').modal('hide');

        $.ajax({
            url: requestUrl,
            type: "GET",
            success: function(media) {
             $("#absent").find('.modal-body').html(media)
            }
        });
  });

  $(document).on('click', '#modal_checkout', function(){
      var urlCheckout = '{{ route('home.show_absent_checkout') }}';
      $('#checkout').modal('hide');

        $.ajax({
            url: urlCheckout,
            type: "GET",
            success: function(media) {
             $("#checkout").find('.modal-body').html(media)
            }
        });
  });
</script>
@endpush
