@extends('layouts.app')

@section('content')
<section class="section">
  <div class="section-header">
    <h1>Profile</h1>
    <div class="section-header-breadcrumb">
      <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
      <div class="breadcrumb-item">Profiler</div>
    </div>
  </div>
  <div class="section-body">
    @include('flash::message')
    <div class="row">

      <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <div class="text-center">
              <img src="{{ auth()->user()->avatar == 'default.jpg' ? asset('images/default.jpg') : get_file_from_storage('/', auth()->user()->avatar) }}" class="img-circle img-thumbnail rounded-circle mb-4" alt="profile" style="height: 150px">
            </div>
          </div>
          <div class="col-sm-9">
            <form method="POST" action="{{route('profile.store')}}" enctype="multipart/form-data">
              @csrf
              <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" name="username" placeholder="Username" readonly value="{{ auth()->user()->username }}">
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" placeholder="Email" readonly value="{{ auth()->user()->email }}">
              </div>
              <div class="form-group">
                <label for="last_login">Last Login</label>
                <input type="text" class="form-control" name="last_login" placeholder="Last Login" readonly value="{{ auth()->user()->last_login }}">
              </div>
              <div class="form-group">
                <div class="input-group mb-3">
                  <label class="custom-file border">
                    <input type="file" name="avatar" class="custom-file-input">
                    <span class="custom-file-control pr-3" style="white-space: nowrap;">Change Photo</span>
                    <div class="input-group-append">
                      <span class="input-group-text">Select</span>
                    </div>
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label>Place of Birth</label>
                <input type="text" name="pob" value="{{ $profile->pob }}" class="form-control">
              </div>
              <div class="form-group">
                <label>Date of Birth</label>
                <input type="date" name="dob" value="{{ $profile->dob }}" class="form-control datepicker">
              </div>
              <div class="form-group">
                <div class="control-label">Gender</div>
                <div class="custom-switches-stacked mt-2">
                  <label class="custom-switch">
                    <input type="radio" name="gender" value="1" required class="custom-switch-input" {{ $profile->gender == "1" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Male</span>
                  </label>
                  <label class="custom-switch">
                    <input type="radio" name="gender" value="0" required class="custom-switch-input" {{ $profile->gender == "0" ? 'checked' : '' }}>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description">Female</span>
                  </label>
                </div>
              </div>
              <div class="form-group">
                <label>Address</label>
                <textarea name="address" class="form-control">{{ $profile->address }}</textarea>
              </div>
              <div class="form-group">
                <label>Phone</label>
                <input type="text" name="phone" value="{{ $profile->phone }}" class="form-control">
              </div>
              <div class="form-group">
                <label>Mobile Phone</label>
                <input type="text" name="mobile_phone" value="{{ $profile->mobile_phone }}" class="form-control">
              </div>
              <div class="form-group">
                <label>Nationality</label>
                <input type="text" name="nationality" value="{{ $profile->nationality }}" class="form-control">
              </div>
              <div class="form-group">
                <label>Religion</label>
                <select name="religion" class="form-control select2">
                  <option value="Islam" {{ $profile->religion == "Islam" ? 'selected' : '' }}>Islam</option>
                  <option value="Buddhism" {{ $profile->religion == "Buddhism" ? 'selected' : '' }}>Buddhism</option>
                  <option value="Taoism" {{ $profile->religion == "Taoism" ? 'selected' : '' }}>Taoism</option>
                  <option value="Hinduism" {{ $profile->religion == "Hinduism" ? 'selected' : '' }}>Hinduism</option>
                  <option value="Sikhism" {{ $profile->religion == "Sikhism" ? 'selected' : '' }}>Sikhism</option>
                  <option value="Catholicism" {{ $profile->religion == "Catholicism" ? 'selected' : '' }}>Catholicism</option>
                  <option value="Christianity" {{ $profile->religion == "Christianity" ? 'selected' : '' }}>Christianity</option>
                  <option value="No Religion" {{ $profile->religion == "No Religion" ? 'selected' : '' }}>No Religion</option>
                  <option value="PREFER NOT TO SAY" {{ $profile->religion == "Prefer Not to Say" ? 'selected' : '' }}>Prefer Not to Say</option>
                  <option value="THERE IS NO FAITH" {{ $profile->religion == "There Is no Faith" ? 'selected' : '' }}>There Is no Faith.</option>
                  <option value="Others" {{ $profile->religion == "Others" ? 'selected' : '' }}>Others</option>
                </select>
              </div>
              <div class="form-group">
                <button class="btn btn-primary" type="submit"><i class="fa fa-save"></i> Save</button>
              </div>
          </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection