<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        @php
            $logo = \App\Models\Logo::where('active', true)->first();
        @endphp
        <a href="{{ url('/home') }}">
        <img src="{{  $logo ? asset("uploads/path/$logo->path") : asset('images/default_logo.png') }}" alt="logo" width="50" >
        </a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="#">{{ strtoupper(substr(env('APP_NAME'), 0, 2)) }}</a>
    </div>
    <ul class="sidebar-menu">
        {{-- <li class="menu-header">Dashboard</li> --}}
        {{-- <li class="{{ request()->is('/home') ? 'active' : '' }}">
            <a class="nav-link" href="{{ url('/home') }}">
                <i class="fa fa-columns"></i>
                <span>Dashboard</span>
            </a>
        </li> --}}
        <br>
        <li class="">
            <a class="nav-link" href="{{ route('procurement.master.index')}}">
                <i class="fa fa-cog"></i>
                <span>Settings</span>
            </a>
        </li>
        <li class="">
            <a class="nav-link" href="{{ route('procurement.view')}}">
                <i class="fa fa-box"></i>
                <span>Pembelian</span>
            </a>
        </li>
        <li class="">
            <a class="nav-link" href="{{ route('billing.view')}}">
                <i class="fa fa-file-invoice-dollar"></i>
                <span>Penjualan</span>
            </a>
        </li>
        @can('view supplier')
        <li class="">
            <a class="nav-link" href="{{ route('procurement.master.supplier.index')}}">
                <i class="fa fa-industry"></i>
                <span>Supplier</span>
            </a>
        </li>
        @endcan
        @can('view customer')
        <li class="">
            <a class="nav-link" href="{{ route('procurement.master.customer.index')}}">
                <i class="fa fa-user"></i>
                <span>Customer</span>
            </a>
        </li>
        @endcan
        @can('view project')
        <li class="">
            <a class="nav-link" href="{{ route('procurement.master.project.index')}}">
                <i class="fa fa-project-diagram"></i>
                <span>Project</span>
            </a>
        </li>
        @endcan
        <li class="">
            <a class="nav-link" href="{{ route('operational.view') }}">
                <i class="fa fa-wrench"></i>
                <span>Operational</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown">
                <i class="fa fa-user-friends"></i>
                <span>Human Resources</span>
            </a>
            <ul class="dropdown-menu">
                @can('view employee')
                <li>
                    <a class="nav-link" href="{{ route('hr.employee.index')}}">
                        <span>Employee</span>
                    </a>
                </li>
                @endcan
                @can('view hr setting')
                    <li>
                        <a class="nav-link" href="{{ route('hr.setting.setting-index')}}">
                            <span>Setting</span>
                        </a>
                    </li>
                @endcan
                <li>
                    <a class="nav-link" href="{{ route('hr.master.index')}}">
                        <span>Master</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('hr.leave.view')}}">
                        <span>Leave</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('hr.shift-task.index')}}">
                        <span>Shift Task</span>
                    </a>
                </li>
                <li>
                    <a class="nav-link" href="{{ route('hr.report.index')}}">
                    <span>Report</span>
                    </a>
                </li>
            </ul>
        </li>
        @can('view project')
        @endcan
        <li class="">
            <a class="nav-link" href="{{ route('cashbook.view') }}">
                <i class="fas fa-cash-register"></i>
                <span>Kas</span>
            </a>
        </li>

        <li class="">
            <a class="nav-link" href="{{ route('procurement.voucher.index') }}">
                <i class="fas fa-ticket-alt"></i>
                <span>Voucher</span>
            </a>
        </li>

        <li class="dropdown">
            @can('view stock')
            <a href="#" class="nav-link has-dropdown">
                <i class="fa fa-shopping-bag"></i>
                <span>Stock</span>
            </a>
            <!-- <ul class="dropdown-menu">
                <li>
                    <a class="nav-link" href="{{ route('stock.stock.index')}}">
                        <span>Stock </span>
                    </a>
                </li>
            </ul> -->
            @endcan
            @can('view stock adjustment')
             <ul class="dropdown-menu">
                <li>
                    <a class="nav-link" href="{{ route('stock.stock-adjustment.index')}}">
                        <span>Stock Adjustment</span>
                    </a>
                </li>
            </ul>
            <ul class="dropdown-menu">
                <li>
                    <a class="nav-link" href="{{ route('stock.stock-adjustment-report')}}">
                        <span>Stock Adjustment Reports</span>
                    </a>
                </li>
            </ul>
            @endcan
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-user-shield"></i>
                <span>Administration</span>
            </a>
            <ul class="dropdown-menu">
                @can('view role')
                <li>
                    <a class="nav-link" href="{{ route('admin.roles.index') }}">
                        <span>Roles</span>
                    </a>
                </li>
                @endcan
                @can('view permission')
                <li>
                    <a class="nav-link" href="{{ route('admin.permissions.index') }}">
                        <span>Permissions</span>
                    </a>
                </li>
                @endcan
                @can('impersonate')
                <li>
                    <a class="nav-link" href="{{ route('impersonate.index') }}">
                        <span>Impersonate</span>
                    </a>
                </li>
                @endcan
                <li>
                    <a class="nav-link" href="{{ route('setting.index') }}">
                        <span>Setting</span>
                    </a>
                </li>
                @can('view logo')
                <li>
                    <a class="nav-link" href="/logo">
                        <span>Logo</span>
                    </a>
                </li>
                @endcan
            </ul>
        </li>
    </ul>
</aside>
