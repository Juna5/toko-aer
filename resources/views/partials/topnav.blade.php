<form class="form-inline mr-auto" action="">
    <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
    </ul>
</form>
@php
$approvalLeave = filterModuleByStatusCount(Modules\HR\Entities\Leave::class);

$employee = Modules\HR\Entities\Employee::where('user_id', auth()->user()->id)->first();

$rejectLeave = Modules\HR\Entities\Leave::where('employee_id', $employee ? $employee->id : 0)->where('status', 'rejected')->count();

$sumNotif = $approvalLeave + $rejectLeave;

@endphp
<ul class="navbar-nav navbar-right">
    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg {{ $sumNotif > 0 ? 'beep' : ''}}"><i class="far fa-bell"></i></a>
        <div class="dropdown-menu dropdown-list dropdown-menu-right">
          <div class="dropdown-header">Notifications
          </div>
          <div class="dropdown-list-content dropdown-list-icons">
            <div class="dropdown-item dropdown-item-unread">
                <div class="dropdown-item-icon bg-info text-white">
                  <i class="fas fa-user"></i>
                </div>
                <div class="dropdown-item-desc">
                  Leave
                  <a href="{{ route('hr.leave.approval.index') }}" class="badge badge-pill badge-warning">
                      {{ $approvalLeave }}
                  </a>
                  <a href="{{ route('hr.leave.entry.index') }}" class="badge badge-pill badge-danger">
                      {{ $rejectLeave }}
                  </a>
                </div>
            </div>
            <!-- <a href="#" class="dropdown-item">
              <div class="dropdown-item-icon bg-success text-white">
                <i class="fas fa-check"></i>
              </div>
              <div class="dropdown-item-desc">
                <b>Kusnaedi</b> has moved task <b>Fix bug header</b> to <b>Done</b>
                <div class="time">12 Hours Ago</div>
              </div>
            </a>
            <a href="#" class="dropdown-item">
              <div class="dropdown-item-icon bg-danger text-white">
                <i class="fas fa-exclamation-triangle"></i>
              </div>
              <div class="dropdown-item-desc">
                Low disk space. Let's clean it!
                <div class="time">17 Hours Ago</div>
              </div>
            </a>
            <a href="#" class="dropdown-item">
              <div class="dropdown-item-icon bg-info text-white">
                <i class="fas fa-bell"></i>
              </div>
              <div class="dropdown-item-desc">
                Welcome to Stisla template!
                <div class="time">Yesterday</div>
              </div>
            </a> -->
          </div>
        </div>
    </li>
    <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
             <img alt="image" src="{{ auth()->user()->avatar ? (auth()->user()->avatar == "default.jpg" ? asset('images/default.jpg') :get_file_from_storage('/', auth()->user()->avatar)) : asset('images/default.jpg') }}" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ strtoupper(auth()->user()->name) }}</div>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-title">Welcome, {{ strtoupper(auth()->user()->name) }}</div>
            @if(auth()->user()->isImpersonating() || session()->has('impersonate'))
            <a href="{{ route('impersonate.stop') }}" class="dropdown-item has-icon">
                <i class="fa fa-user-times"></i> Stop Impersonate
            </a>
            @endif
            <a href="{{ route('profile.index') }}" class="dropdown-item has-icon">
                <i class="fa fa-user"></i> Profile Settings
            </a>
            <a href="{{ route('setting.reset-password.index') }}" class="dropdown-item has-icon">
                <i class="fas fa-key"></i> Reset Password
            </a>
            <div class="dropdown-divider"></div>
            <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-sign-out-alt"></i>{{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </li>
</ul>
