<form action="{{ route('home.store_absent') }}" method="POST" enctype="multipart/form-data" id="modal-body">
    @csrf
    @method('POST')
    <div class="form-group"><h5>Absent Form Check In</h5></div>
        <div class="form-group m-form__group">
            <div class="form-group">
                <table width="100%">
                    <tr>
                        <td>Nama</td>
                        <td>:</td>
                        <td>{{ $employee->name }}</td>
                    </tr>
                    <tr>
                        <td>Tanggal</td>
                        <td>:</td>
                        <td>{{ format_d_month_y($date) }}</td>
                    </tr>
                    <tr>
                        <td>Jam</td>
                        <td>:</td>
                        <td class="time"></td>
                    </tr>
                    <tr>
                        <td>IP</td>
                        <td>:</td>
                        <td id="ipspan"></td>
                    </tr>
                   
                    <tr>
                        <td>Latitude</td>
                        <td>:</td>
                        <td id="latspan"></td>
                    </tr>
                    <tr>
                        <td>Longitude</td>
                        <td>:</td>
                        <td id="longspan"></td>
                    </tr>

                </table>
                <input type="hidden" name="ip_address" id="ip" class="form-control" >
                <input type="hidden" name="latitude" id="lat" class="form-control" >
                <input type="hidden" name="longitude" id="long" class="form-control" >
                <input type="hidden" name="time_in" id="time_in" value="" class="timein">
                <br>
                <br>
                <div class="row">
                    <div class="form-group col-md-1">
                        
                        <label class="colorinput">
                            <input name="is_seragam" type="checkbox" value="1" class="colorinput-input" required />
                            <span class="colorinput-color bg-info"></span>
                        </label>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="photo_profile">Saya dalam keadaan sehat</label><br>
                    </div>
                    </div>
            </div>
        </div>

        
        @if($shiftAttendance)
            @if($shiftAttendance->time_in != null)
            <p style="background-color: #d9fae2; padding:5px;">Kamu sudah absen masuk untuk hari ini, {{ $shiftAttendance->time_in }}</p>
            @endif
            
        @endif

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            @if($shiftAttendance)
            @if($shiftAttendance->time_in != null)
            <button type="submit" class="btn btn-primary" id="modal-btn-save" disabled>Submit</button>
            @else
            <button type="submit" class="btn btn-primary" id="modal-btn-save">Submit</button>
            @endif
            @else
            <button type="submit" class="btn btn-primary" id="modal-btn-save">Submit</button>
            @endif
        </div>
        
</form>

<script type="text/javascript">
var request = new XMLHttpRequest();

request.open('GET', 'https://api.ipdata.co/?api-key=e5e7032e4168e2ce161fad6d4f2cf466d08be8d354fa73167be20e6b');

request.setRequestHeader('Accept', 'application/json');

request.onreadystatechange = function () {
  if (this.readyState === 4) {
    var response = JSON.parse(this.responseText)
    document.getElementById("ip").value = response.ip
    document.getElementById("lat").value = response.latitude
    document.getElementById("long").value = response.longitude
    document.getElementById("ipspan").innerHTML = response.ip
    document.getElementById("latspan").innerHTML = response.latitude
    document.getElementById("longspan").innerHTML = response.longitude
  }
};

request.send();


function updateTime() {
      var dateInfo = new Date();

      var hr,
        _min = (dateInfo.getMinutes() < 10) ? "0" + dateInfo.getMinutes() : dateInfo.getMinutes(),
        sec = (dateInfo.getSeconds() < 10) ? "0" + dateInfo.getSeconds() : dateInfo.getSeconds(),
        ampm = (dateInfo.getHours() >= 12) ? "PM" : "AM";

      if (dateInfo.getHours() == 0) {
        hr = 12;
      } else if (dateInfo.getHours() > 12) {
        hr = dateInfo.getHours() - 12;
      } else {
        hr = dateInfo.getHours();
      }

      var currentTime = hr + ":" + _min + ":" + sec;

      document.getElementsByClassName("time")[0].innerHTML = currentTime +" "+ ampm;
      document.getElementsByClassName("timein")[0].value = currentTime +" "+ ampm;

      /* date */
      var dow = [
          "Sunday",
          "Monday",
          "Tuesday",
          "Wednesday",
          "Thursday",
          "Friday",
          "Saturday"
        ],
        month = [
          "January",
          "February",
          "March",
          "April",
          "May",
          "June",
          "July",
          "August",
          "September",
          "October",
          "November",
          "December"
        ],
        day = dateInfo.getDate();
};

updateTime();
setInterval(function() {
  updateTime()
}, 1000);

</script>



