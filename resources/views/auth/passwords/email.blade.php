@extends('layouts.auth')

@section('content')
<div class="card card-warning">
    <div class="card-header">
        <div class="col-7">
            <h4>{{ __('Reset Password') }}</h4>
        </div>
        <div class="col-5 text-right">
            <a class="" href="{{ route('login') }}">{{ __('Login') }}</a>

        </div>
    </div>

    <div class="card-body">
        @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
        @endif
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="form-group">
                <label for="email">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-warning btn-lg btn-block" tabindex="4">
                    {{ __('Send Password Reset Link') }}
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
