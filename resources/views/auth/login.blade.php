@extends('layouts.auth')

@section('content')
<div class="card card-primary">
    <div class="card-header"><h4>{{ __('Login') }}</h4></div>

    <div class="card-body">
        @include('include.error-list')
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group">
                <label for="email">{{ __('Username or email') }}</label>
                <input type="text" class="form-control @error('login') is-invalid @enderror" name="login" value="{{ old('login') }}" autocomplete="email" autofocus>
                @error('login')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <div class="d-block">
                    <label for="password" class="control-label">{{ __('Password') }}</label>
                    <div class="float-right">
                        <a href="{{route('password.request')}}" class="text-small">
                            Forgot Password?
                        </a>
                    </div>
                </div>
                <input id="password" type="password"
                    class="form-control @error('password') is-invalid @enderror" name="password"
                    autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <div class="custom-control custom-checkbox">

                    <input class="custom-control-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}>

                    <label class="custom-control-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                    {{ __('Login') }}
                </button>
            </div>
        </form>
        <div class="text-center mt-4 mb-3">
            <div class="text-job text-muted">Login With Social</div>
        </div>
        <div class="row">
            <div class="col-12">
                <a class="btn btn-block btn-lg btn-warning text-white">
                    Google
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
