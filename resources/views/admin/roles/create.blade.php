@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Add Role</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ route('admin.roles.index') }}">Roles</a></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <div class="text-right">
                                        <a href="{{ route('admin.roles.index') }}" class="btn btn-warning">
                                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                        </a>
                                    </div>
                                </h5>
                                <hr>
                                @include('include.error-list')
                                {{ Form::open(['route' => 'admin.roles.store']) }}
                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>
                                <h5><b>Assign Permissions</b></h5>
                                <div class='form-group'>
                                    @foreach ($permissions as $permission)
                                    {{ Form::checkbox('permissions[]',  $permission->id ) }}
                                    {{ Form::label($permission->name, ucfirst($permission->name)) }}<br>
                                    @endforeach
                                </div>
                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
