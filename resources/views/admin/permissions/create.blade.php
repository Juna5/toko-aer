@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Available Permission</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"> <a href="{{ route('admin.permissions.index') }}">Permission</a></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        @include('include.error-list')

                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <div class="text-right">
                                        <a href="{{ route('admin.permissions.index') }}" class="btn btn-warning">
                                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                        </a>
                                    </div>
                                </h5>
                                <hr>
                                {{ Form::open(['route' => 'admin.permissions.store']) }}
                                <div class="form-group">
                                    {{ Form::label('name', 'Name') }}
                                    {{ Form::text('name', '', array('class' => 'form-control')) }}
                                </div>
                                @if(!$roles->isEmpty())
                                <h4>Assign Permission to Roles</h4>
                                @foreach ($roles as $role)
                                {{ Form::checkbox('roles[]',  $role->id ) }}
                                {{ Form::label($role->name, ucfirst($role->name)) }}
                                @endforeach
                                @endif
                                <br>
                                {{ Form::submit('Add', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
