@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Edit :{{$permission->name}}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"> <a href="{{ route('admin.permissions.index') }}">Permission</a></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        @include('include.error-list')

                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">

                                    <div class="text-right">
                                        <a href="{{ route('admin.permissions.index') }}" class="btn btn-warning">
                                            <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                        </a>
                                    </div>
                                </h5>
                                <hr>
                                {{ Form::model($permission, array('route' => array('admin.permissions.update', $permission->id), 'method' => 'PUT')) }}
                                <div class="form-group">
                                    {{ Form::label('name', 'Permission Name') }}
                                    {{ Form::text('name', null, array('class' => 'form-control')) }}
                                </div>
                                <br>
                                {{ Form::submit('Edit', array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<section>

@endsection
