@extends('layouts.app')

@push ('stylesheet')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endpush

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Impersonate</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
                <div class="breadcrumb-item">Impersonate</div>
            </div>
        </div>
        <div class="section-body">
            <div class="row">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">Impersonate</div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover table-striped" cellspacing="0"
                                               id="datatable"></table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push ('javascript')
    <script src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#datatable').dataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    method: 'POST',
                    url: '{{ route('impersonate.datatable') }}',
                    headers: {
                        'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    }
                },
                columns: [
                    {title: 'User Name', data: 'username', name: 'username', class: 'text-center'},
                    {title: 'Name', data: 'name', name: 'name', class: 'text-center'},
                    {title: 'Email', data: 'email', name: 'email', class: 'text-center'}
                ],
                responsive: true,
                "pageLength": 50
            });
        });
    </script>
@endpush
