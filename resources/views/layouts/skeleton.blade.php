<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title', 'Home') &mdash; {{ env('APP_NAME') }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-material-datetimepicker.css')}}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap-clockpicker.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" rel="stylesheet"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet"/>

    @stack('stylesheet')
    <style media="screen">
    /* Chrome, Safari, Edge, Opera */
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
-webkit-appearance: none;
margin: 0;
}

/* Firefox */
input[type=number] {
-moz-appearance: textfield;
}
    </style>
</head>
<body>
<div id="app">
    @yield('app')
</div>

<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
<script src="{{ mix('js/manifest.js') }}"></script>
<script src="{{ mix('js/vendor.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
<script src="{{ asset('assets/js/jquery-clockpicker.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.full.min.js"></script>
<script src="{{ asset('js/dropify.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap-material-datetimepicker.js')}}"></script>
<script src="{{ asset('js/customd-jquery-number/jquery.number.min.js')  }}" type="text/javascript"></script>

<script type="text/javascript">
    var input = $('.clockPicker');
    input.clockpicker({
        autoclose: true
    });

    $('input[type=number]').on('mousewheel',function(e){ $(this).blur(); });
// Disable keyboard scrolling
$('input[type=number]').on('keydown',function(e) {
    var key = e.charCode || e.keyCode;
    // Disable Up and Down Arrows on Keyboard
    if(key == 38 || key == 40 ) {
  e.preventDefault();
    } else {
  return;
    }
});

    $(document).ready(function () {
        $("input[data-type='currency']").on({
	keyup: function() {
	  formatCurrency($(this));
	},
	blur: function() {
	  formatCurrency($(this), "blur");
	}
});


function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.

  // get input value
  var input_val = input.val();

  // don't validate empty input
  if (input_val === "") { return; }

  // original length
  var original_len = input_val.length;

  // initial caret position
  var caret_pos = input.prop("selectionStart");

  // check for decimal
  if (input_val.indexOf(".") >= 0) {

	// get position of first decimal
	// this prevents multiple decimals from
	// being entered
	var decimal_pos = input_val.indexOf(".");

	// split number by decimal point
	var left_side = input_val.substring(0, decimal_pos);
	var right_side = input_val.substring(decimal_pos);

	// add commas to left side of number
	left_side = formatNumber(left_side);

	// validate right side
	right_side = formatNumber(right_side);

	// On blur make sure 2 numbers after decimal
	if (blur === "blur") {
	  right_side += "00";
	}

	// Limit decimal to only 2 digits
	right_side = right_side.substring(0, 2);

	// join number by .
	input_val = left_side + "." + right_side;

  } else {
	// no decimal entered
	// add commas to number
	// remove all non-digits
	input_val = formatNumber(input_val);
	input_val = input_val;

	// final formatting
	if (blur === "blur") {
	  input_val;
	}
  }

  // send updated string to input
  input.val(input_val);

  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}
        $('.select2').select2({
            placeholder: 'Please Select',
            containerCssClass: 'form-control',
            allowClear: true,
            width: '100%'
        });

        $('.datepicker').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });

        $('.dropify').dropify();

        $('.separator.currency').number(true, 2);
        $('.separator').not('.separator.currency').number(true, 0);
        $('.separator').keyup(function() {
            $(this).next('.separator-hidden').val($(this).val());
        });
    });
</script>
@stack('javascript')
{!! noty_assets() !!}

</body>

</html>
