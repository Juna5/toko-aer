@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Create New Logo</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"> <a href="{{ url('/logo') }}">Logo</a></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">
                                    <div class="text-right">
                                        <a href="{{ url('/logo') }}" title="Back">
                                            <button class="btn btn-warning">
                                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                            </button>
                                        </a>
                                    </div>
                                </h5>

                                {!! Form::open([
                                'url' => '/logo',
                                'class' => 'form-horizontal',
                                'files' => true,
                                'onsubmit' => "submitButton.disabled = true"
                                ]) !!}

                                @include ('logo.form')

                                {!! Form::close() !!}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection