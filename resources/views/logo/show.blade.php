@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Logo {{ $logo->id }}</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item"> <a href="{{ url('/logo') }}">Logo</a></div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="container">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">

                            <div class="card-body">
                                <h5 class="card-title">
                                    <div class="text-right">
                                        <a href="{{ url('/logo') }}" title="Back">
                                            <button class="btn btn-warning">
                                                <i class="fa fa-arrow-left" aria-hidden="true"></i> Back
                                            </button>
                                        </a>
                                    </div>
                                </h5>
                                <a href="{{ url('/logo/' . $logo->id . '/edit') }}" title="Edit Logo">
                                    <button class="btn btn-primary">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                    </button>
                                </a>
                                {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['logo', $logo->id],
                                'style' => 'display:inline'
                                ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                'type' => 'submit',
                                'class' => 'btn btn-danger',
                                'title' => 'Delete Logo',
                                'onclick'=>'return confirm("Confirm delete?")'
                                ))!!}
                                {!! Form::close() !!}

                                <div class="table-responsive py-4">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <th>ID</th>
                                                <td>{{ $logo->id }}</td>
                                            </tr>
                                            <tr>
                                                <th> Path </th>
                                                <td> {{ $logo->path }} </td>
                                            </tr>
                                            <tr>
                                                <th> Active </th>
                                                <td> {{ $logo->active }} </td>
                                            </tr>
                                            <tr>
                                                <th> Description </th>
                                                <td> {{ $logo->description }} </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection