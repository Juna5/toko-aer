@extends('layouts.app')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Logo</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('home') }}">Dashboard</a></div>
            <div class="breadcrumb-item">Logo</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header"></div>
                            <div class="card-body">
                                <a href="{{ url('/logo/create') }}" class="btn btn-success mb-4" title="Add New Logo">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>

                                {!! Form::open(['method' => 'GET', 'url' => '/logo', 'class' => 'navbar-form navbar-right', 'role' => 'search']) !!}
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="Search...">
                                    <span class="input-group-append">
                                        <button class="btn btn-dark" type="submit">
                                            Search
                                        </button>
                                    </span>
                                </div>
                                {!! Form::close() !!}

                                <div class="table-responsive py-4">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Path</th>
                                                <th>Active</th>
                                                <th>Description</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($logo as $item)
                                            <tr>
                                                <td>{{ $loop->index + 1 }}</td>
                                                <td>{{ $item->path }}</td>
                                                <td>{{ $item->active }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>
                                                    <a href="{{ url('/logo/' . $item->id) }}" title="View Logo"><button class="btn btn-info"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                    <a href="{{ url('/logo/' . $item->id . '/edit') }}" title="Edit Logo"><button class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                                                    {!! Form::open([
                                                    'method'=>'DELETE',
                                                    'url' => ['/logo', $item->id],
                                                    'style' => 'display:inline'
                                                    ]) !!}
                                                    {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger',
                                                    'title' => 'Delete Logo',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                </td>
                                            </tr>
                                            @empty
                                            <tr>
                                                <td colspan="{{ config('laravelmanthra.view_columns_number') + 2 }}">
                                                    Data not found, <a href="{{ url('/logo/create') }}"> create new </a>
                                                </td>
                                            </tr>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    <div class="pagination-wrapper"> {!! $logo->appends(['search' => Request::get('search')])->render() !!} </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
</section>

@include('manthra::include.flash_message')
@endsection