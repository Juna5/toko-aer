<div class="form-group {{ $errors->has('path') ? 'has-error' : ''}}">
    {!! Form::label('path', 'Path', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('path', null, ['class' => ($errors->has('path')) ? 'form-control is-invalid' : 'form-control', 'required' => 'required']) !!}

        {!! $errors->first('path', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('active') ? 'has-error' : ''}}">
    {!! Form::label('active', 'Active', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
            <label>{!! Form::radio('active', '1') !!} Yes</label>
        </div>
        <div class="checkbox">
            <label>{!! Form::radio('active', '0', true) !!} No</label>
        </div>
        {!! $errors->first('active', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    {!! Form::label('description', 'Description', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('description', null, ['class' => ($errors->has('description')) ? 'form-control is-invalid' : 'form-control']) !!}

        {!! $errors->first('description', '<div class="invalid-feedback d-block">:message</div>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', [
        'class' => 'btn btn-primary',
        'name' => 'submitButton'
        ]) !!}
    </div>
</div>