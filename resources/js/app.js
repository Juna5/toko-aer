import Select2 from 'v-select2-component';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import VueCurrencyInput from 'vue-currency-input';

require('./bootstrap');

window.Vue = require('vue');

Vue.use(VueSweetalert2);

Vue.use(VueCurrencyInput, {
    globalOptions: {
        allowNegative: false,
        currency: null
    },
    componentName: 'Money'
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
Vue.component('ValidationErrors', require('./components/commons/validation-errors'));
Vue.component('Input', require('./components/commons/input'));
Vue.component('example-component', require('./components/ExampleComponent.vue').default);
// Vue.component(
//     'purchaserequisition-create',
//     require('../../Modules/Procurement/Resources/assets/js/components/purchase_requisition/Create.vue').default
// );
// Vue.component(
//     'purchaserequisition-edit',
//     require('../../Modules/Procurement/Resources/assets/js/components/purchase_requisition/Edit.vue').default
// );
Vue.component(
    "purchase-order-create",
    require("../../Modules/Procurement/Resources/assets/js/components/purchase_order/Create.vue")
    .default
);
Vue.component(
    "purchase-order-edit",
    require("../../Modules/Procurement/Resources/assets/js/components/purchase_order/Edit.vue")
    .default
);
// Vue.component(
//     "quotation-material-create",
//     require("../../Modules/Transaction/Resources/assets/js/components/quotation_material/Create.vue")
//     .default
// );
// Vue.component(
//     "quotation-material-edit",
//     require("../../Modules/Transaction/Resources/assets/js/components/quotation_material/Edit.vue")
//     .default
// );
Vue.component(
    "sales-order-material-create",
    require("../../Modules/Transaction/Resources/assets/js/components/sales_order_material/Create.vue")
    .default
);
Vue.component(
    "sales-order-material-edit",
    require("../../Modules/Transaction/Resources/assets/js/components/sales_order_material/Edit.vue")
    .default
);
// Vue.component(
//     "delivery-order-material-create",
//     require("../../Modules/Transaction/Resources/assets/js/components/delivery_order_material/Create.vue")
//     .default
// );
// Vue.component(
//     "delivery-order-material-edit",
//     require("../../Modules/Transaction/Resources/assets/js/components/delivery_order_material/Edit.vue")
//     .default
// );
Vue.component(
    "stock-transfer-create",
    require("../../Modules/Stock/Resources/assets/js/components/stock_transfer/Create.vue")
    .default
);
Vue.component(
    "stock-transfer-edit",
    require("../../Modules/Stock/Resources/assets/js/components/stock_transfer/Edit.vue")
    .default
);
Vue.component(
    "stock-checking-create",
    require("../../Modules/Stock/Resources/assets/js/components/stock_checking/Create.vue")
    .default
);
Vue.component(
    "stock-checking-edit",
    require("../../Modules/Stock/Resources/assets/js/components/stock_checking/Edit.vue")
    .default
);
// Vue.component('receivable-create', require('../../Modules/Finance/Resources/assets/js/receivable/Create.vue').default);
// Vue.component('receivable-edit', require('../../Modules/Finance/Resources/assets/js/receivable/Edit.vue').default);
//
// Vue.component('payable-create', require('../../Modules/Finance/Resources/assets/js/payable/Create.vue').default);

Vue.component('price-history', require('../../Modules/Transaction/Resources/assets/js/components/sales_order_material/PriceHistory.vue').default);
// Vue.component('payable-edit', require('../../Modules/Finance/Resources/assets/js/payable/Edit.vue').default);

// Vue.component('purchase-return-create', require("../../Modules/Procurement/Resources/assets/js/components/purchase_return/Create.vue").default);
// Vue.component('purchase-return-edit', require("../../Modules/Procurement/Resources/assets/js/components/purchase_return/Edit.vue").default);
//
// Vue.component('sales-return-create', require("../../Modules/Transaction/Resources/assets/js/components/sales_return/Create").default);
// Vue.component('sales-return-edit', require("../../Modules/Transaction/Resources/assets/js/components/sales_return/Edit").default);
Vue.component('create-shift-setting', require('../../Modules/HR/Resources/assets/js/setting_shift/Create.vue').default);

Vue.component(
    "stock-in-create",
    require("../../Modules/Stock/Resources/assets/js/components/stock_in/Create.vue")
    .default
);
Vue.component(
    "stock-in-edit",
    require("../../Modules/Stock/Resources/assets/js/components/stock_in/Edit.vue")
    .default
);

// Vue.component('payable-deposit-create', require('../../Modules/Finance/Resources/assets/js/payable_deposit/payable_deposit.vue').default);
// Vue.component('payable-deposit-edit', require('../../Modules/Finance/Resources/assets/js/payable_deposit/edit_payable_deposit.vue').default);
//
// Vue.component('receivable-deposit-create', require('../../Modules/Finance/Resources/assets/js/receivable_deposit/receivable_deposit.vue').default);
// Vue.component('receivable-deposit-edit', require('../../Modules/Finance/Resources/assets/js/receivable_deposit/edit_receivable_deposit.vue').default);

Vue.component('Select2', Select2);
Vue.component('Loader', require('./components/commons/loader'));

const app = new Vue({
    el: '#app'
});
