<?php

return [

        'menu' => [
                'leave',
                'purchase requisition',
                'purchase order',
                'invoice',
                'quotation material',
                'sales order material',
                'delivery order material',
                'stock adjustment',
                'stock transfer',
                'invoice',
                'journal',
                'other income',
                'cost',
                'business travel',
                'receivable',
                'credit note',
                'debit note',
                'payable',
                'purchase return',
                'sales return',
                'payable deposit',
                'receivable deposit',
                'payment voucher',
                'receipt voucher',
        ],

];
